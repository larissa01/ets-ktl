package br.com.digio.uber.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.account.GetAccountUseCase
import br.com.digio.uber.domain.usecase.home.ConfigFeatureType
import br.com.digio.uber.domain.usecase.home.ConfigFeatureUseCase
import br.com.digio.uber.domain.usecase.remuneration.GetRemunerationIncomeUseCase
import br.com.digio.uber.home.mapper.AccountMapper
import br.com.digio.uber.home.mapper.FeatureItemMapper
import br.com.digio.uber.home.mapper.SimpleIncomeMapper
import br.com.digio.uber.home.uimodel.FeatureItemUiModel
import br.com.digio.uber.home.uimodel.FeaturePixItem
import br.com.digio.uber.util.combineWith
import br.com.digio.uber.util.safeLet

class FeatureListViewModel(
    configFeatureUseCase: ConfigFeatureUseCase,
    getAccountUseCase: GetAccountUseCase,
    getRemunerationIncomeUseCase: GetRemunerationIncomeUseCase
) : BaseViewModel() {

    val listFeatures: MutableLiveData<List<FeatureItemUiModel>> = MutableLiveData()

    val loaded: LiveData<Boolean> = MutableLiveData(false).combineWith(
        getAccountUseCase(Unit).exec().map(AccountMapper()),
        getRemunerationIncomeUseCase(Unit).exec().map(SimpleIncomeMapper()),
        configFeatureUseCase(ConfigFeatureType.CONFIG_FEATURE_TYPE_FEATURE_LIST).exec()
    ) { _, account, income, features ->
        safeLet(account?.cpf, income?.incomeIndex, features) { cpf, incomeIndex, items ->
            listFeatures.value = FeatureItemMapper(FeaturePixItem(cpf, incomeIndex)).map(items)
        }
        true
    }
}