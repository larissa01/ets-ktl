package br.com.digio.uber.home.fragment

import android.view.View
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.home.BR
import br.com.digio.uber.home.R
import br.com.digio.uber.home.databinding.HomeCardFragmentBinding
import br.com.digio.uber.home.viewmodel.CardViewModel
import br.com.digio.uber.util.Const
import org.koin.android.ext.android.inject

class HomeCardFragment : BaseViewModelFragment<HomeCardFragmentBinding, CardViewModel>() {

    override val bindingVariable: Int? = BR.cardViewModel
    override val getLayoutId: Int? = R.layout.home_card_fragment
    override val viewModel: CardViewModel by inject()
    override fun tag(): String = HomeFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it) }
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.fragmentLayoutCard -> navigation.navigationToDynamic(
                requireActivity(),
                Const.Activities.VIRTUAL_CARD_ACTIVITY
            )
        }
    }
}