package br.com.digio.uber.home.di.mapper

import br.com.digio.uber.home.mapper.AccountMapper
import org.koin.dsl.module

val homeMapperModule = module {
    single { AccountMapper() }
}