package br.com.digio.uber.home.fragment

import android.content.Intent
import android.view.View
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.home.BR
import br.com.digio.uber.home.R
import br.com.digio.uber.home.databinding.LayoutProfileDetailsViewBinding
import br.com.digio.uber.home.fragment.HomeFragment.Companion.ACCOUNT_PARAMETER
import br.com.digio.uber.home.uimodel.AccountUiModel
import br.com.digio.uber.home.viewmodel.ProfileDetailViewModel
import br.com.digio.uber.util.safeLet
import org.koin.android.ext.android.inject

class ProfileDetailFragment : BaseViewModelFragment<LayoutProfileDetailsViewBinding, ProfileDetailViewModel>() {

    override val bindingVariable: Int = BR.profileDetailViewModel
    override val getLayoutId: Int = R.layout.layout_profile_details_view
    override val viewModel: ProfileDetailViewModel by inject()

    override fun tag(): String = ProfileDetailFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() {
        super.initialize()
        setupViewModel()
        val accountModel = arguments?.get(ACCOUNT_PARAMETER)
        if (accountModel != null && accountModel is AccountUiModel) {
            viewModel.initializer({ onItemClicked(it) }, accountModel)
        }
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.btnShared ->
                onShareClick(viewModel.getShareOrCopyText())
            R.id.layoutKeyPix ->
                safeLet(
                    viewModel.accountUiModel.value,
                    viewModel.incomeIndexLiveData.value
                ) { account, incomeIndex ->
                    navigation.navigationToPixMyKey(requireContext(), account.cpf, incomeIndex)
                }
            R.id.layoutKeyRegistrationChange ->
                navigation.navigationToChangeRegistrationActivity(requireContext())
        }
    }

    private fun setupViewModel() {
        viewModel.initializer {}
    }

    private fun onShareClick(transferText: String) {
        val sharingIntent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, transferText)
        }

        requireContext().startActivity(
            Intent.createChooser(
                sharingIntent,
                getString(R.string.profileHeader_title_shareIntentChooser)
            )
        )
    }
}