package br.com.digio.uber.home.activity

import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.home.R
import br.com.digio.uber.home.di.mapper.homeMapperModule
import br.com.digio.uber.home.di.viewmodel.homeViewModelModule
import org.koin.core.module.Module

class HomeTempActivity : BaseActivity() {

    override fun getLayoutId(): Int? = R.layout.home_temp_activity

    override fun getModule(): List<Module>? =
        listOf(homeViewModelModule, homeMapperModule)
}