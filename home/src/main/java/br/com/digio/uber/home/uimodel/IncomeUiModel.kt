package br.com.digio.uber.home.uimodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class IncomeUiModel(
    val incomeIndex: String,
    val isAvailable: Boolean,
) : Parcelable