package br.com.digio.uber.home.adapter.viewholder

import android.annotation.SuppressLint
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.home.databinding.BalanceListTagItemBinding
import br.com.digio.uber.home.uimodel.FeatureItemUI
import br.com.digio.uber.home.uimodel.FeatureItemUiModel

class BalanceTagListViewHolder(
    private val viewDataBinding: BalanceListTagItemBinding,
    private val listener: ViewTypesListener<FeatureItemUiModel>
) : AbstractViewHolder<FeatureItemUI>(viewDataBinding.root) {

    @SuppressLint("DefaultLocale")
    override fun bind(item: FeatureItemUI) {
        viewDataBinding.balanceTagListViewHolder = this
        viewDataBinding.balanceTagItem = item
        viewDataBinding.executePendingBindings()
    }

    fun onClick(item: FeatureItemUI) {
        listener(item)
    }
}