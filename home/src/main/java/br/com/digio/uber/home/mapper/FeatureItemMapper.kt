package br.com.digio.uber.home.mapper

import android.os.Bundle
import br.com.digio.uber.common.util.FeatureInfoRoute
import br.com.digio.uber.common.util.FeatureInfoRoute.Companion.FEATURE_PIX
import br.com.digio.uber.common.util.FeatureInfoRoute.Companion.FEATURE_RECEIVE
import br.com.digio.uber.home.uimodel.FeatureItemUI
import br.com.digio.uber.home.uimodel.FeatureItemUiModel
import br.com.digio.uber.home.uimodel.FeaturePixItem
import br.com.digio.uber.model.config.FeatureItem
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.mapper.AbstractMapper

class FeatureItemMapper(
    private val featurePixItem: FeaturePixItem? = null
) : AbstractMapper<List<FeatureItem>, List<FeatureItemUiModel>> {

    override fun map(param: List<FeatureItem>): List<FeatureItemUiModel> =
        param.mapNotNull {
            FeatureInfoRoute.getFeature(it.id)?.let { featureRoute ->
                if (featureRoute.id == FEATURE_PIX || featureRoute.id == FEATURE_RECEIVE) {
                    val bundle = if (featureRoute.bundle != null) featureRoute.bundle else Bundle()

                    FeatureItemUI(
                        label = it.label,
                        image = featureRoute.iconDefault,
                        activity = featureRoute.route,
                        bundle = bundle?.apply {
                            putString(Const.Extras.ARG_CPF, featurePixItem?.document ?: "")
                            putString(Const.Extras.ARG_INCOME, featurePixItem?.incomeIndex ?: "")
                        }
                    )
                } else {
                    FeatureItemUI(
                        label = it.label,
                        image = featureRoute.iconDefault,
                        activity = featureRoute.route,
                        bundle = featureRoute.bundle
                    )
                }
            }
        }
}