package br.com.digio.uber.home.adapter.viewholder

import android.annotation.SuppressLint
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.home.databinding.ShortcutListItemBinding
import br.com.digio.uber.home.uimodel.FeatureItemUI
import br.com.digio.uber.home.uimodel.FeatureItemUiModel

class ShortcutListViewHolder(
    private val viewDataBinding: ShortcutListItemBinding,
    private val listener: ViewTypesListener<FeatureItemUiModel>
) : AbstractViewHolder<FeatureItemUI>(viewDataBinding.root) {

    @SuppressLint("DefaultLocale")
    override fun bind(item: FeatureItemUI) {
        viewDataBinding.sthortcuItem = item
        viewDataBinding.sthortcuListItemViewHolder = this
        viewDataBinding.executePendingBindings()
    }

    fun onClick(item: FeatureItemUI) {
        listener(item)
    }
}