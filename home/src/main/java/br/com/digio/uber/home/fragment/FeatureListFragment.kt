package br.com.digio.uber.home.fragment

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.home.BR
import br.com.digio.uber.home.R
import br.com.digio.uber.home.adapter.FeatureListAdapter
import br.com.digio.uber.home.databinding.FeatureListFragmentBinding
import br.com.digio.uber.home.uimodel.FeatureItemUI
import br.com.digio.uber.home.viewmodel.FeatureListViewModel
import br.com.digio.uber.util.Const
import org.koin.android.ext.android.inject

class FeatureListFragment : BaseViewModelFragment<FeatureListFragmentBinding, FeatureListViewModel>() {

    override val bindingVariable: Int? = BR.featureListViewModel
    override val getLayoutId: Int? = R.layout.feature_list_fragment
    override val viewModel: FeatureListViewModel by inject()
    override fun tag(): String = FeatureListFragment::class.java.name

    private val adapter: FeatureListAdapter by lazy {
        FeatureListAdapter(
            listener = { value ->
                if (value is FeatureItemUI) {
                    if (value.isRefreshBack == true) {
                        navigation.navigationToDynamicWithResult(
                            requireActivity(),
                            value.activity,
                            value.bundle,
                            Const.RequestOnResult.FeatureRefreshHome.KEY_FEATURE_REFRESH_BACK
                        )
                    } else {
                        navigation.navigationToDynamic(requireActivity(), value.activity, value.bundle)
                    }
                }
            }
        )
    }

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() {
        super.initialize()
        setupAdapter()
        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel.initializer { onItemClicked(it) }
    }

    private fun setupAdapter() {
        binding?.apply {
            recyclerFeatures.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            recyclerFeatures.adapter = adapter
        }
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.textView_feature_list_arrow_left -> {
            }
            R.id.textView_feature_list_arrow_right -> {
            }
        }
    }
}