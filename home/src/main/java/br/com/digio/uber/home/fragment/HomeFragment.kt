package br.com.digio.uber.home.fragment

import android.annotation.SuppressLint
import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.initHostFragment
import br.com.digio.uber.common.extensions.stateGetLiveData
import br.com.digio.uber.common.extensions.stateSetValue
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.home.BR
import br.com.digio.uber.home.R
import br.com.digio.uber.home.databinding.HomeFragmentBinding
import br.com.digio.uber.home.viewmodel.HomeViewModel
import br.com.digio.uber.util.Const
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.layout_profile_header.*
import org.koin.android.ext.android.inject

class HomeFragment : BaseViewModelFragment<HomeFragmentBinding, HomeViewModel>() {

    override val bindingVariable: Int = BR.homeViewModel
    override val getLayoutId: Int = R.layout.home_fragment
    override val viewModel: HomeViewModel by inject()
    override fun tag(): String = HomeFragment::class.java.name

    private val hostNavigationFragmentList: HashMap<Int, Int> = hashMapOf(
        R.id.nav_host_balance_fragment to R.id.balanceFragment,
        R.id.nav_host_list_feature to R.id.featureListFragment,
        R.id.nav_host_card to R.id.cardFragment,
        R.id.nav_host_shortcut to R.id.shortcutFragment
    )

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() {
        super.initialize()
        initHostFragment(hostNavigationFragmentList)
        setupViewModel()
        setupEvent()
        setupObserve()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupEvent() {
        imageView_profileView_arrow.setOnClickListener {
            it.visibility = View.GONE
            imageView_profileView_close.visibility = View.VISIBLE
            openProfileDetail()
        }

        textView_profileView_name.setOnClickListener {
            it.visibility = View.VISIBLE
            imageView_profileView_close.visibility = View.VISIBLE
            openProfileDetail()
        }

        imageView_profileView_avatar.setOnClickListener {
            it.visibility = View.VISIBLE
            imageView_profileView_close.visibility = View.VISIBLE
            openProfileDetail()
        }

        imageView_profileView_close.setOnClickListener {
            it.visibility = View.VISIBLE
            imageView_profileView_close.visibility = View.GONE
            openProfileDetail()
        }

        view_header_content.setOnClickListener {
            it.visibility = View.VISIBLE
            imageView_profileView_close.visibility = View.GONE
            openProfileDetail()
        }

        constraintLayoutMotionHomeFragment.setTransitionListener(
            object : MotionLayout.TransitionListener {
                override fun onTransitionTrigger(
                    p0: MotionLayout?,
                    p1: Int,
                    p2: Boolean,
                    p3: Float
                ) {
                    /*empty*/
                }

                override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {
                    viewModel.expand.value = !viewModel.expand.value!!
                }

                override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {
                    /*empty*/
                }

                override fun onTransitionCompleted(p0: MotionLayout?, p1: Int) {
                    if (viewModel.expand.value == true) {
                        layout_fragment_home_content.visibility = View.GONE
                    } else {
                        layout_fragment_home_content.visibility = View.VISIBLE
                    }
                }
            }
        )
    }

    private fun setupViewModel() {
        viewModel.initializer {}
    }

    private fun openProfileDetail() {
        action_profile_conta.performClick()
    }

    private fun setupObserve() {
        viewModel.accountUiModel.observe(
            this,
            Observer {
                initHostFragment(
                    R.id.nav_host_toolbar_detail,
                    R.id.ProfileFragment,
                    bundleOf(ACCOUNT_PARAMETER to it)
                )
            }
        )

        viewModel.updateValues.observe(
            this,
            Observer {
                if (it == true) {
                    initHostFragment(hostNavigationFragmentList)
                    viewModel.updateValues.value = false
                }
            }
        )

        stateGetLiveData<Boolean>(Const.RequestOnResult.FeatureRefreshHome.FEATURE_ITEM_RESULT_SUCCESS)?.observe(
            this,
            Observer {
                if (it == true) {
                    viewModel.onRefresh()
                    stateSetValue(
                        Const.RequestOnResult.FeatureRefreshHome.FEATURE_ITEM_RESULT_SUCCESS,
                        false
                    )
                }
            }
        )
    }

    companion object {
        const val ACCOUNT_PARAMETER: String = "ACCOUNT_PARAMETER"
    }
}