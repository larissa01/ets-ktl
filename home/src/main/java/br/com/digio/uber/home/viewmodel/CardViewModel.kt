package br.com.digio.uber.home.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.home.uimodel.CardHomeUiModel

class CardViewModel : BaseViewModel() {

    val cardUi = MutableLiveData<CardHomeUiModel>()

    private val cardHomeUiHomeDefault: CardHomeUiModel by lazy {
        CardHomeUiModel()
    }

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        cardUi.value = cardHomeUiHomeDefault
    }
}