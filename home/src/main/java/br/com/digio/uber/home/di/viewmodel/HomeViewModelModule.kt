package br.com.digio.uber.home.di.viewmodel

import br.com.digio.uber.home.viewmodel.BalanceViewModel
import br.com.digio.uber.home.viewmodel.CardViewModel
import br.com.digio.uber.home.viewmodel.FeatureListViewModel
import br.com.digio.uber.home.viewmodel.HomeViewModel
import br.com.digio.uber.home.viewmodel.ProfileDetailViewModel
import br.com.digio.uber.home.viewmodel.ShortcutViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val homeViewModelModule = module {
    viewModel { HomeViewModel(get(), get(), get()) }
    viewModel { ProfileDetailViewModel(get(), get()) }
    viewModel { BalanceViewModel(get(), get(), get(), get(), get(), get()) }
    viewModel { CardViewModel() }
    viewModel { FeatureListViewModel(get(), get(), get()) }
    viewModel { ShortcutViewModel(get()) }
}