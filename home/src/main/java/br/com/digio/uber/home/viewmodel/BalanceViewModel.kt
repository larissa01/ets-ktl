package br.com.digio.uber.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.usecase.balance.GetBalanceUseCase
import br.com.digio.uber.domain.usecase.eyes.GetEyesUseCase
import br.com.digio.uber.domain.usecase.eyes.InsertEyesUseCase
import br.com.digio.uber.domain.usecase.home.ConfigFeatureType
import br.com.digio.uber.domain.usecase.home.ConfigFeatureUseCase
import br.com.digio.uber.domain.usecase.remuneration.GetRemunerationIncomeUseCase
import br.com.digio.uber.home.mapper.BalanceMapper
import br.com.digio.uber.home.mapper.FeatureItemMapper
import br.com.digio.uber.home.mapper.IncomeMapper
import br.com.digio.uber.home.uimodel.BalanceUiModel
import br.com.digio.uber.home.uimodel.FeatureItemUiModel
import br.com.digio.uber.home.uimodel.IncomeUiModel
import br.com.digio.uber.util.combineWith
import kotlinx.coroutines.ExperimentalCoroutinesApi

class BalanceViewModel constructor(
    private val getBalanceUseCase: GetBalanceUseCase,
    getEyesUseCase: GetEyesUseCase,
    val insertEyesUseCase: InsertEyesUseCase,
    configFeatureUseCase: ConfigFeatureUseCase,
    getRemunerationIncomeUseCase: GetRemunerationIncomeUseCase,
    resourceManager: ResourceManager
) : BaseViewModel() {

    val balanceUiModel: MutableLiveData<BalanceUiModel> = MutableLiveData<BalanceUiModel>()
    val tagList = MutableLiveData<List<FeatureItemUiModel>>()
    val incomeUiModel = MutableLiveData<IncomeUiModel>()
    var loaded: LiveData<Boolean> = MutableLiveData(false).combineWith(
        getRemunerationIncomeUseCase(Unit).exec().map(IncomeMapper(resourceManager)),
        configFeatureUseCase(ConfigFeatureType.CONFIG_FEATURE_TYPE_TAG_LIST).exec()
            .map(FeatureItemMapper())
    ) { _, income, list ->
        tagList.value = list
        incomeUiModel.value = income
        true
    }

    val mutableBalanceVisibility = getEyesUseCase(Unit)

    @ExperimentalCoroutinesApi
    fun balanceVisibilityChanged(isChecked: Boolean) {
        if (isChecked != mutableBalanceVisibility.value) {
            insertEyesUseCase(isChecked).singleExec()
        }
    }

    fun getBalance() {
        getBalanceUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                balanceUiModel.value = BalanceMapper().map(it)
            }
        )
    }
}