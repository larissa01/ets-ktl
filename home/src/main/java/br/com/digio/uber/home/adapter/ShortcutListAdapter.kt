package br.com.digio.uber.home.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.home.R
import br.com.digio.uber.home.adapter.viewholder.ShortcutListViewHolder
import br.com.digio.uber.home.databinding.ShortcutListItemBinding
import br.com.digio.uber.home.uimodel.FeatureItemUI
import br.com.digio.uber.home.uimodel.FeatureItemUiModel
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

typealias OnClickShortcutItem = (item: FeatureItemUiModel) -> Unit

class ShortcutListAdapter(
    private var values: MutableList<FeatureItemUiModel> = mutableListOf(),
    private val listener: OnClickShortcutItem
) : BaseRecyclerViewAdapter<FeatureItemUiModel, ShortcutListAdapter.ViewTypesDataBindingFactoryImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl =
        ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): FeatureItemUiModel =
        values[position]

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<FeatureItemUiModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<FeatureItemUiModel>
    }

    override fun getItemCount(): Int =
        values.size

    override fun onBindViewHolder(
        holder: AbstractViewHolder<FeatureItemUiModel>,
        position: Int
    ) = holder.bind(values[holder.adapterPosition])

    class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<FeatureItemUiModel> {
        override fun type(model: FeatureItemUiModel): Int =
            when (model) {
                is FeatureItemUI -> R.layout.shortcut_list_item
            }

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<FeatureItemUiModel>
        ): AbstractViewHolder<*> =
            when (type) {
                R.layout.shortcut_list_item -> ShortcutListViewHolder(
                    view.safeHeritage<ShortcutListItemBinding>()!!,
                    listener
                )
                else -> throw IndexOutOfBoundsException("Invalid view type")
            }
    }

    override fun replaceItems(list: List<Any>) {
        addValues(list.safeHeritage())
    }

    private fun addValues(values: List<FeatureItemUiModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }
}