package br.com.digio.uber.home.uimodel

import br.com.digio.uber.common.R

data class CardHomeUiModel(
    val backgroundImage: Int = R.drawable.ic_card_debit,
    val flagImage: Int? = R.drawable.ic_logo_elo
)