package br.com.digio.uber.home.fragment

import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.home.BR
import br.com.digio.uber.home.R
import br.com.digio.uber.home.adapter.BalanceTagListAdapter
import br.com.digio.uber.home.databinding.BalanceFragmentBinding
import br.com.digio.uber.home.uimodel.FeatureItemUI
import br.com.digio.uber.home.viewmodel.BalanceViewModel
import br.com.digio.uber.util.Const.ShimmerConfig.DELAYED_DEFAULT
import kotlinx.android.synthetic.main.balance_fragment.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject

class BalanceFragment : BaseViewModelFragment<BalanceFragmentBinding, BalanceViewModel>() {

    override val bindingVariable: Int = BR.balanceViewModel
    override val getLayoutId: Int = R.layout.balance_fragment
    override val viewModel: BalanceViewModel by inject()
    override fun tag(): String = BalanceFragment::class.java.name

    private val adapter: BalanceTagListAdapter by lazy {
        BalanceTagListAdapter(
            listener = { value ->
                if (value is FeatureItemUI) {
                    navigation.navigationToDynamic(requireActivity(), value.activity, value.bundle)
                }
            }
        )
    }

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    @ExperimentalCoroutinesApi
    override fun initialize() {
        super.initialize()
        setupAdapter()
        setupObserve()
        setupEvent()
        viewModel.initializer {}
    }

    override fun onResume() {
        super.onResume()
        viewModel.getBalance()
    }

    @ExperimentalCoroutinesApi
    private fun setupEvent() {
        btnBalanceVisibility.setOnCheckedChangeListener { _, isChecked ->
            viewModel.balanceVisibilityChanged(isChecked)
        }
    }

    private fun setupObserve() {
        viewModel.loaded.observe(
            this
        ) {
            if (!it) shimmerBalance?.startLayoutAnimation()
            else {
                shimmerBalance?.postDelayed(
                    { shimmerBalance?.hideShimmer() },
                    DELAYED_DEFAULT
                )
            }
        }
    }

    private fun setupAdapter() {
        binding?.apply {
            recyclerBalanceTagFeature.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            recyclerBalanceTagFeature.adapter = adapter
        }
    }
}