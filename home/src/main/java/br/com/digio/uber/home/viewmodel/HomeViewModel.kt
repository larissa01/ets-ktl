package br.com.digio.uber.home.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.GenericEvent
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.account.GetAccountUseCase
import br.com.digio.uber.domain.usecase.balance.ClearHomeCacheUseCase
import br.com.digio.uber.home.mapper.AccountMapper
import br.com.digio.uber.home.uimodel.AccountUiModel

class HomeViewModel constructor(
    private val getAccountUseCase: GetAccountUseCase,
    private val accountMapper: AccountMapper,
    private val clearHomeCacheUseCase: ClearHomeCacheUseCase
) : BaseViewModel() {

    val accountUiModel: MutableLiveData<AccountUiModel> = MutableLiveData()
    val expand: MutableLiveData<Boolean> = MutableLiveData(false)

    val updateValues: MutableLiveData<Boolean> = MutableLiveData()
    val isRefresh: MutableLiveData<Boolean> = MutableLiveData()

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        getAccountUseCase()
    }

    val onRefresh: GenericEvent = {
        clearHomeCacheUseCase(Unit).singleExec(showLoadingFlag = false)
        updateValues.value = true
        getAccountUseCase(true)
    }

    private fun getAccountUseCase(forceUpdate: Boolean = false) {
        getAccountUseCase(Unit, forceUpdate).singleExec(
            onSuccessBaseViewModel = {
                accountUiModel.value = accountMapper.map(it)
                isRefresh.value = false
            }
        )
    }
}