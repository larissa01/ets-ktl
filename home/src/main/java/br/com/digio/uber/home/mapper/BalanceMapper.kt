package br.com.digio.uber.home.mapper

import br.com.digio.uber.common.extensions.toMoney
import br.com.digio.uber.common.extensions.toMoneyOrEmpty
import br.com.digio.uber.home.uimodel.BalanceUiModel
import br.com.digio.uber.model.BalanceResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class BalanceMapper : AbstractMapper<BalanceResponse, BalanceUiModel> {

    override fun map(param: BalanceResponse): BalanceUiModel {
        return BalanceUiModel(
            param.balance.toMoney(),
            param.blockedBalance.toMoneyOrEmpty(),
            param.blockedBalance != null &&
                param.blockedBalance != 0.toBigDecimal()
        )
    }
}