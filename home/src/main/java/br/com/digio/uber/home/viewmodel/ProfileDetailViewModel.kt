package br.com.digio.uber.home.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.remuneration.GetRemunerationIncomeUseCase
import br.com.digio.uber.home.R
import br.com.digio.uber.home.uimodel.AccountUiModel

class ProfileDetailViewModel constructor(
    private val resourceManager: ResourceManager,
    private val getRemunerationIncomeUseCase: GetRemunerationIncomeUseCase
) : BaseViewModel() {

    val accountUiModel = MutableLiveData<AccountUiModel>()
    val incomeIndexLiveData = MutableLiveData<String>()

    fun initializer(onClickItem: OnClickItem, accountModel: AccountUiModel?) {
        super.initializer(onClickItem)
        accountUiModel.value = accountModel
        getRemunerationIncomeUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                incomeIndexLiveData.value = it.incomeIndex
            }
        )
    }

    fun getShareOrCopyText(): String {
        accountUiModel.value?.apply {
            return resourceManager.getString(
                R.string.home_profile_share_copy_text,
                fullName,
                cpf,
                bank,
                agency,
                accountNumber
            )
        }
        return ""
    }
}