package br.com.digio.uber.home.fragment

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.home.BR
import br.com.digio.uber.home.R
import br.com.digio.uber.home.adapter.ShortcutListAdapter
import br.com.digio.uber.home.databinding.ShortcutFragmentBinding
import br.com.digio.uber.home.uimodel.FeatureItemUI
import br.com.digio.uber.home.viewmodel.ShortcutViewModel
import org.koin.android.ext.android.inject

class ShortcutFragment : BaseViewModelFragment<ShortcutFragmentBinding, ShortcutViewModel>() {

    override val bindingVariable: Int? = BR.shortcutViewModel
    override val getLayoutId: Int? = R.layout.shortcut_fragment
    override val viewModel: ShortcutViewModel by inject()
    override fun tag(): String = ShortcutFragment::class.java.name
    private val adapter: ShortcutListAdapter by lazy {
        ShortcutListAdapter(
            listener = { value ->
                if (value is FeatureItemUI) {
                    navigation.navigationToDynamic(requireActivity(), value.activity, value.bundle)
                }
            }
        )
    }

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() {
        super.initialize()
        setupAdapter()
        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel.initializer {}
    }

    private fun setupAdapter() {
        binding?.apply {
            recyclerShortcut.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            recyclerShortcut.adapter = adapter
        }
    }
}