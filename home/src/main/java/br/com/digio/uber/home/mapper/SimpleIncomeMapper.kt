package br.com.digio.uber.home.mapper

import br.com.digio.uber.home.uimodel.IncomeUiModel
import br.com.digio.uber.model.remuneration.IncomeIndexResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class SimpleIncomeMapper : AbstractMapper<IncomeIndexResponse, IncomeUiModel> {

    override fun map(param: IncomeIndexResponse): IncomeUiModel {
        return IncomeUiModel(
            param.incomeIndex ?: "",
            param.isAvailable ?: false,
        )
    }
}