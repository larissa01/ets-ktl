package br.com.digio.uber.home.uimodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AccountUiModel(
    val cpf: String,
    val fullName: String,
    val nickName: String,
    val nameInitials: String,
    val image: String?,
    val bank: String,
    val agency: String,
    val accountNumber: String,
    val statusCode: String = "",
    val productType: String = ""
) : Parcelable