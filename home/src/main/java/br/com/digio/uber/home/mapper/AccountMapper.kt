package br.com.digio.uber.home.mapper

import br.com.digio.uber.home.uimodel.AccountUiModel
import br.com.digio.uber.model.account.AccountResponse
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.capitalizeWords
import br.com.digio.uber.util.formatAsAccountNumber
import br.com.digio.uber.util.formatAsAgencyNumber
import br.com.digio.uber.util.getFirstName
import br.com.digio.uber.util.getNameAbbreviation
import br.com.digio.uber.util.mapper.AbstractMapper

class AccountMapper : AbstractMapper<AccountResponse, AccountUiModel> {
    override fun map(param: AccountResponse): AccountUiModel {
        return AccountUiModel(
            cpf = param.cpf.applyMask(Const.MaskPattern.CPF),
            fullName = param.fullName.getFirstName().capitalizeWords(),
            nickName = param.fullName.getFirstName().capitalizeWords(),
            nameInitials = param.fullName.getNameAbbreviation(),
            accountNumber = param.accountNumber.formatAsAccountNumber(),
            bank = param.bank,
            agency = param.branch.formatAsAgencyNumber(),
            image = ""
        )
    }
}