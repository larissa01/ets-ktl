package br.com.digio.uber.home.mapper

import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.home.R
import br.com.digio.uber.home.uimodel.IncomeUiModel
import br.com.digio.uber.model.remuneration.IncomeIndexResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class IncomeMapper(
    private val resourceManager: ResourceManager,
) : AbstractMapper<IncomeIndexResponse, IncomeUiModel> {

    override fun map(param: IncomeIndexResponse): IncomeUiModel {
        return IncomeUiModel(
            resourceManager.getString(R.string.balance_card_info_value_income).format(param.incomeIndex) ?: "",
            param.isAvailable ?: false,
        )
    }
}