package br.com.digio.uber.home.viewmodel

import androidx.lifecycle.LiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.home.ConfigFeatureType
import br.com.digio.uber.domain.usecase.home.ConfigFeatureUseCase
import br.com.digio.uber.home.mapper.FeatureItemMapper
import br.com.digio.uber.home.uimodel.FeatureItemUiModel

class ShortcutViewModel(
    private val configFeatureUseCase: ConfigFeatureUseCase
) : BaseViewModel() {
    val shortcutList: LiveData<List<FeatureItemUiModel>> =
        configFeatureUseCase(ConfigFeatureType.CONFIG_FEATURE_TYPE_SHORTCUT_LIST).exec().map(
            FeatureItemMapper()
        )
}