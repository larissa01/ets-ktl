package br.com.digio.uber.home.uimodel

import android.os.Bundle
import android.os.Parcelable
import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import kotlinx.android.parcel.Parcelize

sealed class FeatureItemUiModel : AdapterViewModel<FeatureItemUiModel> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<FeatureItemUiModel>) = typesFactory.type(
        model = this
    )
}

@Parcelize
data class FeatureItemUI(
    val label: String = "",
    val image: Int? = -1,
    val activity: String,
    val contentDescription: String = "",
    val isRefreshBack: Boolean? = false,
    val bundle: Bundle? = null
) : Parcelable, FeatureItemUiModel()