package br.com.digio.uber.home.uimodel

data class FeaturePixItem(val document: String, val incomeIndex: String)