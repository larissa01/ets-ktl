@file:Suppress("TooManyFunctions")

package br.com.digio.uber.statement.ui.fragment

import android.os.Bundle
import androidx.core.widget.NestedScrollView
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.generics.makeMessageSuccessObject
import br.com.digio.uber.common.generics.makeStatementMessageObject
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.ReceiptOrigin
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.statement.BR
import br.com.digio.uber.statement.R
import br.com.digio.uber.statement.databinding.FragmentStatementMainBinding
import br.com.digio.uber.statement.navigation.StatementOpKey
import br.com.digio.uber.statement.ui.adapter.futureEntries.OnClickStatementFutureEntryItem
import br.com.digio.uber.statement.ui.adapter.futureEntries.StatementFutureEntriesAdapter
import br.com.digio.uber.statement.ui.adapter.pastEntries.OnClickStatementPastEntryItem
import br.com.digio.uber.statement.ui.adapter.pastEntries.StatementPastEntriesAdapter
import br.com.digio.uber.statement.ui.uiModel.EntryCategory
import br.com.digio.uber.statement.ui.uiModel.FutureEntry
import br.com.digio.uber.statement.ui.uiModel.PastEntriesData
import br.com.digio.uber.statement.ui.uiModel.PastEntry
import br.com.digio.uber.statement.ui.uiModel.StatementDetailUIModel
import br.com.digio.uber.statement.ui.uiModel.StatementEntriesListener
import br.com.digio.uber.statement.ui.uiModel.StatementFutureUiModel
import br.com.digio.uber.statement.ui.uiModel.StatementPastUiModel
import br.com.digio.uber.statement.ui.viewModel.StatementMainViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.Extras.RECEIPT_ORIGIN
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class StatementMainFragment :
    BNWFViewModelFragment<FragmentStatementMainBinding, StatementMainViewModel>(),
    StatementEntriesListener.Future,
    StatementEntriesListener.Past {

    override val bindingVariable: Int = BR.statementMainViewModel
    override val getLayoutId = R.layout.fragment_statement_main
    override val viewModel: StatementMainViewModel by viewModel()
    override fun tag(): String = StatementOpKey.MAIN.name
    override fun getStatusBarAppearance(): StatusBarColor = StatementOpKey.MAIN.theme
    private var itemToShow: PastEntry? = null

    private val adapter: StatementPastEntriesAdapter by lazy {
        StatementPastEntriesAdapter(
            entryListener = this,
            listener = object : OnClickStatementPastEntryItem {
                override fun invoke(item: StatementPastUiModel) {
                    Timber.d("")
                }
            }
        )
    }

    companion object : FragmentNewInstance<StatementMainFragment>(::StatementMainFragment)

    override fun initialize() {
        viewModel.loadStatementLists()
        setupObservables()

        setupPaginationListener()
    }

    private fun setupObservables() {
        viewModel.pastEntries.observe(
            this,
            {
                if (it != null) {
                    adapter.replaceItems(it.dateList.toMutableList())
                }
            }
        )
        viewModel.futureEntries.observe(
            this,
            {
                if (it != null) {
                    binding?.rvStatementFutureEntries?.adapter =
                        StatementFutureEntriesAdapter(
                            it.futureDataList.toMutableList(),
                            this,
                            object : OnClickStatementFutureEntryItem {
                                override fun invoke(item: StatementFutureUiModel) {
                                    Timber.d("")
                                }
                            }
                        )
                }
            }
        )
        viewModel.onDeleteScheduleSuccess.observe(
            this,
            {
                if (it && context != null) {
                    showCancelSuccess()
                    viewModel.loadStatementLists()
                }
            }
        )
        viewModel.mutableAccount.observe(viewLifecycleOwner, {})

        viewModel.detailStatementResponse.observe(
            this,
            { statementDetail ->
                itemToShow?.let {
                    showPastDialog(it, statementDetail)
                }
            }
        )
    }

    private fun showCancelSuccess() {
        val cancelSuccessObject = makeMessageSuccessObject {
            icon = br.com.digio.uber.common.R.drawable.ic_success
            title = R.string.schedule_delete_success
            hideMessage = true
        }

        showMessage(cancelSuccessObject)
    }

    private fun openReceipt(entryId: String, isPayment: Boolean = false) {
        activity?.let {
            val bundle = Bundle()
            bundle.putString(Const.Extras.RECEIPT_ID, entryId)
            bundle.putBoolean(Const.Extras.PAYMENT_RECEIPT, isPayment)
            bundle.putSerializable(RECEIPT_ORIGIN, ReceiptOrigin.STATEMENT)
            navigation.navigationToDynamic(it, Const.Activities.RECEIPT_ACTIVITY, bundle)
        }
    }

    override fun onClickFutureEntry(futureEntry: FutureEntry) {
        val statementDialogObject = makeStatementMessageObject {
            this.icon = futureEntry.categoryDrawableId
            this.titleString = futureEntry.entryName
            this.messageString = formatDialogDescription(futureEntry.description)
            this.amountValue = futureEntry.amount
            this.dateValue = futureEntry.dateText
            hideMessage = futureEntry.category == EntryCategory.BILL_PAYMENT
        }

        if (futureEntry.id != null && futureEntry.hasAttachment) {
            statementDialogObject.showPositiveButton = true
            statementDialogObject.positiveOptionTextInt = R.string.see_receipt
            statementDialogObject.onPositiveButtonClick =
                { openReceipt(futureEntry.id, futureEntry.category == EntryCategory.BILL_PAYMENT) }

            statementDialogObject.showNegativeButton = true
            statementDialogObject.negativeOptionTextInt = R.string.cancel_schedule
            statementDialogObject.onNegativeButtonClick = { viewModel.showCancelDialog(futureEntry.id) }
        } else {
            statementDialogObject.showPositiveButton = false
        }

        showMessage(statementDialogObject)
    }

    private fun showPastDialog(pastEntry: PastEntry, entryDetailUIModel: StatementDetailUIModel? = null) {
        val statementDialogObject = makeStatementMessageObject {
            this.icon = pastEntry.categoryDrawableId
            this.titleString = pastEntry.entryName
            this.messageString = formatDialogDescription(pastEntry.description)
            this.amountValue = pastEntry.amount
            this.dateValue = pastEntry.dateText
        }

//        if (entryDetailUIModel?.action == RefundType.MAY_REFUND) {
//            statementDialogObject.showPositiveButton = true
//            statementDialogObject.positiveOptionTextInt = R.string.see_receipt
//            statementDialogObject.onPositiveButtonClick = {
//                pastEntry.id?.let {
//                    openReceipt(it, pastEntry.category == EntryCategory.BILL_PAYMENT)
//                }
//            }
//            statementDialogObject.negativeOptionTextInt = R.string.pix_refund
//            statementDialogObject.onNegativeButtonClick = {
//                // TODO chamar tela de devolução do PIX
//                viewModel.analytics.pushSimpleEvent(PixTags.PIX_DEVOLVER)
//            }
//        }
        if (pastEntry.id != null && pastEntry.hasAttachment) {
            statementDialogObject.showPositiveButton = true
            statementDialogObject.positiveOptionTextInt = R.string.see_receipt
            statementDialogObject.onPositiveButtonClick =
                { openReceipt(pastEntry.id, pastEntry.category == EntryCategory.BILL_PAYMENT) }
        } else {
            statementDialogObject.showPositiveButton = false
        }

        // statementDialogObject.showNegativeButton = entryDetailUIModel?.action == RefundType.MAY_REFUND
        statementDialogObject.showNegativeButton = false

        showMessage(statementDialogObject)
    }

    override fun onClickPastEntry(pastEntry: PastEntry) {
        if (pastEntry.hasDetails) {
            pastEntry.id?.let {
                itemToShow = pastEntry
                viewModel.getStatementDetail(it)
            }
        } else {
            showPastDialog(pastEntry)
        }
    }

    private fun formatDialogDescription(description: String?): String? {
        if (description != null && description.contains('|') && description.split("|").size > 1) {
            val splittedDescription = description.split('|')
            return splittedDescription[1]
        }
        return description ?: ""
    }

    private fun setupPaginationListener() {
        val scrollView = activity?.findViewById<NestedScrollView>(br.com.digio.uber.common.R.id.collapsingScrollView)
        scrollView?.let {
            it.setOnScrollChangeListener(
                NestedScrollView.OnScrollChangeListener { v, _, _, _, _ ->

                    v?.apply {
                        if (!canScrollVertically(1) && canLoadMoreStatements()) {
                            viewModel.loadingDownPage.value = true
                            binding?.apply {
                                val adapter = rvStatementPastEntries.adapter as StatementPastEntriesAdapter
                                viewModel.loadMore(adapter.values.last() as PastEntriesData)
                            }
                        }
                    }
                }
            )
        }
        binding?.rvStatementPastEntries?.adapter = adapter
    }

    private fun canLoadMoreStatements(): Boolean {
        var canLoad = false
        binding?.apply {
            val adapter = rvStatementPastEntries.adapter as StatementPastEntriesAdapter
            val size1 = adapter.values.size
            val size2 = viewModel.pastEntries.value?.dateList?.size ?: 0
            canLoad = size1 == size2 && viewModel.loadingDownPage.value == false &&
                viewModel.showLoading.value == false && viewModel.pastEntries.value?.isInitial == false
        }
        return canLoad
    }
}