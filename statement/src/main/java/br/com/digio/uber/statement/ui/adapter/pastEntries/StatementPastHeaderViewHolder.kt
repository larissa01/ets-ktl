package br.com.digio.uber.statement.ui.adapter.pastEntries

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.statement.BR
import br.com.digio.uber.statement.R
import br.com.digio.uber.statement.ui.uiModel.LastStatementUIItem
import br.com.digio.uber.statement.ui.uiModel.PastEntriesData
import br.com.digio.uber.statement.ui.uiModel.StatementEntriesListener

class StatementPastHeaderViewHolder(
    private val viewDataBinding: ViewDataBinding,
    private val listener: StatementEntriesListener.Past
) : AbstractViewHolder<PastEntriesData>(viewDataBinding.root) {

    private lateinit var itemPastEntryAdapter: StatementPastItemAdapter

    override fun bind(item: PastEntriesData) {
        itemPastEntryAdapter = StatementPastItemAdapter(item.pastEntries.toMutableList(), listener)
        val rv = itemView.findViewById<RecyclerView>(R.id.recycler_lastEntries_cardRecycler)
        rv.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
        rv.setHasFixedSize(true)
        rv.adapter = itemPastEntryAdapter
        viewDataBinding.setVariable(BR.statementPastItem, item)
        viewDataBinding.setVariable(BR.statementPastEntriesViewHolder, this)
        viewDataBinding.executePendingBindings()
    }
}

class LastStatementViewHolder(
    private val viewDataBinding: ViewDataBinding
) : AbstractViewHolder<LastStatementUIItem>(viewDataBinding.root) {

    override fun bind(item: LastStatementUIItem) {
        viewDataBinding.setVariable(BR.itemDescription, item.itemDescription)
        viewDataBinding.executePendingBindings()
    }
}