package br.com.digio.uber.statement.ui.adapter.futureEntries

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.statement.BR
import br.com.digio.uber.statement.R
import br.com.digio.uber.statement.ui.uiModel.FutureEntriesData
import br.com.digio.uber.statement.ui.uiModel.StatementEntriesListener

class StatementFutureHeaderViewHolder(
    private val viewDataBinding: ViewDataBinding,
    private val listener: StatementEntriesListener.Future
) : AbstractViewHolder<FutureEntriesData>(viewDataBinding.root) {

    private lateinit var itemFutureEntryAdapter: StatementFutureItemAdapter

    override fun bind(item: FutureEntriesData) {
        itemFutureEntryAdapter = StatementFutureItemAdapter(item.futureEntries.toMutableList(), listener)
        val rv = itemView.findViewById<RecyclerView>(R.id.recycler_futureEntries_cardRecycler)
        rv.layoutManager = LinearLayoutManager(itemView.context, LinearLayoutManager.VERTICAL, false)
        rv.setHasFixedSize(true)
        rv.adapter = itemFutureEntryAdapter
        viewDataBinding.setVariable(BR.statementFutureItem, item)
        viewDataBinding.setVariable(BR.statementFutureEntriesViewHolder, this)
        viewDataBinding.executePendingBindings()
    }
}