package br.com.digio.uber.statement.ui.adapter.pastEntries

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.statement.R
import br.com.digio.uber.statement.ui.uiModel.LastStatementUIItem
import br.com.digio.uber.statement.ui.uiModel.StatementEntriesListener
import br.com.digio.uber.statement.ui.uiModel.StatementPastUiModel
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

typealias OnClickStatementPastEntryItem = (item: StatementPastUiModel) -> Unit

class StatementPastEntriesAdapter(
    val values: MutableList<StatementPastUiModel> = mutableListOf(),
    private val entryListener: StatementEntriesListener.Past,
    private val listener: OnClickStatementPastEntryItem
) : BaseRecyclerViewAdapter<StatementPastUiModel, StatementPastEntriesAdapter.ViewTypesDataBindingFactoryImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl = ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): StatementPastUiModel = values[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder<StatementPastUiModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<StatementPastUiModel>
    }

    override fun onBindViewHolder(holder: AbstractViewHolder<StatementPastUiModel>, position: Int) =
        values[holder.adapterPosition].let { holder.bind(it) }

    override fun getItemCount(): Int = values.size

    override fun replaceItems(list: List<Any>) = addValues(list.safeHeritage())

    private fun addValues(values: List<StatementPastUiModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }

    inner class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<StatementPastUiModel> {
        override fun type(model: StatementPastUiModel): Int =
            if (model is LastStatementUIItem) {
                R.layout.view_holder_last_statement_item
            } else {
                R.layout.view_holder_past_entry
            }

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<StatementPastUiModel>
        ): AbstractViewHolder<*> =
            if (type == R.layout.view_holder_last_statement_item) {
                LastStatementViewHolder(view)
            } else {
                StatementPastHeaderViewHolder(view, entryListener)
            }
    }
}