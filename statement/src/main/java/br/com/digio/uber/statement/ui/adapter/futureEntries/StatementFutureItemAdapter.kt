package br.com.digio.uber.statement.ui.adapter.futureEntries

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.statement.R
import br.com.digio.uber.statement.ui.uiModel.FutureEntry
import br.com.digio.uber.statement.ui.uiModel.StatementEntriesListener
import kotlinx.android.synthetic.main.view_holder_future_statement_item.view.*

class StatementFutureItemAdapter(
    private val values: MutableList<FutureEntry> = mutableListOf(),
    private val listener: StatementEntriesListener.Future
) : RecyclerView.Adapter<StatementFutureItemAdapter.StatementFutureEntriesItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatementFutureEntriesItemViewHolder =
        StatementFutureEntriesItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.view_holder_future_statement_item,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: StatementFutureEntriesItemViewHolder, position: Int) =
        holder.bind(values[position])

    override fun getItemCount(): Int = values.size

    inner class StatementFutureEntriesItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: FutureEntry) {
            itemView.setOnClickListener { listener.onClickFutureEntry(item) }
            itemView.imageView_futureEntries_icon.setImageDrawable(
                ContextCompat.getDrawable(itemView.context, item.categoryDrawableId)
            )
            itemView.textView_futureEntries_name.text = item.entryName
            itemView.textView_futureEntries_description.text = item.description
            itemView.textView_futureEntries_amount.text = item.amount
            if (!item.hasAttachment) itemView.txt_receipt.visibility = View.GONE
        }
    }
}