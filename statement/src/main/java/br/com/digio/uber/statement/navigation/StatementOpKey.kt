package br.com.digio.uber.statement.navigation

import br.com.digio.uber.common.kenum.StatusBarColor

enum class StatementOpKey(val theme: StatusBarColor) {
    MAIN(StatusBarColor.BLACK)
}