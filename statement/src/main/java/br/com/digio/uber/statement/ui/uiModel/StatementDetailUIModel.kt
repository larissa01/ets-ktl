package br.com.digio.uber.statement.ui.uiModel

import br.com.digio.uber.common.kenum.RefundType

data class StatementDetailUIModel(
    val counterpartName: String?,
    val counterpartDocument: String?,
    val counterpartBank: String?,
    val counterpartBankBranch: String?,
    val counterpartBankAccount: String?,
    val action: RefundType,
    val refundObservation: String?
)