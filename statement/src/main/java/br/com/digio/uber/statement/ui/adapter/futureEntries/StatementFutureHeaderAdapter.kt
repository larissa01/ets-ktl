package br.com.digio.uber.statement.ui.adapter.futureEntries

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.statement.R
import br.com.digio.uber.statement.ui.uiModel.StatementEntriesListener
import br.com.digio.uber.statement.ui.uiModel.StatementFutureUiModel
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

typealias OnClickStatementFutureEntryItem = (item: StatementFutureUiModel) -> Unit

class StatementFutureEntriesAdapter constructor(
    private val values: MutableList<StatementFutureUiModel> = mutableListOf(),
    private val entryListener: StatementEntriesListener.Future,
    private val listener: OnClickStatementFutureEntryItem
) : BaseRecyclerViewAdapter<StatementFutureUiModel,
    StatementFutureEntriesAdapter.ViewTypesDataBindingFactoryFutureImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryFutureImpl = ViewTypesDataBindingFactoryFutureImpl()

    override fun getItemType(position: Int): StatementFutureUiModel = values[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder<StatementFutureUiModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<StatementFutureUiModel>
    }

    override fun onBindViewHolder(holder: AbstractViewHolder<StatementFutureUiModel>, position: Int) =
        values[holder.adapterPosition].let { holder.bind(it) }

    override fun getItemCount(): Int = values.size

    override fun replaceItems(list: List<Any>) = addValues(list.safeHeritage())

    private fun addValues(values: List<StatementFutureUiModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }

    inner class ViewTypesDataBindingFactoryFutureImpl : ViewTypesDataBindingFactory<StatementFutureUiModel> {
        override fun type(model: StatementFutureUiModel): Int =
            R.layout.view_holder_future_entry

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<StatementFutureUiModel>
        ): AbstractViewHolder<*> =
            StatementFutureHeaderViewHolder(view, entryListener)
    }
}