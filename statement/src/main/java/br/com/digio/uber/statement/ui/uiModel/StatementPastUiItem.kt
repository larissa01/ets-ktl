package br.com.digio.uber.statement.ui.uiModel

import android.os.Parcelable
import androidx.annotation.DrawableRes
import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import kotlinx.android.parcel.Parcelize
import java.util.Calendar

@Parcelize
open class StatementPastUiModel : AdapterViewModel<StatementPastUiModel>, Parcelable {
    override fun type(typesFactory: ViewTypesDataBindingFactory<StatementPastUiModel>) = typesFactory.type(
        model = this
    )
}

@Parcelize
data class LastStatementUIItem(
    val itemDescription: String
) : StatementPastUiModel()

@Parcelize
data class StatementPastUiItem(
    var isInitial: Boolean? = null,
    var dateList: MutableList<StatementPastUiModel>
) : StatementPastUiModel()

@Parcelize
data class PastEntriesData(
    val dateCalendar: String,
    val date: String,
    val pastEntries: List<PastEntry>
) : StatementPastUiModel()

@Parcelize
data class PastEntry(
    val id: String?,
    val amount: String,
    val category: EntryCategory,
    val date: Calendar,
    val dateText: String,
    val description: String?,
    val entryName: String,
    val hasAttachment: Boolean,
    val hasDetails: Boolean,
    val type: String,
    @DrawableRes val categoryDrawableId: Int
) : Parcelable