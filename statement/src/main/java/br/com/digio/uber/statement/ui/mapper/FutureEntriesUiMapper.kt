@file:Suppress(
    "LongMethod",
    "ComplexMethod"
)

package br.com.digio.uber.statement.ui.mapper

import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.model.statement.FutureDataResponse
import br.com.digio.uber.model.statement.FutureEntriesResponse
import br.com.digio.uber.model.statement.FutureEntryResponse
import br.com.digio.uber.statement.R
import br.com.digio.uber.statement.ui.uiModel.EntryCategory
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.BANKSLIP_INCOME
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.BANKSLIP_INCOME_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.BANKSLIP_INCOME_OPS
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.BANKSLIP_INCOME_OPS_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.BANKSLIP_ISSUE_TAX
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.BANKSLIP_ISSUE_TAX_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.BILL_PAYMENT
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.BILL_PAYMENT_REFUND
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.BILL_PAYMENT_TAC_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.BILL_PAYMENT_TAX
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.DEBIT_PURCHASE
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.DEBIT_PURCHASE_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.DEBIT_PURCHASE_TAX
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.DEBIT_PURCHASE_TAX_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.DIGIO_CASH
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.DIGIO_CASH_CANCELLATION
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.DIGIO_CASH_TAX
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.DIGIO_CASH_TAX_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.EGIFT_PURCHASE
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.EGIFT_PURCHASE_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.EGIFT_PURCHASE_TAX
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.EGIFT_PURCHASE_TAX_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.INVOICE_PAYMENT
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.INVOICE_PAYMENT_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.INVOICE_TAX
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.INVOICE_TAX_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.MOBILE_RECHARGE
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.MOBILE_RECHARGE_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.MOBILE_RECHARGE_TAX
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.MOBILE_RECHARGE_TAX_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.OTHERS
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.P2P_CREDIT
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.P2P_DEBIT
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.P2P_OPS_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.P2P_REFUND
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.P2P_TAX
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.P2P_TAX_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.PIX_INCOME
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.PIX_INCOME_QRCODE
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.PIX_INCOME_REFUND
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.PIX_PAYMENT_REFUND
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.SCHEDULING
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.TED_BRADESCO_OPS
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.TED_BRADESCO_OPS_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.TED_RECEIVED
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.TED_RECEIVED_STR008
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.TED_REFUND
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.TED_SENT
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.TED_TAX
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.TED_TAX_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.VISA_TRANSFER_CREDIT
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.VISA_TRANSFER_CREDIT_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.WITHDRAW
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.WITHDRAW_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.WITHDRAW_TAX
import br.com.digio.uber.statement.ui.uiModel.EntryCategory.WITHDRAW_TAX_CHARGEBACK
import br.com.digio.uber.statement.ui.uiModel.FutureEntriesData
import br.com.digio.uber.statement.ui.uiModel.FutureEntry
import br.com.digio.uber.statement.ui.uiModel.StatementFutureUiItem
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.util.toCalendar
import br.com.digio.uber.util.toNegativeMoney
import java.text.DateFormatSymbols
import java.util.Calendar
import java.util.Locale
import kotlin.collections.ArrayList
import kotlin.collections.List
import kotlin.collections.forEach

class FutureEntriesUiMapper constructor(
    private val resourceManager: ResourceManager
) : AbstractMapper<FutureEntriesResponse, StatementFutureUiItem> {
    override fun map(param: FutureEntriesResponse): StatementFutureUiItem = StatementFutureUiItem(
        futureDataList = convertDataToDateList(param.futureData)
    )

    private fun convertDataToDateList(data: List<FutureDataResponse>): List<FutureEntriesData> {
        val lista = ArrayList<FutureEntriesData>()
        data.forEach {
            lista.add(
                FutureEntriesData(
                    dateCalendar = it.date.toCalendar(),
                    date = mapDate(it.date.toCalendar()),
                    futureEntries = convertEntryToEntryUiModel(it.futureEntriesResponse)
                )
            )
        }
        return lista
    }

    private fun convertEntryToEntryUiModel(entries: List<FutureEntryResponse>): List<FutureEntry> {
        val lista = ArrayList<FutureEntry>()
        entries.forEach {
            lista.add(
                FutureEntry(
                    id = it.id,
                    amount = it.amount.toNegativeMoney(),
                    category = EntryCategory.parse(it.category),
                    date = it.date.toCalendar(),
                    dateText = mapDateAndHour(it.date.toCalendar()),
                    description = it.description,
                    entryName = it.entryName,
                    hasAttachment = it.hasAttachment,
                    hasDetails = it.hasDetails,
                    categoryDrawableId = getDrawableResId(EntryCategory.parse(it.category))
                )
            )
        }
        return lista
    }

    private fun getDrawableResId(item: EntryCategory): Int {
        return when (item) {
            //region API categories
            BANKSLIP_INCOME,
            BANKSLIP_INCOME_CHARGEBACK,
            BANKSLIP_INCOME_OPS,
            BANKSLIP_INCOME_OPS_CHARGEBACK,
            BANKSLIP_ISSUE_TAX,
            BANKSLIP_ISSUE_TAX_CHARGEBACK -> R.drawable.ic_barcode_svg

            BILL_PAYMENT,
            BILL_PAYMENT_REFUND,
            BILL_PAYMENT_TAC_CHARGEBACK,
            BILL_PAYMENT_TAX -> R.drawable.ic_money_svg

            DEBIT_PURCHASE,
            DEBIT_PURCHASE_CHARGEBACK,
            DEBIT_PURCHASE_TAX,
            DEBIT_PURCHASE_TAX_CHARGEBACK -> R.drawable.ic_card

            DIGIO_CASH,
            DIGIO_CASH_CANCELLATION,
            DIGIO_CASH_TAX,
            DIGIO_CASH_TAX_CHARGEBACK -> R.drawable.ic_money_svg

            EGIFT_PURCHASE,
            EGIFT_PURCHASE_CHARGEBACK,
            EGIFT_PURCHASE_TAX,
            EGIFT_PURCHASE_TAX_CHARGEBACK -> R.drawable.ic_gift

            INVOICE_PAYMENT,
            INVOICE_PAYMENT_CHARGEBACK,
            INVOICE_TAX,
            INVOICE_TAX_CHARGEBACK -> R.drawable.ic_money_svg

            MOBILE_RECHARGE,
            MOBILE_RECHARGE_CHARGEBACK,
            MOBILE_RECHARGE_TAX,
            MOBILE_RECHARGE_TAX_CHARGEBACK -> R.drawable.ic_recarga

            P2P_DEBIT,
            P2P_TAX,
            P2P_REFUND,
            P2P_TAX_CHARGEBACK,
            P2P_CREDIT,
            P2P_OPS_CHARGEBACK -> R.drawable.ic_transfer

            PIX_INCOME,
            PIX_INCOME_REFUND,
            PIX_PAYMENT_REFUND,
            PIX_INCOME_QRCODE -> R.drawable.ic_pix

            TED_BRADESCO_OPS,
            TED_BRADESCO_OPS_CHARGEBACK,
            TED_REFUND,
            TED_RECEIVED,
            TED_RECEIVED_STR008,
            TED_SENT,
            TED_TAX,
            TED_TAX_CHARGEBACK -> R.drawable.ic_transfer

            VISA_TRANSFER_CREDIT,
            VISA_TRANSFER_CREDIT_CHARGEBACK -> R.drawable.ic_transfer

            WITHDRAW,
            WITHDRAW_CHARGEBACK,
            WITHDRAW_TAX,
            WITHDRAW_TAX_CHARGEBACK -> R.drawable.ic_money_svg
            //endregion

            //region Internal categories
            OTHERS -> R.drawable.ic_money_svg
            SCHEDULING -> R.drawable.ic_calendar_schedule
            //endregion
        }
    }

    private fun mapDate(calendar: Calendar): String {
        return "${DateFormatSymbols().shortWeekdays[calendar.get(Calendar.DAY_OF_WEEK)].capitalize(Locale.ROOT)}," +
            " ${calendar.get(Calendar.DAY_OF_MONTH)} de" +
            " ${DateFormatSymbols().months[calendar.get(Calendar.MONTH)].capitalize(Locale.ROOT)} de" +
            " ${calendar.get(Calendar.YEAR)}"
    }

    private fun mapDateAndHour(calendar: Calendar): String {
        return " ${calendar.get(Calendar.DAY_OF_MONTH)} " +
            resourceManager.getArray(R.array.all_months_short)[calendar.get(Calendar.MONTH)] +
            " ${calendar.get(Calendar.YEAR)}"
    }
}