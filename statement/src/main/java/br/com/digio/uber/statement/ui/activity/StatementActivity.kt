package br.com.digio.uber.statement.ui.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.balancetoolbar.balance.di.balanceModule
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.helper.ViewIds
import br.com.digio.uber.common.navigation.GenericNavigation
import br.com.digio.uber.statement.R
import br.com.digio.uber.statement.di.statementViewModelModule
import br.com.digio.uber.statement.navigation.StatementNavigation
import com.google.android.material.appbar.CollapsingToolbarLayout
import org.koin.core.module.Module

class StatementActivity : BaseViewModelActivity<ViewDataBinding, BaseViewModel>() {

    private val statementNavigation = StatementNavigation(this)
    override fun getLayoutId(): Int = LayoutIds.navigationLayoutWithCollapsingToolbarScrollActivity
    override val bindingVariable: Int? = null
    override val viewModel: BaseViewModel? = null
    override fun getModule(): List<Module> = listOf(statementViewModelModule, balanceModule)
    override fun getNavigation(): GenericNavigation = statementNavigation
    override fun getToolbar(): Toolbar? = findViewById<Toolbar>(ViewIds.toolbarId).apply {
        this@StatementActivity.findViewById<CollapsingToolbarLayout>(ViewIds.collapsingToolbarId)
            .title = getString(R.string.statement)
        this.navigationIcon = ContextCompat.getDrawable(context, br.com.digio.uber.common.R.drawable.ic_back_arrow)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        statementNavigation.init(intent.extras)
    }
}