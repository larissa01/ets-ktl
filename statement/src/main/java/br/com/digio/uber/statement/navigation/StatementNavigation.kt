package br.com.digio.uber.statement.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName
import br.com.digio.uber.statement.ui.fragment.StatementMainFragment

class StatementNavigation(
    activity: AppCompatActivity
) : GenericNavigationWithFlow(activity) {

    fun init(bundle: Bundle?) {
        goTo(GoTo(StatementOpKey.MAIN.name, bundle))
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            StatementOpKey.MAIN.name to showMain()
        )

    private fun showMain(): GenericNavigationWIthFlowGetFragmentByName = {
        StatementMainFragment.newInstance(this, it)
    }
}