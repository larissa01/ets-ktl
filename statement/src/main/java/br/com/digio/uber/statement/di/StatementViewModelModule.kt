package br.com.digio.uber.statement.di

import br.com.digio.uber.statement.ui.viewModel.StatementMainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val statementViewModelModule = module {
    viewModel { StatementMainViewModel(get(), get(), get(), get(), get(), get(), get()) }
}