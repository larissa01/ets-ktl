package br.com.digio.uber.statement.ui.adapter.pastEntries

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.statement.R
import br.com.digio.uber.statement.ui.uiModel.PastEntry
import br.com.digio.uber.statement.ui.uiModel.StatementEntriesListener
import br.com.digio.uber.util.EntryType
import kotlinx.android.synthetic.main.view_holder_past_statement_item.view.*

class StatementPastItemAdapter(
    private val values: MutableList<PastEntry> = mutableListOf(),
    private val listener: StatementEntriesListener.Past
) : RecyclerView.Adapter<StatementPastItemAdapter.StatementPastEntriesItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatementPastEntriesItemViewHolder =
        StatementPastEntriesItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.view_holder_past_statement_item,
                parent,
                false
            ),
            parent.context
        )

    override fun onBindViewHolder(holder: StatementPastEntriesItemViewHolder, position: Int) =
        holder.bind(values[position])

    override fun getItemCount(): Int = values.size

    inner class StatementPastEntriesItemViewHolder(
        itemView: View,
        private val context: Context
    ) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: PastEntry) {
            itemView.setOnClickListener { listener.onClickPastEntry(item) }
            itemView.imageView_lastEntries_icon.setImageDrawable(
                ContextCompat.getDrawable(itemView.context, item.categoryDrawableId)
            )
            itemView.textView_lastEntries_name.text = item.entryName
            itemView.textView_lastEntries_description.text = item.description
            itemView.textView_lastEntries_amount.text = item.amount
            itemView.textView_lastEntries_amount.setTextColor(
                ContextCompat.getColor(
                    context,
                    if (item.type == EntryType.CREDIT.name) {
                        br.com.digio.uber.common.R.color.green_05A357
                    } else {
                        br.com.digio.uber.common.R.color.black
                    }
                )
            )
            if (!item.hasAttachment) itemView.imageView_lastEntries_attach.visibility = View.GONE
        }
    }
}