package br.com.digio.uber.statement.ui.uiModel

interface StatementEntriesListener {
    interface Future {
        fun onClickFutureEntry(futureEntry: FutureEntry)
    }

    interface Past {
        fun onClickPastEntry(pastEntry: PastEntry)
    }
}