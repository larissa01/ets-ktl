@file:Suppress("LongParameterList")

package br.com.digio.uber.statement.ui.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.makeChooseMessageObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.usecase.statement.DeleteScheduledEntryUseCase
import br.com.digio.uber.domain.usecase.statement.GetDetailStatementUseCase
import br.com.digio.uber.domain.usecase.statement.GetFutureEntriesUseCase
import br.com.digio.uber.domain.usecase.statement.GetPastEntriesUseCase
import br.com.digio.uber.domain.usecase.transfer.GetAccountTedUseCase
import br.com.digio.uber.model.statement.PastEntriesRequest
import br.com.digio.uber.model.transfer.AccountDomainResponse
import br.com.digio.uber.statement.R
import br.com.digio.uber.statement.ui.mapper.FutureEntriesUiMapper
import br.com.digio.uber.statement.ui.mapper.PastEntriesUiMapper
import br.com.digio.uber.statement.ui.mapper.StatementDetailMapper
import br.com.digio.uber.statement.ui.uiModel.LastStatementUIItem
import br.com.digio.uber.statement.ui.uiModel.PastEntriesData
import br.com.digio.uber.statement.ui.uiModel.StatementDetailUIModel
import br.com.digio.uber.statement.ui.uiModel.StatementFutureUiItem
import br.com.digio.uber.statement.ui.uiModel.StatementPastUiItem
import br.com.digio.uber.util.DateFormatter
import br.com.digio.uber.util.formatAsAccountNumber
import br.com.digio.uber.util.formatAsAgencyNumber
import java.util.Date

class StatementMainViewModel constructor(
    private val getStatementEntriesUseCase: GetPastEntriesUseCase,
    private val getFutureEntriesUseCase: GetFutureEntriesUseCase,
    private val deleteScheduleUseCase: DeleteScheduledEntryUseCase,
    private val getDetailStatementUseCase: GetDetailStatementUseCase,
    getAccountUseCase: GetAccountTedUseCase,
    private val resourceManager: ResourceManager,
    val analytics: Analytics
) : BaseViewModel() {

    var mutableAccount: LiveData<AccountDomainResponse> = getAccountUseCase(Unit).exec(showLoadingFlag = false)
    val pastEntries = MutableLiveData<StatementPastUiItem>()
    val futureEntries = MutableLiveData<StatementFutureUiItem>()
    val onDeleteScheduleSuccess = MutableLiveData<Boolean>()
    private var lastDate: String? = null
    val loadingDownPage: MutableLiveData<Boolean> = MutableLiveData(false)
    var detailStatementResponse = MutableLiveData<StatementDetailUIModel>()

    fun loadStatementLists() {
        showLoading()
        getFutureEntriesUseCase(Unit).singleExec(
            showLoadingFlag = false,
            onSuccessBaseViewModel = {
                futureEntries.value = FutureEntriesUiMapper(resourceManager).map(it)
            }
        )
        loadPastStatements()
    }

    private fun loadPastStatements() {
        getStatementEntriesUseCase(getDefaultPastEntriesRequest()).singleExec(
            showLoadingFlag = false,
            onSuccessBaseViewModel = {
                if (pastEntries.value == null) {
                    pastEntries.value = PastEntriesUiMapper(resourceManager).map(it)
                    if (it.isInitial == true) {
                        val entries = pastEntries.value
                        entries?.dateList?.add(LastStatementUIItem(getLastItemDesc()))
                        pastEntries.value = entries
                    }
                } else {
                    val oldEntries = pastEntries.value
                    val newEntries = PastEntriesUiMapper(resourceManager).map(it)
                    oldEntries?.isInitial = newEntries.isInitial
                    oldEntries?.dateList?.addAll(newEntries.dateList)
                    if (it.isInitial == true) {
                        oldEntries?.dateList?.add(LastStatementUIItem(getLastItemDesc()))
                    }

                    pastEntries.value = oldEntries
                }
                hideLoading()
                loadingDownPage.value = false
            }
        )
    }

    private fun getLastItemDesc(): String {
        var description = ""
        mutableAccount.value?.apply {
            description = String.format(
                resourceManager.getString(R.string.last_item_bank_info),
                branch.formatAsAgencyNumber(),
                accountNumber.formatAsAccountNumber()
            )
        }
        return description
    }

    fun loadMore(entriesData: PastEntriesData) {
        lastDate = entriesData.dateCalendar
        loadPastStatements()
    }

    private fun getDefaultPastEntriesRequest(): PastEntriesRequest {
        if (lastDate == null) {
            lastDate = DateFormatter.dateUSFormatter.format(Date())
        }

        return PastEntriesRequest(lastDate = lastDate)
    }

    fun showCancelDialog(id: String?) {
        message.value = makeChooseMessageObject {
            icon = R.drawable.ic_trash_can
            title = R.string.cancel_dialog_title
            onPositiveButtonClick = { id?.let { cancelSchedule(it) } }
            positiveOptionTextInt = R.string.cancel_confirm_button
            negativeOptionTextInt = R.string.cancel_back_button
            hideMessage = true
        }
    }

    private fun cancelSchedule(id: String?) {
        id?.let {
            deleteScheduleUseCase(id).singleExec(
                onSuccessBaseViewModel = {
                    onDeleteScheduleSuccess.value = true
                }
            )
        }
    }

    fun getStatementDetail(id: String) {
        getDetailStatementUseCase(id).singleExec(
            onSuccessBaseViewModel = {
                detailStatementResponse.value = StatementDetailMapper().map(it)
            }
        )
    }
}