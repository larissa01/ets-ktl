package br.com.digio.uber.statement.ui.uiModel

import android.os.Parcelable
import androidx.annotation.DrawableRes
import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import kotlinx.android.parcel.Parcelize
import java.util.Calendar

sealed class StatementFutureUiModel : AdapterViewModel<StatementFutureUiModel> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<StatementFutureUiModel>) = typesFactory.type(
        model = this
    )
}

@Parcelize
data class StatementFutureUiItem(
    val futureDataList: List<FutureEntriesData>
) : Parcelable, StatementFutureUiModel()

@Parcelize
data class FutureEntriesData(
    val dateCalendar: Calendar,
    val date: String,
    val futureEntries: List<FutureEntry>
) : Parcelable, StatementFutureUiModel()

@Parcelize
data class FutureEntry(
    val id: String?,
    val amount: String,
    val category: EntryCategory,
    val date: Calendar,
    val dateText: String,
    val description: String?,
    val entryName: String,
    val hasAttachment: Boolean,
    val hasDetails: Boolean,
    @DrawableRes val categoryDrawableId: Int
) : Parcelable