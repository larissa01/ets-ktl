package br.com.digio.uber.statement.ui.mapper

import br.com.digio.uber.common.kenum.RefundType
import br.com.digio.uber.model.statement.StatementDetailResponse
import br.com.digio.uber.statement.ui.uiModel.StatementDetailUIModel
import br.com.digio.uber.util.mapper.AbstractMapper

class StatementDetailMapper : AbstractMapper<StatementDetailResponse, StatementDetailUIModel> {
    override fun map(param: StatementDetailResponse): StatementDetailUIModel = with(param) {
        StatementDetailUIModel(
            counterpartName,
            counterpartDocument,
            counterpartBank,
            counterpartBankBranch,
            counterpartBankAccount,
            RefundType.parse(action),
            refundObservation
        )
    }
}