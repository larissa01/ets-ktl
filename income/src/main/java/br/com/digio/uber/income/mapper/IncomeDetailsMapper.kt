package br.com.digio.uber.income.mapper

import br.com.digio.uber.income.uiModel.DepositUIModel
import br.com.digio.uber.income.uiModel.IncomeDetailsUIModel
import br.com.digio.uber.income.uiModel.InternDepositUIModel
import br.com.digio.uber.model.income.InternDeposit
import br.com.digio.uber.model.income.ListIncomesResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class IncomeDetailsMapper : AbstractMapper<ListIncomesResponse, IncomeDetailsUIModel> {
    override fun map(param: ListIncomesResponse): IncomeDetailsUIModel = with(param) {
        IncomeDetailsUIModel(
            deposits = deposits.map { deposit ->
                DepositUIModel(
                    deposit.yearMonth,
                    internalDeposits = deposit.internalDeposits.map {
                        getInternalDeposit(it)
                    }
                )
            },
            grossIncome = getParseValue(grossIncome),
            incomeIndex = incomeIndex,
            iofValue = getParseValue(iofValue),
            irValue = getParseValue(irValue),
            netIncome = getParseValue(netIncome),
            yearMonth = yearMonth,
            grossIncomeBD = grossIncome,
            iofValueBD = iofValue,
            irValueBD = irValue,
            netIncomeBD = netIncome
        )
    }

    private fun getInternalDeposit(interDeposit: InternDeposit): InternDepositUIModel {
        return InternDepositUIModel(
            interDeposit.depositId,
            interDeposit.startDate,
            interDeposit.depositAmount,
            interDeposit.currentAmount
        )
    }
}