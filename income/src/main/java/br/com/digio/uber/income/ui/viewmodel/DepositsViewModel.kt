package br.com.digio.uber.income.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.usecase.income.GetDepositDetailsUseCase
import br.com.digio.uber.domain.usecase.income.GetMonthsUseCase
import br.com.digio.uber.income.R
import br.com.digio.uber.income.mapper.AvailableMonthsMapper
import br.com.digio.uber.income.mapper.DepositDetailMapper
import br.com.digio.uber.income.ui.fragment.DepositsFragment
import br.com.digio.uber.income.uiModel.IncomeDetailsUIModel
import br.com.digio.uber.income.uiModel.InternDepositUIModel
import br.com.digio.uber.income.uiModel.MonthUIModel
import br.com.digio.uber.model.income.DetailDepositDomainResponse
import java.text.DateFormatSymbols
import java.util.Locale

class DepositsViewModel(
    private val getMonthsUseCase: GetMonthsUseCase,
    private val getDepositDetailsUseCase: GetDepositDetailsUseCase,
    private val resourceManager: ResourceManager
) : BaseViewModel() {
    private val currentMonthUiModel: MutableLiveData<MonthUIModel> = MutableLiveData<MonthUIModel>()
    val availableMonths: MutableLiveData<List<String>> = MutableLiveData<List<String>>()
    val deposit: MutableLiveData<DetailDepositDomainResponse> = MutableLiveData<DetailDepositDomainResponse>()
    val depositStartDate: MutableLiveData<String> = MutableLiveData<String>()
    val depositEndDate: MutableLiveData<String> = MutableLiveData<String>()
    val currentAmount: MutableLiveData<String> = MutableLiveData<String>()
    val isDialogOpen: MutableLiveData<Boolean> = MutableLiveData(false)
    val currentYearMonth: MutableLiveData<String> = MutableLiveData<String>().apply {
        value = resourceManager.getString(R.string.income_select_semester)
    }
    val isActiveDeposits: MutableLiveData<Boolean> = MutableLiveData(true)
    val subtitle: MutableLiveData<String> = MutableLiveData()
    val showMonthsCombo: MutableLiveData<Boolean> = MutableLiveData(false)
    val incomeDetails: MutableLiveData<IncomeDetailsUIModel> = MutableLiveData()

    init {
        getAvailableMonths()
    }

    private fun getAvailableMonths() {
        getMonthsUseCase(Unit).singleExec(
            onSuccessBaseViewModel = { response ->
                val months = AvailableMonthsMapper().map(response)
                availableMonths.value = months.yearMonths
                val yearMonth = months.yearMonths[0]
                val parts = yearMonth.split("-")
                val array = DateFormatSymbols().months.map { it.capitalize(Locale.ROOT) }
                val fullValue = String.format(
                    resourceManager.getString(R.string.income_months_format),
                    array[parts[1].toInt() - 1],
                    parts[0]
                ) // "${array[parts[1].toInt() - 1]} - ${parts[0]}"
                val monthUiModel = MonthUIModel(yearMonth, fullValue)
                onMonthSelected(monthUiModel)
            }
        )
    }

    fun fetchData(depositType: DepositsFragment.DepositType, incomeDetails: IncomeDetailsUIModel) {
        subtitle.value =
            if (depositType == DepositsFragment.DepositType.ACTIVE) {
                resourceManager.getString(R.string.active_incomes)
            } else {
                resourceManager.getString(R.string.finished_incomes)
            }

        this.incomeDetails.value = incomeDetails
    }

    fun detailDeposit(iDeposit: InternDepositUIModel) {
        getDepositDetailsUseCase(iDeposit.depositId).singleExec(
            onSuccessBaseViewModel = { response ->
                deposit.value = DepositDetailMapper().map(response)
                deposit.value?.let {
                    depositStartDate.value = formatIncomeDate(it.depositStartDate)
                    depositEndDate.value = formatIncomeDate(it.depositEndDate)
                    currentAmount.value = it.currentAmount
                }
            }
        )
    }

    private fun formatIncomeDate(dateString: String): String {
        val dateParts = dateString.split("-")
        val months = DateFormatSymbols().shortMonths.map { it.toUpperCase(Locale.ROOT) }
        return String.format(
            resourceManager.getString(R.string.income_date_format),
            dateParts[2],
            months[dateParts[1].toInt() - 1],
            dateParts[0]
        )
    }

    fun showList() {
        isDialogOpen.value = true
    }

    fun closeList() {
        isDialogOpen.value = false
    }

    fun onMonthSelected(model: MonthUIModel) {
        isDialogOpen.value = false
        currentYearMonth.value = model.monthYearScreen
        currentMonthUiModel.value = model
    }
}