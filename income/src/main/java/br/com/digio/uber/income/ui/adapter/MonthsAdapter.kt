package br.com.digio.uber.income.ui.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.income.R
import br.com.digio.uber.income.databinding.IncomeMonthItemBinding
import br.com.digio.uber.income.mapper.toItemUIModel
import br.com.digio.uber.income.ui.adapter.viewholder.MonthViewHolder
import br.com.digio.uber.income.uiModel.MonthItem
import br.com.digio.uber.income.uiModel.MonthItemUIModel
import br.com.digio.uber.income.uiModel.MonthUIModel
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

typealias OnClickMonthItem = (item: MonthItemUIModel) -> Unit

class MonthsAdapter(
    private var values: MutableList<MonthItemUIModel> = mutableListOf(),
    private val listener: OnClickMonthItem
) :
    BaseRecyclerViewAdapter<MonthItemUIModel, MonthsAdapter.ViewTypesDataBindingFactoryImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl =
        ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): MonthItemUIModel =
        values[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder<MonthItemUIModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<MonthItemUIModel>
    }

    override fun onBindViewHolder(holder: AbstractViewHolder<MonthItemUIModel>, position: Int) =
        holder.bind(values[holder.adapterPosition])

    override fun getItemCount(): Int =
        values.size

    override fun replaceItems(list: List<Any>) {
        val items: List<MonthUIModel> = list.safeHeritage()
        val itemsUI: List<MonthItemUIModel> = items.toItemUIModel()
        addValues(itemsUI)
    }

    private fun addValues(values: List<MonthItemUIModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }

    class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<MonthItemUIModel> {
        override fun type(model: MonthItemUIModel): Int =
            when (model) {
                is MonthItem -> R.layout.income_month_item
            }

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<MonthItemUIModel>
        ): AbstractViewHolder<*> =
            when (type) {
                R.layout.income_month_item -> MonthViewHolder(
                    view.safeHeritage<IncomeMonthItemBinding>()!!,
                    listener
                )
                else -> throw IndexOutOfBoundsException("Invalid view type")
            }
    }
}