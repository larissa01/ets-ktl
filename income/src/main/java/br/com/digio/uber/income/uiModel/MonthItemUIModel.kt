package br.com.digio.uber.income.uiModel

import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory

sealed class MonthItemUIModel : AdapterViewModel<MonthItemUIModel> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<MonthItemUIModel>) = typesFactory.type(
        model = this
    )
}

private const val ZERO_VALUE = 0

data class MonthItem(
    val monthYear: String,
    val monthYearScreen: String,
    val position: Int = ZERO_VALUE
) : MonthItemUIModel()