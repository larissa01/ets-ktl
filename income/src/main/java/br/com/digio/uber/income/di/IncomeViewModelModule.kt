package br.com.digio.uber.income.di

import br.com.digio.uber.income.ui.viewmodel.DepositsViewModel
import br.com.digio.uber.income.ui.viewmodel.IncomeChoiceViewModel
import br.com.digio.uber.income.ui.viewmodel.IncomeViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val incomeViewModelModule = module {
    viewModel { IncomeViewModel(get(), get(), get(), androidContext(), get()) }
    viewModel { IncomeChoiceViewModel() }
    viewModel { DepositsViewModel(get(), get(), get()) }
}