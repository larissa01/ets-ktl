package br.com.digio.uber.income.ui.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.income.BR
import br.com.digio.uber.income.R
import br.com.digio.uber.income.databinding.FragmentIncomeBinding
import br.com.digio.uber.income.mapper.toUIModel
import br.com.digio.uber.income.ui.adapter.MonthsAdapter
import br.com.digio.uber.income.ui.viewmodel.IncomeViewModel
import br.com.digio.uber.income.uiModel.MonthUIModel
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DateFormatSymbols
import java.util.Locale

@Suppress("TooManyFunctions")
class IncomeFragment : BaseViewModelFragment<FragmentIncomeBinding, IncomeViewModel>() {

    override val bindingVariable: Int = BR.incomeViewModel
    override val getLayoutId: Int = R.layout.fragment_income

    override val viewModel: IncomeViewModel by viewModel()

    override fun tag(): String = IncomeFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true

    override fun getToolbar(): Toolbar? =
        activity?.findViewById(R.id.toolbar_income)

    private val monthAdapter: MonthsAdapter by lazy {
        MonthsAdapter(
            listener = {
                viewModel.onMonthSelected(it.toUIModel())
            }
        )
    }

    override fun onBackPressed() {
        activity?.finish()
    }

    override fun onNavigationClick(view: View) {
        activity?.finish()
    }

    override fun initialize() {
        super.initialize()
        setupObservers()
        setupListeners()
        bindRecyclerView()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getAvailableMonths()
    }

    private fun bindRecyclerView() {
        binding?.recyclerSpinner?.adapter = monthAdapter
    }

    private fun setupListeners() {
        binding?.btnDeposits?.setOnClickListener { goToDeposits() }
    }

    private fun setupObservers() {
        viewModel.availableMonths.observe(
            viewLifecycleOwner,
            {
                fetchMonths(it)
            }
        )
    }

    private fun fetchMonths(months: List<String>) {
        val listMonthUiModel = mutableListOf<MonthUIModel>()
        for (monthYear in months) {
            val parts = monthYear.split("-")
            val array = DateFormatSymbols().months.map { it.capitalize(Locale.ROOT) }
            val fullValue = "${array[parts[1].toInt() - 1]} - ${parts[0]}"
            listMonthUiModel.add(MonthUIModel(monthYear, fullValue))
        }
        monthAdapter.replaceItems(listMonthUiModel)
    }

    private fun goToDeposits() {
        viewModel.currentIncomeDetails.value?.let {
            navigateTo(
                IncomeFragmentDirections.actionIncomeFragmentToDepositsFragment(
                    it,
                    DepositsFragment.DepositType.ACTIVE
                )
            )
        }
    }
}