package br.com.digio.uber.income.ui.adapter.viewholder

import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.income.databinding.IncomeMonthItemBinding
import br.com.digio.uber.income.uiModel.MonthItem
import br.com.digio.uber.income.uiModel.MonthItemUIModel

class MonthViewHolder(
    private val viewDataBinding: IncomeMonthItemBinding,
    private val listener: ViewTypesListener<MonthItemUIModel>
) : AbstractViewHolder<MonthItem>(viewDataBinding.root) {

    override fun bind(item: MonthItem) {
        viewDataBinding.monthItem = item
        viewDataBinding.viewHolder = this
        viewDataBinding.executePendingBindings()
    }

    fun onClick(item: MonthItem) {
        listener(item)
    }
}