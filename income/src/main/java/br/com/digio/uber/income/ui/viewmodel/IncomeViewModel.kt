package br.com.digio.uber.income.ui.viewmodel

import android.content.Context
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags.INCOME_ACCOUNT_MONTH
import br.com.digio.uber.common.R
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.usecase.income.GetIncomeDetailsUseCase
import br.com.digio.uber.domain.usecase.income.GetMonthsUseCase
import br.com.digio.uber.income.mapper.IncomeDetailsMapper
import br.com.digio.uber.income.uiModel.IncomeDetailsUIModel
import br.com.digio.uber.income.uiModel.MonthUIModel
import java.math.BigDecimal
import java.text.DateFormatSymbols
import java.util.Calendar
import java.util.Locale

class IncomeViewModel(
    private val getMonthsUseCase: GetMonthsUseCase,
    private val getIncomeDetailsUseCase: GetIncomeDetailsUseCase,
    private val resourceManager: ResourceManager,
    private val context: Context,
    private val analytics: Analytics
) : BaseViewModel() {

    val availableMonths: MutableLiveData<List<String>> = MutableLiveData<List<String>>()
    val availableButton: MutableLiveData<Boolean> = MutableLiveData<Boolean>()

    private val currentMonthUiModel: MutableLiveData<MonthUIModel> = MutableLiveData<MonthUIModel>()
    val currentIncomeDetails: MutableLiveData<IncomeDetailsUIModel> =
        MutableLiveData<IncomeDetailsUIModel>()
    val currentYearMonth: MutableLiveData<String> = MutableLiveData<String>().apply {
        value = resourceManager.getString(br.com.digio.uber.income.R.string.income_select_semester)
    }

    val isDialogOpen: MutableLiveData<Boolean> = MutableLiveData(false)

    var colorGrossIncome: MutableLiveData<Int> =
        MutableLiveData(ContextCompat.getColor(context, R.color.grey_747474))
    var colorIncomeTax: MutableLiveData<Int> =
        MutableLiveData(ContextCompat.getColor(context, R.color.grey_747474))
    var colorIof: MutableLiveData<Int> =
        MutableLiveData(ContextCompat.getColor(context, R.color.grey_747474))
    var colorEarnings: MutableLiveData<Int> =
        MutableLiveData(ContextCompat.getColor(context, R.color.grey_747474))

    companion object {
        const val MONTH_CHOSE = "mes_escolhido"
    }

    fun getAvailableMonths() {
        getMonthsUseCase(Unit).singleExec(
            onSuccessBaseViewModel = { response ->
                availableMonths.value = response.yearMonths
                response.yearMonths.forEach { yearMonth ->
                    if (onlyIsToday(yearMonth)) {
                        val parts = yearMonth.split("-")
                        val array = DateFormatSymbols().months.map { it.capitalize(Locale.ROOT) }
                        val fullValue =
                            String.format(
                                resourceManager.getString(br.com.digio.uber.income.R.string.income_months_format),
                                array[parts[1].toInt() - 1],
                                parts[0]
                            )
                        val monthUiModel = MonthUIModel(yearMonth, fullValue)
                        onMonthSelected(monthUiModel)
                    }
                }
            }
        )
    }

    private fun onlyIsToday(yearMonth: String): Boolean {
        val parts = yearMonth.split("-")
        val calendar = Calendar.getInstance()
        val month = calendar.get(Calendar.MONTH) + 1
        val year = calendar.get(Calendar.YEAR)

        return parts[0].toInt() == year && parts[1].toInt() == month
    }

    private fun isToday(yearMonth: String): Boolean {
        availableButton.value = onlyIsToday(yearMonth)
        return availableButton.value ?: false
    }

    fun showList() {
        isDialogOpen.value = true
    }

    fun closeList() {
        isDialogOpen.value = false
    }

    fun onMonthSelected(model: MonthUIModel) {
        isDialogOpen.value = false
        currentYearMonth.value = model.monthYearScreen
        currentMonthUiModel.value = model

        val bundle = Bundle()
        bundle.putString(MONTH_CHOSE, model.monthYearScreen)
        analytics.pushSimpleEvent(INCOME_ACCOUNT_MONTH)

        isToday(model.monthYear)

        getIncomeDetailsUseCase(model.monthYear).singleExec(
            onSuccessBaseViewModel = {
                val incomeDetails = IncomeDetailsMapper().map(it)
                val positiveColor = ContextCompat.getColor(
                    context,
                    br.com.digio.uber.income.R.color.digital_account_deposits_positive
                )

                val negativeColor = ContextCompat.getColor(
                    context,
                    br.com.digio.uber.income.R.color.digital_account_deposits_negative
                )

                colorGrossIncome.value =
                    if (incomeDetails.grossIncomeBD >= BigDecimal.ZERO) {
                        positiveColor
                    } else {
                        negativeColor
                    }

                colorIncomeTax.value =
                    if (incomeDetails.irValueBD >= BigDecimal.ZERO) {
                        positiveColor
                    } else {
                        negativeColor
                    }

                colorIof.value =
                    if (incomeDetails.iofValueBD >= BigDecimal.ZERO) {
                        positiveColor
                    } else {
                        negativeColor
                    }

                colorEarnings.value =
                    if (incomeDetails.netIncomeBD >= BigDecimal.ZERO) {
                        positiveColor
                    } else {
                        negativeColor
                    }

                currentIncomeDetails.value = incomeDetails
            }
        )
    }
}