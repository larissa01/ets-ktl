package br.com.digio.uber.income.uiModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal

@Parcelize
data class InternDepositUIModel(
    val depositId: Int,
    val startDate: String,
    val depositAmount: BigDecimal,
    val currentAmount: BigDecimal
) : Parcelable