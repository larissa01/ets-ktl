package br.com.digio.uber.income.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.income.databinding.DepositsMonthItemBinding
import br.com.digio.uber.income.uiModel.DepositUIModel
import br.com.digio.uber.income.uiModel.InternDepositUIModel
import java.text.DateFormatSymbols
import java.util.Locale

typealias OnDepositClick = (item: InternDepositUIModel) -> Unit

class DepositsMonthsAdapter(
    private val deposits: List<DepositUIModel>,
    private val listener: OnDepositClick
) :
    RecyclerView.Adapter<DepositsMonthsAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = DepositsMonthItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(deposits, position)
    }

    override fun getItemCount() = deposits.size

    class MyViewHolder(
        private val binding: DepositsMonthItemBinding,
        private val listener: OnDepositClick
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(deposits: List<DepositUIModel>, position: Int) {
            binding.apply {
                val parts = deposits[position].yearMonth.split("-")
                val array = DateFormatSymbols().months.map { it.capitalize(Locale.ROOT) }
                txtDepositMonth.text = array[parts[1].toInt() - 1]

                recyclerInternDeposits.adapter = DepositsAdapter(
                    deposits[position].internalDeposits.sortedBy { it.startDate },
                    listener
                )
            }
        }
    }
}