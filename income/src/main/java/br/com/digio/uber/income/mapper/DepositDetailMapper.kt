package br.com.digio.uber.income.mapper

import br.com.digio.uber.model.income.DetailDepositDomainResponse
import br.com.digio.uber.model.income.DetailDepositResponse
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.util.toMoneyNumber
import java.math.BigDecimal

class DepositDetailMapper : AbstractMapper<DetailDepositResponse, DetailDepositDomainResponse> {
    override fun map(param: DetailDepositResponse): DetailDepositDomainResponse = with(param) {
        DetailDepositDomainResponse(
            closed = closed,
            currentAmount = getParseValue(currentAmount),
            currentAmountVl = currentAmount,
            depositAmount = getParseValue(depositAmount),
            depositAmountVl = depositAmount,
            depositEndDate = depositEndDate,
            depositStartDate = depositStartDate,
            depositId = depositId,
            incomeIndex = incomeIndex,
            iofValue = getParseValue(iofValue),
            iofValueVl = iofValue,
            irValue = getParseValue(irValue),
            irValueVl = irValue,
            grossIncome = getParseValue(grossIncome),
            grossIncomeVl = grossIncome,
            remunerationType = remunerationType,
            withdrawAmount = getParseValue(withdrawAmount),
            withdrawAmountVl = withdrawAmount,
            liquidIncome = getParseValue(liquidIncome ?: 0.toBigDecimal()),
            liquidIncomeVl = liquidIncome
        )
    }
}

fun getParseValue(value: BigDecimal): String {
    return when {
        value < BigDecimal.ZERO -> {
            "R$ -${value.abs().toMoneyNumber()}"
        }
        else -> {
            "R$ ${value.toMoneyNumber()}"
        }
    }
}