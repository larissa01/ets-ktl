package br.com.digio.uber.income.ui.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.choice.Choice
import br.com.digio.uber.common.choice.adapter.ChoicesAdapter
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.income.BR
import br.com.digio.uber.income.R
import br.com.digio.uber.income.databinding.FragmentIncomeChoiceBinding
import br.com.digio.uber.income.ui.viewmodel.IncomeChoiceViewModel
import br.com.digio.uber.income.uiModel.IncomeDetailsUIModel
import br.com.digio.uber.util.Const
import org.koin.android.viewmodel.ext.android.viewModel

class IncomeChoiceFragment : BaseViewModelFragment<FragmentIncomeChoiceBinding, IncomeChoiceViewModel>() {

    override val bindingVariable: Int = BR.incomeChoiceViewModel
    override val getLayoutId: Int = R.layout.fragment_income_choice
    override val viewModel: IncomeChoiceViewModel by viewModel()

    private val incomeDetails: IncomeDetailsUIModel by lazy {
        IncomeChoiceFragmentArgs.fromBundle(requireArguments()).incomeDetails
    }

    override fun tag(): String = IncomeChoiceFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun getToolbar(): Toolbar? =
        activity?.findViewById(R.id.toolbar_income)

    override val showHomeAsUp: Boolean = true

    override val showDisplayShowTitle: Boolean = true

    override fun onNavigationClick(view: View) {
        pop()
    }

    override fun initialize() {
        super.initialize()
        fetchChoices()
    }

    private fun fetchChoices() {
        val choices = arrayListOf(
            Choice(
                br.com.digio.uber.common.R.drawable.ic_chart_bar,
                R.string.active_incomes,
                R.string.active_incomes_desc,
                Const.Activities.TRANSFER_ACTIVITY,
                DepositsFragment.DepositType.ACTIVE.name
            ),
            Choice(
                br.com.digio.uber.common.R.drawable.ic_balance_uber_account,
                R.string.finished_incomes,
                R.string.finished_incomes_desc,
                Const.Activities.TRANSFER_ACTIVITY,
                DepositsFragment.DepositType.FINISHED.name
            )
        )
        binding?.rvChoices?.adapter = ChoicesAdapter(choices) { selectedChoice ->
            selectedChoice.fragmentToStart?.let {
                var depositType = DepositsFragment.DepositType.ACTIVE
                when (it) {
                    DepositsFragment.DepositType.ACTIVE.name ->
                        depositType = DepositsFragment.DepositType.ACTIVE

                    DepositsFragment.DepositType.FINISHED.name ->
                        depositType = DepositsFragment.DepositType.FINISHED
                }
                navigateTo(
                    IncomeChoiceFragmentDirections
                        .actionIncomeChoiceFragmentToDepositsFragment(incomeDetails, depositType)
                )
            }
        }
    }
}