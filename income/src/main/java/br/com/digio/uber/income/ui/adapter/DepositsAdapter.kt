package br.com.digio.uber.income.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.income.databinding.InternDepositMonthItemBinding
import br.com.digio.uber.income.uiModel.InternDepositUIModel
import br.com.digio.uber.util.dpToPixel
import br.com.digio.uber.util.getDayShortMonthYear
import br.com.digio.uber.util.toCalendar
import br.com.digio.uber.util.toMoney

class DepositsAdapter(
    private val deposits: List<InternDepositUIModel>,
    private val listener: OnDepositClick
) :
    RecyclerView.Adapter<DepositsAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemBinding =
            InternDepositMonthItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(itemBinding, listener, parent.context)
    }

    override fun getItemCount() = deposits.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(deposits[position], position == (deposits.size - 1))
    }

    class MyViewHolder(
        private val binding: InternDepositMonthItemBinding,
        private val listener: OnDepositClick,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {

        companion object {
            private const val margin16 = 16
            private const val margin46 = 46
            private const val marginZero = 0
        }

        fun bind(deposit: InternDepositUIModel, isLastItem: Boolean) {
            binding.apply {
                date = deposit.startDate.toCalendar().getDayShortMonthYear().toUpperCase()

                value = deposit.currentAmount.toMoney()

                val layoutParams = binding.root.layoutParams as ViewGroup.MarginLayoutParams
                val topMargin = margin16.toFloat().dpToPixel(context).toInt()
                val bottomMargin =
                    if (isLastItem) {
                        margin46.toFloat().dpToPixel(context).toInt()
                    } else marginZero
                layoutParams.setMargins(marginZero, topMargin, marginZero, bottomMargin)
                root.layoutParams = layoutParams

                viewSeparator.visibility =
                    if (isLastItem) View.GONE else View.VISIBLE

                layoutInternDeposit.setOnClickListener { listener(deposit) }
            }
        }
    }
}