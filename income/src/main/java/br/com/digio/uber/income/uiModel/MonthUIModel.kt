package br.com.digio.uber.income.uiModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MonthUIModel(
    val monthYear: String,
    val monthYearScreen: String
) : Parcelable