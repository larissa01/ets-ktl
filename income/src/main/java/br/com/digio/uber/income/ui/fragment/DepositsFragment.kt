package br.com.digio.uber.income.ui.fragment

import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.income.BR
import br.com.digio.uber.income.R
import br.com.digio.uber.income.databinding.FragmentDepositsBinding
import br.com.digio.uber.income.mapper.toUIModel
import br.com.digio.uber.income.ui.adapter.DepositsMonthsAdapter
import br.com.digio.uber.income.ui.adapter.MonthsAdapter
import br.com.digio.uber.income.ui.viewmodel.DepositsViewModel
import br.com.digio.uber.income.uiModel.IncomeDetailsUIModel
import br.com.digio.uber.income.uiModel.MonthUIModel
import br.com.digio.uber.util.dpToPixel
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.DateFormatSymbols

class DepositsFragment : BaseViewModelFragment<FragmentDepositsBinding, DepositsViewModel>() {
    enum class DepositType {
        ACTIVE,
        FINISHED
    }

    companion object {
        private const val margin23 = 23
        private const val margin140 = 140
        private const val marginZero = 0
    }

    override val bindingVariable: Int = BR.depositsViewModel
    override val getLayoutId: Int = R.layout.fragment_deposits
    override val viewModel: DepositsViewModel by viewModel()

    private val incomeDetails: IncomeDetailsUIModel by lazy {
        DepositsFragmentArgs.fromBundle(requireArguments()).incomeDetails
    }

    private val depositType: DepositType by lazy {
        DepositsFragmentArgs.fromBundle(requireArguments()).depositType
    }

    private val monthAdapter: MonthsAdapter by lazy {
        MonthsAdapter(
            listener = {
                viewModel.onMonthSelected(it.toUIModel())
            }
        )
    }

    override fun tag(): String = DepositsFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun getToolbar(): Toolbar? =
        activity?.findViewById(R.id.toolbar_income)

    override val showHomeAsUp: Boolean = true

    override val showDisplayShowTitle: Boolean = true

    override fun initialize() {
        super.initialize()
        fetchDeposits()
        setupObservers()
    }

    override fun onNavigationClick(view: View) {
        pop()
    }

    private fun setupObservers() {
        viewModel.availableMonths.observe(
            viewLifecycleOwner,
            {
                fetchMonths(it)
            }
        )
    }

    private fun fetchDeposits() {
        viewModel.fetchData(depositType, incomeDetails)
        binding?.apply {
            recyclerDates.adapter = DepositsMonthsAdapter(incomeDetails.deposits) { selectedDeposit ->
                navigateTo(
                    DepositsFragmentDirections.actionDepositsFragmentToDepositDetailFragment(
                        depositType,
                        selectedDeposit
                    )
                )
            }
            val layoutParams = recyclerDates.layoutParams as ViewGroup.MarginLayoutParams
            val topMargin = if (depositType == DepositType.ACTIVE) margin23 else margin140

            layoutParams.setMargins(
                marginZero,
                topMargin.toFloat().dpToPixel(requireContext()).toInt(),
                marginZero,
                marginZero
            )
            recyclerDates.layoutParams = layoutParams
        }
    }

    private fun fetchMonths(months: List<String>) {
        val listMonthUiModel = mutableListOf<MonthUIModel>()
        for (monthYear in months) {
            val parts = monthYear.split("-")
            val array = DateFormatSymbols().months
            val fullValue = "${array[parts[1].toInt() - 1]} - ${parts[0]}"
            listMonthUiModel.add(MonthUIModel(monthYear, fullValue))
        }
        monthAdapter.replaceItems(listMonthUiModel)
    }
}