package br.com.digio.uber.income.mapper

import br.com.digio.uber.model.income.AvailableMonthsDomainResponse
import br.com.digio.uber.model.income.AvailableMonthsResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class AvailableMonthsMapper : AbstractMapper<AvailableMonthsResponse, AvailableMonthsDomainResponse> {
    override fun map(param: AvailableMonthsResponse): AvailableMonthsDomainResponse = with(param) {
        AvailableMonthsDomainResponse(
            yearMonths = yearMonths
        )
    }
}