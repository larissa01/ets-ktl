package br.com.digio.uber.income.mapper

import br.com.digio.uber.income.uiModel.MonthItem
import br.com.digio.uber.income.uiModel.MonthItemUIModel
import br.com.digio.uber.income.uiModel.MonthUIModel

fun List<MonthUIModel>.toItemUIModel(): List<MonthItemUIModel> {
    return this.map {
        MonthItem(
            it.monthYear,
            it.monthYearScreen,
            this.indexOf(it)
        )
    }
}

fun MonthItemUIModel.toUIModel(): MonthUIModel {
    (this as MonthItem).apply {
        return MonthUIModel(
            this.monthYear,
            this.monthYearScreen
        )
    }
}