package br.com.digio.uber.income.uiModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DepositUIModel(
    val yearMonth: String,
    val internalDeposits: List<InternDepositUIModel>
) : Parcelable