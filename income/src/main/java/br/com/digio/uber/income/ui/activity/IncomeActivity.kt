package br.com.digio.uber.income.ui.activity

import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.income.R
import br.com.digio.uber.income.di.incomeViewModelModule
import org.koin.core.module.Module

class IncomeActivity : BaseActivity() {

    override fun getLayoutId(): Int = R.layout.activity_income
    override fun getModule(): List<Module> = listOf(incomeViewModelModule)

    override fun showDisplayShowTitle(): Boolean = true

    override fun getToolbar(): Toolbar? = findViewById<Toolbar>(R.id.toolbar_income).apply {
        this.title = getString(R.string.income_title)
        this.navigationIcon = ContextCompat.getDrawable(context, br.com.digio.uber.common.R.drawable.ic_back_arrow)
    }
}