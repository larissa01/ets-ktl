package br.com.digio.uber.income.ui.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.income.BR
import br.com.digio.uber.income.R
import br.com.digio.uber.income.databinding.FragmentDepositDetailBinding
import br.com.digio.uber.income.ui.viewmodel.DepositsViewModel
import br.com.digio.uber.income.uiModel.InternDepositUIModel
import org.koin.android.viewmodel.ext.android.viewModel

class DepositDetailFragment : BaseViewModelFragment<FragmentDepositDetailBinding, DepositsViewModel>() {

    override val bindingVariable: Int = BR.depositsViewModel
    override val getLayoutId: Int = R.layout.fragment_deposit_detail
    override val viewModel: DepositsViewModel by viewModel()

    private val detailDeposit: InternDepositUIModel by lazy {
        DepositDetailFragmentArgs.fromBundle(requireArguments()).internDeposit
    }

    private val depositType: DepositsFragment.DepositType by lazy {
        DepositDetailFragmentArgs.fromBundle(requireArguments()).depositType
    }

    override fun tag(): String = DepositDetailFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun getToolbar(): Toolbar? =
        activity?.findViewById(R.id.toolbar_income)

    override val showHomeAsUp: Boolean = true

    override val showDisplayShowTitle: Boolean = true

    override fun onNavigationClick(view: View) {
        pop()
    }

    override fun initialize() {
        super.initialize()
        binding?.viewModel = viewModel
        setupUI()
    }

    private fun setupUI() {
        viewModel.detailDeposit(detailDeposit)
        viewModel.isActiveDeposits.value = depositType == DepositsFragment.DepositType.ACTIVE
        binding?.txtDetailDepositEnddateLabel?.text = getString(
            if (depositType == DepositsFragment.DepositType.ACTIVE) {
                R.string.income_enddate
            } else {
                R.string.income_last_withdrawal
            }
        )
    }
}