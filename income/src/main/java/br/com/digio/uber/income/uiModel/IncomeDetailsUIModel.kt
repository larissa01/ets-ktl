package br.com.digio.uber.income.uiModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal

@Parcelize
data class IncomeDetailsUIModel(
    var deposits: List<DepositUIModel>,
    val grossIncome: String,
    val grossIncomeBD: BigDecimal,
    val incomeIndex: String,
    val iofValue: String,
    val iofValueBD: BigDecimal,
    val irValue: String,
    val irValueBD: BigDecimal,
    val netIncome: String,
    val netIncomeBD: BigDecimal,
    val yearMonth: String
) : Parcelable {
    fun hasDeposits(): Boolean = deposits.isNotEmpty()
}