#!/bin/sh

# Add Android SDK license in a default file
echo "initing git submodules"
echo $(pwd)
git submodule add ssh://$REPOSITORY_OAUTH_ACCESS_TOKEN@bitbucket.org/cbssmobile/pix.git
git submodule update --init --recursive
echo "finish git submodules"