package br.com.digio.uber.splash.viewmodel

import androidx.lifecycle.LiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.splash.CheckUserHasLoggedInUseCase

class SplashViewModel constructor(
    private val checkUserHasLoggedInUseCase: CheckUserHasLoggedInUseCase
) : BaseViewModel() {

    val checkUser: LiveData<Boolean> = checkUserHasLoggedInUseCase(Unit).exec()
}