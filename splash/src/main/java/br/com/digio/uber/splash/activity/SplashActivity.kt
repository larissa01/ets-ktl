package br.com.digio.uber.splash.activity

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.splash.BuildConfig
import br.com.digio.uber.splash.di.splashModule
import br.com.digio.uber.splash.viewmodel.SplashViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module

class SplashActivity : BaseViewModelActivity<ViewDataBinding, SplashViewModel>() {

    override val viewModel: SplashViewModel by viewModel()

    override fun getModule(): List<Module>? = listOf(splashModule)

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)

        viewModel.checkUser.observe(
            this,
            Observer {
                when {
                    BuildConfig.SHOW_FEATURE_NAVIGATION -> {
                        navigation.navigationToFeatureNavigationActivity(this)
                    }
                    it == true -> {
                        navigation.navigationToMainActivity(this)
                    }
                    else -> {
                        navigation.navigationToLoginActivity(this)
                    }
                }
            }
        )
    }

    override val bindingVariable: Int? = null
    override fun getLayoutId(): Int? = null
}