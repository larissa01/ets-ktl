/* Apply Module base configurations */
plugins {
    id("com.android.dynamic-feature")
    kotlin("android")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.dynamic.library")
}

configureAndroidDynamicLibrary(
    releaseBuildTypeConfig = {
        buildConfigField("Boolean", "SHOW_FEATURE_NAVIGATION", "false")
    },
    homolBuildTypeConfig = {
        buildConfigField("Boolean", "SHOW_FEATURE_NAVIGATION", "false")
    },
    debugBuildTypeConfig = {
        buildConfigField("Boolean", "SHOW_FEATURE_NAVIGATION", "true")
    }
)

dependencies {

    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.ANDROIDXCORE)
    implementation(Dependencies.ACTIVITYKTX)
    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.FRAGMENTKTX)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.KOIN)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)

    implementation(project(":app"))
    implementation(project(":common"))
    implementation(project(":domain"))

    testImplementation(TestDependencies.JUNIT)
    testImplementation(TestDependencies.MOCKK)
    testImplementation(TestDependencies.COROUTINESTEST)
    testImplementation(TestDependencies.ANDROIDX_CORE_TESTING)
}