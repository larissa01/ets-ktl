package br.com.digio.uber.choice.activity

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.uber.choice.BR
import br.com.digio.uber.choice.R
import br.com.digio.uber.choice.databinding.ActivityChoiceBinding
import br.com.digio.uber.choice.di.choiceViewModelModule
import br.com.digio.uber.choice.viewModel.ChoiceViewModel
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.choice.Choice
import br.com.digio.uber.common.choice.adapter.ChoicesAdapter
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.Extras.ARG_CPF
import br.com.digio.uber.util.Const.Extras.FRAGMENT_KEY
import br.com.digio.uber.util.Const.RequestOnResult.Transfer.FINISH_TRANSFER_FLOW_RESULT_CODE
import br.com.digio.uber.util.Const.RequestOnResult.Transfer.NEW_TRANSFER_REQUEST_CODE
import org.koin.android.ext.android.inject
import org.koin.core.module.Module
import java.util.ArrayList

class ChoiceActivity : BaseViewModelActivity<ActivityChoiceBinding, ChoiceViewModel>() {

    override fun getLayoutId(): Int? = R.layout.activity_choice
    override fun getModule(): List<Module>? = listOf(choiceViewModelModule)
    override val bindingVariable: Int? = BR.choiceViewModel
    override val viewModel: ChoiceViewModel by inject()

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        initToolbar()
        initBundle(intent.extras)

        viewModel.getCpf()
    }

    private fun initToolbar() {
        setSupportActionBar(binding?.toolbarNavigation)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == NEW_TRANSFER_REQUEST_CODE &&
            resultCode == FINISH_TRANSFER_FLOW_RESULT_CODE
        ) {
            finish()
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun initBundle(extras: Bundle?) {
        var choices: ArrayList<Choice>? = null

        if (intent.categories?.contains(Const.ChoiceCategories.PixReceivementInputValuesActivity) == true) {
            choices = arrayListOf(
                Choice(
                    br.com.digio.uber.common.R.drawable.ic_receive_with_pix,
                    br.com.digio.uber.common.R.string.receive_with_pix,
                    br.com.digio.uber.common.R.string.receive_with_pix_subtitle,
                    Const.Activities.PIX_RECEIVEMENT_INPUT_VALUES_ACTIVITY
                ),
                Choice(
                    br.com.digio.uber.common.R.drawable.ic_generate_bankslip,
                    br.com.digio.uber.common.R.string.generate_bankslip,
                    br.com.digio.uber.common.R.string.generate_bankslip_subtitle,
                    Const.Activities.BANK_SLIP_ACTIVITY
                ),
                Choice(
                    br.com.digio.uber.common.R.drawable.ic_generated_bankslips,
                    br.com.digio.uber.common.R.string.generated_bankslips,
                    br.com.digio.uber.common.R.string.generated_bankslips_subtitle,
                    Const.Activities.BANK_SLIP_GENERATED_ACTIVITY
                )
            )
            supportActionBar?.title = getString(br.com.digio.uber.common.R.string.feature_receive)
        } else {
            extras?.let { extrasLet ->
                val toolbarName = getString(extrasLet.getInt(Const.Extras.TOOLBAR_ID))
                supportActionBar?.title = toolbarName
                choices = extrasLet.getParcelableArrayList(Const.Extras.CHOICES)
            }
        }

        choices?.let {
            binding?.rvChoices?.layoutManager = LinearLayoutManager(this)
            binding?.rvChoices?.adapter = ChoicesAdapter(it) { selectedChoice ->
                val bundle = Bundle()

                if (selectedChoice.keepBundle) {
                    bundle.putAll(extras)
                }

                if (selectedChoice.fragmentToStart != null) {
                    bundle.putString(FRAGMENT_KEY, selectedChoice.fragmentToStart)
                }
                if (selectedChoice.activityToStart == Const.Activities.TRANSFER_ACTIVITY) {
                    navigation.navigationToDynamicWithResult(
                        this,
                        selectedChoice.activityToStart,
                        bundle,
                        NEW_TRANSFER_REQUEST_CODE
                    )
                } else if (selectedChoice.activityToStart == Const.Activities.PIX_RECEIVEMENT_INPUT_VALUES_ACTIVITY) {
                    bundle.putString(ARG_CPF, viewModel.cpfValue.value)
                    navigation.navigationToDynamic(this, selectedChoice.activityToStart, bundle)
                    finish()
                } else {
                    navigation.navigationToDynamic(this, selectedChoice.activityToStart, bundle)
                }
            }
        }
    }
}