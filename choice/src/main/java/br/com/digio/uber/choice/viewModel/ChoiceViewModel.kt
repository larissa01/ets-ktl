package br.com.digio.uber.choice.viewModel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.account.GetAccountUseCase

class ChoiceViewModel(
    private val getAccountUseCase: GetAccountUseCase
) : BaseViewModel() {

    val cpfValue = MutableLiveData<String>()

    fun getCpf() {
        getAccountUseCase(Unit).exec().observeForever { accountResponse ->
            cpfValue.value = accountResponse.cpf
        }
    }
}