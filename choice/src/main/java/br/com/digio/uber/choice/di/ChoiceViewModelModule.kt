package br.com.digio.uber.choice.di

import br.com.digio.uber.choice.viewModel.ChoiceViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val choiceViewModelModule = module {
    viewModel { ChoiceViewModel(get()) }
}