package br.com.digio.uber.menu.di

import br.com.digio.uber.menu.viewmodel.MenuViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val menuViewModelModule = module {
    viewModel { MenuViewModel(get()) }
}