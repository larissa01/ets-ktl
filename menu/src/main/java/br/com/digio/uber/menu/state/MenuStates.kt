package br.com.digio.uber.menu.state

sealed class MenuStates {
    object GoToAppSettings : MenuStates()
    object GoToChat : MenuStates()
}