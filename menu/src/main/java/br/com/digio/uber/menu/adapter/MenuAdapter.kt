package br.com.digio.uber.menu.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.menu.R
import br.com.digio.uber.menu.adapter.viewholder.MenuArrowViewModel
import br.com.digio.uber.menu.adapter.viewholder.MenuBigSeparatorViewHolder
import br.com.digio.uber.menu.adapter.viewholder.MenuSeparatorViewHolder
import br.com.digio.uber.menu.adapter.viewholder.MenuSwitchViewModel
import br.com.digio.uber.menu.adapter.viewholder.MenuTitleViewModel
import br.com.digio.uber.menu.uimodel.MenuArrowItem
import br.com.digio.uber.menu.uimodel.MenuBigSeparatorItem
import br.com.digio.uber.menu.uimodel.MenuItem
import br.com.digio.uber.menu.uimodel.MenuSeparatorItem
import br.com.digio.uber.menu.uimodel.MenuSwitchItem
import br.com.digio.uber.menu.uimodel.MenuTitleItem
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

typealias OnClickMenuItemAdapter = (item: MenuItem) -> Unit

class MenuAdapter constructor(
    private var values: MutableList<MenuItem> = mutableListOf(),
    private val listener: OnClickMenuItemAdapter
) : BaseRecyclerViewAdapter<MenuItem, MenuAdapter.ViewTypesDataBindingFactoryImpl>() {

    class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<MenuItem> {
        override fun type(model: MenuItem): Int =
            when (model) {
                is MenuBigSeparatorItem -> R.layout.menu_big_separator_item
                is MenuSeparatorItem -> R.layout.menu_separator_item
                is MenuArrowItem -> R.layout.menu_arrow_item
                is MenuSwitchItem -> R.layout.menu_switch_item
                is MenuTitleItem -> R.layout.menu_title_item
            }

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<MenuItem>
        ): AbstractViewHolder<*> =
            when (type) {
                R.layout.menu_big_separator_item -> MenuBigSeparatorViewHolder(view, listener)
                R.layout.menu_separator_item -> MenuSeparatorViewHolder(view, listener)
                R.layout.menu_arrow_item -> MenuArrowViewModel(view, listener)
                R.layout.menu_switch_item -> MenuSwitchViewModel(view, listener)
                R.layout.menu_title_item -> MenuTitleViewModel(view, listener)
                else -> throw IndexOutOfBoundsException("Invalid view type")
            }
    }

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl =
        ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): MenuItem =
        values[position]

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<MenuItem> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder =
            typeFactory.holder(type = viewType, view = viewDataBinding, listener = listener)

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<MenuItem>
    }

    override fun getItemCount(): Int =
        values.size

    override fun onBindViewHolder(holder: AbstractViewHolder<MenuItem>, position: Int) =
        holder.bind(values[holder.adapterPosition])

    override fun replaceItems(list: List<Any>) {
        addValues(list.safeHeritage())
    }

    fun addValues(values: List<MenuItem>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }
}