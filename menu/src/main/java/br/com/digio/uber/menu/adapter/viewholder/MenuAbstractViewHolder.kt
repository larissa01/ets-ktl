package br.com.digio.uber.menu.adapter.viewholder

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.menu.BR
import br.com.digio.uber.menu.R
import br.com.digio.uber.menu.adapter.OnClickMenuItemAdapter
import br.com.digio.uber.menu.uimodel.MenuArrowItem
import br.com.digio.uber.menu.uimodel.MenuBigSeparatorItem
import br.com.digio.uber.menu.uimodel.MenuItem
import br.com.digio.uber.menu.uimodel.MenuSeparatorItem
import br.com.digio.uber.menu.uimodel.MenuSwitchItem
import br.com.digio.uber.menu.uimodel.MenuTitleItem

sealed class MenuAbstractViewHolder<ITEM : MenuItem>(
    private val viewDataBinding: ViewDataBinding,
    private val listener: OnClickMenuItemAdapter
) : AbstractViewHolder<ITEM>(viewDataBinding.root) {
    var item: ITEM? = null
    override fun bind(item: ITEM) {
        this.item = item
        viewDataBinding.setVariable(BR.menuItem, item)
        viewDataBinding.setVariable(BR.menuViewHolder, this)
        viewDataBinding.executePendingBindings()
    }

    fun onClick(item: ITEM) {
        item.onClickMenuItemAdapter?.invoke(item)
        listener(item)
    }
}

class MenuBigSeparatorViewHolder(
    viewDataBinding: ViewDataBinding,
    listener: OnClickMenuItemAdapter
) : MenuAbstractViewHolder<MenuBigSeparatorItem>(viewDataBinding, listener)

class MenuSeparatorViewHolder(
    viewDataBinding: ViewDataBinding,
    listener: OnClickMenuItemAdapter
) : MenuAbstractViewHolder<MenuSeparatorItem>(viewDataBinding, listener)

class MenuArrowViewModel(
    viewDataBinding: ViewDataBinding,
    listener: OnClickMenuItemAdapter
) : MenuAbstractViewHolder<MenuArrowItem>(viewDataBinding, listener) {
    fun getSubtitle(): Int =
        item?.subtitle ?: R.string.empty
}

class MenuSwitchViewModel(
    viewDataBinding: ViewDataBinding,
    listener: OnClickMenuItemAdapter
) : MenuAbstractViewHolder<MenuSwitchItem>(viewDataBinding, listener) {
    fun getSubtitle(): Int =
        item?.subtitle ?: R.string.empty
}

class MenuTitleViewModel(
    viewDataBinding: ViewDataBinding,
    listener: OnClickMenuItemAdapter
) : MenuAbstractViewHolder<MenuTitleItem>(viewDataBinding, listener)