package br.com.digio.uber.menu.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.common.generics.makeChooseMessageObject
import br.com.digio.uber.domain.usecase.login.LogoutUseCase
import br.com.digio.uber.menu.R
import br.com.digio.uber.menu.uimodel.MenuArrowItem
import br.com.digio.uber.menu.uimodel.MenuBigSeparatorItem
import br.com.digio.uber.menu.uimodel.MenuItem
import br.com.digio.uber.menu.uimodel.MenuSeparatorItem
import br.com.digio.uber.menu.uimodel.MenuTitleItem
import br.com.digio.uber.model.pid.PidType

class MenuViewModel constructor(
    private val logoutUseCase: LogoutUseCase
) : BaseViewModel() {

    val itemCalledToPid: MutableLiveData<MenuItem> = MutableLiveData()
    val onLogoutClick: MutableLiveData<Boolean> = MutableLiveData()

    val items = MutableLiveData<List<MenuItem>>().apply {
        value = listOf(
            MenuBigSeparatorItem,
            MenuArrowItem(
                icon = R.drawable.ic_register_data,
                title = R.string.register_data
            ),
            MenuBigSeparatorItem,
            MenuTitleItem(
                title = R.string.my_application
            ),
            MenuArrowItem(
                icon = R.drawable.ic_show_card_password,
                title = R.string.show_card_password,
                requirePIDType = PidType.SHOW_CARD_PASSWORD
            ),
            MenuSeparatorItem,
            MenuArrowItem(
                icon = R.drawable.ic_change_password,
                title = R.string.change_password_menu
            ),
            MenuSeparatorItem,
            MenuArrowItem(
                icon = R.drawable.ic_config,
                title = R.string.config
            ),
            MenuBigSeparatorItem,
            MenuTitleItem(
                title = R.string.helpper_center
            ),
            MenuArrowItem(
                icon = R.drawable.ic_call,
                title = R.string.call
            ),
//            MenuSeparatorItem,
//            MenuArrowItem(
//                icon = R.drawable.ic_loss_or_theft,
//                title = R.string.loss_or_theft,
//                activity = CARD_REISSUE_ACTIVITY
//            ),
            MenuSeparatorItem,
            MenuArrowItem(
                icon = R.drawable.ic_account_franchise,
                title = R.string.account_franchise
            ),
            MenuSeparatorItem,
            MenuArrowItem(
                icon = R.drawable.ic_contract,
                title = R.string.contract
            ),
            MenuBigSeparatorItem,
            MenuArrowItem(
                icon = R.drawable.ic_exit,
                title = R.string.exit,
                onClickMenuItemAdapter = {
                    showExitMessage()
                }
            ),
            MenuBigSeparatorItem
        )
    }

    private fun showExitMessage() {
        message.value = makeChooseMessageObject {
            icon = br.com.digio.uber.common.R.drawable.ic_alert
            title = R.string.are_you_sure_you_want_to_leave
            messageString = String()
            positiveOptionTextInt = R.string.exit
            negativeOptionTextInt = br.com.digio.uber.common.R.string.no
            hideMessage = true
            onPositiveButtonClick = ::makeLogoutToUser
        }
    }

    private fun makeLogoutToUser() {
        logoutUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                onLogoutClick.value = true
            },
            onError = { _, _, _ ->
                onLogoutClick.value = true
                EmptyErrorObject
            }
        )
    }
}