package br.com.digio.uber.menu.activity

import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.menu.R
import br.com.digio.uber.menu.di.menuViewModelModule
import org.koin.core.module.Module

class MenuActivity : BaseActivity() {
    override fun getLayoutId(): Int? =
        R.layout.activity_menu

    override fun getModule(): List<Module>? =
        listOf(menuViewModelModule)
}