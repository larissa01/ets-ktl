package br.com.digio.uber.menu.uimodel

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.menu.adapter.OnClickMenuItemAdapter
import br.com.digio.uber.model.pid.PidType

sealed class MenuItem(
    @DrawableRes
    open val icon: Int? = null,
    @StringRes
    open val title: Int? = null,
    @StringRes
    open val subtitle: Int? = null,
    open val onClickMenuItemAdapter: OnClickMenuItemAdapter? = null,
    open val requirePIDType: PidType? = null,
    open val activity: String? = null
) : AdapterViewModel<MenuItem> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<MenuItem>) = typesFactory.type(
        model = this
    )
}

object MenuBigSeparatorItem : MenuItem()

object MenuSeparatorItem : MenuItem()

data class MenuArrowItem(
    @DrawableRes
    override val icon: Int,
    @StringRes
    override val title: Int,
    @StringRes
    override val subtitle: Int? = null,
    override val onClickMenuItemAdapter: OnClickMenuItemAdapter? = null,
    override val requirePIDType: PidType? = null,
    override val activity: String? = null
) : MenuItem(
    icon = icon,
    title = title,
    subtitle = subtitle,
    onClickMenuItemAdapter = onClickMenuItemAdapter
)

data class MenuSwitchItem(
    @DrawableRes
    override val icon: Int,
    @StringRes
    override val title: Int,
    @StringRes
    override val subtitle: Int? = null,
    override val onClickMenuItemAdapter: OnClickMenuItemAdapter? = null,
    override val requirePIDType: PidType? = null,
    var switch: Boolean = false
) : MenuItem(
    icon = icon,
    title = title,
    subtitle = subtitle,
    onClickMenuItemAdapter = onClickMenuItemAdapter
)

data class MenuTitleItem(
    @StringRes
    override val title: Int,
) : MenuItem(
    title = title
)