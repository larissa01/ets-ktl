package br.com.digio.uber.menu.fragment

import android.content.Intent
import android.net.Uri
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.goToAppSettings
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.menu.BR
import br.com.digio.uber.menu.R
import br.com.digio.uber.menu.adapter.MenuAdapter
import br.com.digio.uber.menu.databinding.MenuFragmentBinding
import br.com.digio.uber.menu.uimodel.MenuItem
import br.com.digio.uber.menu.viewmodel.MenuViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.OLD_PASSWORD_SCREEN
import br.com.digio.uber.util.safeLet
import org.koin.android.ext.android.inject

class MenuFragment : BaseViewModelFragment<MenuFragmentBinding, MenuViewModel>() {

    override val bindingVariable: Int? = BR.menuViewModel
    override val getLayoutId: Int? = R.layout.menu_fragment
    override val viewModel: MenuViewModel? by inject()

    override fun tag(): String = MenuFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.BLACK

    override fun getToolbar(): Toolbar? =
        binding?.toolbarMenu

    override fun initialize() {
        super.initialize()
        binding?.recyMenu?.adapter = MenuAdapter(
            listener = { menuItem ->
                menuItem.requirePIDType?.let { pidType ->
                    viewModel?.itemCalledToPid?.value = menuItem
                    navigation.navigationToPidActivity(this, pidType)
                } ?: menuItemClick(menuItem)
            }
        )
        binding?.recyMenu?.layoutManager = LinearLayoutManager(context)

        viewModel?.onLogoutClick?.observe(
            this,
            Observer {
                if (it == true) {
                    context?.let { contextLet ->
                        navigation.navigationToSplash(contextLet)
                    }
                    viewModel?.onLogoutClick?.value = false
                }
            }
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Const.RequestOnResult.PID.PID_REQUEST_CODE ->
                handlePidRequestCode(resultCode, data)
        }
    }

    private fun handlePidRequestCode(resultCode: Int, data: Intent?) {
        when (resultCode) {
            Const.RequestOnResult.PID.RESULT_PID_SUCCESS ->
                safeLet(
                    data?.extras?.getString(Const.RequestOnResult.PID.KEY_PID_HASH),
                    viewModel?.itemCalledToPid?.value
                ) { pidHash, menuItem ->
                    handleMenuItemClickWithPid(
                        pidHash,
                        menuItem
                    )
                }
        }
    }

    private fun handleMenuItemClickWithPid(pidHash: String, menuItem: MenuItem) {
        when (menuItem.title) {
            R.string.show_card_password ->
                activity?.let { activity ->
                    navigation.navigationToShowCardPassword(activity, pidHash)
                }
            R.string.register_data -> {
                activity?.let { activity ->
                    navigation.navigationToChangeRegistrationActivity(activity)
                }
            }
        }
    }

    private fun menuItemClick(menuItem: MenuItem) {
        when (menuItem.title) {
            R.string.change_password_menu ->
                navigation.navigateToChangePasswordActivity(
                    this@MenuFragment,
                    OLD_PASSWORD_SCREEN
                )
            R.string.call ->
                findNavController().navigate(Uri.parse(Const.DeepLink.DEEPLINK_FAQ_INITIAL))
            R.string.contract -> navigation.navigationToFaqPdf(requireActivity())

            R.string.config -> {
                context?.goToAppSettings(requireContext())
            }
            R.string.register_data -> {
                activity?.let { activity ->
                    navigation.navigationToChangeRegistrationActivity(activity)
                }
            }

            R.string.account_franchise -> {
                navigation.navigationToFranchiseActivity(this@MenuFragment, 0)
            }
        }

        menuItem.activity?.let {
            navigation.navigationToDynamic(requireActivity(), it)
        }
    }

    override val showDisplayShowTitle: Boolean = true
}