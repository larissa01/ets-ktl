package br.com.digio.uber.store.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.cashback.GetAmountCashbackUseCase
import br.com.digio.uber.domain.usecase.cashback.GetCashbackStoresUseCase
import br.com.digio.uber.domain.usecase.store.GetStoreCatalogUseCase
import br.com.digio.uber.model.cashback.CashbackStatus
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.model.store.StoreProduct
import br.com.digio.uber.store.R
import br.com.digio.uber.util.toMoney

class StoreViewModel constructor(
    private val getStoreCatalogUseCase: GetStoreCatalogUseCase,
    private val getAmountCashbackUseCase: GetAmountCashbackUseCase,
    private val getCashbackStoresUseCase: GetCashbackStoresUseCase,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    val services: MutableLiveData<List<StoreProduct>> = MutableLiveData()
    val spotlights: MutableLiveData<List<StoreProduct>> = MutableLiveData()
    val cashbackAmount: MutableLiveData<String> = MutableLiveData()
    val cashbackStores: MutableLiveData<List<CashbackStore>> = MutableLiveData()
    val totalElements: MutableLiveData<Int> = MutableLiveData()

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)

        getStoreCatalogUseCase(Unit).singleExec(
            onError = { _, _, _ ->
                EmptyErrorObject
            },
            onSuccessBaseViewModel = { catalog ->
                services.value = catalog.content
                spotlights.value = catalog.content.filter { it.spotlight == true }
            }
        )

        getAmountCashbackUseCase(CashbackStatus.RELEASED.name).singleExec(
            onError = { _, _, _ ->
                cashbackAmount.value = resourceManager.getString(R.string.unavailable)
                EmptyErrorObject
            },
            onSuccessBaseViewModel = { amount ->
                cashbackAmount.value = amount.amount?.toBigDecimal()?.toMoney()
            }
        )

        getCashbackStoresUseCase(QUANTITY_ITENS).singleExec(
            onError = { _, error, _ ->
                error
            },
            onSuccessBaseViewModel = { stores ->
                cashbackStores.value = stores.content.subList(INIT_COUNT_LIST, END_COUNT_LIST)
                totalElements.value = stores.totalElements
            }
        )
    }

    companion object {
        const val INIT_COUNT_LIST = 0
        const val END_COUNT_LIST = 5
        const val QUANTITY_ITENS = 5
    }
}