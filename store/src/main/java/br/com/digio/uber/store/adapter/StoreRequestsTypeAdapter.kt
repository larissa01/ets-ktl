package br.com.digio.uber.store.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.store.databinding.ItemStoreRequestsTypeBinding
import br.com.digio.uber.store.model.StoreRequestType
import br.com.digio.uber.util.safeHeritage

typealias OnClickOrderTypeItem = (item: StoreRequestType) -> Unit

class StoreRequestsTypeAdapter(
    private val listener: OnClickOrderTypeItem
) : RecyclerView.Adapter<StoreRequestsTypeAdapter.StoreRequestsTypeViewHolder>(), AdapterItemsContract {

    private var types: List<StoreRequestType> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreRequestsTypeViewHolder {
        val binding =
            ItemStoreRequestsTypeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StoreRequestsTypeViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: StoreRequestsTypeViewHolder, position: Int) {
        holder.bind(types[position], listener)
    }

    override fun getItemCount() = types.size

    override fun replaceItems(list: List<Any>) {
        types = list.safeHeritage()
        notifyDataSetChanged()
    }

    class StoreRequestsTypeViewHolder(private val binding: ItemStoreRequestsTypeBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(type: StoreRequestType, listener: OnClickOrderTypeItem) {
            binding.storeRequestType = type

            backgroundBlack(type.selected)

            binding.itemStoreRequestTypeTxt.setOnClickListener {
                val storeRequestType = type.copy(selected = true)
                backgroundBlack(true)
                listener.invoke(storeRequestType)
            }
        }

        private fun backgroundBlack(selected: Boolean) {
            binding.itemStoreRequestTypeTxt.background =
                ContextCompat.getDrawable(
                    context,
                    if (selected) {
                        br.com.digio.uber.common.R.drawable.button_border_radius_60_bg_000000
                    } else {
                        br.com.digio.uber.common.R.drawable.button_border_radius_60_bg_eeeeee
                    }
                )
            binding.itemStoreRequestTypeTxt.setTextColor(
                ContextCompat.getColor(
                    context,
                    if (selected) {
                        br.com.digio.uber.common.R.color.white
                    } else {
                        br.com.digio.uber.common.R.color.black
                    }
                )
            )
        }
    }
}