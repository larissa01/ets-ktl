package br.com.digio.uber.store.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.extensions.loadImage
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.common.typealiases.OnClickProductItem
import br.com.digio.uber.model.store.StoreProduct
import br.com.digio.uber.store.databinding.ItemStoreSpotlightBinding
import br.com.digio.uber.util.safeHeritage

class StoreSpotlightAdapter(
    private val listener: OnClickProductItem
) :
    RecyclerView.Adapter<StoreSpotlightAdapter.StoreSpotlightViewHolder>(), AdapterItemsContract {

    private var spotlights: List<StoreProduct> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreSpotlightViewHolder {
        val binding =
            ItemStoreSpotlightBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StoreSpotlightViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: StoreSpotlightViewHolder, position: Int) {
        holder.bind(spotlights[position], listener)
    }

    override fun getItemCount() = spotlights.size

    override fun replaceItems(list: List<Any>) {
        spotlights = list.safeHeritage()
        notifyDataSetChanged()
    }

    class StoreSpotlightViewHolder(private val binding: ItemStoreSpotlightBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(spotlight: StoreProduct, listener: OnClickProductItem) {
            spotlight.image?.let { binding.itemStoreImgSpotlight.loadImage(context, it) }
            binding.itemStoreCardContent.setOnClickListener { listener.invoke(spotlight) }
        }
    }
}