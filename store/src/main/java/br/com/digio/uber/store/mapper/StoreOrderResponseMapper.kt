package br.com.digio.uber.store.mapper

import br.com.digio.uber.model.store.StoreOrderResponse
import br.com.digio.uber.store.model.StoreOrderResponseUiModel
import br.com.digio.uber.util.mapper.AbstractMapper

class StoreOrderResponseMapper :
    AbstractMapper<StoreOrderResponse, StoreOrderResponseUiModel> {
    override fun map(param: StoreOrderResponse): StoreOrderResponseUiModel = with(param) {
        StoreOrderResponseUiModel(
            id = id,
            customerId = customerId,
            customerDocument = customerDocument,
            value = value,
            installments = installments,
            quantity = quantity,
            orderStatusDescription = orderStatusDescription,
            orderStatus = orderStatus,
            orderCode = orderCode,
            paymentStatus = paymentStatus,
            partnerStatus = partnerStatus,
            history = history,
            inputDate = inputDate,
            lastUpdatedDate = lastUpdatedDate,
            category = category,
            orderDetail = orderDetail,
            usageInstructions = usageInstructions,
            activationCode = activationCode,
            shareText = shareText,
            message = message,
            statusColor = statusColor
        )
    }
}