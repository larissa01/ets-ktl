package br.com.digio.uber.store.model

data class StoreRequestType(
    val type: String? = null,
    val selected: Boolean = false
)