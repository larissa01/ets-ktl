package br.com.digio.uber.store.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.helper.PaginationListener
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.store.BR
import br.com.digio.uber.store.R
import br.com.digio.uber.store.adapter.StoreMyRequestsAdapter
import br.com.digio.uber.store.adapter.StoreRequestsTypeAdapter
import br.com.digio.uber.store.databinding.FragmentStoreMyRequestsBinding
import br.com.digio.uber.store.viewmodel.StoreMyRequestsViewModel
import org.koin.android.ext.android.inject

class StoreMyRequestsFragment : BaseViewModelFragment<FragmentStoreMyRequestsBinding, StoreMyRequestsViewModel>() {

    override val bindingVariable: Int? = BR.storeMyRequestsViewModel

    override val getLayoutId: Int? = R.layout.fragment_store_my_requests

    override val viewModel: StoreMyRequestsViewModel by inject()

    override fun tag(): String = StoreMyRequestsFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.GREY

    override fun getToolbar(): Toolbar? = binding?.storeMyRequestsAppBar?.toolbarStoreMyRequests

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true

    override fun onNavigationClick(view: View) {
        onBackPressed()
    }

    override fun initialize() {
        super.initialize()

        setAdapters()

        viewModel.initializer { }
    }

    private fun setAdapters() {
        binding?.storeMyRequestsRvTypes?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = StoreRequestsTypeAdapter() {
                viewModel.filterOrders(it)
            }
        }

        binding?.storeMyRequestsRvDetails?.apply {
            val linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            layoutManager = linearLayoutManager
            adapter = StoreMyRequestsAdapter() { _ -> }
            setupPaginationListener(linearLayoutManager)
        }
    }

    private fun setupPaginationListener(linearLayoutManager: LinearLayoutManager) {
        val paginationListener = object : PaginationListener(layoutManager = linearLayoutManager) {
            override var isLastPage: Boolean = viewModel.isLastPage.value == true
            override var isLoading: Boolean = viewModel.loadingDownPage.value == true

            override fun loadMoreItems() {
                viewModel.page.value = viewModel.page.value?.plus(1) ?: 0

                viewModel.getStoreOrders()
            }
        }

        binding?.storeMyRequestsRvDetails?.addOnScrollListener(paginationListener)

        viewModel.maxPages.observe(
            this,
            Observer {
                it?.let { maxPages ->
                    paginationListener.pageSize = maxPages
                }
            }
        )

        viewModel.isLastPage.observe(
            this,
            Observer {
                it?.let { isLastPage ->
                    paginationListener.isLastPage = isLastPage
                }
            }
        )

        viewModel.loadingDownPage.observe(
            this,
            Observer {
                it?.let { isLoading ->
                    paginationListener.isLoading = isLoading
                }
            }
        )

        viewModel.page.observe(
            this,
            Observer {
                paginationListener.isLoading = true
            }
        )
    }

    override fun onBackPressed() {
        super.onBackPressed()
        activity?.finish()
    }
}