package br.com.digio.uber.store.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.store.GetStoreOrdersUseCase
import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.store.StoreOrderResponse
import br.com.digio.uber.store.R
import br.com.digio.uber.store.mapper.StoreOrderResponseMapper
import br.com.digio.uber.store.model.StoreOrderResponseItemUiModel
import br.com.digio.uber.store.model.StoreRequestType

class StoreMyRequestsViewModel constructor(
    private val resourceManager: ResourceManager,
    private val getStoreOrdersUseCase: GetStoreOrdersUseCase
) : BaseViewModel() {

    val loadingDownPage: MutableLiveData<Boolean> = MutableLiveData()
    val maxPages: MutableLiveData<Int> = MutableLiveData()
    val page: MutableLiveData<Int> = MutableLiveData<Int>().apply {
        value = 0
    }
    val isLastPage: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = false
    }
    val orders: MutableLiveData<List<StoreOrderResponseItemUiModel>> = MutableLiveData()
    private var allOrders: MutableList<StoreOrderResponse> = mutableListOf()

    val category: MutableLiveData<List<StoreRequestType>> = MutableLiveData()
    private val listType: MutableList<StoreRequestType> =
        mutableListOf(StoreRequestType(resourceManager.getString(R.string.see_all), true))
    private var typeSelected = StoreRequestType(resourceManager.getString(R.string.see_all), true)
    val hideEmptyList: MutableLiveData<Boolean> = MutableLiveData(false)
    private val firstRequest: MutableLiveData<Boolean> = MutableLiveData(true)

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)

        getStoreOrders()
    }

    fun getStoreOrders() {
        if (isLastPage.value == false) {
            loadingDownPage.value = true

            getStoreOrdersUseCase(page.value ?: 0).singleExec(
                onError = { _, error, _ ->
                    hideEmptyList.value = true
                    error
                },
                onSuccessBaseViewModel = { ordersResponse ->
                    updatePaginatorList(ordersResponse)
                    loadingDownPage.value = false
                }
            )
        }
    }

    private fun addFilters(ordersResponse: ResponseList<StoreOrderResponse>) {
        ordersResponse.content.groupBy { it.category?.type }.keys.forEach { type ->
            if (listType.any { it.type == type }.not()) {
                listType.add(StoreRequestType(type, false))
            }
        }
        category.value = listType
    }

    private fun addOrders(ordersResponse: ResponseList<StoreOrderResponse>) {
        allOrders.addAll(ordersResponse.content)

        filterOrders(typeSelected)
    }

    fun filterOrders(category: StoreRequestType) {
        typeSelected = category

        val storeRequestType: MutableList<StoreRequestType> = mutableListOf()

        listType.forEach {
            storeRequestType.add(
                StoreRequestType(
                    it.type,
                    if (it.type == category.type) category.selected
                    else false
                )
            )
        }

        this.category.value = storeRequestType

        if (typeSelected.type == resourceManager.getString(R.string.see_all)) {
            orders.value = allOrders.map { StoreOrderResponseMapper().map(it) }
        } else {
            orders.value =
                allOrders.map { StoreOrderResponseMapper().map(it) }.filter { it.category?.type == category.type }
        }
    }

    private fun updatePaginatorList(ordersResponse: ResponseList<StoreOrderResponse>) {
        hideEmptyList.value = ordersResponse.content.isNullOrEmpty() && category.value.isNullOrEmpty()

        addFilters(ordersResponse)

        addOrders(ordersResponse)

        firstRequest.value = false
        isLastPage.value = ordersResponse.last
    }
}