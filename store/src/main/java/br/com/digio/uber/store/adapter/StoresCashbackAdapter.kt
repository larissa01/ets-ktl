package br.com.digio.uber.store.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.extensions.hide
import br.com.digio.uber.common.extensions.show
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.common.typealiases.OnClickCashbackStoreHomeItem
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.model.cashback.Tiers
import br.com.digio.uber.store.R
import br.com.digio.uber.store.databinding.ItemStoreCashbackBinding
import br.com.digio.uber.util.convertToPercent
import br.com.digio.uber.util.safeHeritage

class StoresCashbackAdapter(
    private val listener: OnClickCashbackStoreHomeItem
) : RecyclerView.Adapter<StoresCashbackAdapter.StoresCashbackViewHolder>(), AdapterItemsContract {

    private var stores: List<CashbackStore> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoresCashbackViewHolder {
        val binding = ItemStoreCashbackBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StoresCashbackViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: StoresCashbackViewHolder, position: Int) {
        holder.bind(stores[position], position + 1 == itemCount, listener)
    }

    override fun getItemCount() = stores.size

    override fun replaceItems(list: List<Any>) {
        stores = list.safeHeritage()
        notifyDataSetChanged()
    }

    class StoresCashbackViewHolder(private val binding: ItemStoreCashbackBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(product: CashbackStore, last: Boolean, listener: OnClickCashbackStoreHomeItem) {
            binding.cashbackStore = product
            binding.itemStoreCashbackCard.setOnClickListener { listener.invoke(product, last) }

            if (last) {
                binding.itemStoreContentSeeAll.show()
                binding.itemStoreContent.hide()
            } else {
                binding.itemStoreContentSeeAll.hide()
                binding.itemStoreContent.show()

                binding.itemStoreCashbackTxtTier.text =
                    String.format(
                        if (product.storeTierStoreInfo?.maxTierCustomerCommission !=
                            product.storeTierStoreInfo?.minTierCustomerCommission
                        ) {
                            context.getString(R.string.tier_to_percentage)
                        } else context.getString(R.string.tier_percentage),
                        product.storeTierStoreInfo?.tierCode?.tier,
                        product.storeTierStoreInfo?.maxTierCustomerCommission?.convertToPercent()
                    )

                if (product.storeTierStoreInfo?.tierCode != Tiers.TIER_4) {
                    binding.itemStoreCashbackTxtNextTier.text =
                        String.format(
                            if (product.nextStoreTierStoreInfo?.maxTierCustomerCommission !=
                                product.nextStoreTierStoreInfo?.minTierCustomerCommission
                            ) {
                                context.getString(R.string.tier_to_percentage)
                            } else context.getString(
                                R.string.tier_percentage
                            ),
                            product.nextStoreTierStoreInfo?.tierCode?.tier,
                            product.nextStoreTierStoreInfo?.maxTierCustomerCommission?.convertToPercent()
                        )
                } else {
                    binding.itemStoreCashbackTxtNextTier.hide()
                    binding.itemStoreCashbackImgNextTier.hide()
                }

                binding.executePendingBindings()
            }
        }
    }
}