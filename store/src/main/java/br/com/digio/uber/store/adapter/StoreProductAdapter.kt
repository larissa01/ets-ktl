package br.com.digio.uber.store.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.extensions.loadImage
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.common.typealiases.OnClickProductItem
import br.com.digio.uber.model.store.StoreProduct
import br.com.digio.uber.store.databinding.ItemStoreProductBinding
import br.com.digio.uber.util.safeHeritage

class StoreProductAdapter(
    private val listener: OnClickProductItem
) : RecyclerView.Adapter<StoreProductAdapter.StoreProductsViewHolder>(), AdapterItemsContract {
    private var products: List<StoreProduct> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreProductsViewHolder {
        val binding = ItemStoreProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StoreProductsViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: StoreProductsViewHolder, position: Int) {
        holder.bind(products[position], listener)
    }

    override fun getItemCount() = products.size

    override fun replaceItems(list: List<Any>) {
        products = list.safeHeritage()
        notifyDataSetChanged()
    }

    class StoreProductsViewHolder(private val binding: ItemStoreProductBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(product: StoreProduct, listener: OnClickProductItem) {
            product.icon?.let {
                binding.itemStoreProductImg.loadImage(context, it)
            }
            product.name?.let {
                binding.itemStoreProductTxt.text = it
            }

            binding.itemStoreProductCard.setOnClickListener { listener.invoke(product) }
        }
    }
}