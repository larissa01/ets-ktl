package br.com.digio.uber.store.activity

import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.store.R
import br.com.digio.uber.store.di.storeViewModelModule
import org.koin.core.module.Module

class MyRequestActivity : BaseActivity() {
    override fun getLayoutId(): Int? = R.layout.activity_my_requests

    override fun getModule(): List<Module>? = listOf(storeViewModelModule)
}