package br.com.digio.uber.store.adapter.viewholder

import android.annotation.SuppressLint
import android.graphics.Color
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.common.extensions.hide
import br.com.digio.uber.common.extensions.show
import br.com.digio.uber.model.store.ProductCategory
import br.com.digio.uber.store.BR
import br.com.digio.uber.store.R
import br.com.digio.uber.store.databinding.ItemStoreMyRequestsBinding
import br.com.digio.uber.store.model.StoreOrderResponseItemUiModel
import br.com.digio.uber.store.model.StoreOrderResponseUiModel
import br.com.digio.uber.util.getDateTimeWithBarSeparator
import br.com.digio.uber.util.toCalendarOrNull
import br.com.digio.uber.util.toNegativeMoney

class StoreMyRequestsViewHolder(
    private val viewDataBinding: ItemStoreMyRequestsBinding,
    private val listener: ViewTypesListener<StoreOrderResponseItemUiModel>
) : AbstractViewHolder<StoreOrderResponseUiModel>(viewDataBinding.root) {

    @SuppressLint("DefaultLocale")
    override fun bind(item: StoreOrderResponseUiModel) = with(viewDataBinding.root) {
        viewDataBinding.itemStoreMyRequestsTxtTitle.text = item.category?.type

        if (item.category != ProductCategory.MOBILITY) {
            viewDataBinding.itemStoreMyRequestsTxtValue.show()
            viewDataBinding.itemStoreMyRequestsTxtValue.text = item.value?.toBigDecimal()?.toNegativeMoney()
            viewDataBinding.itemStoreMyRequestsTxtDetails.text =
                item.inputDate.toCalendarOrNull()?.getDateTimeWithBarSeparator()
            viewDataBinding.itemStoreMyRequestsTxtDetails.setTextColor(Color.GRAY)
        } else {
            viewDataBinding.itemStoreMyRequestsTxtValue.hide()
            viewDataBinding.itemStoreMyRequestsTxtDetails.text = item.orderStatusDescription
            viewDataBinding.itemStoreMyRequestsTxtDetails.setTextColor(Color.parseColor(item.statusColor))
        }

        item.category?.let {
            viewDataBinding.itemStoreMyRequestsImg.setImageResource(iconSelect(it))
        }

        viewDataBinding.setVariable(BR.storeOrderResponseUiModel, item)
        viewDataBinding.setVariable(BR.storeMyRequestsViewHolder, this@StoreMyRequestsViewHolder)
        viewDataBinding.executePendingBindings()
    }

    private fun iconSelect(category: ProductCategory) = when (category) {
        ProductCategory.MOBILITY -> br.com.digio.uber.common.R.drawable.ic_tags_veloe
        ProductCategory.RECHARGE -> br.com.digio.uber.common.R.drawable.ic_phone
        else -> R.drawable.ic_phone
    }
}