package br.com.digio.uber.store.model

import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.model.store.OrderStatusEnum
import br.com.digio.uber.model.store.PartnerRequestStatusEnum
import br.com.digio.uber.model.store.PaymentRequestStatusEnum
import br.com.digio.uber.model.store.ProductCategory
import br.com.digio.uber.model.store.StoreOrderhistory

sealed class StoreOrderResponseItemUiModel : AdapterViewModel<StoreOrderResponseItemUiModel> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<StoreOrderResponseItemUiModel>) = typesFactory.type(
        model = this
    )
}

data class StoreOrderResponseUiModel(
    val id: Long,
    val customerId: Long,
    val customerDocument: String? = null,
    val value: Float? = null,
    val installments: Int? = null,
    val quantity: Int? = null,
    val orderStatusDescription: String? = null,
    val orderStatus: OrderStatusEnum? = null,
    val orderCode: String? = null,
    val paymentStatus: PaymentRequestStatusEnum? = null,
    val partnerStatus: PartnerRequestStatusEnum? = null,
    val history: List<StoreOrderhistory> = listOf(),
    val inputDate: String? = null,
    val lastUpdatedDate: String? = null,
    val category: ProductCategory? = null,
    val orderDetail: Map<String, String>? = null,
    val usageInstructions: String? = null,
    val activationCode: String? = null,
    val shareText: String? = null,
    val message: String? = null,
    val statusColor: String? = null
) : StoreOrderResponseItemUiModel()