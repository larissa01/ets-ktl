package br.com.digio.uber.store.di

import br.com.digio.uber.store.viewmodel.StoreMyRequestsViewModel
import br.com.digio.uber.store.viewmodel.StoreViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val storeViewModelModule = module {
    viewModel { StoreViewModel(get(), get(), get(), get()) }
    viewModel { StoreMyRequestsViewModel(get(), get()) }
}