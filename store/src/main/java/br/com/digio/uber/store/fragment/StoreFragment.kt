package br.com.digio.uber.store.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.component.CirclePagerIndicatorDecoration
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.store.ProductCategory
import br.com.digio.uber.model.store.StoreProduct
import br.com.digio.uber.store.BR
import br.com.digio.uber.store.R
import br.com.digio.uber.store.adapter.StoreProductAdapter
import br.com.digio.uber.store.adapter.StoreSpotlightAdapter
import br.com.digio.uber.store.adapter.StoresCashbackAdapter
import br.com.digio.uber.store.databinding.FragmentStoreBinding
import br.com.digio.uber.store.viewmodel.StoreViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.enums.CallCashbackFragment
import com.google.android.material.appbar.AppBarLayout
import org.koin.android.ext.android.inject

class StoreFragment : BaseViewModelFragment<FragmentStoreBinding, StoreViewModel>() {

    override val bindingVariable: Int? = BR.storeViewModel
    override val getLayoutId: Int? = R.layout.fragment_store
    override val viewModel: StoreViewModel by inject()
    override fun tag(): String = StoreFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.GREY

    override fun initialize() {
        super.initialize()

        setAdapters()

        setOffsetListener()

        viewModel.initializer { onItemClicked(it) }
    }

    private fun setOffsetListener() {
        val listener = AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
            val seekPosition = -verticalOffset / (binding?.storeHomeAppBar?.totalScrollRange?.toFloat() ?: 1f)
            binding?.storeHomeMotion?.progress = seekPosition
        }

        binding?.storeHomeAppBar?.addOnOffsetChangedListener(listener)
    }

    private fun setAdapters() {
        binding?.storeHomeRvServices?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = StoreProductAdapter(
                listener = { value ->
                    call(value)
                }
            )
        }

        val snapHelper = PagerSnapHelper()
        binding?.storeHomeRvHighlights?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            addItemDecoration(CirclePagerIndicatorDecoration(Color.GRAY, Color.BLACK))
            snapHelper.attachToRecyclerView(this)
            adapter = StoreSpotlightAdapter(
                listener = { value ->
                    call(value)
                }
            )
        }

        binding?.storeHomeRvMoreCashback?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = StoresCashbackAdapter() { product, last ->
                if (last) {
                    goToCashbackActivity(
                        Bundle().apply {
                            putString(
                                Const.RequestOnResult.Cashback.CALL_CASHBACK_FRAGMENT,
                                CallCashbackFragment.ALL_STORES.name
                            )
                            putInt(Const.RequestOnResult.Cashback.TOTAL_ELEMENTS, viewModel.totalElements.value ?: 0)
                        }
                    )
                } else {
                    goToCashbackActivity(
                        Bundle().apply {
                            putString(
                                Const.RequestOnResult.Cashback.CALL_CASHBACK_FRAGMENT,
                                CallCashbackFragment.CASHBACK_STORE.name
                            )
                            putSerializable(Const.RequestOnResult.Cashback.STORE, product)
                        }
                    )
                }
            }
        }
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.store_home_img_my_requests -> activity?.let { navigation.navigationToStoreMyRequestsActivity(it) }
            R.id.store_home_btn_see_cashback -> goToCashbackActivity(
                Bundle().apply {
                    putString(
                        Const.RequestOnResult.Cashback.CALL_CASHBACK_FRAGMENT,
                        CallCashbackFragment.MY_CASHBACK.name
                    )
                }
            )
        }
    }

    private fun goToCashbackActivity(bundle: Bundle?) {
        activity?.let { activitLet ->
            navigation.navigationToCashbackActivity(activitLet, bundle)
        }
    }

    private fun call(product: StoreProduct) {
        when (product.category) {
            ProductCategory.RECHARGE -> activity?.let { activityLet ->
                navigation.navigationToRechargeActivity(activityLet, product)
            }

            ProductCategory.MOBILITY -> activity?.let { activityLet ->
                navigation.navigationToVeloeActivity(activityLet, product)
            }

            ProductCategory.CASHBACK -> activity?.let { activityLet ->
                navigation.navigationToCashbackActivity(activityLet, null)
            }

            else -> activity?.let { activityLet ->
                navigation.navigationToRechargeActivity(activityLet, product)
            }
        }
    }
}