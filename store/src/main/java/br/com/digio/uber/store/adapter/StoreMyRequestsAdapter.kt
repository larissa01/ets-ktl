package br.com.digio.uber.store.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.store.R
import br.com.digio.uber.store.adapter.viewholder.StoreMyRequestsViewHolder
import br.com.digio.uber.store.databinding.ItemStoreMyRequestsBinding
import br.com.digio.uber.store.model.StoreOrderResponseItemUiModel
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

typealias OnClickOrderItem = (item: StoreOrderResponseItemUiModel) -> Unit

class StoreMyRequestsAdapter(
    private var values: MutableList<StoreOrderResponseItemUiModel> = mutableListOf(),
    private val listener: OnClickOrderItem
) : BaseRecyclerViewAdapter<StoreOrderResponseItemUiModel, StoreMyRequestsAdapter.ViewTypesDataBindingFactoryImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl =
        ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): StoreOrderResponseItemUiModel =
        values[position]

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<StoreOrderResponseItemUiModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<StoreOrderResponseItemUiModel>
    }

    override fun getItemCount(): Int = values.size

    override fun onBindViewHolder(holder: AbstractViewHolder<StoreOrderResponseItemUiModel>, position: Int) =
        holder.bind(values[holder.adapterPosition])

    override fun replaceItems(list: List<Any>) {
        addValues(list.safeHeritage())
    }

    private fun addValues(values: List<StoreOrderResponseItemUiModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }

    class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<StoreOrderResponseItemUiModel> {
        override fun type(model: StoreOrderResponseItemUiModel): Int = R.layout.item_store_my_requests

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<StoreOrderResponseItemUiModel>
        ): AbstractViewHolder<*> =
            StoreMyRequestsViewHolder(
                view.safeHeritage<ItemStoreMyRequestsBinding>()!!,
                listener
            )
    }
}