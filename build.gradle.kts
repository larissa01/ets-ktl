import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask

allprojects {
    repositories {
        google()
        jcenter()
        flatDir {
            dirs(
                project(":domain").file("libs"),
                project(":gemalto").file("libs")
            )
        }
        maven {
            url = uri("https://salesforce-marketingcloud.github.io/MarketingCloudSDK-Android/repository")
        }
    }
}

plugins {
    id("io.gitlab.arturbosch.detekt") version LibVersions.DETEKT
    id("org.jlleitschuh.gradle.ktlint") version LibVersions.KTLINT_PLUGIN
    id("com.github.ben-manes.versions") version LibVersions.BEN_MANES_DEPENDENCY_VERSIONS
}

buildscript {
    val dexguardMavenUser: String by project
    val dexguardMavenPassword: String by project

    repositories {
        maven {
            url = uri("https://maven.guardsquare.com")
            credentials {
                username = dexguardMavenUser
                password = dexguardMavenPassword
            }
            content {
                includeGroupByRegex("com\\.guardsquare.*")
            }
            authentication {
                create("basic", BasicAuthentication::class.java)
            }
        }
        google()
    }

    dependencies {
        classpath("android.arch.navigation:navigation-safe-args-gradle-plugin:${LibVersions.NAVIGATION_ARGS}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${LibVersions.KOTLIN_VERSION}")
        classpath("com.github.ben-manes:gradle-versions-plugin:${LibVersions.BEN_MANES_DEPENDENCY_VERSIONS}")
        classpath("com.google.gms:google-services:${LibVersions.GOOGLE_SERVICES_VERSION}")
        classpath("com.google.firebase:firebase-crashlytics-gradle:${LibVersions.CRASHLYTICS_PLUGIN_VERSION}")
        classpath("com.guardsquare:dexguard-gradle-plugin:${LibVersions.DEXGUARD_GRADLE_VERSION}")
        classpath("com.android.tools.build:gradle:${LibVersions.AGP}")
    }
}

subprojects {

    apply {
        plugin("io.gitlab.arturbosch.detekt")
        plugin("org.jlleitschuh.gradle.ktlint")
    }

    ktlint {
        debug.set(false)
        version.set(LibVersions.KTLINT_VERSION)
        verbose.set(true)
        android.set(false)
        outputToConsole.set(true)
        ignoreFailures.set(false)
        enableExperimentalRules.set(true)
        filter {
            exclude("**/generated/**")
            exclude("**/pix/**")
            include("**/kotlin/**")
        }
    }

    detekt {
        config = rootProject.files("config/detekt/detekt.yml")
        reports {
            html {
                enabled = true
                destination = file("build/reports/detekt.html")
            }
        }
    }

    tasks.withType<io.gitlab.arturbosch.detekt.Detekt>().configureEach {
        exclude("**/pix/**")
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

tasks.withType<DependencyUpdatesTask> {
    rejectVersionIf {
        isNonStable(candidate.version)
    }
}

fun isNonStable(version: String) = "^[0-9,.v-]+(-r)?$".toRegex().matches(version).not()