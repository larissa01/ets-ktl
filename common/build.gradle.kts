/* Apply Module base configurations */
plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.library")
}

dependencies {
    api(Dependencies.SDP)
    api(Dependencies.SSP)

    kapt(Dependencies.GLIDE_COMPILER)

    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.ANDROIDXCORE)
    implementation(Dependencies.ACTIVITYKTX)
    implementation(Dependencies.LIFECYCLE_LIVEDATA)
    implementation(Dependencies.LIFECYCLE_EXTENSIONS)
    implementation(Dependencies.LIFECYCLE_VIEWMODEL)
    implementation(Dependencies.LIFECYCLE_RUNTIME)
    implementation(Dependencies.LIFECYCLE_RUNTIME_KTX)
    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.FRAGMENTKTX)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.KOIN)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)
    implementation(Dependencies.LOCAL_BCAST_MANAGER)
    implementation(Dependencies.LOTTIE)
    implementation(Dependencies.GLIDE)
    implementation(Dependencies.NAV_FRAGMENT)
    implementation(Dependencies.NAV_UI)
    implementation(Dependencies.SWIPE_REFRESH_LAYOUT)

    implementation(project(":util"))
    implementation(project(":domain"))
    implementation(project(":model"))

    testImplementation(TestDependencies.JUNIT)
    testImplementation(TestDependencies.MOCKK)
    testImplementation(TestDependencies.COROUTINESTEST)
    testImplementation(TestDependencies.ANDROIDX_CORE_TESTING)
    testImplementation(project(":repository"))

    // PAYMENT MODULE DEPENDENCIES
    implementation(Dependencies.PERMISSIONS_DISPATCHER)
    implementation(Dependencies.ZXING_ANDROID_EMBEDDED)
    implementation(Dependencies.ZXING_ANDROID_EMBEDDED_OLD)
}