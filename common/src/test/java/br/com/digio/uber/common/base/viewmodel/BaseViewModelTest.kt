package br.com.digio.uber.common.base.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import br.com.digio.uber.common.base.viewmodel.abstract.usecase.sample.MockUseCase
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.generics.MessageGenericObject
import br.com.digio.uber.common.typealiases.OnCompletionBaseViewModel
import br.com.digio.uber.common.typealiases.OnErrorBaseViewModel
import br.com.digio.uber.common.typealiases.OnSuccessBaseViewModel
import br.com.digio.uber.domain.Error
import br.com.digio.uber.domain.ResultWrapper
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class BaseViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val mockUseCase: MockUseCase = spyk(MockUseCase())

    @ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @ObsoleteCoroutinesApi
    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @ObsoleteCoroutinesApi
    @ExperimentalCoroutinesApi
    @After
    fun after() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `on exec and get simple livedata`() = runBlocking {
        val mockResult = true
        val onError: OnErrorBaseViewModel = { statusCode, error, throwable ->
            assertNull(statusCode)
            assertNull(throwable)
            error
        }
        val onSuccessBaseViewModel: OnSuccessBaseViewModel<Boolean> = {
            assertEquals(mockResult, it)
        }
        val onCompletionBaseViewModel: OnCompletionBaseViewModel = { /*nothing*/ }
        val showLoader = true

        val observableShowLoading: Observer<Boolean> = mockk(relaxed = true)
        val observableShowError: Observer<MessageGenericObject> = mockk(relaxed = true)
        val mockObserverForResult: Observer<Boolean> = mockk(relaxed = true)

        val mockViewModel = MockBaseViewModel(
            mockResult,
            mockUseCase,
            onError,
            onSuccessBaseViewModel,
            onCompletionBaseViewModel,
            showLoader
        )

        mockViewModel.showLoading.observeForever(observableShowLoading)
        mockViewModel.message.observeForever(observableShowError)
        mockViewModel.liveDataTest.observeForever(mockObserverForResult)

        delay(3000)

        verify(exactly = 0) { observableShowError.onChanged(any()) }
        verify(exactly = 3) { observableShowLoading.onChanged(any()) }
        verify(exactly = 1) { mockObserverForResult.onChanged(mockResult) }
    }

    @Test
    fun `on exec and get simple livedata without loading`() = runBlocking {
        val mockResult = true
        val onError: OnErrorBaseViewModel = { statusCode, error, throwable ->
            assertNull(statusCode)
            assertNull(throwable)
            error
        }
        val onSuccessBaseViewModel: OnSuccessBaseViewModel<Boolean> = {
            assertEquals(mockResult, it)
        }
        val onCompletionBaseViewModel: OnCompletionBaseViewModel = { /*nothing*/ }
        val showLoader = false

        val observableShowLoading: Observer<Boolean> = mockk(relaxed = true)
        val observableShowError: Observer<MessageGenericObject> = mockk(relaxed = true)
        val mockObserverForResult: Observer<Boolean> = mockk(relaxed = true)

        val mockViewModel = MockBaseViewModel(
            mockResult,
            mockUseCase,
            onError,
            onSuccessBaseViewModel,
            onCompletionBaseViewModel,
            showLoader
        )

        mockViewModel.showLoading.observeForever(observableShowLoading)
        mockViewModel.message.observeForever(observableShowError)
        mockViewModel.liveDataTest.observeForever(mockObserverForResult)

        delay(3000)

        verify(exactly = 0) { observableShowError.onChanged(any()) }
        verify(exactly = 1) { observableShowLoading.onChanged(eq(false)) }
        verify(exactly = 1) { mockObserverForResult.onChanged(eq(mockResult)) }
    }

    @Test
    fun `on exec and get simple livedata and return erro`() = runBlocking {
        val mockResult = true
        val exception = IllegalStateException("Opss")
        val error = ResultWrapper.Failure(Error.UnknownException(exception))

        val flowMock: Flow<ResultWrapper<Boolean>> = flow {
            emit(ResultWrapper.Loading)
            emit(error)
            emit(ResultWrapper.DismissLoading)
        }

        every { mockUseCase(mockResult) } returns flowMock

        val onError: OnErrorBaseViewModel = { statusCode, errorBaseVM, throwable ->
            assertNull(statusCode)
            assertNotNull(throwable)
            errorBaseVM
        }
        val onSuccessBaseViewModel: OnSuccessBaseViewModel<Boolean> = {
            assertNotEquals(mockResult, it)
        }
        val onCompletionBaseViewModel: OnCompletionBaseViewModel = { /*nothing*/ }
        val showLoader = true

        val observableShowLoading: Observer<Boolean> = mockk(relaxed = true)
        val observableShowError: Observer<MessageGenericObject> = mockk(relaxed = true)
        val mockObserverForResult: Observer<Boolean> = mockk(relaxed = true)

        val mockViewModel = MockBaseViewModel(
            mockResult,
            mockUseCase,
            onError,
            onSuccessBaseViewModel,
            onCompletionBaseViewModel,
            showLoader
        )

        mockViewModel.showLoading.observeForever(observableShowLoading)
        mockViewModel.message.observeForever(observableShowError)
        mockViewModel.liveDataTest.observeForever(mockObserverForResult)

        delay(3000)

        verify(exactly = 1) { observableShowError.onChanged(any()) }
        verify(exactly = 3) { observableShowLoading.onChanged(any()) }
        verify(exactly = 0) { mockObserverForResult.onChanged(mockResult) }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `on exec and get simple singleExec`() = runBlocking {
        val mockResult = true
        val onError: OnErrorBaseViewModel = mockk(relaxed = true)
        val onSuccessBaseViewModel: OnSuccessBaseViewModel<Boolean> = mockk(relaxed = true)
        val onCompletionBaseViewModel: OnCompletionBaseViewModel = mockk(relaxed = true)
        val showLoader = true

        val observableShowLoading: Observer<Boolean> = mockk(relaxed = true)
        val observableShowError: Observer<MessageGenericObject> = mockk(relaxed = true)

        val mockViewModel = MockBaseViewModel(
            mockResult,
            mockUseCase,
            onError,
            onSuccessBaseViewModel,
            onCompletionBaseViewModel,
            showLoader
        )

        mockViewModel.showLoading.observeForever(observableShowLoading)
        mockViewModel.message.observeForever(observableShowError)

        mockViewModel.singleExecMock()

        delay(3000)

        coVerify(exactly = 0) { onError.invoke(any(), any(), any()) }
        coVerify(exactly = 1) { onSuccessBaseViewModel(mockResult) }
        coVerify(exactly = 1) { onCompletionBaseViewModel() }

        verify(exactly = 0) { observableShowError.onChanged(any()) }
        verify(exactly = 3) { observableShowLoading.onChanged(any()) }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `on exec and get simple singleExec and return error`() = runBlocking {
        val mockResult = true
        val exception = IllegalStateException("Opss")
        val error = ResultWrapper.Failure(Error.UnknownException(exception))
        val errorObject = ErrorObject(exception)

        val flowMock: Flow<ResultWrapper<Boolean>> = flow {
            emit(ResultWrapper.Loading)
            emit(error)
            emit(ResultWrapper.DismissLoading)
        }

        every { mockUseCase(mockResult) } returns flowMock

        val onError: OnErrorBaseViewModel = mockk(relaxed = true)

        coEvery { onError(null, any(), exception) } returns errorObject

        val onSuccessBaseViewModel: OnSuccessBaseViewModel<Boolean> = mockk(relaxed = true)
        val onCompletionBaseViewModel: OnCompletionBaseViewModel = mockk(relaxed = true)
        val showLoader = true

        val observableShowLoading: Observer<Boolean> = mockk(relaxed = true)
        val observableShowError: Observer<MessageGenericObject> = mockk(relaxed = true)

        val mockViewModel = MockBaseViewModel(
            mockResult,
            mockUseCase,
            onError,
            onSuccessBaseViewModel,
            onCompletionBaseViewModel,
            showLoader
        )

        mockViewModel.showLoading.observeForever(observableShowLoading)
        mockViewModel.message.observeForever(observableShowError)

        mockViewModel.singleExecMock()

        delay(3000)

        coVerify(exactly = 1) { onError.invoke(any(), any(), any()) }
        coVerify(exactly = 0) { onSuccessBaseViewModel(mockResult) }
        coVerify(exactly = 1) { onCompletionBaseViewModel() }

        verify(exactly = 1) { observableShowError.onChanged(any()) }
        verify(exactly = 3) { observableShowLoading.onChanged(any()) }
    }
}