package br.com.digio.uber.common.base.viewmodel

import androidx.lifecycle.LiveData
import br.com.digio.uber.common.base.viewmodel.abstract.usecase.sample.MockUseCase
import br.com.digio.uber.common.typealiases.OnCompletionBaseViewModel
import br.com.digio.uber.common.typealiases.OnErrorBaseViewModel
import br.com.digio.uber.common.typealiases.OnSuccessBaseViewModel

class MockBaseViewModel constructor(
    private val mockReturnValue: Boolean,
    private val mockUseCase: MockUseCase,
    private val onError: OnErrorBaseViewModel = { _, err, _ -> err },
    private val onSuccessBaseViewModel: OnSuccessBaseViewModel<Boolean>? = null,
    private val onCompletionBaseViewModel: OnCompletionBaseViewModel? = null,
    private val showLoadingFlag: Boolean = true
) : BaseViewModel() {

    val liveDataTest: LiveData<Boolean> = mockUseCase(mockReturnValue).exec(
        onError = onError,
        showLoadingFlag = showLoadingFlag
    )

    fun singleExecMock() {
        mockUseCase(mockReturnValue).singleExec(
            onError = onError,
            onSuccessBaseViewModel = onSuccessBaseViewModel,
            onCompletionBaseViewModel = onCompletionBaseViewModel,
            showLoadingFlag = showLoadingFlag
        )
    }
}