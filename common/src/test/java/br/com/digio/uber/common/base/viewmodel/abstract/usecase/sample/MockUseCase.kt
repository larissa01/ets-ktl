package br.com.digio.uber.common.base.viewmodel.abstract.usecase.sample

import br.com.digio.uber.domain.AbstractUseCase
import kotlinx.coroutines.delay

class MockUseCase : AbstractUseCase<Boolean, Boolean>() {
    override suspend fun execute(param: Boolean): Boolean {
        delay(1000)
        return param
    }
}