package br.com.digio.uber.common.base.activity

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.extensions.associateViewModel
import br.com.digio.uber.domain.usecase.login.LogoutUseCase
import org.koin.android.ext.android.inject

abstract class BaseViewModelActivity<T : ViewDataBinding, VM : BaseViewModel> : CoreBaseActivity() {

    abstract val bindingVariable: Int?

    abstract val viewModel: VM?

    private val logoutUseCase: LogoutUseCase by inject()

    @LayoutRes
    abstract fun getLayoutId(): Int?

    val binding: T? by lazy {
        getLayoutId()?.let { layoutId ->
            DataBindingUtil.setContentView<T>(
                this,
                layoutId
            )
        }
    }

    override fun initialize(savedInstanceState: Bundle?) {
        associateViewModel(logoutUseCase)
    }
}