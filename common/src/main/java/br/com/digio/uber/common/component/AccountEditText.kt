package br.com.digio.uber.common.component

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import br.com.digio.uber.common.R
import com.google.android.material.textfield.TextInputEditText

open class AccountEditText : TextInputEditText {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    init {
        backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.transparent))
    }
}