package br.com.digio.uber.common.extensions

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.databinding.BindingAdapter
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigator
import br.com.digio.uber.common.R
import br.com.digio.uber.util.Const.Utils.DELAY_MILLISECOND
import br.com.digio.uber.util.Const.Utils.MILLIS_COUNTDOWN
import br.com.digio.uber.util.safeHeritage
import timber.log.Timber

fun View.hide() {
    if (visibility != View.GONE) {
        visibility = View.GONE
    }
}

fun View.show() {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
}

fun View.invisible() {
    if (visibility != View.INVISIBLE) {
        visibility = View.INVISIBLE
    }
}

fun View.visible() {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
}

fun View.isVisible() = this.visibility == View.VISIBLE
fun View.isInvisible() = this.visibility == View.INVISIBLE
fun View.isHide() = this.visibility == View.GONE

fun View.visibleAnimation(visibility: Float, speed: Long) {
    val view = this
    view.animate()
        .alpha(visibility)
        .setDuration(speed)
        .setListener(
            object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    view.visibility = if (visibility == 0f) View.GONE else View.VISIBLE
                }
            }
        )
}

fun booleanToViewVisibleOrGone(visibility: Boolean): Int =
    if (visibility) View.VISIBLE else View.GONE

@BindingAdapter("hide")
fun setHide(view: View, visible: Boolean) = if (visible) view.show() else view.hide()

@BindingAdapter("visible")
fun setVisible(view: View, visible: Boolean) = if (visible) view.visible() else view.invisible()

@BindingAdapter("setBackgroundColorForCircler")
fun setBackgroundColorForCircler(view: View, colorString: String) {
    view.background?.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
        Color.parseColor(colorString),
        BlendModeCompat.SRC_ATOP
    )
}

@BindingAdapter("setMarginTopByConstraintLayoutView")
fun setMarginTopByConstraintLayoutView(view: View, margin: Int) {
    view.layoutParams?.safeHeritage<ConstraintLayout.LayoutParams>()?.apply {
        topMargin = margin
    }
}

@BindingAdapter("elevationIfSupport")
fun setElevationIfSupport(view: View, elevationNumber: Int) {
    view.elevation = elevationNumber.toFloat()
}

@BindingAdapter("marginTop")
fun View.setMarginTop(margin: Float) {
    setMarginCustom(top = margin.toInt())
}

@BindingAdapter("marginLeft")
fun View.setMarginLeft(margin: Float) {
    setMarginCustom(left = margin.toInt())
}

@BindingAdapter("marginRight")
fun View.setMarginRight(margin: Float) {
    setMarginCustom(right = margin.toInt())
}

@BindingAdapter("marginBottom")
fun View.setMarginBottom(margin: Float) {
    setMarginCustom(bottom = margin.toInt())
}

fun View.setMarginCustom(top: Int? = null, left: Int? = null, bottom: Int? = null, right: Int? = null) {
    val menuLayoutParams = this.layoutParams.safeHeritage<ViewGroup.MarginLayoutParams>()
    menuLayoutParams?.setMarginCustom(
        left = left,
        top = top,
        right = right,
        bottom = bottom
    )
    if (menuLayoutParams != null) {
        layoutParams = menuLayoutParams
    }
}

fun ViewGroup.MarginLayoutParams.setMarginCustom(
    top: Int? = null,
    left: Int? = null,
    bottom: Int? = null,
    right: Int? = null
) {
    setMargins(
        left ?: leftMargin,
        top ?: topMargin,
        right ?: rightMargin,
        bottom ?: bottomMargin
    )
}

@BindingAdapter("layout_height")
fun View.setLayoutHeight(height: Float) {
    layoutParams = layoutParams.apply {
        layoutParams.height = height.toInt()
    }
}

@BindingAdapter("layout_height")
fun View.setHeight(height: Int) {
    layoutParams.height = height
    layoutParams = layoutParams
}

@BindingAdapter("layout_width")
fun View.setWidth(width: Int) {
    layoutParams.width = width
    layoutParams = layoutParams
}

@BindingAdapter("background")
fun View.setBackground(defaultColor: Int) {
    val attrs = intArrayOf(defaultColor)
    val ta: TypedArray = context.obtainStyledAttributes(attrs)
    background = ta.getDrawable(0)
    ta.recycle()
}

@BindingAdapter("imageResource")
fun setImageResource(imageView: ImageView, resource: Int) {
    imageView.setImageResource(resource)
}

@BindingAdapter("visibleGone")
fun View.visibleGone(isVisible: Boolean) {
    setHide(this, isVisible)
}

fun SwitchCompat.setupToggle(currentState: Boolean, action: (Boolean) -> Unit) {
    isChecked = currentState
    setOnCheckedChangeListener { _, isChecked ->
        action(isChecked)
    }
}

// Account

@Keep
fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun View.showKeyboard() {
    val imm =
        context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun View.getActivityLifecycleOwner(): AppCompatActivity? =
    this.context?.safeHeritage<AppCompatActivity>()

const val FULL_ALPHA = 1F
const val HALF_ALPHA = 0.5f

fun View.setEnableWithAlpha(enabled: Boolean) {
    isEnabled = enabled
    alpha = if (enabled) FULL_ALPHA
    else HALF_ALPHA
}

@BindingAdapter("isSelected")
fun View.setViewSelected(isSelected: Boolean?) {
    this.isSelected = isSelected == true
}

fun View.navigateTo(direction: NavDirections) {
    try {
        findNavController().navigate(direction)
    } catch (e: IllegalArgumentException) {
        Timber.e(e)
    }
}

fun View.navigateWithTransition(
    direction: NavDirections,
    transitionExtras: FragmentNavigator.Extras
) {
    try {
        findNavController().navigate(direction, transitionExtras)
    } catch (e: IllegalArgumentException) {
        Timber.e(e)
    }
}

fun View.pop() {
    try {
        findNavController().navigateUp()
    } catch (e: IllegalArgumentException) {
        Timber.e(e)
    }
}

@SuppressLint("InflateParams")
@BindingAdapter("tooltipCompatText")
fun View.setTooltipCompat(text: String?) {
    if (text.isNullOrBlank()) return

    val layoutInflater = LayoutInflater.from(context)

    val popup = PopupWindow(context).apply {
        contentView = layoutInflater.inflate(R.layout.layout_tooltip, null)
        width = LinearLayout.LayoutParams.WRAP_CONTENT
        height = LinearLayout.LayoutParams.WRAP_CONTENT
        isFocusable = true
        setBackgroundDrawable(
            ColorDrawable(ContextCompat.getColor(context, R.color.transparent))
        )
        animationStyle = R.style.Animation_Dialog_Fade

        contentView.findViewById<TextView>(R.id.textView_tooltip).text = text
    }

    // Timer to dismiss like Tooltip
    val timer = object : CountDownTimer(MILLIS_COUNTDOWN, DELAY_MILLISECOND) {
        override fun onTick(p0: Long) {
            Timber.i("tick")
        }

        override fun onFinish() {
            popup.dismiss()
        }
    }

    setOnTouchListener { _, motionEvent ->
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                popup.showAsDropDown(this)
                true
            }
            MotionEvent.ACTION_CANCEL,
            MotionEvent.ACTION_UP -> {
                timer.start()
                true
            }
            else -> false
        }
    }
}

@BindingAdapter("enable")
fun ViewGroup.setEnable(enable: Boolean) {
    isEnabled = enable
}