package br.com.digio.uber.common.androidUtils

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import br.com.digio.uber.util.safeLet
import java.util.ArrayList
import java.util.HashMap

object PermissionUtils {
    private val PERMISSION_INTENT_FILTER_KEY =
        PermissionUtils::class.java.name + "PERMISSION_REQUEST_RESULT_BROADCAST_FILTER"
    private val PERMISSION_INTENT_FILTER = IntentFilter(PERMISSION_INTENT_FILTER_KEY)
    private val PERMISSION_RESULT_PERMISSIONS_EXTRA =
        PermissionUtils::class.java.name + "PERMISSION_RESULT_PERMISSIONS_EXTRA"
    private val PERMISSION_RESULT_GRANTS_EXTRA =
        PermissionUtils::class.java.name + "PERMISSION_RESULT_GRANTS_EXTRA"
    private val PERMISSION_OBSERVERS: MutableMap<PermissionRequestObserver, BroadcastReceiver> =
        HashMap()

    fun Context.hasPermission(permission: String): Boolean {
        val hasPermission = ContextCompat.checkSelfPermission(this, permission)
        return hasPermission == PackageManager.PERMISSION_GRANTED
    }

    fun Context.hasPhonePermission(): Boolean {
        return hasPermission(Manifest.permission.READ_PHONE_STATE)
    }

    /**
     * Register a broadcast receiver that will be notified each time a permission request is answered
     * @param context Any instance of context
     * @param observer The broadcast receiver that will be notified
     */
    private fun Context.registerPermissionObserver(observer: PermissionRequestObserver) {
        // Unregister any other broadcast using the same name
        applicationContext.unregisterPermissionObserver(observer)
        val receiver: BroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val permissions = intent.getStringArrayExtra(PERMISSION_RESULT_PERMISSIONS_EXTRA)
                val results = intent.getIntArrayExtra(PERMISSION_RESULT_GRANTS_EXTRA)
                val permissionResults: MutableList<PermissionResult> = ArrayList()
                safeLet(permissions, results) { permissionsLet, resultsLet ->
                    for (i in permissionsLet.indices)
                        permissionResults.add(
                            PermissionResult(permissionsLet[i], resultsLet[i])
                        )
                }
                observer.onPermissionRequestResult(permissionResults)
            }
        }
        PERMISSION_OBSERVERS[observer] = receiver
        LocalBroadcastManager.getInstance(applicationContext)
            .registerReceiver(receiver, PERMISSION_INTENT_FILTER)
    }

    /**
     * Unregister the given broadcast receiver
     * @param context Any instance of context
     * @param observer The previously registered broadcast receiver
     */
    private fun Context.unregisterPermissionObserver(
        observer: PermissionRequestObserver
    ) {
        val broadcastReceiver = PERMISSION_OBSERVERS[observer] ?: return
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver)
        PERMISSION_OBSERVERS.remove(observer)
    }

    /**
     * Dispatch the permission results to all registered broadcast receivers
     */
    private fun Context.notifyPermissionResult(
        permissions: Array<String>,
        results: IntArray
    ) {
        val intent = Intent(PERMISSION_INTENT_FILTER_KEY)
        intent.putExtra(PERMISSION_RESULT_PERMISSIONS_EXTRA, permissions)
        intent.putExtra(PERMISSION_RESULT_GRANTS_EXTRA, results)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    val apiInstance: PermissionRequestApi
        get() = object : PermissionRequestApi {
            override fun registerPermissionObserver(
                context: Context,
                observer: PermissionRequestObserver
            ) {
                context.registerPermissionObserver(observer)
            }

            override fun unregisterPermissionObserver(
                context: Context,
                observer: PermissionRequestObserver
            ) {
                context.unregisterPermissionObserver(observer)
            }

            override fun notifyPermissionResult(
                context: Context,
                permissions: Array<String>,
                results: IntArray
            ) {
                context.notifyPermissionResult(permissions, results)
            }

            override fun requestPermissions(
                activity: Activity,
                permissions: Array<String>,
                requestCode: Int
            ) {
                ActivityCompat.requestPermissions(activity, permissions, requestCode)
            }

            override fun requestPermissions(
                fragment: Fragment,
                permissions: Array<String>,
                requestCode: Int
            ) {
                fragment.requestPermissions(permissions, requestCode)
            }
        }

    interface PermissionRequestApi {
        fun registerPermissionObserver(context: Context, observer: PermissionRequestObserver)
        fun unregisterPermissionObserver(context: Context, observer: PermissionRequestObserver)
        fun notifyPermissionResult(context: Context, permissions: Array<String>, results: IntArray)
        fun requestPermissions(activity: Activity, permissions: Array<String>, requestCode: Int)
        fun requestPermissions(fragment: Fragment, permissions: Array<String>, requestCode: Int)
    }

    interface PermissionRequestObserver {
        fun onPermissionRequestResult(results: List<PermissionResult>)
    }

    class PermissionResult(val permission: String, val result: Int) {
        val isGranted: Boolean
            get() = result == PackageManager.PERMISSION_GRANTED
    }
}