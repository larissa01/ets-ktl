package br.com.digio.uber.common.base.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.androidUtils.PermissionUtils
import br.com.digio.uber.common.generics.MessageGenericObjectConfig
import br.com.digio.uber.common.navigation.GenericNavigation
import br.com.digio.uber.common.navigation.Navigation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.koin.android.ext.android.inject
import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module
import kotlin.coroutines.CoroutineContext

abstract class CoreBaseActivity :
    AppCompatActivity(),
    CoroutineScope,
    MessageGenericObjectConfig {

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    abstract fun initialize(savedInstanceState: Bundle?)

    open fun getNavigation(): GenericNavigation? = null

    val navigation: Navigation by inject()

    private fun bindToolbar() {
        getToolbar()?.let { toolbar ->
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(showHomeAsUp())
            supportActionBar?.setDisplayShowTitleEnabled(showDisplayShowTitle())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectFeature()
        super.onCreate(savedInstanceState)

        initialize(savedInstanceState)
        bindToolbar()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        getNavigation()?.onActivityResult(requestCode, resultCode, data)
            ?: super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        getNavigation()?.onBackPressed() ?: super.onBackPressed()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionUtils.apiInstance.notifyPermissionResult(this, permissions, grantResults)
    }

    open fun getToolbar(): Toolbar? = null

    open fun showHomeAsUp(): Boolean = true

    open fun showDisplayShowTitle(): Boolean = false

    override fun getActivityForError(): CoreBaseActivity? = this

    open fun getModule(): List<Module>? = null

    private val loadFeature by lazy {
        getModule()?.let { modulesList ->
            loadKoinModules(modulesList)
            modulesList
        }
    }

    private fun injectFeature() = loadFeature
}