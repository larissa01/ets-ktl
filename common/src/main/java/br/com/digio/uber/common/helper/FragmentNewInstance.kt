package br.com.digio.uber.common.helper

import android.os.Bundle
import br.com.digio.uber.common.base.fragment.BaseNavigationWithFlowFragment
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow

open class FragmentNewInstance<T : BaseNavigationWithFlowFragment>(val creator: () -> T) {
    fun newInstance(navigation: GenericNavigationWithFlow, bundle: Bundle?): T =
        creator().apply {
            this.navigationListener = navigation
            arguments = bundle
        }
}