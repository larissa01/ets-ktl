package br.com.digio.uber.common.helper

import android.text.InputFilter
import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils

class EmojiInputFilter : InputFilter {

    override fun filter(
        source: CharSequence?,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        var keepOriginal = true

        val stringBuilder = StringBuilder(end - start)
        source?.let {
            for (i in start until end) {
                val char = it[i]
                val type = Character.getType(char)
                if (type == Character.SURROGATE.toInt() || type == Character.OTHER_SYMBOL.toInt()) return ""

                if (isCharAllowed(char)) stringBuilder.append(char)
                else keepOriginal = false
            }
        }

        val finalResult = if (source is Spanned) {
            val spannableString = SpannableString(stringBuilder)
            TextUtils.copySpansFrom(source, start, stringBuilder.length, null, spannableString, 0)
            spannableString
        } else if (keepOriginal) {
            null
        } else if (source.isNullOrEmpty()) {
            ""
        } else {
            stringBuilder
        }

        return finalResult
    }

    private fun isCharAllowed(char: Char): Boolean {
        return Character.isLetterOrDigit(char) || Character.isSpaceChar(char)
    }
}