package br.com.digio.uber.common.extensions

import android.app.Activity
import androidx.core.content.ContextCompat
import br.com.digio.uber.common.kenum.StatusBarColor

object StatusBarUtils {

    private val listOfStatusBar: MutableList<StatusBarColor> = mutableListOf()

    fun Activity.setColorStatusBar(statusBarColor: StatusBarColor) {
        if (statusBarColor.dark) setDarkStatusTextBar() else setLightStatusTextBar()
        window.statusBarColor = ContextCompat.getColor(this, statusBarColor.color)
        updateList(statusBarColor)
    }

    private fun updateList(statusBarColor: StatusBarColor) {
        listOfStatusBar.add(statusBarColor)
        if (listOfStatusBar.size > 2) {
            listOfStatusBar.removeFirstOrNull()
        }
    }

    fun Activity.returnToOldStatusBar() {
        listOfStatusBar.firstOrNull()?.let { last ->
            setColorStatusBar(last)
        }
    }
}