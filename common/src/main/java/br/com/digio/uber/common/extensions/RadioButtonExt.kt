package br.com.digio.uber.common.extensions

import android.widget.RadioButton
import androidx.databinding.BindingAdapter
import br.com.digio.uber.util.HtmlUtil

@BindingAdapter(value = ["htmlString"], requireAll = true)
fun RadioButton.setHtmlString(resourceResId: Int = 0) {
    if (resourceResId != 0) {
        val resourceString = context.getString(resourceResId)
        text = HtmlUtil.fromHtml(resourceString)
    }
}

@BindingAdapter(value = ["htmlString"], requireAll = true)
fun RadioButton.setHtmlString(resourceString: String? = null) {
    if (resourceString != null) {
        text = HtmlUtil.fromHtml(resourceString)
    }
}