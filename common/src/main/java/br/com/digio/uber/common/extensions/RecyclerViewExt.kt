package br.com.digio.uber.common.extensions

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.util.safeHeritage

fun RecyclerView.isLastIndex(position: Int): Boolean = position == adapter?.itemCount?.minus(1)

@BindingAdapter("items")
fun RecyclerView.setItems(list: List<Any>?) {
    list?.let { listLet ->
        adapter?.safeHeritage<AdapterItemsContract>()?.replaceItems(listLet)
    }
}