package br.com.digio.uber.common.component

import android.animation.ValueAnimator
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.TextPaint
import android.util.AttributeSet
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.FontRes
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.ColorUtils
import androidx.databinding.BindingAdapter
import br.com.digio.uber.common.R
import kotlin.math.max
import kotlin.math.min
import kotlin.math.pow

class EditTextPassword constructor(
    context: Context,
    attr: AttributeSet? = null
) : AppCompatEditText(context, attr) {

    // Default attributes
    @ColorRes
    private val defaultRectBackgroundColorRes: Int = R.color.grey_EEEEEE

    @ColorRes
    private val defaultBorderSelectedRectColorRes: Int = R.color.black

    @ColorRes
    private val defaultTextColorRes: Int = R.color.black

    @ColorRes
    private val defaultHintColorRes: Int = R.color.grey_757575

    @ColorRes
    private val defaultHintErrorColorRes: Int = R.color.red_E11900

    @ColorRes
    private val defaultColorSuccessBackgroundRes: Int = R.color.green_E6F2ED

    @ColorRes
    private val defaultColorBorderSelectedSuccessRes: Int = R.color.green_66D19E

    @ColorRes
    private val defaultColorErrorBackgroundRes: Int = R.color.red_FFEFED

    @ColorRes
    private val defaultColorErrorSelectedSuccessRes: Int = R.color.red_F1998E

    @DimenRes
    private val defaultRectMaxWidthRes: Int = R.dimen._54sdp

    @DimenRes
    private val defaultRectMaxHeightRes: Int = R.dimen._54sdp

    @DimenRes
    private val defaultMarginBetweenRectRes: Int = R.dimen._8sdp

    @DimenRes
    private val defaultRectBorderWidth: Int = R.dimen._2sdp

    @DimenRes
    private val defaultHintTextSize: Int = R.dimen._14ssp

    @DimenRes
    private val defaultMarginHintTop: Int = R.dimen._16sdp

    @DimenRes
    private val defaultLineHeightHint: Int = R.dimen._20ssp

    private val defaultMaxLinesHint: Int = 2

    @FontRes
    private val defaultFontFamilyHintRes: Int = R.font.uber_move_text_regular

    // Memory attributes
    @ColorRes
    private var rectBackgroundColorRes: Int = defaultRectBackgroundColorRes

    @ColorRes
    private var borderSelectedRectColorRes: Int = defaultBorderSelectedRectColorRes

    @ColorRes
    private var textColorRes: Int = defaultTextColorRes

    @ColorRes
    private var hintColorRes: Int = defaultHintColorRes

    @ColorRes
    private var hintErrorColorRes: Int = defaultHintErrorColorRes

    @ColorRes
    private var colorSuccessBackgroundRes: Int = defaultColorSuccessBackgroundRes

    @ColorRes
    private var colorBorderSelectedSuccessRes: Int = defaultColorBorderSelectedSuccessRes

    @ColorRes
    private var colorErrorBackgroundRes: Int = defaultColorErrorBackgroundRes

    @ColorRes
    private var colorErrorSelectedSuccessRes: Int = defaultColorErrorSelectedSuccessRes

    @DimenRes
    private var rectMaxWidthRes: Int = defaultRectMaxWidthRes

    @DimenRes
    private var rectMaxHeightRes: Int = defaultRectMaxHeightRes

    @DimenRes
    private var marginBetweenRectRes: Int = defaultMarginBetweenRectRes

    @DimenRes
    private var rectBorderWidth: Int = defaultRectBorderWidth

    @DimenRes
    private var hintTextSize: Int = defaultHintTextSize

    @DimenRes
    private var marginHintTop: Int = defaultMarginHintTop

    @DimenRes
    private var lineHeightHint: Int = defaultLineHeightHint

    private var maxLinesHint: Int = defaultMaxLinesHint

    @FontRes
    private var fontFamilyHintRes: Int = defaultFontFamilyHintRes

    private var isPasswordVisible: Boolean = false

    private var maxRect: Int = MAX_RECT

    // Container variables

    private val backgroundRectPaintColor: Paint by lazy {
        Paint().apply {
            isAntiAlias = true
            style = Paint.Style.FILL
            color = ContextCompat.getColor(context, rectBackgroundColorRes)
        }
    }

    private val backgroundRectPaintErrorColor: Paint by lazy {
        Paint().apply {
            isAntiAlias = true
            style = Paint.Style.FILL
            color = ContextCompat.getColor(context, colorErrorBackgroundRes)
        }
    }

    private val backgroundRectPaintSuccessColor: Paint by lazy {
        Paint().apply {
            isAntiAlias = true
            style = Paint.Style.FILL
            color = ContextCompat.getColor(context, colorSuccessBackgroundRes)
        }
    }

    private val backgroundRectSelectedPaintColor: Paint by lazy {
        Paint().apply {
            isAntiAlias = true
            style = Paint.Style.STROKE
            strokeWidth = resources.getDimensionPixelSize(rectBorderWidth).toFloat()
            color = ContextCompat.getColor(context, borderSelectedRectColorRes)
        }
    }

    private val backgroundRectSelectedPaintErrorColor: Paint by lazy {
        Paint().apply {
            isAntiAlias = true
            style = Paint.Style.STROKE
            strokeWidth = resources.getDimensionPixelSize(rectBorderWidth).toFloat()
            color = ContextCompat.getColor(context, colorErrorSelectedSuccessRes)
        }
    }

    private val backgroundRectSelectedPaintSuccessColor: Paint by lazy {
        Paint().apply {
            isAntiAlias = true
            style = Paint.Style.STROKE
            strokeWidth = resources.getDimensionPixelSize(rectBorderWidth).toFloat()
            color = ContextCompat.getColor(context, colorBorderSelectedSuccessRes)
        }
    }

    private val textPaint: Paint by lazy {
        Paint().apply {
            style = Paint.Style.FILL
            typeface = this@EditTextPassword.typeface
            textSize = this@EditTextPassword.textSize
            color = ContextCompat.getColor(context, textColorRes)
        }
    }

    private val textHintPaint: Paint by lazy {
        Paint().apply {
            style = Paint.Style.FILL
            typeface = TextPaint().apply {
                typeface = ResourcesCompat.getFont(context, fontFamilyHintRes)
            }.typeface
            textSize = resources.getDimensionPixelSize(hintTextSize).toFloat()
            color = ContextCompat.getColor(context, hintColorRes)
        }
    }

    private val textHintErrorPaint: Paint by lazy {
        Paint().apply {
            style = Paint.Style.FILL
            typeface = TextPaint().apply {
                typeface = ResourcesCompat.getFont(context, fontFamilyHintRes)
            }.typeface
            textSize = resources.getDimensionPixelSize(hintTextSize).toFloat()
            color = ContextCompat.getColor(context, hintErrorColorRes)
        }
    }

    private var animationPercentForHint: Float = 0f
    private var showHint = false
    private var animationDurationInnerText: Int = ANIMATION_DURATION_INNER_TEXT
    private var animationDurationHint: Int = ANIMATION_DURATION_HINT
    private val valueAnimatorForInnerText: List<ValueAnimator> = generateValueAnimationList()
    private var valueAnimatorForHint: ValueAnimator? = null

    var hintError: CharSequence? = null

    init {
        val typedArray = context.obtainStyledAttributes(attr, R.styleable.EditTextPassword)
        try {
            setupTypeArray(typedArray)
            setupView()
        } finally {
            typedArray.recycle()
        }
    }

    private fun setupTypeArray(typedArray: TypedArray) {
        setupColorParameter(typedArray)
        setupCommonValuesParameter(typedArray)
    }

    private fun setupCommonValuesParameter(typedArray: TypedArray) {
        rectMaxWidthRes = typedArray.getResourceId(
            R.styleable.EditTextPassword_rectMaxWidthRes,
            defaultRectMaxWidthRes
        )
        rectMaxHeightRes =
            typedArray.getResourceId(
                R.styleable.EditTextPassword_rectMaxHeightRes,
                defaultRectMaxHeightRes
            )
        marginBetweenRectRes =
            typedArray.getResourceId(
                R.styleable.EditTextPassword_marginBetweenRectRes,
                defaultMarginBetweenRectRes
            )
        rectBorderWidth =
            typedArray.getResourceId(
                R.styleable.EditTextPassword_rectBorderWidth,
                defaultRectBorderWidth
            )
        marginHintTop =
            typedArray.getResourceId(
                R.styleable.EditTextPassword_marginHintTop,
                defaultMarginHintTop
            )
        lineHeightHint =
            typedArray.getResourceId(
                R.styleable.EditTextPassword_lineHeightHint,
                defaultLineHeightHint
            )
        maxLinesHint = typedArray.getInteger(
            R.styleable.EditTextPassword_maxLinesHint,
            defaultMaxLinesHint
        )
        hintTextSize =
            typedArray.getResourceId(R.styleable.EditTextPassword_hintTextSize, defaultHintTextSize)
        maxRect = typedArray.getInteger(R.styleable.EditTextPassword_maxRect, MAX_RECT)
        isPasswordVisible =
            typedArray.getBoolean(R.styleable.EditTextPassword_isPasswordVisible, false)
        animationDurationInnerText =
            typedArray.getInteger(
                R.styleable.EditTextPassword_animationDuration,
                ANIMATION_DURATION_INNER_TEXT
            )
        animationDurationHint =
            typedArray.getInteger(
                R.styleable.EditTextPassword_animationDurationHint,
                ANIMATION_DURATION_HINT
            )
        fontFamilyHintRes =
            typedArray.getResourceId(
                R.styleable.EditTextPassword_fontFamilyHint,
                defaultFontFamilyHintRes
            )
    }

    private fun setupColorParameter(typedArray: TypedArray) {
        rectBackgroundColorRes =
            typedArray.getResourceId(
                R.styleable.EditTextPassword_rectBackgroundColorRes,
                defaultRectBackgroundColorRes
            )
        borderSelectedRectColorRes = typedArray.getResourceId(
            R.styleable.EditTextPassword_borderSelectedRectColorRes,
            defaultBorderSelectedRectColorRes
        )
        textColorRes =
            typedArray.getResourceId(R.styleable.EditTextPassword_textColorRes, defaultTextColorRes)
        hintColorRes =
            typedArray.getResourceId(R.styleable.EditTextPassword_hintColorRes, defaultHintColorRes)
        hintErrorColorRes =
            typedArray.getResourceId(
                R.styleable.EditTextPassword_hintErrorColorRes,
                defaultHintErrorColorRes
            )
        colorSuccessBackgroundRes = typedArray.getResourceId(
            R.styleable.EditTextPassword_colorSuccessBackgroundRes,
            defaultColorSuccessBackgroundRes
        )
        colorBorderSelectedSuccessRes = typedArray.getResourceId(
            R.styleable.EditTextPassword_colorBorderSelectedSuccessRes,
            defaultColorBorderSelectedSuccessRes
        )
        colorErrorBackgroundRes = typedArray.getResourceId(
            R.styleable.EditTextPassword_colorErrorBackgroundRes,
            defaultColorErrorBackgroundRes
        )
        colorErrorSelectedSuccessRes = typedArray.getResourceId(
            R.styleable.EditTextPassword_colorErrorSelectedSuccessRes,
            defaultColorErrorSelectedSuccessRes
        )
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.apply {
            val rects: List<Rect> = generateRectList()
            rects.forEachIndexed { index, rect ->
                if (isEnabled) {
                    if (hintError != null && text?.length == maxRect) {
                        drawRect(rect, backgroundRectPaintErrorColor)
                        drawRect(rect, backgroundRectSelectedPaintErrorColor)
                    } else if (hintError == null && text?.length == maxRect) {
                        drawRect(rect, backgroundRectPaintSuccessColor)
                        drawRect(rect, backgroundRectSelectedPaintSuccessColor)
                    } else {
                        drawRect(rect, backgroundRectPaintColor)
                        if (text?.length == index) {
                            drawRect(rect, backgroundRectSelectedPaintColor)
                        }
                    }
                } else {
                    drawRect(rect, backgroundRectPaintColor)
                }
                showTextInnerRect(index, rect)
                drawHint(rect, rects.concat())
            }
        }
    }

    private fun Canvas.drawHint(rect: Rect, concatRects: Rect) {
        val hintRect = getHintHeight()
        if (hintError != null) {
            val hintTopMargin = resources.getDimensionPixelSize(marginHintTop)
            drawTextMultipleLine(
                DrawTextMultipleLineParrametter(
                    hintError.toString(),
                    concatRects.left.toFloat(),
                    rect.bottom.toFloat() + hintTopMargin + hintRect,
                    maxLinesHint,
                    resources.getDimensionPixelSize(lineHeightHint).toFloat(),
                    concatRects.width().toFloat(),
                    textHintErrorPaint.apply {
                        color = ColorUtils.setAlphaComponent(
                            color,
                            (ALPHA_COLOR_MAX * animationPercentForHint).toInt()
                        )
                    }
                )
            )
        } else {
            val hintTopMargin = resources.getDimensionPixelSize(marginHintTop)
            drawTextMultipleLine(
                DrawTextMultipleLineParrametter(
                    hint ?: String(),
                    concatRects.left.toFloat(),
                    rect.bottom.toFloat() + hintTopMargin + hintRect,
                    maxLinesHint,
                    resources.getDimensionPixelSize(lineHeightHint).toFloat(),
                    concatRects.width().toFloat(),
                    textHintPaint
                )
            )
        }
    }

    private fun Canvas.showTextInnerRect(
        index: Int,
        rect: Rect
    ) {
        val isSelected = text?.length?.minus(1) == index
        val currentPercentForThisIndex = if (isSelected) {
            valueAnimatorForInnerText.getOrNull(index)?.animatedFraction ?: 1f
        } else 1f

        text?.getOrNull(index)?.let { character ->
            val bounds = getCharacterBoundsForText(character)
            if (isPasswordVisible) {
                drawText(
                    character.toString(),
                    rect.exactCenterX() - (bounds.exactCenterX() * currentPercentForThisIndex),
                    rect.exactCenterY() - (bounds.exactCenterY() * currentPercentForThisIndex),
                    Paint(textPaint).apply {
                        textSize = this@EditTextPassword.textSize * currentPercentForThisIndex
                    }
                )
            } else {
                // Reference = https://www.mathopenref.com/arcradius.html
                val radius =
                    (bounds.height() / 2f) +
                        (
                            bounds.width().toFloat().pow(2) /
                                (HEIGHT_DIVISION_FOR_RADIUS * bounds.height())
                            )

                val percentForBools =
                    1f - if ((1f - currentPercentForThisIndex) <= PERCENT_DIVIDED_BY_TWO) {
                        (1f - currentPercentForThisIndex) / PERCENT_DIVIDED_BY_TWO
                    } else 1.0f

                val percentForText = if (currentPercentForThisIndex <= PERCENT_DIVIDED_BY_TWO) {
                    currentPercentForThisIndex / PERCENT_DIVIDED_BY_TWO
                } else 1.0f

                if (currentPercentForThisIndex <= PERCENT_DIVIDED_BY_TWO) {
                    drawText(
                        character.toString(),
                        rect.exactCenterX() - (bounds.exactCenterX() * (1f - percentForText)),
                        rect.exactCenterY() - (bounds.exactCenterY() * (1f - percentForText)),
                        Paint(textPaint).apply {
                            textSize = this@EditTextPassword.textSize * (1f - percentForText)
                        }
                    )
                } else {
                    drawCircle(
                        rect.exactCenterX(),
                        rect.exactCenterY(),
                        radius * percentForBools,
                        textPaint
                    )
                }
            }
        }
    }

    private fun generateRectList(): List<Rect> {
        val margin = resources.getDimensionPixelSize(marginBetweenRectRes)
        val hintHeight = getHintRect()
        val capSizeWidth = ((width - (margin * (maxRect - 1))) / maxRect)
        val pMaxHeight = resources.getDimensionPixelSize(rectMaxHeightRes)
        val pMaxWidth = resources.getDimensionPixelSize(rectMaxWidthRes)
        val borderWidth = resources.getDimensionPixelSize(rectBorderWidth)

        val lengthListRange = (0 until maxRect)

        return lengthListRange.map {
            val widthDx = (capSizeWidth * it)
            val innerRect = Rect(
                widthDx,
                0,
                widthDx + capSizeWidth,
                height - hintHeight
            )

            val pCurrentWidth = innerRect.width()
            val pCurrentHeight = innerRect.height()

            val respHeight = if (pMaxHeight >= pCurrentHeight || (pMaxWidth * maxRect) >= width) {
                (pMaxHeight / pMaxWidth) * pCurrentWidth
            } else {
                pMaxHeight
            }
            val respWidth = if (pMaxWidth >= pCurrentWidth || (pMaxWidth * maxRect) >= width) {
                (pMaxWidth / pMaxHeight) * respHeight
            } else {
                pMaxWidth
            }

            val respWidthWithWidth =
                ((width - ((respWidth * maxRect) + margin * (maxRect - 1))) / 2) + margin * it

            val left = (respWidth * it) + (borderWidth / 2)
            val top = borderWidth / 2
            val right = ((respWidth * it) + respWidth) - (borderWidth / 2)
            val bottom = (respHeight + top)

            val width = right - left
            val height = bottom - top

            val bottomWithSizeConfirmation = if (height > width) {
                bottom - (height - width)
            } else {
                bottom
            }

            Rect(
                left,
                top,
                right,
                bottomWithSizeConfirmation
            ).apply {
                offset(respWidthWithWidth, 0)
            }
        }
    }

    private fun getCharacterBoundsForText(character: Char): Rect {
        val bounds = Rect()
        textPaint.getTextBounds(character.toString(), 0, 1, bounds)
        return bounds
    }

    private fun getCharacterBoundsForTextHint(character: CharSequence): Rect {
        val bounds = Rect()
        textHintPaint.getTextBounds(character.toString(), 0, character.length, bounds)
        return bounds
    }

    override fun onSelectionChanged(selStart: Int, selEnd: Int) {
        setSelection(text?.length ?: 0, text?.length ?: 0)
        super.onSelectionChanged(selStart, selEnd)
    }

    private fun setupView() {
        val hintHeight = getHintRect()
        minHeight = resources.getDimensionPixelSize(rectMaxHeightRes) +
            resources.getDimensionPixelSize(rectBorderWidth) +
            hintHeight +
            resources.getDimensionPixelSize(marginHintTop)
        minWidth = resources.getDimensionPixelSize(rectMaxWidthRes) * maxRect
        background = null

        filters = arrayOf<InputFilter>(LengthFilter(maxRect))

        isCursorVisible = false
    }

    private fun getHintHeight(): Int =
        if (!hint.isNullOrEmpty()) {
            getCharacterBoundsForTextHint(hint).height()
        } else if (!hintError.isNullOrEmpty()) {
            getCharacterBoundsForTextHint(hintError!!).height()
        } else {
            0
        }

    private fun getHintRect(): Int =
        if (!hint.isNullOrEmpty()) {
            val margin = resources.getDimensionPixelSize(marginBetweenRectRes)
            groupBySpace((width - (margin * (maxRect - 1))).toFloat(), hint.toString()).map {
                getCharacterBoundsForTextHint(it)
            }.sumBy { it.height() }
        } else if (!hintError.isNullOrEmpty()) {
            val margin = resources.getDimensionPixelSize(marginBetweenRectRes)
            groupBySpace((width - (margin * (maxRect - 1))).toFloat(), hintError.toString()).map {
                getCharacterBoundsForTextHint(it)
            }.sumBy { it.height() }
        } else {
            0
        }

    override fun onTextChanged(
        text: CharSequence?,
        start: Int,
        lengthBefore: Int,
        lengthAfter: Int
    ) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)
        if (!(lengthBefore > lengthAfter || text?.isEmpty() == true)) {
            startAnimationRect()
        }
    }

    private fun showHintError() {
        if (!showHint) {
            startAnimationHint()
            showHint = true
        }
    }

    private fun hideHintError() {
        if (showHint) {
            valueAnimatorForHint?.reverse()
            showHint = false
        }
    }

    private fun startAnimationRect() {
        valueAnimatorForInnerText.getOrNull(text?.length?.minus(1) ?: 0)?.start()
    }

    private fun generateValueAnimationList(): List<ValueAnimator> =
        (0..maxRect).map { index ->
            val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
            valueAnimator?.duration = animationDurationInnerText.toLong()
            valueAnimator?.addUpdateListener {
                postInvalidate()
            }
            valueAnimator
        }

    private fun startAnimationHint() {
        valueAnimatorForHint = ValueAnimator.ofFloat(0f, 1f)
        valueAnimatorForHint?.duration = animationDurationHint.toLong()
        valueAnimatorForHint?.addUpdateListener {
            animationPercentForHint = it.animatedFraction
            postInvalidate()
        }
        valueAnimatorForHint?.start()
    }

    private fun Iterable<Rect>.concat(): Rect =
        fold(Rect()) { acc, value ->
            acc.set(
                min(acc.left, value.left),
                min(acc.top, value.top),
                max(acc.right, value.right),
                max(acc.bottom, value.bottom)
            )
            acc
        }

    private fun Canvas.drawTextMultipleLine(parametter: DrawTextMultipleLineParrametter) {
        val bounds = getCharacterBoundsForTextHint(parametter.text)
        if (bounds.width() > parametter.maxWidth) {
            val group = groupBySpace(parametter.maxWidth, parametter.text.toString())
            group.subList(0, min(group.size, parametter.maxLines)).forEachIndexed { index, value ->
                drawText(
                    value,
                    parametter.x,
                    parametter.y + (parametter.marginBetweenLine * index),
                    parametter.paint
                )
            }
        } else {
            drawText(
                parametter.text.toString(),
                parametter.x,
                parametter.y,
                parametter.paint
            )
        }
    }

    private fun groupBySpace(maxWidth: Float, textWithoutSpaces: String): List<String> =
        textWithoutSpaces.split("\\s".toRegex())
            .fold(mutableListOf(emptyList<String>().toMutableList())) { acc, value ->
                val bounds = acc.lastOrNull()?.toMutableList()?.apply {
                    add(value)
                }?.joinToString(" ")?.let { joinedString ->
                    getCharacterBoundsForTextHint(joinedString)
                }

                if ((bounds?.width() ?: 0) >= maxWidth) {
                    acc.add(mutableListOf(value))
                } else {
                    acc.lastOrNull()?.add(value)
                }

                acc
            }.map { currentList ->
                currentList.joinToString(" ")
            }

    companion object {
        const val MAX_RECT = 4
        const val ANIMATION_DURATION_INNER_TEXT = 500
        const val ANIMATION_DURATION_HINT = 250
        const val ALPHA_COLOR_MAX = 255
        const val HEIGHT_DIVISION_FOR_RADIUS = 8f
        const val PERCENT_DIVIDED_BY_TWO = 0.5f

        @JvmStatic
        @BindingAdapter("isPasswordVisible")
        fun EditTextPassword.setIsPasswordVisible(isPasswordVisible: Boolean) {
            if (isPasswordVisible != this.isPasswordVisible) {
                this.isPasswordVisible = isPasswordVisible
                postInvalidate()
            }
        }

        @JvmStatic
        @BindingAdapter("hint")
        fun EditTextPassword.setHintCompat(value: String? = null) {
            if (value != hint) {
                hint = value
                postInvalidate()
                setupView()
            }
        }

        @JvmStatic
        @BindingAdapter("hintError")
        fun EditTextPassword.setHintError(value: String? = null) {
            if (value != hintError) {
                hintError = value
                postInvalidate()
                setupView()
                if (value != null) {
                    showHintError()
                } else {
                    hideHintError()
                }
            }
        }
    }

    data class DrawTextMultipleLineParrametter(
        val text: CharSequence,
        val x: Float,
        val y: Float,
        val maxLines: Int,
        val marginBetweenLine: Float,
        val maxWidth: Float,
        val paint: Paint
    )
}