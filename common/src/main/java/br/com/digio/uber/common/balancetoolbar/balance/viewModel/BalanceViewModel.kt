package br.com.digio.uber.common.balancetoolbar.balance.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.balancetoolbar.balance.mapper.BalanceMapper
import br.com.digio.uber.common.balancetoolbar.balance.uiModel.BalanceUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.balance.GetBalanceUseCase
import br.com.digio.uber.domain.usecase.eyes.GetEyesUseCase
import br.com.digio.uber.domain.usecase.eyes.InsertEyesUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi

class BalanceViewModel constructor(
    getBalanceUseCase: GetBalanceUseCase,
    getEyesUseCase: GetEyesUseCase,
    val insertEyesUseCase: InsertEyesUseCase
) : BaseViewModel() {

    val balanceUiModel: LiveData<BalanceUiModel> = getBalanceUseCase(Unit).exec().map(BalanceMapper())
    val mutableBalanceVisibility = getEyesUseCase(Unit)
    val blockValue: MutableLiveData<String?> = MutableLiveData()

    @ExperimentalCoroutinesApi
    fun balanceVisibilityChanged(isChecked: Boolean) {
        if (isChecked != mutableBalanceVisibility.value) {
            insertEyesUseCase(isChecked).singleExec()
        }
    }
}