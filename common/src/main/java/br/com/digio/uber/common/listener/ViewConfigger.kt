package br.com.digio.uber.common.listener

import android.content.pm.ActivityInfo

interface ViewConfigger {

    fun getOrientation(): Int = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    fun getRequestCode(): MutableList<Int> = emptyArray<Int>().toMutableList()
    fun getRequestPermisionCode(): MutableList<Int> = emptyArray<Int>().toMutableList()
    fun updateView() = Unit
}