package br.com.digio.uber.common.helper

import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import br.com.digio.uber.common.R

object LayoutIds {
    @LayoutRes val navigationLayoutActivity: Int = R.layout.navigation_layout_activity
    @LayoutRes val navigationLayoutWithToolbarActivity: Int = R.layout.navigation_layout_with_toolbar_activity
    @LayoutRes val navigationLayoutWithToolbarActivityWhite: Int =
        R.layout.navigation_layout_with_toolbar_activity_white
    @LayoutRes val navigationLayoutWithCollapsingToolbarScrollActivity: Int =
        R.layout.navigation_layout_with_collapsing_toolbar_layout_activity
    @LayoutRes val navigationLayoutWithCollapsingToolbarScrollActivityWhite: Int =
        R.layout.navigation_layout_with_collapsing_toolbar_layout_activity_white
}

object ViewIds {
    @IdRes val navHostId: Int = R.id.nav_host
    @IdRes val toolbarId: Int = R.id.nav_toolbar
    @IdRes val basicToolbarIdWhite: Int = R.id.nav_toolbar_basic_white
    @IdRes val toolbarIdWhite: Int = R.id.nav_toolbar_white
    @IdRes val collapsingToolbarId: Int = R.id.nav_collapsing_toolbar_layout
    @IdRes val collapsingToolbarIdWhite: Int = R.id.nav_collapsing_toolbar_layout_white
}