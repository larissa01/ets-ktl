package br.com.digio.uber.common.util

import android.os.Bundle
import br.com.digio.uber.common.R
import br.com.digio.uber.common.choice.Choice
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.RequestOnResult.Payment.FORCE_QRCODE
import java.io.Serializable

class FeatureInfoRoute {
    companion object {

        const val FEATURE_PIX = "PIX"
        const val FEATURE_RECEIVE = "RECEIVE"

        private val INFO_ROUTE_VALUE_LIST: List<FeatureInfoRouteValue> = listOf(
            FeatureInfoRouteValue(
                id = "PAY",
                labelDefault = R.string.feature_pay,
                route = Const.Activities.PAYMENT_ACTIVITY,
                iconDefault = R.drawable.ic_barcode
            ),
            FeatureInfoRouteValue(
                id = FEATURE_PIX,
                labelDefault = R.string.feature_pix,
                route = Const.Activities.PIX_MENU_ACTIVITY,
                iconDefault = R.drawable.ic_pix
            ),
            FeatureInfoRouteValue(
                id = "RECHARGE",
                labelDefault = R.string.feature_recharge,
                route = Const.Activities.RECHARGE_ACTIVITY,
                iconDefault = R.drawable.ic_recharge,
            ),
            FeatureInfoRouteValue(
                id = "TRANSFER",
                labelDefault = R.string.feature_transfer,
                route = Const.Activities.CHOICE_ACTIVITY,
                iconDefault = R.drawable.ic_tranfer,
                isRefreshBack = true,
                bundle = Bundle().apply {
                    val choices = arrayListOf(
                        Choice(
                            R.drawable.ic_transfer_ted,
                            R.string.to_another_banks,
                            R.string.to_another_banks_text,
                            Const.Activities.TRANSFER_ACTIVITY,
                            Const.Fragments.TED_FRAGMENT
                        ),
                        Choice(
                            R.drawable.ic_transfer_p2p,
                            R.string.to_a_uber_account,
                            R.string.to_a_uber_account_text,
                            Const.Activities.TRANSFER_ACTIVITY,
                            Const.Fragments.P2P_FRAGMENT
                        )
                    )
                    putInt(Const.Extras.TOOLBAR_ID, R.string.feature_transfer_title)
                    putParcelableArrayList(Const.Extras.CHOICES, choices)
                }
            ),
            FeatureInfoRouteValue(
                id = FEATURE_RECEIVE,
                labelDefault = R.string.feature_receive,
                route = Const.Activities.CHOICE_ACTIVITY,
                iconDefault = R.drawable.ic_receive,
                bundle = Bundle().apply {
                    val choices = arrayListOf(
                        Choice(
                            R.drawable.ic_receive_with_pix,
                            R.string.receive_with_pix,
                            R.string.receive_with_pix_subtitle,
                            Const.Activities.PIX_RECEIVEMENT_INPUT_VALUES_ACTIVITY,
                            keepBundle = true
                        ),
                        Choice(
                            R.drawable.ic_generate_bankslip,
                            R.string.generate_bankslip,
                            R.string.generate_bankslip_subtitle,
                            Const.Activities.BANK_SLIP_ACTIVITY
                        ),
                        Choice(
                            R.drawable.ic_generated_bankslips,
                            R.string.generated_bankslips,
                            R.string.generated_bankslips_subtitle,
                            Const.Activities.BANK_SLIP_GENERATED_ACTIVITY
                        )
                    )

                    putInt(Const.Extras.TOOLBAR_ID, R.string.feature_receive)
                    putParcelableArrayList(Const.Extras.CHOICES, choices)
                }
            ),
            FeatureInfoRouteValue(
                id = "WITHDRAW",
                labelDefault = R.string.feature_withdraw,
                route = Const.Activities.WITHDRAW_ACTIVITY,
                iconDefault = R.drawable.ic_withdraw
            ),
            // Tag List
            FeatureInfoRouteValue(
                id = "EXTRACT",
                labelDefault = R.string.balance_tag_extract,
                route = Const.Activities.STATEMENT_ACTIVITY
            ),
            FeatureInfoRouteValue(
                id = "INCOME",
                labelDefault = R.string.balance_tag_income,
                route = Const.Activities.INCOME_ACTIVITY
            ),
            FeatureInfoRouteValue(
                id = "SPENDING_CONTROL",
                labelDefault = R.string.balance_tag_spending_control,
                route = Const.Activities.FEATURE_NAVIGATION_ACTIVITY
            ),
            // ShortcutItem
            FeatureInfoRouteValue(
                id = "TRACK_CARD",
                labelDefault = R.string.shortcut_track_card,
                route = Const.Activities.TRACKING_ACTIVITY,
                iconDefault = R.drawable.ic_track_card
            ),
            FeatureInfoRouteValue(
                id = "RECEIVED_MY_CARD",
                labelDefault = R.string.shortcut_received_my_card,
                route = Const.Activities.CARD_RECEIVED_ACTIVITY,
                iconDefault = R.drawable.ic_received_my_card
            ),
            FeatureInfoRouteValue(
                id = "CASHBACK",
                labelDefault = R.string.shortcut_received_my_card,
                route = Const.Activities.CASHBACK_ACTIVITY,
                iconDefault = R.drawable.ic_cashback
            ),
            FeatureInfoRouteValue(
                id = "PAY_WITH_QR_CODE",
                labelDefault = R.string.shortcut_pay_with_qrcod,
                route = Const.Activities.PAYMENT_ACTIVITY,
                iconDefault = R.drawable.ic_pay_with_qrcode,
                bundle = Bundle().apply {
                    putBoolean(FORCE_QRCODE, true)
                }
            ),

            FeatureInfoRouteValue(
                id = "FRANCHISE",
                labelDefault = R.string.shortcut_franchise,
                route = Const.Activities.FRANCHISE_ACTIVITY,
                iconDefault = R.drawable.ic_bank,
            )
        )

        fun getFeature(id: String): FeatureInfoRouteValue? =
            INFO_ROUTE_VALUE_LIST.find { featureRoute ->
                featureRoute.id == id
            }
    }

    data class FeatureInfoRouteValue constructor(
        val id: String,
        val labelDefault: Int,
        val route: String,
        val iconDefault: Int? = R.drawable.ic_pay_with_qrcode,
        val isRefreshBack: Boolean? = false,
        val bundle: Bundle? = null
    ) : Serializable
}