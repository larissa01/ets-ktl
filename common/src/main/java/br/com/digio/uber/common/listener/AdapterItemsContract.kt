package br.com.digio.uber.common.listener

interface AdapterItemsContract {
    fun replaceItems(list: List<Any>)
}