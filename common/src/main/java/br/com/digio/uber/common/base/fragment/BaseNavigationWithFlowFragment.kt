package br.com.digio.uber.common.base.fragment

import android.os.Bundle
import android.view.WindowManager
import br.com.digio.uber.common.androidUtils.AnimationTransactionObjectTypes
import br.com.digio.uber.common.generics.MessageGenericObject
import br.com.digio.uber.common.listener.BaseNavigationWithFlowListeners
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.listener.NavigationListenerWithFlow
import br.com.digio.uber.common.listener.ResultsCode
import br.com.digio.uber.common.typealiases.OnDismissDialog

abstract class BaseNavigationWithFlowFragment :
    BaseNavigationFragment(),
    BaseNavigationWithFlowListeners {

    override var resultCode: ResultsCode = ResultsCode.RESULT_OK

    private var originalMode: Int? = null

    var navigationListener: NavigationListenerWithFlow? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        originalMode = activity?.window?.attributes?.softInputMode
        updateSoftInputMode()
    }

    override fun callGoTo(
        fragmentName: String,
        bundle: Bundle?,
        forResult: Boolean,
        animationTransactionObject: AnimationTransactionObjectTypes?
    ) {
        navigationListener?.goTo(
            GoTo(
                fragmentName,
                bundle
                    ?: arguments,
                this,
                forResult,
                animationTransactionObject?.animation
            )
        )
        returnSoftInputModeToOriginalMode()
    }

    override fun callGoBack(bundle: Bundle?) {
        navigationListener?.back(bundle, this)
    }

    override fun clearAllBackStack() {
        navigationListener?.clearAllBackStack()
    }

    override fun getTagRegistration() = tag()

    override fun showMessage(
        message: MessageGenericObject?,
        closeable: Boolean,
        onCloseDialog: OnDismissDialog?
    ) {
        super.showMessage(message, closeable) {
            if (message?.callGoBack == true) {
                callGoBack()
            }
            (onCloseDialog ?: message?.onCloseDialog)?.invoke(this, it)
        }
    }

    override fun onBackPressedFromNavigation() {
        super.onBackPressedFromNavigation()

        if (isSecurityScreen()) {
            activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        } else {
            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
        }
    }

    override fun onFragmentVisible() {
        super.onFragmentVisible()
        updateStatusBar()
        updateSoftInputMode()
    }

    open fun getSoftInputMode(): Int? = activity?.window?.attributes?.softInputMode

    private fun updateSoftInputMode() {
        getSoftInputMode()?.let {
            activity?.window?.setSoftInputMode(it)
        }
    }

    private fun returnSoftInputModeToOriginalMode() {
        originalMode?.let {
            activity?.window?.setSoftInputMode(it)
        }
    }
}