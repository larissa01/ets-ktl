package br.com.digio.uber.common.base.adapter

import android.view.View
import androidx.databinding.ViewDataBinding

interface ViewTypesFactory<T> {
    fun type(model: T): Int
    fun holder(type: Int, view: View, listener: ViewTypesListener<T>): AbstractViewHolder<*>
}

interface ViewTypesDataBindingFactory<T> {
    fun type(model: T): Int
    fun holder(type: Int, view: ViewDataBinding, listener: ViewTypesListener<T>): AbstractViewHolder<*>
}

interface AdapterViewModel<T> {
    fun type(typesFactory: ViewTypesDataBindingFactory<T>): Int
}

typealias ViewTypesListener<T> = (T) -> Unit