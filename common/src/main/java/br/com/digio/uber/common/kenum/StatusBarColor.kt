package br.com.digio.uber.common.kenum

import androidx.annotation.ColorRes
import br.com.digio.uber.common.R

enum class StatusBarColor constructor(
    var dark: Boolean,
    @ColorRes
    var color: Int = R.color.black
) {
    WHITE(
        false,
        R.color.white
    ),
    BLACK(
        true,
        R.color.black
    ),
    TRANSPARENT(
        true,
        android.R.color.transparent
    ),
    GREY(
        true,
        R.color.grey_1F1F1F
    ),
    GREEN(
        false,
        R.color.green_E6F2ED
    ),
    ORANGE(
        true,
        R.color.orange_FA9269
    ),
    YELLOW(
        true,
        R.color.yellow_F4DCA2
    )
}