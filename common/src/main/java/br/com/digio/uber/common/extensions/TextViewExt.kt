package br.com.digio.uber.common.extensions

import android.graphics.Paint
import android.text.SpannableString
import android.view.View
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter
import br.com.digio.uber.common.R

fun AppCompatTextView.unStrike() {
    paintFlags = paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
}

fun AppCompatTextView.strike() {
    paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
}

@BindingAdapter("textWithSpannable")
fun TextView.setTextWithSpannable(text: SpannableString) {
    setText(text, TextView.BufferType.SPANNABLE)
}

@BindingAdapter("textInt")
fun TextView.setTextInt(@StringRes text: Int) {
    if (text != 0) {
        setText(text)
    }
}

@BindingAdapter("strike")
fun strikeView(text: AppCompatTextView, isStrike: Boolean) {
    if (isStrike) {
        text.strike()
    } else {
        text.unStrike()
    }
}

@BindingAdapter(value = ["formatString", "value"], requireAll = false)
fun TextView.setStringFormated(resourceString: String? = null, textToFormat: Any? = null) {
    if (resourceString != null && textToFormat != null) {
        text = resourceString.format(textToFormat)
    }
}

@BindingAdapter(value = ["formatString", "values"], requireAll = false)
fun TextView.setStringFormated(resourceString: String? = null, textToFormat: List<Any>? = null) {
    if (resourceString != null && textToFormat != null) {
        text = resourceString.format(textToFormat.toTypedArray())
    }
}

@BindingAdapter("fontFamily")
fun TextView.setFontFamily(fontFamily: Int) {
    val typeFace = ResourcesCompat.getFont(context, fontFamily)
    typeface = typeFace
}

@BindingAdapter(value = ["htmlString"], requireAll = true)
fun AppCompatTextView.setHtmlString(resourceResId: Int = 0) {
    if (resourceResId != 0) {
        val resourceString = context.getString(resourceResId)
        text = HtmlCompat.fromHtml(resourceString, HtmlCompat.FROM_HTML_MODE_LEGACY)
    }
}

@BindingAdapter(value = ["htmlString"], requireAll = true)
fun AppCompatTextView.setHtmlString(resourceString: String? = null) {
    if (resourceString != null) {
        text = HtmlCompat.fromHtml(resourceString, HtmlCompat.FROM_HTML_MODE_LEGACY)
    }
}

@BindingAdapter("android:text", "disableMask", requireAll = false)
fun AppCompatTextView.maskValue(
    text: String?,
    disableMask: Boolean? = false
) {
    if (disableMask == true) setText(R.string.mask_value)
    else this.text = text
}

@BindingAdapter("textColor")
fun TextView.setTextColor(color: Int) {
    val color = ContextCompat.getColor(context, color)
    setTextColor(color)
}

@BindingAdapter("isVisible")
fun TextView.setIsVisible(isVisible: Boolean) {
    if (isVisible) this.visibility = View.VISIBLE
    else this.visibility = View.INVISIBLE
}