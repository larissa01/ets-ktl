package br.com.digio.uber.common.extensions

import android.widget.Button
import android.widget.ImageButton
import androidx.appcompat.widget.AppCompatButton
import androidx.databinding.BindingAdapter
import com.google.android.material.button.MaterialButton

@BindingAdapter("isSelected")
fun AppCompatButton.setIsSelected(selected: Boolean) {
    isSelected = selected
}

@BindingAdapter("isEnabledAlpha")
fun AppCompatButton.setIsEnable(isEnabled: Boolean) {
    this.isEnabled = isEnabled
    alpha = if (isEnabled) {
        FULL_ALPHA
    } else {
        HALF_ALPHA
    }
}

@BindingAdapter("isEnable")
fun Button.setIsEnable(isEnabled: Boolean) {
    this.isEnabled = isEnabled
}

@BindingAdapter("isEnable")
fun ImageButton.setIsEnable(isEnabled: Boolean) {
    this.isEnabled = isEnabled
}

@BindingAdapter("buttonEnabled")
fun MaterialButton.setEnabledWithAlpha(value: Boolean) {
    this.isEnabled = value

    alpha = if (value) {
        FULL_ALPHA
    } else {
        HALF_ALPHA
    }
}