package br.com.digio.uber.common.extensions

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import br.com.digio.uber.common.base.dialog.BNWFViewModelDialog
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.dialogs.LoadingDialog
import br.com.digio.uber.domain.usecase.login.LogoutUseCase
import timber.log.Timber

fun <T : ViewDataBinding> BNWFViewModelDialog<T, *>.associateViewModel(
    layoutInflater: LayoutInflater,
    logoutUseCase: LogoutUseCase,
    container: ViewGroup?,
    layoutId: Int? = null,
    viewModel: BaseViewModel? = null,
    bindingVariable: Int? = null
): T? {
    val binding = layoutId?.let {
        DataBindingUtil.inflate<T>(layoutInflater, it, container, false)
    }
    viewModel?.message?.observe(
        viewLifecycleOwner,
        Observer { error ->
            error?.let { errorLet ->
                showMessage(errorLet)
                viewModel.message.value = null
            }
        }
    )

    viewModel?.showLoading?.observe(
        viewLifecycleOwner,
        Observer {
            if (it == true) {
                getActivityForError()?.let { activityLet ->
                    LoadingDialog.showDialog(activityLet)
                } ?: run {
                    Timber.e("Dialog didn't show loading")
                }
            } else {
                LoadingDialog.dismissDialog()
            }
        }
    )

    viewModel?.makeLogout?.observe(
        this,
        Observer {
            context?.let { contextLet ->
                if (it != null) {
                    executeLogout(viewModel, logoutUseCase, contextLet, navigation)
                }
            }
        }
    )

    binding?.lifecycleOwner = viewLifecycleOwner
    bindingVariable?.let { bindingVariableLet ->
        binding?.setVariable(bindingVariableLet, viewModel)
    }
    binding?.executePendingBindings()

    return binding
}