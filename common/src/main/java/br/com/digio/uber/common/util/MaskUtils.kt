package br.com.digio.uber.common.util

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import br.com.digio.uber.common.typealiases.AfterTextChanged
import br.com.digio.uber.common.typealiases.BeforeTextChanged
import br.com.digio.uber.common.typealiases.OnTextChanged
import br.com.digio.uber.common.typealiases.OnTextChangedWithMask
import java.text.NumberFormat
import java.util.Locale

object MaskUtils {

    fun unmask(s: String): String {
        return s.replace("[.]".toRegex(), "").replace("[-]".toRegex(), "")
            .replace("[/]".toRegex(), "").replace("[(]".toRegex(), "")
            .replace("[)]".toRegex(), "").replace(" ".toRegex(), "")
            .replace(",".toRegex(), "")
    }

    fun unmaskPhone(value: String): String {
        return value.replace("[(]".toRegex(), "").replace("[)]".toRegex(), "")
            .replace("[-]".toRegex(), "").replace("[ ]".toRegex(), "")
    }

    fun isASign(c: Char): Boolean {
        return c == '.' || c == '-' || c == '/' || c == '(' || c == ')' || c == ',' || c == ' '
    }

    fun floatToMonetary(value: Double): String {
        return NumberFormat.getCurrencyInstance(Locale("pt", "BR")).format(value)
    }

    fun insert(
        mask: String?,
        ediTxt: EditText,
        onTextChanged: OnTextChanged? = null,
        beforeTextChanged: BeforeTextChanged? = null,
        afterTextChanged: AfterTextChanged? = null,
        onTextChangedWithMask: OnTextChangedWithMask? = null
    ): TextWatcher {
        return object : TextWatcher {
            var isUpdating: Boolean = false
            var old = ""

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                mask?.let {
                    onTextChanged?.invoke(s, start, before, count)
                    val str = unmask(s.toString())
                    if (isUpdating) {
                        old = str
                        onTextChangedWithMask?.invoke(s.toString())
                        isUpdating = false
                        return
                    }

                    var index = 0
                    var mascara: String = mask.fold(mutableListOf<Char>()) { acc, element ->
                        val strOrNull = str.getOrNull(index)
                        if (element == '#' && strOrNull != null) {
                            acc.add(strOrNull)
                            index++
                        } else if (strOrNull != null) {
                            acc.add(element)
                        }
                        acc
                    }.joinToString("")

                    if (mascara.isNotEmpty()) {
                        var lastChar = mascara.lastOrNull()
                        var hadSign = false
                        while (lastChar != null && isASign(lastChar) && str.length == old.length
                        ) {
                            val lastIndexOrZero = if (mascara.isNotEmpty()) mascara.length - 1 else 0
                            mascara = mascara.substring(0, lastIndexOrZero)
                            lastChar = mascara.lastOrNull()
                            hadSign = true
                        }

                        if (mascara.isNotEmpty() && hadSign) {
                            val lastIndexOrZero = if (mascara.isNotEmpty()) mascara.length - 1 else 0
                            mascara = mascara.substring(0, lastIndexOrZero)
                        }
                    }

                    isUpdating = true
                    ediTxt.setText(mascara)
                    ediTxt.setSelection(mascara.length)
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                beforeTextChanged?.invoke(s, start, count, after)
            }

            override fun afterTextChanged(s: Editable) {
                afterTextChanged?.invoke(s)
            }
        }
    }
}