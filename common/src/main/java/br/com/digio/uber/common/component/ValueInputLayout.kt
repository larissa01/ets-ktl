package br.com.digio.uber.common.component

import android.content.Context
import android.text.Spannable
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.google.android.material.textfield.TextInputLayout
import timber.log.Timber

class ValueInputLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : TextInputLayout(context, attrs, defStyleAttr) {

    init {
        isHintEnabled = false

        addOnEndIconChangedListener { _, endIconMode ->
            if (endIconMode == END_ICON_CLEAR_TEXT) {
                for (i in 0..childCount) {
                    (getChildAt(i) as? ValueInputEditText)?.resetValue()
                }
            }
        }
    }

    override fun setHelperTextEnabled(enabled: Boolean) {
        super.setHelperTextEnabled(enabled)

        if (!enabled) {
            return
        }

        try {
            val helperView =
                this.findViewById<TextView>(com.google.android.material.R.id.textinput_helper_text)
            helperView.layoutParams =
                FrameLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT
                )
            helperView.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
            helperView.gravity = Gravity.END
        } catch (e: ClassCastException) {
            Timber.e(e)
        }
    }

    override fun setErrorEnabled(enabled: Boolean) {
        super.setErrorEnabled(enabled)

        if (!enabled) {
            return
        }

        try {
            val errorView =
                this.findViewById<TextView>(com.google.android.material.R.id.textinput_error)

            val params = FrameLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT
            )

            errorView.layoutParams = params
            errorView.textAlignment = View.TEXT_ALIGNMENT_TEXT_END
            errorView.gravity = Gravity.END
        } catch (e: ClassCastException) {
            Timber.e(e)
        }
    }

    fun setErrorText(errorText: Spannable?) {
        if (errorText.isNullOrBlank()) {
            isErrorEnabled = false
            return
        }

        isErrorEnabled = true

        error = errorText
    }
}