package br.com.digio.uber.common.choice

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Choice(

    val icon: Int,
    val title: Int,
    val message: Int,
    val activityToStart: String,
    val fragmentToStart: String? = null,
    val keepBundle: Boolean = false

) : Parcelable