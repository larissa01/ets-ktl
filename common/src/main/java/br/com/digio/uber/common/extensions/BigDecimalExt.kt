package br.com.digio.uber.common.extensions

import br.com.digio.uber.util.Const.LocaleConst.LOCALE_COUNTRY_BR
import br.com.digio.uber.util.Const.LocaleConst.LOCALE_PT
import br.com.digio.uber.util.removeNoBreakingSpace
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale

val LOCALE_PT_BR by lazy { Locale("pt", "BR") }

fun BigDecimal.toMoney(withFraction: Boolean = false): String {
    val formatter = DecimalFormat("#.##")
    val locale = Locale("pt", "BR")
    val symbol = Currency.getInstance(locale).symbol
    formatter.maximumFractionDigits = if (withFraction) 2 else 0
    formatter.minimumFractionDigits = if (withFraction) 2 else 0
    formatter.decimalFormatSymbols = formatter.decimalFormatSymbols.apply {
        groupingSeparator = '.'
        decimalSeparator = ','
    }
    formatter.positivePrefix = "$symbol "
    formatter.negativePrefix = "$symbol -"
    return formatter.format(this)
}

fun BigDecimal.toCurrencyValue(symbol: Boolean): String {
    val format: NumberFormat = NumberFormat.getCurrencyInstance(Locale(LOCALE_PT, LOCALE_COUNTRY_BR))
    val currency: String = format.format(this)

    return if (symbol) currency else currency.substring(2)
}

fun BigDecimal.toMoneyNumber(): String {
    return this.toMoney().replace("R$", "").trim()
}

fun BigDecimal.toMoney() = formatWithSpace()

fun BigDecimal?.toMoneyOrEmpty(): String {
    return this?.formatWithSpace() ?: ""
}

private fun BigDecimal.formatWithSpace(): String {
    val numberFormat = NumberFormat.getCurrencyInstance(LOCALE_PT_BR)
    return numberFormat.format(this).run {
        val symbol = numberFormat.currency.symbol
        this.removeNoBreakingSpace()
            .replace(symbol, "$symbol ")
    }
}