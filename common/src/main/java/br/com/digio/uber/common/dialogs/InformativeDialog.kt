package br.com.digio.uber.common.dialogs

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.FrameLayout
import androidx.annotation.DrawableRes
import androidx.annotation.NonNull
import br.com.digio.uber.common.BR
import br.com.digio.uber.common.R
import br.com.digio.uber.common.base.activity.CoreBaseActivity
import br.com.digio.uber.common.base.dialog.BNWFViewModelDialog
import br.com.digio.uber.common.databinding.InformativeDialogDatabinding
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.BOOLEAN_SHOW_INFORMATIVE_DIALOG_ICON
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.BOOLEAN_SHOW_INFORMATIVE_DIALOG_TEXT
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.BOOLEAN_SHOW_INFORMATIVE_DIALOG_TITLE
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.BOOLEAN_SHOW_POSITIVE_BUTTON
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.BOOLEAN_SHOW_SUPPORT_TEXT_ONE
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.BOOLEAN_SHOW_SUPPORT_TEXT_TWO
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.INFORMATIVE_DIALOG_BUTTON_TEXT
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.INFORMATIVE_DIALOG_ICON
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.INFORMATIVE_DIALOG_MIDDLE_BUTTON_TEXT
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.INFORMATIVE_DIALOG_MULTIPLE_CHOOSE
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.INFORMATIVE_DIALOG_NEGATIVE_BUTTON_TEXT
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.INFORMATIVE_DIALOG_STATUS_BAR_COLOR
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.INFORMATIVE_DIALOG_SUPPORT_TEXT_ONE
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.INFORMATIVE_DIALOG_SUPPORT_TEXT_TWO
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.INFORMATIVE_DIALOG_TEXT
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.INFORMATIVE_DIALOG_TITLE
import br.com.digio.uber.common.dialogs.viewmodel.InformativeViewModel
import br.com.digio.uber.common.extensions.TAG_INFORMATIVE
import br.com.digio.uber.common.helper.DialogNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.common.typealiases.GenericEvent
import br.com.digio.uber.common.typealiases.OnDismissDialog
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_COLLAPSED
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_HIDDEN
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.koin.android.ext.android.inject

class InformativeDialog :
    BNWFViewModelDialog<InformativeDialogDatabinding, InformativeViewModel>() {

    var onDismiss: OnDismissDialog? = null
    var onPositiveButton: GenericEvent? = null
    var onMiddleButton: GenericEvent? = null
    var onNegativeButton: GenericEvent? = null

    var closeWithButton: Boolean = false

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.valueOf(
            arguments?.getString(INFORMATIVE_DIALOG_STATUS_BAR_COLOR)
                ?: StatusBarColor.WHITE.name
        )

    override val viewModel: InformativeViewModel by inject()
    override val bindingVariable: Int? = BR.informativeViewModel
    override val getLayoutId: Int? = R.layout.redesign_informative_dialog

    override fun initialize() {
        addScreenHeightObserverForInflating(view)
        viewModel.initializer {
            setupEvents(it)
        }
        viewModel.title.value = arguments?.getString(INFORMATIVE_DIALOG_TITLE, null)
        viewModel.text.value = arguments?.getString(INFORMATIVE_DIALOG_TEXT, null)
        viewModel.supportTextOne.value =
            arguments?.getString(INFORMATIVE_DIALOG_SUPPORT_TEXT_ONE, null)
        viewModel.supportTextTwo.value =
            arguments?.getString(INFORMATIVE_DIALOG_SUPPORT_TEXT_TWO, null)
        viewModel.icon.value = {
            val iconTemp = arguments?.getInt(INFORMATIVE_DIALOG_ICON, -1)
            if (iconTemp != -1) {
                iconTemp
            } else {
                null
            }
        }()
        viewModel.buttonPositiveText.value =
            arguments?.getString(INFORMATIVE_DIALOG_BUTTON_TEXT, null)
            ?: context?.getString(R.string.ok)
        viewModel.buttonMiddleText.value =
            arguments?.getString(INFORMATIVE_DIALOG_MIDDLE_BUTTON_TEXT, null)
        viewModel.buttonNegativeText.value =
            arguments?.getString(INFORMATIVE_DIALOG_NEGATIVE_BUTTON_TEXT, null)
            ?: context?.getString(R.string.no)
        viewModel.showNegativeButton.value =
            arguments?.getBoolean(INFORMATIVE_DIALOG_MULTIPLE_CHOOSE, false)

        viewModel.hideTitle.value =
            arguments?.getBoolean(BOOLEAN_SHOW_INFORMATIVE_DIALOG_TITLE, false)
        viewModel.hideMessage.value =
            arguments?.getBoolean(BOOLEAN_SHOW_INFORMATIVE_DIALOG_TEXT, false)
        viewModel.hideIcon.value =
            arguments?.getBoolean(BOOLEAN_SHOW_INFORMATIVE_DIALOG_ICON, false)
        viewModel.hideSupportTextOne.value =
            arguments?.getBoolean(BOOLEAN_SHOW_SUPPORT_TEXT_ONE, true)
        viewModel.hideSupportTextTwo.value =
            arguments?.getBoolean(BOOLEAN_SHOW_SUPPORT_TEXT_TWO, true)
        viewModel.showPositiveButton.value =
            arguments?.getBoolean(BOOLEAN_SHOW_POSITIVE_BUTTON, true)
    }

    private fun setupEvents(it: View) {
        when (it.id) {
            R.id.bt_positive_dialog_informative -> {
                onPositiveButton?.invoke()
                closeWithButton = true
                dismiss()
            }
            R.id.bt_middle_dialog_informative -> {
                onMiddleButton?.invoke()
                closeWithButton = true
                dismiss()
            }
            R.id.bt_negative_dialog_informative -> {
                onNegativeButton?.invoke()
                closeWithButton = true
                dismiss()
            }
        }
    }

    private val bottomSheetBehaviorCallback: BottomSheetCallback = object : BottomSheetCallback() {
        override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {
            if (newState == STATE_COLLAPSED || newState == STATE_HIDDEN) {
                dismiss()
            }
        }

        override fun onSlide(@NonNull bottomSheet: View, slideOffset: Float) {
            /*empty*/
        }
    }

    private fun addScreenHeightObserverForInflating(view: View?) {
        view?.viewTreeObserver?.addOnGlobalLayoutListener(
            object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    view.viewTreeObserver?.removeOnGlobalLayoutListener(this)
                    val dialog = dialog as? BottomSheetDialog?
                    val bottomSheet =
                        dialog?.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as? FrameLayout
                    val behavior: BottomSheetBehavior<*>? = bottomSheet?.let { bottomSheetLet ->
                        BottomSheetBehavior.from(bottomSheetLet)
                    }
                    behavior?.state = BottomSheetBehavior.STATE_EXPANDED
                    behavior?.peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO
                    behavior?.setBottomSheetCallback(bottomSheetBehaviorCallback)
                }
            }
        )
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismiss?.invoke(this, closeWithButton)
    }

    override fun tag(): String =
        arguments?.getString(Build.INFORMATIVE_DIALOG_TAG, null)
            ?: throw IllegalStateException("Tag is not defined")

    companion object : DialogNewInstance<InformativeDialog>(::InformativeDialog)

    class Build {

        companion object {
            const val INFORMATIVE_DIALOG_TITLE = "INFORMATIVE_DIALOG_TITLE"
            const val INFORMATIVE_DIALOG_TEXT = "INFORMATIVE_DIALOG_TEXT"
            const val INFORMATIVE_DIALOG_ICON = "INFORMATIVE_DIALOG_ICON"
            const val BOOLEAN_SHOW_INFORMATIVE_DIALOG_TITLE =
                "BOOLEAN_SHOW_INFORMATIVE_DIALOG_TITLE"
            const val INFORMATIVE_DIALOG_SUPPORT_TEXT_ONE = "INFORMATIVE_DIALOG_SUPPORT_TEXT_ONE"
            const val INFORMATIVE_DIALOG_SUPPORT_TEXT_TWO = "INFORMATIVE_DIALOG_SUPPORT_TEXT_TWO"
            const val BOOLEAN_SHOW_INFORMATIVE_DIALOG_TEXT = "BOOLEAN_SHOW_INFORMATIVE_DIALOG_TEXT"
            const val BOOLEAN_SHOW_INFORMATIVE_DIALOG_ICON = "BOOLEAN_SHOW_INFORMATIVE_DIALOG_ICON"
            const val BOOLEAN_SHOW_SUPPORT_TEXT_ONE = "BOOLEAN_SHOW_SUPPORT_TEXT_ONE"
            const val BOOLEAN_SHOW_SUPPORT_TEXT_TWO = "BOOLEAN_SHOW_SUPPORT_TEXT_TWO"
            const val INFORMATIVE_DIALOG_TAG = "INFORMATIVE_DIALOG_TAG"
            const val INFORMATIVE_DIALOG_STATUS_BAR_COLOR = "INFORMATIVE_DIALOG_STATUS_BAR_COLOR"
            const val INFORMATIVE_DIALOG_BUTTON_TEXT = "INFORMATIVE_DIALOG_BUTTON_TEXT"
            const val INFORMATIVE_DIALOG_MIDDLE_BUTTON_TEXT =
                "INFORMATIVE_DIALOG_MIDDLE_BUTTON_TEXT"
            const val INFORMATIVE_DIALOG_NEGATIVE_BUTTON_TEXT =
                "INFORMATIVE_DIALOG_NEGATIVE_BUTTON_TEXT"
            const val INFORMATIVE_DIALOG_MULTIPLE_CHOOSE = "INFORMATIVE_DIALOG_MULTIPLE_CHOOSE"
            const val BOOLEAN_SHOW_POSITIVE_BUTTON = "BOOLEAN_SHOW_POSITIVE_BUTTON"

            fun buildInformativeDialog(comp: Build.() -> Unit): InformativeDialog =
                Build().run {
                    comp()
                    val dialog = InformativeDialog.newInstance(
                        Bundle().apply {
                            putString(INFORMATIVE_DIALOG_TITLE, title)
                            putString(INFORMATIVE_DIALOG_TEXT, text)
                            putString(
                                INFORMATIVE_DIALOG_SUPPORT_TEXT_ONE,
                                informativeDialogSupportTextOne
                            )
                            putString(
                                INFORMATIVE_DIALOG_SUPPORT_TEXT_TWO,
                                informativeDialogSupportTextTwo
                            )
                            putString(INFORMATIVE_DIALOG_BUTTON_TEXT, buttonPositiveText)
                            putString(INFORMATIVE_DIALOG_MIDDLE_BUTTON_TEXT, buttonMiddleText)
                            putString(INFORMATIVE_DIALOG_NEGATIVE_BUTTON_TEXT, buttonNegativeText)
                            putBoolean(INFORMATIVE_DIALOG_MULTIPLE_CHOOSE, multipleChoose)
                            putBoolean(BOOLEAN_SHOW_INFORMATIVE_DIALOG_TITLE, hideTitle)
                            putBoolean(BOOLEAN_SHOW_INFORMATIVE_DIALOG_TEXT, hideMessage)
                            putBoolean(BOOLEAN_SHOW_INFORMATIVE_DIALOG_ICON, hideIcon)
                            putBoolean(BOOLEAN_SHOW_SUPPORT_TEXT_ONE, hideSupportTextOne)
                            putBoolean(BOOLEAN_SHOW_SUPPORT_TEXT_TWO, hideSupportTextTwo)
                            putBoolean(BOOLEAN_SHOW_POSITIVE_BUTTON, showPositiveButton)

                            putString(INFORMATIVE_DIALOG_TAG, tag)
                            putString(INFORMATIVE_DIALOG_STATUS_BAR_COLOR, statusBarColor.name)
                            icon?.let {
                                putInt(INFORMATIVE_DIALOG_ICON, it)
                            }
                        }
                    )
                    dialog.onDismiss = onDismiss
                    dialog.onPositiveButton = onPositiveButtom
                    dialog.onMiddleButton = onMiddleButton
                    dialog.onNegativeButton = onNegativeButton
                    dialog
                }

            fun CoreBaseActivity.buildInformativeAndShow(comp: Build.() -> Unit): InformativeDialog {
                val dialog = buildInformativeDialog(comp)
                dialog.show(supportFragmentManager, TAG_INFORMATIVE)
                return dialog
            }
        }

        var title: String? = null
        var text: String? = null

        var informativeDialogSupportTextOne: String? = null
        var informativeDialogSupportTextTwo: String? = null

        @DrawableRes
        var icon: Int? = null
        var buttonPositiveText: String? = null
        var buttonMiddleText: String? = null
        var buttonNegativeText: String? = null
        var tag: String = TAG_INFORMATIVE
        var multipleChoose: Boolean = false

        var onDismiss: OnDismissDialog? = null
        var onPositiveButtom: GenericEvent? = null
        var onMiddleButton: GenericEvent? = null
        var onNegativeButton: GenericEvent? = null

        var hideIcon: Boolean = false
        var hideTitle: Boolean = false
        var hideMessage: Boolean = false
        var hideSupportTextOne: Boolean = true
        var hideSupportTextTwo: Boolean = true
        var showPositiveButton: Boolean = true
        var statusBarColor: StatusBarColor = StatusBarColor.WHITE
    }
}