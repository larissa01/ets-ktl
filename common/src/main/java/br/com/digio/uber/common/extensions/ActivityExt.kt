package br.com.digio.uber.common.extensions

import android.annotation.TargetApi
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import androidx.navigation.fragment.NavHostFragment
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.dialogs.LoadingDialog
import br.com.digio.uber.domain.usecase.login.LogoutUseCase
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.castOrNull
import timber.log.Timber
import java.io.File

const val TAG_INFORMATIVE = "InformativeDialog"

@TargetApi(Build.VERSION_CODES.M)
fun Activity.setLightStatusTextBar() {
    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
}

fun Activity.setDarkStatusTextBar() {
    window.decorView.systemUiVisibility = 0
}

fun Activity.makeStatusBarTransparent() {
    window.apply {
        clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else {
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
        statusBarColor = Color.TRANSPARENT
    }
}

fun FragmentActivity.isActivityRunningFragment(): Boolean =
    (!isFinishing && !isDestroyed)

fun <T : ViewDataBinding> BaseViewModelActivity<T, *>.associateViewModel(
    logoutUseCase: LogoutUseCase
) {
    viewModel?.message?.observe(
        this,
        Observer { error ->
            error?.let { errorLet ->
                showMessage(errorLet)
                viewModel?.message?.value = null
            }
        }
    )

    viewModel?.showLoading?.observe(
        this,
        Observer {
            if (it == true) {
                LoadingDialog.showDialog(this)
            } else {
                LoadingDialog.dismissDialog()
            }
        }
    )

    viewModel?.let { viewModelLet ->
        viewModelLet.makeLogout.observe(
            this,
            Observer {
                if (it != null) {
                    executeLogout(viewModelLet, logoutUseCase, this, navigation)
                }
            }
        )
    }

    bindingVariable?.let { bindingVariable ->
        binding?.setVariable(bindingVariable, viewModel)
    }
    binding?.lifecycleOwner = this
    binding?.executePendingBindings()
}

fun Activity.hideKeyboard() {
    val imm: InputMethodManager? = ContextCompat.getSystemService(
        this,
        InputMethodManager::class.java
    )
    imm?.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
}

fun Activity.shareFile(
    file: File,
    authority: String,
    type: String,
    title: String,
    extraSubject: String? = null
) {
    Intent(Intent.ACTION_SEND).apply {
        this.type = type
        putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(this@shareFile, authority, file))
        if (extraSubject != null) putExtra(Intent.EXTRA_SUBJECT, extraSubject)
        flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
    }.run { startActivity(Intent.createChooser(this, title)) }
}

fun Activity.seeFile(file: File, authority: String, title: String) {
    val path = FileProvider.getUriForFile(this@seeFile, authority, file)
    Intent(Intent.ACTION_VIEW).apply {
        setDataAndType(path, Const.MimeTypes.PDF_MIME_TYPE)
        flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
    }.run { startActivity(Intent.createChooser(this, title)) }
}

fun AppCompatActivity.navigateTo(@IdRes viewId: Int, direction: NavDirections) {
    try {
        supportFragmentManager.findFragmentById(viewId)
            ?.castOrNull<NavHostFragment>()
            ?.navController
            ?.navigate(direction)
    } catch (e: IllegalArgumentException) {
        Timber.e(e)
    } catch (e: IllegalStateException) {
        Timber.e(e)
    }
}

fun AppCompatActivity.navigateTo(@IdRes viewId: Int, idDirection: Int, bundle: Bundle) {
    try {
        supportFragmentManager.findFragmentById(viewId)
            ?.castOrNull<NavHostFragment>()
            ?.navController
            ?.navigate(idDirection, bundle)
    } catch (e: IllegalArgumentException) {
        Timber.e(e)
    } catch (e: IllegalStateException) {
        Timber.e(e)
    }
}

fun Activity.openInBrowserLink(url: String) {
    try {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    } catch (ex: ActivityNotFoundException) {
        Timber.e(ex)
    }
}