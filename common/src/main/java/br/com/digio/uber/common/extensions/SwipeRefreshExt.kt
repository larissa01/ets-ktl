package br.com.digio.uber.common.extensions

import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import br.com.digio.uber.common.typealiases.GenericEvent

@BindingAdapter("onRefreshListener")
fun SwipeRefreshLayout.setBindingOnRefreshListener(genericEvent: GenericEvent) {
    setOnRefreshListener(genericEvent)
}

@BindingAdapter("refresh")
fun SwipeRefreshLayout.refresh(value: Boolean) {
    isRefreshing = value
}