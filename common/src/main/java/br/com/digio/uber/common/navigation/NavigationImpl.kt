package br.com.digio.uber.common.navigation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import br.com.digio.uber.common.choice.Choice
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.model.store.StoreProduct
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.KEY_DOCUMENT
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.KEY_OLD_PASSWORD
import br.com.digio.uber.util.Const.RequestOnResult.FAQ.KEY_FAQ_CHAT
import br.com.digio.uber.util.loadClassOrNull

class NavigationImpl : Navigation {
    override fun navigationToSplash(context: Context) {
        loadClassOrNull<Activity>(Const.Activities.SPLASH_ACTIVITY)
            ?.also { goToActivity ->
                context.startActivity(
                    Intent(context, goToActivity).apply {
                        addFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or
                                Intent.FLAG_ACTIVITY_CLEAR_TASK or
                                Intent.FLAG_ACTIVITY_NEW_TASK
                        )
                    }
                )
            }
    }

    override fun navigationToMainActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.MAIN_ACTIVITY)
            ?.also { goToActivity ->
                val intent = Intent(activity, goToActivity)

                intent.addFlags(
                    Intent.FLAG_ACTIVITY_CLEAR_TOP or
                        Intent.FLAG_ACTIVITY_CLEAR_TASK or
                        Intent.FLAG_ACTIVITY_NEW_TASK
                )
                activity.startActivity(intent)
                activity.finish()
            }
    }

    override fun navigationToDigioTestActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.DIGIO_TEST_ACTIVITY)
            ?.also {
                activity.startActivity(Intent(activity, it))
            }
    }

    override fun navigationToSampleActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.SAMPLE_ACTIVITY)
            ?.also {
                activity.startActivity(Intent(activity, it))
            }
    }

    override fun navigationToSampleNavigationActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.SAMPLE_NAVIGATION_ACTIVITY)
            ?.also {
                activity.startActivity(Intent(activity, it))
            }
    }

    override fun navigationToTransferActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.TRANSFER_ACTIVITY)
            ?.also {
                activity.startActivity(Intent(activity, it))
            }
    }

    override fun navigationToFeatureNavigationActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.FEATURE_NAVIGATION_ACTIVITY)
            ?.also {
                activity.startActivity(Intent(activity, it))
            }
    }

    override fun navigationToBankSlipActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.BANK_SLIP_ACTIVITY)
            ?.also {
                activity.startActivity(Intent(activity, it))
            }
    }

    override fun navigationToDynamic(activity: Activity, uri: String, bundle: Bundle?) {
        loadClassOrNull<Activity>(uri)?.also {
            val i = Intent(activity, it)
            if (bundle != null) i.putExtras(bundle)
            activity.startActivity(i)
        }
    }

    override fun navigationToDynamicWithResult(
        activity: Activity,
        uri: String,
        bundle: Bundle?,
        requestCode: Int
    ) {
        loadClassOrNull<Activity>(uri)?.also {
            val i = Intent(activity, it)
            if (bundle != null) i.putExtras(bundle)
            activity.startActivityForResult(i, requestCode)
        }
    }

    override fun navigationToDynamicWithResult(
        fragment: Fragment,
        uri: String,
        bundle: Bundle?,
        requestCode: Int
    ) {
        loadClassOrNull<Activity>(uri)?.also {
            fragment.activity?.startActivityFromFragment(
                fragment,
                Intent(fragment.context, it).apply {
                    putExtras(bundle ?: Bundle())
                },
                requestCode
            )
        }
    }

    override fun navigationToLoginActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.LOGIN_ACTIVITY)
            ?.also {
                activity.startActivity(
                    Intent(activity, it).apply {
                        addFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or
                                Intent.FLAG_ACTIVITY_CLEAR_TASK or
                                Intent.FLAG_ACTIVITY_NEW_TASK
                        )
                    }
                )
                activity.finish()
            }
    }

    override fun navigationToFaqActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.FAQ_ACTIVITY)
            ?.also {
                activity.startActivity(Intent(activity, it))
            }
    }

    override fun navigationToChoiceActivity(
        activity: Activity,
        toolbarId: Int,
        choices: ArrayList<Choice>
    ) {
        loadClassOrNull<Activity>(Const.Activities.CHOICE_ACTIVITY)
            ?.also { goToActivity ->
                val intent = Intent(activity, goToActivity)

                intent.putExtra(Const.Extras.TOOLBAR_ID, toolbarId)
                intent.putExtra(Const.Extras.CHOICES, choices)

                activity.startActivity(intent)
            }
    }

    override fun navigateToFacialBiometryCaptureActivity(fragment: Fragment, tag: String?) {
        loadClassOrNull<Activity>(Const.Activities.FACIAL_BIOMETRY_ACTIVITY)
            ?.also {
                fragment.activity?.startActivityFromFragment(
                    fragment,
                    Intent(fragment.context, it).apply {
                        putExtra(Const.RequestOnResult.FacialBiometry.KEY_TAG_FIREBASE, tag)
                    },
                    Const.RequestOnResult.FacialBiometry.REQUEST_FACIAL_CAPTURE
                )
            }
    }

    override fun navigateToFacialBiometryCaptureActivity(activity: Activity, tag: String?) {
        loadClassOrNull<Activity>(Const.Activities.FACIAL_BIOMETRY_ACTIVITY)
            ?.also {
                activity.startActivityForResult(
                    Intent(activity, it).apply {
                        putExtra(Const.RequestOnResult.FacialBiometry.KEY_TAG_FIREBASE, tag)
                    },
                    Const.RequestOnResult.FacialBiometry.REQUEST_FACIAL_CAPTURE
                )
            }
    }

    override fun navigationToRechargeActivity(activity: Activity, product: StoreProduct) {
        loadClassOrNull<Activity>(Const.Activities.RECHARGE_ACTIVITY)
            ?.also {
                val intent = Intent(activity, it)
                intent.putExtra(Const.Extras.STORE_PRODUCT, product)

                activity.startActivity(intent)
            }
    }

    override fun navigateToResetPasswordActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.RESET_PASSWORD_ACTIVITY)
            ?.also {
                activity.startActivity(Intent(activity, it))
            }
    }

    override fun navigateToChangePasswordActivity(
        fragment: Fragment,
        screen: String,
        oldPassword: String?,
        document: String?
    ) {
        loadClassOrNull<Activity>(Const.Activities.CHANGE_PASSWORD_ACTIVITY)
            ?.also { activity ->
                fragment.activity?.startActivityFromFragment(
                    fragment,
                    Intent(fragment.context, activity).apply {
                        putExtra(Const.RequestOnResult.ChangePassword.KEY_SCREEN, screen)
                        putExtra(KEY_DOCUMENT, document)
                        putExtra(KEY_OLD_PASSWORD, oldPassword)
                    },
                    Const.RequestOnResult.ChangePassword.CHANGE_PASSWORD_REQUEST_CODE
                )
            }
    }

    override fun navigationToPaymentActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.PAYMENT_ACTIVITY)
            ?.also {
                activity.startActivity(Intent(activity, it))
            }
    }

    override fun navigationToVeloeActivity(activity: Activity, product: StoreProduct) {
        loadClassOrNull<Activity>(Const.Activities.VELOE_ACTIVITY)
            ?.also {
                val intent = Intent(activity, it)
                intent.putExtra(Const.Extras.STORE_PRODUCT, product)

                activity.startActivity(intent)
            }
    }

    override fun navigationToIncomeActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.INCOME_ACTIVITY)?.also {
            activity.startActivity(Intent(activity, it))
        }
    }

    override fun navigationToPidActivity(activity: Activity, pidType: PidType, requestCode: Int?) {
        loadClassOrNull<Activity>(Const.Activities.PID_ACTIVITY)
            ?.also {
                activity.startActivityForResult(
                    Intent(activity, it).apply {
                        putExtra(Const.RequestOnResult.PID.KEY_PID_TYPE, pidType.name)
                    },
                    requestCode ?: Const.RequestOnResult.PID.PID_REQUEST_CODE
                )
            }
    }

    override fun navigationToPidActivity(fragment: Fragment, pidType: PidType, requestCode: Int?) {
        loadClassOrNull<Activity>(Const.Activities.PID_ACTIVITY)
            ?.also {
                fragment.activity?.startActivityFromFragment(
                    fragment,
                    Intent(fragment.context, it).apply {
                        putExtra(Const.RequestOnResult.PID.KEY_PID_TYPE, pidType.name)
                    },
                    requestCode ?: Const.RequestOnResult.PID.PID_REQUEST_CODE
                )
            }
    }

    override fun navigationToCashbackActivity(activity: Activity, bundle: Bundle?) {
        loadClassOrNull<Activity>(Const.Activities.CASHBACK_ACTIVITY)?.also {
            val intent = Intent(activity, it).apply {
                bundle?.apply {
                    putExtras(this)
                }
            }

            activity.startActivity(intent)
        }
    }

    override fun navigationToShowCardPassword(activity: Activity, pidHash: String) {
        loadClassOrNull<Activity>(Const.Activities.SHOW_CARD_PASSWORD_ACTIVITY)
            ?.also {
                activity.startActivity(
                    Intent(activity, it).apply {
                        putExtra(
                            Const.RequestOnResult.ShowCardPassword.KEY_SHOW_CARD_PASSWORD_HASH,
                            pidHash
                        )
                    }
                )
            }
    }

    override fun navigationToQrCodeReaderActivity(
        activity: Activity,
        message: String,
        requestCode: Int?
    ) {
        loadClassOrNull<Activity>(Const.Activities.QRCODE_ACTIVITY)?.also {
            activity.startActivityForResult(
                Intent(activity, it).apply {
                    putExtra(Const.Extras.MESSAGE_QR_CODE, message)
                },
                requestCode ?: Const.RequestOnResult.QRCode.QR_REQUEST_CODE
            )
        }
    }

    override fun navigationToQrCodeReaderActivity(
        fragment: Fragment,
        message: String,
        requestCode: Int?
    ) {
        loadClassOrNull<Activity>(Const.Activities.QRCODE_ACTIVITY)?.also {
            fragment.activity?.startActivityFromFragment(
                fragment,
                Intent(fragment.context, it).apply {
                    putExtra(Const.Extras.MESSAGE_QR_CODE, message)
                },
                requestCode ?: Const.RequestOnResult.QRCode.QR_REQUEST_CODE
            )
        }
    }

    override fun navigationToStoreMyRequestsActivity(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.STORE_MY_REQUEST_ACTIVITY)?.also {
            activity.startActivity(Intent(activity, it))
        }
    }

    override fun navigationToFaqPdf(activity: Activity) {
        loadClassOrNull<Activity>(Const.Activities.FAQ_PDF)?.also {
            activity.startActivity(Intent(activity, it))
        }
    }

    override fun navigationToChangeRegistrationActivity(context: Context) {
        loadClassOrNull<Activity>(Const.Activities.CHANGE_REGISTRATION_ACTIVITY)
            ?.also {
                context.startActivity(Intent(context, it))
            }
    }

    override fun navigationToReceiptActivity(
        activity: Activity,
        requestCode: Int?,
        bundle: Bundle?
    ) {
        loadClassOrNull<Activity>(Const.Activities.RECEIPT_ACTIVITY)?.also {
            activity.startActivityForResult(
                Intent(activity, it).apply {
                    bundle?.apply {
                        putExtras(this)
                    }
                },
                requestCode ?: Const.RequestOnResult.Transfer.NEW_TRANSFER_REQUEST_CODE
            )
        }
    }

    override fun navigationToFaqDeepLinkActivity(
        context: Context,
        deepLink: String,
        data: Bundle?
    ) {
        loadClassOrNull<Activity>(Const.Activities.FAQ_DEEP_LINK_ACTIVITY)
            ?.also {
                context.startActivity(
                    Intent(context, it).apply {
                        putExtras(
                            (data ?: Bundle()).apply {
                                putString(
                                    Const.RequestOnResult.FaqDeepLink.KEY_FAQ_DEEP_LINK,
                                    deepLink
                                )
                            }
                        )
                    }
                )
            }
    }

    override fun navigationToFranchiseActivity(fragment: Fragment, requestCode: Int?) {
        loadClassOrNull<Activity>(Const.Activities.FRANCHISE_ACTIVITY)
            ?.also {
                fragment.activity?.startActivityFromFragment(
                    fragment,
                    Intent(fragment.context, it),
                    requestCode ?: 0
                )
            }
    }

    override fun navigationToPixMyKey(context: Context, cpf: String, incomeIndex: String) {
        loadClassOrNull<Activity>(Const.Activities.PIX_MY_KEY_ACTIVITY)?.also {
            context.startActivity(
                Intent(context, it).apply {
                    putExtra(Const.Extras.ARG_CPF, cpf)
                    putExtra(Const.Extras.ARG_INCOME, incomeIndex)
                }
            )
        }
    }

    override fun navigationToPixMenuPayment(context: Context, cpf: String, incomeIndex: String) {
        loadClassOrNull<Activity>(Const.Activities.PIX_MENU_PAYMENT_ACTIVITY)?.also {
            context.startActivity(
                Intent(context, it).apply {
                    putExtra(Const.Extras.ARG_CPF, cpf)
                    putExtra(Const.Extras.ARG_INCOME, incomeIndex)
                }
            )
        }
    }

    override fun navigationToPixSummaryPayment(activity: Activity, bundle: Bundle, requestCode: Int) {
        loadClassOrNull<Activity>(Const.Activities.PIX_SUMMARY_PAYMENT_ACTIVITY)?.also {
            activity.startActivityForResult(
                Intent(activity, it).apply {
                    putExtras(bundle)
                },
                requestCode
            )
        }
    }

    override fun navigationToPixReceipt(context: Context, bundle: Bundle) {
        loadClassOrNull<Activity>(Const.Activities.PIX_RECEIPT_ACTIVITY)?.also {
            context.startActivity(
                Intent(context, it).apply {
                    putExtras(bundle)
                }
            )
        }
    }

    override fun navigationToFaqChatActivity(context: Context, tag: String) {
        loadClassOrNull<Activity>(Const.Activities.FAQ_CHAT_ACTIVITY)?.also {
            context.startActivity(
                Intent(context, it).apply {
                    putExtra(KEY_FAQ_CHAT, tag)
                }
            )
        }
    }
}