package br.com.digio.uber.common.component

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Interpolator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import kotlin.math.max

class CirclePagerIndicatorDecoration(
    private val colorInactive: Int,
    private val colotActive: Int
) : ItemDecoration() {

    private val density: Float by lazy {
        android.content.res.Resources.getSystem().displayMetrics.density
    }

    private val mIndicatorHeight = (density * DP_16).toInt()
    private val mIndicatorStrokeWidth = density * DP_2
    private val mIndicatorItemLength = density * DP_16
    private val mIndicatorItemPadding = density * DP_4
    private val mInterpolator: Interpolator = AccelerateDecelerateInterpolator()
    private val paint: Paint = Paint()

    init {
        paint.strokeCap = Paint.Cap.ROUND
        paint.strokeWidth = mIndicatorStrokeWidth
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
    }

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDrawOver(c, parent, state)
        val itemCount = parent.adapter?.itemCount ?: 0

        val totalLength = mIndicatorItemLength * itemCount
        val paddingBetweenItems = max(0, itemCount - 1) * mIndicatorItemPadding
        val indicatorTotalWidth = totalLength + paddingBetweenItems
        val indicatorStartX = (parent.width - indicatorTotalWidth) / 2f

        val indicatorPosY = parent.height - mIndicatorHeight / 2f
        drawInactiveIndicators(c, indicatorStartX, indicatorPosY, itemCount)

        val layoutManager = parent.layoutManager as LinearLayoutManager?
        val activePosition = layoutManager?.findFirstVisibleItemPosition() ?: 0
        if (activePosition == RecyclerView.NO_POSITION) {
            return
        }

        val activeChild: View? = layoutManager?.findViewByPosition(activePosition)
        val left: Int = activeChild!!.left
        val width: Int = activeChild.width

        val progress: Float = mInterpolator.getInterpolation(left * -1 / width.toFloat())
        drawActiveIndicatiors(c, indicatorStartX, indicatorPosY, activePosition, progress)
    }

    private fun drawInactiveIndicators(
        c: Canvas,
        indicatorStartX: Float,
        indicatorPosY: Float,
        itemCount: Int
    ) {
        paint.color = colorInactive

        val itemWidth = mIndicatorItemLength + mIndicatorItemPadding
        var start = indicatorStartX
        for (i in 0 until itemCount) {
            c.drawCircle(start + mIndicatorItemLength, indicatorPosY, itemWidth / DP_6, paint)
            start += itemWidth
        }
    }

    private fun drawActiveIndicatiors(
        c: Canvas,
        indicatorStartX: Float,
        indicatorPosY: Float,
        highlightPosition: Int,
        progress: Float
    ) {
        paint.color = colotActive

        val itemWidth = mIndicatorItemLength + mIndicatorItemPadding
        if (progress == 0f) {
            val highlightStart = indicatorStartX + itemWidth * highlightPosition
            c.drawCircle(highlightStart, indicatorPosY, itemWidth / DP_6, paint)
        } else {
            val highlightStart = indicatorStartX + itemWidth * highlightPosition

            c.drawCircle(highlightStart + mIndicatorItemLength, indicatorPosY, itemWidth / DP_6, paint)
        }
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.bottom = mIndicatorHeight
    }

    companion object {
        const val DP_2 = 2
        const val DP_4 = 4
        const val DP_6 = 6
        const val DP_16 = 16
    }
}