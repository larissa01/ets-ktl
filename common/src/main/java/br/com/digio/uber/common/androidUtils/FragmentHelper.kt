package br.com.digio.uber.common.androidUtils

import androidx.fragment.app.FragmentManager
import br.com.digio.uber.common.base.fragment.BaseNavigationFragment

fun BaseNavigationFragment.attachFragmentWithAnimationAllowingStateLoss(
    fragmentManager: FragmentManager,
    containerId: Int,
    addToBackStack: Boolean,
    animationTransactionObject: AnimationTransactionObject?,
    replace: Boolean = false
) {
    val fragmentTransaction = fragmentManager.beginTransaction()

    animationTransactionObject?.let {
        fragmentTransaction.setCustomAnimations(
            it.enterFragment,
            it.exitFragment,
            it.popEnter,
            it.popExit
        )
    }

    if (addToBackStack && !replace) {
        fragmentTransaction.addToBackStack(tag())
    } else if (replace) {
        fragmentTransaction.addToBackStack(null)
    }

    if (replace) {
        fragmentTransaction.replace(containerId, this, tag())
    } else {
        fragmentTransaction.add(containerId, this, tag())
    }
    fragmentTransaction.commitAllowingStateLoss()
}