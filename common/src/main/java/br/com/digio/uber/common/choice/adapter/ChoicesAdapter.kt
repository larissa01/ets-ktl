package br.com.digio.uber.common.choice.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.choice.Choice
import br.com.digio.uber.common.databinding.ViewHolderChoicesBinding

typealias OnClickChoice = (item: Choice) -> Unit

class ChoicesAdapter(
    private val choices: List<Choice>,
    private val listener: OnClickChoice
) : RecyclerView.Adapter<ChoicesAdapter.ChoicesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChoicesViewHolder {
        val binding = ViewHolderChoicesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ChoicesViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: ChoicesViewHolder, position: Int) = holder.bind(choices, position)

    override fun getItemCount() = choices.size

    class ChoicesViewHolder(private val binding: ViewHolderChoicesBinding, val listener: OnClickChoice) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(choices: List<Choice>, position: Int) {
            val choice = choices[position]
            binding.choice = choice
            binding.executePendingBindings()
            binding.root.setOnClickListener { listener(choice) }
            if (position == (choices.size - 1)) {
                binding.divider.visibility = View.GONE
            }
        }
    }
}