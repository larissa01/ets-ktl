package br.com.digio.uber.common.base.activity

import android.os.Bundle
import androidx.annotation.LayoutRes

abstract class BaseActivity : CoreBaseActivity() {

    @LayoutRes
    open fun getLayoutId(): Int? = null

    override fun initialize(savedInstanceState: Bundle?) {
        getLayoutId()?.let { layoutId ->
            setContentView(layoutId)
        }
    }
}