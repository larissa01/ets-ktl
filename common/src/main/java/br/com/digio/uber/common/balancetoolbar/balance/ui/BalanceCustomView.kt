package br.com.digio.uber.common.balancetoolbar.balance.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import br.com.digio.uber.common.R
import br.com.digio.uber.common.balancetoolbar.balance.uiModel.BalanceUiModel
import br.com.digio.uber.common.balancetoolbar.balance.viewModel.BalanceViewModel
import br.com.digio.uber.common.databinding.LayoutBalanceBinding
import br.com.digio.uber.common.extensions.setTooltipCompat
import br.com.digio.uber.util.safeHeritage
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@ExperimentalCoroutinesApi
@KoinApiExtension
class BalanceCustomView constructor(
    context: Context,
    attributeSet: AttributeSet? = null
) : FrameLayout(context, attributeSet), KoinComponent, LifecycleOwner {

    private val viewModel: BalanceViewModel by inject()
    private val binding by lazy {
        LayoutBalanceBinding.inflate(
            LayoutInflater.from(context),
            this,
            true
        )
    }

    override fun getLifecycle(): Lifecycle =
        context.safeHeritage<LifecycleOwner>()?.lifecycle ?: throw NotImplementedError()

    init {
        binding.balanceViewModel = viewModel
        viewModel.initializer { onItemClicked(it) }
        binding.lifecycleOwner = this
        binding.btnBalanceVisibility.setOnCheckedChangeListener { _, isChecked ->
            viewModel.balanceVisibilityChanged(isChecked)
        }
    }

    fun getBalanceLiveData(): LiveData<BalanceUiModel>? = viewModel.balanceUiModel

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.btn_balance_block -> {
                v.setTooltipCompat(context.getString(R.string.balance_blocked_info))
            }
        }
    }
}