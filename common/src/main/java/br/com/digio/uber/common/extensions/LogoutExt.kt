package br.com.digio.uber.common.extensions

import android.content.Context
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.common.navigation.Navigation
import br.com.digio.uber.domain.usecase.login.LogoutUseCase

fun executeLogout(
    viewModel: BaseViewModel,
    logoutUseCase: LogoutUseCase,
    contextLet: Context,
    navigation: Navigation
) {
    val executeDialogLogout = {
        viewModel.message.value = viewModel.makeLogout.value?.apply {
            onCloseDialog = {
                navigation.navigationToSplash(contextLet)
            }
        }
        viewModel.makeLogout.value = null
    }

    viewModel.singleExecParametter(
        logoutUseCase(Unit),
        onError = { _, _, _ ->
            executeDialogLogout()
            EmptyErrorObject
        },
        onSuccessBaseViewModel = {
            executeDialogLogout()
        }
    )
}