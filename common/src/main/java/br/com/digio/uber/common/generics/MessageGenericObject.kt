package br.com.digio.uber.common.generics

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import br.com.digio.uber.common.R
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.common.typealiases.GenericEvent
import br.com.digio.uber.common.typealiases.OnDismissDialog
import timber.log.Timber

abstract class MessageGenericObject {
    abstract var type: TypeMessage

    @DrawableRes
    open var icon: Int = R.drawable.ic_error

    open var title: Int = R.string.generic_error_message_title

    open var titleString: String? = null

    open var message: Int = R.string.generic_error_message_message

    open var messageString: String? = null

    open var buttonText: Int = R.string.ok

    open var buttonTextString: String? = null

    open var close: Boolean = false

    open var callGoBack: Boolean = false

    open var onCloseDialog: OnDismissDialog? = null

    open var onPositiveButtonClick: GenericEvent? = null

    open var hideIcon: Boolean = false

    open var hideTitle: Boolean = false

    open var hideMessage: Boolean = false

    open var showPositiveButton: Boolean = true
    open var showNegativeButton: Boolean = true

    open var statusBarColor: StatusBarColor = StatusBarColor.WHITE
}

open class ErrorObject constructor(
    ex: Throwable? = null
) : MessageGenericObject() {

    init {
        Timber.d(ex)
    }

    override var type: TypeMessage = TypeMessage.ERROR
}

data class ErrorNetwork constructor(
    val ex: Throwable? = null
) : ErrorObject(ex) {

    init {
        Timber.d(ex)
    }

    override var title: Int = R.string.generic_error_message_network_title
    override var message: Int = R.string.generic_error_message_network_message
    override var type: TypeMessage = TypeMessage.ERROR
}

object EmptyErrorObject : MessageGenericObject() {
    override var type: TypeMessage = TypeMessage.EMPTY_ERROR
}

class MessageSuccessObject : MessageGenericObject() {
    override var type: TypeMessage = TypeMessage.SUCCESS

    override var icon: Int = R.drawable.ic_success
}

class ChooseMessageObject : MessageGenericObject() {

    @StringRes
    var positiveOptionTextInt: Int = R.string.yes
    val positiveOptionTextString: String? = null

    @StringRes
    var negativeOptionTextInt: Int = R.string.no
    val negativeOptionTextString: String? = null

    override var type: TypeMessage = TypeMessage.CHOOSE

    var onNegativeButtonClick: GenericEvent? = null
}

class StatementMessageObject : MessageGenericObject() {

    @StringRes
    var positiveOptionTextInt: Int = R.string.see_receipt
    val positiveOptionTextString: String? = null

    @StringRes
    var negativeOptionTextInt: Int = R.string.cancel_schedule
    val negativeOptionTextString: String? = null

    var amountValue: String = ""
    var dateValue: String = ""
    var hideSupportTextOne: Boolean = false
    var hideSupportTextTwo: Boolean = false
    override var type: TypeMessage = TypeMessage.STATEMENT
    var onNegativeButtonClick: GenericEvent? = null

    override var onPositiveButtonClick: GenericEvent? = null
}

class ThreeOptionsMessageObject : MessageGenericObject() {

    @StringRes
    var positiveOptionTextInt: Int = R.string.yes
    var positiveOptionTextString: String? = null

    @StringRes
    var middleOptionTextInt: Int = R.string.no
    var middleOptionTextString: String? = null

    @StringRes
    var negativeOptionTextInt: Int = R.string.no
    var negativeOptionTextString: String? = null

    override var type: TypeMessage = TypeMessage.THREE_OPTIONS

    var onMiddleButtonClick: GenericEvent? = null
    var onNegativeButtonClick: GenericEvent? = null
}

enum class TypeMessage {
    ERROR,
    SUCCESS,
    EMPTY_ERROR,
    CHOOSE,
    THREE_OPTIONS,
    STATEMENT
}

fun makeErrorObject(ex: Exception? = null, onErrorObjectCreate: ErrorObject.() -> Unit): ErrorObject =
    ErrorObject(ex).apply(onErrorObjectCreate)

fun makeMessageSuccessObject(
    onMessageSuccessObjectDefine: MessageSuccessObject.() -> Unit
): MessageSuccessObject =
    MessageSuccessObject().apply(onMessageSuccessObjectDefine)

fun makeChooseMessageObject(
    onChooseMessageObjectDefine: ChooseMessageObject.() -> Unit
): ChooseMessageObject =
    ChooseMessageObject().apply(onChooseMessageObjectDefine)

fun makeThreeOptionsMessageObject(
    threeOptionsMessageObjectDefine: ThreeOptionsMessageObject.() -> Unit
): ThreeOptionsMessageObject =
    ThreeOptionsMessageObject().apply(threeOptionsMessageObjectDefine)

fun makeStatementMessageObject(
    onStatementMessageObjectDefine: StatementMessageObject.() -> Unit
): StatementMessageObject =
    StatementMessageObject().apply(onStatementMessageObjectDefine)