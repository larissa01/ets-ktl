package br.com.digio.uber.common.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import br.com.digio.uber.common.androidUtils.AnimationTransactionObject
import br.com.digio.uber.common.base.dialog.BaseNavigationWithFlowDialog
import br.com.digio.uber.common.base.fragment.BaseNavigationWithFlowFragment
import br.com.digio.uber.common.listener.BaseNavigationWithFlowListeners
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.listener.NavigationListenerWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByNameDialog
import br.com.digio.uber.util.safeHeritage

abstract class GenericNavigationWithFlow constructor(
    private val activity: AppCompatActivity
) : GenericNavigation(activity), NavigationListenerWithFlow {

    private val mapOfFragmentResult: MutableMap<BaseNavigationWithFlowListeners, BaseNavigationWithFlowListeners> =
        emptyMap<BaseNavigationWithFlowListeners, BaseNavigationWithFlowListeners>().toMutableMap()

    abstract fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>>

    open fun getAnimationTransactionObject(): AnimationTransactionObject? =
        AnimationTransactionObject

    /**
     * Should be used when using dialog instead of fragment
     */
    open fun getFlowDialog(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByNameDialog>> =
        emptyList()

    override fun getMaxNavigationItems(): Int =
        getFlow().size

    protected open fun nextStep(goTo: GoTo): Fragment? {
        val fragment = getFlow().firstOrNull { it.first == goTo.fragmentName }
        val dialog = getFlowDialog().firstOrNull { it.first == goTo.fragmentName }
        return when {
            fragment != null -> nextFragment(
                fragment,
                goTo
            )
            dialog != null -> nextDialog(dialog, goTo.bundle)
            else -> {
                activity.finish()
                null
            }
        }
    }

    private fun nextDialog(
        pair: Pair<String, GenericNavigationWIthFlowGetFragmentByNameDialog>,
        bundle: Bundle?
    ): BaseNavigationWithFlowDialog? =
        pair.second.invoke(bundle)?.let {
            showDialg(it)
            it
        }

    private fun nextFragment(
        pair: Pair<String, GenericNavigationWIthFlowGetFragmentByName>,
        goTo: GoTo
    ): BaseNavigationWithFlowFragment? =
        pair.second.invoke(goTo.bundle)?.let {
            showFragment(
                it,
                animationTransactionObject = goTo.animationTransactionObject,
                addToBackStack = goTo.addToBackStack,
                replace = goTo.replace
            )
            it
        }

    override fun goTo(goTo: GoTo) {
        if (goTo.fragmentForResult && goTo.currentFragment != null) {
            nextStep(goTo)?.safeHeritage<BaseNavigationWithFlowListeners>()?.let { nextFragment ->
                mapOfFragmentResult[goTo.currentFragment] = nextFragment
            }
        } else {
            nextStep(goTo)
        }
    }

    open fun onBackPressed(bundle: Bundle?, currentFragment: BaseNavigationWithFlowListeners?) {
        super.onBackPressed()

        mapOfFragmentResult.mapNotNull {
            if (it.value == currentFragment) {
                it
            } else {
                null
            }
        }.firstOrNull()?.let {
            it.key.fragmentResult(bundle, it.value.getTagRegistration(), it.value.resultCode)
            mapOfFragmentResult.remove(it.key)
        }
    }

    override fun back(bundle: Bundle?, currentFragment: BaseNavigationWithFlowListeners?) {
        onBackPressed(bundle, currentFragment)
    }

    override fun clearAllBackStack() {
        for (i in 0..activity.supportFragmentManager.backStackEntryCount) {
            activity.supportFragmentManager.popBackStack()
        }
    }
}