package br.com.digio.uber.common.listener
import android.os.Bundle
import br.com.digio.uber.common.androidUtils.AnimationTransactionObject

interface NavigationListenerWithFlow {

    fun goTo(goTo: GoTo)

    fun back(bundle: Bundle?, currentFragment: BaseNavigationWithFlowListeners? = null)
    fun clearAllBackStack()
}

data class GoTo(
    val fragmentName: String,
    val bundle: Bundle?,
    val currentFragment: BaseNavigationWithFlowListeners? = null,
    val fragmentForResult: Boolean = false,
    val animationTransactionObject: AnimationTransactionObject? = null,
    val addToBackStack: Boolean = true,
    val replace: Boolean = false
)