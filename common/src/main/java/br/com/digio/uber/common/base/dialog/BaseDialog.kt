package br.com.digio.uber.common.base.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import br.com.digio.uber.common.R
import br.com.digio.uber.common.base.activity.CoreBaseActivity
import br.com.digio.uber.common.extensions.StatusBarUtils.returnToOldStatusBar
import br.com.digio.uber.common.extensions.StatusBarUtils.setColorStatusBar
import br.com.digio.uber.common.generics.MessageGenericObjectConfig
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.common.listener.ViewConfigger
import br.com.digio.uber.common.navigation.Navigation
import br.com.digio.uber.util.safeHeritage
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.koin.android.ext.android.inject
import kotlin.coroutines.CoroutineContext

abstract class BaseDialog : BottomSheetDialogFragment(), CoroutineScope, ViewConfigger, MessageGenericObjectConfig {

    val navigation: Navigation by inject()

    abstract fun tag(): String

    abstract fun getStatusBarAppearance(): StatusBarColor

    abstract fun initialize()

    open fun isSecurityScreen(): Boolean = false

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setStyle(DialogFragment.STYLE_NO_FRAME, R.style.Theme_UberDriverAccount_Dialog)

        if (isSecurityScreen()) {
            activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_SECURE)
        } else {
            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
        }

        activity?.setColorStatusBar(getStatusBarAppearance())
    }

    override fun onDismiss(dialog: DialogInterface) {
        activity?.returnToOldStatusBar()
        super.onDismiss(dialog)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }

    override fun getActivityForError(): CoreBaseActivity? = activity?.safeHeritage()

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
}