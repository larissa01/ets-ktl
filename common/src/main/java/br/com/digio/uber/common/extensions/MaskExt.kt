package br.com.digio.uber.common.extensions

import br.com.digio.uber.util.unmask

/**
 * @author Marlon D. Rocha
 * @since 30/09/20
 */

fun String.validateZipCode(zipCodeLength: Int = 8): Boolean =
    if (this.length > zipCodeLength) {
        val cepWithoutFormat = this.unmask()
        if (!("^\\b(\\d)\\1+\\b".toRegex().matches(cepWithoutFormat))) {
            "^\\d{5}-\\d{3}\$".toRegex().matches(this)
        } else {
            false
        }
    } else {
        false
    }