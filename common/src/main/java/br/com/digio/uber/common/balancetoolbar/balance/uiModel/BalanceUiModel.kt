package br.com.digio.uber.common.balancetoolbar.balance.uiModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BalanceUiModel(
    val balance: String,
    val blockedBalance: String?,
    val shouldShowBlockedBalance : Boolean?
) : Parcelable