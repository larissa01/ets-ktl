package br.com.digio.uber.common.androidUtils

import androidx.annotation.AnimRes
import br.com.digio.uber.common.R

object AnimationTransactionObject {

    @AnimRes
    var enterFragment: Int = R.anim.cross_navigation_pop_enter_slide_up

    @AnimRes
    var exitFragment: Int = R.anim.cross_navigation_pop_exit_slide_up

    @AnimRes
    var popEnter: Int = R.anim.cross_navigation_enter_slide_down

    @AnimRes
    var popExit: Int = R.anim.cross_navigation_exit_slide_down
}

enum class AnimationTransactionObjectTypes constructor(val animation: AnimationTransactionObject) {
    DOWN_TO_UP(
        AnimationTransactionObject.apply {
            enterFragment = R.anim.cross_navigation_pop_enter_slide_up
            exitFragment = R.anim.cross_navigation_pop_exit_slide_up
            popEnter = R.anim.cross_navigation_enter_slide_down
            popExit = R.anim.cross_navigation_exit_slide_down
        }
    ),
    LEFT_TO_RIGHT(
        AnimationTransactionObject.apply {
            enterFragment = R.anim.slide_in_right
            exitFragment = R.anim.slide_out_left
            popEnter = R.anim.slide_in_left
            popExit = R.anim.slide_out_right
        }
    )
}