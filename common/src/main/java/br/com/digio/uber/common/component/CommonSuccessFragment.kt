package br.com.digio.uber.common.component

import android.os.Bundle
import android.view.View
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.BR
import br.com.digio.uber.common.R
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.databinding.FragmentCommonSuccessBinding
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor

class CommonSuccessFragment : BaseViewModelFragment<FragmentCommonSuccessBinding, CommonSuccessViewModel>() {

    override val bindingVariable: Int? = BR.commonSuccessViewModel
    override val getLayoutId: Int? = R.layout.fragment_common_success
    override val viewModel: CommonSuccessViewModel = CommonSuccessViewModel()
    override fun tag(): String = CommonSuccessFragment::javaClass.name
    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    var onBtnGoBackClickedCallback: (CommonSuccessFragment.() -> Unit)? = null
    var shouldPop: Boolean? = false

    companion object {
        fun newInstance(
            arguments: Bundle,
            onBtnGoBackClickedCallback: CommonSuccessFragment.() -> Unit
        ): CommonSuccessFragment {
            val fragment = CommonSuccessFragment()
            fragment.arguments = arguments
            fragment.onBtnGoBackClickedCallback = onBtnGoBackClickedCallback
            return fragment
        }

        const val COMMON_SUCCESS_FRAGMENT_TITLE = "TITLE"
        const val COMMON_SUCCESS_FRAGMENT_MESSAGE = "MESSAGE"
        const val COMMON_SUCCESS_FRAGMENT_BTN_TEXT = "BTN_TEXT"
        const val COMMON_SUCCESS_FRAGMENT_SHOULD_POP = "SHOULD_POP"
    }

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it) }
        viewModel.title.value = arguments?.getString(COMMON_SUCCESS_FRAGMENT_TITLE) ?: getString(R.string.success)
        viewModel.messageSuccess.value = arguments?.getString(COMMON_SUCCESS_FRAGMENT_MESSAGE) ?: ""
        viewModel.btnText.value = arguments?.getString(COMMON_SUCCESS_FRAGMENT_BTN_TEXT)
            ?: getString(R.string.go_to_begin)
        shouldPop = arguments?.getBoolean(COMMON_SUCCESS_FRAGMENT_SHOULD_POP)
    }

    fun onItemClicked(v: View) {
        when (v.id) {
            R.id.btn_go_back -> {
                if (onBtnGoBackClickedCallback != null) {
                    onBtnGoBackClickedCallback?.invoke(this)
                } else {
                    if (shouldPop == true) {
                        pop()
                    } else {
                        activity?.finish()
                    }
                }
            }
        }
    }
}

class CommonSuccessViewModel : BaseViewModel() {
    val title = MutableLiveData<String>()
    val messageSuccess = MutableLiveData<String>()
    val btnText = MutableLiveData<String>()
}