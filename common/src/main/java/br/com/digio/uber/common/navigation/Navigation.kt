package br.com.digio.uber.common.navigation

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import br.com.digio.uber.common.choice.Choice
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.model.store.StoreProduct

interface Navigation {
    fun navigationToSplash(context: Context)
    fun navigationToMainActivity(activity: Activity)
    fun navigationToDigioTestActivity(activity: Activity)
    fun navigationToSampleActivity(activity: Activity)
    fun navigationToSampleNavigationActivity(activity: Activity)
    fun navigationToTransferActivity(activity: Activity)
    fun navigationToBankSlipActivity(activity: Activity)
    fun navigationToFeatureNavigationActivity(activity: Activity)
    fun navigationToDynamic(activity: Activity, uri: String, bundle: Bundle? = null)
    fun navigationToDynamicWithResult(activity: Activity, uri: String, bundle: Bundle? = null, requestCode: Int)
    fun navigationToDynamicWithResult(fragment: Fragment, uri: String, bundle: Bundle? = null, requestCode: Int)
    fun navigationToLoginActivity(activity: Activity)
    fun navigationToFaqActivity(activity: Activity)
    fun navigationToRechargeActivity(activity: Activity, product: StoreProduct)
    fun navigationToChoiceActivity(
        activity: Activity,
        toolbarId: Int,
        choices: ArrayList<Choice>
    )

    fun navigateToFacialBiometryCaptureActivity(fragment: Fragment, tag: String? = null)
    fun navigateToFacialBiometryCaptureActivity(activity: Activity, tag: String? = null)
    fun navigateToResetPasswordActivity(activity: Activity)
    fun navigateToChangePasswordActivity(
        fragment: Fragment,
        screen: String,
        oldPassword: String? = null,
        document: String? = null
    )
    fun navigationToPaymentActivity(activity: Activity)
    fun navigationToVeloeActivity(activity: Activity, product: StoreProduct)
    fun navigationToIncomeActivity(activity: Activity)
    fun navigationToPidActivity(activity: Activity, pidType: PidType, requestCode: Int? = null)
    fun navigationToPidActivity(fragment: Fragment, pidType: PidType, requestCode: Int? = null)
    fun navigationToStoreMyRequestsActivity(activity: Activity)
    fun navigationToCashbackActivity(activity: Activity, bundle: Bundle? = null)
    fun navigationToShowCardPassword(activity: Activity, pidHash: String)
    fun navigationToQrCodeReaderActivity(activity: Activity, message: String, requestCode: Int? = null)
    fun navigationToQrCodeReaderActivity(fragment: Fragment, message: String, requestCode: Int? = null)
    fun navigationToFaqPdf(activity: Activity)
    fun navigationToChangeRegistrationActivity(context: Context)
    fun navigationToReceiptActivity(activity: Activity, requestCode: Int? = null, bundle: Bundle? = null)
    fun navigationToFranchiseActivity(fragment: Fragment, requestCode: Int? = null)
    fun navigationToFaqDeepLinkActivity(context: Context, deepLink: String, data: Bundle? = null)
    fun navigationToPixMyKey(context: Context, cpf: String, incomeIndex: String)
    fun navigationToPixMenuPayment(context: Context, cpf: String, incomeIndex: String)
    fun navigationToPixSummaryPayment(activity: Activity, bundle: Bundle, requestCode: Int)
    fun navigationToPixReceipt(context: Context, bundle: Bundle)
    fun navigationToFaqChatActivity(context: Context, tag: String)
}