package br.com.digio.uber.common.component

import android.content.Context
import android.graphics.Canvas
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.Gravity
import android.view.MotionEvent
import br.com.digio.uber.util.toMoneyNumber
import br.com.digio.uber.util.unmaskMoney
import com.google.android.material.textfield.TextInputEditText
import java.lang.NumberFormatException

class ValueInputEditText : TextInputEditText {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    init {
        setText(DEFAULT_VALUE)
        gravity = Gravity.END
        inputType = InputType.TYPE_CLASS_NUMBER
        isLongClickable = false
        setPadding(paddingLeft, 0, paddingBottom, 0)

        setOnClickListener(null)

        setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_MOVE) {
                setSelection(text?.length ?: 0)
            } else if (event.action == MotionEvent.ACTION_UP) {
                this.performClick()
            }

            true
        }

        setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                this.performClick()
            } else {
                Handler(Looper.getMainLooper()).postDelayed(
                    {
                        setSelection(text?.length ?: 0)
                    },
                    HANDLER_DELAY
                )
            }
            false
        }

        addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    removeTextChangedListener(this)

                    isActivated = try {
                        val value = s.toString().unmaskMoney()

                        val formatedNumber = value.toMoneyNumber()
                        if (s.toString() != formatedNumber) {
                            setText(formatedNumber)
                        }

                        formatedNumber != DEFAULT_VALUE
                    } catch (e: NumberFormatException) {
                        setText(DEFAULT_VALUE)
                        false
                    }

                    addTextChangedListener(this)
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
            }
        )
    }

    override fun setSelection(start: Int, stop: Int) {
        super.setSelection(start, stop)
    }

    override fun setSelection(index: Int) {
        super.setSelection(index)
    }

    override fun onDraw(canvas: Canvas) {
        setSelection(text?.length ?: 0)
        super.onDraw(canvas)
    }

    fun resetValue() = setText(DEFAULT_VALUE)

    companion object {
        private const val DEFAULT_VALUE = "0,00"
        private const val HANDLER_DELAY: Long = 50
    }
}