package br.com.digio.uber.common.component

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.R
import br.com.digio.uber.common.extensions.isLastIndex

class SpaceItemDecoration : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val viewPosition = parent.getChildAdapterPosition(view)
        val marginNormal = view.context.resources.getDimensionPixelSize(R.dimen.dimen_24dp)
        val marginXSmall = view.context.resources.getDimensionPixelSize(R.dimen.dimen_12dp)

        val isFirstIndex = viewPosition == 0
        if (isFirstIndex) {
            outRect.left = marginNormal
        }

        if (parent.isLastIndex(viewPosition)) {
            outRect.right = marginNormal
        } else {
            outRect.right = marginXSmall
        }
    }
}