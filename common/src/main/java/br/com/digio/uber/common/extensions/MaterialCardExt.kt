package br.com.digio.uber.common.extensions

import androidx.databinding.BindingAdapter
import com.google.android.material.card.MaterialCardView

@BindingAdapter("isChecked")
fun MaterialCardView.setViewChecked(isChecked: Boolean?) {
    this.isChecked = isChecked == true
}