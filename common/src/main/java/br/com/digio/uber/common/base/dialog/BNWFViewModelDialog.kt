package br.com.digio.uber.common.base.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.extensions.associateViewModel
import br.com.digio.uber.domain.usecase.login.LogoutUseCase
import org.koin.android.ext.android.inject

abstract class BNWFViewModelDialog<T : ViewDataBinding, VM : BaseViewModel> :
    BaseNavigationWithFlowDialog() {

    abstract val bindingVariable: Int?

    abstract val getLayoutId: Int?

    abstract val viewModel: VM?

    protected var binding: T? = null

    private val logoutUseCase: LogoutUseCase by inject()

    override fun initialize() {
        viewModel?.initializer { }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = associateViewModel(
            inflater,
            logoutUseCase,
            container,
            getLayoutId,
            viewModel,
            bindingVariable
        )
        return binding?.root ?: super.onCreateView(inflater, container, savedInstanceState)
    }
}