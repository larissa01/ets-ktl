package br.com.digio.uber.common.dialogs.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel

class InformativeViewModel : BaseViewModel() {

    val title: MutableLiveData<String> = MutableLiveData()
    val text: MutableLiveData<String> = MutableLiveData()
    val supportTextOne: MutableLiveData<String> = MutableLiveData()
    val supportTextTwo: MutableLiveData<String> = MutableLiveData()
    val icon: MutableLiveData<Int> = MutableLiveData()
    val buttonPositiveText: MutableLiveData<String> = MutableLiveData()
    val buttonMiddleText: MutableLiveData<String> = MutableLiveData()
    val showPositiveButton = MutableLiveData<Boolean>().apply { value = true }
    val showNegativeButton = MutableLiveData<Boolean>().apply { value = true }
    val buttonNegativeText: MutableLiveData<String> = MutableLiveData()
    val isThirdButtonVisible: MediatorLiveData<Boolean> =
        MediatorLiveData<Boolean>().apply {
            addSource(showNegativeButton) {
                value = it == true && !buttonMiddleText.value.isNullOrEmpty()
            }
            addSource(buttonMiddleText) {
                value = showNegativeButton.value == true && !it.isNullOrEmpty()
            }
        }
    val hideIcon = MutableLiveData<Boolean>().apply { value = false }
    val hideTitle = MutableLiveData<Boolean>().apply { value = false }
    val hideMessage = MutableLiveData<Boolean>().apply { value = false }
    val hideSupportTextOne = MutableLiveData<Boolean>().apply { value = true }
    val hideSupportTextTwo = MutableLiveData<Boolean>().apply { value = true }
}