package br.com.digio.uber.common.typealiases

import android.os.Bundle
import android.text.Editable
import android.view.KeyEvent
import android.view.View
import android.widget.TextView
import br.com.digio.uber.common.base.dialog.BaseNavigationWithFlowDialog
import br.com.digio.uber.common.base.fragment.BaseNavigationWithFlowFragment
import br.com.digio.uber.common.dialogs.InformativeDialog
import br.com.digio.uber.common.generics.MessageGenericObject
import br.com.digio.uber.model.cashback.CashbackOfferResponse
import br.com.digio.uber.model.cashback.CashbackPurchase
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.model.store.StoreProduct
import java.util.Calendar

typealias GenericEvent = () -> Unit
typealias OnDismiss = (dialog: InformativeDialog) -> Unit
typealias OnPositiveButton = (infoDialog: InformativeDialog) -> Unit
typealias OnCodeCompleteListener = (text: String) -> Unit
typealias OnCodeChangeListener = (text: String) -> Unit
typealias OnAdapterChange = (values: List<Any>) -> Unit
typealias OnClickItem = (v: View) -> Unit
typealias OnTextChange = (text: String?) -> Unit
typealias OnTextChanged = (value: CharSequence?, start: Int, before: Int, count: Int) -> Unit
typealias BeforeTextChanged = (s: CharSequence?, start: Int, count: Int, after: Int) -> Unit
typealias AfterTextChanged = (s: Editable?) -> Unit
typealias OnEditorActionListener = (view: TextView, actionId: Int, event: KeyEvent) -> Boolean
typealias OnTextChangedWithMask = (value: String) -> Unit
typealias OnCancelDialog = () -> Unit
typealias OnOkDialog = (dateSelected: Calendar?) -> Unit
typealias OnHideValueComponentFlagChange = (flag: Boolean) -> Unit
typealias GenericNavigationWIthFlowGetFragmentByName = (bundle: Bundle?) -> BaseNavigationWithFlowFragment?
typealias GenericNavigationWIthFlowGetFragmentByNameDialog = (bundle: Bundle?) -> BaseNavigationWithFlowDialog?
typealias OnErrorBaseViewModel = suspend (
    statusCode: Int?,
    error: MessageGenericObject,
    throwable: Throwable?
) -> MessageGenericObject
typealias OnSuccessBaseViewModel<T> = suspend (value: T) -> Unit
typealias OnCompletionBaseViewModel = suspend () -> Unit
typealias OnDismissDialog = InformativeDialog.(closeWithButton: Boolean) -> Unit
typealias OnClickProductItem = (item: StoreProduct) -> Unit
typealias OnClickCashbackStoreItem = (item: CashbackStore) -> Unit
typealias OnClickCashbackOfferItem = (item: CashbackOfferResponse) -> Unit
typealias OnClickGoToStoreButtom = (item: String?) -> Unit
typealias OnClickCheckBoxToPurchase = (item: CashbackPurchase, checked: Boolean) -> Unit
typealias OnClickCashbackStoreHomeItem = (item: CashbackStore, last: Boolean) -> Unit