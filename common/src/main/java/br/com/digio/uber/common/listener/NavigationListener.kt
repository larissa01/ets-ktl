package br.com.digio.uber.common.listener

interface NavigationListener {
    fun onBackButtonClick()
    fun requestPermissions(permision: List<String>, codePermisionRequest: Int)
}