package br.com.digio.uber.common.extensions

import androidx.annotation.StringRes
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("error")
fun TextInputLayout.setErrorBind(charSequence: CharSequence?) {
    isErrorEnabled = charSequence != null
    error = charSequence
}

@BindingAdapter("error")
fun TextInputLayout.setErrorBind(@StringRes value: Int?) {
    val valueResolveZero = if (value == 0) null else value
    isErrorEnabled = valueResolveZero != null
    valueResolveZero?.let { error = context.getString(it) }
}

@BindingAdapter("helperMessage")
fun TextInputLayout.setHelpperMessage(charSequence: CharSequence?) {
    isHelperTextEnabled = charSequence != null
    helperText = charSequence
}

@BindingAdapter("hint")
fun TextInputLayout.setHint(@StringRes value: Int) {
    if (value != 0) {
        hint = context.getString(value)
    }
}