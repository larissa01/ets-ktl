package br.com.digio.uber.common.generics

import br.com.digio.uber.common.base.activity.CoreBaseActivity
import br.com.digio.uber.common.dialogs.InformativeDialog.Build.Companion.buildInformativeAndShow
import br.com.digio.uber.common.typealiases.OnDismissDialog
import br.com.digio.uber.util.safeHeritage

interface MessageGenericObjectConfig {

    fun getActivityForError(): CoreBaseActivity?

    fun showMessage(
        message: MessageGenericObject?,
        closeable: Boolean = false,
        onCloseDialog: OnDismissDialog? = null
    ) {
        when (message?.type) {
            TypeMessage.SUCCESS -> showSuccess(message, closeable, onCloseDialog)
            TypeMessage.ERROR -> showError(message, closeable, onCloseDialog)
            TypeMessage.CHOOSE -> showChoose(message, closeable, onCloseDialog)
            TypeMessage.STATEMENT -> showStatement(message, closeable, onCloseDialog)
            TypeMessage.THREE_OPTIONS -> showThreeOptions(message, closeable, onCloseDialog)
            TypeMessage.EMPTY_ERROR -> Unit
            else -> message?.let {
                showMessageDefault(
                    it,
                    closeable,
                    onCloseDialog ?: message.onCloseDialog
                )
            }
        }
    }

    fun showStatement(message: MessageGenericObject, closeable: Boolean, onCloseDialog: OnDismissDialog?) {
        message
            .safeHeritage<StatementMessageObject>()?.let {
                showStatementDialog(it, closeable, onCloseDialog ?: message.onCloseDialog)
            }
    }

    fun showError(
        message: MessageGenericObject,
        closeable: Boolean,
        onCloseDialog: OnDismissDialog?
    ) {
        message.safeHeritage<ErrorObject>()
            ?.let { showError(it, closeable, onCloseDialog, message) }
    }

    fun showSuccess(
        message: MessageGenericObject,
        closeable: Boolean,
        onCloseDialog: OnDismissDialog?
    ) {
        message.safeHeritage<MessageSuccessObject>()
            ?.let { showSuccess(it, closeable, onCloseDialog, message) }
    }

    fun showSuccess(
        it: MessageSuccessObject,
        closeable: Boolean,
        onCloseDialog: OnDismissDialog?,
        message: MessageGenericObject
    ) {
        showMessageSuccess(it, closeable, onCloseDialog ?: message.onCloseDialog)
    }

    fun showError(
        it: ErrorObject,
        closeable: Boolean,
        onCloseDialog: OnDismissDialog?,
        message: MessageGenericObject
    ) {
        showError(it, closeable, onCloseDialog ?: message.onCloseDialog)
    }

    fun showThreeOptions(
        message: MessageGenericObject,
        closeable: Boolean,
        onCloseDialog: OnDismissDialog?
    ) {
        message
            .safeHeritage<ThreeOptionsMessageObject>()?.let {
                showThreeOptions(it, closeable, onCloseDialog ?: message.onCloseDialog)
            }
    }

    fun showChoose(
        message: MessageGenericObject,
        closeable: Boolean,
        onCloseDialog: OnDismissDialog?
    ) {
        message
            .safeHeritage<ChooseMessageObject>()?.let {
                showChoose(it, closeable, onCloseDialog ?: message.onCloseDialog)
            }
    }

    fun showThreeOptions(
        three: ThreeOptionsMessageObject,
        closeable: Boolean,
        onCloseDialog: OnDismissDialog?
    ) {
        getActivityForError()?.apply {
            buildInformativeAndShow {
                title = three.titleString ?: getString(three.title)
                text = three.messageString ?: getString(three.message)
                icon = three.icon
                hideTitle = three.hideTitle
                hideIcon = three.hideIcon
                hideMessage = three.hideMessage
                multipleChoose = true
                buttonPositiveText =
                    three.positiveOptionTextString ?: getString(three.positiveOptionTextInt)
                buttonMiddleText =
                    three.middleOptionTextString ?: getString(three.middleOptionTextInt)
                buttonNegativeText =
                    three.negativeOptionTextString ?: getString(three.negativeOptionTextInt)
                statusBarColor = three.statusBarColor
                onDismiss = {
                    if (closeable || three.close) {
                        finish()
                    }
                    onCloseDialog?.invoke(this, it)
                }
                onPositiveButtom = three.onPositiveButtonClick
                onMiddleButton = three.onMiddleButtonClick
                onNegativeButton = three.onNegativeButtonClick
            }
        }
    }

    fun showStatementDialog(choose: StatementMessageObject, closeable: Boolean, onCloseDialog: OnDismissDialog?) {
        getActivityForError()?.apply {
            buildInformativeAndShow {
                title = choose.titleString ?: getString(choose.title)
                text = choose.messageString ?: getString(choose.message)
                informativeDialogSupportTextOne = choose.amountValue
                informativeDialogSupportTextTwo = choose.dateValue
                icon = choose.icon
                hideTitle = choose.hideTitle
                hideIcon = choose.hideIcon
                hideMessage = choose.hideMessage
                hideSupportTextOne = choose.hideSupportTextOne
                hideSupportTextTwo = choose.hideSupportTextTwo
                showPositiveButton = choose.showPositiveButton
                multipleChoose = choose.showNegativeButton
                buttonPositiveText = choose.positiveOptionTextString ?: getString(choose.positiveOptionTextInt)

                buttonNegativeText = choose.negativeOptionTextString ?: getString(choose.negativeOptionTextInt)
                onDismiss = {
                    if (closeable || choose.close) {
                        finish()
                    }
                    onCloseDialog?.invoke(this, it)
                }
                onPositiveButtom = choose.onPositiveButtonClick
                onNegativeButton = choose.onNegativeButtonClick
            }
        }
    }

    fun showChoose(
        choose: ChooseMessageObject,
        closeable: Boolean,
        onCloseDialog: OnDismissDialog?
    ) {
        getActivityForError()?.apply {
            buildInformativeAndShow {
                title = choose.titleString ?: getString(choose.title)
                text = choose.messageString ?: getString(choose.message)
                icon = choose.icon
                hideTitle = choose.hideTitle
                hideIcon = choose.hideIcon
                hideMessage = choose.hideMessage
                multipleChoose = true
                buttonPositiveText =
                    choose.positiveOptionTextString ?: getString(choose.positiveOptionTextInt)
                buttonNegativeText =
                    choose.negativeOptionTextString ?: getString(choose.negativeOptionTextInt)
                statusBarColor = choose.statusBarColor
                onDismiss = {
                    if (closeable || choose.close) {
                        finish()
                    }
                    onCloseDialog?.invoke(this, it)
                }
                onPositiveButtom = choose.onPositiveButtonClick
                onNegativeButton = choose.onNegativeButtonClick
            }
        }
    }

    fun showError(error: ErrorObject, closeable: Boolean, onCloseDialog: OnDismissDialog?) {
        getActivityForError()?.apply {
            buildInformativeAndShow {
                title = error.titleString ?: getString(error.title)
                text = error.messageString ?: getString(error.message)
                icon = error.icon
                hideTitle = error.hideTitle
                hideIcon = error.hideIcon
                hideMessage = error.hideMessage
                multipleChoose = false
                buttonPositiveText = error.buttonTextString ?: getString(error.buttonText)
                onPositiveButtom = error.onPositiveButtonClick
                statusBarColor = error.statusBarColor
                onDismiss = {
                    if (closeable || error.close) {
                        finish()
                    }
                    onCloseDialog?.invoke(this, it)
                }
            }
        }
    }

    fun showMessageSuccess(
        message: MessageSuccessObject,
        closeable: Boolean,
        onCloseDialog: OnDismissDialog?
    ) {
        getActivityForError()?.apply {
            buildInformativeAndShow {
                title = message.titleString ?: getString(message.title)
                text = message.messageString ?: getString(message.message)
                icon = message.icon
                hideTitle = message.hideTitle
                hideIcon = message.hideIcon
                hideMessage = message.hideMessage
                multipleChoose = false
                buttonPositiveText = message.buttonTextString ?: getString(message.buttonText)
                onPositiveButtom = message.onPositiveButtonClick
                statusBarColor = message.statusBarColor
                onDismiss = {
                    if (closeable || message.close) {
                        finish()
                    }
                    onCloseDialog?.invoke(this, it)
                }
            }
        }
    }

    fun showMessageDefault(
        message: MessageGenericObject,
        closeable: Boolean,
        onCloseDialog: OnDismissDialog?
    ) {
        getActivityForError()?.apply {
            buildInformativeAndShow {
                title = message.titleString ?: getString(message.title)
                text = message.messageString ?: getString(message.message)
                icon = message.icon
                hideTitle = message.hideTitle
                hideIcon = message.hideIcon
                hideMessage = message.hideMessage
                multipleChoose = false
                buttonPositiveText = message.buttonTextString ?: getString(message.buttonText)
                onPositiveButtom = message.onPositiveButtonClick
                statusBarColor = message.statusBarColor
                onDismiss = {
                    if (closeable || message.close) {
                        finish()
                    }
                    onCloseDialog?.invoke(this, it)
                }
            }
        }
    }
}