package br.com.digio.uber.common.kenum

enum class RefundType {
    MAY_REFUND, MAY_NOT_REFUND, REFUND_OVERDUE;

    companion object {
        fun parse(name: String?): RefundType {
            for (refundType in RefundType.values()) {
                if (refundType.name == name) return refundType
            }
            return RefundType.MAY_NOT_REFUND
        }
    }
}