package br.com.digio.uber.common.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import br.com.digio.uber.model.cashback.Tiers
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder

inline fun ImageView.loadImage(
    context: Context,
    url: String,
    onConfigure: RequestBuilder<Drawable>.() -> RequestBuilder<Drawable> = { this }
) =
    Glide.with(context)
        .load(url)
        .onConfigure()
        .into(this)

fun ImageView.loadImage(context: Context, url: String?, placeholder: Drawable?, errorLoad: Drawable?) =
    Glide.with(context)
        .load(url)
        .error(errorLoad)
        .placeholder(placeholder)
        .into(this)

@BindingAdapter("urlload")
fun ImageView.loadImageBinding(imageUrl: String?) =
    imageUrl?.let { loadImage(context, it) }

@BindingAdapter(value = ["urlload", "placeholder", "errorLoad"], requireAll = true)
fun ImageView.loadImageBinding(imageUrl: String?, placeHolder: Drawable, errorLoad: Drawable) =
    loadImage(context, imageUrl, placeHolder, errorLoad)

@BindingAdapter("imageResource")
fun ImageView.imageResource(@DrawableRes image: Int?) {
    image?.let { imageLet ->
        setImageResource(imageLet)
    }
}

@BindingAdapter("iconTier")
fun ImageView.iconTier(tier: Tiers?) {
    when (tier) {
        Tiers.TIER_1 -> setImageResource(br.com.digio.uber.common.R.drawable.ic_cashback_blue_tier)
        Tiers.TIER_2 -> setImageResource(br.com.digio.uber.common.R.drawable.ic_cashback_gold_tier)
        Tiers.TIER_3 -> setImageResource(br.com.digio.uber.common.R.drawable.ic_cashback_platinum_tier)
        Tiers.TIER_4 -> setImageResource(br.com.digio.uber.common.R.drawable.ic_cashback_diamond_tier)
        Tiers.COURIER -> setImageResource(br.com.digio.uber.common.R.drawable.ic_cashback_green_tier)
        else -> setImageResource(br.com.digio.uber.common.R.drawable.ic_cashback_blue_tier)
    }
}

@BindingAdapter("enable")
fun ImageView.setEnable(enable: Boolean) {
    isEnabled = enable
}

@BindingAdapter("isVisible")
fun ImageView.setIsVisible(isVisible: Boolean) {
    if (isVisible) this.visibility = View.VISIBLE
    else this.visibility = View.INVISIBLE
}