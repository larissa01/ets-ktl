package br.com.digio.uber.common.base.dialog

import android.os.Bundle
import br.com.digio.uber.common.androidUtils.AnimationTransactionObjectTypes
import br.com.digio.uber.common.listener.BaseNavigationWithFlowListeners
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.listener.NavigationListenerWithFlow
import br.com.digio.uber.common.listener.ResultsCode

abstract class BaseNavigationWithFlowDialog :
    BaseDialog(),
    BaseNavigationWithFlowListeners {

    override var resultCode: ResultsCode = ResultsCode.RESULT_OK

    var navigationListener: NavigationListenerWithFlow? = null

    override fun callGoTo(
        fragmentName: String,
        bundle: Bundle?,
        forResult: Boolean,
        animationTransactionObject: AnimationTransactionObjectTypes?
    ) {
        navigationListener?.goTo(
            GoTo(
                fragmentName,
                bundle ?: arguments,
                this,
                forResult,
                animationTransactionObject?.animation
            )
        )
    }

    override fun callGoBack(bundle: Bundle?) {
        navigationListener?.back(bundle, this)
    }

    override fun clearAllBackStack() {
        navigationListener?.clearAllBackStack()
    }

    override fun getTagRegistration() =
        tag()

    open fun onBackPressedFromNavigation(): Unit = Unit

    open fun onFragmentVisible(): Unit = Unit
}