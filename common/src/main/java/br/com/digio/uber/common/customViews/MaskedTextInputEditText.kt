package br.com.digio.uber.common.customViews

import android.content.Context
import android.content.res.Resources
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.EditText
import br.com.digio.uber.common.R
import br.com.digio.uber.util.Const.MaskPattern.CPF_LENGTH
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.unmask
import com.google.android.material.textfield.TextInputEditText

class MaskedTextInputEditText(context: Context, attrs: AttributeSet) :
    TextInputEditText(context, attrs) {

    private val mask: String
    private var clickIcon: (() -> Unit)? = null

    init {
        val array = context.obtainStyledAttributes(attrs, R.styleable.MaskedTextInputEditText)

        this.mask = array.getString(R.styleable.MaskedTextInputEditText_mask).toString()

        array.recycle()

        addTextChangedListener(MaskTextWatcher(context.resources, this, this.mask))

        setOnTouchListener { v, event ->
            var ret = super.onTouchEvent(event)
            if (event?.action == MotionEvent.ACTION_UP) {
                ret = if (this.compoundDrawables[DRAWABLE_RIGHT] != null &&
                    event.rawX >= this.right - this.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                ) {
                    clickIcon?.let { it.invoke(); true } ?: super.onTouchEvent(event)
                } else {
                    super.onTouchEvent(event)
                }
            }
            ret
        }
    }

    private class MaskTextWatcher(
        private val resources: Resources,
        private val editText: EditText,
        private val mask: String?
    ) : TextWatcher {

        private var isUpdating: Boolean = false

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            if (mask == null || mask.isEmpty()) {
                return
            }

            if (isUpdating) {
                isUpdating = false

                return
            }

            val current = s.toString()

            val masked = when {
                mask == resources.getString(R.string.all_mask_cpf_cnpj) -> {
                    if (current.unmask().length <= CPF_LENGTH) {
                        current.applyMask(resources.getString(R.string.all_mask_cpf))
                    } else {
                        current.applyMask(resources.getString(R.string.all_mask_cnpj))
                    }
                }
                else -> current.applyMask(mask)
            }

            if (masked != current) {
                isUpdating = true

                editText.setText(masked)

                editText.setSelection(editText.length())
            }
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) = Unit

        override fun afterTextChanged(s: Editable) = Unit
    }

    fun setOnClickDrawableRight(func: () -> Unit) {
        clickIcon = func
    }

    companion object {
        const val DRAWABLE_RIGHT = 2
    }
}