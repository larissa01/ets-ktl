package br.com.digio.uber.common.base.adapter

import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.listener.AdapterItemsContract

abstract class BaseRecyclerViewAdapter<T : AdapterViewModel<T>, VTF : ViewTypesDataBindingFactory<T>> :
    RecyclerView.Adapter<AbstractViewHolder<T>>(),
    AdapterItemsContract {

    protected val typeFactory: VTF by lazy {
        getViewTypeFactory()
    }

    override fun getItemViewType(position: Int): Int =
        getItemType(position).type(typeFactory)

    abstract fun getViewTypeFactory(): VTF

    abstract fun getItemType(position: Int): T
}