package br.com.digio.uber.common.extensions

import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.databinding.BindingAdapter

@BindingAdapter("goToAnimationMotion")
fun MotionLayout.goToAnimationMotion(anim: Boolean) {
    if (anim) {
        transitionToEnd()
    } else {
        transitionToStart()
    }
}