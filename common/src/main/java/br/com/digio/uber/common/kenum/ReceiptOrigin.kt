package br.com.digio.uber.common.kenum

enum class ReceiptOrigin {
    TRANSFER,
    STATEMENT,
    PAYMENT,
}