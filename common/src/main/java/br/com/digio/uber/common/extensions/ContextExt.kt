package br.com.digio.uber.common.extensions

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import timber.log.Timber

/**
 * @author Marlon D. Rocha
 * @since 19/10/20
 */

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.openDeviceSettings() {
    val appCompatActivity = (this as? AppCompatActivity)

    if (appCompatActivity == null) {
        Timber.e("Not a valid activity context.")
        return
    }

    val uri =
        Uri.fromParts("package", appCompatActivity.packageName, null)
    Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        data = uri
    }.also {
        startActivity(it)
    }
}

fun Context.goToAppSettings(context: Context) {
    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
    val uri: Uri = Uri.fromParts(PACKAGE, context.packageName, null)
    intent.data = uri
    context.startActivity(intent)
}

private const val PACKAGE = "package"