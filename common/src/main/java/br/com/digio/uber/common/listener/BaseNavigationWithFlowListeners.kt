package br.com.digio.uber.common.listener

import android.os.Bundle
import br.com.digio.uber.common.androidUtils.AnimationTransactionObjectTypes

interface BaseNavigationWithFlowListeners {
    fun getTagRegistration(): String

    fun callGoTo(
        fragmentName: String,
        bundle: Bundle? = null,
        forResult: Boolean = false,
        animationTransactionObject: AnimationTransactionObjectTypes? = AnimationTransactionObjectTypes.LEFT_TO_RIGHT
    )

    fun callGoBack(bundle: Bundle? = null)
    fun clearAllBackStack()
    fun fragmentResult(bundle: Bundle?, from: String?, resultCode: ResultsCode?) {}
    var resultCode: ResultsCode
}

enum class ResultsCode {
    RESULT_OK,
    RESULT_ERROR,
    RESULT_CANCELED
}