package br.com.digio.uber.common.helper

import android.os.Bundle
import br.com.digio.uber.common.base.dialog.BNWFViewModelDialog
import br.com.digio.uber.common.base.dialog.BaseDialog
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow

open class DialogNewInstanceWithNavigation<T : BNWFViewModelDialog<*, *>>(val creator: () -> T) {
    fun newInstance(navigation: GenericNavigationWithFlow, bundle: Bundle?): T =
        creator().apply {
            this.navigationListener = navigation
            arguments = bundle
        }
}

open class DialogNewInstance<T : BaseDialog>(val creator: () -> T) {
    fun newInstance(bundle: Bundle?): T =
        creator().apply {
            arguments = bundle
        }
}