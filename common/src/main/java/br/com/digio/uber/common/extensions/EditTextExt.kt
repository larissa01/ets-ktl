package br.com.digio.uber.common.extensions

import android.content.Context
import android.graphics.Paint
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.helper.EmojiInputFilter
import br.com.digio.uber.common.typealiases.AfterTextChanged
import br.com.digio.uber.common.typealiases.BeforeTextChanged
import br.com.digio.uber.common.typealiases.OnTextChanged
import br.com.digio.uber.common.typealiases.OnTextChangedWithMask
import br.com.digio.uber.common.util.MaskUtils
import br.com.digio.uber.util.TextWatcherObject.addSingleInstanceOfTextWatcher
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textview.MaterialTextView

fun EditText.maskSimple(
    onTextChanged: OnTextChanged? = null,
    beforeTextChanged: BeforeTextChanged? = null,
    afterTextChanged: AfterTextChanged? = null
) {
    addSingleInstanceOfTextWatcher(
        object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                afterTextChanged?.invoke(s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                beforeTextChanged?.invoke(s, start, count, after)
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                onTextChanged?.invoke(s, start, before, count)
            }
        }
    )
}

@BindingAdapter("onTextChangedObserver")
fun EditText.onTextChangedObserver(observable: MutableLiveData<String>) {
    maskSimple(
        onTextChanged = { value, _, _, _ ->
            observable.value = value.toString()
        }
    )
}

@BindingAdapter("onTextChangedListen")
fun EditText.onTextChangedListen(onTextChanged: OnTextChanged) {
    maskSimple(
        onTextChanged = onTextChanged
    )
}

@BindingAdapter("afterTextChanged")
fun EditText.afterTextChanged(observable: MutableLiveData<Editable>) {
    maskSimple(
        afterTextChanged = { editable ->
            observable.value = editable
        }
    )
}

@BindingAdapter(
    value = [
        "mask",
        "onTextChanged",
        "beforeTextChanged",
        "afterTextChanged",
        "onTextChangedWithMask"
    ],
    requireAll = false
)
fun EditText.mask(
    mask: String? = null,
    onTextChanged: OnTextChanged? = null,
    beforeTextChanged: BeforeTextChanged? = null,
    afterTextChanged: AfterTextChanged? = null,
    onTextChangedWithMask: OnTextChangedWithMask? = null
) {
    addSingleInstanceOfTextWatcher(
        MaskUtils.insert(
            mask,
            this,
            onTextChanged,
            beforeTextChanged,
            afterTextChanged,
            onTextChangedWithMask
        )
    )
}

@BindingAdapter("enableEmoji")
fun TextInputEditText.enableEmoji(shouldEnable: Boolean?) {
    // emojis are enabled by default
    if (shouldEnable == false) {
        val mFilters = Array<InputFilter>(filters.size + 1) { EmojiInputFilter() }
        filters.forEach { mFilters[filters.indexOf(it)] = it }
        filters = mFilters
    }
}

@BindingAdapter("strikeThrough")
fun MaterialTextView.enableStrikeThrough(enabled: Boolean? = false) {
    paintFlags = if (enabled == true) {
        paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
    } else {
        paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
    }
}

fun EditText.showKeyboard() {
    requestFocus()
    val imm: InputMethodManager? =
        context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    imm?.showSoftInput(
        this,
        InputMethodManager.SHOW_IMPLICIT
    )
}

@BindingAdapter("onTextChanged")
fun EditText.onTextChanged(observable: MutableLiveData<String>) {
    maskSimple(
        onTextChanged = { value, start, before, count ->
            observable.value = value.toString()
        }
    )
}