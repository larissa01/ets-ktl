package br.com.digio.uber.common.balancetoolbar.balance.di

import br.com.digio.uber.common.balancetoolbar.balance.viewModel.BalanceViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val balanceModule = module {
    viewModel { BalanceViewModel(get(), get(), get()) }
}