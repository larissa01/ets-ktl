package br.com.digio.uber.common.component

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.annotation.FontRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import br.com.digio.uber.common.R
import br.com.digio.uber.common.extensions.hide
import br.com.digio.uber.common.extensions.invisible
import br.com.digio.uber.common.extensions.setFontFamily
import br.com.digio.uber.common.extensions.setMarginCustom
import br.com.digio.uber.common.extensions.show

class ListTile constructor(
    context: Context,
    attr: AttributeSet? = null
) : ConstraintLayout(context, attr) {

    // Default Resources

    @ColorRes
    private val defaultTitleTextColor: Int = R.color.black

    @ColorRes
    private val defaultSubtitleTextColor: Int = R.color.grey_545454

    @DimenRes
    private val defaultTitleTextSize: Int = R.dimen.dimen_16sp

    @DimenRes
    private val defaultSubTitleTextSize: Int = R.dimen.dimen_12sp

    @DimenRes
    private val defaultIconMarginLeft: Int = R.dimen._24sdp

    @DimenRes
    private val defaultIconMarginRight: Int = R.dimen._16sdp

    @DimenRes
    private val defaultIconNextMarginLeft: Int = R.dimen._44sdp

    @DimenRes
    private val defaultIconNextMarginRight: Int = R.dimen._24sdp

    @DimenRes
    private val defaultSubtitleTextMarginTop: Int = R.dimen._5sdp

    @DrawableRes
    private val defaultIconNext: Int = R.drawable.ic_arrow_right_grey_thick

    @IdRes
    private val iconId = View.generateViewId()

    @IdRes
    private val titleId = View.generateViewId()

    @IdRes
    private val subtitleId = View.generateViewId()

    @IdRes
    private val iconNextId = View.generateViewId()

    @DrawableRes
    var iconNext: Int = defaultIconNext

    @ColorRes
    private var titleTextColor: Int = defaultTitleTextColor

    @ColorRes
    private var subtitleTextColor: Int = defaultSubtitleTextColor

    @DimenRes
    private var titleTextSize: Int = defaultTitleTextSize

    @DimenRes
    private var subtitleTextSize: Int = defaultSubTitleTextSize

    @DimenRes
    private var subTitleMarginTop: Int = defaultSubtitleTextMarginTop

    @DimenRes
    private var iconMarginLeft: Int = defaultIconMarginLeft

    @DimenRes
    private var iconMarginRight: Int = defaultIconMarginRight

    @DimenRes
    private var iconNextMarginLeft: Int = defaultIconNextMarginLeft

    @DimenRes
    private var iconNextMarginRight: Int = defaultIconNextMarginRight

    @FontRes
    private var defaultTitleFont: Int = R.font.uber_move_text_medium

    @FontRes
    private var defaultSubTitleFont: Int = R.font.uber_move_text_regular

    private var isIconNextVisible: Boolean = true

    @DrawableRes
    var icon: Int? = null
        set(value) {
            if (field != value) {
                field = value
                setupIcon()
            }
        }

    var title: String? = null
        set(value) {
            if (field != value) {
                field = value
                setupTitle()
            }
        }

    var subtitle: String? = null
        set(value) {
            if (field != value) {
                field = value
                setupSubtitle()
            }
        }

    init {
        val typeArray: TypedArray = context.obtainStyledAttributes(attr, R.styleable.ListTile)
        try {
            setupTypeArray(typeArray)
            setupView()
        } finally {
            typeArray.recycle()
        }
    }

    private fun setupTypeArray(typeArray: TypedArray) {
        title = typeArray.getString(R.styleable.ListTile_title)

        subtitle = typeArray.getString(R.styleable.ListTile_subtitle)

        typeArray.getResourceId(R.styleable.ListTile_icon) {
            icon = it
        }
        typeArray.getResourceId(R.styleable.ListTile_iconNext) { iconNextRes ->
            iconNext = iconNextRes
        }
        typeArray.getResourceId(R.styleable.ListTile_titleTextColor) { titleTextColorRes ->
            titleTextColor = titleTextColorRes
        }
        typeArray.getResourceId(R.styleable.ListTile_subtitleTextColor) { subitleTextColorRes ->
            subtitleTextColor = subitleTextColorRes
        }
        typeArray.getResourceId(R.styleable.ListTile_titleTextSize) { titleTextSizeRes ->
            titleTextSize = titleTextSizeRes
        }
        typeArray.getResourceId(R.styleable.ListTile_subtitleTextSize) { suttitleTextSizeRes ->
            subtitleTextSize = suttitleTextSizeRes
        }
        typeArray.getResourceId(R.styleable.ListTile_subtitleMarginTop) { subTitleMarginTopRes ->
            subTitleMarginTop = subTitleMarginTopRes
        }
        typeArray.getResourceId(R.styleable.ListTile_iconMarginLeft) { iconMarginLeftRes ->
            iconMarginLeft = iconMarginLeftRes
        }
        typeArray.getResourceId(R.styleable.ListTile_iconMarginRight) { iconMarginRightRes ->
            iconMarginRight = iconMarginRightRes
        }
        typeArray.getResourceId(R.styleable.ListTile_iconNextMarginLeft) { iconNextMarginLeftRes ->
            iconNextMarginLeft = iconNextMarginLeftRes
        }
        typeArray.getResourceId(R.styleable.ListTile_iconNextMarginRight) { iconNextMarginRightRes ->
            iconNextMarginRight = iconNextMarginRightRes
        }
        isIconNextVisible = typeArray.getBoolean(R.styleable.ListTile_isIconNextVisible, true)
    }

    private fun TypedArray.getResourceId(styleable: Int, onChangeResource: (resource: Int) -> Unit) {
        getResourceId(styleable, -1).also { res ->
            if (res != -1) {
                onChangeResource(res)
            }
        }
    }

    private fun setupView() {
        removeAllViews()
        setupIcon()
        setupTitle()
        setupSubtitle()
        setupNextIcon()
    }

    private fun setupIcon() {
        addView(
            AppCompatImageView(context).apply {
                id = iconId
                this@ListTile.icon?.let { iconLet ->
                    setImageResource(iconLet)
                    show()
                } ?: invisible()
            },
            LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
                topToTop = LayoutParams.PARENT_ID
                bottomToBottom = LayoutParams.PARENT_ID
                leftToLeft = LayoutParams.PARENT_ID
                rightToLeft = titleId
                horizontalBias = 0f
                horizontalChainStyle = LayoutParams.CHAIN_PACKED
                setMarginCustom(
                    left = resources.getDimensionPixelSize(iconMarginLeft),
                    right = resources.getDimensionPixelSize(iconMarginRight)
                )
            }
        )
    }

    private fun setupTitle() {
        addView(
            AppCompatTextView(context).apply {
                id = titleId
                text = title
                setTextColor(ContextCompat.getColor(context, titleTextColor))
                setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(titleTextSize))
                setFontFamily(defaultTitleFont)
                includeFontPadding = false
                if (title != null) {
                    show()
                } else {
                    invisible()
                }
            },
            LayoutParams(0, LayoutParams.WRAP_CONTENT).apply {
                topToTop = LayoutParams.PARENT_ID
                leftToRight = iconId
                rightToLeft = iconNextId
                if (subtitle == null) {
                    LayoutParams.PARENT_ID
                }
                verticalChainStyle = LayoutParams.CHAIN_PACKED
            }
        )
    }

    private fun setupSubtitle() {
        addView(
            AppCompatTextView(context).apply {
                id = subtitleId
                text = subtitle
                setTextColor(ContextCompat.getColor(context, subtitleTextColor))
                setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(subtitleTextSize))
                setFontFamily(defaultSubTitleFont)
                includeFontPadding = false
                isSingleLine = false
                if (subtitle != null) {
                    show()
                } else {
                    hide()
                }
            },
            LayoutParams(0, LayoutParams.WRAP_CONTENT).apply {
                topToBottom = titleId
                leftToLeft = titleId
                rightToLeft = iconNextId
                horizontalBias = 0f
                setMarginCustom(
                    top = resources.getDimensionPixelSize(defaultSubtitleTextMarginTop),
                    right = resources.getDimensionPixelSize(defaultSubtitleTextMarginTop)

                )
            }
        )
    }

    private fun setupNextIcon() {
        addView(
            AppCompatImageView(context).apply {
                id = iconNextId
                setImageResource(iconNext)
                if (isIconNextVisible) {
                    show()
                } else {
                    invisible()
                }
            },
            LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
                leftToRight = if (subtitle != null) {
                    subtitleId
                } else {
                    titleId
                }
                bottomToBottom = LayoutParams.PARENT_ID
                topToTop = LayoutParams.PARENT_ID
                rightToRight = LayoutParams.PARENT_ID
                setMarginCustom(
                    left = resources.getDimensionPixelSize(iconNextMarginLeft),
                    right = resources.getDimensionPixelSize(iconNextMarginRight)
                )
            }
        )
    }

    companion object {
        @JvmStatic
        @BindingAdapter("icon")
        fun ListTile.setIcon(@DrawableRes icon: Int) {
            this.icon = icon
        }

        @JvmStatic
        @BindingAdapter("title")
        fun ListTile.setTitle(title: String?) {
            this.title = title
        }

        @JvmStatic
        @BindingAdapter("subtitle")
        fun ListTile.setSubTitle(subtitle: String?) {
            this.subtitle = subtitle
        }

        @JvmStatic
        @BindingAdapter("title")
        fun ListTile.setTitle(@StringRes title: Int) {
            this.title = context.getString(title)
        }

        @JvmStatic
        @BindingAdapter("subtitle")
        fun ListTile.setSubTitle(@StringRes subtitle: Int) {
            this.subtitle = context.getString(subtitle)
        }
    }
}