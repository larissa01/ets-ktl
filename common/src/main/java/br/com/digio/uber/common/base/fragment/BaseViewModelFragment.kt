package br.com.digio.uber.common.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.extensions.associateViewModel
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.domain.usecase.login.LogoutUseCase
import org.koin.android.ext.android.inject

abstract class BaseViewModelFragment<T : ViewDataBinding, VM : BaseViewModel> : BaseFragment() {

    abstract val bindingVariable: Int?

    abstract val getLayoutId: Int?

    abstract val viewModel: VM?

    var binding: T? = null

    private val logoutUseCase: LogoutUseCase by inject()

    override fun initialize() {
        activity?.onBackPressedDispatcher?.addCallback(this) {
            onBackPressed()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = associateViewModel(
            inflater,
            logoutUseCase,
            container,
            getLayoutId,
            viewModel,
            bindingVariable
        )
        return binding?.root ?: super.onCreateView(inflater, container, savedInstanceState)
    }

    open fun onBackPressed() {
        pop()
    }
}