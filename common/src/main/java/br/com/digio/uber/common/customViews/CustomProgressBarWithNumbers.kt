package br.com.digio.uber.common.customViews

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import br.com.digio.uber.common.R
import br.com.digio.uber.common.extensions.setFontFamily

class CustomProgressBarWithNumbers @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private var startValue: Int = DEFAULT_START_VALUE
    private var endValue: Int = DEFAULT_END_VALUE
    private var progress = DEFAULT_PROGRESS
    private var backgroundProgress = R.color.black
    private var defaultTextColor = R.color.grey_AFAFAF
    private var progressTextColor = R.color.white
    private var textSize = R.dimen._12ssp
    private var backgroundDivider = R.color.grey_EEEEEE
    private var backgroundDividerProgress = R.color.grey_1F1F1F

    init {
        getAttrs(attrs)
        initViews()
    }

    fun setValues(startValue: Int, endValue: Int, progress: Int) {
        this.startValue = startValue
        this.endValue = endValue
        this.progress = progress
        removeAllViews()
        initViews()
    }

    private fun initViews() {
        (startValue..endValue).forEach { number ->
            val numberView = TextView(context)
            if (number <= progress) {
                numberView.setBackgroundColor(ContextCompat.getColor(context, backgroundProgress))
                numberView.setTextColor(ContextCompat.getColor(context, progressTextColor))
            } else {
                numberView.setTextColor(ContextCompat.getColor(context, defaultTextColor))
            }
            numberView.text = number.toString()
            numberView.textAlignment = TEXT_ALIGNMENT_CENTER
            numberView.gravity = Gravity.CENTER
            numberView.setFontFamily(R.font.uber_move_regular)
            numberView.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(textSize))

            val layoutParams = LayoutParams(0, LayoutParams.MATCH_PARENT)
            layoutParams.weight = 1f
            layoutParams.gravity = Gravity.CENTER
            numberView.layoutParams = layoutParams

            addView(numberView)

            if (number < endValue) {
                val divider = View(context)
                val backgroundDivider = if (number < progress) {
                    backgroundDividerProgress
                } else {
                    backgroundDivider
                }
                divider.setBackgroundColor(ContextCompat.getColor(context, backgroundDivider))
                val dividerLayoutParams =
                    LayoutParams(resources.getDimensionPixelSize(R.dimen._1sdp), LayoutParams.MATCH_PARENT)
                divider.layoutParams = dividerLayoutParams

                addView(divider)
            }
        }
    }

    private fun getAttrs(attrs: AttributeSet?) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.CustomProgressBarWithNumbers)

        startValue = array.getInt(R.styleable.CustomProgressBarWithNumbers_startValue, startValue)
        endValue = array.getInt(R.styleable.CustomProgressBarWithNumbers_startValue, endValue)
        progress = array.getInt(R.styleable.CustomProgressBarWithNumbers_startValue, progress)
        backgroundProgress = array
            .getResourceId(R.styleable.CustomProgressBarWithNumbers_backgroundProgress, backgroundProgress)
        defaultTextColor = array
            .getResourceId(R.styleable.CustomProgressBarWithNumbers_defaultTextColor, defaultTextColor)
        progressTextColor = array
            .getResourceId(R.styleable.CustomProgressBarWithNumbers_progressTextColor, progressTextColor)
        textSize = array
            .getResourceId(R.styleable.CustomProgressBarWithNumbers_defaultTextSize, textSize)
        backgroundDivider = array
            .getResourceId(R.styleable.CustomProgressBarWithNumbers_backgroundDivider, backgroundDivider)
        backgroundDividerProgress = array
            .getResourceId(
                R.styleable.CustomProgressBarWithNumbers_backgroundDividerProgress,
                backgroundDividerProgress
            )

        array.recycle()
    }

    companion object {
        private const val DEFAULT_START_VALUE = 1
        private const val DEFAULT_END_VALUE = 10
        private const val DEFAULT_PROGRESS = 2
    }
}