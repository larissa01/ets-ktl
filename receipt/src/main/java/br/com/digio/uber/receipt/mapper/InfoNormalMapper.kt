package br.com.digio.uber.receipt.mapper

import br.com.digio.uber.model.receipt.Receipt
import br.com.digio.uber.receipt.model.InfoNormalPresentationModel
import br.com.digio.uber.util.mapper.AbstractMapper

interface InfoNormalMapper : AbstractMapper<Receipt.InfoNormal?, InfoNormalPresentationModel?>