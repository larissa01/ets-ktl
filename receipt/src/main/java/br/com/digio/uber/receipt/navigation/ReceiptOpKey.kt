package br.com.digio.uber.receipt.navigation

import br.com.digio.uber.common.kenum.StatusBarColor

enum class ReceiptOpKey(val theme: StatusBarColor) {
    RECEIPT_MAIN(StatusBarColor.WHITE)
}