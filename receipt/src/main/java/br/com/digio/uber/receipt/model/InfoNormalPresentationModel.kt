package br.com.digio.uber.receipt.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class InfoNormalPresentationModel constructor(
    var payeeName: String? = null,
    var finalPayerName: String? = null,
    var finalPayerDocument: String? = null
) : Parcelable