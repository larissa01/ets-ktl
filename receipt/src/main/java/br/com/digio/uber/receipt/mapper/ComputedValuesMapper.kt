package br.com.digio.uber.receipt.mapper

import br.com.digio.uber.model.receipt.Receipt
import br.com.digio.uber.receipt.model.ComputedValuesPresentationModel
import br.com.digio.uber.util.mapper.AbstractMapper

interface ComputedValuesMapper : AbstractMapper<Receipt.ComputedValues?, ComputedValuesPresentationModel?>