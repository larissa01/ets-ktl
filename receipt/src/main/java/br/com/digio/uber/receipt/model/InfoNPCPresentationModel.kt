package br.com.digio.uber.receipt.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class InfoNPCPresentationModel(
    var payerName: String? = null,
    var payerDocument: String? = null,
    var finalPayerName: String? = null,
    var finalPayerDocument: String? = null,
    var payeeName: String? = null,
    var payeeDocument: String? = null,
    var computedValues: ComputedValuesPresentationModel? = null
) : Parcelable