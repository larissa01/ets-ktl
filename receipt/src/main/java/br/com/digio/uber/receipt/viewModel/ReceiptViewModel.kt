package br.com.digio.uber.receipt.viewModel

import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.kenum.ReceiptOrigin
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.receipt.GetReceiptUseCase
import br.com.digio.uber.receipt.R
import br.com.digio.uber.receipt.mapper.ReceiptMapper
import br.com.digio.uber.receipt.model.ReceiptUiModel
import br.com.digio.uber.util.Const.Utils.MULTIPLY_PADDING_RATIO

class ReceiptViewModel constructor(
    val receiptUseCase: GetReceiptUseCase,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    val receiptUiModel = MutableLiveData<ReceiptUiModel>()
    var receiptOrigin: ReceiptOrigin = ReceiptOrigin.STATEMENT
    var paymentWasRescheduled = false

    val isPayment = MutableLiveData<Boolean>().apply { value = false }
    val isPaymentAndNPC = MutableLiveData<Boolean>().apply { value = false }
    val paymentDateLabel = MutableLiveData<String>()
    val newOperationText = MutableLiveData<String>()
    val hideBankInfo = MutableLiveData<Boolean>().apply { value = false }

    fun initializer(receiptId: String, onClickItem: OnClickItem) {
        super.initializer(onClickItem)

        receiptUseCase(receiptId).singleExec(
            onSuccessBaseViewModel = {
                receiptUiModel.value = ReceiptMapper(resourceManager, receiptOrigin).map(it)
                isPayment.value = receiptUiModel.value?.billData != null
                hideBankInfo.value =
                    if (isPayment.value == true) {
                        isPayment.value
                    } else {
                        receiptUiModel.value?.transferType.toString() == resourceManager.getString(R.string.pix_type)
                    }

                paymentDateLabel.value =
                    if (isPayment.value == true) {
                        resourceManager.getString(R.string.receipt_date_payment_text)
                    } else {
                        resourceManager.getString(
                            R.string.receipt_date_transfer_text
                        )
                    }
                isPaymentAndNPC.value = (isPayment.value ?: false) && (receiptUiModel.value?.isNPC() ?: false)

                newOperationText.value = if (isPayment.value == true) {
                    resourceManager.getString(R.string.receipt_new_payment)
                } else {
                    resourceManager.getString(R.string.receipt_new_transfer)
                }
            }
        )
    }

    fun prepareBitmap(bitmapImage: Bitmap, padding: Int): Bitmap {
        val printBitmap = Bitmap.createBitmap(
            bitmapImage.width + (padding * MULTIPLY_PADDING_RATIO),
            bitmapImage.height,
            Bitmap.Config.ARGB_8888
        )

        Canvas(printBitmap).apply {
            drawColor(resourceManager.getColor(R.color.white))
            drawBitmap(bitmapImage, padding.toFloat(), padding.toFloat(), null)
        }
        return printBitmap
    }
}