package br.com.digio.uber.receipt.mapper

import br.com.digio.uber.model.receipt.Receipt
import br.com.digio.uber.receipt.model.InfoNormalPresentationModel

class InfoNormalMapperImpl : InfoNormalMapper {
    override fun map(response: Receipt.InfoNormal?): InfoNormalPresentationModel? =
        response?.let { responseLet ->
            InfoNormalPresentationModel(
                payeeName = responseLet.payeeName,
                finalPayerDocument = responseLet.finalPayerDocument,
                finalPayerName = responseLet.finalPayerName
            )
        }
}