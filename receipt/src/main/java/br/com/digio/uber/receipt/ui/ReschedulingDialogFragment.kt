package br.com.digio.uber.receipt.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.digio.uber.common.base.dialog.BaseDialog
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.receipt.databinding.ReschedulingDialogBinding
import br.com.digio.uber.receipt.model.ReceiptUiModel
import br.com.digio.uber.util.DateFormatter
import java.util.Date

class ReschedulingDialogFragment : BaseDialog() {

    private lateinit var binding: ReschedulingDialogBinding
    private lateinit var receiptItem: ReceiptUiModel

    companion object {
        const val RESCHEDULING_DIALOG_TAG = "RESCHEDULING_DIALOG_TAG"

        fun newInstance(receiptItem: ReceiptUiModel) = ReschedulingDialogFragment().apply {
            this.receiptItem = receiptItem
        }
    }

    override fun tag(): String = RESCHEDULING_DIALOG_TAG

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() {
        setupListeners()
        setupUI()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = ReschedulingDialogBinding.inflate(inflater)
        return binding.root
    }

    private fun setupUI() {
        receiptItem.scheduleDate?.let {
            binding.apply {
                val scheduleDate = DateFormatter.dateBRFormatter.parse(it) ?: Date()
                weekDay = DateFormatter.simpleWeekDateFormatter.format(scheduleDate)
                monthDay = DateFormatter.simpleMonthDateFormatter.format(scheduleDate)
            }
        }
    }

    private fun setupListeners() {
        binding.btOk.setOnClickListener {
            dismiss()
        }
    }
}