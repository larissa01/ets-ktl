package br.com.digio.uber.receipt.mapper

import br.com.digio.uber.model.receipt.Receipt
import br.com.digio.uber.receipt.model.ComputedValuesPresentationModel

class ComputedValuesMapperImpl : ComputedValuesMapper {
    override fun map(param: Receipt.ComputedValues?): ComputedValuesPresentationModel? =
        param?.let { responseLet ->
            ComputedValuesPresentationModel(
                calculatedFineValue = responseLet.calculatedFineValue,
                discountValueCalculated = responseLet.discountValueCalculated,
                totalAmountToCharge = responseLet.totalAmountToCharge,
                paymentAmountUpdated = responseLet.paymentAmountUpdated,
                computedDate = responseLet.computedDate,
                calculatedInterestAmount = responseLet.calculatedInterestAmount
            )
        }
}