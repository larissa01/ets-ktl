package br.com.digio.uber.receipt.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.helper.ViewIds
import br.com.digio.uber.common.navigation.GenericNavigation
import br.com.digio.uber.receipt.R
import br.com.digio.uber.receipt.di.receiptsModule
import br.com.digio.uber.receipt.navigation.ReceiptNavigation
import com.google.android.material.appbar.CollapsingToolbarLayout
import org.koin.core.module.Module

class ReceiptActivity : BaseViewModelActivity<ViewDataBinding, BaseViewModel>() {

    private val receiptNavigation = ReceiptNavigation(this)
    override val bindingVariable: Int? = null
    override val viewModel: BaseViewModel? = null
    override fun getLayoutId(): Int? = LayoutIds.navigationLayoutWithCollapsingToolbarScrollActivityWhite
    override fun getModule(): List<Module>? = listOf(receiptsModule)
    override fun getNavigation(): GenericNavigation? = receiptNavigation
    override fun getToolbar(): Toolbar? = findViewById<Toolbar>(ViewIds.toolbarIdWhite).apply {
        this@ReceiptActivity.findViewById<CollapsingToolbarLayout>(ViewIds.collapsingToolbarIdWhite)
            .title = getString(R.string.receipt)
        this.navigationIcon = ContextCompat.getDrawable(
            context,
            br.com.digio.uber.common.R.drawable.ic_back_arrow_black
        )
    }

    override fun showDisplayShowTitle(): Boolean = true

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        receiptNavigation.init(intent.extras)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}