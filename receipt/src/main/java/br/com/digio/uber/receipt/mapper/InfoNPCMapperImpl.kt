package br.com.digio.uber.receipt.mapper

import br.com.digio.uber.model.receipt.Receipt
import br.com.digio.uber.receipt.model.InfoNPCPresentationModel

class InfoNPCMapperImpl(
    private val computedValuesMapper: ComputedValuesMapper
) : InfoNPCMapper {
    override fun map(param: Receipt.InfoNPC?): InfoNPCPresentationModel? =
        param?.let { responseLet ->
            InfoNPCPresentationModel(
                payerName = responseLet.payerName,
                payeeDocument = responseLet.payeeDocument,
                payerDocument = responseLet.payerDocument,
                computedValues = computedValuesMapper.map(responseLet.computedValues),
                finalPayerName = responseLet.finalPayerName,
                finalPayerDocument = responseLet.finalPayerDocument,
                payeeName = responseLet.payeeName
            )
        }
}