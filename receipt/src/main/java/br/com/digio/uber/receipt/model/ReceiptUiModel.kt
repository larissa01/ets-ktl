package br.com.digio.uber.receipt.model

import android.text.Spannable
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.kenum.ReceiptOrigin
import br.com.digio.uber.model.receipt.Receipt
import br.com.digio.uber.receipt.R
import br.com.digio.uber.util.formatAsAgencyNumber
import br.com.digio.uber.util.formatAsDocument
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.validateCNPJ

data class ReceiptUiModel(
    val transferDate: String,
    val scheduleDate: String?,
    val transferTime: String,
    val transferDebitant: Receipt.Debitant,
    val transferValue: String,
    val transferBeneficiary: Receipt.Beneficiary?,
    val transferType: Spannable?,
    val comment: Spannable,
    val schedule: Spannable,
    val isSchedule: Boolean,
    val isScheduleText: String,
    val transferDescription: Spannable,
    val authenticationCode: String,
    val receiptOrigin: ReceiptOrigin,
    val resourceManager: ResourceManager,
    val amount: String,
    val barcode: String,
    val billData: Receipt.BillData?,
    val transactionDate: String?,
    val payeeName: String,
    val payeeDocument: String,
    val currencyValue: String,
    val payerDocument: String,
    val finalPayerDocument: String,
    val discountValueCalculated: String,
    val calculatedInterestAmount: String,
    val calculatedFineValue: String,
    val totalCharges: String,
    val payerName: String
) {

    fun showNewTransfer(): Boolean = receiptOrigin != ReceiptOrigin.STATEMENT

    fun showBackButton(): Boolean = receiptOrigin != ReceiptOrigin.STATEMENT && (!isSchedule || billData != null)

    fun getBeneficiaryFullBank(): String =
        "${if (transferBeneficiary?.bankCode == null) ""
        else transferBeneficiary?.bankCode + " - "}${transferBeneficiary?.bankDescription}"

    fun getDebitantFullBank(): String =
        "${if (transferDebitant.bankCode == null) ""
        else transferDebitant.bankCode + " - "}${transferDebitant.bankDescription}"

    fun getDebitantDocumentType(): String {
        return (
            if (transferDebitant.document.unmask().validateCNPJ()) {
                resourceManager.getString(R.string.receipt_document_CNPJ)
            } else {
                resourceManager.getString(R.string.receipt_document_CPF)
            }
            )
    }

    fun getBeneficiaryDocumentType(): String {
        return (
            if (transferBeneficiary?.document?.unmask()?.validateCNPJ() == true) {
                resourceManager.getString(R.string.receipt_document_CNPJ)
            } else {
                resourceManager.getString(R.string.receipt_document_CPF)
            }
            )
    }

    fun getFormattedDebitantAgency(): String = transferDebitant.branch?.formatAsAgencyNumber() ?: ""

    fun getFormattedBeneficiaryAgency(): String? = transferBeneficiary?.branch?.formatAsAgencyNumber()

    fun getFormattedDebitantDocument(): String =
        if (transferDebitant.document.indexOf('*') >= 0) {
            transferDebitant.document
        } else {
            transferDebitant.document.formatAsDocument()
        }

    fun getFormattedBeneficiaryDocument(): String? =
        if (transferType.toString() == resourceManager.getString(R.string.pix_type)) {
            transferBeneficiary?.document
        } else {
            transferBeneficiary?.document?.formatAsDocument()
        }

    fun isNPC(): Boolean = billData?.infoNPC != null
}