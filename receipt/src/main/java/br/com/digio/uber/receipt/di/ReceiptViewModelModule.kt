package br.com.digio.uber.receipt.di

import br.com.digio.uber.receipt.viewModel.ReceiptViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val receiptsModule = module {
    viewModel { ReceiptViewModel(get(), get()) }
}