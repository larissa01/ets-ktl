package br.com.digio.uber.receipt.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName
import br.com.digio.uber.receipt.ui.ReceiptFragment

class ReceiptNavigation(
    activity: AppCompatActivity
) : GenericNavigationWithFlow(activity) {

    fun init(bundle: Bundle?) {
        goTo(GoTo(ReceiptOpKey.RECEIPT_MAIN.name, bundle))
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            ReceiptOpKey.RECEIPT_MAIN.name to showReceiptMain()
        )

    private fun showReceiptMain(): GenericNavigationWIthFlowGetFragmentByName = {
        ReceiptFragment.newInstance(this, it)
    }
}