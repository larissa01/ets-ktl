package br.com.digio.uber.receipt.mapper

import android.text.Spannable
import android.text.SpannableStringBuilder
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.kenum.ReceiptOrigin
import br.com.digio.uber.model.receipt.Receipt
import br.com.digio.uber.receipt.R
import br.com.digio.uber.receipt.model.ReceiptUiModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.DatePattern
import br.com.digio.uber.util.EntryType
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.formatAsAccountNumber
import br.com.digio.uber.util.getDateTime
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.util.toCalendar
import br.com.digio.uber.util.toCalendarOrNull
import br.com.digio.uber.util.toCurrencyValue
import br.com.digio.uber.util.toMoney
import br.com.digio.uber.util.toMoneyNumber
import br.com.digio.uber.util.toPattern
import br.com.digio.uber.util.toPatternOrNull

class ReceiptMapper constructor(
    private val resourceManager: ResourceManager,
    private val receiptOrigin: ReceiptOrigin
) : AbstractMapper<Receipt, ReceiptUiModel> {
    override fun map(param: Receipt): ReceiptUiModel = with(param) {
        val newDebitant = debitant.copy(account = debitant.account?.formatAsAccountNumber())
        val newBeneficiary = beneficiary?.copy(account = beneficiary?.account?.formatAsAccountNumber())

        return ReceiptUiModel(
            transferDate = transactionDate.toCalendar().toPattern(DatePattern.DD_MM_YYYY_BR),
            transferTime = resourceManager.getString(
                R.string.receipt_time_text,
                transactionDate.toCalendar().toPattern(Const.DatePattern.HH_MM_BR)
            ),
            scheduleDate = schedulingDate.toCalendarOrNull()?.toPatternOrNull(DatePattern.DD_MM_YYYY_BR),
            transferDebitant = newDebitant,
            transferBeneficiary = newBeneficiary,
            transferDescription = createDescriptionText(comment ?: "", billData == null),
            transferValue = transactionValue.toMoneyNumber(),
            transferType = SpannableStringBuilder()
                .append(
                    resourceManager.getString(TransferType.fromType(this.transactionOperationType.type).resId)
                ),
            authenticationCode = authentication,
            comment = SpannableStringBuilder().append(comment ?: description ?: ""),
            isSchedule = statusTransaction == "SCHEDULED",
            isScheduleText = getTitle(statusTransaction, transactionOperationType.type, operation),
            schedule = if (statusTransaction == "SCHEDULED") createSpanableText(
                resourceManager.getString(R.string.receipt_transfer_schedule),
                transactionDate.toCalendar().toPattern(DatePattern.DD_MM_YYYY_BR)
            ) else SpannableStringBuilder(),
            resourceManager = resourceManager,
            receiptOrigin = receiptOrigin,
            amount = transactionValue.toMoney(EntryType.CREDIT),
            barcode = getBarcode(),
            billData = billData,
            transactionDate = transactionDate.toCalendarOrNull()?.getDateTime() ?: "",
            payeeName = billData?.infoNPC?.payeeName ?: billData?.infoNormal?.payeeName ?: "",
            payeeDocument = getPayeeDocument(),
            currencyValue = billData?.infoNPC?.computedValues?.totalAmountToCharge?.toCurrencyValue(false)
                ?: transactionValue.toMoney(EntryType.CREDIT),
            payerDocument = getPayerDocument(),
            finalPayerDocument = getFinalPayerDocument(),
            discountValueCalculated = billData?.infoNPC?.computedValues?.discountValueCalculated?.toCurrencyValue(true)
                ?: "",
            calculatedInterestAmount = billData?.infoNPC?.computedValues?.calculatedInterestAmount?.toCurrencyValue(
                true
            )
                ?: "",
            calculatedFineValue = billData?.infoNPC?.computedValues?.calculatedFineValue?.toCurrencyValue(true)
                ?: "",
            totalCharges = getTotalCharges(),
            payerName = billData?.infoNPC?.payerName ?: ""
        )
    }

    private fun Receipt.getBarcode() = if (billData?.barcode?.startsWith("8") == true) {
        billData?.barcode?.applyMask(Const.MaskPattern.BARCODE_ARRECADACAO)
    } else {
        billData?.barcode?.applyMask(Const.MaskPattern.BARCODE_CIP)
    } ?: ""

    private fun Receipt.getPayeeDocument() = billData?.infoNPC?.payeeDocument?.let {
        if (it.length == Const.MaskPattern.CPF_LENGTH) {
            it.applyMask(Const.MaskPattern.CPF)
        } else {
            it.applyMask(Const.MaskPattern.CNPJ)
        }
    } ?: ""

    private fun Receipt.getTotalCharges() =
        billData?.infoNPC?.computedValues?.calculatedInterestAmount?.plus(
            billData?.infoNPC?.computedValues?.calculatedFineValue ?: 0.0
        )?.toCurrencyValue(true) ?: ""

    private fun Receipt.getFinalPayerDocument() = billData?.infoNPC?.finalPayerDocument?.let {
        if (it.length == Const.MaskPattern.CPF_LENGTH) {
            it.applyMask(Const.MaskPattern.CPF)
        } else {
            it.applyMask(Const.MaskPattern.CNPJ)
        }
    } ?: ""

    private fun Receipt.getPayerDocument() = billData?.infoNPC?.payerDocument?.let {
        if (it.length == Const.MaskPattern.CPF_LENGTH) {
            it.applyMask(Const.MaskPattern.CPF)
        } else {
            it.applyMask(Const.MaskPattern.CNPJ)
        }
    } ?: ""

    private fun createDescriptionText(
        descriptionText: String = "",
        showLabel: Boolean
    ): Spannable {
        val builder = SpannableStringBuilder()
        if (showLabel) {
            builder.append(resourceManager.getString(R.string.receipt_description))
        }
        builder.append(" $descriptionText")
        return builder
    }

    private fun createSpanableText(
        firstText: String,
        secondText: String
    ): Spannable {
        return SpannableStringBuilder()
            .append(firstText)
            .append(" $secondText")
    }

    enum class TransferType(val resId: Int) {
        P2P(R.string.digio_transfer_type),
        TED_SO(R.string.same_owndership_type),
        TED_TP(R.string.other_ownership_type),
        BILL_PAYMENT(R.string.bill_payment_type),
        PIX(R.string.pix_type);

        companion object {
            fun fromType(type: String?): TransferType {
                return values().firstOrNull {
                    it.name == type
                } ?: throw IllegalArgumentException("TransferType $type not mapped.")
            }
        }
    }

    enum class OperationType(val resId: Int) {
        RECEIVING(R.string.receiving_operation_type),
        OPERATION(R.string.payment_operation_type);

        companion object {
            fun fromType(type: String?): OperationType {
                return values().firstOrNull {
                    it.name == type
                } ?: throw IllegalArgumentException("OperationType $type not mapped.")
            }
        }
    }

    private fun getTitle(statusTransaction: String, type: String, operaton: String): String {
        return if (statusTransaction == "SCHEDULED") {
            resourceManager.getString(R.string.schedule_receipt)
        } else {
            if (type == TransferType.BILL_PAYMENT.toString()) {
                resourceManager.getString(R.string.payment_receipt)
            } else if (type == TransferType.PIX.toString()) {
                if (operaton == OperationType.RECEIVING.toString()) {
                    resourceManager.getString(R.string.receivment_pix_receipt)
                } else {
                    resourceManager.getString(R.string.payment_pix_receipt)
                }
            } else {
                resourceManager.getString(R.string.transfer_receipt)
            }
        }
    }
}