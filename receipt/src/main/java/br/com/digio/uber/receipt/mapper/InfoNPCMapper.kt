package br.com.digio.uber.receipt.mapper

import br.com.digio.uber.model.receipt.Receipt
import br.com.digio.uber.receipt.model.InfoNPCPresentationModel
import br.com.digio.uber.util.mapper.AbstractMapper

interface InfoNPCMapper : AbstractMapper<Receipt.InfoNPC?, InfoNPCPresentationModel?>