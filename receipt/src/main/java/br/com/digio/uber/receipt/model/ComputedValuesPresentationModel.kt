package br.com.digio.uber.receipt.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ComputedValuesPresentationModel(
    var calculatedInterestAmount: Double? = null,
    var calculatedFineValue: Double? = null,
    var discountValueCalculated: Double? = null,
    var totalAmountToCharge: Double? = null,
    var paymentAmountUpdated: Double? = null,
    var computedDate: String? = null
) : Parcelable