package br.com.digio.uber.receipt.ui

import android.app.Activity.RESULT_OK
import android.view.View
import androidx.core.view.drawToBitmap
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.ReceiptOrigin
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.receipt.BR
import br.com.digio.uber.receipt.R
import br.com.digio.uber.receipt.databinding.FragmentReceiptBinding
import br.com.digio.uber.receipt.model.ReceiptUiModel
import br.com.digio.uber.receipt.navigation.ReceiptOpKey
import br.com.digio.uber.receipt.ui.ReschedulingDialogFragment.Companion.RESCHEDULING_DIALOG_TAG
import br.com.digio.uber.receipt.viewModel.ReceiptViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.MimeTypes.RECEIPT_PRINT_FILENAME
import br.com.digio.uber.util.Const.RequestOnResult.Transfer.FINISH_TRANSFER_FLOW_RESULT_CODE
import br.com.digio.uber.util.createUri
import br.com.digio.uber.util.getProviderPackage
import br.com.digio.uber.util.safeLet
import br.com.digio.uber.util.shareReceiptIntent
import org.koin.android.ext.android.inject

class ReceiptFragment : BNWFViewModelFragment<FragmentReceiptBinding, ReceiptViewModel>() {

    override val bindingVariable: Int = BR.receiptViewModel
    override val getLayoutId: Int = R.layout.fragment_receipt
    override val viewModel: ReceiptViewModel by inject()
    override fun tag(): String = ReceiptOpKey.RECEIPT_MAIN.name
    override fun getStatusBarAppearance(): StatusBarColor = ReceiptOpKey.RECEIPT_MAIN.theme

    override fun initialize() {
        super.initialize()
        arguments?.let { bundle ->
            bundle.getString(Const.Extras.RECEIPT_ID)?.let { receiptId ->
                viewModel.initializer(receiptId) { onItemClicked(it) }
            }
            bundle.getSerializable(Const.Extras.RECEIPT_ORIGIN)?.let { receiptOrigin ->
                viewModel.receiptOrigin = receiptOrigin as ReceiptOrigin
            }
            bundle.getBoolean(Const.Extras.PAYMENT_RESCHEDULED).let { wasScheduled ->
                viewModel.paymentWasRescheduled = wasScheduled
            }
        }
        setupObservers()
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.button_receipt_back -> {
                activity?.apply {
                    setResult(FINISH_TRANSFER_FLOW_RESULT_CODE)
                    finish()
                }
            }
            R.id.button_receipt_share -> {
                safeLet(binding?.constraintReceipt?.drawToBitmap(), context) { bitmapLet, contextLet ->
                    val printBitmap = viewModel.prepareBitmap(bitmapLet, resources.getDimensionPixelSize(R.dimen.dp_16))
                    val uri = printBitmap.createUri(
                        contextLet,
                        contextLet.getProviderPackage(),
                        RECEIPT_PRINT_FILENAME
                    )
                    contextLet.shareReceiptIntent(title = getString(R.string.receipt_intent_share), uri = uri)
                }
            }
            R.id.button_receipt_new_transfer -> {
                activity?.apply {
                    setResult(RESULT_OK)
                    finish()
                }
            }
            R.id.button_receipt_new_payment -> {
                activity?.let {
                    navigation.navigationToPaymentActivity(it)
                    it.finish()
                }
            }
        }
    }

    private fun setupObservers() {
        viewModel.receiptUiModel.observe(
            viewLifecycleOwner,
            {
                if (viewModel.paymentWasRescheduled) {
                    showReschedulingDialog(it)
                }
            }
        )
    }

    private fun showReschedulingDialog(receiptItem: ReceiptUiModel) {
        val dialog = ReschedulingDialogFragment.newInstance(receiptItem)
        activity?.supportFragmentManager?.let {
            dialog.show(it, RESCHEDULING_DIALOG_TAG)
        }
    }

    companion object : FragmentNewInstance<ReceiptFragment>(::ReceiptFragment)
}