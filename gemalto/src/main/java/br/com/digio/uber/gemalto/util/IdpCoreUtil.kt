package br.com.digio.uber.gemalto.util

import android.content.Context
import br.com.digio.uber.gemalto.BuildConfig
import com.gemalto.idp.mobile.core.ApplicationContextHolder
import com.gemalto.idp.mobile.core.IdpCore
import com.gemalto.idp.mobile.otp.OtpConfiguration

internal object IdpCoreUtil {
    internal fun isConfigured(): Boolean =
        if (BuildConfig.ENABLED_GEMALTO) {
            IdpCore.isConfigured()
        } else {
            false
        }

    internal fun getInstance(): IdpCore? =
        if (BuildConfig.ENABLED_GEMALTO) {
            IdpCore.getInstance()
        } else {
            null
        }

    internal fun getOtpConfigurationBuild(): OtpConfiguration.Builder? =
        if (BuildConfig.ENABLED_GEMALTO) {
            OtpConfiguration.Builder()
        } else {
            null
        }

    internal fun configure(bool: Boolean, otpConfig: OtpConfiguration): IdpCore? =
        if (BuildConfig.ENABLED_GEMALTO) {
            IdpCore.configure(bool, otpConfig)
        } else {
            null
        }

    internal fun applicationContextHolderSetContext(context: Context) {
        if (BuildConfig.ENABLED_GEMALTO) {
            ApplicationContextHolder.setContext(context.applicationContext)
        }
    }
}