package br.com.digio.uber.gemalto.implementation

import br.com.digio.uber.gemalto.GemaltoSetupLogic
import br.com.digio.uber.gemalto.exception.GemaltoException
import br.com.digio.uber.gemalto.model.GemaltoCredential
import com.gemalto.idp.mobile.otp.oath.soft.SoftOathToken
import timber.log.Timber

class GemaltoSetupLogicFake : GemaltoSetupLogic {

    override suspend fun checkCpfHasToken(cpf: String): Boolean =
        false

    override suspend fun saveCredentials(credential: GemaltoCredential): Boolean =
        true

    override suspend fun getCredentials(cpf: String): GemaltoCredential? =
        GemaltoCredential("FakeCpf", "FakePin", "FakeRegistration")

    override suspend fun getToken(cpf: String, registrationCode: String): SoftOathToken =
        throw GemaltoException("This method can't be call in this mode")

    override suspend fun getTokenWithoutResult(cpf: String, registrationCode: String) {
        /* nothing */
    }

    override suspend fun generateOTP(
        values: Map<String, Any>,
        credentials: GemaltoCredential
    ): String =
        "fakeOTP"

    override suspend fun clearTokenCpf(cpf: String) {
        Timber.d("Gemalto not enable")
    }
}