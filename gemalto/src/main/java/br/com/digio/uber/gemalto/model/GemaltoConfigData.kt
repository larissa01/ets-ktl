package br.com.digio.uber.gemalto.model

internal data class GemaltoConfigData(
    val rsaKeyModulus: ByteArray,
    val rsaKeyExponent: ByteArray,
    val url: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GemaltoConfigData

        if (!rsaKeyModulus.contentEquals(other.rsaKeyModulus)) return false
        if (!rsaKeyExponent.contentEquals(other.rsaKeyExponent)) return false
        if (url != other.url) return false

        return true
    }

    override fun hashCode(): Int {
        var result = rsaKeyModulus.contentHashCode()
        result = 31 * result + rsaKeyExponent.contentHashCode()
        result = 31 * result + url.hashCode()
        return result
    }
}