package br.com.digio.uber.gemalto.implementation

import br.com.digio.uber.gemalto.GemaltoInitConfiguration
import timber.log.Timber

object GemaltoInitConfigurationFake : GemaltoInitConfiguration {
    override fun initConfiguration() {
        Timber.d("Gemalto disable")
    }
}