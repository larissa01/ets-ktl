package br.com.digio.uber.gemalto.exception

class GemaltoException(message: String) : Exception(message)