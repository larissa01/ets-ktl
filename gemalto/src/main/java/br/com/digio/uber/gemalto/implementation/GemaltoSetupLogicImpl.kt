package br.com.digio.uber.gemalto.implementation

import android.annotation.SuppressLint
import br.com.digio.uber.gemalto.GemaltoSetupLogic
import br.com.digio.uber.gemalto.config.GemaltoConfig
import br.com.digio.uber.gemalto.implementation.GemaltoStorage.PIN_CODE
import br.com.digio.uber.gemalto.implementation.GemaltoStorage.REGISTRATION_CODE
import br.com.digio.uber.gemalto.model.GemaltoCredential
import br.com.digio.uber.util.safeHeritage
import br.com.digio.uber.util.safeLet
import com.gemalto.idp.mobile.authentication.AuthenticationModule
import com.gemalto.idp.mobile.authentication.mode.pin.PinAuthService
import com.gemalto.idp.mobile.core.IdpCore
import com.gemalto.idp.mobile.core.IdpException
import com.gemalto.idp.mobile.core.devicefingerprint.DeviceFingerprintSource
import com.gemalto.idp.mobile.otp.OtpModule
import com.gemalto.idp.mobile.otp.Token
import com.gemalto.idp.mobile.otp.TokenManager
import com.gemalto.idp.mobile.otp.devicefingerprint.DeviceFingerprintTokenPolicy
import com.gemalto.idp.mobile.otp.oath.OathDevice
import com.gemalto.idp.mobile.otp.oath.OathService
import com.gemalto.idp.mobile.otp.oath.OathTokenManager
import com.gemalto.idp.mobile.otp.oath.soft.SoftOathToken
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.withContext
import timber.log.Timber
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

internal class GemaltoSetupLogicImpl constructor(
    private val core: IdpCore
) : GemaltoSetupLogicBaseConfiguration(core),
    GemaltoSetupLogic {

    @Suppress("TooGenericExceptionCaught")
    override suspend fun checkCpfHasToken(cpf: String): Boolean =
        withContext(Default) {
            try {
                makeLogin()
                val oathTokenManager: OathTokenManager =
                    OathService.create(OtpModule.create()).tokenManager
                oathTokenManager.tokenNames.contains(cpf)
            } catch (e: Throwable) {
                Timber.e(e)
                false
            } finally {
                makeLogout()
            }
        }

    @Suppress("TooGenericExceptionCaught")
    override suspend fun saveCredentials(credential: GemaltoCredential): Boolean =
        withContext(Default) {
            try {
                val gemaltoStorage =
                    GemaltoStorage.getInstance(core)

                listOf(
                    gemaltoStorage.writeProperty(
                        credential.cpf,
                        REGISTRATION_CODE,
                        credential.registrationCode
                    ),
                    gemaltoStorage.writeProperty(credential.cpf, PIN_CODE, credential.pin)
                ).all { it }
            } catch (e: Exception) {
                Timber.e(e)
                false
            }
        }

    @Suppress("TooGenericExceptionCaught")
    override suspend fun getCredentials(cpf: String): GemaltoCredential? =
        withContext(Default) {
            try {
                val gemaltoStorage =
                    GemaltoStorage.getInstance(core)
                val gemaltoCredential = safeLet(
                    gemaltoStorage.readProperty(cpf, REGISTRATION_CODE),
                    gemaltoStorage.readProperty(cpf, PIN_CODE)
                ) { registrationLet, pinLet ->
                    GemaltoCredential(
                        cpf = cpf,
                        pin = pinLet,
                        registrationCode = registrationLet
                    )
                }

                gemaltoCredential
            } catch (e: Exception) {
                Timber.e(e)
                null
            }
        }

    override suspend fun getToken(cpf: String, registrationCode: String): SoftOathToken =
        withContext(Default) {
            makeLogin()
            val oathTokenManager: OathTokenManager =
                OathService.create(OtpModule.create()).tokenManager
            val gemaltoConfig = GemaltoConfig.getConfig()
            val provisioningConfiguration = getEpsConfiguration(registrationCode, gemaltoConfig)

            val deviceFingerprintSource = DeviceFingerprintSource(DeviceFingerprintSource.Type.SOFT)
            val deviceFingerprintTokenPolicy =
                DeviceFingerprintTokenPolicy(true, deviceFingerprintSource)

            withContext(Default) {
                suspendCoroutine<SoftOathToken> {
                    if (oathTokenManager.tokenNames.contains(cpf)) {
                        it.resume(oathTokenManager.getToken(cpf))
                    } else {
                        oathTokenManager.createToken(
                            cpf,
                            provisioningConfiguration,
                            deviceFingerprintTokenPolicy,
                            object : TokenManager.TokenCreationCallback {
                                @SuppressLint("LogNotTimber")
                                override fun onSuccess(
                                    token: Token?,
                                    p1: MutableMap<String, String>?
                                ) {
                                    token?.safeHeritage<SoftOathToken>()?.let { tokenLet ->
                                        it.resume(tokenLet)
                                    } ?: run {
                                        if (oathTokenManager.tokenNames.contains(cpf)) {
                                            it.resume(oathTokenManager.getToken(cpf))
                                        }
                                    }
                                    makeLogout()
                                }

                                override fun onError(exception: IdpException?) {
                                    Timber.e(exception)
                                    it.resumeWithException(
                                        exception ?: Exception("Empty exception")
                                    )
                                    makeLogout()
                                }
                            }
                        )
                    }
                }
            }
        }

    override suspend fun getTokenWithoutResult(cpf: String, registrationCode: String) {
        getToken(cpf, registrationCode)
    }

    @Suppress("TooGenericExceptionCaught")
    override suspend fun generateOTP(
        values: Map<String, Any>,
        credentials: GemaltoCredential
    ): String =
        withContext(Default) {
            try {
                checkTimeoutHasPassed()
                val oathTokenManager: OathTokenManager =
                    OathService.create(OtpModule.create()).tokenManager
                makeLogin()
                val softOathToken = try {
                    oathTokenManager.getToken<SoftOathToken>(credentials.cpf)
                } catch (e: Exception) {
                    getToken(credentials.cpf, credentials.registrationCode)
                }

                val oathFactory = OathService.create(OtpModule.create()).factory
                val softOathSettings =
                    OathService.create(OtpModule.create()).factory.createSoftOathSettings()
                val option = getSecureString(values)
                softOathSettings.setOcraSuite(core.secureContainerFactory.fromString("OCRA-1:HOTP-SHA256-8:QH64-T30S"))
                val oathDevice: OathDevice =
                    oathFactory.createSoftOathDevice(softOathToken, softOathSettings)
                val otp = PinAuthService.create(AuthenticationModule.create())
                    .createAuthInput(credentials.pin).let { authInputPin ->
                        val otp = oathDevice.getOcraOtp(authInputPin, option, null, null, null)
                        authInputPin.wipe()
                        makeLogout()
                        otp
                    }
                return@withContext otp.toString()
            } finally {
                makeLogout()
            }
        }

    @Suppress("TooGenericExceptionCaught")
    override suspend fun clearTokenCpf(cpf: String) {
        withContext(Default) {
            try {
                makeLogin()
                val oathTokenManager: OathTokenManager =
                    OathService.create(OtpModule.create()).tokenManager
                oathTokenManager.removeToken(cpf)
                GemaltoStorage.getInstance(core).deleteProperties(cpf)
            } catch (e: Throwable) {
                Timber.e(e)
            } finally {
                makeLogout()
            }
        }
    }
}