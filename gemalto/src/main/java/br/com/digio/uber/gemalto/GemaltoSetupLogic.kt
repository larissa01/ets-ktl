package br.com.digio.uber.gemalto

import br.com.digio.uber.gemalto.model.GemaltoCredential
import com.gemalto.idp.mobile.otp.oath.soft.SoftOathToken

interface GemaltoSetupLogic {
    suspend fun checkCpfHasToken(cpf: String): Boolean
    suspend fun saveCredentials(credential: GemaltoCredential): Boolean
    suspend fun getCredentials(cpf: String): GemaltoCredential?
    suspend fun getToken(cpf: String, registrationCode: String): SoftOathToken
    suspend fun getTokenWithoutResult(cpf: String, registrationCode: String)
    suspend fun generateOTP(values: Map<String, Any>, credentials: GemaltoCredential): String
    suspend fun clearTokenCpf(cpf: String)
}