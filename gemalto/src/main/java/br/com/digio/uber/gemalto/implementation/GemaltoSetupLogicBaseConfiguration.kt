package br.com.digio.uber.gemalto.implementation

import br.com.digio.uber.gemalto.BuildConfig
import br.com.digio.uber.gemalto.model.GemaltoConfigData
import com.gemalto.idp.mobile.core.IdpCore
import com.gemalto.idp.mobile.core.net.TlsConfiguration
import com.gemalto.idp.mobile.core.util.SecureString
import com.gemalto.idp.mobile.otp.provisioning.EpsConfigurationBuilder
import com.gemalto.idp.mobile.otp.provisioning.MobileProvisioningProtocol
import com.gemalto.idp.mobile.otp.provisioning.ProvisioningConfiguration
import kotlinx.coroutines.delay
import java.io.ByteArrayOutputStream
import java.net.URL
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.Calendar

internal open class GemaltoSetupLogicBaseConfiguration constructor(
    private val core: IdpCore
) {

    private val delayTime =
        DELAY_TIME_DEFAULT
    private var date: Long = -1L

    internal fun getEpsConfiguration(
        registrationCode: String,
        gemaltoConfig: GemaltoConfigData
    ): ProvisioningConfiguration? =
        EpsConfigurationBuilder(
            core.secureContainerFactory.fromString(registrationCode),
            URL(gemaltoConfig.url),
            BuildConfig.GEMALTO_DOMAIN,
            MobileProvisioningProtocol.PROVISIONING_PROTOCOL_V5,
            BuildConfig.GEMALTO_RSA_KEY_ID,
            gemaltoConfig.rsaKeyExponent,
            gemaltoConfig.rsaKeyModulus
        ).setTlsConfiguration(TlsConfiguration(TLS_TIMEOUT_CONFIG)).build()

    internal suspend fun checkTimeoutHasPassed() {
        val restTime = Calendar.getInstance().timeInMillis - date
        date = if (restTime < delayTime && date != -1L) {
            delay(delayTime - restTime)
            Calendar.getInstance().timeInMillis
        } else {
            Calendar.getInstance().timeInMillis
        }
    }

    internal fun makeLogout() {
        if (core.passwordManager.isLoggedIn) {
            core.passwordManager.logout()
        }
    }

    internal fun makeLogin() {
        if (!core.passwordManager.isLoggedIn) {
            core.passwordManager.login()
        }
    }

    internal fun getSecureString(values: Map<String, Any>): SecureString =
        IdpCore.getInstance().secureContainerFactory.fromString(
            getOcraChallenge(values.entries.map { it.key to it.value }.toList())
        )

    private fun getOcraChallenge(values: List<Pair<String, Any>>): String {
        var retValue: String? = null

        // Use builder to append TLV
        val buffer = ByteArrayOutputStream()

        // Go through all values, calculate and append TLV for each one of them.
        for (index in values.indices) {
            // Convert keyvalue to UTF8 string
            val keyValueUTF8 = values[index].getKeyValueUTF8()

            // Build TLV.
            buffer.write(HEX_INIT_TRANSMISSION)
            buffer.write(HEX_INIT_SIZE_TRANSMISSION + index)
            buffer.write(keyValueUTF8.size)
            buffer.write(keyValueUTF8, 0, keyValueUTF8.size)
        }

        // Try to calculate digest from final string and build retValue.
        try {
            val digest = MessageDigest.getInstance("SHA-256")
            val hash = digest.digest(buffer.toByteArray())

            // Server challenge expect hex string not byte array.
            retValue = bytesToHex(hash)
        } catch (exception: NoSuchAlgorithmException) {
            exception.printStackTrace()
        }

        return retValue.toString()
    }

    private fun <A, B> Pair<A, B>.getKeyValueUTF8(): ByteArray =
        "$first:$second".toByteArray(Charsets.UTF_8)

    private fun bytesToHex(bytes: ByteArray): String {
        val hexArray = "0123456789ABCDEF".toCharArray()

        val hexChars = CharArray(bytes.size * 2)
        for (index in bytes.indices) {
            val value = bytes[index].toInt() and HEX_BYTE_CHECK
            hexChars[index * 2] = hexArray[value.ushr(HEX_SHIFT_COUNT)]
            hexChars[index * 2 + 1] = hexArray[value and 0x0F]
        }
        return String(hexChars)
    }

    companion object {
        private const val DELAY_TIME_DEFAULT = 30000L
        private const val TLS_TIMEOUT_CONFIG = 10000
        private const val HEX_INIT_TRANSMISSION = 0xDF
        private const val HEX_INIT_SIZE_TRANSMISSION = 0x71
        private const val HEX_BYTE_CHECK = 0xFF
        private const val HEX_SHIFT_COUNT = 4
    }
}