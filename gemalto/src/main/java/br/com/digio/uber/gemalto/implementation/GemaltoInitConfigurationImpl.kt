package br.com.digio.uber.gemalto.implementation

import br.com.digio.uber.gemalto.BuildConfig
import br.com.digio.uber.gemalto.GemaltoInitConfiguration
import com.gemalto.idp.mobile.core.SecurityDetectionService

class GemaltoInitConfigurationImpl : GemaltoInitConfiguration {
    override fun initConfiguration() {
        SecurityDetectionService.setDebuggerDetection(BuildConfig.ENABLED_GEMALTO)
    }
}