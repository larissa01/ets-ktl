package br.com.digio.uber.gemalto.model

import android.os.Parcelable
import br.com.digio.uber.model.gemalto.Enroll
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GemaltoCredential constructor(
    val cpf: String,
    val pin: String,
    val registrationCode: String
) : Parcelable {
    fun getEnrollFromGemaltoCredential(): Enroll =
        Enroll(
            pin,
            registrationCode
        )
}