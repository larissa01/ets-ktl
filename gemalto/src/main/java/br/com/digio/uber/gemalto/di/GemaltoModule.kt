package br.com.digio.uber.gemalto.di

import br.com.digio.uber.gemalto.BuildConfig
import br.com.digio.uber.gemalto.GemaltoInitConfiguration
import br.com.digio.uber.gemalto.GemaltoSetupLogic
import br.com.digio.uber.gemalto.exception.GemaltoException
import br.com.digio.uber.gemalto.implementation.GemaltoInitConfigurationFake
import br.com.digio.uber.gemalto.implementation.GemaltoInitConfigurationImpl
import br.com.digio.uber.gemalto.implementation.GemaltoSetupLogicFake
import br.com.digio.uber.gemalto.implementation.GemaltoSetupLogicImpl
import br.com.digio.uber.gemalto.util.IdpCoreUtil
import com.gemalto.idp.mobile.core.IdpCore
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val gemaltoModule = module {
    single<IdpCore> {
        IdpCoreUtil.applicationContextHolderSetContext(androidContext())
        if (!IdpCoreUtil.isConfigured()) {
            IdpCoreUtil.getOtpConfigurationBuild()
                ?.build()
                ?.let { otpConfig ->
                    IdpCoreUtil.configure(true, otpConfig)
                } ?: throw GemaltoException("Can't create gemalto fake")
        } else {
            IdpCore.getInstance()
        }
    }
    single<GemaltoSetupLogic> {
        if (BuildConfig.ENABLED_GEMALTO) {
            GemaltoSetupLogicImpl(
                get()
            )
        } else {
            GemaltoSetupLogicFake()
        }
    }
    single<GemaltoInitConfiguration> {
        if (BuildConfig.ENABLED_GEMALTO) {
            GemaltoInitConfigurationImpl()
        } else {
            GemaltoInitConfigurationFake
        }
    }
}