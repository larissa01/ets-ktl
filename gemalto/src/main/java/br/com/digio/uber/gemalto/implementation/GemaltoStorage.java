package br.com.digio.uber.gemalto.implementation;

import com.gemalto.idp.mobile.core.IdpCore;
import com.gemalto.idp.mobile.core.devicefingerprint.DeviceFingerprintException;
import com.gemalto.idp.mobile.core.devicefingerprint.DeviceFingerprintSource;
import com.gemalto.idp.mobile.core.passwordmanager.PasswordManager;
import com.gemalto.idp.mobile.core.passwordmanager.PasswordManagerException;
import com.gemalto.idp.mobile.core.util.SecureByteArray;
import com.gemalto.idp.mobile.securestorage.IdpSecureStorageException;
import com.gemalto.idp.mobile.securestorage.PropertyStorage;
import com.gemalto.idp.mobile.securestorage.SecureStorageManager;
import com.gemalto.idp.mobile.securestorage.SecureStorageModule;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import timber.log.Timber;

class GemaltoStorage {

    private static GemaltoStorage instance;
    private IdpCore core;

    private GemaltoStorage(IdpCore core) {
        this.core = core;
    }

    static String REGISTRATION_CODE = "REGISTRATION_CODE";
    static String PIN_CODE = "PIN_CODE";

    static GemaltoStorage getInstance(IdpCore core) {
        if (instance != null) {
            return instance;
        }
        instance = new GemaltoStorage(core);
        return instance;
    }

    @NotNull
    boolean writeProperty(String propertyStorage, String key, String value) {
        try {
            PasswordManager pm = getPasswordManager(core);

            byte[] keyBytes = key.getBytes();
            SecureByteArray data = core.getSecureContainerFactory().fromString(value);

            PropertyStorage storage = getPropertyStorage(propertyStorage);

            storage.open();
            storage.writeProperty(keyBytes, data, false);
            storage.close();

            pm.logout();
            return true;
        } catch (PasswordManagerException | DeviceFingerprintException | IdpSecureStorageException e) {
            Timber.e(e);
            return false;
        }
    }

    @Nullable
    String readProperty(String propertyStorage, String key) {
        try {
            PasswordManager pm = getPasswordManager(core);

            byte[] keyBytes = key.getBytes();

            PropertyStorage storage = getPropertyStorage(propertyStorage);

            storage.open();
            String value = new String(storage.readProperty(keyBytes).toByteArray());
            storage.close();

            pm.logout();
            return value;
        } catch (PasswordManagerException | DeviceFingerprintException | IdpSecureStorageException e) {
            Timber.e(e);
            return null;
        }
    }

    boolean deleteProperties(String propertyStorage) {
        try {
            PasswordManager pm = getPasswordManager(core);

            PropertyStorage storage = getPropertyStorage(propertyStorage);

            storage.open();
            storage.deleteAllProperties();
            storage.close();

            pm.logout();
            return true;
        } catch (PasswordManagerException | DeviceFingerprintException | IdpSecureStorageException e) {
            Timber.e(e);
            return false;
        }
    }

    private PropertyStorage getPropertyStorage(String propertyStorage) throws IdpSecureStorageException {
        SecureStorageModule secureStorageModule = SecureStorageModule.create();
        SecureStorageManager ssManager = secureStorageModule.getSecureStorageManager();
        DeviceFingerprintSource dfs = DeviceFingerprintSource.DEFAULT;
        return ssManager.getPropertyStorage(propertyStorage, dfs);
    }

    @NotNull
    private PasswordManager getPasswordManager(IdpCore core) throws PasswordManagerException {
        PasswordManager pm = core.getPasswordManager();
        if (!pm.isLoggedIn()) {
            pm.login();
        }
        return pm;
    }

}
