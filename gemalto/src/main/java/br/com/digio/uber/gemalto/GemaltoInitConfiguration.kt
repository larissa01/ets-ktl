package br.com.digio.uber.gemalto

interface GemaltoInitConfiguration {
    fun initConfiguration()
}