package br.com.digio.uber.gemalto.config

import br.com.digio.uber.gemalto.BuildConfig
import br.com.digio.uber.gemalto.model.GemaltoConfigData
import br.com.digio.uber.util.BuildTypes

object GemaltoConfig {

    const val GEMALTO_URL_HML =
        "https://provisioner-eps-demo.rnd.gemaltodigitalbankingidcloud.com/provisioner/domains/digio/provision"
    const val GEMALTO_URL_PROD =
        "https://provisioner.gemaltodigitalbankingidcloud.com/provisioner/domains/digio/provision"

    internal fun getConfig(): GemaltoConfigData =
        when (BuildConfig.BUILD_TYPE) {
            BuildTypes.DEBUG.value -> GemaltoConfigData(
                rsaKeyModulus = GemaltoCredential.rsaKeyModulusHml,
                rsaKeyExponent = GemaltoCredential.rsaKeyExponent,
                url = GEMALTO_URL_HML
            )
            BuildTypes.HOMOL.value -> GemaltoConfigData(
                rsaKeyModulus = GemaltoCredential.rsaKeyModulusHml,
                rsaKeyExponent = GemaltoCredential.rsaKeyExponent,
                url = GEMALTO_URL_HML
            )
            BuildTypes.RELEASE.value -> GemaltoConfigData(
                rsaKeyModulus = GemaltoCredential.rsaKeyModulusProd,
                rsaKeyExponent = GemaltoCredential.rsaKeyExponent,
                url = GEMALTO_URL_PROD
            )
            else -> GemaltoConfigData(
                rsaKeyModulus = GemaltoCredential.rsaKeyModulusHml,
                rsaKeyExponent = GemaltoCredential.rsaKeyExponent,
                url = GEMALTO_URL_HML
            )
        }
}