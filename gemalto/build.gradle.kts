plugins {
    id("com.android.library")
    kotlin("android")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.library")
}

val gemaltoDomainHml: String by project
val gemaltoRsaKeyIdHml: String by project
val gemaltoDomain: String by project
val gemaltoRsaKeyId: String by project

configureAndroidLibrary(
    releaseBuildTypeConfig = {
        buildConfigField("String", "GEMALTO_RSA_KEY_ID", gemaltoRsaKeyId)
        buildConfigField("String", "GEMALTO_DOMAIN", gemaltoDomain)
        buildConfigField("Boolean", "ENABLED_GEMALTO", "true")
        it.packagingOptions {
            exclude("**/libs/gemalto/debug/x86/*")
            exclude("**/libs/gemalto/release/x86/*")
            exclude("**/libs/gemalto/debug/x86_64/*")
            exclude("**/libs/gemalto/release/x86_64/*")
        }
    },
    homolBuildTypeConfig = {
        buildConfigField("String", "GEMALTO_RSA_KEY_ID", gemaltoRsaKeyIdHml)
        buildConfigField("String", "GEMALTO_DOMAIN", gemaltoDomainHml)
        buildConfigField("Boolean", "ENABLED_GEMALTO", "true")
        it.packagingOptions {
            exclude("**/libs/gemalto/debug/x86/*")
            exclude("**/libs/gemalto/release/x86/*")
            exclude("**/libs/gemalto/debug/x86_64/*")
            exclude("**/libs/gemalto/release/x86_64/*")
        }
    },
    debugBuildTypeConfig = {
        buildConfigField("String", "GEMALTO_RSA_KEY_ID", gemaltoRsaKeyIdHml)
        buildConfigField("String", "GEMALTO_DOMAIN", gemaltoDomainHml)
        buildConfigField("Boolean", "ENABLED_GEMALTO", "false")
    }
)

dependencies {
    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.COROUTINES)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)
    implementation(Dependencies.JNA)
    implementation(Dependencies.TIMBER)

    // Gemalto SDK
    releaseImplementation(files("libs/gemalto/release/libidpmobile.jar"))
    homolImplementation(files("libs/gemalto/release/libidpmobile.jar"))
    debugImplementation(files("libs/gemalto/debug/libidpmobile.jar"))

    testImplementation(TestDependencies.JUNIT)
    testImplementation(TestDependencies.MOCKK)
    testImplementation(TestDependencies.COROUTINESTEST)

    implementation(project(":model"))
    implementation(project(":util"))
}