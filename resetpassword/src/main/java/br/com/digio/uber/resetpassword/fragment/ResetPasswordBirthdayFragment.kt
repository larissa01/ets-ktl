package br.com.digio.uber.resetpassword.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.resetpassword.BR
import br.com.digio.uber.resetpassword.R
import br.com.digio.uber.resetpassword.databinding.ResetPasswordBirthdayFragmentBinding
import br.com.digio.uber.resetpassword.viewmodel.ResetPasswordBirthdayViewModel
import org.koin.android.ext.android.inject

class ResetPasswordBirthdayFragment :
    BaseViewModelFragment<ResetPasswordBirthdayFragmentBinding, ResetPasswordBirthdayViewModel>() {

    override val bindingVariable: Int? =
        BR.resetPasswordBirthdayViewModel

    override val getLayoutId: Int? =
        R.layout.reset_password_birthday_fragment

    override val viewModel: ResetPasswordBirthdayViewModel? by inject()

    private val analytics: Analytics by inject()

    private val userLogin: String by lazy {
        ResetPasswordBirthdayFragmentArgs
            .fromBundle(requireArguments())
            .userLogin
    }

    override fun tag(): String = ResetPasswordBirthdayFragment::class.java.name

    override fun initialize() {
        super.initialize()
        viewModel?.initializer(userLogin) {}
        setupObserver()
    }

    override fun getToolbar(): Toolbar? =
        binding?.includeAppBarResetPassword?.toolbarResetPassword

    private fun setupObserver() {
        viewModel?.goToSuccess?.observe(
            this,
            Observer { email ->
                if (email != null) {
                    analytics.pushSimpleEvent(Tags.RESET_PASSWORD_BIRTHDATE)
                    navigateTo(
                        ResetPasswordBirthdayFragmentDirections
                            .actionResetPasswordBirthdayFragmentToResetPasswordConfirmationFragment(
                                email
                            )
                    )
                }
            }
        )
    }

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE

    override fun onNavigationClick(view: View) {
        pop()
    }

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true
}