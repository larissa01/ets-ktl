package br.com.digio.uber.resetpassword.activity

import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.resetpassword.R
import br.com.digio.uber.resetpassword.di.viewmodel.resetPasswordViewModelModule
import org.koin.core.module.Module

class ResetPasswordActivity : BaseActivity() {

    override fun getLayoutId(): Int = R.layout.reset_password_activity

    override fun getModule(): List<Module> = listOf(resetPasswordViewModelModule)
}