package br.com.digio.uber.resetpassword.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.resetpassword.R

class ResetPasswordConfirmationViewModel constructor(
    private val resourceManager: ResourceManager
) : BaseViewModel() {
    val textSubtitle: MutableLiveData<String> = MutableLiveData()

    fun addEmailToSubtitle(email: String) {
        textSubtitle.value = resourceManager.getString(R.string.access_you_email, email)
    }
}