package br.com.digio.uber.resetpassword.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.common.typealiases.OnTextChanged
import br.com.digio.uber.domain.usecase.resetpassword.ResetPasswordUseCase
import br.com.digio.uber.model.resetpassword.ResetPasswordReq
import br.com.digio.uber.resetpassword.R
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.FieldsRegex
import br.com.digio.uber.util.getDate
import br.com.digio.uber.util.isOlderThanEighteen
import br.com.digio.uber.util.safeLet
import br.com.digio.uber.util.toCalendar

class ResetPasswordBirthdayViewModel constructor(
    resourceManager: ResourceManager,
    private val analytics: Analytics,
    private val resetPasswordUseCase: ResetPasswordUseCase
) : BaseViewModel() {

    val date: MutableLiveData<String> = MutableLiveData()
    val dateError: MutableLiveData<String> = MutableLiveData()
    val enableButton: MutableLiveData<Boolean> = MutableLiveData()
    val goToSuccess: MutableLiveData<String> = MutableLiveData()
    private val userLogin: MutableLiveData<String> = MutableLiveData()

    val mask = Const.MaskPattern.DATE

    private val dateBirthErrorString: String =
        resourceManager.getString(R.string.date_birth_invalid)

    val onBirthDateChange: OnTextChanged = { value, _, _, _ ->
        enableButton.value = isValidatedInputData(value.toString())
    }

    fun initializer(userLogin: String, onClickItem: OnClickItem) {
        this.userLogin.value = userLogin
        super.initializer {
            when (it.id) {
                R.id.btnNext -> sendResetPassword(date.value)
                else -> onClickItem.invoke(it)
            }
        }
    }

    private fun sendResetPassword(value: String?) {
        safeLet(value?.toCalendar()?.getDate(), userLogin.value) { birthDate, userLogin ->
            resetPasswordUseCase(
                ResetPasswordReq(
                    birthDate = birthDate,
                    username = userLogin
                )
            ).singleExec(
                onError = { _, error, _ ->
                    pushErrorToAnalytics()
                    if (error.messageString != null) {
                        dateError.value = error.messageString
                        EmptyErrorObject
                    } else {
                        error
                    }
                },
                onSuccessBaseViewModel = { res ->
                    if (res.email != null) {
                        goToSuccess.value = res.email
                    }
                }
            )
        }
    }

    private fun isValidatedInputData(value: String): Boolean {
        return if (value.length == MAX_DATE_LENGTH) {
            return isValidatedDateBirth(value)
        } else {
            dateError.value = null
            false
        }
    }

    private fun isValidatedDateBirth(value: String?): Boolean {
        return when {
            value?.isEmpty() == true -> {
                dateError.value = dateBirthErrorString
                false
            }
            value?.matches(FieldsRegex.DATE_DD_MM_YYYY.regex.toRegex()) == true -> {
                if (value.isOlderThanEighteen()) {
                    dateError.value = null
                    true
                } else {
                    dateError.value = dateBirthErrorString
                    pushErrorToAnalytics()
                    false
                }
            }
            else -> {
                dateError.value = dateBirthErrorString
                pushErrorToAnalytics()
                false
            }
        }
    }

    private fun pushErrorToAnalytics() {
        analytics.pushSimpleEvent(
            Tags.LOGIN_ERROS,
            Tags.PARAMETER_RESET_PASSWORD to "nascimento_invalido"
        )
    }

    companion object {
        const val MAX_DATE_LENGTH = 10
    }
}