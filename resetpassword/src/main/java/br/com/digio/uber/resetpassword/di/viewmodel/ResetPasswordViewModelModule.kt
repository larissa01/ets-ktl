package br.com.digio.uber.resetpassword.di.viewmodel

import br.com.digio.uber.resetpassword.viewmodel.ResetPasswordBirthdayViewModel
import br.com.digio.uber.resetpassword.viewmodel.ResetPasswordConfirmationViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val resetPasswordViewModelModule = module {
    viewModel { ResetPasswordBirthdayViewModel(get(), get(), get()) }
    viewModel { ResetPasswordConfirmationViewModel(get()) }
}