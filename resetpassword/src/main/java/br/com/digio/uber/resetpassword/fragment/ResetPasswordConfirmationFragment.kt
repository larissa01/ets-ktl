package br.com.digio.uber.resetpassword.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.extensions.stateSetValue
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.resetpassword.BR
import br.com.digio.uber.resetpassword.R
import br.com.digio.uber.resetpassword.databinding.ResetPasswordConfirmationFragmentBinding
import br.com.digio.uber.resetpassword.viewmodel.ResetPasswordConfirmationViewModel
import br.com.digio.uber.util.Const
import org.koin.android.ext.android.inject

class ResetPasswordConfirmationFragment :
    BaseViewModelFragment<ResetPasswordConfirmationFragmentBinding, ResetPasswordConfirmationViewModel>() {

    override val bindingVariable: Int? = BR.resetPasswordConfirmationViewModel

    override val getLayoutId: Int? = R.layout.reset_password_confirmation_fragment

    override val viewModel: ResetPasswordConfirmationViewModel? by inject()

    private val analytics: Analytics by inject()

    private val email: String by lazy {
        ResetPasswordConfirmationFragmentArgs.fromBundle(requireArguments()).email
    }

    override fun initialize() {
        super.initialize()
        viewModel?.initializer {
            when (it.id) {
                R.id.btnConfirmationNext -> backToLoginPassword()
            }
        }
        viewModel?.addEmailToSubtitle(email)
    }

    override fun tag(): String = ResetPasswordConfirmationFragment::class.java.name

    override fun getToolbar(): Toolbar? =
        binding?.includeAppBarResetPassword?.toolbarResetPassword

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE

    override fun onNavigationClick(view: View) {
        backToLoginPassword()
    }

    private fun backToLoginPassword() {
        analytics.pushSimpleEvent(Tags.RESET_COD_SUCCESS)
        pop()
        stateSetValue(Const.RequestOnResult.ResetPassword.RESULT_SUCCESS, true)
    }

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true
}