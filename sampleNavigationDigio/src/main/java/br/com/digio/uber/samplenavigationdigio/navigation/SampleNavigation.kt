package br.com.digio.uber.samplenavigationdigio.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName
import br.com.digio.uber.samplenavigationdigio.fragment.BarFragment
import br.com.digio.uber.samplenavigationdigio.fragment.FooFragment

class SampleNavigation(
    activity: AppCompatActivity
) : GenericNavigationWithFlow(activity) {

    fun init(bundle: Bundle?) {
        goTo(GoTo(SampleOpKey.FOO.name, bundle))
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            SampleOpKey.FOO.name to showFoo(),
            SampleOpKey.BAR.name to showBar()
        )

    private fun showFoo(): GenericNavigationWIthFlowGetFragmentByName = {
        FooFragment.newInstance(this, it)
    }

    private fun showBar(): GenericNavigationWIthFlowGetFragmentByName = {
        BarFragment.newInstance(this, it)
    }
}