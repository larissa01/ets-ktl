package br.com.digio.uber.samplenavigationdigio.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.balancetoolbar.balance.di.balanceModule
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.helper.ViewIds
import br.com.digio.uber.common.navigation.GenericNavigation
import br.com.digio.uber.samplenavigationdigio.R
import br.com.digio.uber.samplenavigationdigio.di.viewModel.sampleNavigationViewModelModule
import br.com.digio.uber.samplenavigationdigio.navigation.SampleNavigation
import com.google.android.material.appbar.CollapsingToolbarLayout
import org.koin.core.module.Module

class SampleNavigationActivity : BaseViewModelActivity<ViewDataBinding, BaseViewModel>() {

    private val sampleNavigation = SampleNavigation(this)
    override val bindingVariable: Int? = null
    override val viewModel: BaseViewModel? = null
    override fun getLayoutId(): Int? = LayoutIds.navigationLayoutWithCollapsingToolbarScrollActivity
    override fun getModule(): List<Module>? = listOf(sampleNavigationViewModelModule, balanceModule)
    override fun getNavigation(): GenericNavigation? = sampleNavigation
    override fun getToolbar(): Toolbar? = findViewById<Toolbar>(ViewIds.toolbarId).apply {
        this@SampleNavigationActivity.findViewById<CollapsingToolbarLayout>(ViewIds.collapsingToolbarId)
            .title = "Navigation example"
        this.navigationIcon = ContextCompat.getDrawable(context, br.com.digio.uber.common.R.drawable.ic_back_arrow)
    }
    override fun showDisplayShowTitle(): Boolean = true

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        sampleNavigation.init(intent.extras)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}