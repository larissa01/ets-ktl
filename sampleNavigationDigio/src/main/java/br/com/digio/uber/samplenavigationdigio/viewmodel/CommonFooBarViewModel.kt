package br.com.digio.uber.samplenavigationdigio.viewmodel

import android.graphics.Color
import android.graphics.RectF
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.samplenavigationdigio.R
import br.com.digio.uber.util.Const
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.random.Random

class CommonFooBarViewModel constructor(
    private val analytics: Analytics
) : BaseViewModel() {

    val vec2 = MutableLiveData<RectF>()
    val randomColor = MutableLiveData<Int>()
    val onFinishedLongProcess: MutableLiveData<Boolean> = MutableLiveData()

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer {
            when (it.id) {
                R.id.btnGoToFoo -> {
                    analytics.pushSimpleEvent("foo")
                    analytics.pushSimpleEvent("foo", "foo" to "foo")
                    executeLongProcess()
                }
                R.id.btnGoToBar -> {
                    analytics.pushSimpleEvent("bar")
                    analytics.pushSimpleEvent("bar", "bar" to "bar")
                    executeLongProcess()
                }
                else -> onClickItem(it)
            }
        }
    }

    private fun executeLongProcess() {
        launch {
            showLoading.value = true
            val returnValue = withContext(Dispatchers.IO) {
                delay(Const.Utils.DELAY_MILLISECOND)
                true
            }

            showLoading.value = false
            onFinishedLongProcess.value = returnValue
        }
    }

    fun randomVec2(widthLimit: Float, heightLimit: Float, fixedWidth: Float, fixedHeight: Float) {
        val positionX = Random.nextFloat() * widthLimit - fixedWidth
        val positionY = Random.nextFloat() * heightLimit - fixedHeight

        vec2.value =
            RectF(
                positionX,
                positionY,
                fixedWidth + positionX,
                fixedHeight + positionY
            )

        randomColor.value = Color.argb(
            ALPHA,
            Random.nextInt(RGB),
            Random.nextInt(RGB),
            Random.nextInt(RGB)
        )
    }

    companion object {
        private const val ALPHA: Int = 255
        private const val RGB: Int = 256
    }
}