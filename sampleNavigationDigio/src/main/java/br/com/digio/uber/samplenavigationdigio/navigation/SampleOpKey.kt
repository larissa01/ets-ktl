package br.com.digio.uber.samplenavigationdigio.navigation

import br.com.digio.uber.common.kenum.StatusBarColor

enum class SampleOpKey(val theme: StatusBarColor) {
    FOO(StatusBarColor.WHITE),
    BAR(StatusBarColor.BLACK)
}