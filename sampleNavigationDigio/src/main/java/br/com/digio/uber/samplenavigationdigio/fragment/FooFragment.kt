package br.com.digio.uber.samplenavigationdigio.fragment

import android.content.res.Resources
import androidx.lifecycle.Observer
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.samplenavigationdigio.BR
import br.com.digio.uber.samplenavigationdigio.R
import br.com.digio.uber.samplenavigationdigio.databinding.LayoutFooFragmentBinding
import br.com.digio.uber.samplenavigationdigio.navigation.SampleOpKey
import br.com.digio.uber.samplenavigationdigio.viewmodel.CommonFooBarViewModel
import org.koin.android.ext.android.inject

class FooFragment : BNWFViewModelFragment<LayoutFooFragmentBinding, CommonFooBarViewModel>() {

    override val bindingVariable: Int? = BR.fooViewModel
    override val getLayoutId: Int? = R.layout.layout_foo_fragment
    override val viewModel: CommonFooBarViewModel by inject()
    override fun tag(): String = SampleOpKey.FOO.name
    override fun getStatusBarAppearance(): StatusBarColor = SampleOpKey.FOO.theme

    override fun initialize() {
        super.initialize()
        setupViewModel()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.onFinishedLongProcess.observe(
            this,
            Observer {
                if (it == true) {
                    callGoTo(SampleOpKey.BAR.name)
                }
            }
        )
    }

    private fun setupViewModel() {
        viewModel.initializer {}
        val resources = Resources.getSystem()
        val width = resources.displayMetrics.widthPixels
        val height = resources.displayMetrics.heightPixels
        viewModel.randomVec2(
            width.toFloat(),
            height.toFloat(),
            binding?.btnGoToBar?.minWidth?.toFloat() ?: 0f,
            binding?.btnGoToBar?.minWidth?.toFloat() ?: 0f,
        )
    }

    companion object : FragmentNewInstance<FooFragment>(::FooFragment)
}