package br.com.digio.uber.samplenavigationdigio.di.viewModel

import br.com.digio.uber.samplenavigationdigio.viewmodel.CommonFooBarViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val sampleNavigationViewModelModule = module {
    viewModel { CommonFooBarViewModel(get()) }
}