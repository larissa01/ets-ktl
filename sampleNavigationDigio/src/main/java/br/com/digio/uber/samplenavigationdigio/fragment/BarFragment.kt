package br.com.digio.uber.samplenavigationdigio.fragment

import android.content.res.Resources
import androidx.lifecycle.Observer
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.samplenavigationdigio.BR
import br.com.digio.uber.samplenavigationdigio.R
import br.com.digio.uber.samplenavigationdigio.databinding.LayoutBarFragmentBinding
import br.com.digio.uber.samplenavigationdigio.navigation.SampleOpKey
import br.com.digio.uber.samplenavigationdigio.viewmodel.CommonFooBarViewModel
import org.koin.android.ext.android.inject

class BarFragment : BNWFViewModelFragment<LayoutBarFragmentBinding, CommonFooBarViewModel>() {

    override val bindingVariable: Int = BR.barViewModel
    override val getLayoutId: Int = R.layout.layout_bar_fragment
    override val viewModel: CommonFooBarViewModel by inject()
    override fun tag(): String = SampleOpKey.BAR.name
    override fun getStatusBarAppearance(): StatusBarColor = SampleOpKey.BAR.theme

    override fun initialize() {
        super.initialize()
        setupViewModel()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.onFinishedLongProcess.observe(
            this,
            Observer {
                if (it == true) {
                    callGoTo(SampleOpKey.FOO.name)
                }
            }
        )
    }

    private fun setupViewModel() {
        viewModel.initializer {}
        val resources = Resources.getSystem()
        val width = resources.displayMetrics.widthPixels
        val height = resources.displayMetrics.heightPixels
        viewModel.randomVec2(
            width.toFloat(),
            height.toFloat(),
            binding?.btnGoToFoo?.minWidth?.toFloat() ?: 0f,
            binding?.btnGoToFoo?.minWidth?.toFloat() ?: 0f,
        )
    }

    companion object : FragmentNewInstance<BarFragment>(::BarFragment)
}