package br.com.digio.uber.withdraw.di

import br.com.digio.uber.withdraw.viewmodel.WithdrawFragmentViewModel
import br.com.digio.uber.withdraw.viewmodel.WithdrawNoNotesFragmentViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val withdrawViewModelModule = module {
    viewModel { WithdrawFragmentViewModel(get(), get(), get(), get(), get(), get()) }
    viewModel { WithdrawNoNotesFragmentViewModel(get(), get()) }
}