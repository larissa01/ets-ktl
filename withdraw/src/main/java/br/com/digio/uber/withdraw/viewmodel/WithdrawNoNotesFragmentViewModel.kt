package br.com.digio.uber.withdraw.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags.WITHDRAW_ERROR
import br.com.digio.uber.analytics.utils.Tags.WITHDRAW_NOT_DEBITED
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.makeChooseMessageObject
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.withdraw.DoWithdrawUseCase
import br.com.digio.uber.model.withdraw.WithdrawReq
import br.com.digio.uber.util.safeLet
import br.com.digio.uber.util.toCurrency
import br.com.digio.uber.withdraw.R
import br.com.digio.uber.withdraw.ui.WithdrawNoNotesFragment.Companion.FIRST_POSITION
import br.com.digio.uber.withdraw.ui.WithdrawNoNotesFragment.Companion.FOURTH_POSITION
import br.com.digio.uber.withdraw.ui.WithdrawNoNotesFragment.Companion.SECOND_POSITION
import br.com.digio.uber.withdraw.ui.WithdrawNoNotesFragment.Companion.THIRD_POSITION

class WithdrawNoNotesFragmentViewModel(
    private val doWithdrawUseCase: DoWithdrawUseCase,
    private val analytics: Analytics
) : BaseViewModel() {

    private var possibleAmounts: ArrayList<Int>? = null

    val firstValue = MutableLiveData<String>()
    val secondValue = MutableLiveData<String>()
    val thirdValue = MutableLiveData<String>()
    val fourthValue = MutableLiveData<String>()

    val showFirst = MutableLiveData<Boolean>()
    val showSecond = MutableLiveData<Boolean>()
    val showThird = MutableLiveData<Boolean>()
    val showFourth = MutableLiveData<Boolean>()

    var goToSuccess = MutableLiveData<Boolean>()
    var shouldPop = MutableLiveData<Boolean>()

    var qrCode: String? = null
    var hashPid: String? = null
    var latitude: Double? = null
    var longitude: Double? = null

    fun initializer(possibleAmounts: ArrayList<Int>?, onClickItem: OnClickItem) {
        super.initializer(onClickItem)

        this.possibleAmounts = possibleAmounts

        firstValue.value = possibleAmounts?.getOrNull(FIRST_POSITION)?.toDouble()?.toCurrency()
        secondValue.value = possibleAmounts?.getOrNull(SECOND_POSITION)?.toDouble()?.toCurrency()
        thirdValue.value = possibleAmounts?.getOrNull(THIRD_POSITION)?.toDouble()?.toCurrency()
        fourthValue.value = possibleAmounts?.getOrNull(FOURTH_POSITION)?.toDouble()?.toCurrency()

        showFirst.value = possibleAmounts?.getOrNull(FIRST_POSITION) != null
        showSecond.value = possibleAmounts?.getOrNull(SECOND_POSITION) != null
        showThird.value = possibleAmounts?.getOrNull(THIRD_POSITION) != null
        showFourth.value = possibleAmounts?.getOrNull(FOURTH_POSITION) != null
    }

    fun doWithdraw(amountPosition: Int) {
        val selectedAmount = possibleAmounts?.get(amountPosition)
        safeLet(selectedAmount, qrCode, hashPid, latitude, longitude) {
            selectedAmountLet, qrCodeLet, hashPidLet, latitudeLet, longitudeLet ->
            doWithdrawUseCase(WithdrawReq(qrCodeLet, selectedAmountLet, latitudeLet, longitudeLet, hashPidLet))
                .singleExec(
                    onSuccessBaseViewModel = {
                        goToSuccess.value = true
                    },
                    onError = { _, error, trowable ->
                        analytics.pushSimpleEvent(WITHDRAW_ERROR, WITHDRAW_ERROR to WITHDRAW_NOT_DEBITED)
                        makeChooseMessageObject {
                            title = br.com.digio.uber.common.R.string.ops
                            message = R.string.withdraw_error
                            positiveOptionTextInt = R.string.try_again
                            negativeOptionTextInt = R.string.go_back
                            onPositiveButtonClick = { shouldPop.value = true }
                            onNegativeButtonClick = { shouldPop.value = false }
                        }
                    }
                )
        }
    }
}