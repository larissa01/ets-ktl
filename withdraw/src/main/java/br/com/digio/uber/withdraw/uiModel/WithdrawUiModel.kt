package br.com.digio.uber.withdraw.uiModel

import androidx.lifecycle.MutableLiveData

data class WithdrawUiModel(
    var lastingDailyWithdraws : String,
    var lastingMonthlyWithdraws : String,
    var maxWithdrawValue : Double,
    var maxWithdrawValueText : String,
    var minWithdrawValue : Int,
    val maxDailyWithdraws : String,
    val maxMonthWithdraws : String,
    var overlimitTaxValue : Double?,
    var overlimitTaxText : String,
    var showOverlimitText : Boolean
)