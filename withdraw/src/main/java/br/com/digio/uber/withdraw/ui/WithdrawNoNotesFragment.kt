package br.com.digio.uber.withdraw.ui

import android.view.View
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.withdraw.BR
import br.com.digio.uber.withdraw.R
import br.com.digio.uber.withdraw.databinding.FragmentNoNotesBinding
import br.com.digio.uber.withdraw.viewmodel.WithdrawNoNotesFragmentViewModel
import org.koin.android.ext.android.inject

class WithdrawNoNotesFragment : BaseViewModelFragment<FragmentNoNotesBinding, WithdrawNoNotesFragmentViewModel>() {

    override val bindingVariable: Int = BR.noNodesViewModel
    override val getLayoutId: Int = R.layout.fragment_no_notes
    override val viewModel: WithdrawNoNotesFragmentViewModel by inject()
    override fun tag(): String = WithdrawNoNotesFragment::javaClass.name
    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    companion object {
        const val KEY_POSSIBLE_AMOUNTS = "KEY_POSSIBLE_AMOUNTS"
        const val KEY_QR_CODE = "KEY_QR_CODE"
        const val KEY_HASH_PID_WITHDRAW = "HASH_PID_WITHDRAW"
        const val KEY_LATITUDE = "KEY_LATITUDE"
        const val KEY_LONGITUDE = "KEY_LONGITUDE"
        const val FIRST_POSITION = 0
        const val SECOND_POSITION = 1
        const val THIRD_POSITION = 2
        const val FOURTH_POSITION = 3
    }

    override fun initialize() {
        super.initialize()
        viewModel.qrCode = arguments?.getString(KEY_QR_CODE)
        viewModel.hashPid = arguments?.getString(KEY_HASH_PID_WITHDRAW)
        viewModel.latitude = arguments?.getDouble(KEY_LATITUDE)
        viewModel.longitude = arguments?.getDouble(KEY_LONGITUDE)
        arguments?.getIntegerArrayList(KEY_POSSIBLE_AMOUNTS).let { possibleAmountsLet ->
            viewModel.initializer(possibleAmountsLet) { onItemClicked(it) }
        }
        viewModel.goToSuccess.observe(this) {
            navigateTo(WithdrawNoNotesFragmentDirections.actionWithdrawNoNotesFragmentToCommonSuccessFragment())
        }
        viewModel.shouldPop.observe(this) {
            if (it == true) pop()
            else activity?.finish()
        }
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.button_first_choice -> viewModel.doWithdraw(FIRST_POSITION)
            R.id.button_second_choice -> viewModel.doWithdraw(SECOND_POSITION)
            R.id.button_third_choice -> viewModel.doWithdraw(THIRD_POSITION)
            R.id.button_fourth_choice -> viewModel.doWithdraw(FOURTH_POSITION)
            R.id.btn_go_back -> pop()
        }
    }
}