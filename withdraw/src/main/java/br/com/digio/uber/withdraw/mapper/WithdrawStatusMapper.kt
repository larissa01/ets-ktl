package br.com.digio.uber.withdraw.mapper

import br.com.digio.uber.common.extensions.toMoney
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.model.bankSlip.CustomerLimitsResponse
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.util.toMoney
import br.com.digio.uber.withdraw.R
import br.com.digio.uber.withdraw.uiModel.WithdrawUiModel

class WithdrawStatusMapper(private val resourceManager: ResourceManager) :
    AbstractMapper<CustomerLimitsResponse, WithdrawUiModel> {

    override fun map(param: CustomerLimitsResponse): WithdrawUiModel =
        WithdrawUiModel(
            maxDailyWithdraws = param.generalLimits.maxQtdPerDay.toString(),
            maxMonthWithdraws = param.generalLimits.maxQtdPerMonth.toString(),
            lastingDailyWithdraws = param.userLimits.maxQtdForToday.toString(),
            lastingMonthlyWithdraws = param.userLimits.maxQtdForMonth.toString(),
            maxWithdrawValue = param.generalLimits.maxAmount.toDouble(),
            minWithdrawValue = param.generalLimits.minAmount.toInt(),
            maxWithdrawValueText = resourceManager.getString(R.string.the_maximum_value_for_withdraw_is) +
                " ${param.generalLimits.maxAmount.toMoney(
                    true
                )}",
            overlimitTaxValue = param.overlimitTax?.toDouble(),
            overlimitTaxText = treatOverlimitText(param.mustChargeOverlimitTax, param.overlimitTax?.toMoney(true)),
            showOverlimitText = param.mustChargeOverlimitTax
        )

    private fun treatOverlimitText(mustChargeOverLimitTax: Boolean, taxValueString: String?): String {
        var finalTaxValueString = taxValueString
        if (!mustChargeOverLimitTax) {
            finalTaxValueString = resourceManager.getString(R.string.value_free_withdraw)
        }
        return "${resourceManager.getString(R.string.tax_value)} $finalTaxValueString"
    }
}