package br.com.digio.uber.withdraw.ui

import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import br.com.digio.uber.common.balancetoolbar.balance.di.balanceModule
import br.com.digio.uber.common.base.activity.BaseJetNavigationActivity
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.helper.ViewIds
import br.com.digio.uber.withdraw.R
import br.com.digio.uber.withdraw.di.withdrawViewModelModule
import org.koin.core.module.Module

class WithdrawActivity : BaseJetNavigationActivity() {

    override fun getLayoutId(): Int? = LayoutIds.navigationLayoutWithToolbarActivity
    override fun navGraphId(): Int? = R.navigation.nav_withdraw
    override fun getModule(): List<Module>? = listOf(withdrawViewModelModule, balanceModule)
    override fun getToolbar(): Toolbar? = findViewById<Toolbar>(ViewIds.toolbarId).apply {
        this.title = getString(R.string.withdraw_title)
        this.navigationIcon = ContextCompat.getDrawable(context, br.com.digio.uber.common.R.drawable.ic_back_arrow)
    }

    override fun showDisplayShowTitle(): Boolean = true

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onBackPressed() {
        finish()
    }
}