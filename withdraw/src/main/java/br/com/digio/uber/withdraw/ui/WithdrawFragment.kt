package br.com.digio.uber.withdraw.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.component.CommonSuccessFragment.Companion.COMMON_SUCCESS_FRAGMENT_BTN_TEXT
import br.com.digio.uber.common.component.CommonSuccessFragment.Companion.COMMON_SUCCESS_FRAGMENT_MESSAGE
import br.com.digio.uber.common.component.CommonSuccessFragment.Companion.COMMON_SUCCESS_FRAGMENT_SHOULD_POP
import br.com.digio.uber.common.component.CommonSuccessFragment.Companion.COMMON_SUCCESS_FRAGMENT_TITLE
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.Extras.REQUEST_PERMISSION_CODE
import br.com.digio.uber.util.Const.RequestOnResult.PID.KEY_PID_HASH
import br.com.digio.uber.util.Const.RequestOnResult.PID.PID_REQUEST_CODE
import br.com.digio.uber.util.Const.RequestOnResult.PID.RESULT_PID_SUCCESS
import br.com.digio.uber.util.Const.RequestOnResult.QRCode.QR_REQUEST_CODE
import br.com.digio.uber.withdraw.BR
import br.com.digio.uber.withdraw.R
import br.com.digio.uber.withdraw.databinding.FragmentWithdrawBinding
import br.com.digio.uber.withdraw.viewmodel.WithdrawFragmentViewModel
import kotlinx.android.synthetic.main.fragment_withdraw.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.android.inject
import org.koin.core.component.KoinApiExtension

@ExperimentalCoroutinesApi
@KoinApiExtension
class WithdrawFragment : BaseViewModelFragment<FragmentWithdrawBinding, WithdrawFragmentViewModel>() {

    override val bindingVariable: Int = BR.withdrawViewModel
    override val getLayoutId: Int = R.layout.fragment_withdraw
    override val viewModel: WithdrawFragmentViewModel by inject()
    override fun tag(): String = WithdrawFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() {
        super.initialize()
        requestPermission()

        balance_toolbar_withdraw.getBalanceLiveData()?.observe(
            this,
            { balanceUiModel ->
                viewModel.initializer(balanceUiModel) { onClickItem(it) }
            }
        )
        viewModel.isGpsEnabledObservable.observe(
            this,
            {
                if (it) {
                    navigation.navigationToQrCodeReaderActivity(
                        this,
                        getString(R.string.qr_messge_from_withdraw),
                        QR_REQUEST_CODE
                    )
                    viewModel.isGpsEnabledObservable.value = false
                }
            }
        )
        viewModel.minWithdrawBalanceValueObservable.observe(
            this,
            {
                showInformativeMessageAndFinishActivity(getString(R.string.insufficient_min_withdraw_message))
            }
        )
        viewModel.withdrawQuantityObservable.observe(
            this,
            {
                if (it.isNotBlank()) {
                    showInformativeMessageAndFinishActivity(it)
                }
            }
        )
        viewModel.navigateToNoNotesScreenBundle.observe(
            this,
            {
                navigateTo(WithdrawFragmentDirections.actionWithdrawFragmentToWithdrawNoNotesFragment(), it)
            }
        )
        viewModel.navigateToSuccessScreen.observe(
            this,
            {
                if (it == true) {
                    putBundleAndNavigateToSuccess()
                }
            }
        )
    }

    private fun putBundleAndNavigateToSuccess() {
        val bundleSuccess = Bundle()
        bundleSuccess.putString(
            COMMON_SUCCESS_FRAGMENT_TITLE,
            getString(br.com.digio.uber.common.R.string.success)
        )
        bundleSuccess.putString(
            COMMON_SUCCESS_FRAGMENT_MESSAGE,
            getString(R.string.withdraw_effected)
        )
        bundleSuccess.putString(
            COMMON_SUCCESS_FRAGMENT_BTN_TEXT,
            getString(br.com.digio.uber.common.R.string.go_to_begin)
        )
        bundleSuccess.putBoolean(COMMON_SUCCESS_FRAGMENT_SHOULD_POP, false)
        navigateTo(
            WithdrawFragmentDirections.actionWithdrawFragmentToCommonSuccessFragment(),
            bundleSuccess
        )
        viewModel.navigateToSuccessScreen.value = false
    }

    private fun requestPermission() {
        activity?.let { activityLet ->
            val havePermission = checkHavePermission(activityLet)
            if (!havePermission) {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ),
                    REQUEST_PERMISSION_CODE
                )
                return
            }
        }
    }

    private fun checkHavePermission(it: FragmentActivity): Boolean =
        ContextCompat.checkSelfPermission(
            it,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            it,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            it,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            it,
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            it,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

    override fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_CODE && grantResults.isNotEmpty()) {
            if (!grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                showInformativeMessageAndFinishActivity(getString(R.string.permission_denied_message))
            }
        }
    }

    private fun showInformativeMessageAndFinishActivity(message: String) {
        showMessage(
            ErrorObject().apply {
                title = br.com.digio.uber.common.R.string.ops
                messageString = message
            },
            false
        ) {
            activity?.finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == QR_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.getStringExtra(Const.Extras.RESULT_QR_CODE_VALUE)?.let { qrCodeValueLet ->
                viewModel.qrCode = qrCodeValueLet
                navigation.navigationToPidActivity(this, PidType.WITHDRAW, PID_REQUEST_CODE)
            }
        }
        if (requestCode == PID_REQUEST_CODE && resultCode == RESULT_PID_SUCCESS) {
            data?.getStringExtra(KEY_PID_HASH)?.let { hashPidLet ->
                viewModel.getUserLocation(hashPidLet)
            }
        }
    }

    private fun onClickItem(v: View) {
        when (v.id) {
            R.id.button_continue -> {
                viewModel.checkIsLocationEnabled()
            }
        }
    }
}