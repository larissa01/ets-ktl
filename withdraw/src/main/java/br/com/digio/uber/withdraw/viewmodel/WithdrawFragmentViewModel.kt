package br.com.digio.uber.withdraw.viewmodel

import android.location.Location
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags.WITHDRAW_ERROR
import br.com.digio.uber.analytics.utils.Tags.WITHDRAW_ERROR_HAS_NOT
import br.com.digio.uber.analytics.utils.Tags.WITHDRAW_SUCCESS
import br.com.digio.uber.analytics.utils.Tags.WITHDRAW_VALUE
import br.com.digio.uber.common.balancetoolbar.balance.uiModel.BalanceUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.common.generics.MessageGenericObject
import br.com.digio.uber.common.generics.makeErrorObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.exception.withdraw.NoNotesException
import br.com.digio.uber.domain.usecase.location.GetLastKnowLocationUseCase
import br.com.digio.uber.domain.usecase.location.IsLocationEnabledUseCase
import br.com.digio.uber.domain.usecase.withdraw.DoWithdrawUseCase
import br.com.digio.uber.domain.usecase.withdraw.GetWithdrawStatusUseCase
import br.com.digio.uber.model.withdraw.WithdrawReq
import br.com.digio.uber.util.Const.Utils.INITIAL_WITHDRAW_VALUE
import br.com.digio.uber.util.Const.Utils.MAX_WITHDRAW_VALUE
import br.com.digio.uber.util.Const.Utils.MIDDLE_WITHDRAW_VALUE
import br.com.digio.uber.util.Const.Utils.MIN_WITHDRAW_VALUE
import br.com.digio.uber.util.safeLet
import br.com.digio.uber.util.toCurrencyNumber
import br.com.digio.uber.util.toCurrencyValue
import br.com.digio.uber.withdraw.R
import br.com.digio.uber.withdraw.mapper.WithdrawStatusMapper
import br.com.digio.uber.withdraw.ui.WithdrawNoNotesFragment.Companion.KEY_HASH_PID_WITHDRAW
import br.com.digio.uber.withdraw.ui.WithdrawNoNotesFragment.Companion.KEY_LATITUDE
import br.com.digio.uber.withdraw.ui.WithdrawNoNotesFragment.Companion.KEY_LONGITUDE
import br.com.digio.uber.withdraw.ui.WithdrawNoNotesFragment.Companion.KEY_POSSIBLE_AMOUNTS
import br.com.digio.uber.withdraw.ui.WithdrawNoNotesFragment.Companion.KEY_QR_CODE
import br.com.digio.uber.withdraw.uiModel.WithdrawUiModel

class WithdrawFragmentViewModel(
    private val getWithdrawStatusUseCase: GetWithdrawStatusUseCase,
    private val isLocationEnabledUseCase: IsLocationEnabledUseCase,
    private val getLastKnowLocationUseCase: GetLastKnowLocationUseCase,
    private val doWithdrawUseCase: DoWithdrawUseCase,
    private val resourceManager: ResourceManager,
    private val analytics: Analytics
) : BaseViewModel() {

    var qrCode: String? = null
    var hashPid: String? = null
    private val currentBalance = MutableLiveData<BalanceUiModel>()
    val withdrawUiModel = MutableLiveData<WithdrawUiModel>()
    val enableContinue = MutableLiveData<Boolean>()
    val isGpsEnabledObservable = MutableLiveData<Boolean>()
    val minWithdrawBalanceValueObservable = MutableLiveData<Boolean>()
    val withdrawQuantityObservable = MutableLiveData<String>()
    val activateFirstButtonMutableLiveData = MutableLiveData<Boolean>()
    val activateSecondButtonMutableLiveData = MutableLiveData<Boolean>()
    val activateThirdButtonMutableLiveData = MutableLiveData<Boolean>()
    val navigateToSuccessScreen = MutableLiveData<Boolean>()
    val navigateToNoNotesScreenBundle = MutableLiveData<Bundle>()
    val currentWithdrawValue: MutableLiveData<String> = MutableLiveData<String>().apply {
        value = INITIAL_WITHDRAW_VALUE.toCurrencyNumber()
    }

    fun initializer(balanceUiModel: BalanceUiModel, onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        if (balanceUiModel.balance.toCurrencyValue() < MIN_WITHDRAW_VALUE) {
            minWithdrawBalanceValueObservable.value = true
        } else {
            currentBalance.value = balanceUiModel
            getWithdrawStatusUseCase(Unit).singleExec(
                onSuccessBaseViewModel = {
                    if (it.userLimits.maxQtdForToday == 0) {
                        withdrawQuantityObservable.value =
                            resourceManager.getString(R.string.daily_withdraw_limit_reached)
                    }
                    if (it.userLimits.maxQtdForMonth == 0) {
                        withdrawQuantityObservable.value =
                            resourceManager.getString(R.string.monthly_withdraw_limit_reached)
                    }

                    withdrawUiModel.value = WithdrawStatusMapper(resourceManager).map(it)
                    checkAvailableValues()
                }
            )
        }
    }

    fun increaseValue(plusValue: Int) {
        val currentValue = (currentWithdrawValue.value.toCurrencyValue())
        currentWithdrawValue.value = (currentValue + plusValue).toCurrencyNumber()
        checkAvailableValues()
    }

    fun clearWithdrawValue() {
        currentWithdrawValue.value = INITIAL_WITHDRAW_VALUE.toCurrencyNumber()
        checkAvailableValues()
    }

    private fun checkAvailableValues() {
        safeLet(withdrawUiModel.value, currentBalance.value) { uiModelLet, balanceLet ->
            val withdrawValue = currentWithdrawValue.value.toCurrencyValue()
            var availableValueToWithdraw: Double = balanceLet.balance.toCurrencyValue()
            if (availableValueToWithdraw >= uiModelLet.maxWithdrawValue) {
                availableValueToWithdraw = uiModelLet.maxWithdrawValue
            }

            if (uiModelLet.showOverlimitText) {
                uiModelLet.overlimitTaxValue?.let { overLimitTaxValueLet ->
                    availableValueToWithdraw -= overLimitTaxValueLet
                }
            }

            activateFirstButtonMutableLiveData.value =
                availableValueToWithdraw - withdrawValue >= MIN_WITHDRAW_VALUE

            activateSecondButtonMutableLiveData.value =
                availableValueToWithdraw - withdrawValue >= MIDDLE_WITHDRAW_VALUE

            activateThirdButtonMutableLiveData.value =
                availableValueToWithdraw - withdrawValue >= MAX_WITHDRAW_VALUE

            if (balanceLet.toString().toCurrencyValue() < uiModelLet.minWithdrawValue) {
                buildErrorDialog(resourceManager.getString(R.string.minimum_balance))
            }
            enableContinue.value = withdrawValue != INITIAL_WITHDRAW_VALUE
        }
    }

    private fun buildErrorDialog(currentMessage: String) {
        message.value = makeErrorObject {
            messageString = currentMessage
        }
    }

    fun checkIsLocationEnabled() {
        isLocationEnabledUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                if (it) {
                    analytics.pushSimpleEvent(WITHDRAW_VALUE)
                    isGpsEnabledObservable.value = it
                } else {
                    message.value = makeErrorObject {
                        title = R.string.gps_is_disabled_title
                        message = R.string.gps_is_disabled_message
                    }
                }
            }
        )
    }

    fun getUserLocation(qrCodeValue: String) {
        hashPid = qrCodeValue
        getLastKnowLocationUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                doWithdraw(it)
            },
            onError = { _, error, _ ->
                error.apply {
                    message = R.string.gps_is_disabled_message
                }
            },
            showLoadingFlag = false
        )
    }

    private fun doWithdraw(location: Location) {
        safeLet(qrCode, hashPid) { qrCodeLet, hashPidLet ->
            doWithdrawUseCase(
                WithdrawReq(
                    qrCodeLet,
                    currentWithdrawValue.value.toCurrencyValue().toInt(),
                    location.latitude,
                    location.longitude,
                    hashPidLet
                )
            ).singleExec(
                onSuccessBaseViewModel = {
                    analytics.pushSimpleEvent(WITHDRAW_SUCCESS)
                    navigateToSuccessScreen.value = true
                },
                onError = { _, error, throwable ->
                    treatWithdrawError(throwable, error, qrCodeLet, hashPidLet, location)
                }
            )
        }
    }

    private fun treatWithdrawError(
        throwable: Throwable?,
        error: MessageGenericObject,
        qrCodeValue: String,
        hashPidLet: String,
        location: Location
    ): MessageGenericObject {
        return if (throwable is NoNotesException) {
            analytics.pushSimpleEvent(WITHDRAW_ERROR, WITHDRAW_ERROR to WITHDRAW_ERROR_HAS_NOT)
            val possibleAmounts = throwable.customWithdrawError.possibleAmounts
            if (possibleAmounts != null) {
                val bundle = Bundle()
                bundle.putIntegerArrayList(KEY_POSSIBLE_AMOUNTS, ArrayList(possibleAmounts))
                bundle.putString(KEY_QR_CODE, qrCodeValue)
                bundle.putString(KEY_HASH_PID_WITHDRAW, hashPidLet)
                bundle.putDouble(KEY_LATITUDE, location.latitude)
                bundle.putDouble(KEY_LONGITUDE, location.longitude)
                navigateToNoNotesScreenBundle.value = bundle
                EmptyErrorObject
            } else {
                error
            }
        } else {
            error
        }
    }
}