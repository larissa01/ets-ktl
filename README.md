# Uber Conta

## Code-Style  

Após fazer o clone do projeto, será necessário configurar o codestyle do **[ktlint](https://github.com/pinterest/ktlint)** na sua máquina usando esta task:
`./gradlew ktlintApplyToIdea` 

> **Nota:** Confira se você está utilizando o code-style do projeto em:
> **Settings** -> **Editor** -> **Code Style** -> **Scheme** -> Selecionar **Project**

## Lint
  
O projeto utiliza as ferramentas de lint **[ktlint](https://github.com/pinterest/ktlint)** e **[detekt](https://github.com/detekt/detekt)** para fazer a análise estática de código. Isso já está configurado no CI ao abrir novas PRs, visando melhorar o codebase do projeto.

Basicamente, se você ainda não tem familiaridade com as ferramentas, são estas as tasks que você vai precisar inicialmente:

- Fazer a análise do código com ktlint :
`./gradlew ktlintCheck`
  
- Corrigir erros identificados pelo ktlint:
`./gradlew ktlintFormat`
> **Nota:** nem sempre esta task consegue resolver todos os problemas encontrados. Para algumas regras, o dev precisará fazer a correcão manualmente.
  
- Fazer a análise do código com detekt:
`./gradlew detekt`

### Como identificar erros?

Após rodar a task, tanto o ktlint quanto o detekt, disponibilizam os erros no terminal, indicando a regra infringida e a linha de código dentro da classe, e também na pasta **build/reports** do projeto ou do módulo com problemas. Caso o arquivo de report esteja vazio, significa que está tudo certo!

### Como corrigir os erros?

A maior parte dos erros relacionados à identacão de código, a task **ktlintFormat** já vai resolver para você.
Agora para erros encontrados, principalmente do detekt, cabe ao desenvolvedor resolver:

- Se não conhece a regra ainda, da uma pesquisada no Google com o nome dela e vai encontrar solucões compatíveis

- Use `@Suppress("NomeDaRegra")` **MAS** use com **cautela**. Apenas em casos onde você tem certeza de que aquele código está legível ou faz sentido para os outros devs também

- As regras não estão escritas em pedras, caso você se depare com alguma que não concorda, ou não se aplica para o nosso projeto, por favor, traga para discussão com os **Chapters** e tomem uma decisão em conjunto :)

## Como gerar um APK da aplicação?

Para selecionar a BuildVariant a ser gerada, basta ir no arquivo gradle.properties e descomentar a buildVariant (importante deixar só uma build descomentada).
Logo depois do sync do gradle (obrigatório), é importante fazer o clean do projeto, para isso basta ir no menu superior do android studio, e na opção `build` tem a opção de `clean project`. Depois do Clean, agora sim podemos gerar nossa versão.

### App Bundle
Nós estamos trabalhando com App Bundle, isso significa que a maneira de gerar um apk vai ser diferente, por conta da caracteristica de build do App Bundle.
Por conta disso nós vamos precisar ir na opção de build no Android Studio e lá ir em `Build Bundle(s) / APK(s)` e em seguida `Build Bundle(s)`.
Com isso ele vai gerar um `.aab`. Não pode ser feito upload do `.aap` no Firebase App Distribution e nem podemos instala-lo diretamente no device, como fazemos normalmente com o `.apk` para isso nós devemos extrair o apk contido no bundle.

### Bundletool

Para isso nós devemos instalar a ferramenta `bundletool` ele consegue extrair qualquer caracteristica de APK que precisamos a partir de um bundle. Com o comando `bundletool build-apks --bundle=app-debug.aab --output=app-debug.apks --mode=universal` nós conseguimos gerar uma lista de apks que será incluido no arquivo `app-debug.apks` do `--output`. Uma coisa a se notar é o `--mode=universal` ele serve para indicar ao bundletool que nós queremos um apk com todos os dynamics libraries que temos.

Logo após esse comando nós devemos fazer um unzip do `.apks` gerado, para isso, no mac podemos usar o comando: `unzip app-homol.apks`.

Com isso nós teremos um apk chamado `universal.apk`, esse APK é o que deve ser passado para os QAs ou para nós testarmos nos nossos devices.


## Git Hook

Para facilitar o uso das ferramentas de lint, criamos um git-hook para facilitar a vida e lembrar os devs caso esquecam de executar as ferramentas de lint.
Esse arquivo de pre-commit fará com que ao realizar um commit, ele execute as tasks `ktlintFormat` e `detekt` antes do commit. Em caso de problemas, seu commit falhará e será necessário fazer as devidas correcões.
Para configurar, execute este comando no terminal (na pasta do projeto):
`yes | cp pre-commit ./.git/hooks/pre-commit`

## Submodules

Dentro do projeto estamos trabalhando também com submodules.

Para atualizar os submodules do projeto, siga as orientações abaixo.

Abre o arquivo na raiz do projeto .gitmodules altere a URL para o seu usuário
No terminal, raiz do projeto, utilize os comandosgit
- git submodule init
- git submodule update
- git submodule sync --recursive

Finalize com
- Build > Clean
- File > Invalidade Caches / Restart