package br.com.digio.uber.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import br.com.digio.uber.db.tables.Credentials

@Dao
interface CredentialsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(credentials: Credentials)

    @Query("SELECT * FROM credentials_table ORDER BY cpfCrypted ASC LIMIT 1")
    fun getCredentials(): Credentials?

    @Update
    fun updateCredentials(account: Credentials)

    @Delete
    fun delete(account: Credentials)

    @Query("DELETE FROM credentials_table")
    suspend fun nukedTable()
}