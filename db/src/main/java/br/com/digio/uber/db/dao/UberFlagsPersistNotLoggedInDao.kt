package br.com.digio.uber.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.digio.uber.db.tables.UberFlagsPersistNotLoggedInTable

@Dao
interface UberFlagsPersistNotLoggedInDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(uberFlagsPersistNotLoggedInTable: UberFlagsPersistNotLoggedInTable)

    @Query("SELECT * FROM uber_flags_table ORDER BY id ASC LIMIT 1")
    suspend fun getUberFlags(): UberFlagsPersistNotLoggedInTable?

    @Query("SELECT * FROM uber_flags_table ORDER BY id ASC LIMIT 1")
    fun getUberFlagsLiveData(): LiveData<UberFlagsPersistNotLoggedInTable>
}