package br.com.digio.uber.db.tables

import androidx.room.Embedded
import androidx.room.Relation

data class FaqSubjectWithQuestions(

    @Embedded
    val subject: FaqSubject,

    @Relation(
        parentColumn = "subjectId",
        entityColumn = "subjectId"
    )
    val questions: List<FaqQuestion>
)