package br.com.digio.uber.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import br.com.digio.uber.db.tables.CardTable
import br.com.digio.uber.model.card.TypeCard

@Dao
interface CardDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(card: CardTable)

    @Query("SELECT * FROM card_table")
    fun getAllCard(): List<CardTable>?

    @Query(
        "SELECT * FROM card_table " +
            "WHERE productId = :productId" +
            " AND isVirtual = :isVirtual" +
            " AND status IN (:status)" +
            " ORDER BY status ASC LIMIT 1"
    )
    fun getCardByType(
        productNameCard: String,
        isVirtual: Int,
        status: List<Int> = TypeCard.getListValidStatus()
    ): CardTable?

    @Query("SELECT * FROM card_table WHERE cardId = :id")
    fun getCardById(id: Long): CardTable?

    @Update
    fun updateCard(card: CardTable)

    @Delete
    fun delete(card: CardTable)

    @Query(
        "SELECT * FROM card_table" +
            " WHERE isVirtual = 1" +
            " AND status IN (:status)" +
            " ORDER BY status ASC"
    )
    fun getVirtualCardAll(
        status: List<Int> = TypeCard.getListValidStatus()
    ): List<CardTable>?

    @Query("DELETE FROM card_table")
    suspend fun nukedTable()
}