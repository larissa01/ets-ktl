package br.com.digio.uber.db.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "faq_metadata")
data class FaqMetadata constructor(
    @PrimaryKey
    @ColumnInfo(name = "faq_version")
    val version: String
)