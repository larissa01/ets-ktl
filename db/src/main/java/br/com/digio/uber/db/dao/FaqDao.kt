package br.com.digio.uber.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import br.com.digio.uber.db.tables.FaqMetadata
import br.com.digio.uber.db.tables.FaqQuestion
import br.com.digio.uber.db.tables.FaqSubject
import br.com.digio.uber.db.tables.FaqSubjectWithQuestions

@Dao
abstract class FaqDao {

    suspend fun saveSubjects(faqResponse: List<FaqSubjectWithQuestions>) {
        val subjects: MutableList<FaqSubject> = mutableListOf()
        val questions: MutableList<FaqQuestion> = mutableListOf()
        faqResponse.forEach { subject ->
            subjects.add(subject.subject)
            subject.questions.forEach { question ->
                questions.add(question.copy(subjectId = subject.subject.id))
            }
        }
        saveAllQuestions(questions)
        saveAllSubjects(subjects)
    }

    suspend fun nukedTable() {
        cleanQuestionsTable()
        cleanSubjectsTable()
        cleanFaqMetadata()
    }

    @Transaction
    @Query("SELECT * FROM faq_subject")
    abstract suspend fun getSubjects(): List<FaqSubjectWithQuestions>

    @Query("SELECT  * FROM faq_metadata LIMIT 1")
    abstract suspend fun getFaqMetadata(): FaqMetadata?

    @Insert
    abstract suspend fun saveFaqMetadata(faqMetadata: FaqMetadata)

    @Insert
    abstract suspend fun saveAllSubjects(subject: List<FaqSubject>)

    @Insert
    abstract suspend fun saveAllQuestions(subject: List<FaqQuestion>)

    @Query("DELETE FROM faq_subject")
    abstract suspend fun cleanSubjectsTable()

    @Query("DELETE FROM faq_question")
    abstract suspend fun cleanQuestionsTable()

    @Query("DELETE FROM faq_metadata")
    abstract suspend fun cleanFaqMetadata()
}