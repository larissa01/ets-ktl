package br.com.digio.uber.db.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import br.com.digio.uber.model.transfer.BankDomain

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */

@Entity(tableName = "bank")
class BankTable(
    @PrimaryKey
    @ColumnInfo(name = "code")
    val code: String,
    @ColumnInfo(name = "name")
    val name: String
)

private fun BankTable.toDomain(): BankDomain =
    BankDomain(this.name, this.code)