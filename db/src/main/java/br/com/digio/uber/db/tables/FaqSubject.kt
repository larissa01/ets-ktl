package br.com.digio.uber.db.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "faq_subject")
data class FaqSubject(
    @PrimaryKey
    @ColumnInfo(name = "subjectId")
    val id: String,

    @ColumnInfo(name = "subject")
    val subject: String?,

    @ColumnInfo(name = "icon")
    val icon: String?,

    @ColumnInfo(name = "client")
    val client: String?
)