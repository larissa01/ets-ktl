package br.com.digio.uber.db.tables

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "card_table")
data class CardTable constructor(
    @PrimaryKey
    @ColumnInfo(name = "cardId")
    val cardId: Long,

    @ColumnInfo(name = "cardMaskedNumber")
    val cardMaskedNumber: String?,

    @ColumnInfo(name = "cardSequential")
    val cardSequential: Int?,

    @ColumnInfo(name = "expirationDate")
    val expirationDate: String?,

    @ColumnInfo(name = "isVirtualCard")
    val isVirtualCard: Int?,

    @ColumnInfo(name = "issueDate")
    val issueDate: String?,

    @ColumnInfo(name = "personId")
    val personId: Int?,

    @ColumnInfo(name = "productId")
    val productId: Int?,

    @ColumnInfo(name = "productName")
    val productName: String,

    @ColumnInfo(name = "productType")
    val stageId: Int,

    @ColumnInfo(name = "statusId")
    val statusId: Long,

    @ColumnInfo(name = "accountId")
    val accountId: Long?
) : Parcelable