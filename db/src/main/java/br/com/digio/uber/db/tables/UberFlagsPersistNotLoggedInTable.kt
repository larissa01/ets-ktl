package br.com.digio.uber.db.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "uber_flags_table")
data class UberFlagsPersistNotLoggedInTable(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Long = 1L,
    @ColumnInfo(name = "saveCpfInLogin")
    val saveCpfInLogin: Boolean = false
)