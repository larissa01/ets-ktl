package br.com.digio.uber.db.file

import android.content.Context
import br.com.digio.uber.db.R
import br.com.digio.uber.model.faq.FaqResponse
import com.google.gson.Gson

class FaqFileReaderImpl(private val context: Context) : FaqFileReader {

    var cache: FaqResponse? = null
    override fun readFaqFile(): FaqResponse? {
        if (cache == null) {
            val text = context.resources.openRawResource(R.raw.faq).bufferedReader().use { it.readText() }
            cache = Gson().fromJson<FaqResponse>(text, FaqResponse::class.java)
        }
        return cache
    }
}