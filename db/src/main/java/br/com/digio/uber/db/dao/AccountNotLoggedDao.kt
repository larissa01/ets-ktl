package br.com.digio.uber.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import br.com.digio.uber.db.tables.AccountNotLoggedTable

@Dao
interface AccountNotLoggedDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(card: AccountNotLoggedTable)

    @Query("SELECT * FROM account_not_logged_table ORDER BY id ASC LIMIT 1")
    suspend fun getAccountNotLoggedTable(): AccountNotLoggedTable?

    @Update
    suspend fun updateAccountNotLoggedTable(card: AccountNotLoggedTable)

    @Delete
    suspend fun deleteAccountNotLoggedTable(table: AccountNotLoggedTable)

    @Query("DELETE FROM account_not_logged_table")
    suspend fun nukedTable()
}