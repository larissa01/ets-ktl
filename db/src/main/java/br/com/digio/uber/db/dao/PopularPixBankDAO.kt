package br.com.digio.uber.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.digio.uber.db.tables.PopularPixBankTable

@Dao
interface PopularPixBankDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(account: PopularPixBankTable)

    @Query("SELECT * FROM popular_pix_bank ORDER BY code")
    suspend fun getBanks(): List<PopularPixBankTable>?

    @Delete
    suspend fun deleteBanks(table: PopularPixBankTable)

    @Query("DELETE FROM popular_pix_bank")
    suspend fun nukedTable()
}