package br.com.digio.uber.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.digio.uber.db.tables.PopularBankTable

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */

@Dao
interface PopularBankDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(account: PopularBankTable)

    @Query("SELECT * FROM popular_bank ORDER BY code")
    suspend fun getBanks(): List<PopularBankTable>?

    @Delete
    suspend fun deleteBanks(table: PopularBankTable)

    @Query("DELETE FROM popular_bank")
    suspend fun nukedTable()
}