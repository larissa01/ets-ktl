package br.com.digio.uber.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.digio.uber.db.tables.EyesVisibility

@Dao
interface EyesDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(eyesVisibility: EyesVisibility)

    @Query("SELECT * FROM eyes_visibility ORDER BY id ASC LIMIT 1")
    fun getEyesVisibility(): LiveData<EyesVisibility>

    @Delete
    suspend fun deleteEyesVisibility(table: EyesVisibility)

    @Query("DELETE FROM eyes_visibility")
    suspend fun nukedTable()
}