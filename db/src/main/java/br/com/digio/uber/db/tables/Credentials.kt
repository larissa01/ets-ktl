package br.com.digio.uber.db.tables

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "credentials_table")
data class Credentials constructor(
    @PrimaryKey
    @ColumnInfo(name = "cpfCrypted")
    var cpfCrypted: String,
    @ColumnInfo(name = "password")
    var password: String
) : Parcelable