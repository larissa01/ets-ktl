package br.com.digio.uber.db.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */

@Entity(tableName = "popular_bank")
class PopularBankTable(
    @PrimaryKey
    @ColumnInfo(name = "code")
    val code: String,
    @ColumnInfo(name = "name")
    val name: String
)