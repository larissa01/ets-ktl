package br.com.digio.uber.db.file

import br.com.digio.uber.model.faq.FaqResponse

interface FaqFileReader {
    fun readFaqFile(): FaqResponse?
}