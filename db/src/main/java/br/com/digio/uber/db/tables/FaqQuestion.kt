package br.com.digio.uber.db.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "faq_question")
data class FaqQuestion constructor(
    @PrimaryKey
    @ColumnInfo(name = "questionId")
    val id: String,

    @ColumnInfo(name = "title")
    val title: String?,

    @ColumnInfo(name = "tag")
    val tag: String?,

    @ColumnInfo(name = "is_frequent_question")
    val isFrequentQuestion: Boolean?,

    @ColumnInfo(name = "body")
    val body: String?,

    @ColumnInfo(name = "text_button")
    val textButton: String?,

    @ColumnInfo(name = "text_help_button")
    val textHelpButton: String?,

    @ColumnInfo(name = "type_button")
    val typeButton: String?,

    @ColumnInfo(name = "semantics")
    val semantics: String?,

    @ColumnInfo(name = "external_link")
    val externalLink: String?,

    @ColumnInfo(name = "helpful")
    val helpful: Boolean?,

    @ColumnInfo(name = "client")
    val client: String?,

    @ColumnInfo(name = "subjectId")
    val subjectId: String
)