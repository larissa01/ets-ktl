package br.com.digio.uber.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.com.digio.uber.db.BuildConfig.DB_PASSWORD
import br.com.digio.uber.db.dao.AccountNotLoggedDao
import br.com.digio.uber.db.dao.BankDAO
import br.com.digio.uber.db.dao.CredentialsDao
import br.com.digio.uber.db.dao.EyesDAO
import br.com.digio.uber.db.dao.FaqDao
import br.com.digio.uber.db.dao.PixBankDAO
import br.com.digio.uber.db.dao.PopularBankDAO
import br.com.digio.uber.db.dao.PopularPixBankDAO
import br.com.digio.uber.db.dao.UberFlagsPersistNotLoggedInDao
import br.com.digio.uber.db.tables.AccountNotLoggedTable
import br.com.digio.uber.db.tables.BankTable
import br.com.digio.uber.db.tables.CardTable
import br.com.digio.uber.db.tables.Credentials
import br.com.digio.uber.db.tables.EyesVisibility
import br.com.digio.uber.db.tables.FaqMetadata
import br.com.digio.uber.db.tables.FaqQuestion
import br.com.digio.uber.db.tables.FaqSubject
import br.com.digio.uber.db.tables.PixBankTable
import br.com.digio.uber.db.tables.PopularBankTable
import br.com.digio.uber.db.tables.PopularPixBankTable
import br.com.digio.uber.db.tables.UberFlagsPersistNotLoggedInTable
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SupportFactory

@Database(
    entities = [
        CardTable::class,
        AccountNotLoggedTable::class,
        EyesVisibility::class,
        UberFlagsPersistNotLoggedInTable::class,
        BankTable::class,
        PopularBankTable::class,
        Credentials::class,
        FaqQuestion::class,
        FaqSubject::class,
        FaqMetadata::class,
        PixBankTable::class,
        PopularPixBankTable::class,
    ],
    version = 6,
    exportSchema = false
)
abstract class UberDatabase : RoomDatabase() {

    abstract fun eyesVisibilityDAO(): EyesDAO

    abstract fun accountNotLoggedDAO(): AccountNotLoggedDao

    abstract fun uberFlagsPersistNotLoggedInDao(): UberFlagsPersistNotLoggedInDao

    abstract fun bankDAO(): BankDAO

    abstract fun pixBankDAO(): PixBankDAO

    abstract fun popularBankDAO(): PopularBankDAO

    abstract fun popularPixBankDAO(): PopularPixBankDAO

    abstract fun credentialsDao(): CredentialsDao

    abstract fun faqDao(): FaqDao

    suspend fun deleteTablesLogout() {
        eyesVisibilityDAO().nukedTable()
        bankDAO().nukedTable()
        pixBankDAO().nukedTable()
        popularBankDAO().nukedTable()
        popularPixBankDAO().nukedTable()
        credentialsDao().nukedTable()
        faqDao().nukedTable()
    }
}

fun createUberDatabase(context: Context): UberDatabase {
    val passphrase = SQLiteDatabase.getBytes(DB_PASSWORD.toCharArray())
    val factory = SupportFactory(passphrase)
    return Room.databaseBuilder(context, UberDatabase::class.java, "uber_database.db")
        .fallbackToDestructiveMigration()
        .openHelperFactory(factory)
        .build()
}