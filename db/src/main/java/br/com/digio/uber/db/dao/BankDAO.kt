package br.com.digio.uber.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.digio.uber.db.tables.BankTable

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */

@Dao
interface BankDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(account: BankTable)

    @Query("SELECT * FROM bank ORDER BY code")
    suspend fun getBanks(): List<BankTable>?

    @Delete
    suspend fun deleteBanks(table: BankTable)

    @Query("DELETE FROM bank")
    suspend fun nukedTable()
}