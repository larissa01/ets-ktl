package br.com.digio.uber.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.digio.uber.db.tables.PixBankTable

@Dao
interface PixBankDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(account: PixBankTable)

    @Query("SELECT * FROM pix_bank ORDER BY code")
    suspend fun getBanks(): List<PixBankTable>?

    @Delete
    suspend fun deleteBanks(table: PixBankTable)

    @Query("DELETE FROM pix_bank")
    suspend fun nukedTable()
}