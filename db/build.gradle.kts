plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.library")
}

val dpPassword: String by project

configureAndroidLibrary(
    releaseBuildTypeConfig = {
        buildConfigField("String", "DB_PASSWORD", dpPassword)
    },
    homolBuildTypeConfig = {
        buildConfigField("String", "DB_PASSWORD", dpPassword)
    },
    debugBuildTypeConfig = {
        buildConfigField("String", "DB_PASSWORD", dpPassword)
    }
)

dependencies {
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.ROOM)
    implementation(Dependencies.ROOM_KTX)
    implementation(Dependencies.SQLCIPHER)
    implementation(Dependencies.LIFECYCLE_LIVEDATA)
    implementation(Dependencies.GSON)

    kapt(Dependencies.ROOM_KAPT)
    implementation(project(":model"))
}