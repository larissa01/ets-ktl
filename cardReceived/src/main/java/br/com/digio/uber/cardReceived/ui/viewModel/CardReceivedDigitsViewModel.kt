package br.com.digio.uber.cardReceived.ui.viewModel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.cardReceived.R
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager

class CardReceivedDigitsViewModel constructor(
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private companion object {
        private const val LAST_DIGITS_LENGTH = 4
    }

    val cardId = MutableLiveData<Long>()
    val lastDigits = MutableLiveData<String>()
    val edtLastDigits = MutableLiveData<String>()
    val checkPassword = MutableLiveData<Boolean>()
    val enableButton = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<String>()

    fun checkButtonStatus() {
        enableButton.value = validateLength()
    }

    fun validateLastDigits(): Boolean =
        if (validateLength() &&
            edtLastDigits.value?.toIntOrNull() != null &&
            edtLastDigits.value == lastDigits.value
        ) {
            true
        } else {
            errorMessage.value = resourceManager.getString(R.string.card_received_activation_error_invalid_number)
            false
        }

    private fun validateLength(): Boolean {
        errorMessage.value = null
        return edtLastDigits.value?.length == LAST_DIGITS_LENGTH
    }
}