package br.com.digio.uber.cardReceived.ui.viewModel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.cardReceived.R
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.usecase.cardReceived.CardReceivedActivationUseCase
import br.com.digio.uber.model.cardReceived.CardReceivedActivationRequest
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.RSAUtil

class CardReceivedCVVViewModel constructor(
    private val cardReceivedActivationUseCase: CardReceivedActivationUseCase,
    private val resourceManager: ResourceManager,
    private val analytics: Analytics
) : BaseViewModel() {

    private companion object {
        private const val CVV_LENGTH = 3
    }

    val cardId = MutableLiveData<Long>()
    val lastDigits = MutableLiveData<String>()
    val edtCVV = MutableLiveData<String>()
    val checkPassword = MutableLiveData<Boolean>()
    val enableButton = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<String>()
    val response = MutableLiveData<Boolean>()

    fun checkButtonStatus() {
        enableButton.value = validateLength()
    }

    fun validateCVV(): Boolean =
        if (validateLength() && edtCVV.value?.toIntOrNull() != null) {
            true
        } else {
            errorMessage.value = resourceManager.getString(R.string.card_received_activation_error_invalid_number)
            false
        }

    private fun validateLength(): Boolean {
        errorMessage.value = null
        return edtCVV.value?.length == CVV_LENGTH
    }

    fun postToValidateCard() {
        cardReceivedActivationUseCase(
            CardReceivedActivationRequest(
                cvv = RSAUtil.generateHash(resourceManager.context, edtCVV.value.toString()),
                cryptoKeyId = Const.CryptoKey.KEY_ID,
                cardLastDigits = lastDigits.value.toString(),
                cardId = cardId.value.toString()
            )
        ).singleExec(
            onError = { _, _, _ ->
                analytics.pushSimpleEvent(Tags.CARD_RECEIVED_ACTIVATION_ERROR)
                errorMessage.value = resourceManager.getString(R.string.card_received_activation_error_invalid_number)
                EmptyErrorObject
            },
            onSuccessBaseViewModel = {
                analytics.pushSimpleEvent(Tags.CARD_RECEIVED_ACTIVATION_SUCCESS)
                errorMessage.value = null
                response.value = true
            },
        )
    }
}