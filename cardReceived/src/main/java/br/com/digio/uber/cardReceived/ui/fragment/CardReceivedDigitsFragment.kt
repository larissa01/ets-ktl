package br.com.digio.uber.cardReceived.ui.fragment

import android.os.Bundle
import android.view.View
import br.com.digio.uber.cardReceived.BR
import br.com.digio.uber.cardReceived.R
import br.com.digio.uber.cardReceived.databinding.FragmentCardReceivedDigitsBinding
import br.com.digio.uber.cardReceived.navigation.CardReceivedOpKey
import br.com.digio.uber.cardReceived.ui.activity.CardReceivedActivity
import br.com.digio.uber.cardReceived.ui.viewModel.CardReceivedDigitsViewModel
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const.Extras.CARD_ID
import br.com.digio.uber.util.Const.Extras.LAST_DIGITS
import org.koin.android.viewmodel.ext.android.viewModel

class CardReceivedDigitsFragment :
    BNWFViewModelFragment<FragmentCardReceivedDigitsBinding, CardReceivedDigitsViewModel>() {

    override val bindingVariable: Int = BR.cardReceivedDigitsViewModel
    override val getLayoutId: Int = R.layout.fragment_card_received_digits
    override val viewModel: CardReceivedDigitsViewModel? by viewModel()
    override fun tag(): String = CardReceivedOpKey.DIGITS.name
    override fun getStatusBarAppearance(): StatusBarColor = CardReceivedOpKey.DIGITS.theme

    companion object : FragmentNewInstance<CardReceivedDigitsFragment>(::CardReceivedDigitsFragment)

    private val cardId: Long by lazy { checkNotNull(arguments?.getLong(CARD_ID)) }
    private val lastDigits: String by lazy { checkNotNull(arguments?.getString(LAST_DIGITS)) }

    override fun initialize() {
        (activity as CardReceivedActivity).getToolbar()?.setTitle(R.string.title_cardreceived_activation)
        viewModel?.initializer { onBtnClicked(it) }
        setupArguments()
        setupObservables()
    }

    private fun setupArguments() {
        viewModel?.cardId?.postValue(cardId)
        viewModel?.lastDigits?.postValue(lastDigits)
    }

    private fun setupObservables() {
        viewModel?.edtLastDigits?.observe(
            this,
            {
                if (it != null) {
                    viewModel?.checkButtonStatus()
                }
            }
        )
    }

    private fun onBtnClicked(v: View) {
        when (v.id) {
            R.id.btnCardReceivedActivate -> {
                if (viewModel?.validateLastDigits() == true) {
                    callGoTo(
                        CardReceivedOpKey.CVV.name,
                        Bundle().apply {
                            viewModel?.cardId?.value?.let { putLong(CARD_ID, it) }
                            viewModel?.lastDigits?.value?.let { putString(LAST_DIGITS, it) }
                        }
                    )
                }
            }
        }
    }
}