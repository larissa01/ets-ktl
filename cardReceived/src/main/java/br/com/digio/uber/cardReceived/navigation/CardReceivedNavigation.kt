package br.com.digio.uber.cardReceived.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.cardReceived.ui.fragment.CardReceivedCVVFragment
import br.com.digio.uber.cardReceived.ui.fragment.CardReceivedDigitsFragment
import br.com.digio.uber.cardReceived.ui.fragment.CardReceivedMainFragment
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName

class CardReceivedNavigation(
    activity: AppCompatActivity,
) : GenericNavigationWithFlow(activity) {

    fun init(bundle: Bundle?) {
        goTo(GoTo(CardReceivedOpKey.MAIN.name, bundle))
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            CardReceivedOpKey.MAIN.name to showMain(),
            CardReceivedOpKey.DIGITS.name to showDigits(),
            CardReceivedOpKey.CVV.name to showCVV()
        )

    private fun showMain(): GenericNavigationWIthFlowGetFragmentByName = {
        CardReceivedMainFragment.newInstance(this, it)
    }

    private fun showDigits(): GenericNavigationWIthFlowGetFragmentByName = {
        CardReceivedDigitsFragment.newInstance(this, it)
    }

    private fun showCVV(): GenericNavigationWIthFlowGetFragmentByName = {
        CardReceivedCVVFragment.newInstance(this, it)
    }
}