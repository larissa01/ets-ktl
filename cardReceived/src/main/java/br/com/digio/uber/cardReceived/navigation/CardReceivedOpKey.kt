package br.com.digio.uber.cardReceived.navigation

import br.com.digio.uber.common.kenum.StatusBarColor

enum class CardReceivedOpKey(val theme: StatusBarColor) {
    MAIN(StatusBarColor.WHITE),
    DIGITS(StatusBarColor.WHITE),
    CVV(StatusBarColor.WHITE),
    ACTIVATED(StatusBarColor.WHITE)
}