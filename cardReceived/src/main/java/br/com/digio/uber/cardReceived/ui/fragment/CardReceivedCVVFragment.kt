package br.com.digio.uber.cardReceived.ui.fragment

import android.view.View
import br.com.digio.uber.cardReceived.BR
import br.com.digio.uber.cardReceived.R
import br.com.digio.uber.cardReceived.databinding.FragmentCardReceivedCvvBinding
import br.com.digio.uber.cardReceived.navigation.CardReceivedOpKey
import br.com.digio.uber.cardReceived.ui.activity.CardReceivedActivity
import br.com.digio.uber.cardReceived.ui.viewModel.CardReceivedCVVViewModel
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.generics.makeMessageSuccessObject
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const
import org.koin.android.viewmodel.ext.android.viewModel

class CardReceivedCVVFragment :
    BNWFViewModelFragment<FragmentCardReceivedCvvBinding, CardReceivedCVVViewModel>() {

    override val bindingVariable: Int = BR.cardReceivedCVVViewModel
    override val getLayoutId: Int = R.layout.fragment_card_received_cvv
    override val viewModel: CardReceivedCVVViewModel? by viewModel()
    override fun tag(): String = CardReceivedOpKey.CVV.name
    override fun getStatusBarAppearance(): StatusBarColor = CardReceivedOpKey.CVV.theme

    companion object : FragmentNewInstance<CardReceivedCVVFragment>(::CardReceivedCVVFragment)

    private val cardId: Long by lazy { checkNotNull(arguments?.getLong(Const.Extras.CARD_ID)) }
    private val lastDigits: String by lazy { checkNotNull(arguments?.getString(Const.Extras.LAST_DIGITS)) }

    override fun initialize() {
        (activity as CardReceivedActivity).getToolbar()?.setTitle(R.string.title_cardreceived_activation)
        viewModel?.initializer { onBtnClicked(it) }
        setupArguments()
        setupObservables()
    }

    private fun setupArguments() {
        viewModel?.cardId?.postValue(cardId)
        viewModel?.lastDigits?.postValue(lastDigits)
    }

    private fun setupObservables() {
        viewModel?.edtCVV?.observe(
            this,
            {
                if (it != null) {
                    viewModel?.checkButtonStatus()
                }
            }
        )
        viewModel?.response?.observe(
            this,
            {
                if (it != null) {
                    if (it == true) {
                        viewModel?.message?.value = makeMessageSuccessObject {
                            title = R.string.card_received_activated_title
                            message = R.string.card_received_activated_desc_all
                            onCloseDialog = {
                                activity?.finish()
                            }
                        }
                    }
                }
            }
        )
    }

    private fun onBtnClicked(v: View) {
        when (v.id) {
            R.id.btnCardReceivedActivate -> {
                if (viewModel?.validateCVV() == true) {
                    viewModel?.postToValidateCard()
                }
            }
        }
    }
}