package br.com.digio.uber.cardReceived.mapper

import br.com.digio.uber.cardReceived.ui.uiModel.CardReceivedUiModel
import br.com.digio.uber.model.card.CardResponse
import br.com.digio.uber.util.getFinalDigits
import br.com.digio.uber.util.mapper.AbstractMapper

class CardReceivedMapper : AbstractMapper<CardResponse?, CardReceivedUiModel> {

    private companion object {
        private const val LAST_DIGITS_LENGTH = 4
    }

    override fun map(param: CardResponse?): CardReceivedUiModel = mapItem(param)

    private fun mapItem(param: CardResponse?): CardReceivedUiModel {
        return CardReceivedUiModel(
            cardId = param?.cardId ?: -1,
            lastDigits = param?.cardMaskedNumber?.getFinalDigits(LAST_DIGITS_LENGTH) ?: ""
        )
    }
}