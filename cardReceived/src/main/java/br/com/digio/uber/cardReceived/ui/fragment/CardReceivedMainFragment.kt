package br.com.digio.uber.cardReceived.ui.fragment

import android.os.Bundle
import android.view.View
import br.com.digio.uber.cardReceived.BR
import br.com.digio.uber.cardReceived.R
import br.com.digio.uber.cardReceived.databinding.FragmentCardReceivedMainBinding
import br.com.digio.uber.cardReceived.navigation.CardReceivedOpKey
import br.com.digio.uber.cardReceived.ui.activity.CardReceivedActivity
import br.com.digio.uber.cardReceived.ui.viewModel.CardReceivedMainViewModel
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const.Extras.CARD_ID
import br.com.digio.uber.util.Const.Extras.LAST_DIGITS
import org.koin.android.viewmodel.ext.android.viewModel

class CardReceivedMainFragment :
    BNWFViewModelFragment<FragmentCardReceivedMainBinding, CardReceivedMainViewModel>() {

    override val bindingVariable: Int = BR.cardReceivedMainViewModel
    override val getLayoutId: Int = R.layout.fragment_card_received_main
    override val viewModel: CardReceivedMainViewModel by viewModel()
    override fun tag(): String = CardReceivedOpKey.MAIN.name
    override fun getStatusBarAppearance(): StatusBarColor = CardReceivedOpKey.MAIN.theme

    companion object : FragmentNewInstance<CardReceivedMainFragment>(::CardReceivedMainFragment)

    override fun initialize() {
        (activity as CardReceivedActivity).getToolbar()?.setTitle(R.string.title_cardreceived)
        viewModel.initializer { onBtnClicked(it) }
    }

    private fun onBtnClicked(v: View) {
        when (v.id) {
            R.id.btnCardReceivedActivate -> {
                viewModel.cardUi.observe(
                    this,
                    {
                        if (it != null) {
                            callGoTo(
                                CardReceivedOpKey.DIGITS.name,
                                Bundle().apply {
                                    putLong(CARD_ID, it.cardId)
                                    putString(LAST_DIGITS, it.lastDigits)
                                }
                            )
                        }
                    }
                )
            }
        }
    }
}