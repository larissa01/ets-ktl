package br.com.digio.uber.cardReceived.di

import br.com.digio.uber.cardReceived.ui.viewModel.CardReceivedCVVViewModel
import br.com.digio.uber.cardReceived.ui.viewModel.CardReceivedDigitsViewModel
import br.com.digio.uber.cardReceived.ui.viewModel.CardReceivedMainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val cardReceivedViewModelModule = module {
    viewModel { CardReceivedMainViewModel(get()) }
    viewModel { CardReceivedDigitsViewModel(get()) }
    viewModel { CardReceivedCVVViewModel(get(), get(), get()) }
}