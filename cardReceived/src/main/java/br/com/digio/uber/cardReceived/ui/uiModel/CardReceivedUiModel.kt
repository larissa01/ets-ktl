package br.com.digio.uber.cardReceived.ui.uiModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CardReceivedUiModel(
    internal val cardId: Long,
    internal val lastDigits: String
) : Parcelable {
    internal fun checkIfCardExists(): Boolean = cardId != -1L && lastDigits.isNotEmpty()
}