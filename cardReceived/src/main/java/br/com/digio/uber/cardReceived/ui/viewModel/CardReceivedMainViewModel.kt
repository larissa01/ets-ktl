package br.com.digio.uber.cardReceived.ui.viewModel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.cardReceived.mapper.CardReceivedMapper
import br.com.digio.uber.cardReceived.ui.uiModel.CardReceivedUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.card.GetCardTypeUseCase
import br.com.digio.uber.model.card.TypeCard

class CardReceivedMainViewModel constructor(
    private val getCardTypeUseCase: GetCardTypeUseCase
) : BaseViewModel() {

    val cardUi = MutableLiveData<CardReceivedUiModel?>()

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        getVirtualDebitCard()
    }

    private fun getVirtualDebitCard() {
        getCardTypeUseCase(TypeCard.DEBIT_ELO).singleExec(
            onSuccessBaseViewModel = { response ->
                val card = CardReceivedMapper().map(response)
                card.checkIfCardExists().let { cardUi.value = card }
            }
        )
    }
}