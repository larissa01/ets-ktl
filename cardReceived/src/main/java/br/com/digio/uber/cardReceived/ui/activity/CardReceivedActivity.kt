package br.com.digio.uber.cardReceived.ui.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.cardReceived.R
import br.com.digio.uber.cardReceived.di.cardReceivedViewModelModule
import br.com.digio.uber.cardReceived.navigation.CardReceivedNavigation
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.helper.ViewIds
import br.com.digio.uber.common.navigation.GenericNavigation
import org.koin.core.module.Module

class CardReceivedActivity : BaseViewModelActivity<ViewDataBinding, BaseViewModel>() {

    private val cardReceivedNavigation = CardReceivedNavigation(this)
    override val bindingVariable: Int? = null
    override val viewModel: BaseViewModel? = null
    override fun getLayoutId(): Int = LayoutIds.navigationLayoutWithToolbarActivityWhite
    override fun getModule(): List<Module> = listOf(cardReceivedViewModelModule)
    override fun getNavigation(): GenericNavigation = cardReceivedNavigation
    override fun getToolbar(): Toolbar? = findViewById<Toolbar>(ViewIds.basicToolbarIdWhite).apply {
        this.title = getString(R.string.title_cardreceived)
        this.navigationIcon = ContextCompat.getDrawable(
            context,
            br.com.digio.uber.common.R.drawable.ic_back_arrow_black
        )
    }

    override fun showDisplayShowTitle(): Boolean = true

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        cardReceivedNavigation.init(intent.extras)
    }
}