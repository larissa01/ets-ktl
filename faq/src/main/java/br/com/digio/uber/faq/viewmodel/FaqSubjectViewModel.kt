package br.com.digio.uber.faq.viewmodel

import androidx.lifecycle.LiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.faq.GetFaqSubjectsUseCase
import br.com.digio.uber.faq.dataui.FaqSubject
import br.com.digio.uber.faq.mapper.SubjectListMapper

class FaqSubjectViewModel(getFaqSubjectsUseCase: GetFaqSubjectsUseCase) : BaseViewModel() {

    val subjectsLiveData: LiveData<List<FaqSubject>> = getFaqSubjectsUseCase(Unit).exec().map(SubjectListMapper())
}