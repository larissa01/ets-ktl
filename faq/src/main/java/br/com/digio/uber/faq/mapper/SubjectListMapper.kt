package br.com.digio.uber.faq.mapper

import br.com.digio.uber.faq.dataui.FaqSubject
import br.com.digio.uber.model.faq.FaqSubjectResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class SubjectListMapper : AbstractMapper<List<FaqSubjectResponse>, List<FaqSubject>> {

    override fun map(param: List<FaqSubjectResponse>): List<FaqSubject> {
        return param.map { SubjectMapper().map(it) }
    }
}