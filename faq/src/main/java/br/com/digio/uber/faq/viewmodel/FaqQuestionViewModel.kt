package br.com.digio.uber.faq.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.dataui.FaqQuestion
import br.com.digio.uber.faq.dataui.FaqQuestionScreenState

class FaqQuestionViewModel : BaseViewModel() {

    val questionLiveData: MutableLiveData<FaqQuestion> = MutableLiveData()
    val screenStateLiveData: MutableLiveData<FaqQuestionScreenState> = MutableLiveData(
        FaqQuestionScreenState(
            showWasUtil = true,
            showChatButton = false,
            false
        )
    )

    override fun onItemClicked(v: View) {
        when (v.id) {
            R.id.img_was_util_deslike ->
                screenStateLiveData.value = FaqQuestionScreenState(
                    showWasUtil = false,
                    showChatButton = true,
                    false
                )
            R.id.label_no ->
                screenStateLiveData.value = FaqQuestionScreenState(
                    showWasUtil = false,
                    showChatButton = true,
                    false
                )
            R.id.img_was_util_like ->
                screenStateLiveData.value = FaqQuestionScreenState(
                    showWasUtil = false,
                    showChatButton = false,
                    showThanks = true
                )
            R.id.label_yes ->
                screenStateLiveData.value = FaqQuestionScreenState(
                    showWasUtil = false,
                    showChatButton = false,
                    showThanks = true
                )
        }
    }

    fun setQuestion(question: FaqQuestion) {
        questionLiveData.value = question
    }
}