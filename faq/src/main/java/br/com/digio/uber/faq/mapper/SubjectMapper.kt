package br.com.digio.uber.faq.mapper

import br.com.digio.uber.faq.dataui.FaqSubject
import br.com.digio.uber.model.faq.FaqSubjectResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class SubjectMapper : AbstractMapper<FaqSubjectResponse, FaqSubject> {

    override fun map(param: FaqSubjectResponse): FaqSubject {
        return FaqSubject(
            param.id,
            param.subject,
            param.icon,
            param.client,
            param.questions.map { QuestionMapper().map(it) }
        )
    }
}