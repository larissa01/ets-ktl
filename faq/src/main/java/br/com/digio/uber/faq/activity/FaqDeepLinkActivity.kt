package br.com.digio.uber.faq.activity

import android.net.Uri
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.di.faqViewModelModule
import br.com.digio.uber.util.Const
import org.koin.core.module.Module

class FaqDeepLinkActivity : BaseActivity() {

    private val deepLinkFaqKey: String? by lazy {
        intent.extras?.getString(Const.RequestOnResult.FaqDeepLink.KEY_FAQ_DEEP_LINK)
    }

    override fun getLayoutId(): Int? =
        R.layout.activity_faq_deep_link

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        val navHost =
            supportFragmentManager
                .findFragmentById(R.id.nav_host_faq_deep_link) as? NavHostFragment
        deepLinkFaqKey?.let { deepLinkFaqKeyLet ->
            navHost?.navController?.navigate(Uri.parse(deepLinkFaqKeyLet))
        } ?: finish()
    }

    override fun getModule(): List<Module>? =
        listOf(faqViewModelModule)
}