package br.com.digio.uber.faq.adapter.items

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.faq.dataui.FaqQuestion
import br.com.digio.uber.faq.utils.StringFormatUtil
import kotlinx.android.synthetic.main.item_faq_frequent_questions.view.*

class FrequentQuestionViewHolder(view: View, val onItemClick: (FaqQuestion) -> Unit) : RecyclerView.ViewHolder(view) {
    fun bind(question: FaqQuestion, search: String = "") {
        itemView.apply {
            textview_question.text = question.title
            if (search.isNotEmpty()) {
                StringFormatUtil.highlightTextPart(context, textview_question, search)
            }
            setOnClickListener { onItemClick(question) }
        }
    }
}