package br.com.digio.uber.faq.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.adapter.items.FrequentQuestionViewHolder
import br.com.digio.uber.faq.dataui.FaqQuestion
import br.com.digio.uber.util.inflate

class FrequentQuestionsAdapter(
    private var questionList: List<FaqQuestion>,
    private val onItemClick: (FaqQuestion) -> Unit
) :
    RecyclerView.Adapter<FrequentQuestionViewHolder>() {
    var search: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FrequentQuestionViewHolder {
        return FrequentQuestionViewHolder(parent.inflate(R.layout.item_faq_frequent_questions), onItemClick)
    }

    override fun getItemCount(): Int = questionList.size

    override fun onBindViewHolder(holder: FrequentQuestionViewHolder, position: Int) {
        holder.bind(questionList[position], search)
    }

    fun search(search: String, results: List<FaqQuestion>) {
        this.search = search
        questionList = results
        notifyDataSetChanged()
    }
}