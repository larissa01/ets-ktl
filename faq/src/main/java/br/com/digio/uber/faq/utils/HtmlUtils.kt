package br.com.digio.uber.faq.utils

fun String.setFonts(): String {
    return """
                    <!DOCTYPE html>
                    <html>
                        <head>
                            <style>
                           @font-face{
                                font-family:'UberMoveText-Regular';
                                src: local('UberMoveText-Regular'), url('UberMoveText-Regular.otf') format('truetype');
                            }
                            
                            @font-face{
                                font-family:'UberMoveText-Regular';
                                src: local('UberMoveText-Regular'), url('UberMoveText-Regular.otf') format('truetype');
                            }
                            
                            body { margin: 0px; padding: 0px; background-color: transparent; }
                            
                            body, p, span, td, th {
                                font-family: 'UberMoveText-Regular' !important;
                                font-weight: normal;
                                font-size: 14px !important;
                                color: #687384;
                                line-height: 20.5px;
                            }
                            
                            th, b {
                                font-weight: normal;
                                font-size: 14px !important;
                                color: #000000;
                                font-family: 'UberMoveText-Regular' !important;
                            }
                            
                            table { border-spacing: 0px; }
                            
                            .table-bordered>thead>tr>th, .table-bordered>tbody>tr>td {
                                border: 1px solid #ddd;
                                padding: 5px;
                            
                            }
                            </style>
                        </head>
                        <body  style="background-color:#F9F9F9;">
                            ${this.replace(
        "font-family: Roboto;",
        "font-family: 'UberMoveText-Regular';",
        true
    )}
                        </body>
                    </html>
    """.trimIndent()
}