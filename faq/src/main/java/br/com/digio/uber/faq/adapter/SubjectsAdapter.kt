package br.com.digio.uber.faq.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.adapter.items.ClosedSubjectViewHolder
import br.com.digio.uber.faq.adapter.items.OpenSubjectViewHolder
import br.com.digio.uber.faq.adapter.items.SubjectQuestionViewHolder
import br.com.digio.uber.faq.dataui.FaqQuestion
import br.com.digio.uber.faq.dataui.FaqSubject
import br.com.digio.uber.util.inflate

class SubjectsAdapter(
    listSubject: List<FaqSubject>,
    val onQuestionClicked: (FaqQuestion) -> Unit,
    val onSubjectExpanded: (FaqSubject, Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val listItems: MutableList<ItemList> = mutableListOf()
    private var lastSubjectExpanded: FaqSubject? = null

    init {
        listSubject.mapTo(listItems) {
            ItemList(
                CLOSED_SUBJECT,
                it,
                null
            )
        }
    }

    override fun getItemCount(): Int = listItems.size

    override fun getItemViewType(position: Int): Int = listItems[position].type

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            CLOSED_SUBJECT ->
                ClosedSubjectViewHolder(parent.inflate(R.layout.item_subjects_closed)) { faqSubject, position ->
                    expand(faqSubject, position)
                }
            OPEN_SUBJECT ->
                OpenSubjectViewHolder(parent.inflate(R.layout.item_subjects_open)) { faqSubject, position ->
                    collapse(faqSubject, position)
                }
            else ->
                SubjectQuestionViewHolder(parent.inflate(R.layout.item_faq_frequent_questions)) { question ->
                    onQuestionClicked(question)
                }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (listItems[position].type) {
            QUESTION -> {
                listItems[position].question?.let {
                    (holder as SubjectQuestionViewHolder).bind(it)
                }
            }
            OPEN_SUBJECT -> {
                listItems[position].subject?.let {
                    (holder as OpenSubjectViewHolder).bind(it)
                }
            }
            CLOSED_SUBJECT -> {
                listItems[position].subject?.let {
                    (holder as ClosedSubjectViewHolder).bind(it)
                }
            }
        }
    }

    private fun getSubjectPosition(faqSubject: FaqSubject): Int {
        listItems.forEachIndexed { index, itemList ->
            if (itemList.type == OPEN_SUBJECT || itemList.type == CLOSED_SUBJECT) {
                if (itemList.subject?.id == faqSubject.id) {
                    return index
                }
            }
        }
        return -1
    }

    private fun expand(subject: FaqSubject, position: Int) {
        listItems[position].type = OPEN_SUBJECT
        val questionList = subject.questions.map {
            ItemList(QUESTION, null, it)
        }
        listItems.addAll(position + 1, questionList)
        notifyItemRangeInserted(position + 1, questionList.size)
        notifyItemChanged(position)
        collapseOpenSubject(subject)
        onSubjectExpanded(subject, getSubjectPosition(subject))
    }

    private fun collapse(subject: FaqSubject, position: Int) {
        listItems[position].type = CLOSED_SUBJECT
        listItems.removeAll {
            it.type == QUESTION &&
                subject.questions.contains(it.question)
        }
        notifyItemChanged(position)
        notifyItemRangeRemoved(position + 1, subject.questions.size)
    }

    private fun collapseOpenSubject(subject: FaqSubject) {
        lastSubjectExpanded?.let {
            if (it.id != subject.id) {
                val lastSubjectPosition = getSubjectPosition(it)
                if (lastSubjectPosition != -1) {
                    collapse(it, lastSubjectPosition)
                }
            }
        }
        lastSubjectExpanded = subject
    }

    data class ItemList(
        var type: Int,
        var subject: FaqSubject?,
        var question: FaqQuestion?
    )

    companion object {
        const val CLOSED_SUBJECT = 1
        const val OPEN_SUBJECT = 2
        const val QUESTION = 3
    }
}