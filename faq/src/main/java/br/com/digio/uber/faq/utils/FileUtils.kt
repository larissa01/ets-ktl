package br.com.digio.uber.faq.utils

import java.io.File
import java.io.FileOutputStream
import java.io.InputStream

object FileUtils {
    fun copyInputStreamToFile(inputStream: InputStream, file: File) {
        FileOutputStream(file).use { fileOut ->
            inputStream.copyTo(fileOut)
        }
    }
}