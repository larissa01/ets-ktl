package br.com.digio.uber.faq.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.faq.BR
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.adapter.SubjectsAdapter
import br.com.digio.uber.faq.databinding.FragmentFaqSubjectsBinding
import br.com.digio.uber.faq.dataui.FaqQuestion
import br.com.digio.uber.faq.viewmodel.FaqSubjectViewModel
import br.com.digio.uber.util.Const
import kotlinx.android.synthetic.main.fragment_faq_subjects.*
import org.koin.android.ext.android.inject

class FaqSubjectsFragment : BaseViewModelFragment<FragmentFaqSubjectsBinding, FaqSubjectViewModel>() {
    override val bindingVariable = BR.viewModel
    override val getLayoutId = R.layout.fragment_faq_subjects
    override val viewModel: FaqSubjectViewModel by inject()
    override fun tag(): String = FaqSubjectsFragment::javaClass.name
    override fun getToolbar(): Toolbar? = binding?.navToolbar
    override val showDisplayShowTitle = true

    override fun initialize() {
        recyclerview_subjects.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        binding?.navToolbar?.apply {
            setTitle(R.string.question_by_subject)
        }

        viewModel.subjectsLiveData.observe(
            this,
            Observer { list ->
                recyclerview_subjects.adapter = SubjectsAdapter(
                    listSubject = list,
                    onQuestionClicked = {
                        navigateToQuestion(it)
                    },
                    onSubjectExpanded = { _, position ->
                        (recyclerview_subjects?.layoutManager as? LinearLayoutManager)?.scrollToPositionWithOffset(
                            position,
                            0
                        )
                    }
                )
            }
        )
    }

    private fun navigateToQuestion(question: FaqQuestion) {
        navigateTo(
            FaqSubjectsFragmentDirections.actionFaqSubjectsToFaqQuestionFragment(),
            bundleOf(Const.Extras.FAQ_QUESTION to question)
        )
    }

    override fun onNavigationClick(view: View) {
        onBackPressed()
    }

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK
}