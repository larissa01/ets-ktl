package br.com.digio.uber.faq.activity

import android.R.id
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.FileProvider
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.extensions.setLightStatusTextBar
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.faq.BR
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.databinding.ActivityFaqPdfBinding
import br.com.digio.uber.faq.dataui.FaqQuestion
import br.com.digio.uber.faq.di.faqViewModelModule
import br.com.digio.uber.faq.states.FaqPdfStates
import br.com.digio.uber.faq.viewmodel.FaqPdfViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.cachePath
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module
import java.io.File
import java.io.IOException
import java.util.Calendar

class FaqPdfActivity : BaseViewModelActivity<ActivityFaqPdfBinding, FaqPdfViewModel>() {

    override fun getLayoutId(): Int = R.layout.activity_faq_pdf
    override fun getModule(): List<Module> = listOf(faqViewModelModule)
    override val bindingVariable: Int = BR.faqViewModel
    override val viewModel: FaqPdfViewModel by viewModel()
    override fun getToolbar(): Toolbar? = findViewById(R.id.nav_toolbar)
    override fun showDisplayShowTitle(): Boolean = true

    private var pdfFile: File? = null

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        setLightStatusTextBar()

        binding?.navToolbar?.setTitle(R.string.contract)
        createPdfFile()
        viewModel.getPdfs(pdfFile)
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.pdfLiveData.observe(
            this,
            {
                when (it) {
                    is FaqPdfStates.ShowPdf -> {
                        binding?.faqPdfViewer?.fromFile(it.pdfFile)?.load()
                    }
                    is FaqPdfStates.ErrorPdf -> showMessageDefault(
                        ErrorObject(),
                        true,
                        null
                    )
                }
            }
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            id.home -> {
                finish()
                true
            }
            R.id.share_pdf -> {
                pdfFile?.let {
                    sharePdf(it)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_pdf, menu)
        return true
    }

    private fun sharePdf(pdfFile: File) {
        val intent = Intent(Intent.ACTION_SEND).apply {
            type = Const.MimeTypes.PDF_MIME_TYPE
            putExtra(
                Intent.EXTRA_STREAM,
                FileProvider.getUriForFile(
                    this@FaqPdfActivity,
                    packageName + Const.Extras.DOT_PROVIDER,
                    pdfFile
                )
            )
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        }

        startActivity(intent)
    }

    private fun createPdfFile() {
        try {
            val file = File(cachePath("PDF_${Calendar.getInstance().timeInMillis}.pdf"))

            this.pdfFile = file
        } catch (exception: IOException) {
            null
        }
    }

    companion object {
        fun getIntent(context: Context, question: FaqQuestion): Intent =
            Intent(context, FaqPdfActivity::class.java)
    }
}