package br.com.digio.uber.faq.fragment

import android.content.res.Resources
import android.view.View
import br.com.digio.uber.common.base.dialog.BNWFViewModelDialog
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.faq.BR
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.databinding.FragmentFaqShortcutBinding
import br.com.digio.uber.faq.viewmodel.FaqViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import org.koin.android.ext.android.inject

class FaqShortcutDialogFragment : BNWFViewModelDialog<FragmentFaqShortcutBinding, FaqViewModel>() {
    override fun tag(): String = FaqShortcutDialogFragment::class.java.name

    override val viewModel: FaqViewModel by inject()

    override val bindingVariable = BR.faqViewModel

    override val getLayoutId = R.layout.fragment_faq_shortcut

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun onResume() {
        super.onResume()
        setup()
    }

    private fun setup() {
        val bottomSheet = dialog?.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
        bottomSheet?.let {
            setBehavior(it)
            setLayoutParams(it)
        }
        binding?.faqShortcutIvClose?.setOnClickListener { dialog?.dismiss() }
        binding?.faqShortcutBtnOk?.setOnClickListener { dialog?.dismiss() }
    }

    private fun setBehavior(it: View) {
        val behavior = BottomSheetBehavior.from(it)
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun setLayoutParams(it: View) {
        val layoutParams = it.layoutParams
        layoutParams.height = getWindowHeight()
        it.layoutParams = layoutParams
    }

    private fun getWindowHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    companion object {
        fun newInstance(): FaqShortcutDialogFragment {
            return FaqShortcutDialogFragment()
        }
    }
}