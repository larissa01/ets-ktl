package br.com.digio.uber.faq.dataui

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FaqQuestion(
    val id: String?,
    val title: String?,
    val tag: String?,
    val isFrequentQuestion: Boolean?,
    val body: String?,
    val textButton: String?,
    val textHelpButton: String?,
    val typeButton: String?,
    val semantics: String?,
    val externalLink: String?,
    val helpful: Boolean?,
    val client: String?
) : Parcelable