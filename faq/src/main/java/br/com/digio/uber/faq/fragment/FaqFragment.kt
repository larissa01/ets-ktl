package br.com.digio.uber.faq.fragment

import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.extensions.hide
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.show
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.common.kenum.StatusBarColor.BLACK
import br.com.digio.uber.faq.BR
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.adapter.FrequentQuestionsAdapter
import br.com.digio.uber.faq.databinding.FragmentFaqBinding
import br.com.digio.uber.faq.dataui.FaqQuestion
import br.com.digio.uber.faq.viewmodel.FaqViewModel
import br.com.digio.uber.util.Const.Extras.FAQ_QUESTION
import kotlinx.android.synthetic.main.faq_content.*
import kotlinx.android.synthetic.main.faq_search_bar.*
import org.koin.android.ext.android.inject

class FaqFragment : BNWFViewModelFragment<FragmentFaqBinding, FaqViewModel>() {

    override fun tag(): String = FaqFragment::class.java.name

    override val viewModel: FaqViewModel by inject()

    override fun autoSetColorStatusBar(): Boolean = true

    override val bindingVariable = BR.faqViewModel

    override val getLayoutId = R.layout.fragment_faq

    override fun getStatusBarAppearance(): StatusBarColor = BLACK

    private lateinit var adapter: FrequentQuestionsAdapter

    private var currentSearch = ""

    override fun initialize() {
        viewModel.updateFaq()
        setupClicks()
        recyclerview_questions.apply {
            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            isNestedScrollingEnabled = false
        }

        bnt_show_subjects.setOnClickListener {
            viewModel.showSubjects(true)
        }

        viewModel.showSubjectsLiveData.observe(
            this,
            Observer { show ->
                if (show) {
                    navigateToSubjects()
                    viewModel.showSubjects(false)
                }
            }
        )

        viewModel.questionListLiveData.observe(
            this
        ) { list ->
            adapter = FrequentQuestionsAdapter(list) { question ->
                navigateToQuestion(question)
            }
            recyclerview_questions.adapter = adapter
        }

        viewModel.blockUnlockQuestion.observe(this) { question ->
            if (question.isNotEmpty()) {
                binding?.faqContent?.btnBlockUnlock?.setOnClickListener {
                    navigateToQuestion(question[0])
                }
            }
        }

        viewModel.faqSearch.observe(
            this
        ) { list ->
            if (this::adapter.isInitialized) {
                if (list.isNotEmpty()) {
                    showSearch()
                    adapter.search(currentSearch, list)
                } else {
                    showNotFoundSearch()
                }
            }
        }

        initViews()
    }

    private fun setupClicks() {
        binding?.faqContent?.btnCardDelivery?.setOnClickListener {
            activity?.supportFragmentManager?.let {
                FaqShortcutDialogFragment.newInstance().show(it, null)
            }
        }
    }

    private fun navigateToQuestion(question: FaqQuestion) {
        navigateTo(FaqFragmentDirections.actionFaqFragmentToFaqQuestionFragment(), bundleOf(FAQ_QUESTION to question))
    }

    private fun navigateToSubjects() {
        navigateTo(FaqFragmentDirections.actionFaqFragmentToFaqSubjectsFragment())
    }

    private fun showNotFoundSearch() {
        recyclerview_questions.hide()
        textview_not_found.show()
    }

    private fun showSearch() {
        recyclerview_questions.show()
        textview_not_found.hide()
    }

    private fun initViews() {
        search_view.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(text: String?): Boolean {
                    currentSearch = text ?: ""
                    viewModel.getFrequentQuestionFilter(text)
                    return false
                }
            }
        )
    }
}