package br.com.digio.uber.faq.mapper

import br.com.digio.uber.faq.dataui.FaqQuestion
import br.com.digio.uber.model.faq.FaqQuestionResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class QuestionListMapper : AbstractMapper<List<FaqQuestionResponse>, List<FaqQuestion>> {

    override fun map(param: List<FaqQuestionResponse>): List<FaqQuestion> {
        val mapper = QuestionMapper()
        return param.map { mapper.map(it) }
    }
}