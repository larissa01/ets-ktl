package br.com.digio.uber.faq.adapter.items

import android.view.View
import br.com.digio.uber.faq.dataui.FaqSubject
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_subjects_open.view.*

class ClosedSubjectViewHolder(view: View, private val onClickListener: (FaqSubject, Int) -> Unit) :
    BaseViewHolder<FaqSubject>(view) {
    override fun bind(subject: FaqSubject) {
        itemView.apply {
            subject_title.text = subject.subject
            Glide.with(context).load(subject.icon).into(icon)
            setOnClickListener { onClickListener(subject, adapterPosition) }
        }
    }
}