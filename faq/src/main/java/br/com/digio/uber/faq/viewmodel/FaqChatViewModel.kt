package br.com.digio.uber.faq.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.faq.FaqDeviceInfoUseCase
import br.com.digio.uber.domain.usecase.faq.GetFaqChatUseCase
import br.com.digio.uber.model.faq.FaqChatUrlRequest
import br.com.digio.uber.model.request.DeviceInfo

class FaqChatViewModel(val faqDeviceInfoUseCase: FaqDeviceInfoUseCase, val faqChatUseCase: GetFaqChatUseCase) :
    BaseViewModel() {

    val url: MutableLiveData<String> = MutableLiveData()

    fun sendDeviceInfo(tag: String, networkType: String, identifier: String) {
        faqDeviceInfoUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                getChatLink(it, tag, networkType, identifier)
            },
            onCompletionBaseViewModel = {}
        )
    }

    private fun getChatLink(info: DeviceInfo, tag: String, networkType: String, identifier: String) {
        val faqRequest = FaqChatUrlRequest(
            appVersion = info.appVersion,
            carrier = info.brand,
            connectionType = networkType,
            countryCode = "BR",
            identifier = identifier,
            language = "US",
            nameIdentifier = "uber",
            osVersion = info.osVersion,
            phoneModel = info.deviceModel,
            platfom = "Android",
            tag = tag
        )

        faqChatUseCase(faqRequest).singleExec(
            onSuccessBaseViewModel = {
                url.value = it.url
            },
            onCompletionBaseViewModel = {}
        )
    }
}