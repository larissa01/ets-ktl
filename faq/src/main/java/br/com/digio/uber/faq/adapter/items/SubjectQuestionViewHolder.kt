package br.com.digio.uber.faq.adapter.items

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.faq.dataui.FaqQuestion
import kotlinx.android.synthetic.main.item_faq_frequent_questions.view.*

class SubjectQuestionViewHolder(view: View, val onItemClick: (FaqQuestion) -> Unit) : RecyclerView.ViewHolder(view) {
    fun bind(question: FaqQuestion) {
        itemView.apply {
            textview_question.text = question.title
            setOnClickListener { onItemClick(question) }
        }
    }
}