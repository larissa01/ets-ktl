package br.com.digio.uber.faq.dataui

data class FaqQuestionScreenState(
    val showWasUtil: Boolean,
    val showChatButton: Boolean,
    val showThanks: Boolean
)