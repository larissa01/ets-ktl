package br.com.digio.uber.faq.utils

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.BackgroundColorSpan
import android.text.style.TextAppearanceSpan
import android.widget.TextView
import androidx.core.content.ContextCompat
import br.com.digio.uber.faq.R
import br.com.digio.uber.util.unaccent

object StringFormatUtil {
    fun highlightTextPart(context: Context, textView: TextView, search: String) {
        val searchArray = search.split(" ").map { it.unaccent() }
        val fullText = textView.text.toString()
        val spannable: Spannable = SpannableString(fullText)
        searchArray.forEach {
            if (it.isNotEmpty()) {
                val length = it.length
                val resultsString = arrayListOf<Int>()

                var startPosition: Int = fullText.unaccent().indexOf(it, ignoreCase = true)
                while (startPosition >= 0) {
                    resultsString.add(startPosition)
                    startPosition = fullText.indexOf(it, startPosition + 1, ignoreCase = true)
                }

                val whiteColor = ColorStateList(arrayOf(intArrayOf()), intArrayOf(Color.WHITE))

                resultsString.forEach { startPos ->
                    if (spannable.length > 0) {
                        val endPos = startPos + length
                        val textAppearanceSpan = TextAppearanceSpan(null, Typeface.NORMAL, -1, whiteColor, null)
                        val backgroundColorSpan =
                            BackgroundColorSpan(ContextCompat.getColor(context, R.color.green_68d09f))
                        spannable.setSpan(textAppearanceSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                        spannable.setSpan(backgroundColorSpan, startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    }
                }
                textView.text = spannable
            }
        }
    }
}