package br.com.digio.uber.faq.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.faq.GetFaqSubjectsUseCase
import br.com.digio.uber.domain.usecase.faq.GetFrequentQuestionsUseCase
import br.com.digio.uber.domain.usecase.faq.GetQuestionByTagUseCase
import br.com.digio.uber.domain.usecase.faq.UpdateFaqUseCase
import br.com.digio.uber.faq.dataui.FaqQuestion
import br.com.digio.uber.faq.dataui.FaqSubject
import br.com.digio.uber.faq.mapper.QuestionListMapper
import br.com.digio.uber.faq.mapper.SubjectListMapper

class FaqViewModel(
    private val getFrequentQuestionUseCase: GetFrequentQuestionsUseCase,
    getFaqSubjectsUseCase: GetFaqSubjectsUseCase,
    private val updateFaqUseCase: UpdateFaqUseCase,
    getQuestionByTagUseCase: GetQuestionByTagUseCase
) : BaseViewModel() {

    val questionListLiveData: LiveData<List<FaqQuestion>> =
        getFrequentQuestionUseCase(null).exec().map(QuestionListMapper())

    val blockUnlockQuestion: LiveData<List<FaqQuestion>> =
        getQuestionByTagUseCase(TAG_BLOCK_UNLOCK).exec().map(QuestionListMapper())

    val faqSearch = MutableLiveData<List<FaqQuestion>>()

    val subjectsLiveData: LiveData<List<FaqSubject>> = getFaqSubjectsUseCase(Unit).exec().map(SubjectListMapper())

    val showSubjectsLiveData: MutableLiveData<Boolean> = MutableLiveData(false)

    var showShortcutBox: MutableLiveData<Boolean> = MutableLiveData(true)

    fun showSubjects(show: Boolean) {
        showSubjectsLiveData.value = show
    }

    fun updateFaq() {
        updateFaqUseCase(Unit).singleExec()
    }

    fun getFrequentQuestionFilter(text: String?) {
        showShortcutBox.value = text?.isEmpty() ?: true
        getFrequentQuestionUseCase(text).singleExec(
            onSuccessBaseViewModel = {
                faqSearch.value = QuestionListMapper().map(it)
            },
            showLoadingFlag = false
        )
    }

    companion object {
        private const val TAG_BLOCK_UNLOCK = "tag_bloqueio_desbloqueio"
    }
}