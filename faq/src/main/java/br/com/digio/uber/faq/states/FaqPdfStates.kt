package br.com.digio.uber.faq.states

import java.io.File

sealed class FaqPdfStates {
    class ShowPdf(val pdfFile: File?) : FaqPdfStates()
    object ErrorPdf : FaqPdfStates()
}