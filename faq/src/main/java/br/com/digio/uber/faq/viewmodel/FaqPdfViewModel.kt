package br.com.digio.uber.faq.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.faq.FaqPdfUseCase
import br.com.digio.uber.faq.states.FaqPdfStates
import br.com.digio.uber.faq.utils.FileUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.InputStream

class FaqPdfViewModel(private val faqPdfUseCase: FaqPdfUseCase) : BaseViewModel() {
    private val _pdfLiveData: MutableLiveData<FaqPdfStates> = MutableLiveData()
    val pdfLiveData: LiveData<FaqPdfStates> = _pdfLiveData

    fun getPdfs(file: File?) {
        faqPdfUseCase(Unit).singleExec(
            onSuccessBaseViewModel = { responsePdf ->
                if (responsePdf.isSuccess) {
                    file?.let { file ->
                        responsePdf.pdfInputStream?.let { inputStream ->
                            getFilePdf(inputStream, file)
                        }
                    }
                } else {
                    _pdfLiveData.value = FaqPdfStates.ErrorPdf
                }
            },
            onError = { _, error, _ ->
                error
            }
        )
    }

    fun getFilePdf(inputStream: InputStream, file: File) {
        viewModelScope.launch {
            val filePdf = withContext(Dispatchers.IO) {
                FileUtils.copyInputStreamToFile(inputStream, file)
                file
            }
            _pdfLiveData.value = FaqPdfStates.ShowPdf(filePdf)
        }
    }
}