package br.com.digio.uber.faq.fragment

import android.net.Uri
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.findNavController
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.faq.BR
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.databinding.FragmentFaqInitialBinding
import br.com.digio.uber.faq.viewmodel.FaqViewModel
import br.com.digio.uber.util.Const
import org.koin.android.ext.android.inject

class FaqInitialFragment : BaseViewModelFragment<FragmentFaqInitialBinding, FaqViewModel>() {
    override val bindingVariable = BR.faqViewModel
    override val getLayoutId = R.layout.fragment_faq_initial
    override val viewModel: FaqViewModel by inject()

    override fun getToolbar(): Toolbar? = binding?.faqAppbar?.faqToolbar

    override fun tag() = FaqInitialFragment::class.java.name

    override fun getStatusBarAppearance() = StatusBarColor.WHITE

    override fun initialize() {
        getToolbar()?.title = getString(R.string.attendance)
        viewModel.initializer { onItemClicked(it) }
    }

    override fun onNavigationClick(view: View) {
        findNavController().popBackStack()
    }

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.needHelpButton -> findNavController().navigate(Uri.parse(Const.DeepLink.DEEPLINK_FAQ_FRAGMENT))
            R.id.attendanceButton -> findNavController().navigate(Uri.parse(Const.DeepLink.DEEPLINK_FAQ_SAC))
        }
    }
}