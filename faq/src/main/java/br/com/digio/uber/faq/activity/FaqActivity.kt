package br.com.digio.uber.faq.activity

import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.di.faqViewModelModule

class FaqActivity : BaseActivity() {
    override fun getLayoutId(): Int? =
        R.layout.activity_faq

    override fun getModule() = listOf(faqViewModelModule)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val navHost = supportFragmentManager.findFragmentById(R.id.nav_host_faq) as NavHostFragment?
        val navController = navHost?.navController
        val navInflater = navController?.navInflater
        val graph = navInflater?.inflate(R.navigation.nav_faq)

        if (graph != null) {
            navController.graph = graph
        }
    }
}