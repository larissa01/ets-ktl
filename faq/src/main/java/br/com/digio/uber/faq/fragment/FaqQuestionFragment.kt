package br.com.digio.uber.faq.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.faq.BR
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.activity.FaqChatActivity
import br.com.digio.uber.faq.databinding.ActivityFaqQuestionBinding
import br.com.digio.uber.faq.dataui.FaqQuestion
import br.com.digio.uber.faq.utils.setFonts
import br.com.digio.uber.faq.viewmodel.FaqQuestionViewModel
import br.com.digio.uber.util.Const
import org.koin.android.viewmodel.ext.android.viewModel

class FaqQuestionFragment : BaseViewModelFragment<ActivityFaqQuestionBinding, FaqQuestionViewModel>() {

    override val getLayoutId: Int = R.layout.activity_faq_question
    override val bindingVariable: Int = BR.faqViewModel
    override val viewModel: FaqQuestionViewModel by viewModel()

    override fun tag(): String = FaqQuestionFragment::javaClass.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK
    override fun getToolbar(): Toolbar? = binding?.navToolbar
    override val showDisplayShowTitle = true

    override fun initialize() {
        super.initialize()

        binding?.navToolbar?.apply {
            setTitle(R.string.title_help)
        }

        setupObservers()
        setupListeners()
        getData()
    }

    private fun getData() {
        arguments?.getParcelable<FaqQuestion>(Const.Extras.FAQ_QUESTION)?.let {
            viewModel.setQuestion(it)
        }
    }

    private fun setupListeners() {
        binding?.btnStartChat?.setOnClickListener {
            viewModel.questionLiveData.value?.tag?.let {
                startActivity(FaqChatActivity.getIntent(requireContext(), it))
            }
        }
    }

    private fun setupObservers() {
        viewModel.questionLiveData.observe(
            this,
            Observer { question ->
                if (question.body != null) {
                    val internalFilePath = "file://" + requireActivity().filesDir.absolutePath + "/"

                    binding?.webviewQuestionBody?.loadDataWithBaseURL(
                        internalFilePath,
                        question.body.setFonts(),
                        HTML,
                        UTF8,
                        ""
                    )
                }
            }
        )
    }

    override fun onNavigationClick(view: View) {
        onBackPressed()
    }

    companion object {
        private const val HTML = "text/html; charset=utf-8"
        private const val UTF8 = "UTF-8"
    }
}