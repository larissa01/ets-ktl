package br.com.digio.uber.faq.dataui

data class FaqSubject(
    val id: String?,
    val subject: String?,
    val icon: String?,
    val client: String?,
    val questions: List<FaqQuestion>
)