package br.com.digio.uber.faq.di

import br.com.digio.uber.faq.viewmodel.FaqChatViewModel
import br.com.digio.uber.faq.viewmodel.FaqInitialViewModel
import br.com.digio.uber.faq.viewmodel.FaqPdfViewModel
import br.com.digio.uber.faq.viewmodel.FaqQuestionViewModel
import br.com.digio.uber.faq.viewmodel.FaqSubjectViewModel
import br.com.digio.uber.faq.viewmodel.FaqViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val faqViewModelModule = module {
    viewModel { FaqViewModel(get(), get(), get(), get()) }
    viewModel { FaqQuestionViewModel() }
    viewModel { FaqSubjectViewModel(get()) }
    viewModel { FaqInitialViewModel() }
    viewModel { FaqPdfViewModel(get()) }
    viewModel { FaqChatViewModel(get(), get()) }
}