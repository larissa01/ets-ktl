package br.com.digio.uber.faq.activity

import android.R.id
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.faq.BR
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.databinding.ActivityFaqChatBinding
import br.com.digio.uber.faq.di.faqViewModelModule
import br.com.digio.uber.faq.viewmodel.FaqChatViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.NetworkUtil
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module

class FaqChatActivity : BaseViewModelActivity<ActivityFaqChatBinding, FaqChatViewModel>() {

    override fun getLayoutId(): Int? = R.layout.activity_faq_chat
    override fun getModule(): List<Module>? = listOf(faqViewModelModule)
    override val bindingVariable: Int? = BR.faqViewModel
    override val viewModel: FaqChatViewModel by viewModel()
    override fun getToolbar(): Toolbar? = findViewById(R.id.nav_toolbar)
    override fun showDisplayShowTitle(): Boolean = true

    private val tag: String by lazy {
        checkNotNull(intent.extras?.getString(Const.RequestOnResult.FAQ.KEY_FAQ_CHAT))
    }

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)

        binding?.navToolbar?.setTitle(R.string.title_chat)
        setupObservers()
        sendDeviceInfo()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupObservers() {
        viewModel.url.observe(
            this,
            {
                binding?.faqChatWebview?.let { webView ->
                    webView.settings.javaScriptEnabled = true
                    webView.settings.javaScriptCanOpenWindowsAutomatically = true
                    webView.settings.domStorageEnabled = true
                    webView.loadUrl(it)
                    webView.webViewClient = object : WebViewClient() {
                        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                            return false
                        }
                    }
                }
            }
        )
    }

    private fun sendDeviceInfo() {
        tag?.let {
            viewModel.sendDeviceInfo(it, NetworkUtil.getConnectionType(this), packageName)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        fun getIntent(context: Context, tag: String?): Intent =
            Intent(context, FaqChatActivity::class.java).apply {
                putExtra(Const.RequestOnResult.FAQ.KEY_FAQ_CHAT, tag)
            }
    }
}