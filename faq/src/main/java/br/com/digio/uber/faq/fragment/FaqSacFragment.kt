package br.com.digio.uber.faq.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.faq.BR
import br.com.digio.uber.faq.R
import br.com.digio.uber.faq.activity.FaqDeepLinkActivity
import br.com.digio.uber.faq.databinding.FragmentFaqSacBinding
import br.com.digio.uber.faq.viewmodel.FaqInitialViewModel
import br.com.digio.uber.util.Const
import org.koin.android.ext.android.inject

class FaqSacFragment : BaseViewModelFragment<FragmentFaqSacBinding, FaqInitialViewModel>() {

    override val bindingVariable = BR.faqViewModel
    override val getLayoutId = R.layout.fragment_faq_sac
    override val viewModel: FaqInitialViewModel by inject()

    private val titleToolbar: String? by lazy {
        activity?.intent?.getStringExtra(Const.RequestOnResult.FaqSac.KEY_TOOLBAR_TITLE)
    }

    override fun getToolbar(): Toolbar? = binding?.faqAppbar?.faqToolbar

    override fun tag(): String = FaqSacFragment::class.java.name

    override fun getStatusBarAppearance() = StatusBarColor.WHITE

    override fun initialize() {
        getToolbar()?.title = titleToolbar ?: getString(R.string.attendance)
        binding?.faqOkButton?.setOnClickListener {
            goBack()
        }
    }

    override fun onNavigationClick(view: View) {
        goBack()
    }

    private fun goBack() {
        if (activity is FaqDeepLinkActivity) {
            activity?.finish()
        } else {
            pop()
        }
    }

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true
}