package br.com.digio.uber.cashback.ui.dialog

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.os.bundleOf
import br.com.digio.uber.cashback.databinding.CashbackMessageDialogBinding
import br.com.digio.uber.cashback.ui.model.CashbackMessageUiModel
import br.com.digio.uber.common.base.dialog.BaseDialog
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.safeHeritage
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog

class CashbackMessageDialog : BaseDialog() {

    private lateinit var binding: CashbackMessageDialogBinding

    private val cashbackDialog by lazy {
        arguments?.getSerializable(CASHBACK_DIALOG)?.safeHeritage<CashbackMessageUiModel>()
    }

    companion object {
        private const val CASHBACK_DIALOG = "cashbackDialog"

        fun newInstance(cashbackDialog: CashbackMessageUiModel) = CashbackMessageDialog().apply {
            arguments = bundleOf(
                CASHBACK_DIALOG to cashbackDialog
            )
        }
    }

    override fun tag(): String = CashbackMessageDialog::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun initialize() { /*nothgin*/ }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = CashbackMessageDialogBinding.inflate(inflater)
        binding.cashbackMessageUiModel = cashbackDialog

        binding.cashbackMessageBtn.setOnClickListener { dismiss() }

        return binding.root
    }

    override fun onStart() {
        super.onStart()

        dialog?.safeHeritage<BottomSheetDialog>()?.let { bottomSheetDialog ->
            bottomSheetDialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
                ?.let { bottomSheet ->
                    bottomSheet.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
                    bottomSheet.parent?.safeHeritage<View>()?.setBackgroundColor(Color.TRANSPARENT)
                }
        }
        view?.post {
            val parentView = view?.parent?.safeHeritage<View>()
            val bottomSheetBehavior =
                parentView?.layoutParams?.safeHeritage<CoordinatorLayout.LayoutParams>()
                    ?.behavior?.safeHeritage<BottomSheetBehavior<View>>()
            bottomSheetBehavior?.peekHeight = binding.cashbackMessageContent.measuredHeight
        }
    }
}