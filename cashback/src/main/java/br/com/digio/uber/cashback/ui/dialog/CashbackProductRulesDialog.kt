package br.com.digio.uber.cashback.ui.dialog

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.os.bundleOf
import br.com.digio.uber.cashback.databinding.CashbackRulesDialogBinding
import br.com.digio.uber.cashback.ui.fragment.CashbackStoreProductsFragmentDirections
import br.com.digio.uber.common.base.dialog.BaseDialog
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.cashback.CashbackOfferResponse
import br.com.digio.uber.util.safeHeritage
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog

class CashbackProductRulesDialog : BaseDialog() {

    private lateinit var binding: CashbackRulesDialogBinding

    private val product by lazy {
        arguments?.getSerializable(PRODUCT)?.safeHeritage<CashbackOfferResponse>()
    }

    override fun tag(): String = PRODUCT_RULES_DIALOG

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() { /*nothgin*/ }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = CashbackRulesDialogBinding.inflate(inflater)
        binding.product = product
        binding.cashbackRulesImgClose.setOnClickListener { dismiss() }
        binding.cashbackRulesTxtGoToStore.setOnClickListener {
            product?.url?.let {
                dismiss()
                navigateTo(
                    CashbackStoreProductsFragmentDirections
                        .actionCashbackStoreProductsFragmentToCashbackConfirmationFragment(it)
                )
            }
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()

        dialog?.safeHeritage<BottomSheetDialog>()?.let { bottomSheetDialog ->
            bottomSheetDialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
                ?.let { bottomSheet ->
                    bottomSheet.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
                    bottomSheet.parent?.safeHeritage<View>()?.setBackgroundColor(Color.TRANSPARENT)
                }
        }
        view?.post {
            val parentView = view?.parent?.safeHeritage<View>()
            val bottomSheetBehavior =
                parentView?.layoutParams?.safeHeritage<CoordinatorLayout.LayoutParams>()
                    ?.behavior?.safeHeritage<BottomSheetBehavior<View>>()
            bottomSheetBehavior?.peekHeight = binding.cashbackRulesContent.measuredHeight
        }
    }

    companion object {
        private const val PRODUCT = "product"
        private const val PRODUCT_RULES_DIALOG = "productRulesDialog"

        fun newInstance(product: CashbackOfferResponse) = CashbackProductRulesDialog().apply {
            arguments = bundleOf(
                PRODUCT to product
            )
        }
    }
}