package br.com.digio.uber.cashback.ui.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.ItemMyCashbackBinding
import br.com.digio.uber.common.extensions.hide
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.common.typealiases.OnClickCheckBoxToPurchase
import br.com.digio.uber.model.cashback.CashbackPurchase
import br.com.digio.uber.model.cashback.MyCashbackStatus
import br.com.digio.uber.util.FORMAT_US_DATE
import br.com.digio.uber.util.getDateBR
import br.com.digio.uber.util.safeHeritage
import br.com.digio.uber.util.toCalendar
import br.com.digio.uber.util.toMoneyOrEmpty

class CashbackPurchasesAdapter(
    private val status: MyCashbackStatus,
    private val listener: OnClickCheckBoxToPurchase
) : RecyclerView.Adapter<CashbackPurchasesAdapter.CashbackPurchasesViewHolder>(), AdapterItemsContract {

    private var purchase: List<CashbackPurchase> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CashbackPurchasesViewHolder {
        val binding =
            ItemMyCashbackBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CashbackPurchasesViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: CashbackPurchasesViewHolder, position: Int) {
        holder.bind(purchase[position], status, listener)
    }

    override fun getItemCount() = purchase.size

    override fun replaceItems(list: List<Any>) {
        purchase = list.safeHeritage()
        notifyDataSetChanged()
    }

    class CashbackPurchasesViewHolder(private val binding: ItemMyCashbackBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(purchase: CashbackPurchase, status: MyCashbackStatus, listener: OnClickCheckBoxToPurchase) {
            binding.purchase = purchase

            binding.itemStatusTxtValue.text = purchase.amount.toMoneyOrEmpty()
            binding.itemStatusCbSelect.setOnCheckedChangeListener { _, isChecked ->
                listener.invoke(purchase, isChecked)
            }

            when (status) {
                MyCashbackStatus.RELEASED -> {
                    binding.itemStatusImgSelect.hide()
                    binding.itemStatusTxtCashback.setTextColor(Color.BLACK)
                    binding.itemStatusTxtDetails.text =
                        purchase.transactionDate?.toCalendar(FORMAT_US_DATE)?.getDateBR()
                    binding.itemStatusTxtValue.setTextColor(ContextCompat.getColor(context, R.color.green_05A357))
                }
                MyCashbackStatus.WAITING_APPROVED -> {
                    binding.itemStatusCbSelect.hide()
                    binding.itemStatusImgSelect.hide()
                    binding.itemStatusTxtCashback.setTextColor(
                        ContextCompat.getColor(context, br.com.digio.uber.common.R.color.grey_757575)
                    )
                    binding.itemStatusTxtDetails.text = purchase.status
                    binding.itemStatusTxtDetails.setTextColor(Color.parseColor(purchase.color))
                    binding.itemStatusTxtValue.setTextColor(Color.parseColor(purchase.color))
                }
                MyCashbackStatus.RECEIVED -> {
                    binding.itemStatusCbSelect.hide()
                    binding.itemStatusTxtDetails.text =
                        purchase.transactionDate?.toCalendar(FORMAT_US_DATE)?.getDateBR()
                    binding.itemStatusTxtValue.setTextColor(ContextCompat.getColor(context, R.color.green_05A357))
                }
                MyCashbackStatus.CANCELED, MyCashbackStatus.LOST -> {
                    binding.itemStatusCbSelect.hide()
                    binding.itemStatusImgSelect.imageTintList =
                        ColorStateList.valueOf(
                            ContextCompat.getColor(context, br.com.digio.uber.common.R.color.grey_AFAFAF)
                        )
                    binding.itemStatusTxtDetails.text = purchase.status
                    binding.itemStatusTxtCashback.setTextColor(
                        ContextCompat.getColor(context, br.com.digio.uber.common.R.color.grey_AFAFAF)
                    )
                    binding.itemStatusTxtDetails.setTextColor(
                        ContextCompat.getColor(context, br.com.digio.uber.common.R.color.grey_AFAFAF)
                    )
                    binding.itemStatusTxtValue.setTextColor(
                        ContextCompat.getColor(context, br.com.digio.uber.common.R.color.grey_AFAFAF)
                    )
                }
            }
        }
    }
}