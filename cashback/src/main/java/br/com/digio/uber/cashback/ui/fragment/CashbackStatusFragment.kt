package br.com.digio.uber.cashback.ui.fragment

import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.cashback.BR
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.CashbackStatusFragmentBinding
import br.com.digio.uber.cashback.interfaces.MustFinish
import br.com.digio.uber.cashback.ui.adapter.CashbackPurchasesAdapter
import br.com.digio.uber.cashback.ui.viewmodel.CashbackStatusViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.hide
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.helper.PaginationListener
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.cashback.MyCashbackStatus
import org.koin.android.ext.android.inject

class CashbackStatusFragment :
    BaseViewModelFragment<CashbackStatusFragmentBinding, CashbackStatusViewModel>() {

    private var mustFinish: MustFinish? = null

    private val status by lazy { arguments?.getSerializable(STATUS) as MyCashbackStatus }

    override val bindingVariable: Int? = BR.cashbackStatusViewModel
    override val getLayoutId: Int? = R.layout.cashback_status_fragment
    override val viewModel: CashbackStatusViewModel? by inject()

    override fun tag(): String = CashbackStatusFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.GREY

    override fun initialize() {
        super.initialize()

        viewModel?.initializer {
            when (it.id) {
                R.id.cashback_status_btn_received -> sendCashbackAccount()

                R.id.cashback_status_btn_history -> {
                    navigateTo(
                        CashbackPurchasesFragmentDirections
                            .actionCashbackPurchasesFragmentToCashbackHistoryPurchasesFragment()
                    )
                }
            }
        }

        setupAdapter()

        setupObserver()

        selectAllListener()

        hideInfo()

        viewModel?.getPurchases(status)

        viewModel?.setLayoutShow(status)
    }

    private fun setupAdapter() {
        binding?.cashbackStatusRvPurchases?.apply {
            val linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            layoutManager = linearLayoutManager
            adapter = CashbackPurchasesAdapter(status) { purchase, checked ->
                viewModel?.checkUncheskPurchase(purchase, checked)
            }
            setupPaginationListener(linearLayoutManager)
        }
    }

    private fun setupPaginationListener(linearLayoutManager: LinearLayoutManager) {
        val paginationListener = object : PaginationListener(layoutManager = linearLayoutManager) {
            override var isLastPage: Boolean = viewModel?.isLastPage?.value == true
            override var isLoading: Boolean = viewModel?.loadingDownPage?.value == true

            override fun loadMoreItems() {
                viewModel?.page?.value = viewModel?.page?.value?.plus(1) ?: 0

                viewModel?.getPurchases(status)
            }
        }

        binding?.cashbackStatusRvPurchases?.addOnScrollListener(paginationListener)

        viewModel?.maxPages?.observe(
            this,
            Observer {
                it?.let { maxPages ->
                    paginationListener.pageSize = maxPages
                }
            }
        )

        viewModel?.isLastPage?.observe(
            this,
            Observer {
                it?.let { isLastPage ->
                    paginationListener.isLastPage = isLastPage
                }
            }
        )

        viewModel?.loadingDownPage?.observe(
            this,
            Observer {
                it?.let { isLoading ->
                    paginationListener.isLoading = isLoading
                }
            }
        )

        viewModel?.page?.observe(
            this,
            Observer {
                paginationListener.isLoading = true
            }
        )
    }

    private fun setupObserver() {
        viewModel?.isSendCashbackAccountSuccess?.observe(
            this,
            Observer {
                if (it) {
                    navigateTo(
                        CashbackPurchasesFragmentDirections.actionCashbackPurchasesFragmentToCashbackSuccessFragment()
                    )
                }
            }
        )
    }

    private fun selectAllListener() {
        binding?.cashbackStatusSelectAll?.setOnCheckedChangeListener { _, isChecked ->
            viewModel?.selectAllPurchases(isChecked)
        }
    }

    private fun hideInfo() {
        binding?.cashbackStatusAlertLayout?.cashbackTopImgClose?.setOnClickListener {
            viewModel?.alertHide?.value = true
            binding?.cashbackStatusAlertContent?.hide()
        }
    }

    private fun sendCashbackAccount() {
        viewModel?.sendCashbackAccount()
    }

    override fun onBackPressed() {
        mustFinish?.mustFinish()
    }

    companion object {
        const val STATUS = "status"

        fun newInstance(status: MyCashbackStatus, mustFinish: MustFinish): CashbackStatusFragment =
            CashbackStatusFragment().apply {
                arguments = bundleOf(
                    STATUS to status
                )
                this.mustFinish = mustFinish
            }
    }
}