package br.com.digio.uber.cashback.ui.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.cashback.BR
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.CashbackStoreProductsFragmentBinding
import br.com.digio.uber.cashback.ui.adapter.CashbackOffersAdapter
import br.com.digio.uber.cashback.ui.dialog.CashbackComissionCategoryDialog
import br.com.digio.uber.cashback.ui.dialog.CashbackProductRulesDialog
import br.com.digio.uber.cashback.ui.viewmodel.CashbackStoreProductsViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.safeHeritage
import org.koin.android.ext.android.inject

class CashbackStoreProductsFragment :
    BaseViewModelFragment<CashbackStoreProductsFragmentBinding, CashbackStoreProductsViewModel>() {

    private val mustFinish: Boolean? by lazy {
        arguments?.getBoolean(Const.RequestOnResult.Cashback.MUST_FINISH)
    }

    private val store: CashbackStore? by lazy {
        arguments?.getSerializable(Const.RequestOnResult.Cashback.STORE)?.safeHeritage<CashbackStore>()
    }

    override val bindingVariable: Int? = BR.cashbackStoreProductsViewModel
    override val getLayoutId: Int? = R.layout.cashback_store_products_fragment
    override val viewModel: CashbackStoreProductsViewModel? by inject()

    override fun tag(): String = CashbackStoreProductsFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun getToolbar(): Toolbar? = binding?.cashbackStoreProductsAppBar?.navToolbar.apply {
        this?.title = getString(R.string.partner_store)
        this?.navigationIcon =
            ContextCompat.getDrawable(requireContext(), br.com.digio.uber.common.R.drawable.ic_back_arrow_black)
        this?.setBackgroundColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.white))
        this?.setTitleTextColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.black))
    }

    override val showDisplayShowTitle = true

    override val showHomeAsUp = true

    override fun onNavigationClick(view: View) {
        onBackPressed()
    }

    override fun initialize() {
        super.initialize()

        setupAdapter()
        viewModel?.initializer {
            when (it.id) {
                R.id.cashback_store_products_btn_see_all_products -> {
                    store?.url?.let { url ->
                        goToConfirmationScreen(url)
                    }
                }
                R.id.cashback_store_products_txt_category -> {
                    activity?.supportFragmentManager?.let {
                        store?.id?.let { id -> CashbackComissionCategoryDialog.newInstance(id).show(it, null) }
                    }
                }
            }
        }

        store?.let {
            viewModel?.setStoreData(it)

            viewModel?.getStoreOffers(it.id)
        }
    }

    private fun setupAdapter() {
        binding?.cashbackStoreProductsRvStores?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = CashbackOffersAdapter(
                goToStore = { url ->
                    url?.let {
                        goToConfirmationScreen(it)
                    }
                },
                goToRules = { product ->
                    activity?.supportFragmentManager?.let {
                        CashbackProductRulesDialog.newInstance(product).show(it, null)
                    }
                }
            )
        }
    }

    private fun goToConfirmationScreen(url: String) {
        navigateTo(
            CashbackStoreProductsFragmentDirections.actionCashbackStoreProductsFragmentToCashbackConfirmationFragment(
                url
            )
        )
    }

    override fun onBackPressed() {
        if (mustFinish == true) {
            activity?.finish()
        } else {
            pop()
        }
    }
}