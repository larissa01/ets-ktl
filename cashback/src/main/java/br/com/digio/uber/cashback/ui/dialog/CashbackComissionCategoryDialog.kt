package br.com.digio.uber.cashback.ui.dialog

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.cashback.BR
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.CashbackCategoryDialogBinding
import br.com.digio.uber.cashback.ui.adapter.CashbackComissionAdapter
import br.com.digio.uber.cashback.ui.viewmodel.CashbackComissionCategoryViewModel
import br.com.digio.uber.common.base.dialog.BNWFViewModelDialog
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.safeHeritage
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.koin.android.ext.android.inject

class CashbackComissionCategoryDialog :
    BNWFViewModelDialog<CashbackCategoryDialogBinding, CashbackComissionCategoryViewModel>() {

    private val id by lazy {
        arguments?.getLong(ID)
    }

    override val viewModel: CashbackComissionCategoryViewModel by inject()

    override val bindingVariable = BR.cashbackComissionCategoryViewModel

    override val getLayoutId = R.layout.cashback_category_dialog

    override fun tag(): String = CashbackComissionCategoryDialog::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun initialize() {
        super.initialize()

        setupAdapter()

        setObserver()

        id?.let { viewModel.getComissionCategory(it) }

        binding?.cashbackCategoryImgClose?.setOnClickListener { dismiss() }
    }

    private fun setupAdapter() {
        binding?.cashbackCategoryRvComission?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = CashbackComissionAdapter()
        }
    }

    private fun setObserver() {
        viewModel.categories.observe(
            this,
            Observer {
                dialog?.safeHeritage<BottomSheetDialog>()?.let { bottomSheetDialog ->
                    bottomSheetDialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)
                        ?.let { bottomSheet ->
                            bottomSheet.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
                            bottomSheet.parent?.safeHeritage<View>()?.setBackgroundColor(Color.TRANSPARENT)
                        }
                }
                view?.post {
                    val parentView = view?.parent?.safeHeritage<View>()
                    val bottomSheetBehavior =
                        parentView?.layoutParams?.safeHeritage<CoordinatorLayout.LayoutParams>()
                            ?.behavior?.safeHeritage<BottomSheetBehavior<View>>()
                    bottomSheetBehavior?.peekHeight = binding?.cashbackCategoryContent?.measuredHeight ?: 0
                }
            }
        )
    }

    companion object {
        private const val ID = "id"

        fun newInstance(id: Long) = CashbackComissionCategoryDialog().apply {
            arguments = bundleOf(
                ID to id
            )
        }
    }
}