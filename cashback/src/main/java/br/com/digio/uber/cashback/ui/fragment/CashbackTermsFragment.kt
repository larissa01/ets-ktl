package br.com.digio.uber.cashback.ui.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import br.com.digio.uber.cashback.BR
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.CashbackTermsFragmentBinding
import br.com.digio.uber.cashback.ui.viewmodel.CashbackTermsViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const
import org.koin.android.ext.android.inject

class CashbackTermsFragment : BaseViewModelFragment<CashbackTermsFragmentBinding, CashbackTermsViewModel>() {

    override val bindingVariable: Int? = BR.cashbackTermsViewModel
    override val getLayoutId: Int? = R.layout.cashback_terms_fragment
    override val viewModel: CashbackTermsViewModel? by inject()

    override fun tag(): String = CashbackTermsFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun getToolbar(): Toolbar? = binding?.cashbackTermsAppBar?.navToolbar.apply {
        this?.title = getString(R.string.terms_and_conditions)
        this?.navigationIcon =
            context?.let { ContextCompat.getDrawable(it, br.com.digio.uber.common.R.drawable.ic_back_arrow_black) }
        this?.setBackgroundColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.white))
        this?.setTitleTextColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.black))
    }

    override val showDisplayShowTitle = true

    override val showHomeAsUp = true

    override fun initialize() {
        super.initialize()

        setupViewModel()

        scroolListener()

        viewModel?.setVisibilityButton(arguments?.getBoolean(Const.RequestOnResult.Cashback.BUTTON_VISIBLE))
    }

    private fun setupViewModel() {
        viewModel?.initializer { view ->
            when (view.id) {
                R.id.cashback_terms_btn_continue -> confirmTerms()
            }
        }

        viewModel?.acceptTerms?.observe(
            this,
            Observer {
                if (it) {
                    navigateTo(R.id.call_home, bundleOf())
                }
            }
        )
    }

    private fun scroolListener() {
        binding?.cashbackTermsScrollView
            ?.setOnScrollChangeListener { nested: NestedScrollView?, _: Int, scrollY: Int, _: Int, _: Int ->
                val scrollViewHeight =
                    nested?.getChildAt(0)?.bottom?.toFloat()?.minus(nested.height.toFloat()) ?: 0f

                viewModel?.nestedScrollView(scrollViewHeight, scrollY)
            }
    }

    private fun confirmTerms() {
        viewModel?.confirmTerms()
    }

    override fun onNavigationClick(view: View) {
        pop()
    }
}