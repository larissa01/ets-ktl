package br.com.digio.uber.cashback.ui.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class CashbackPagerAdapter(
    fm: Fragment,
    private val fragments: List<Fragment>,
    val context: Context
) : FragmentStateAdapter(fm) {

    override fun getItemCount(): Int = fragments.size

    override fun createFragment(position: Int): Fragment = fragments[position]
}