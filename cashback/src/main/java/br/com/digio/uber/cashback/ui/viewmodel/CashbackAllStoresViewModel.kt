package br.com.digio.uber.cashback.ui.viewmodel

import android.text.Editable
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.cashback.ui.model.CashbackStoresCategoriesUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.cashback.GetCashbackStoresUseCase
import br.com.digio.uber.model.cashback.CashbackStore

class CashbackAllStoresViewModel constructor(
    private val getCashbackStoresUseCase: GetCashbackStoresUseCase
) : BaseViewModel() {

    val searchText: MutableLiveData<Editable> = MutableLiveData()
    val storeSearch: MutableLiveData<String> = MutableLiveData()

    private val allCategories: MutableLiveData<ArrayList<String>> = MutableLiveData()

    val listStoreCashback: MutableLiveData<List<CashbackStore>> = MutableLiveData()
    private val allStores: MutableLiveData<List<CashbackStore>> = MutableLiveData()
    private val filteredStores: MutableLiveData<List<CashbackStore>> = MutableLiveData()

    fun getCashbackStores(totalElements: Int?, categories: List<CashbackStoresCategoriesUiModel>?) {
        getCashbackStoresUseCase(totalElements ?: 0).singleExec(
            onError = { _, error, _ ->
                error
            },
            onSuccessBaseViewModel = { stores ->
                allStores.value = stores.content.sortedBy { it.category?.name }

                setAllCategories()

                if (categories != null) {
                    listStoreCashback.value = filterStores(categories)
                } else {
                    listStoreCashback.value = stores.content.sortedBy { it.category?.name }
                }

                filteredStores.value = listStoreCashback.value
            }
        )
    }

    private fun setAllCategories() {
        val categories = arrayListOf<String>()

        allStores.value?.groupBy { it.category?.name }?.keys?.toList()?.forEach { categories.add(it.toString()) }

        allCategories.value = categories
    }

    private fun filterStores(categories: List<CashbackStoresCategoriesUiModel>?) =
        if (categories.isNullOrEmpty() || categories.all { !it.selected }) {
            allStores.value?.toMutableList()?.sortedBy { it.category?.name }
        } else {
            allStores.value?.filter { item ->
                item.category?.name in categories.filter { it.selected }.map { it.category }
            }?.sortedBy { it.category?.name }
        }

    fun getCategories(categories: List<CashbackStoresCategoriesUiModel>?) =
        if (categories.isNullOrEmpty()) {
            val categoriesSelected = arrayListOf<CashbackStoresCategoriesUiModel>()
            allCategories.value?.forEach {
                categoriesSelected.add(CashbackStoresCategoriesUiModel(it))
            }
            categoriesSelected.toTypedArray()
        } else {
            categories.toTypedArray()
        }

    fun filterStoresBySearchView(editable: Editable) {
        val listStores: MutableList<CashbackStore>? = mutableListOf<CashbackStore>()

        filteredStores.value?.forEach {
            if (it.storeName?.toLowerCase()?.contains(editable.toString().toLowerCase()) == true) {
                listStores?.add(it)
            }
        }
        listStoreCashback.value = listStores?.sortedBy { it.category?.name }
    }
}