package br.com.digio.uber.cashback.ui.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CashbackStoresCategoriesUiModel(
    var category: String,
    var selected: Boolean = false
) : Parcelable