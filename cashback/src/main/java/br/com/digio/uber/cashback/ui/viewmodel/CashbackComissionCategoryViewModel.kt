package br.com.digio.uber.cashback.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.cashback.GetComissionCategoryUseCase
import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.cashback.CashbackComissionCategory

class CashbackComissionCategoryViewModel constructor(
    private val getComissionCategoryUseCase: GetComissionCategoryUseCase
) : BaseViewModel() {

    val categories: MutableLiveData<ResponseList<CashbackComissionCategory>> = MutableLiveData()
    val emptyListHide: MutableLiveData<Boolean> = MutableLiveData(false)

    fun getComissionCategory(id: Long) {
        getComissionCategoryUseCase(id).singleExec(
            onError = { _, error, _ ->
                emptyListHide.value = true
                error
            },
            onSuccessBaseViewModel = { comission ->
                categories.value = comission
                emptyListHide.value = comission.content.isNullOrEmpty()
            }
        )
    }
}