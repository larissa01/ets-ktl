package br.com.digio.uber.cashback.ui.fragment

import android.view.View
import br.com.digio.uber.cashback.BR
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.CashbackReceiveSuccessFragmentBinding
import br.com.digio.uber.cashback.ui.viewmodel.CashbackSuccessViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import org.koin.android.ext.android.inject

class CashbackSuccessFragment :
    BaseViewModelFragment<CashbackReceiveSuccessFragmentBinding, CashbackSuccessViewModel>() {

    override val bindingVariable: Int? = BR.cashbackSuccessViewModel
    override val getLayoutId: Int? = R.layout.cashback_receive_success_fragment
    override val viewModel: CashbackSuccessViewModel? by inject()

    override fun tag(): String = CashbackSuccessFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun onNavigationClick(view: View) {
        pop()
    }

    override fun initialize() {
        viewModel?.initializer {
            when (it.id) {
                R.id.cashback_receive_success_btn_received -> activity?.finish()
            }
        }
    }
}