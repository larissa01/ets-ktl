package br.com.digio.uber.cashback.ui.fragment

import android.annotation.SuppressLint
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.cashback.BR
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.CashbackAllStoresFragmentBinding
import br.com.digio.uber.cashback.ui.adapter.CashbackStoresAdapter
import br.com.digio.uber.cashback.ui.model.CashbackStoresCategoriesUiModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackAllStoresViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.RequestOnResult.Cashback.ALL_CATEGORIES
import br.com.digio.uber.util.Const.RequestOnResult.Cashback.TOTAL_ELEMENTS
import br.com.digio.uber.util.safeHeritage
import br.com.digio.uber.util.safeLet
import org.koin.android.ext.android.inject

class CashbackAllStoresFragment :
    BaseViewModelFragment<CashbackAllStoresFragmentBinding, CashbackAllStoresViewModel>() {

    private val mustFinish: Boolean? by lazy {
        arguments?.getBoolean(Const.RequestOnResult.Cashback.MUST_FINISH)
    }

    private val totalElements: Int? by lazy {
        arguments?.getInt(TOTAL_ELEMENTS)
    }

    private val allCategories by lazy {
        arguments?.getParcelableArray(ALL_CATEGORIES)?.toList()?.safeHeritage<CashbackStoresCategoriesUiModel>()
            ?.toMutableList()
    }

    override val bindingVariable: Int? = BR.cashbackAllStoresViewModel
    override val getLayoutId: Int? = R.layout.cashback_all_stores_fragment
    override val viewModel: CashbackAllStoresViewModel? by inject()

    override fun tag(): String = CashbackHomeFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun getToolbar(): Toolbar? = binding?.cashbackAllStoresAppBar?.navToolbar.apply {
        this?.title = getString(R.string.partner_stores)
        this?.navigationIcon =
            ContextCompat.getDrawable(requireContext(), br.com.digio.uber.common.R.drawable.ic_back_arrow_black)
        this?.setBackgroundColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.white))
        this?.setTitleTextColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.black))
    }

    override val showDisplayShowTitle = true

    override val showHomeAsUp = true

    override val menu = R.menu.cashback_store_filter_menu

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.cashback_store_filter_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.store_filter -> {
                safeLet(viewModel?.getCategories(allCategories), totalElements) { categories, elements ->
                    navigateTo(
                        CashbackAllStoresFragmentDirections.actionCashbackAllStoresFragmentToCashbackFilterFragment(),
                        Bundle().apply {
                            putParcelableArray(ALL_CATEGORIES, categories)
                            putInt(TOTAL_ELEMENTS, elements)
                        }
                    )
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun initialize() {
        super.initialize()

        setupAdapter()

        setTouchListener()

        setUpObservers()

        viewModel?.getCashbackStores(totalElements, allCategories)
    }

    private fun setupAdapter() {
        binding?.cashbackAllStoresRvCashback?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = CashbackStoresAdapter() { store ->
                navigateTo(
                    CashbackAllStoresFragmentDirections.actionCashbackAllStoresFragmentToCashbackStoreProductsFragment(
                        store
                    )
                )
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setTouchListener() {
        binding?.cashbackAllStoresEtSearch?.setOnTouchListener(
            View.OnTouchListener { v, event ->
                when (event.action) {
                    MotionEvent.ACTION_UP -> {
                        binding?.cashbackAllStoresEtSearch?.compoundDrawables?.get(2)?.let { drawableRight ->
                            if (event.rawX >=
                                binding?.cashbackAllStoresEtSearch?.right?.minus(drawableRight.bounds.width()) ?: 1
                            ) {
                                binding?.cashbackAllStoresEtSearch?.text?.clear()

                                return@OnTouchListener true
                            }
                        }
                        v.performClick()
                    }
                    else -> {
                    }
                }
                return@OnTouchListener false
            }
        )
    }

    private fun setUpObservers() {
        viewModel?.searchText?.observe(
            this,
            Observer { text ->
                viewModel?.filterStoresBySearchView(text)

                if (binding?.cashbackAllStoresEtSearch?.text.isNullOrEmpty()) {
                    binding?.cashbackAllStoresEtSearch?.setCompoundDrawablesWithIntrinsicBounds(
                        InsetDrawable(
                            ContextCompat.getDrawable(requireContext(), R.drawable.ic_cashback_search),
                            ICON_CLOSE_WITH_PADDING,
                            ICON_CLOSE_WITH_PADDING,
                            ICON_CLOSE_WITH_PADDING,
                            ICON_CLOSE_WITH_PADDING
                        ),
                        null,
                        null,
                        null
                    )
                } else {
                    binding?.cashbackAllStoresEtSearch?.setCompoundDrawablesWithIntrinsicBounds(
                        null,
                        null,
                        InsetDrawable(
                            ContextCompat.getDrawable(requireContext(), R.drawable.ic_close_cashback),
                            ICON_CLOSE_WITH_PADDING,
                            ICON_CLOSE_WITH_PADDING,
                            ICON_CLOSE_WITH_PADDING,
                            ICON_CLOSE_WITH_PADDING
                        ),
                        null
                    )
                }
            }
        )
    }

    override fun onNavigationClick(view: View) {
        onBackPressed()
    }

    override fun onBackPressed() {
        if (mustFinish == true) {
            activity?.finish()
        } else {
            pop()
        }
    }

    companion object {
        const val ICON_CLOSE_WITH_PADDING = 5
    }
}