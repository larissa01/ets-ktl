package br.com.digio.uber.cashback.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.domain.usecase.cashback.AcceptCashbackTermsUseCase
import br.com.digio.uber.domain.usecase.cashback.GetCashbackTermsUseCase
import br.com.digio.uber.model.cashback.CashbackTerms

class CashbackTermsViewModel constructor(
    private val getCashbackTermsUseCase: GetCashbackTermsUseCase,
    private val acceptCashbackTermsUseCase: AcceptCashbackTermsUseCase
) : BaseViewModel() {

    val terms: LiveData<CashbackTerms> = getCashbackTermsUseCase(Unit).exec(
        showLoadingFlag = true,
        onError = { _, _, _ -> EmptyErrorObject }
    )
    val enableButton = MutableLiveData<Boolean>().apply {
        value = false
    }

    val buttonVisible = MutableLiveData<Boolean>().apply {
        value = true
    }

    val acceptTerms = MutableLiveData<Boolean>()

    fun setVisibilityButton(visible: Boolean?) {
        buttonVisible.value = visible
    }

    fun nestedScrollView(scrollViewHeight: Float, scrollY: Int) {
        val percent = (scrollY.toFloat() / scrollViewHeight)
        if (percent == 1f) {
            enableButton.value = true
        }
    }

    fun confirmTerms() {
        acceptCashbackTermsUseCase(Unit).singleExec(
            onError = { _, error, _ ->
                error
            },
            onSuccessBaseViewModel = {
                acceptTerms.value = true
            }
        )
    }
}