package br.com.digio.uber.cashback.interfaces

interface MustFinish {
    fun mustFinish()
}