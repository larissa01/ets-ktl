package br.com.digio.uber.cashback.ui.viewmodel

import android.text.SpannableString
import android.text.Spanned
import android.text.style.UnderlineSpan
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.cashback.R
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.cashback.GetCashbackDetailsUseCase

class CashbackConfirmationViewModel constructor(
    private val getCashbackDetailsUseCase: GetCashbackDetailsUseCase,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    val textDetails: MutableLiveData<String> = MutableLiveData()
    val txtTerms: MutableLiveData<SpannableString> = MutableLiveData()

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        getText()
    }

    private fun getText() {
        getCashbackDetailsUseCase(TEXT_DETAIL).singleExec(
            onError = { _, error, _ ->
                error
            },
            onSuccessBaseViewModel = { text ->
                textDetails.value = text.text
                txtTerms.value = SpannableString(resourceManager.getString(R.string.see_terms_and_conditions)).apply {
                    setSpan(UnderlineSpan(), 0, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                }
            }
        )
    }

    companion object {
        const val TEXT_DETAIL = "TEXT_REDIRECT_PURCHASE"
    }
}