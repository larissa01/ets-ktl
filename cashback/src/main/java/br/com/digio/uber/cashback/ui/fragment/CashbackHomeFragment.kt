package br.com.digio.uber.cashback.ui.fragment

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.cashback.BR
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.CashbackHomeFragmentBinding
import br.com.digio.uber.cashback.ui.adapter.CashbackTopStoresAdapter
import br.com.digio.uber.cashback.ui.dialog.CashbackMessageDialog
import br.com.digio.uber.cashback.ui.model.CashbackMessageUiModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackHomeViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const
import org.koin.android.ext.android.inject

class CashbackHomeFragment : BaseViewModelFragment<CashbackHomeFragmentBinding, CashbackHomeViewModel>() {

    override val bindingVariable: Int? = BR.cashbackHomeViewModel
    override val getLayoutId: Int? = R.layout.cashback_home_fragment
    override val viewModel: CashbackHomeViewModel? by inject()

    override fun tag(): String = CashbackHomeFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun getToolbar(): Toolbar? = binding?.cashbackHomeAppBar?.navToolbar.apply {
        this?.title = getString(R.string.cashback)
        this?.navigationIcon =
            ContextCompat.getDrawable(requireContext(), br.com.digio.uber.common.R.drawable.ic_back_arrow_black)
        this?.setBackgroundColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.white))
        this?.setTitleTextColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.black))
    }

    override val showDisplayShowTitle = true

    override val showHomeAsUp = true

    override val menu = R.menu.cashback_doubt_menu

    override fun onNavigationClick(view: View) {
        super.onNavigationClick(view)
        onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.cashback_doubt_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.cashback_doubt -> {
                showDoubtMessanger()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun initialize() {
        super.initialize()

        setupAdapter()

        viewModel?.initializer { view ->
            when (view.id) {
                R.id.cashback_home_btn_see_all_stores -> {
                    navigateTo(
                        CashbackHomeFragmentDirections.actionCashbackHomeFragmentToCashbackAllStoresFragment(),
                        Bundle().apply {
                            viewModel?.getTotalElements()?.let {
                                putInt(Const.RequestOnResult.Cashback.TOTAL_ELEMENTS, it)
                            }
                        }
                    )
                }

                R.id.cashback_home_btn_rescue_cashback -> {
                    navigateTo(
                        CashbackHomeFragmentDirections.actionCashbackHomeFragmentToCashbackPurchasesFragment(),
                        Bundle().apply {
                            putBoolean(Const.RequestOnResult.Cashback.MUST_FINISH, false)
                        }
                    )
                }
            }
        }
    }

    private fun setupAdapter() {
        binding?.cashbackHomeRvStores?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = CashbackTopStoresAdapter() {
                navigateTo(
                    CashbackHomeFragmentDirections.actionCashbackHomeFragmentToCashbackStoreProductsFragment(
                        it
                    )
                )
            }
        }
    }

    private fun showDoubtMessanger() {
        activity?.supportFragmentManager?.let {
            CashbackMessageDialog.newInstance(
                CashbackMessageUiModel(
                    getString(R.string.doubt_cashback_title),
                    getString(R.string.doubt_home_text),
                    getString(R.string.understand)
                )
            ).show(it, null)
        }
    }

    override fun onBackPressed() {
        activity?.finish()
    }
}