package br.com.digio.uber.cashback.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import br.com.digio.uber.cashback.BR
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.CashbackConfirmationFragmentBinding
import br.com.digio.uber.cashback.ui.viewmodel.CashbackConfirmationViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.openExternalUrl
import org.koin.android.ext.android.inject

class CashbackConfirmationFragment :
    BaseViewModelFragment<CashbackConfirmationFragmentBinding, CashbackConfirmationViewModel>() {

    private val url: String? by lazy {
        arguments?.getString(Const.RequestOnResult.Cashback.URL_STORE)
    }

    override val bindingVariable: Int? = BR.cashbackConfirmationViewModel
    override val getLayoutId: Int? = R.layout.cashback_confirmation_fragment
    override val viewModel: CashbackConfirmationViewModel? by inject()

    override fun tag(): String = CashbackConfirmationFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun getToolbar(): Toolbar? = binding?.cashbackConfirmationAppBar?.navToolbar.apply {
        this?.title = getString(R.string.partner_store)
        this?.navigationIcon =
            ContextCompat.getDrawable(requireContext(), br.com.digio.uber.common.R.drawable.ic_back_arrow_black)
        this?.setBackgroundColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.white))
        this?.setTitleTextColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.black))
    }

    override val showDisplayShowTitle = true

    override val showHomeAsUp = true

    override fun onNavigationClick(view: View) {
        pop()
    }

    override fun initialize() {
        viewModel?.initializer { view ->
            when (view.id) {
                R.id.cashback_confirmation_btn_continue -> goToWebView()
                R.id.cashback_confirmation_txt_terms -> goToTerms()
            }
        }
    }

    private fun goToWebView() {
        url?.let { context?.openExternalUrl(it) }
    }

    private fun goToTerms() {
        navigateTo(
            CashbackConfirmationFragmentDirections.actionCashbackConfirmationFragmentToCashbackTermsFragment(),
            Bundle().apply {
                putBoolean(Const.RequestOnResult.Cashback.BUTTON_VISIBLE, false)
            }
        )
    }
}