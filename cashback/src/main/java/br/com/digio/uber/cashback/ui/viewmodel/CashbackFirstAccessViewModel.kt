package br.com.digio.uber.cashback.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.cashback.GetCashbackTermsAcceptUseCase

class CashbackFirstAccessViewModel constructor(
    private val getCashbackTermsAcceptUseCase: GetCashbackTermsAcceptUseCase
) : BaseViewModel() {
    val termsAccept: MutableLiveData<Boolean> = MutableLiveData()
    val hideContent: MutableLiveData<Boolean> = MutableLiveData(false)

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)

        getCashbackTermsAcceptUseCase(Unit).singleExec(
            onError = { _, error, _ ->
                termsAccept.value = false
                hideContent.value = true
                error
            },
            onSuccessBaseViewModel = {
                termsAccept.value = it.isSuccessful
                hideContent.value = !it.isSuccessful
            }
        )
    }
}