package br.com.digio.uber.cashback.ui.viewmodel

import android.graphics.Color
import android.graphics.drawable.Drawable
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.cashback.R
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.usecase.cashback.GetCashbackOffersUseCase
import br.com.digio.uber.model.cashback.CashbackOfferResponse
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.model.cashback.Tiers
import br.com.digio.uber.util.convertToPercent

class CashbackStoreProductsViewModel constructor(
    private val getCashbackOffersUseCase: GetCashbackOffersUseCase,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    val products: MutableLiveData<List<CashbackOfferResponse>> = MutableLiveData()
    val storeData: MutableLiveData<CashbackStore> = MutableLiveData()

    val bgTopColor: MutableLiveData<Int> = MutableLiveData()
    val tier: MutableLiveData<String> = MutableLiveData()
    val tierColor: MutableLiveData<Int> = MutableLiveData()
    val cashback: MutableLiveData<String> = MutableLiveData()
    val nextCashback: MutableLiveData<String> = MutableLiveData()
    val emptyListHide: MutableLiveData<Boolean> = MutableLiveData(false)
    val visibleView: MutableLiveData<Boolean> = MutableLiveData(true)
    val iconTierTop: MutableLiveData<Drawable> = MutableLiveData()
    val textColorTierTop: MutableLiveData<Int> = MutableLiveData()

    fun setStoreData(store: CashbackStore) {
        storeData.value = store
        tier.value = store.storeTierStoreInfo?.tierCode?.name
        tierColor.value = Color.parseColor(store.storeTierStoreInfo?.tierColor)
        bgTopColor.value = Color.parseColor(store.storeTierStoreInfo?.tierColor)

        iconTierTop.value = topIconTier(store.storeTierStoreInfo?.tierCode)
        textColorTierTop.value = topTextColor(store.storeTierStoreInfo?.tierCode)

        nextCashback.value = String.format(
            resourceManager.getString(R.string.at_the_next_level_you_will_have_up_to_cashback),
            store.nextStoreTierStoreInfo?.maxTierCustomerCommission?.convertToPercent()
        )

        if (store.storeTierStoreInfo?.tierCode == Tiers.TIER_4) {
            cashback.value = String.format(
                resourceManager.getString(
                    R.string.congratulations_you_reached_the_maximum_level_you_have_intil_cashback
                ),
                store.storeTierStoreInfo?.maxTierCustomerCommission?.convertToPercent()
            )

            visibleView.value = false
        } else {
            cashback.value = String.format(
                resourceManager.getString(R.string.you_have_intil_cashback),
                store.storeTierStoreInfo?.maxTierCustomerCommission?.convertToPercent()
            )
        }
    }

    private fun topIconTier(tier: Tiers?): Drawable? =
        when (tier) {
            Tiers.TIER_1 -> resourceManager.getDrawable(R.drawable.ic_cashback_top_tier_blue)
            Tiers.TIER_2 -> resourceManager.getDrawable(R.drawable.ic_cashback_top_tier_gold)
            Tiers.TIER_3 -> resourceManager.getDrawable(R.drawable.ic_cashback_top_tier_platina)
            Tiers.TIER_4 -> resourceManager.getDrawable(R.drawable.ic_cashback_top_tier_diamond)
            Tiers.COURIER -> resourceManager.getDrawable(R.drawable.ic_cashback_top_tier_green)
            else -> resourceManager.getDrawable(R.drawable.ic_cashback_top_tier_blue)
        }

    private fun topTextColor(tier: Tiers?) =
        if (tier in listOf(Tiers.TIER_4, Tiers.TIER_1, Tiers.COURIER)) {
            resourceManager.getColor(br.com.digio.uber.common.R.color.white)
        } else {
            resourceManager.getColor(br.com.digio.uber.common.R.color.black)
        }

    fun getStoreOffers(storeId: Long) {
        getCashbackOffersUseCase(storeId).singleExec(
            onError = { _, _, _ ->
                emptyListHide.value = true
                EmptyErrorObject
            },
            onSuccessBaseViewModel = { offers ->
                products.value = offers.content
            }
        )
    }
}