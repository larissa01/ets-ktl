package br.com.digio.uber.cashback.ui.fragment

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import br.com.digio.uber.cashback.BR
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.CashbackPurchasesFragmentBinding
import br.com.digio.uber.cashback.interfaces.MustFinish
import br.com.digio.uber.cashback.ui.dialog.CashbackMessageDialog
import br.com.digio.uber.cashback.ui.model.CashbackMessageUiModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackPurchasesViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const
import org.koin.android.ext.android.inject

abstract class CashbackBasePurchasesFragment :
    BaseViewModelFragment<CashbackPurchasesFragmentBinding, CashbackPurchasesViewModel>(), MustFinish {

    val mustFinish: Boolean? by lazy {
        arguments?.getBoolean(Const.RequestOnResult.Cashback.MUST_FINISH)
    }

    override val bindingVariable: Int? = BR.cashbackPurchasesViewModel
    override val getLayoutId: Int? = R.layout.cashback_purchases_fragment
    override val viewModel: CashbackPurchasesViewModel? by inject()
    override val showDisplayShowTitle = true
    override val showHomeAsUp = true
    override val menu = R.menu.cashback_doubt_white_menu
    override fun tag(): String = CashbackPurchasesFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.GREY

    override fun getToolbar(): Toolbar? = binding?.cashbackPurchasesAppBar?.navToolbar.apply {
        this?.title = getString(R.string.cashback)
        this?.navigationIcon =
            ContextCompat.getDrawable(requireContext(), br.com.digio.uber.common.R.drawable.ic_back_arrow_white)
        this?.setBackgroundColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.grey_1F1F1F))
        this?.setTitleTextColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.white))
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.cashback_doubt_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.cashback_doubt -> {
                showDoubtMessanger()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showDoubtMessanger() {
        activity?.supportFragmentManager?.let {
            CashbackMessageDialog.newInstance(
                CashbackMessageUiModel(
                    getString(R.string.about_cashback),
                    getString(R.string.doubt_text),
                    getString(R.string.understand)
                )
            ).show(it, null)
        }
    }
}