package br.com.digio.uber.cashback.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.cashback.databinding.ItemCashbackComissionBinding
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.model.cashback.CashbackComissionCategory
import br.com.digio.uber.util.convertToPercent
import br.com.digio.uber.util.safeHeritage

class CashbackComissionAdapter :
    RecyclerView.Adapter<CashbackComissionAdapter.CashbackComissionViewHolder>(), AdapterItemsContract {

    private var category: List<CashbackComissionCategory> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CashbackComissionViewHolder {
        val binding =
            ItemCashbackComissionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CashbackComissionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CashbackComissionViewHolder, position: Int) {
        holder.bind(category[position])
    }

    override fun getItemCount() = category.size

    override fun replaceItems(list: List<Any>) {
        category = list.safeHeritage()
        notifyDataSetChanged()
    }

    class CashbackComissionViewHolder(private val binding: ItemCashbackComissionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(category: CashbackComissionCategory) {
            binding.cashbackCategoryTxtCategory.text = category.commissionGroup
            binding.cashbackCategoryTxtComission.text = category.commission.convertToPercent()
        }
    }
}