package br.com.digio.uber.cashback.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.ItemCashbackHomeStoreBinding
import br.com.digio.uber.common.extensions.hide
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.common.typealiases.OnClickCashbackStoreItem
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.model.cashback.Tiers
import br.com.digio.uber.util.convertToPercent
import br.com.digio.uber.util.safeHeritage

class CashbackTopStoresAdapter(
    private val listener: OnClickCashbackStoreItem
) : RecyclerView.Adapter<CashbackTopStoresAdapter.CashbackTopStoresViewHolder>(), AdapterItemsContract {

    private var stores: List<CashbackStore> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CashbackTopStoresViewHolder {
        val binding =
            ItemCashbackHomeStoreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CashbackTopStoresViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: CashbackTopStoresViewHolder, position: Int) {
        holder.bind(stores[position], listener)
    }

    override fun getItemCount() = stores.size

    override fun replaceItems(list: List<Any>) {
        stores = list.safeHeritage()
        notifyDataSetChanged()
    }

    class CashbackTopStoresViewHolder(private val binding: ItemCashbackHomeStoreBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(product: CashbackStore, listener: OnClickCashbackStoreItem) {
            binding.cashbackStore = product
            binding.itemCashbackStoreContent.setOnClickListener { listener.invoke(product) }

            binding.itemCashbackStoreTxtTier.text =
                String.format(
                    if (product.storeTierStoreInfo?.maxTierCustomerCommission !=
                        product.storeTierStoreInfo?.minTierCustomerCommission
                    ) {
                        context.getString(R.string.tier_to_percentage)
                    } else context.getString(R.string.tier_percentage),
                    product.storeTierStoreInfo?.tierCode?.tier,
                    product.storeTierStoreInfo?.maxTierCustomerCommission?.convertToPercent()
                )

            if (product.storeTierStoreInfo?.tierCode != Tiers.TIER_4) {
                binding.itemCashbackStoreTxtNextTier.text =
                    String.format(
                        if (product.nextStoreTierStoreInfo?.maxTierCustomerCommission !=
                            product.nextStoreTierStoreInfo?.minTierCustomerCommission
                        ) {
                            context.getString(R.string.tier_to_percentage)
                        } else context.getString(
                            R.string.tier_percentage
                        ),
                        product.nextStoreTierStoreInfo?.tierCode?.tier,
                        product.nextStoreTierStoreInfo?.maxTierCustomerCommission?.convertToPercent()
                    )
            } else {
                binding.itemCashbackStoreTxtNextTier.hide()
                binding.itemCashbackStoreImgNextTier.hide()
            }

            binding.executePendingBindings()
        }
    }
}