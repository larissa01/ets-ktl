package br.com.digio.uber.cashback.ui.activity

import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.di.cashbackViewModelModule
import br.com.digio.uber.common.base.activity.BaseActivity
import org.koin.core.module.Module

class CashbackActivity : BaseActivity() {

    override fun getLayoutId(): Int? = R.layout.cashback_activity

    override fun getModule(): List<Module>? = listOf(cashbackViewModelModule)
}