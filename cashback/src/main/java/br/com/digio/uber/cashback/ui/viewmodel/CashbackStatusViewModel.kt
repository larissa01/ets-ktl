package br.com.digio.uber.cashback.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.cashback.R
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.usecase.cashback.GetCashbackPurchasesUseCase
import br.com.digio.uber.domain.usecase.cashback.SendCashbackAccountUseCase
import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.cashback.CashbackPurchase
import br.com.digio.uber.model.cashback.CashbackPurchaseRequest
import br.com.digio.uber.model.cashback.MyCashbackStatus
import br.com.digio.uber.model.cashback.SendCashbackRequest
import br.com.digio.uber.model.store.PaymentType

class CashbackStatusViewModel constructor(
    private val getCashbackPurchasesUseCase: GetCashbackPurchasesUseCase,
    private val sendCashbackAccountUseCase: SendCashbackAccountUseCase,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    val loadingDownPage: MutableLiveData<Boolean> = MutableLiveData()
    val maxPages: MutableLiveData<Int> = MutableLiveData()
    val page: MutableLiveData<Int> = MutableLiveData<Int>().apply {
        value = 0
    }
    val isLastPage: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = false
    }

    val isReleased: MutableLiveData<Boolean> = MutableLiveData()
    val showCbSelectAll: MutableLiveData<Boolean> = MutableLiveData(false)
    val info: MutableLiveData<String> = MutableLiveData()
    val btReceiveVisible = MutableLiveData<Boolean>(false)
    val btReceiveEnable = MutableLiveData<Boolean>(false)
    val btHistoryVisible = MutableLiveData<Boolean>(false)
    val emptyListHide: MutableLiveData<Boolean> = MutableLiveData(false)
    val emptyListSubtitle: MutableLiveData<String> = MutableLiveData()
    val alertHide: MutableLiveData<Boolean> = MutableLiveData(false)

    val listCashback: MutableLiveData<List<CashbackPurchase>> = MutableLiveData()
    private val allCashback: MutableList<CashbackPurchase> = mutableListOf()
    val isSendCashbackAccountSuccess: MutableLiveData<Boolean> = MutableLiveData()

    fun getPurchases(status: MyCashbackStatus) {
        if (isLastPage.value == false) {
            loadingDownPage.value = true

            getCashbackPurchasesUseCase(Pair(status.name, page.value ?: 0)).singleExec(
                onError = { _, _, _ ->
                    updateLivedata(null, status)
                    EmptyErrorObject
                },
                onSuccessBaseViewModel = { purchases ->
                    updateLivedata(purchases, status)
                    loadingDownPage.value = false
                }
            )
        }
    }

    private fun updateLivedata(purchases: ResponseList<CashbackPurchase>?, status: MyCashbackStatus) {
        purchases?.content?.let { allCashback.addAll(it) }
        listCashback.value = allCashback
        isLastPage.value = purchases?.last
        maxPages.value = purchases?.content?.size

        emptyListHide.value = listCashback.value.isNullOrEmpty()
        alertHide.value = listCashback.value.isNullOrEmpty() || (alertHide.value == true)
        showCbSelectAll.value = !listCashback.value.isNullOrEmpty() && status == MyCashbackStatus.RELEASED
    }

    fun sendCashbackAccount() {
        getPurchasesSelected()?.let { purchases ->
            sendCashbackAccountUseCase(SendCashbackRequest(PaymentType.ACCOUNT.name, purchases)).singleExec(
                onError = { _, error, _ ->
                    error
                },
                onSuccessBaseViewModel = {
                    isSendCashbackAccountSuccess.value = it.isSuccessful
                }
            )
        }
    }

    fun setLayoutShow(status: MyCashbackStatus) {
        isReleased.value = status == MyCashbackStatus.RELEASED
        setTextInfo(status)
        setButtonsVisible(status)
    }

    private fun setTextInfo(status: MyCashbackStatus) {
        when (status) {
            MyCashbackStatus.RELEASED -> {
                info.value = resourceManager.getString(R.string.relesead_info)
                emptyListSubtitle.value = resourceManager.getString(R.string.you_have_no_cashback_released)
            }
            MyCashbackStatus.WAITING_APPROVED -> {
                info.value = resourceManager.getString(R.string.waiting_approved_info)
                emptyListSubtitle.value = resourceManager.getString(R.string.you_have_no_cashback_wait_approved)
            }
            MyCashbackStatus.RECEIVED -> {
                info.value = resourceManager.getString(R.string.received_info)
                emptyListSubtitle.value = resourceManager.getString(R.string.you_have_no_cashback_received)
            }
            MyCashbackStatus.CANCELED, MyCashbackStatus.LOST -> {
                info.value = resourceManager.getString(R.string.canceled_info)
                emptyListSubtitle.value = resourceManager.getString(R.string.you_have_no_cashback_canceled)
            }
        }
    }

    private fun setButtonsVisible(status: MyCashbackStatus) =
        when (status) {
            MyCashbackStatus.RELEASED -> {
                btReceiveVisible.value = true
                btHistoryVisible.value = true
            }
            MyCashbackStatus.WAITING_APPROVED -> {
                btHistoryVisible.value = true
            }
            else -> {
                btReceiveVisible.value = false
                btHistoryVisible.value = false
            }
        }

    fun selectAllPurchases(isSelectAll: Boolean) {
        btReceiveEnable.value = isSelectAll

        val listPurchases = mutableListOf<CashbackPurchase>()
        listCashback.value?.forEach { listPurchases.add(it.copy(checked = isSelectAll)) }
        allCashback.forEach { listPurchases.add(it.copy(checked = isSelectAll)) }
        listCashback.value = listPurchases
    }

    private fun getPurchasesSelected(): List<CashbackPurchaseRequest>? {
        val listRequest = mutableListOf<CashbackPurchaseRequest>()
        listCashback.value?.forEach {
            if (it.checked) {
                listRequest.add(
                    CashbackPurchaseRequest(
                        id = it.id,
                        storeIdentifierId = it.storeIdentifierId,
                        partnerId = it.partnerId
                    )
                )
            }
        }

        return listRequest
    }

    fun checkUncheskPurchase(purchase: CashbackPurchase, checked: Boolean) {
        listCashback.value?.forEach { if (it == purchase) it.checked = checked }
        allCashback.forEach { if (it == purchase) it.checked = checked }

        btReceiveEnable.value = listCashback.value?.any { it.checked }
    }
}