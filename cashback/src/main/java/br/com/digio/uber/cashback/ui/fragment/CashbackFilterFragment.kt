package br.com.digio.uber.cashback.ui.fragment

import android.content.res.ColorStateList
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.widget.CheckBox
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import br.com.digio.uber.cashback.BR
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.CashbackFilterFragmentBinding
import br.com.digio.uber.cashback.ui.model.CashbackStoresCategoriesUiModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackFilterViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.extensions.setMarginCustom
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const.RequestOnResult.Cashback.ALL_CATEGORIES
import br.com.digio.uber.util.Const.RequestOnResult.Cashback.TOTAL_ELEMENTS
import br.com.digio.uber.util.safeHeritage
import br.com.digio.uber.util.safeLet
import org.koin.android.ext.android.inject

class CashbackFilterFragment : BaseViewModelFragment<CashbackFilterFragmentBinding, CashbackFilterViewModel>() {

    private val allCategories by lazy {
        arguments?.getParcelableArray(ALL_CATEGORIES)?.toList()?.safeHeritage<CashbackStoresCategoriesUiModel>()
            ?.toMutableList()
    }

    private val totalElements: Int? by lazy {
        arguments?.getInt(TOTAL_ELEMENTS)
    }

    override val bindingVariable: Int? = BR.cashbackFilterViewModel
    override val getLayoutId: Int? = R.layout.cashback_filter_fragment
    override val viewModel: CashbackFilterViewModel? by inject()

    override fun tag(): String = CashbackHomeFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun getToolbar(): Toolbar? = binding?.cashbackFilterAppBar?.navToolbar.apply {
        this?.title = getString(R.string.filter_of_stores)
        this?.navigationIcon =
            ContextCompat.getDrawable(requireContext(), br.com.digio.uber.common.R.drawable.ic_back_arrow_black)
        this?.setBackgroundColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.white))
        this?.setTitleTextColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.black))
    }

    override val showDisplayShowTitle = true

    override val showHomeAsUp = true

    override fun onNavigationClick(view: View) {
        pop()
    }

    override fun initialize() {
        super.initialize()
        viewModel?.initializer {
            when (it.id) {
                R.id.cashback_filter_btn_continue -> goToAllStores()
            }
        }
        allCategories?.forEach {
            val checkBoxCategory = setUpCheckbox(it)

            binding?.cashbackFilterContent?.addView(checkBoxCategory)
        }
    }

    private fun setUpCheckbox(categories: CashbackStoresCategoriesUiModel): CheckBox {
        return CheckBox(context).also {
            it.layoutParams = ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.WRAP_CONTENT,
                ConstraintLayout.LayoutParams.WRAP_CONTENT
            ).apply {
                leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID
                rightToLeft = ConstraintLayout.LayoutParams.PARENT_ID
                horizontalChainStyle = ConstraintLayout.LayoutParams.CHAIN_PACKED
                setMarginCustom(
                    left = resources.getDimensionPixelSize(br.com.digio.uber.common.R.dimen._12sdp),
                    right = resources.getDimensionPixelSize(br.com.digio.uber.common.R.dimen._8sdp),
                    bottom = resources.getDimensionPixelSize(br.com.digio.uber.common.R.dimen._8sdp)
                )
            }

            it.isChecked = categories.selected
            it.text = categories.category
            it.setTextSize(
                TypedValue.COMPLEX_UNIT_PX,
                resources.getDimension(br.com.digio.uber.common.R.dimen.dimen_16sp)
            )
            it.buttonTintList =
                ColorStateList.valueOf(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.black))

            it.setOnClickListener { _ ->
                addFilteredCategory(it.text.toString(), it.isChecked)
            }
        }
    }

    private fun goToAllStores() {
        safeLet(allCategories?.toTypedArray(), totalElements) { categories, elements ->
            navigateTo(
                CashbackFilterFragmentDirections.actionCashbackFilterFragmentToCashbackAllStoresFragment(),
                Bundle().apply {
                    putParcelableArray(ALL_CATEGORIES, categories)
                    putInt(TOTAL_ELEMENTS, elements)
                }
            )
        }
    }

    private fun addFilteredCategory(category: String, isChecked: Boolean) {
        allCategories?.find { it.category == category }?.apply {
            selected = isChecked
        }
    }
}