package br.com.digio.uber.cashback.ui.fragment

import android.view.View
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.interfaces.MustFinish
import br.com.digio.uber.cashback.ui.adapter.CashbackPagerAdapter
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.model.cashback.MyCashbackStatus
import br.com.digio.uber.util.safeLet
import com.google.android.material.tabs.TabLayoutMediator

class CashbackPurchasesFragment : CashbackBasePurchasesFragment(), MustFinish {

    override fun initialize() {
        super.initialize()

        val fragments = listOf(
            CashbackStatusFragment.newInstance(MyCashbackStatus.RELEASED, this),
            CashbackStatusFragment.newInstance(MyCashbackStatus.WAITING_APPROVED, this)
        )

        binding?.cashbackPurchasesPager?.adapter = CashbackPagerAdapter(this, fragments, requireContext())

        safeLet(binding?.cashbackPurchasesTab, binding?.cashbackPurchasesPager) { tabLayout, viewPager2 ->
            TabLayoutMediator(tabLayout, viewPager2) { tab, position ->
                tab.text = if (position == 0) {
                    requireContext().getString(R.string.released)
                } else {
                    requireContext().getString(R.string.waiting_aproved)
                }
            }.attach()
        }
    }

    override fun onNavigationClick(view: View) {
        super.onNavigationClick(view)
        mustFinish()
    }

    override fun mustFinish() {
        if (mustFinish == true) {
            activity?.finish()
        } else {
            pop()
        }
    }
}