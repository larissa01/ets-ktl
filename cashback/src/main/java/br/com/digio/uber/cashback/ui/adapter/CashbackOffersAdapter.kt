package br.com.digio.uber.cashback.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.cashback.databinding.ItemCashbackStoreProductBinding
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.common.typealiases.OnClickCashbackOfferItem
import br.com.digio.uber.common.typealiases.OnClickGoToStoreButtom
import br.com.digio.uber.model.cashback.CashbackOfferResponse
import br.com.digio.uber.util.safeHeritage

class CashbackOffersAdapter(
    private val goToRules: OnClickCashbackOfferItem,
    private val goToStore: OnClickGoToStoreButtom
) : RecyclerView.Adapter<CashbackOffersAdapter.CashbackOffersViewHolder>(), AdapterItemsContract {

    private var stores: List<CashbackOfferResponse> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CashbackOffersViewHolder {
        val binding =
            ItemCashbackStoreProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CashbackOffersViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CashbackOffersViewHolder, position: Int) {
        holder.bind(stores[position], goToRules, goToStore)
    }

    override fun getItemCount() = stores.size

    override fun replaceItems(list: List<Any>) {
        stores = list.safeHeritage()
        notifyDataSetChanged()
    }

    class CashbackOffersViewHolder(private val binding: ItemCashbackStoreProductBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            product: CashbackOfferResponse,
            listener: OnClickCashbackOfferItem,
            goToStore: OnClickGoToStoreButtom
        ) {
            binding.itemCashbackStoreItemContent.product = product

            binding.itemCashbackStoreItemContent.cashbackRulesTxtRules.setOnClickListener { listener.invoke(product) }

            binding.itemCashbackStoreItemContent.cashbackRulesTxtGoToStore.setOnClickListener {
                goToStore.invoke(
                    product.url
                )
            }
        }
    }
}