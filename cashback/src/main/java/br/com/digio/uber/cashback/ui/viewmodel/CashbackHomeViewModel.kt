package br.com.digio.uber.cashback.ui.viewmodel

import android.graphics.Color
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.cashback.R
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.cashback.GetAmountCashbackUseCase
import br.com.digio.uber.domain.usecase.cashback.GetCashbackCustomerUseCase
import br.com.digio.uber.domain.usecase.cashback.GetCashbackStoresUseCase
import br.com.digio.uber.model.cashback.CashbackCustomerResponse
import br.com.digio.uber.model.cashback.CashbackStatus
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.model.cashback.Tiers
import br.com.digio.uber.util.toMoney

class CashbackHomeViewModel constructor(
    private val getCashbackCustomerUseCase: GetCashbackCustomerUseCase,
    private val getAmountCashbackUseCase: GetAmountCashbackUseCase,
    private val getCashbackStoresUseCase: GetCashbackStoresUseCase,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    val customer: MutableLiveData<CashbackCustomerResponse> = MutableLiveData()
    val bgTopColor: MutableLiveData<Int> = MutableLiveData()
    val txtNextLevel: MutableLiveData<String> = MutableLiveData()
    val txtNextLevelDetails: MutableLiveData<String> = MutableLiveData()
    val cashbackAmount: MutableLiveData<String> = MutableLiveData()
    val cashbackStores: MutableLiveData<List<CashbackStore>> = MutableLiveData()
    val totalElements: MutableLiveData<Int> = MutableLiveData()

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)

        getCashbackStores()

        getCashbackAmount()

        getCashbackCustomer()
    }

    private fun getCashbackAmount() {
        getAmountCashbackUseCase(CashbackStatus.RELEASED.name).singleExec(
            showLoadingFlag = false,
            onError = { _, _, _ ->
                cashbackAmount.value = resourceManager.getString(R.string.unavailable)
                EmptyErrorObject
            },
            onSuccessBaseViewModel = { amount ->
                cashbackAmount.value = amount.amount?.toBigDecimal()?.toMoney()
            }
        )
    }

    private fun getCashbackCustomer() {
        getCashbackCustomerUseCase(Unit).singleExec(
            showLoadingFlag = false,
            onError = { _, error, _ ->
                error
            },
            onSuccessBaseViewModel = {
                customer.value = it
                bgTopColor.value = Color.parseColor(it.tier?.color)
                txtNextLevel.value = textNextTier()
                txtNextLevelDetails.value = textNextTierDetails()
            }
        )
    }

    private fun getCashbackStores() {
        getCashbackStoresUseCase(TOP_STORES_DEFAULT).singleExec(
            onError = { _, error, _ ->
                error
            },
            onSuccessBaseViewModel = { stores ->
                cashbackStores.value = stores.content.toMutableList().sortedBy { it.category?.name }
                totalElements.value = stores.totalElements
            }
        )
    }

    fun getTotalElements() = totalElements.value

    private fun textNextTier() =
        if (customer.value?.tier?.code != Tiers.TIER_4) {
            resourceManager.getString(R.string.next_level)
        } else {
            resourceManager.getString(R.string.max_level)
        }

    private fun textNextTierDetails() =
        if (customer.value?.tier?.code == Tiers.TIER_4) {
            resourceManager.getString(R.string.max_level_details)
        } else {
            String.format(
                resourceManager.getString(
                    R.string.unlock_next_level,
                    customer.value?.nextTier?.code?.tier ?: ""
                )
            )
        }

    companion object {
        const val TOP_STORES_DEFAULT = 6
    }
}