package br.com.digio.uber.cashback.ui.model

import java.io.Serializable

data class CashbackMessageUiModel(
    val title: String?,
    val message: String?,
    val buttonText: String?
) : Serializable