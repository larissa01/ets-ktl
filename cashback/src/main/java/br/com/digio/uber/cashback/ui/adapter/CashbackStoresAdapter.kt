package br.com.digio.uber.cashback.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.ItemCashbackStoreBinding
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.common.typealiases.OnClickCashbackStoreItem
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.util.convertToPercent
import br.com.digio.uber.util.safeHeritage

class CashbackStoresAdapter(
    private val listener: OnClickCashbackStoreItem,
) : RecyclerView.Adapter<CashbackStoresAdapter.CashbackStoresViewHolder>(), AdapterItemsContract {

    private var stores: List<CashbackStore> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CashbackStoresViewHolder {
        val binding =
            ItemCashbackStoreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CashbackStoresViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: CashbackStoresViewHolder, position: Int) {
        val showCategory = if (position > 0) {
            stores[position].category?.id != stores[position - 1].category?.id
        } else true

        holder.bind(stores[position], showCategory, listener)
    }

    override fun getItemCount() = stores.size

    override fun replaceItems(list: List<Any>) {
        stores = list.safeHeritage()
        notifyDataSetChanged()
    }

    class CashbackStoresViewHolder(private val binding: ItemCashbackStoreBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(product: CashbackStore, showCategory: Boolean, listener: OnClickCashbackStoreItem) {
            binding.cashbackStore = product
            binding.showCategory = showCategory

            binding.itemCashbackStoreTxtTier.text =
                String.format(
                    if (product.nextStoreTierStoreInfo?.maxTierCustomerCommission !=
                        product.nextStoreTierStoreInfo?.minTierCustomerCommission
                    ) {
                        context.getString(R.string.range_cashback)
                    } else context.getString(R.string.percentage_cashback),
                    product.storeTierStoreInfo?.minTierCustomerCommission?.convertToPercent(),
                    product.storeTierStoreInfo?.maxTierCustomerCommission?.convertToPercent()
                )

            binding.itemCashbackStoreContent.setOnClickListener { listener.invoke(product) }
        }
    }
}