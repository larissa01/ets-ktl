package br.com.digio.uber.cashback.di

import br.com.digio.uber.cashback.ui.viewmodel.CashbackAllStoresViewModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackComissionCategoryViewModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackConfirmationViewModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackFilterViewModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackFirstAccessViewModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackHomeViewModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackPurchasesViewModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackStatusViewModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackStoreProductsViewModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackSuccessViewModel
import br.com.digio.uber.cashback.ui.viewmodel.CashbackTermsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val cashbackViewModelModule = module {
    viewModel { CashbackFirstAccessViewModel(get()) }
    viewModel { CashbackTermsViewModel(get(), get()) }
    viewModel { CashbackHomeViewModel(get(), get(), get(), get()) }
    viewModel { CashbackAllStoresViewModel(get()) }
    viewModel { CashbackFilterViewModel() }
    viewModel { CashbackStoreProductsViewModel(get(), get()) }
    viewModel { CashbackConfirmationViewModel(get(), get()) }
    viewModel { CashbackComissionCategoryViewModel(get()) }
    viewModel { CashbackPurchasesViewModel() }
    viewModel { CashbackStatusViewModel(get(), get(), get()) }
    viewModel { CashbackSuccessViewModel() }
}