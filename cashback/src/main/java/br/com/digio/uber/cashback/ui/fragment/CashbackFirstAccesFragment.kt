package br.com.digio.uber.cashback.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import br.com.digio.uber.cashback.BR
import br.com.digio.uber.cashback.R
import br.com.digio.uber.cashback.databinding.CashbackFirstAccessFragmentBinding
import br.com.digio.uber.cashback.ui.viewmodel.CashbackFirstAccessViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.enums.CallCashbackFragment
import br.com.digio.uber.util.safeHeritage
import org.koin.android.ext.android.inject

class CashbackFirstAccesFragment :
    BaseViewModelFragment<CashbackFirstAccessFragmentBinding, CashbackFirstAccessViewModel>() {

    override val bindingVariable: Int? = BR.cashbackFirstAccessViewModel
    override val getLayoutId: Int? = R.layout.cashback_first_access_fragment
    override val viewModel: CashbackFirstAccessViewModel? by inject()

    override fun tag(): String = CashbackFirstAccesFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.ORANGE

    override fun getToolbar(): Toolbar? = binding?.cashbackFirstAccessAppBar?.navToolbar.apply {
        this?.title = getString(R.string.cashback)
        this?.navigationIcon = context?.let {
            ContextCompat.getDrawable(
                it,
                br.com.digio.uber.common.R.drawable.ic_back_arrow
            )
        }
        this?.setBackgroundColor(
            ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.orange_FA9269)
        )
        this?.setTitleTextColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.white))
    }

    override val showDisplayShowTitle = true

    override val showHomeAsUp = true

    override fun onNavigationClick(view: View) {
        onBackPressed()
    }

    override fun onBackPressed() {
        activity?.finish()
    }

    override fun initialize() {
        super.initialize()

        setupViewModel()

        viewModel?.initializer { view ->
            when (view.id) {
                R.id.recharge_carrier_btn_continue ->
                    navigateTo(
                        CashbackFirstAccesFragmentDirections.actionCashbackFirstAccesFragmentToCashbackTermsFragment(),
                        Bundle().apply {
                            putBoolean(Const.RequestOnResult.Cashback.BUTTON_VISIBLE, true)
                        }
                    )
            }
        }
    }

    private fun setupViewModel() {
        viewModel?.termsAccept?.observe(
            this,
            Observer {
                if (it) {
                    when (activity?.intent?.extras?.get(Const.RequestOnResult.Cashback.CALL_CASHBACK_FRAGMENT)) {
                        CallCashbackFragment.HOME.name -> navigateTo(R.id.call_home, bundleOf())

                        CallCashbackFragment.MY_CASHBACK.name ->
                            navigateTo(
                                R.id.call_my_cashback,
                                Bundle().apply {
                                    putBoolean(Const.RequestOnResult.Cashback.MUST_FINISH, true)
                                }
                            )

                        CallCashbackFragment.ALL_STORES.name ->
                            navigateTo(
                                R.id.call_all_stores,
                                Bundle().apply {
                                    putInt(
                                        Const.RequestOnResult.Cashback.TOTAL_ELEMENTS,
                                        activity?.intent?.extras?.getInt(Const.RequestOnResult.Cashback.TOTAL_ELEMENTS)
                                            ?: 0
                                    )
                                    putBoolean(Const.RequestOnResult.Cashback.MUST_FINISH, true)
                                }
                            )

                        CallCashbackFragment.CASHBACK_STORE.name ->
                            navigateTo(
                                R.id.call_cashback_store,
                                Bundle().apply {
                                    putSerializable(
                                        Const.RequestOnResult.Cashback.STORE,
                                        activity?.intent?.extras?.get(Const.RequestOnResult.Cashback.STORE)
                                            ?.safeHeritage<CashbackStore>()
                                    )
                                    putBoolean(Const.RequestOnResult.Cashback.MUST_FINISH, true)
                                }
                            )

                        else -> navigateTo(R.id.call_home, bundleOf())
                    }
                }
            }
        )
    }
}