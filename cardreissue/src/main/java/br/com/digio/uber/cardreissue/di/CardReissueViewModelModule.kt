package br.com.digio.uber.cardreissue.di

import br.com.digio.uber.cardreissue.viewmodel.CardReissueViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val CardReissueViewModelModule = module {
    viewModel { CardReissueViewModel(get(), get()) }
}