package br.com.digio.uber.cardreissue.fragment

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.lifecycle.Observer
import br.com.digio.uber.cardreissue.BR
import br.com.digio.uber.cardreissue.R
import br.com.digio.uber.cardreissue.activity.CardReissueActivity
import br.com.digio.uber.cardreissue.databinding.CardReissueFragmentBinding
import br.com.digio.uber.cardreissue.viewmodel.CardReissueViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.generics.makeChooseMessageObject
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const
import org.koin.android.ext.android.inject

class CardReissueFragment : BaseViewModelFragment<CardReissueFragmentBinding, CardReissueViewModel>() {
    override val bindingVariable: Int = BR.cardReissueViewModel
    override val getLayoutId: Int = R.layout.card_reissue_fragment
    override val viewModel: CardReissueViewModel by inject()

    override fun tag(): String = CardReissueFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE

    override fun initialize() {
        super.initialize()
        (activity as CardReissueActivity).getToolbar()?.setTitle(R.string.card_reissue_fragment_title)
        viewModel.initializer { onItemClicked(it) }
        setupObserve()
    }

    override fun onNavigationClick(view: View) {
        activity?.finish()
    }

    override fun onBackPressed() {
        activity?.finish()
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.listCardEeissueLostMyCardIten -> {
                confirmationMessage { viewModel.postLostCard() }
            }
            R.id.listCardReissueTheftCard -> {
                confirmationMessage { viewModel.postTheftTCard() }
            }
            R.id.btnCardReissueveloe -> {
                isValoiApp()
            }
        }
    }

    private fun openSiteVeloi() {
        val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse(Const.URI.URL_VELOI_ACCOUNT))
        startActivity(webIntent)
    }

    private fun isValoiApp() {
        activity?.let {
            val intent = Intent(Intent.ACTION_VIEW).apply {
                setPackage(PACKAGE_VELOI)
            }

            if (intent.resolveActivity(it.packageManager) != null) {
                startActivity(intent)
            } else {
                openSiteVeloi()
            }
        }
    }

    private fun setupObserve() {
        viewModel.successReissue.observe(
            this,
            Observer {
                if (it) {
                    onBackPressed()
                }
            }
        )
    }

    fun confirmationMessage(call: () -> Unit) {
        showMessage(
            makeChooseMessageObject {
                title = R.string.card_reissue_dialog_title
                icon = br.com.digio.uber.common.R.drawable.ic_warning
                message = R.string.card_reissue_dialog_message
                positiveOptionTextInt = R.string.card_reissue_dialog_positive
                negativeOptionTextInt = R.string.card_reissue_dialog_negative
                onPositiveButtonClick = {
                    call()
                }
            }
        )
    }

    companion object {
        private const val PACKAGE_VELOI = "br.com.veloe.mobile"
    }
}