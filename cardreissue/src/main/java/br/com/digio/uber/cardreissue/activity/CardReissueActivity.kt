package br.com.digio.uber.cardreissue.activity

import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import br.com.digio.uber.cardreissue.R
import br.com.digio.uber.cardreissue.di.CardReissueViewModelModule
import br.com.digio.uber.common.base.activity.BaseJetNavigationActivity
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.helper.ViewIds
import org.koin.core.module.Module

class CardReissueActivity : BaseJetNavigationActivity() {
    override fun getLayoutId(): Int = LayoutIds.navigationLayoutWithToolbarActivityWhite
    override fun navGraphId(): Int = R.navigation.nav_card_reissue_card
    override fun getModule(): List<Module> = listOf(CardReissueViewModelModule)

    override fun getToolbar(): Toolbar? = findViewById<Toolbar>(ViewIds.basicToolbarIdWhite).apply {
        this.title = getString(R.string.card_reissue_fragment_title)
        this.navigationIcon = ContextCompat.getDrawable(
            context,
            br.com.digio.uber.common.R.drawable.ic_back_arrow_black
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}