package br.com.digio.uber.cardreissue.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.cardreissue.R
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.generics.makeMessageSuccessObject
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.card.GetCardTypeUseCase
import br.com.digio.uber.domain.usecase.card.cardreissue.CardUseCaseTypeCardReissue
import br.com.digio.uber.domain.usecase.card.cardreissue.PostCardReissueUseCase
import br.com.digio.uber.model.card.TypeCard

class CardReissueViewModel constructor(
    val postCardReissueUseCase: PostCardReissueUseCase,
    val getCardTypeUseCase: GetCardTypeUseCase
) : BaseViewModel() {
    val cardId = MutableLiveData<Long?>()
    val successReissue = MutableLiveData<Boolean>(false)

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        getCard()
    }

    fun getCard() {
        getCardTypeUseCase(TypeCard.DEBIT_ELO).singleExec(
            onError = { _, error, _ ->
                ErrorObject().apply {
                    close = false
                }
            },
            onSuccessBaseViewModel = {
                cardId.value = it.cardId
            }
        )
    }

    fun postLostCard() {
        cardId.value?.let { it ->
            postCardReissueUseCase(Pair(CardUseCaseTypeCardReissue.LOSS, it)).singleExec(
                onError = { _, error, _ ->
                    error.apply {
                        close = false
                    }
                },
                onSuccessBaseViewModel = {
                    it.let { lostStolenResponse ->
                        cardId.value = lostStolenResponse.cardId.toLong()
                        message.value = makeMessageSuccessObject {
                            title = R.string.card_reissue_dialog_success_message_title
                            message = R.string.card_reissue_dialog_success
                            onPositiveButtonClick = {
                                successReissue.postValue(true)
                            }
                        }
                    }
                }
            )
        } ?: errorCard()
    }

    fun postTheftTCard() {
        cardId.value?.let { it ->
            postCardReissueUseCase(Pair(CardUseCaseTypeCardReissue.THEFT, it)).singleExec(
                onError = { _, error, _ ->
                    error.apply {
                        close = false
                    }
                },
                onSuccessBaseViewModel = {
                    it.let { lostStolenResponse ->
                        cardId.value = lostStolenResponse.cardId.toLong()
                        message.value = makeMessageSuccessObject {
                            title = R.string.card_reissue_dialog_success_message_title
                            message = R.string.card_reissue_dialog_success
                            onPositiveButtonClick = {
                                successReissue.postValue(true)
                            }
                        }
                    }
                }
            )
        } ?: errorCard()
    }

    private fun errorCard() {
        ErrorObject().apply {
            message = R.string.card_reissue_card_not_found
            close = true
        }
    }
}