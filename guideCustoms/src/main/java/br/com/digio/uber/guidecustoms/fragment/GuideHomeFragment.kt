package br.com.digio.uber.guidecustoms.fragment

import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.guidecustoms.R
import br.com.digio.uber.guidecustoms.databinding.GuideHomeFragmentBinding

class GuideHomeFragment : BaseViewModelFragment<GuideHomeFragmentBinding, BaseViewModel>() {

    override val bindingVariable: Int? = null
    override val getLayoutId: Int? = R.layout.guide_home_fragment
    override val viewModel: BaseViewModel? = null

    override fun tag(): String = GuideHomeFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() {
        super.initialize()
        binding?.btnGoToButtons?.setOnClickListener {
            navigateTo(GuideHomeFragmentDirections.actionGuideHomeFragmentToGuideButtonsFragment())
        }
        binding?.btnGoToListTile?.setOnClickListener {
            navigateTo(GuideHomeFragmentDirections.actionGuideHomeFragmentToGuideListTileFragment())
        }
        binding?.btnGoToDialogScreen?.setOnClickListener {
            navigateTo(GuideHomeFragmentDirections.actionGuideHomeFragmentToGuideDialogFragment())
        }
        binding?.btnGoToEditTextPassword?.setOnClickListener {
            navigateTo(GuideHomeFragmentDirections.actionGuideHomeFragmentToGuideEditTextPasswordFragment())
        }

        binding?.btnGoToSwipe?.setOnClickListener {
            navigateTo(GuideHomeFragmentDirections.actionGuideHomeFragmentToGuideSwipeFragment())
        }
    }

    override fun onBackPressed() {
        activity?.finish()
    }
}