package br.com.digio.uber.guidecustoms.fragment

import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.guidecustoms.R
import br.com.digio.uber.guidecustoms.databinding.GuideButtonsFragmentBinding

class GuideButtonsFragment : BaseViewModelFragment<GuideButtonsFragmentBinding, BaseViewModel>() {

    override val bindingVariable: Int? = null
    override val getLayoutId: Int? = R.layout.guide_buttons_fragment
    override val viewModel: BaseViewModel? = null

    override fun tag(): String = GuideButtonsFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK
}