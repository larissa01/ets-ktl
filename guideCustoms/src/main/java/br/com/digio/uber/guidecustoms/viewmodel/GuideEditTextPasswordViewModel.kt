package br.com.digio.uber.guidecustoms.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.common.typealiases.OnTextChanged
import br.com.digio.uber.guidecustoms.R

class GuideEditTextPasswordViewModel constructor(
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    val togglePasswordField: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = false
    }

    val hintError: MutableLiveData<String> = MutableLiveData()

    val onTextChangedListen: OnTextChanged = { string, _, _, _ ->
        val fakePassword = resourceManager.getString(R.string.faque_password)
        if (fakePassword == string.toString()) {
            hintError.value = null
        } else {
            hintError.value = resourceManager.getString(R.string.hint_error)
        }
    }

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer {
            when (it.id) {
                R.id.btnChangeFlag -> togglePasswordField.value = (togglePasswordField.value == false)
                else -> onClickItem(it)
            }
        }
    }
}