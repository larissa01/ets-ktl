package br.com.digio.uber.guidecustoms.fragment

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.extensions.toast
import br.com.digio.uber.common.helper.SwipeHelper
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.guidecustoms.R
import br.com.digio.uber.guidecustoms.adapter.SwipeExampleAdapter
import br.com.digio.uber.guidecustoms.databinding.GuideSwipeFragmentBinding

class GuideSwipeHelperFragment : BaseViewModelFragment<GuideSwipeFragmentBinding, BaseViewModel>() {

    override val bindingVariable: Int? = null
    override val getLayoutId: Int? = R.layout.guide_swipe_fragment
    override val viewModel: BaseViewModel? = null

    override fun tag(): String = GuideSwipeHelperFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() {
        super.initialize()

        setupAdapter()
        setupSwipe()
    }

    private fun setupAdapter() {
        binding?.swipeRecycler?.adapter = SwipeExampleAdapter(
            resources.getStringArray(R.array.swipe_sample).toList()
        )
        binding?.swipeRecycler?.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )
        binding?.swipeRecycler?.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun setupSwipe() {
        val itemTouchHelper = ItemTouchHelper(
            object : SwipeHelper(binding?.swipeRecycler, 0, ItemTouchHelper.LEFT) {
                override fun instantiateUnderlayButton(position: Int): List<UnderlayButton> {
                    val deleteButton = deleteButton(position)
                    val markAsUnreadButton = markAsUnreadButton(position)
                    val archiveButton = archiveButton(position)

                    return when (position) {
                        0 -> listOf(deleteButton)
                        1 -> listOf(deleteButton, markAsUnreadButton)
                        2 -> listOf(deleteButton, markAsUnreadButton, archiveButton)
                        else -> listOf(deleteButton)
                    }
                }
            }
        )

        itemTouchHelper.attachToRecyclerView(binding?.swipeRecycler)
    }

    private fun deleteButton(position: Int): SwipeHelper.UnderlayButton {
        return SwipeHelper.UnderlayButton(
            requireContext(),
            android.R.color.holo_blue_light,
            ContextCompat.getDrawable(requireContext(), br.com.digio.uber.common.R.drawable.ic_edit),
            object : SwipeHelper.UnderlayButtonClickListener {
                override fun onClick() {
                    requireContext().toast("$position")
                }
            }
        )
    }

    private fun markAsUnreadButton(position: Int): SwipeHelper.UnderlayButton {
        return SwipeHelper.UnderlayButton(
            requireContext(),
            android.R.color.black,
            ContextCompat.getDrawable(requireContext(), br.com.digio.uber.common.R.drawable.ic_delete),
            object : SwipeHelper.UnderlayButtonClickListener {
                override fun onClick() {
                    requireContext().toast("$position")
                }
            }
        )
    }

    private fun archiveButton(position: Int): SwipeHelper.UnderlayButton {
        return SwipeHelper.UnderlayButton(
            requireContext(),
            android.R.color.darker_gray,
            ContextCompat.getDrawable(requireContext(), R.drawable.ic_sample_icon),
            object : SwipeHelper.UnderlayButtonClickListener {
                override fun onClick() {
                    requireContext().toast("$position")
                }
            }
        )
    }
}