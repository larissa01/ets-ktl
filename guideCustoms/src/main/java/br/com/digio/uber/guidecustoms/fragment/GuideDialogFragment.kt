package br.com.digio.uber.guidecustoms.fragment

import android.widget.Toast
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.makeChooseMessageObject
import br.com.digio.uber.common.generics.makeErrorObject
import br.com.digio.uber.common.generics.makeMessageSuccessObject
import br.com.digio.uber.common.generics.makeThreeOptionsMessageObject
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.guidecustoms.R
import br.com.digio.uber.guidecustoms.databinding.GuideDialogFragmentBinding

class GuideDialogFragment : BaseViewModelFragment<GuideDialogFragmentBinding, BaseViewModel>() {
    override val bindingVariable: Int? = null
    override val getLayoutId: Int? = R.layout.guide_dialog_fragment
    override val viewModel: BaseViewModel? = null

    override fun tag(): String = GuideHomeFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun initialize() {
        super.initialize()
        bindBtnShowDialog()
        bindBtnChoose()
        bindShowDialog()
        bindThreeOptionsDialog()
    }

    private fun bindThreeOptionsDialog() {
        binding?.btnShowDialogThreeOptions?.setOnClickListener {
            showMessage(
                makeThreeOptionsMessageObject {
                    hideIcon = true
                    hideTitle = true
                    hideMessage = true
                    positiveOptionTextString = resources.getString(R.string.copy_code)
                    middleOptionTextString = resources.getString(R.string.see_slip_payment)
                    negativeOptionTextString = resources.getString(R.string.share)
                    onPositiveButtonClick = {
                        Toast.makeText(
                            context,
                            resources.getString(R.string.positive),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    onMiddleButtonClick = {
                        Toast.makeText(
                            context,
                            resources.getString(R.string.middle),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    onNegativeButtonClick = {
                        Toast.makeText(
                            context,
                            resources.getString(R.string.negative),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            )
        }
    }

    private fun bindShowDialog() {
        binding?.btnShowDialogError?.setOnClickListener {
            showMessage(
                makeErrorObject {
                    onCloseDialog = { closeWithButton ->
                        if (closeWithButton) {
                            Toast.makeText(
                                requireContext(),
                                getString(R.string.close_with_button),
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            Toast.makeText(
                                requireContext(),
                                getString(R.string.on_close_dialog),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                }
            )
        }
    }

    private fun bindBtnChoose() {
        binding?.btnShowDialogChoose?.setOnClickListener {
            showMessage(
                makeChooseMessageObject {
                    title = R.string.dialog_two_options
                    message = R.string.body_choose_dialog
                }
            )
        }
    }

    private fun bindBtnShowDialog() {
        binding?.btnShowDialog?.setOnClickListener {
            showMessage(
                makeMessageSuccessObject {
                    title = R.string.dialog_informative_title
                    message = R.string.message_of_success
                }
            )
        }
    }
}