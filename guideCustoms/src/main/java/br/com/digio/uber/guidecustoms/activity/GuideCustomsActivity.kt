package br.com.digio.uber.guidecustoms.activity

import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.guidecustoms.R
import br.com.digio.uber.guidecustoms.di.guideViewModelModule
import org.koin.core.module.Module

class GuideCustomsActivity : BaseActivity() {
    override fun getLayoutId(): Int? = R.layout.activity_guide_customs

    override fun getModule(): List<Module>? =
        listOf(guideViewModelModule)
}