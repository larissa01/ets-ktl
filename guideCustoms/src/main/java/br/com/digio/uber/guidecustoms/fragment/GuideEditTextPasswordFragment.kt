package br.com.digio.uber.guidecustoms.fragment

import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.guidecustoms.BR
import br.com.digio.uber.guidecustoms.R
import br.com.digio.uber.guidecustoms.databinding.GuideEditTextPasswordFragmentBinding
import br.com.digio.uber.guidecustoms.viewmodel.GuideEditTextPasswordViewModel
import org.koin.android.ext.android.inject

class GuideEditTextPasswordFragment :
    BaseViewModelFragment<GuideEditTextPasswordFragmentBinding, GuideEditTextPasswordViewModel>() {

    override val bindingVariable: Int = BR.guideEditTextViewModel
    override val getLayoutId: Int? = R.layout.guide_edit_text_password_fragment
    override val viewModel: GuideEditTextPasswordViewModel by inject()

    override fun tag(): String = GuideHomeFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun initialize() {
        super.initialize()
        viewModel.initializer { }
    }
}