package br.com.digio.uber.guidecustoms.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.guidecustoms.databinding.ItemGuideSwipeBinding

class SwipeExampleAdapter(private val strings: List<String>) : RecyclerView.Adapter<TextViewHolder>() {
    override fun getItemCount() = strings.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TextViewHolder.from(parent)

    override fun onBindViewHolder(holder: TextViewHolder, position: Int) =
        holder.bind(strings[position])
}

class TextViewHolder(private val binding: ItemGuideSwipeBinding) : RecyclerView.ViewHolder(binding.root) {
    companion object {
        fun from(parent: ViewGroup): TextViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ItemGuideSwipeBinding.inflate(inflater, parent, false)
            return TextViewHolder(binding)
        }
    }

    fun bind(text: String) {
        binding.textView.text = text
    }
}