package br.com.digio.uber.guidecustoms.di

import br.com.digio.uber.guidecustoms.viewmodel.GuideEditTextPasswordViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val guideViewModelModule = module {
    viewModel { GuideEditTextPasswordViewModel(get()) }
}