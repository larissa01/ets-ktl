package br.com.digio.uber.facialbiometry.activity

import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.facialbiometry.R
import br.com.digio.uber.facialbiometry.di.facialBiometryViewModelModule
import org.koin.core.module.Module

class FacialBiometryActivity : BaseActivity() {

    override fun getLayoutId(): Int? =
        R.layout.facial_biometry_activity

    override fun getModule(): List<Module>? =
        listOf(facialBiometryViewModelModule)
}