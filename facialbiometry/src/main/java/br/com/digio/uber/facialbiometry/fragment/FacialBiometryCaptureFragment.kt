package br.com.digio.uber.facialbiometry.fragment

import android.content.Intent
import android.graphics.Bitmap
import android.view.View
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.facialbiometry.BR
import br.com.digio.uber.facialbiometry.R
import br.com.digio.uber.facialbiometry.databinding.FragmentFacialBiometryCaptureScreenBinding
import br.com.digio.uber.facialbiometry.viewmodel.FacialBiometryCaptureViewModel
import br.com.digio.uber.util.CameraRendererExt
import br.com.digio.uber.util.Const.RequestOnResult.FacialBiometry.REQUEST_FACIAL_CAPTURE_SUCCESS
import br.com.digio.uber.util.Const.RequestOnResult.FacialBiometry.REQUEST_FACIAL_VALUE
import br.com.digio.uber.util.calcAcesso
import br.com.digio.uber.util.compressToMaxSize
import br.com.digio.uber.util.convertToFile
import br.com.digio.uber.util.fotoapparat
import br.com.digio.uber.util.rotate
import br.com.digio.uber.util.rotateBitmap
import io.fotoapparat.Fotoapparat
import io.fotoapparat.exception.camera.CameraException
import io.fotoapparat.selector.front
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import timber.log.Timber

@Suppress("TooManyFunctions")
class FacialBiometryCaptureFragment :
    BaseViewModelFragment<FragmentFacialBiometryCaptureScreenBinding,
        FacialBiometryCaptureViewModel>() {

    private var fotoapparat: Fotoapparat? = null

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.GREY

    override val bindingVariable: Int? = BR.facialBiometryCaptureViewModel

    override val getLayoutId: Int? = R.layout.fragment_facial_biometry_capture_screen

    override val viewModel: FacialBiometryCaptureViewModel by inject()

    override fun initialize() {
        super.initialize()
        viewModel.initializer(onClickItem)
        setupCamera()
    }

    override fun tag(): String =
        FacialBiometryCaptureFragment::class.java.name

    private val onClickItem: OnClickItem = {
        when (it.id) {
            R.id.btTakePicture -> takePicture()
        }
    }

    private fun setupCamera() {
        fotoapparat = context?.let {
            binding?.frameCamera?.fotoapparat(
                CameraRendererExt(
                    it,
                    binding?.focusView,
                    front(),
                    cameraErrorCallback = { cameraException ->
                        logErrorCamera(cameraException)
                    }
                )
            )
        }
        fotoapparat?.start()
        setPreviewSize()
    }

    private fun logErrorCamera(cameraException: CameraException) {
        cameraException.printStackTrace()
        fotoapparat?.stop()
        fotoapparat = null

        showMessage(
            ErrorObject().apply {
                message = R.string.selfie_tutorial_error_in_camera_message
            },
            true
        )
        Timber.e(cameraException)
    }

    private fun setPreviewSize() {
        binding?.frameCamera?.let { cameraViewLet ->
            fotoapparat?.calcAcesso(cameraViewLet) {
                binding?.facePreviewCameraMask?.layoutParams?.width = it.width
                binding?.facePreviewCameraMask?.layoutParams?.height = it.height
                binding?.facePreviewCameraMask?.requestLayout()
                binding?.facePreviewCameraMask?.visibility = View.VISIBLE
            }
        }
    }

    private fun takePicture() {
        fotoapparat?.takePicture()?.toBitmap()?.transform {
            fotoapparat?.stop()
            it.rotate(0).rotateBitmap(it.rotationDegrees.toFloat())
        }?.whenAvailable {
            compressImage(it)
        }
    }

    private fun compressImage(image: Bitmap?) {
        activity?.let { activityLet ->
            launch {
                try {
                    image?.convertToFile(activityLet, format = Bitmap.CompressFormat.JPEG)
                        ?.compressToMaxSize(activityLet, KB800)?.let {
                            activity?.setResult(
                                REQUEST_FACIAL_CAPTURE_SUCCESS,
                                Intent().apply {
                                    putExtra(
                                        REQUEST_FACIAL_VALUE,
                                        it.absolutePath
                                    )
                                }
                            )
                            activity?.finish()
                        } ?: showErrorForPreview()
                } catch (e: IllegalStateException) {
                    showErrorForPreview()
                    Timber.e(e)
                }
            }
        }
    }

    private fun showErrorForPreview() {
        showMessage(
            ErrorObject().apply {
                message = R.string.preview_photo_error_msg_create_request
            },
            true
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fotoapparat?.stop()
    }

    override fun onPause() {
        super.onPause()
        fotoapparat?.stop()
    }

    override fun onResume() {
        super.onResume()
        fotoapparat?.start()
    }

    companion object {
        // (800 * 1000) = 800KB
        private const val KB800 = (800L * 1000L)
    }
}