package br.com.digio.uber.facialbiometry.di

import br.com.digio.uber.facialbiometry.viewmodel.FacialBiometryCaptureViewModel
import br.com.digio.uber.facialbiometry.viewmodel.FacialBiometryMainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val facialBiometryViewModelModule = module {
    viewModel { FacialBiometryMainViewModel() }
    viewModel { FacialBiometryCaptureViewModel() }
}