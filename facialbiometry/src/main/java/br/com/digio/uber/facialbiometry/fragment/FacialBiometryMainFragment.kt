package br.com.digio.uber.facialbiometry.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.view.View
import androidx.annotation.NonNull
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.facialbiometry.BR
import br.com.digio.uber.facialbiometry.R
import br.com.digio.uber.facialbiometry.databinding.FragmentFacialBiometryMainBinding
import br.com.digio.uber.facialbiometry.viewmodel.FacialBiometryMainViewModel
import org.koin.android.ext.android.inject

class FacialBiometryMainFragment :
    BaseViewModelFragment<FragmentFacialBiometryMainBinding, FacialBiometryMainViewModel>() {

    override fun tag(): String =
        FacialBiometryMainFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE

    override val viewModel: FacialBiometryMainViewModel? by inject()

    override val getLayoutId: Int? = R.layout.fragment_facial_biometry_main

    override val bindingVariable: Int? = BR.facialBiometryMainViewModel

    private val analytics: Analytics by inject()

    private val tagForFirebaseContinue: String? by lazy {
        FacialBiometryMainFragmentArgs.fromBundle(checkNotNull(activity?.intent?.extras)).tagForFirebaseContinue
    }

    override fun initialize() {
        super.initialize()
        viewModel?.initializer {
            when (it.id) {
                R.id.btnNext -> onClickNextStep()
            }
        }
    }

    override fun getToolbar(): Toolbar? =
        binding?.appBarFacialBiometryMain?.toolbarFacialBiometry

    private fun onClickNextStep() {
        activity?.let { activityLet ->
            val notHavePermission = checkNotHavePermission(activityLet)
            if (notHavePermission) {
                analytics.pushSimpleEvent(
                    Tags.LOGIN_BIOMETRY_ACTIVE,
                    Tags.PARAMETER_LOGIN_BIOMETRY_ACTIVE to context
                        ?.getString(br.com.digio.uber.common.R.string.no)
                        .toString()
                )
                requestPermissions(
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    REQUEST_CODE
                )
                return
            }
            goToCaptureScreen()
        }
    }

    private fun checkNotHavePermission(it: FragmentActivity): Boolean =
        ContextCompat.checkSelfPermission(
            it,
            Manifest.permission.CAMERA
        ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            it,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            it,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED

    override fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE && grantResults.isNotEmpty()) {
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                goToCaptureScreen()
            } else {
                analytics.pushSimpleEvent(
                    Tags.LOGIN_BIOMETRY_ACTIVE,
                    Tags.PARAMETER_LOGIN_BIOMETRY_ACTIVE to context
                        ?.getString(br.com.digio.uber.common.R.string.no)
                        .toString()
                )
                showMessage(
                    ErrorObject().apply {
                        message = R.string.selfie_tutorial_permission_message
                    },
                    true
                )
            }
        }
    }

    private fun goToCaptureScreen() {
        analytics.pushSimpleEvent(
            Tags.LOGIN_BIOMETRY_ACTIVE,
            Tags.PARAMETER_LOGIN_BIOMETRY_ACTIVE to context
                ?.getString(br.com.digio.uber.common.R.string.yes)
                .toString()
        )
        analytics.pushSimpleEvent(tagForFirebaseContinue ?: Tags.LOGIN_BIOMETRY_CONTINUE)
        navigateTo(
            FacialBiometryMainFragmentDirections
                .actionFacialBiometryMainFragmentToFacialBiometryCaptureFragment()
        )
    }

    override fun onNavigationClick(view: View) {
        activity?.finish()
    }

    override fun onBackPressed() {
        activity?.finish()
    }

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true

    companion object {
        private const val REQUEST_CODE = 200
    }
}