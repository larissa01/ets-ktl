@file:Suppress("TooManyFunctions")

package br.com.digio.uber.transfer.ui.fragment

import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import androidx.core.content.ContextCompat
import androidx.lifecycle.observe
import br.com.digio.pix.repository.model.PixPaymentAndRefundResponse
import br.com.digio.pix.repository.model.PixValidatePaymentResponse
import br.com.digio.pix.ui.payment.summary.PixSummaryPaymentActivity
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.extensions.setFontFamily
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.transfer.BR
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.databinding.FragmentTransferTedValueBinding
import br.com.digio.uber.transfer.navigation.TransferOpKey
import br.com.digio.uber.transfer.ui.viewmodel.TransferTedValueViewModel
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.util.Const
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.Calendar
import java.util.concurrent.TimeUnit
import br.com.digio.pix.util.Const as PixConst

class TransferTedValueFragment :
    BNWFViewModelFragment<FragmentTransferTedValueBinding, TransferTedValueViewModel>(),
    DatePickerDialog.OnDateSetListener {

    override val bindingVariable: Int = BR.transferValueViewModel

    override val getLayoutId: Int = R.layout.fragment_transfer_ted_value

    override val viewModel: TransferTedValueViewModel by viewModel()

    override fun tag(): String = TransferOpKey.TED_VALUE.name

    override fun getStatusBarAppearance(): StatusBarColor = TransferOpKey.TED_VALUE.theme

    companion object : FragmentNewInstance<TransferTedValueFragment>(::TransferTedValueFragment) {
        private const val DATE_DURATION: Long = 60
        const val TRANSFER_MODEL = "TRANSFER_MODEL"
        private const val REQ_SUMMARY_PAYMENT = 100
    }

    private val account: AccountUIModel by lazy {
        checkNotNull(arguments?.getParcelable(TransferTedAccountDataFragment.ACCOUNT))
    }
    private val saveFavored: Boolean by lazy {
        checkNotNull(arguments?.getBoolean(TransferTedAccountDataFragment.SAVE_FAVORED, false))
    }
    private val isP2p: Boolean by lazy {
        checkNotNull(arguments?.getBoolean(TransferTedAccountDataFragment.ISP2P, false))
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        viewModel.setCalendarDate(year, month, dayOfMonth)
    }

    override fun initialize() {
        super.initialize()

        setupViewModel()
        setupObservers()
        setupOnClickListeners()
    }

    private fun setupViewModel() {
        viewModel.initializer(getPixValidatePaymentRespose()) {}
    }

    private fun setupObservers() {
        viewModel.transferDateData.observe(
            viewLifecycleOwner,
            {
                viewModel.checkTransferDateData(it)
            }
        )
        viewModel.transferValueData.observe(
            viewLifecycleOwner,
            {
                viewModel.checkTransferValueData(it)
            }
        )
        viewModel.descriptionData.observe(
            viewLifecycleOwner,
            {
                viewModel.checkDescriptionData(it)
            }
        )
        viewModel.balanceUiModel.observe(
            viewLifecycleOwner,
            {
                viewModel.setupData(account, saveFavored, isP2p)
            }
        )
        viewModel.continueLiveData.observe(
            viewLifecycleOwner,
            {
                if (it) {
                    viewModel.onBtnContinueClick()
                    arguments = Bundle().apply {
                        putParcelable(TRANSFER_MODEL, viewModel.transfer.value)
                    }
                    callGoTo(TransferOpKey.TED_REVIEW.name)
                }
            }
        )
        viewModel.continuePixLiveData.observe(
            viewLifecycleOwner
        ) {
            viewModel.onBtnContinueClick()
            val bundle = Bundle().apply {
                putParcelable(Const.Extras.ARG_PIX_SUMMARY_PAYMENT, it)
            }
            navigation.navigationToPixSummaryPayment(requireActivity(), bundle, REQ_SUMMARY_PAYMENT)
        }
    }

    private fun setupOnClickListeners() {
        binding?.let { binding ->
            binding.txtTodayDate.setOnClickListener { if (binding.rbFirst.isEnabled) binding.rbFirst.performClick() }
            binding.txtEmptyDate.setOnClickListener { binding.rbSecond.performClick() }

            binding.inputTransferValue.setEndIconOnClickListener(null)
            binding.inputTransferValue.setEndIconOnClickListener {
                viewModel.clearValue()
            }

            binding.rbSecond.setOnClickListener {
                viewModel.onScheduleClick()
                showDateDialog()
            }

            binding.rgpStatementFilter.setOnCheckedChangeListener { _, checkedId ->
                changeDateTextColors(checkedId == R.id.rb_first)
            }

            binding.edtTransferDescription.onFocusChangeListener = onFocusChanged()
        }
    }

    private fun showDateDialog() {
        val date = Calendar.getInstance()

        val dialog = DatePickerDialog(
            requireContext(),
            R.style.DatePicker,
            this,
            date.get(Calendar.YEAR),
            date.get(Calendar.MONTH),
            date.get(Calendar.DAY_OF_MONTH)
        )

        val startingDate = Calendar.getInstance().timeInMillis + TimeUnit.DAYS.toMillis(1)
        dialog.datePicker.minDate = startingDate
        dialog.datePicker.maxDate =
            Calendar.getInstance().timeInMillis + TimeUnit.DAYS.toMillis(DATE_DURATION)

        dialog.setCustomTitle(layoutInflater.inflate(R.layout.calendar_title, null))
        dialog.setOnCancelListener {
            viewModel.cancelTransferScheduling()
        }

        dialog.show()

        val positiveButton = dialog.getButton(DatePickerDialog.BUTTON_POSITIVE)
        positiveButton.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                br.com.digio.uber.common.R.color.black
            )
        )
        positiveButton.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                br.com.digio.uber.common.R.color.white
            )
        )
    }

    private fun changeDateTextColors(forTransferToday: Boolean) {
        val blackColor = br.com.digio.uber.common.R.color.black
        val greyColor = br.com.digio.uber.common.R.color.grey_757575
        val emptyColor = if (forTransferToday) greyColor else blackColor
        val todayColor = if (forTransferToday) blackColor else greyColor
        val uberMedium = br.com.digio.uber.common.R.font.uber_move_text_medium
        val uberRegular = br.com.digio.uber.common.R.font.uber_move_text_regular

        binding?.apply {
            txtEmptyDate.setTextColor(ContextCompat.getColor(requireContext(), emptyColor))
            txtTodayDate.setTextColor(ContextCompat.getColor(requireContext(), todayColor))
            txtTodayDate.setFontFamily(
                if (forTransferToday) {
                    uberMedium
                } else {
                    uberRegular
                }
            )
            txtEmptyDate.setFontFamily(
                if (forTransferToday) {
                    uberRegular
                } else {
                    uberMedium
                }
            )
            showTransferToday = forTransferToday
        }
    }

    private fun onFocusChanged() = View.OnFocusChangeListener { view, hasFocus ->
        if (hasFocus) {
            binding?.scrollviewTransfer?.smoothScrollTo(0, view.bottom)
        }
    }

    private fun getPixValidatePaymentRespose(): PixValidatePaymentResponse? =
        when {
            arguments?.containsKey(PixConst.Transfer.ARG_PIX_TRANSFER_DETAIL) == true ->
                arguments?.getParcelable(PixConst.Transfer.ARG_PIX_TRANSFER_DETAIL)
            requireActivity().intent.hasExtra(PixConst.Transfer.ARG_PIX_TRANSFER_DETAIL) ->
                requireActivity().intent.extras?.getParcelable(
                    PixConst.Transfer.ARG_PIX_TRANSFER_DETAIL
                )
            else -> null
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when {
            requestCode == REQ_SUMMARY_PAYMENT && resultCode == RESULT_OK -> {
                val result = data?.getParcelableExtra<PixPaymentAndRefundResponse>(PixSummaryPaymentActivity.ARG_RESULT)
                result?.let {
                    val bundle = Bundle().apply {
                        putParcelable(Const.Extras.ARG_PIX_RECEIPT, result)
                    }
                    navigation.navigationToPixReceipt(requireActivity(), bundle)
                    requireActivity().setResult(RESULT_OK)
                    requireActivity().finish()
                }
            }
        }
    }

    override fun getRequestCode(): MutableList<Int> = mutableListOf(REQ_SUMMARY_PAYMENT)
}