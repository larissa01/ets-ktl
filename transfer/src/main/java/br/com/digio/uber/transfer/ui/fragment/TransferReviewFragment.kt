package br.com.digio.uber.transfer.ui.fragment

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.View
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_RECEIPT
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.ReceiptOrigin
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.transfer.BR
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.databinding.FragmentTransferReviewBinding
import br.com.digio.uber.transfer.navigation.TransferOpKey
import br.com.digio.uber.transfer.ui.fragment.TransferTedValueFragment.Companion.TRANSFER_MODEL
import br.com.digio.uber.transfer.ui.viewmodel.TransferReviewViewModel
import br.com.digio.uber.transfer.uiModel.TransferUIModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.RequestOnResult.PID.KEY_PID_HASH
import br.com.digio.uber.util.Const.RequestOnResult.PID.PID_REQUEST_CODE
import br.com.digio.uber.util.Const.RequestOnResult.PID.RESULT_PID_SUCCESS
import br.com.digio.uber.util.Const.RequestOnResult.Transfer.FINISH_TRANSFER_FLOW_RESULT_CODE
import br.com.digio.uber.util.Const.RequestOnResult.Transfer.NEW_TRANSFER_REQUEST_CODE
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class TransferReviewFragment :
    BNWFViewModelFragment<FragmentTransferReviewBinding, TransferReviewViewModel>() {

    override val bindingVariable: Int = BR.reviewViewModel

    override val getLayoutId: Int = R.layout.fragment_transfer_review

    override val viewModel: TransferReviewViewModel by viewModel()

    override fun tag(): String = TransferOpKey.TED_REVIEW.name

    override fun getStatusBarAppearance(): StatusBarColor = TransferOpKey.TED_REVIEW.theme

    companion object : FragmentNewInstance<TransferReviewFragment>(::TransferReviewFragment)

    private val analytics: Analytics by inject()

    private val ted: TransferUIModel by lazy {
        checkNotNull(arguments?.getParcelable(TRANSFER_MODEL))
    }

    override fun initialize() {
        super.initialize()

        viewModel.initializer(ted) { onItemClicked(it) }
        setupObservers()
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.btn_continue_review -> {
                activity?.let { activityLet ->
                    navigation.navigationToPidActivity(
                        activityLet,
                        PidType.TRANSFER,
                        PID_REQUEST_CODE
                    )
                }
            }
        }
    }

    override fun getRequestCode(): MutableList<Int> = mutableListOf(NEW_TRANSFER_REQUEST_CODE, PID_REQUEST_CODE)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == NEW_TRANSFER_REQUEST_CODE && resultCode == RESULT_OK) {
            activity?.finish()
        } else if (requestCode == NEW_TRANSFER_REQUEST_CODE && resultCode == FINISH_TRANSFER_FLOW_RESULT_CODE) {
            activity?.setResult(FINISH_TRANSFER_FLOW_RESULT_CODE)
            activity?.finish()
        }
        if (requestCode == PID_REQUEST_CODE && resultCode == RESULT_PID_SUCCESS) {
            data?.getStringExtra(KEY_PID_HASH)?.let { hashPidLet ->
                viewModel.confirmTransfer(hashPidLet)
            }
        }
    }

    private fun setupObservers() {
        viewModel.mutableResult.observe(
            viewLifecycleOwner,
            {
                it?.let {
                    openReceipt(it.code)
                }
            }
        )
    }

    private fun openReceipt(entryId: String) {
        activity?.let {
            val bundle = Bundle()
            bundle.putString(Const.Extras.RECEIPT_ID, entryId)
            bundle.putSerializable(Const.Extras.RECEIPT_ORIGIN, ReceiptOrigin.TRANSFER)
            navigation.navigationToReceiptActivity(it, NEW_TRANSFER_REQUEST_CODE, bundle)
            analytics.pushSimpleEvent(TRANSFER_RECEIPT)
            activity?.finish()
        }
    }
}