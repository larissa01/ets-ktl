@file:Suppress("LongMethod")

package br.com.digio.uber.transfer.ui.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.extensions.hideKeyboard
import br.com.digio.uber.common.generics.makeErrorObject
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.transfer.BR
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.databinding.FragmentTransferP2pAccountDataBinding
import br.com.digio.uber.transfer.navigation.TransferOpKey
import br.com.digio.uber.transfer.ui.adapter.AccountChooseAdapter
import br.com.digio.uber.transfer.ui.fragment.TransferTedAccountDataFragment.Companion.ACCOUNT
import br.com.digio.uber.transfer.ui.fragment.TransferTedAccountDataFragment.Companion.ACCOUNT_LIMIT_LENGTH
import br.com.digio.uber.transfer.ui.fragment.TransferTedAccountDataFragment.Companion.AGENCY_LIMIT_LENGTH
import br.com.digio.uber.transfer.ui.fragment.TransferTedAccountDataFragment.Companion.ISP2P
import br.com.digio.uber.transfer.ui.fragment.TransferTedAccountDataFragment.Companion.SAVE_FAVORED
import br.com.digio.uber.transfer.ui.fragment.TransferTedAccountDataFragment.Companion.ZERO_VALUE
import br.com.digio.uber.transfer.ui.viewmodel.TransferP2PAccountDataViewModel
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.util.Const.MaskPattern.CNPJ_LENGTH
import br.com.digio.uber.util.Const.MaskPattern.CPF_LENGTH
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.validateCNPJ
import br.com.digio.uber.util.validateCpf
import org.koin.android.viewmodel.ext.android.viewModel

class TransferP2PAccountDataFragment :
    BNWFViewModelFragment<FragmentTransferP2pAccountDataBinding,
        TransferP2PAccountDataViewModel>(),
    AccountChooseAdapter.OnItemSelectedListener {

    override fun tag(): String = TransferOpKey.P2P_ACCOUNT_DATA.name

    override fun getStatusBarAppearance(): StatusBarColor = TransferOpKey.P2P_ACCOUNT_DATA.theme

    override val bindingVariable: Int = BR.accountDataViewModel

    override val getLayoutId = R.layout.fragment_transfer_p2p_account_data

    companion object : FragmentNewInstance<TransferP2PAccountDataFragment>(::TransferP2PAccountDataFragment)

    override val viewModel: TransferP2PAccountDataViewModel by viewModel()

    override fun initialize() {
        super.initialize()

        setupObservers()
        setupListeners()
        setupTextWatchers()
    }

    private fun setupTextWatchers() {
        binding?.let {
            it.edtAgency.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        if (s?.length ?: ZERO_VALUE >= AGENCY_LIMIT_LENGTH) {
                            it.edtAccount.requestFocus()
                        }
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
                }
            )

            it.edtAccount.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        if (s?.length ?: ZERO_VALUE >= ACCOUNT_LIMIT_LENGTH) {
                            it.edtDigit.requestFocus()
                        }
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
                }
            )

            it.edtCpf.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        val document = s.toString().unmask()

                        if (document.length == CPF_LENGTH && !document.validateCpf()) {
                            it.inputCpf.error = getString(R.string.ac_invalid_cpf)
                            it.inputCpf.isErrorEnabled = true
                        } else if (document.length >= CNPJ_LENGTH && !document.validateCNPJ()) {
                            it.inputCpf.error = getString(R.string.ac_invalid_cnpj)
                            it.inputCpf.isErrorEnabled = true
                        } else {
                            it.inputCpf.isErrorEnabled = false
                        }
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
                }
            )
        }
    }

    private fun setupListeners() {
        binding?.let {
            it.edtCpf.onFocusChangeListener = onFocusChanged()
            it.inputCpf.error = getString(R.string.ac_invalid_cpf)

            it.edtCpf.setOnEditorActionListener { v, _, _ ->
                v.hideKeyboard()
                v.clearFocus()
                true
            }

            it.edtCpf.setOnFocusChangeListener { _, hasFocus -> if (hasFocus) viewModel.clearAgencyAccountDigit() }
            it.edtDigit.setOnFocusChangeListener { _, hasFocus -> if (hasFocus) viewModel.clearCpfField() }
            it.edtAccount.setOnFocusChangeListener { _, hasFocus -> if (hasFocus) viewModel.clearCpfField() }
        }
    }

    private fun setupObservers() {
        viewModel.account.observe(
            viewLifecycleOwner,
            {
                if (it != null) {
                    arguments = Bundle().apply {
                        putParcelable(ACCOUNT, it)
                        putBoolean(SAVE_FAVORED, viewModel.shouldSaveFavored() ?: false)
                        putBoolean(ISP2P, true)
                    }
                    callGoTo(TransferOpKey.TED_VALUE.name)
                }
            }
        )

        viewModel.accounts.observe(
            viewLifecycleOwner,
            {
                it?.let { accounts ->
                    if (accounts.size > 1) {
                        showAccountsDialog()
                    } else {
                        goToTedValue(accounts.first())
                    }
                }
            }
        )

        viewModel.tedAccountUIModel.continueActivation.observe(
            requireActivity(),
            {
                if (!it && viewModel.isCpfEntire()) {
                    binding?.inputCpf?.isErrorEnabled = true
                    binding?.inputCpf?.error = getString(R.string.ac_invalid_cpf)
                } else {
                    binding?.inputCpf?.error = null
                    binding?.inputCpf?.isErrorEnabled = false
                }
            }
        )

        viewModel.errorMessage.observe(
            viewLifecycleOwner,
            {
                if (it.isNotEmpty()) {
                    showMessage(
                        makeErrorObject {
                            titleString = it
                        }
                    )
                }
            }
        )

        viewModel.tedAccountUIModel.agencyField.observe(viewLifecycleOwner, { viewModel.checkAccountFields() })
        viewModel.tedAccountUIModel.accountField.observe(viewLifecycleOwner, { viewModel.checkAccountFields() })
        viewModel.tedAccountUIModel.digitField.observe(viewLifecycleOwner, { viewModel.checkAccountFields() })
        viewModel.tedAccountUIModel.documentField.observe(
            viewLifecycleOwner,
            {
                viewModel.checkDocumentField()
            }
        )
        viewModel.mutableAccount.observe(viewLifecycleOwner, {})
    }

    private fun showAccountsDialog() {
        val dialog = ChooseAccountDialogFragment.newInstance(viewModel, this)

        activity?.supportFragmentManager?.let { fm ->
            dialog.show(fm, ChooseAccountDialogFragment.CHOOSE_ACCOUNT_DIALOG_TAG)
        }
    }

    private fun goToTedValue(account: AccountUIModel) {
        arguments = Bundle().apply {
            putParcelable(ACCOUNT, account)
            putBoolean(SAVE_FAVORED, viewModel.shouldSaveFavored() ?: false)
            putBoolean(ISP2P, true)
        }
        callGoTo(TransferOpKey.TED_VALUE.name)
    }

    private fun onFocusChanged() = View.OnFocusChangeListener { view, hasFocus ->
        if (hasFocus) {
            binding?.scrollviewTransfer?.smoothScrollTo(0, view.bottom)
        }
    }

    override fun onItemSelected(account: AccountUIModel) {
        goToTedValue(account)
    }
}