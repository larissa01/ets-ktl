package br.com.digio.uber.transfer.ui.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.model.transfer.TransferMode
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.ui.fragment.TransferTedAccountDataFragment
import br.com.digio.uber.transfer.ui.fragment.TransferTedFavoredFragment

class TEDTransferPagerAdapter(
    private val context: Context,
    private val navigation: GenericNavigationWithFlow,
    private val transferMode: TransferMode,
    fm: FragmentManager
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> TransferTedAccountDataFragment.newInstance(navigation, null)
            1 -> TransferTedFavoredFragment.newInstance(navigation, null)
            else -> {
                throw ArrayIndexOutOfBoundsException("Invalid pager size")
            }
        }
    }

    override fun getCount() = when (transferMode) {
        TransferMode.TED -> 2
        TransferMode.PIX -> 1
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.account_data)
            else -> context.getString(R.string.favored)
        }
    }
}