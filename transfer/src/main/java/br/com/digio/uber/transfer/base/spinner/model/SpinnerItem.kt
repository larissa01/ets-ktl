package br.com.digio.uber.transfer.base.spinner.model

import br.com.digio.uber.transfer.base.adapter.BaseAdapterItem
import br.com.digio.uber.transfer.enums.UiType

data class SpinnerItem(
    val title: String,
    val code: Int
) : BaseAdapterItem(UiType.SPINNER_ITEM)