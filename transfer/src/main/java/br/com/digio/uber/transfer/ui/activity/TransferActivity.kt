package br.com.digio.uber.transfer.ui.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.common.balancetoolbar.balance.di.balanceModule
import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.helper.ViewIds
import br.com.digio.uber.common.navigation.GenericNavigation
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.di.transferViewModelModule
import br.com.digio.uber.transfer.navigation.TransferNavigation
import org.koin.android.ext.android.inject
import org.koin.core.module.Module

class TransferActivity : BaseActivity() {

    private val analytics: Analytics by inject()
    private val transferNavigation = TransferNavigation(this, analytics)
    override fun getLayoutId(): Int = LayoutIds.navigationLayoutWithToolbarActivity
    override fun getModule(): List<Module> = listOf(transferViewModelModule, balanceModule)
    override fun getNavigation(): GenericNavigation = transferNavigation
    override fun getToolbar(): Toolbar? = findViewById<Toolbar>(ViewIds.toolbarId).apply {
        this.title = getString(R.string.transfer)
        this.navigationIcon = ContextCompat.getDrawable(context, br.com.digio.uber.common.R.drawable.ic_back_arrow)
    }
    override fun showDisplayShowTitle(): Boolean = true

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        transferNavigation.init(intent.extras)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}