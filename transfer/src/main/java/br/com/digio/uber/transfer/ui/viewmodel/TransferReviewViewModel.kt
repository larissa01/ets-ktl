package br.com.digio.uber.transfer.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_REVIEW
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.transfer.CanTransferTodayUseCase
import br.com.digio.uber.domain.usecase.transfer.ConfirmExternalTransferUseCase
import br.com.digio.uber.domain.usecase.transfer.ConfirmInternalTransferUseCase
import br.com.digio.uber.model.transfer.ExternalTransferDomainRequest
import br.com.digio.uber.model.transfer.InternalTransferDomainRequest
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.enums.OwnershipType
import br.com.digio.uber.transfer.enums.TransferAccountType
import br.com.digio.uber.transfer.mapper.ConfirmedTransferMapper
import br.com.digio.uber.transfer.mapper.TransferStatusMapper
import br.com.digio.uber.transfer.uiModel.TransferResultUIModel
import br.com.digio.uber.transfer.uiModel.TransferUIModel
import br.com.digio.uber.util.DateFormatter
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.validateCNPJ
import java.util.Calendar
import java.util.Date

class TransferReviewViewModel(
    private val confirmInternalTransferUseCase: ConfirmInternalTransferUseCase,
    private val confirmExternalTransferUseCase: ConfirmExternalTransferUseCase,
    private val canTransferTodayUseCase: CanTransferTodayUseCase,
    private val resourceManager: ResourceManager,
    private val analytics: Analytics
) : BaseViewModel() {

    val transferLiveData = MutableLiveData<TransferUIModel>()

    val mutableResult = MutableLiveData<TransferResultUIModel>()

    val transferTypeField = MutableLiveData<String>()

    val accountTypeText = MutableLiveData<String>()

    val tedVisibility = MutableLiveData<Boolean>()

    val dateField = MutableLiveData<String>()
    val dateLabel = MutableLiveData<String>()
    val tariff = MutableLiveData<String>()

    val transferDisclaimer = MutableLiveData<String>()

    val reviewScreenTitle = MutableLiveData<String>()

    val documentType = MutableLiveData<String>().apply {
        value = ""
    }

    lateinit var agency: String
    lateinit var account: String
    lateinit var cpf: String
    lateinit var name: String
    lateinit var amount: String
    var saveBeneficiary: Boolean = true
    lateinit var description: String
    var date: String? = null
    var accountType: TransferAccountType? = null
    lateinit var bankCode: String
    var isSameOwner: Boolean = false

    fun initializer(transfer: TransferUIModel, onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        transferLiveData.value = transfer
        tedVisibility.value = if (transferLiveData.value?.isP2P == true) {
            transferTypeField.value = resourceManager.getString(R.string.digio_transfer_type)
            accountTypeText.value = resourceManager.getString(R.string.digio_account)
            false
        } else {
            transferTypeField.value = resourceManager.getString(R.string.ted)
            accountTypeText.value = TransferAccountType.getStringId(
                transferLiveData.value?.account?.accountType!!,
                transfer.account?.cpf!!
            )?.let {
                resourceManager.getString(it)
            }
            true
        }

        documentType.value =
            if (transfer.account?.cpf?.unmask()?.validateCNPJ() == true) {
                resourceManager.getString(R.string.cnpj)
            } else {
                resourceManager.getString(
                    R.string.cpf
                )
            }

        agency =
            checkNotNull(transferLiveData.value?.account?.agencyCode?.unmask()?.toInt().toString())
        account =
            checkNotNull(transferLiveData.value?.account?.accountNumber?.unmask())
        cpf = checkNotNull(transferLiveData.value?.account?.cpf)
        name = checkNotNull(transferLiveData.value?.account?.userName)
        amount = checkNotNull(transferLiveData.value?.value)
        saveBeneficiary = checkNotNull(transferLiveData.value?.saveBeneficiary)
        description = checkNotNull(transferLiveData.value?.description)
        date = transferLiveData.value?.date?.let {
            DateFormatter.dateUSFormatter.format(
                DateFormatter.dateBRFormatter.parse(it)
            )
        }
        accountType = transferLiveData.value?.account?.accountType
        bankCode = checkNotNull(transferLiveData.value?.account?.bank?.code?.toInt().toString())
        isSameOwner =
            checkNotNull(transferLiveData.value?.account?.ownership == OwnershipType.SAME_OWNERSHIP)

        if (date == null || date == DateFormatter.dateUSFormatter.format(Date())) {
            transferDisclaimer.value = resourceManager.getString(R.string.transfer_disclaimer_two)
            dateLabel.value = resourceManager.getString(R.string.ac_send_date)
            reviewScreenTitle.value = resourceManager.getString(R.string.review)
        } else {
            transferDisclaimer.value =
                resourceManager.getString(R.string.transfer_disclaimer_schedule)
            dateLabel.value = resourceManager.getString(R.string.ac_scheduled_for)
            reviewScreenTitle.value = resourceManager.getString(R.string.schedule_review)
        }
        dateField.value = transferLiveData.value?.date ?: getToday()

        setTariff()
    }

    private fun setTariff() {
        if (dateField.value == getToday()) {
            tariff.value = resourceManager.getString(R.string.zero_value)

            canTransferTodayUseCase(Unit).singleExec(
                onSuccessBaseViewModel = {
                    val statusUIModel = TransferStatusMapper().map(it)
                    tariff.value = if (statusUIModel.showFreeTransactions == true) resourceManager.getString(
                        R.string.zero_value
                    )
                    else statusUIModel.overlimitTax
                }
            )
        } else {
            tariff.value = resourceManager.getString(R.string.tariff_value_schedule)
        }
    }

    fun confirmTransfer(hashPid: String) {
        if (transferLiveData.value?.isP2P == false) {
            val request = ExternalTransferDomainRequest(
                agency = agency,
                account = account,
                cpf = cpf,
                name = name,
                amount = amount,
                saveBeneficiary = saveBeneficiary,
                description = description,
                password = hashPid,
                date = date,
                accountType = accountType?.name ?: "",
                bankCode = bankCode,
                isSameOwner = isSameOwner
            )
            confirmExternalTransferUseCase(request).singleExec(
                showLoadingFlag = true,
                onSuccessBaseViewModel = {
                    analytics.pushSimpleEvent(TRANSFER_REVIEW)
                    mutableResult.value = ConfirmedTransferMapper().map(it)
                }
            )
        } else {
            val request = InternalTransferDomainRequest(
                agency = agency,
                account = account,
                cpf = cpf,
                name = name,
                amount = amount,
                saveBeneficiary = saveBeneficiary,
                description = description,
                password = hashPid,
                date = date
            )
            confirmInternalTransferUseCase(request).singleExec(
                showLoadingFlag = true,
                onSuccessBaseViewModel = {
                    analytics.pushSimpleEvent(TRANSFER_REVIEW)
                    mutableResult.value = ConfirmedTransferMapper().map(it)
                }
            )
        }
    }

    private fun getToday(): String {
        return DateFormatter.dateBRFormatter.format(Calendar.getInstance().time)
    }
}