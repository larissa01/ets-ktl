package br.com.digio.uber.transfer.di

import br.com.digio.uber.transfer.ui.viewmodel.TransferHomeViewModel
import br.com.digio.uber.transfer.ui.viewmodel.TransferP2PAccountDataViewModel
import br.com.digio.uber.transfer.ui.viewmodel.TransferP2PFavoredViewModel
import br.com.digio.uber.transfer.ui.viewmodel.TransferP2PViewModel
import br.com.digio.uber.transfer.ui.viewmodel.TransferReviewViewModel
import br.com.digio.uber.transfer.ui.viewmodel.TransferTedAccountDataViewModel
import br.com.digio.uber.transfer.ui.viewmodel.TransferTedFavoredViewModel
import br.com.digio.uber.transfer.ui.viewmodel.TransferTedValueViewModel
import br.com.digio.uber.transfer.ui.viewmodel.TransferTedViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val transferViewModelModule = module {
    viewModel { TransferHomeViewModel() }
    viewModel { TransferTedViewModel() }
    viewModel { TransferTedAccountDataViewModel(get(), get(), get(), get(), get(), get(), get()) }
    viewModel { TransferTedFavoredViewModel(get(), get(), get(), get(), get(), get()) }
    viewModel { TransferP2PViewModel() }
    viewModel { TransferTedValueViewModel(get(), get(), get(), get()) }
    viewModel { TransferReviewViewModel(get(), get(), get(), get(), get()) }
    viewModel { TransferP2PAccountDataViewModel(get(), get(), get(), get(), get()) }
    viewModel { TransferP2PFavoredViewModel(get(), get(), get(), get(), get()) }
}