package br.com.digio.uber.transfer.ui.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.ui.fragment.TransferP2PAccountDataFragment
import br.com.digio.uber.transfer.ui.fragment.TransferP2PFavoredFragment

class P2PTransferPagerAdapter(
    private val context: Context,
    private val navigation: GenericNavigationWithFlow,
    fm: FragmentManager
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> TransferP2PAccountDataFragment.newInstance(navigation, null)
            1 -> TransferP2PFavoredFragment.newInstance(navigation, null)
            else -> {
                throw ArrayIndexOutOfBoundsException("Invalid pager size")
            }
        }
    }

    override fun getCount() = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.account_data)
            else -> context.getString(R.string.favored)
        }
    }
}