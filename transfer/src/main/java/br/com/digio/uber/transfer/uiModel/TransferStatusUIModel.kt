package br.com.digio.uber.transfer.uiModel

@SuppressWarnings("LongParameterList")
class TransferStatusUIModel(
    val canTransfer: Boolean,
    val geralMinAmount: String,
    val geralMaxAmount: String,
    val maxQtdOverlimit: String? = null,
    val overlimitTax: String? = null,
    val maxQtdPerDay: String? = null,
    val maxQtdPerMonth: String? = null,
    val usedQtdMonth: String? = null,
    val usedQtdDay: String? = null,
    val showFreeTransactions: Boolean? = null
)