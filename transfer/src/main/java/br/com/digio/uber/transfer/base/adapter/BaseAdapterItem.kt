package br.com.digio.uber.transfer.base.adapter

import br.com.digio.uber.transfer.enums.UiType

open class BaseAdapterItem(var uiType: UiType)