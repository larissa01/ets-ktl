package br.com.digio.uber.transfer.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.base.adapter.BaseAdapter
import br.com.digio.uber.transfer.base.adapter.BaseViewHolder
import br.com.digio.uber.transfer.base.adapter.UnsupportedViewTypException
import br.com.digio.uber.transfer.base.spinner.view.BankViewHolder
import br.com.digio.uber.transfer.base.spinner.view.MoreBanksViewHolder
import br.com.digio.uber.transfer.enums.UiType
import br.com.digio.uber.transfer.uiModel.BankUIModel

class BankListAdapter(
    bankList: MutableList<BankUIModel> = mutableListOf(),
    private val listener: OnBankSelectedListener
) : BaseAdapter<BankUIModel>(bankList) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<BankUIModel> {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            UiType.SPINNER_ITEM.value -> BankViewHolder(
                DataBindingUtil.inflate(inflater, R.layout.item_spinner, parent, false)
            )
            UiType.BANK_MORE_ITEMS.value -> MoreBanksViewHolder(
                DataBindingUtil.inflate(inflater, R.layout.item_more_banks, parent, false)
            )
            else ->
                throw UnsupportedViewTypException(viewType)
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<BankUIModel>, position: Int) {
        super.onBindViewHolder(holder, position)
        if (holder is MoreBanksViewHolder) {
            holder.itemView.setOnClickListener {
                listener.onMoreBanksSelected()
            }
        } else {
            holder.itemView.setOnClickListener {
                listener.onBankSelected(list[position].code)
            }
        }
    }

    interface OnBankSelectedListener {
        fun onMoreBanksSelected()
        fun onBankSelected(code: String)
    }
}