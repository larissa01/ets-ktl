package br.com.digio.uber.transfer.base.spinner.view

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.enums.UiType
import br.com.digio.uber.transfer.ui.adapter.BankListAdapter
import br.com.digio.uber.transfer.uiModel.BankUIModel
import kotlinx.android.synthetic.main.dialog_spinner_list.recyclerview_banks

class BankListDialog(
    context: Context,
    private val list: List<BankUIModel>,
    private val listener: OnBankSelectListener
) : Dialog(context),
    BankListAdapter.OnBankSelectedListener {

    override fun onMoreBanksSelected() {
        listener.onMoreBanksClick()
        dismiss()
    }

    override fun onBankSelected(code: String) {
        listener.onBankSelected(code)
        dismiss()
    }

    private lateinit var banksAdapter: BankListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_spinner_list)

        window?.let {
            it.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            it.setGravity(Gravity.CENTER)
        }

        createRecyclerView()
    }

    private fun createRecyclerView() {
        val newList = list.toMutableList()
        if (!listener.hasAllBanks()) {
            newList.add(BankUIModel(code = "", name = "").apply { uiType = UiType.BANK_MORE_ITEMS })
        }

        banksAdapter = BankListAdapter(newList, this)
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        recyclerview_banks.adapter = banksAdapter
        recyclerview_banks.layoutManager = layoutManager
    }

    interface OnBankSelectListener {
        fun onBankSelected(code: String)
        fun onMoreBanksClick()
        fun hasAllBanks(): Boolean
    }
}