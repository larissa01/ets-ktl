package br.com.digio.uber.transfer.mapper

import br.com.digio.uber.model.transfer.TransferStatus
import br.com.digio.uber.transfer.uiModel.TransferStatusUIModel
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.util.toMoney

class TransferStatusMapper : AbstractMapper<TransferStatus, TransferStatusUIModel> {
    override fun map(param: TransferStatus): TransferStatusUIModel = with(param) {
        return TransferStatusUIModel(
            canTransfer = openSpb,
            geralMinAmount = availableLimits.generalLimits.minAmount.toMoney(),
            geralMaxAmount = availableLimits.generalLimits.maxAmount.toMoney(),
            maxQtdOverlimit = availableLimits.generalLimits.maxQtdOverlimit.toString(),
            overlimitTax = availableLimits.overlimitTax?.toMoney(),
            maxQtdPerDay = availableLimits.generalLimits.maxQtdPerDay.toString(),
            maxQtdPerMonth = availableLimits.generalLimits.maxQtdPerMonth.toString(),
            usedQtdMonth = availableLimits.userUsage.qtdForMonth.toString(),
            usedQtdDay = availableLimits.userUsage.qtdForMonth.toString(),
            showFreeTransactions = !availableLimits.mustChargeOverlimitTax
        )
    }
}