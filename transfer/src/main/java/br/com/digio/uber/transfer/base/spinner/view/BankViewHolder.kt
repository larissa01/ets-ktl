package br.com.digio.uber.transfer.base.spinner.view

import br.com.digio.uber.transfer.base.adapter.BaseViewHolder
import br.com.digio.uber.transfer.databinding.ItemSpinnerBinding
import br.com.digio.uber.transfer.uiModel.BankUIModel

class BankViewHolder(override val binding: ItemSpinnerBinding) :
    BaseViewHolder<BankUIModel>(binding) {

    private lateinit var bankUiModel: BankUIModel

    override fun bind(item: BankUIModel) {
        this.bankUiModel = item
        binding.txtSpinnerTitle.text = bankUiModel.title
    }
}