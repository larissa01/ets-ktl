package br.com.digio.uber.transfer.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel

class TransferActivityViewModel : BaseViewModel() {

    var navHostId = MutableLiveData<Int>()
}