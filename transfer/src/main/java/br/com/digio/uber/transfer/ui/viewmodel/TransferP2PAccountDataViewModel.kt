@file:Suppress("TooManyFunctions")

package br.com.digio.uber.transfer.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_ACCOUNT_DATA
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_ACCOUNT_DATA_ERROR
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.usecase.account.GetAccountUseCase
import br.com.digio.uber.domain.usecase.transfer.GetAccountByCPFUseCase
import br.com.digio.uber.domain.usecase.transfer.SearchAccountUseCase
import br.com.digio.uber.model.account.AccountResponse
import br.com.digio.uber.model.transfer.AccountSearchDomainRequest
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.mapper.AccountMapper
import br.com.digio.uber.transfer.mapper.AccountsMapper
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.transfer.uiModel.TedAccountDataUIModel
import br.com.digio.uber.util.formatAsAgencyNumber
import br.com.digio.uber.util.safeLet
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.validateCpf
import br.com.digio.uber.util.validateDocument

class TransferP2PAccountDataViewModel(
    private val getAccountByCPFUseCase: GetAccountByCPFUseCase,
    private val searchAccountUseCase: SearchAccountUseCase,
    getAccountUseCase: GetAccountUseCase,
    private val resourceManager: ResourceManager,
    private val analytics: Analytics
) : BaseViewModel() {

    val tedAccountUIModel = TedAccountDataUIModel()

    private val accountsLiveData = MutableLiveData<List<AccountUIModel>>()
    private val accountLiveData = MutableLiveData<AccountUIModel>()

    var mutableAccount: LiveData<AccountResponse> = getAccountUseCase(Unit).exec(showLoadingFlag = true)

    val account: LiveData<AccountUIModel> = accountLiveData
    val accounts: LiveData<List<AccountUIModel>> = accountsLiveData

    val errorMessage = MutableLiveData<String>()

    init {
        tedAccountUIModel.agencyField.value = DEFAULT_AGENCY.formatAsAgencyNumber()
    }

    private fun validateAccountField() {
        checkSearchable()
        if (!tedAccountUIModel.documentField.value.isNullOrBlank() &&
            !tedAccountUIModel.accountField.value.isNullOrBlank()
        ) {
            tedAccountUIModel.documentField.value = ""
        }
    }

    private fun validateCPF() {
        if (!tedAccountUIModel.documentField.value.isNullOrBlank()) {
            tedAccountUIModel.accountField.value = ""
            tedAccountUIModel.digitField.value = ""
            checkSearchable()
        } else {
            checkAccountFields()
        }
    }

    private fun checkSearchable() {
        tedAccountUIModel.continueActivation.value =
            tedAccountUIModel.documentField.value.validateCpf() || validateAccountFields()
    }

    fun checkAccountFields() {
        validateAccountField()
    }

    fun checkDocumentField() {
        validateCPF()
    }

    private fun searchAccountByCPF(document: String) {
        if (checkIfDocumentIsFromSameUser(document)) {
            return
        }

        tedAccountUIModel.documentField.value?.let { cpf ->
            getAccountByCPFUseCase(cpf.unmask()).singleExec(
                onSuccessBaseViewModel = {
                    clearCpfField()
                    analytics.pushSimpleEvent(TRANSFER_ACCOUNT_DATA)
                    accountsLiveData.value = AccountsMapper(resourceManager).map(it)
                }
            )
        }
    }

    private fun searchAccount() {
        val account = tedAccountUIModel.accountField.value?.unmask()?.trim()

        safeLet(
            DEFAULT_AGENCY,
            account,
            tedAccountUIModel.digitField.value,
            { ag, acc, digit ->
                val request = AccountSearchDomainRequest(ag, acc, digit)

                searchAccountUseCase(request).singleExec(
                    onSuccessBaseViewModel = {
                        if (!checkIfDocumentIsFromSameUser(it.cpf)) {
                            analytics.pushSimpleEvent(TRANSFER_ACCOUNT_DATA)
                            accountLiveData.value = AccountMapper(resourceManager).map(it)
                        }
                    }
                )
            }
        )
    }

    fun search() {
        if (!checkIfDocumentIsFromSameUser(tedAccountUIModel.documentField.value!!)) {
            return when {
                tedAccountUIModel.documentField.value.validateDocument() ->
                    searchAccountByCPF(tedAccountUIModel.documentField.value!!)
                validateAccountFields() -> searchAccount()
                else -> {
                    val message = resourceManager.getString(R.string.invalid_fields)
                    errorMessage.value = message
                    analytics.pushSimpleEvent(
                        TRANSFER_ACCOUNT_DATA_ERROR,
                        TRANSFER_ACCOUNT_DATA_ERROR to message
                    )
                    clearCpfField()
                }
            }
        }
    }

    private fun checkIfDocumentIsFromSameUser(document: String): Boolean {
        if (document.unmask() == mutableAccount.value?.cpf?.unmask()) {
            val message = resourceManager.getString(R.string.cant_transfer_to_same_cpf)
            errorMessage.value = message
            analytics.pushSimpleEvent(
                TRANSFER_ACCOUNT_DATA_ERROR,
                TRANSFER_ACCOUNT_DATA_ERROR to message
            )
            return true
        }
        return false
    }

    private fun validateAccountFields(): Boolean {
        return !tedAccountUIModel.agencyField.value.isNullOrBlank() &&
            !tedAccountUIModel.accountField.value.isNullOrBlank() &&
            !tedAccountUIModel.digitField.value.isNullOrBlank()
    }

    fun isCpfEntire(): Boolean {
        tedAccountUIModel.documentField.value?.let {
            return it.length == MAX_SIZE_CPF
        }
        return false
    }

    fun clearCpfField() {
        tedAccountUIModel.documentField.value = ""
    }

    fun clearAgencyAccountDigit() {
        tedAccountUIModel.accountField.value = ""
        tedAccountUIModel.digitField.value = ""
    }

    fun shouldSaveFavored(): Boolean? {
        return tedAccountUIModel.saveFavored.value
    }

    companion object {
        const val DEFAULT_AGENCY = "1"
        const val MAX_SIZE_CPF = 14
    }
}