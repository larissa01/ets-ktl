package br.com.digio.uber.transfer.uiModel

import android.os.Parcelable
import br.com.digio.pix.repository.model.PixValidatePaymentResponse
import kotlinx.android.parcel.Parcelize

/**
 * @author Marlon D. Rocha
 * @since 06/11/20
 */
@Parcelize
data class TransferUIModel(
    var account: AccountUIModel? = null,
    var description: String = "",
    var value: String = "0,00",
    var date: String? = null,
    var saveBeneficiary: Boolean = false,
    var isP2P: Boolean = false,
    var pixTransferDetail: PixValidatePaymentResponse? = null
) : Parcelable