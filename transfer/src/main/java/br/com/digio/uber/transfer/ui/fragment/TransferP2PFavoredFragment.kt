package br.com.digio.uber.transfer.ui.fragment

import android.annotation.SuppressLint
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_ACCOUNT_DATA
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.transfer.BR
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.databinding.FragmentTransferP2pFavoredBinding
import br.com.digio.uber.transfer.navigation.TransferOpKey
import br.com.digio.uber.transfer.ui.adapter.FavoredAdapter
import br.com.digio.uber.transfer.ui.fragment.DeleteFavoredDialogFragment.Companion.DELETE_FAVORED_DIALOG_TAG
import br.com.digio.uber.transfer.ui.viewmodel.TransferP2PFavoredViewModel
import br.com.digio.uber.transfer.uiModel.FavoredItem
import kotlinx.android.synthetic.main.fragment_transfer_ted_favored.*
import kotlinx.android.synthetic.main.item_favored.view.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class TransferP2PFavoredFragment : BNWFViewModelFragment<FragmentTransferP2pFavoredBinding,
    TransferP2PFavoredViewModel>() {

    override val viewModel: TransferP2PFavoredViewModel by viewModel()
    override fun tag(): String = TransferOpKey.P2P_FAVORED.name
    override fun getStatusBarAppearance(): StatusBarColor = TransferOpKey.P2P_FAVORED.theme
    override val bindingVariable: Int = BR.favoredViewModel
    override val getLayoutId = R.layout.fragment_transfer_p2p_favored

    private val analytics: Analytics by inject()
    private val favoredAdapter: FavoredAdapter by lazy {
        FavoredAdapter(
            listener = {
                showPopupWindow(it as FavoredItem)
            }
        )
    }

    companion object : FragmentNewInstance<TransferP2PFavoredFragment>(::TransferP2PFavoredFragment)

    override fun initialize() {
        super.initialize()

        bindRecyclerView()
        setupObservers()
    }

    private fun bindRecyclerView() {
        recyclerview_favored.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recyclerview_favored.adapter = favoredAdapter
    }

    private fun setupObservers() {
        viewModel.favored.observe(
            viewLifecycleOwner,
            {
                favoredAdapter.replaceItems(it)
            }
        )

        viewModel.selectedAccount.observe(
            viewLifecycleOwner,
            {
                it?.let {
                    analytics.pushSimpleEvent(TRANSFER_ACCOUNT_DATA)
                    arguments = Bundle().apply {
                        putParcelable(TransferTedAccountDataFragment.ACCOUNT, it)
                        putBoolean(TransferTedAccountDataFragment.ISP2P, true)
                    }
                    callGoTo(TransferOpKey.TED_VALUE.name)
                }
            }
        )

        viewModel.mutableFavoredEdition.observe(
            viewLifecycleOwner,
            {
                favoredAdapter.updateEdition(it)
            }
        )

        viewModel.favored.observe(viewLifecycleOwner, {})
    }

    private val arrayCount: Int = 2

    @SuppressLint("InflateParams")
    private fun showPopupWindow(item: FavoredItem) {
        val itemView = binding?.recyclerviewFavored?.layoutManager?.findViewByPosition(item.position)
        itemView?.let {
            val inflater = LayoutInflater.from(it.context)

            PopupWindow(itemView.context).apply {
                contentView = inflater.inflate(R.layout.popup_p2p_favored_item, null)
                width = LinearLayout.LayoutParams.WRAP_CONTENT
                height = LinearLayout.LayoutParams.WRAP_CONTENT
                isFocusable = true
                setBackgroundDrawable(
                    ColorDrawable(
                        ContextCompat.getColor(
                            itemView.context,
                            android.R.color.transparent
                        )
                    )
                )

                val location = IntArray(arrayCount)

                itemView.button_favored_options.getLocationOnScreen(location)

                val resources = itemView.context.resources

                val xOffset = resources.getDimensionPixelSize(R.dimen.favoredPopupWindow_xOffset)
                val yOffset = resources.getDimensionPixelSize(R.dimen.favoredPopupWindow_yOffset)

                contentView.findViewById<TextView>(R.id.textView_popupWindowFavored_label_delete)
                    .setOnClickListener {
                        showDeleteDialog(item)
                        this.dismiss()
                    }

                showAtLocation(
                    itemView,
                    Gravity.NO_GRAVITY,
                    (location[0] - xOffset),
                    (location[1] + yOffset)
                )
            }
        }
    }

    private fun showDeleteDialog(item: FavoredItem) {
        val dialog =
            DeleteFavoredDialogFragment.newInstance(item)
        activity?.supportFragmentManager?.let {
            dialog.show(it, DELETE_FAVORED_DIALOG_TAG)
        }
    }
}