package br.com.digio.uber.transfer.mapper

import br.com.digio.uber.model.transfer.TransferResponse
import br.com.digio.uber.transfer.uiModel.TransferResultUIModel
import br.com.digio.uber.util.mapper.AbstractMapper

class ConfirmedTransferMapper :
    AbstractMapper<TransferResponse, TransferResultUIModel> {
    override fun map(param: TransferResponse): TransferResultUIModel = with(param) {
        TransferResultUIModel(transactionId)
    }
}