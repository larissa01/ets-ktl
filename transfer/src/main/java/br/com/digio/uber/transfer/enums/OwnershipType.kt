package br.com.digio.uber.transfer.enums

import br.com.digio.uber.transfer.R

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */
enum class OwnershipType(val resId: Int) {
    SAME_OWNERSHIP(R.string.same_ownership),
    OTHER_OWNERSHIP(R.string.other_ownership)
}