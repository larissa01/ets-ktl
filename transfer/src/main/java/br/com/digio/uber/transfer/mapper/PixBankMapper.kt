package br.com.digio.uber.transfer.mapper

import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.model.transfer.PixBankDomainResponse
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.uiModel.BankUIModel
import br.com.digio.uber.util.mapper.AbstractMapper

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */
class PixBankMapper(private val resourceManager: ResourceManager) :
    AbstractMapper<PixBankDomainResponse, ArrayList<BankUIModel>> {

    override fun map(param: PixBankDomainResponse): ArrayList<BankUIModel> = with(param) {
        ArrayList(
            bankDomains.map {
                BankUIModel(
                    it.code,
                    resourceManager.getString(R.string.bank_title, it.code, it.name),
                    it.name
                )
            }
        )
    }
}