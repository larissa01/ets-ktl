package br.com.digio.uber.transfer.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.base.adapter.BaseAdapter
import br.com.digio.uber.transfer.base.adapter.BaseViewHolder
import br.com.digio.uber.transfer.base.adapter.UnsupportedViewTypException
import br.com.digio.uber.transfer.base.spinner.model.SpinnerItem
import br.com.digio.uber.transfer.base.spinner.view.SpinnerViewHolder
import br.com.digio.uber.transfer.enums.UiType

class SpinnerListAdapter(
    spinnerList: ArrayList<SpinnerItem> = arrayListOf(),
    val listener: OnItemSelectedListener
) : BaseAdapter<SpinnerItem>(spinnerList) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<SpinnerItem> {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            UiType.SPINNER_ITEM.value -> SpinnerViewHolder(
                DataBindingUtil.inflate(inflater, R.layout.item_spinner, parent, false)
            )
            else ->
                throw UnsupportedViewTypException(viewType)
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<SpinnerItem>, position: Int) {
        super.onBindViewHolder(holder, position)

        holder.itemView.setOnClickListener {
            listener.onItemSelected(list[position].code)
        }
    }

    interface OnItemSelectedListener {
        fun onItemSelected(code: Int)
    }
}