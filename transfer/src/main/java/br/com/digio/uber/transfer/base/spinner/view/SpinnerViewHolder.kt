package br.com.digio.uber.transfer.base.spinner.view

import br.com.digio.uber.transfer.base.adapter.BaseViewHolder
import br.com.digio.uber.transfer.base.spinner.model.SpinnerItem
import br.com.digio.uber.transfer.databinding.ItemSpinnerBinding

class SpinnerViewHolder(override val binding: ItemSpinnerBinding) :
    BaseViewHolder<SpinnerItem>(binding) {

    private lateinit var bankUiModel: SpinnerItem

    override fun bind(item: SpinnerItem) {
        this.bankUiModel = item
        binding.txtSpinnerTitle.text = bankUiModel.title
    }
}