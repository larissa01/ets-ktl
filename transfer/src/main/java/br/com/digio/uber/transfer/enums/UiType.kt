package br.com.digio.uber.transfer.enums

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */
@Suppress("MagicNumber")
enum class UiType(val value: Int) {

    // Statement Future Entries
    SPENDING_CONTROL_HEADER_WON(0),
    SPENDING_CONTROL_HEADER_PAID(1),
    SPENDING_CONTROL_HEADER_ITEM(2),
    SPENDING_CONTROL_EMPTY(3),
    SPENDING_CONTROL_FINAL(4),

    // Franchise
    FRANCHISE_HEADER(0),
    FRANCHISE_TITLE_VALUE(1),
    FRANCHISE_TITLE_DOUBLE_VALUE(2),
    FRANCHISE_FOOTER(3),
    FRANCHISE_BUTTON(4),

    // Statement Future Entries
    STATEMENT_FUTURE_HEADER(0),
    STATEMENT_FUTURE_ITEM(1),

    // Statement Last Entries
    STATEMENT_LAST_ITEM(1),
    STATEMENT_LAST_PLACEHOLDER(2),
    STATEMENT_LOADING(3),

    // Favored Item
    FAVORED_ITEM(0),
    FAVORED_EMPTY(1),
    FAVORED_ITEM_EDIT(2),

    // Spinner List
    SPINNER_ITEM(0),

    // Spinner List
    MONTH_ITEM(0),

    // Bank List
    BANK_MORE_ITEMS(1),

    // Close Account Requirements
    REQUIREMENT_ITEM(1),

    // Close AccountAdvantage
    ADVANTAGE_ITEM(0),

    // Generated BankSlip
    GENERATED_BANKSLIP_ITEM(0)
}