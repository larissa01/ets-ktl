package br.com.digio.uber.transfer.mapper

import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.model.transfer.ThirdPartyAccountResponse
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.enums.TransferAccountType
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.util.formatAsAccountNumber
import br.com.digio.uber.util.formatAsAgencyNumber
import br.com.digio.uber.util.formatAsDocument
import br.com.digio.uber.util.getNameAbbreviation
import br.com.digio.uber.util.mapper.AbstractMapper

class AccountMapper(private val resourceManager: ResourceManager) :
    AbstractMapper<ThirdPartyAccountResponse, AccountUIModel> {
    override fun map(param: ThirdPartyAccountResponse): AccountUIModel = with(param) {
        return AccountUIModel(
            userName = fullName,
            bankName = productType,
            accountNumber = accountNumber.formatAsAccountNumber(),
            nameInitials = fullName.getNameAbbreviation(),
            agencyCode = accountBranch.formatAsAgencyNumber(),
            cpf = cpf.formatAsDocument(),
            bankDetails = "${accountBranch.formatAsAgencyNumber()} - $productType | ${resourceManager.getString(
                R.string.individual_checking_account_juridic
            )}",
            accountType = TransferAccountType.INDIVIDUAL_CHECKING_ACCOUNT
        )
    }
}