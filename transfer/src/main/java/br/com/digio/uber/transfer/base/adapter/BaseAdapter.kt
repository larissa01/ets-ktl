package br.com.digio.uber.transfer.base.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T : BaseAdapterItem>(val list: MutableList<T> = mutableListOf()) :
    RecyclerView.Adapter<BaseViewHolder<T>>() {

    override fun getItemViewType(position: Int): Int {
        return list[position].uiType.value
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) {
        holder.bind(list[position])
    }

    protected fun <E : ViewDataBinding> inflate(parent: ViewGroup, @LayoutRes resId: Int): E {
        return DataBindingUtil.inflate(LayoutInflater.from(parent.context), resId, parent, false)
    }

    open fun add(list: List<T>?) {
        list?.let { this.list.addAll(it) }
    }

    fun clear() = list.clear()
}