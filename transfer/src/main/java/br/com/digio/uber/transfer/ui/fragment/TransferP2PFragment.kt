package br.com.digio.uber.transfer.ui.fragment

import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.transfer.BR
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.databinding.TransferP2pFragmentBinding
import br.com.digio.uber.transfer.navigation.TransferOpKey
import br.com.digio.uber.transfer.ui.adapter.P2PTransferPagerAdapter
import br.com.digio.uber.transfer.ui.viewmodel.TransferP2PViewModel
import br.com.digio.uber.util.safeLet
import kotlinx.android.synthetic.main.transfer_ted_fragment.*
import org.koin.android.ext.android.inject

class TransferP2PFragment : BNWFViewModelFragment<TransferP2pFragmentBinding, TransferP2PViewModel>() {

    override val viewModel: TransferP2PViewModel by inject()

    override fun tag(): String = TransferOpKey.P2P.name

    override fun getStatusBarAppearance(): StatusBarColor = TransferOpKey.P2P.theme

    override val bindingVariable: Int? = BR.P2PViewModel

    override val getLayoutId = R.layout.transfer_p2p_fragment

    companion object : FragmentNewInstance<TransferP2PFragment>(::TransferP2PFragment)

    override fun initialize() {
        super.initialize()
        safeLet(context, activity?.supportFragmentManager) { contextLet, supportFragmentManagerLet ->
            val adapter = P2PTransferPagerAdapter(
                contextLet,
                this.navigationListener as GenericNavigationWithFlow,
                supportFragmentManagerLet
            )
            vp_ted.adapter = adapter
            tl_ted.setupWithViewPager(vp_ted)
        }
    }
}