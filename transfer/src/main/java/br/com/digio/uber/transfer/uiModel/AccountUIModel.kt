package br.com.digio.uber.transfer.uiModel

import android.os.Parcelable
import br.com.digio.uber.transfer.enums.OwnershipType
import br.com.digio.uber.transfer.enums.TransferAccountType
import kotlinx.android.parcel.Parcelize

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */
@Parcelize
data class AccountUIModel(
    val userName: String? = null,
    val bankDetails: String? = null,
    val agencyCode: String? = null,
    val accountNumber: String? = null,
    val accountPhoto: ByteArray? = null,
    val hasImage: Boolean? = null,
    val nameInitials: String? = null,
    val cpf: String? = null,
    val bankName: String? = null,
    val ownership: OwnershipType? = null,
    val bank: BankUIModel? = null,
    var accountType: TransferAccountType? = null,
    val digit: String? = null
) : Parcelable