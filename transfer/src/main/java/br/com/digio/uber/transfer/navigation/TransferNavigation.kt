package br.com.digio.uber.transfer.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_CHOOSED
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_CHOOSE_ANOTHER
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_CHOOSE_UBER
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_OPTION
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName
import br.com.digio.uber.transfer.ui.fragment.TransferHomeFragment
import br.com.digio.uber.transfer.ui.fragment.TransferP2PFragment
import br.com.digio.uber.transfer.ui.fragment.TransferReviewFragment
import br.com.digio.uber.transfer.ui.fragment.TransferTedFragment
import br.com.digio.uber.transfer.ui.fragment.TransferTedValueFragment
import br.com.digio.uber.util.Const.Extras.FRAGMENT_KEY
import br.com.digio.uber.util.Const.Fragments.P2P_FRAGMENT
import br.com.digio.uber.util.Const.Fragments.TED_FRAGMENT
import br.com.digio.uber.util.Const.Fragments.TED_VALUE_FRAGMENT

class TransferNavigation(
    activity: AppCompatActivity,
    private val analytics: Analytics
) : GenericNavigationWithFlow(activity) {

    fun init(bundle: Bundle?) {
        if (bundle != null) {
            when (bundle.getString(FRAGMENT_KEY)) {
                TED_FRAGMENT -> { goTo(GoTo(TransferOpKey.TED.name, bundle)) }
                TED_VALUE_FRAGMENT -> { goTo(GoTo(TransferOpKey.TED_VALUE.name, bundle)) }
                P2P_FRAGMENT -> { goTo(GoTo(TransferOpKey.P2P.name, bundle)) }
            }
        } else {
            goTo(GoTo(TransferOpKey.TED.name, bundle))
        }
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            TransferOpKey.HOME.name to showHome(),
            TransferOpKey.TED.name to showTED(),
            TransferOpKey.P2P.name to showP2P(),
            TransferOpKey.TED_VALUE.name to showTEDValue(),
            TransferOpKey.TED_REVIEW.name to showTEDReview()
        )

    private fun showHome(): GenericNavigationWIthFlowGetFragmentByName = {
        TransferHomeFragment.newInstance(this, it)
    }

    private fun showTED(): GenericNavigationWIthFlowGetFragmentByName = {
        analytics.pushSimpleEvent(TRANSFER_OPTION, TRANSFER_CHOOSED to TRANSFER_CHOOSE_ANOTHER)
        TransferTedFragment.newInstance(this, it)
    }

    private fun showP2P(): GenericNavigationWIthFlowGetFragmentByName = {
        analytics.pushSimpleEvent(TRANSFER_OPTION, TRANSFER_CHOOSED to TRANSFER_CHOOSE_UBER)
        TransferP2PFragment.newInstance(this, it)
    }

    private fun showTEDValue(): GenericNavigationWIthFlowGetFragmentByName = {
        TransferTedValueFragment.newInstance(this, it)
    }

    private fun showTEDReview(): GenericNavigationWIthFlowGetFragmentByName = {
        TransferReviewFragment.newInstance(this, it)
    }
}