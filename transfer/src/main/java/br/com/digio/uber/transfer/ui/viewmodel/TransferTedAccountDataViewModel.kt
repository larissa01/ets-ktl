@file:Suppress("TooManyFunctions", "LongParameterList")

package br.com.digio.uber.transfer.ui.viewmodel

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import br.com.digio.pix.injection.Injector
import br.com.digio.pix.repository.PixKeyRepository
import br.com.digio.pix.repository.model.PixAccountRequest
import br.com.digio.pix.repository.model.PixValidateAccountRequest
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_ACCOUNT_DATA
import br.com.digio.uber.common.R
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.transfer.GetAccountTedUseCase
import br.com.digio.uber.domain.usecase.transfer.GetAllBanksUseCase
import br.com.digio.uber.domain.usecase.transfer.GetAllPixBanksUseCase
import br.com.digio.uber.domain.usecase.transfer.GetFamousBankUseCase
import br.com.digio.uber.domain.usecase.transfer.GetFamousPixBankUseCase
import br.com.digio.uber.model.transfer.AccountDomainResponse
import br.com.digio.uber.model.transfer.TransferMode
import br.com.digio.uber.transfer.base.spinner.model.SpinnerItem
import br.com.digio.uber.transfer.base.spinner.view.BankListDialog
import br.com.digio.uber.transfer.enums.OwnershipType
import br.com.digio.uber.transfer.enums.TransferAccountType
import br.com.digio.uber.transfer.mapper.BankMapper
import br.com.digio.uber.transfer.mapper.PixBankMapper
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.transfer.uiModel.BankUIModel
import br.com.digio.uber.transfer.uiModel.TedAccountDataUIModel
import br.com.digio.uber.transfer.uiModel.TransferUIModel
import br.com.digio.uber.util.Const.MaskPattern.CNPJ_LENGTH
import br.com.digio.uber.util.formatAsAgencyNumber
import br.com.digio.uber.util.formatAsDocument
import br.com.digio.uber.util.getNameAbbreviation
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.validateCNPJ
import br.com.digio.uber.util.validateCpf
import kotlinx.coroutines.launch

class TransferTedAccountDataViewModel(
    private val getFamousBankUseCase: GetFamousBankUseCase,
    private val getFamousPixBankUseCase: GetFamousPixBankUseCase,
    private val getAllBanksUseCase: GetAllBanksUseCase,
    private val getAllPixBanksUseCase: GetAllPixBanksUseCase,
    getAccountUseCase: GetAccountTedUseCase,
    private val resourceManager: ResourceManager,
    private val analytics: Analytics
) : BaseViewModel(), BankListDialog.OnBankSelectListener {

    var transferMode: TransferMode? = null
    val tedAccountUIModel = TedAccountDataUIModel()

    private val accountLiveData = MutableLiveData<AccountUIModel>()
    var mutableAccount: LiveData<AccountDomainResponse> = getAccountUseCase(Unit).exec(showLoadingFlag = true)

    private val ownership = MutableLiveData<OwnershipType>()
    private val bank = MutableLiveData<BankUIModel>()
    private val accountType = MutableLiveData<TransferAccountType>()
    val account: LiveData<AccountUIModel> = accountLiveData

    private val pixAccountLiveData = MutableLiveData<TransferUIModel>()
    val pixAccount: LiveData<TransferUIModel> = pixAccountLiveData

    private var hasAllBanks: Boolean = false

    var bankList: LiveData<ArrayList<BankUIModel>> = MutableLiveData()
    var allBanksList: LiveData<ArrayList<BankUIModel>> = MutableLiveData()

    var mutableBankList: MutableLiveData<ArrayList<BankUIModel>> = MutableLiveData()

    val showSaveFavoredOption: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = true
    }

    private val pixKeyRepository: PixKeyRepository by lazy {
        Injector.provideKeyRepository(
            null,
            mutableAccount.value?.cpf
        )
    }

    fun initializer(transferMode: TransferMode, onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        this.transferMode = transferMode
        when (transferMode) {
            TransferMode.TED -> {
                bankList = getFamousBankUseCase(Unit).exec(showLoadingFlag = true).map(BankMapper(resourceManager))
                allBanksList = getAllBanksUseCase(Unit).exec(showLoadingFlag = true).map(BankMapper(resourceManager))
                showSaveFavoredOption.value = true
            }
            TransferMode.PIX -> {
                bankList =
                    getFamousPixBankUseCase(Unit).exec(showLoadingFlag = true).map(PixBankMapper(resourceManager))
                allBanksList =
                    getAllPixBanksUseCase(Unit).exec(showLoadingFlag = true).map(PixBankMapper(resourceManager))
                showSaveFavoredOption.value = false
            }
        }
    }

    fun checkAccountFields() {
        checkContinue()
    }

    fun checkInstitutionField() {
        checkContinue()
        tedAccountUIModel.institutionField.value?.let {
            val split = it.split("-")
            tedAccountUIModel.labelWarnDigitVisible.value =
                split[0].trim() == CODE_BANCO_DO_BRASIL ||
                split[0].trim() == CODE_BRADESCO ||
                split[0].trim() == CODE_BANRISUL
        }
    }

    fun checkDocumentField() {
        if (documentIsCNPJ()) {
            tedAccountUIModel.accountTypeField.value = ""
            accountType.value = null
        }
        checkContinue()
    }

    fun onContinue() {
        analytics.pushSimpleEvent(TRANSFER_ACCOUNT_DATA)
        when (transferMode) {
            TransferMode.TED -> onContinueTED()
            TransferMode.PIX -> onContinuePix()
        }
    }

    private fun onContinueTED() {
        accountLiveData.value = AccountUIModel(
            userName =
                getName(),
            nameInitials =
                if (ownership.value == OwnershipType.OTHER_OWNERSHIP) {
                    tedAccountUIModel.nameField.value?.getNameAbbreviation()
                } else {
                    mutableAccount.value?.fullName?.getNameAbbreviation()
                },
            bankDetails = bank.value?.title,
            agencyCode = tedAccountUIModel.agencyField.value?.formatAsAgencyNumber(),
            accountNumber = getAccountNumber(),
            hasImage = false,
            cpf =
                getDocument(),
            bankName = bank.value?.name,
            ownership = ownership.value,
            bank = bank.value,
            accountType = accountType.value
        )
    }

    private fun onContinuePix() {
        showLoading()
        viewModelScope.launch {
            runCatching { pixKeyRepository.validateAccount(createPixValidateAccountRequest()) }
                .onFailure {
                    hideLoading()
                    message.value = ErrorObject().apply {
                        title = R.string.ops
                        messageString = it.message
                    }
                }
                .onSuccess {
                    hideLoading()
                    val account = AccountUIModel(
                        userName = getName(),
                        nameInitials = it.beneficiary?.name?.getNameAbbreviation(),
                        bankDetails = it.beneficiary?.institutionName,
                        agencyCode = it.beneficiary?.accountBranch,
                        accountNumber = it.beneficiary?.accountNumber?.formatAsAgencyNumber(),
                        hasImage = false,
                        cpf = it.beneficiary?.document,
                        bankName = bank.value?.name,
                        ownership = ownership.value,
                        bank = bank.value,
                        accountType = accountType.value
                    )
                    pixAccountLiveData.value = TransferUIModel(
                        account = account,
                        pixTransferDetail = it
                    )
                }
        }
    }

    private fun createPixValidateAccountRequest(): PixValidateAccountRequest {
        val document = getDocument()?.replace("[.|-]".toRegex(), "")
        return PixValidateAccountRequest(
            document,
            bank.value?.code,
            getName(),
            PixAccountRequest(
                tedAccountUIModel.agencyField.value?.formatAsAgencyNumber(),
                getAccountNumber(),
                resourceManager.getString(accountType.value?.pixType ?: R.string.generic_error_message_message)
            )
        )
    }

    private fun documentIsCNPJ() = tedAccountUIModel.documentField.value?.unmask()?.length == CNPJ_LENGTH

    private fun getName(): String? {
        return if (ownership.value == OwnershipType.OTHER_OWNERSHIP) {
            tedAccountUIModel.nameField.value
        } else {
            mutableAccount.value?.fullName?.trim()
        }
    }

    private fun getDocument(): String? {
        return if (ownership.value == OwnershipType.OTHER_OWNERSHIP) {
            tedAccountUIModel.documentField.value?.formatAsDocument()
        } else {
            mutableAccount.value?.cpf?.formatAsDocument()
        }
    }

    private fun getAccountNumber(): String {
        return String.format(
            resourceManager.getString(
                R.string.account_number,
                tedAccountUIModel.accountField.value ?: "",
                tedAccountUIModel.digitField.value ?: ""
            )
        )
    }

    private fun checkContinue() {
        tedAccountUIModel.continueActivation.value =
            validateAccountFields() &&
            !tedAccountUIModel.institutionField.value.isNullOrBlank() &&
            validateOwnership() &&
            accountType.value != null
    }

    private fun validateOwnership(): Boolean {
        return if (ownership.value == OwnershipType.OTHER_OWNERSHIP) {
            !tedAccountUIModel.documentField.value.isNullOrBlank() &&
                !tedAccountUIModel.nameField.value.isNullOrBlank() &&
                tedAccountUIModel.documentField.value?.unmask().validateCpf() ||
                (tedAccountUIModel.documentField.value?.unmask()?.validateCNPJ() ?: false)
        } else ownership.value == OwnershipType.SAME_OWNERSHIP
    }

    private fun validateAccountFields(): Boolean {
        return !tedAccountUIModel.agencyField.value.isNullOrBlank() &&
            !tedAccountUIModel.accountField.value.isNullOrBlank() &&
            !tedAccountUIModel.digitField.value.isNullOrBlank()
    }

    fun getBanks() {
        mutableBankList.postValue(bankList.value)
    }

    fun getAccountTypes(): List<SpinnerItem> {
        val accountTypes: Array<TransferAccountType> = TransferAccountType.values()

        return if (isCNPJAccountType()) {
            accountTypes.filter {
                it.juridicResId != null
            }.map {
                SpinnerItem(resourceManager.getString(it.juridicResId!!), it.ordinal)
            }
        } else {
            accountTypes.map {
                SpinnerItem(resourceManager.getString(it.personalResId), it.ordinal)
            }
        }
    }

    fun getOwnershipOptions(): List<SpinnerItem> {
        return OwnershipType.values().map {
            SpinnerItem(resourceManager.getString(it.resId), it.ordinal)
        }
    }

    fun shouldSaveFavored(): Boolean? {
        return tedAccountUIModel.saveFavored.value
    }

    fun setOwnership(code: Int) {
        ownership.value = OwnershipType.values().firstOrNull { it.ordinal == code }
        tedAccountUIModel.ownershipTypeField.value = ownership.value?.resId?.let { resourceManager.getString(it) }
        tedAccountUIModel.ownershipFieldsVisibility.value =
            if (ownership.value!! == OwnershipType.OTHER_OWNERSHIP) View.VISIBLE else View.GONE
    }

    fun onAccountTypeSelected(code: Int) {
        accountType.value = TransferAccountType.values().firstOrNull { it.ordinal == code }
        updateAccountTypeField()
    }

    private fun updateAccountTypeField() {
        tedAccountUIModel.accountTypeField.value =
            TransferAccountType.getStringId(
                accountType.value!!,
                tedAccountUIModel.documentField.value?.unmask()
                    ?: ""
            )?.let {
                resourceManager.getString(it)
            }
    }

    private fun isCNPJAccountType() =
        ownership.value != OwnershipType.SAME_OWNERSHIP &&
            (tedAccountUIModel.documentField.value?.unmask()?.validateCNPJ() ?: false)

    companion object {
        const val CODE_BANCO_DO_BRASIL = "001"
        const val CODE_BRADESCO = "237"
        const val CODE_BANRISUL = "041"
    }

    override fun onBankSelected(code: String) {
        bank.value = mutableBankList.value?.first {
            it.code == code
        }

        tedAccountUIModel.institutionField.value = bank.value?.title
    }

    override fun onMoreBanksClick() {
        hasAllBanks = true
        mutableBankList.value = allBanksList.value
    }

    override fun hasAllBanks(): Boolean = hasAllBanks
}