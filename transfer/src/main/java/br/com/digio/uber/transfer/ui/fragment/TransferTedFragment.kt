package br.com.digio.uber.transfer.ui.fragment

import br.com.digio.pix.util.Const
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.model.transfer.TransferMode
import br.com.digio.uber.transfer.BR
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.databinding.TransferTedFragmentBinding
import br.com.digio.uber.transfer.navigation.TransferOpKey
import br.com.digio.uber.transfer.ui.adapter.TEDTransferPagerAdapter
import br.com.digio.uber.transfer.ui.viewmodel.TransferTedViewModel
import br.com.digio.uber.util.safeLet
import kotlinx.android.synthetic.main.transfer_ted_fragment.*
import org.koin.android.ext.android.inject

class TransferTedFragment : BNWFViewModelFragment<TransferTedFragmentBinding, TransferTedViewModel>() {

    override val viewModel: TransferTedViewModel by inject()

    override fun tag(): String = TransferOpKey.TED.name

    override fun getStatusBarAppearance(): StatusBarColor = TransferOpKey.TED.theme

    override val bindingVariable: Int = BR.TedViewModel

    override val getLayoutId = R.layout.transfer_ted_fragment

    companion object : FragmentNewInstance<TransferTedFragment>(::TransferTedFragment)

    override fun initialize() {
        super.initialize()
        safeLet(context, activity?.supportFragmentManager) { contextLet, supportFragmentManagerLet ->
            val adapter = TEDTransferPagerAdapter(
                contextLet,
                this.navigationListener as GenericNavigationWithFlow,
                getTransferMode(),
                supportFragmentManagerLet
            )
            vp_ted.adapter = adapter
            tl_ted.setupWithViewPager(vp_ted)
        }
    }

    private fun getTransferMode(): TransferMode {
        val isPixTransfer = requireActivity().intent
            .extras?.getBoolean(Const.ManualPayment.ARG_TRANSFER_PIX, false) ?: false
        return if (isPixTransfer) TransferMode.PIX else TransferMode.TED
    }
}