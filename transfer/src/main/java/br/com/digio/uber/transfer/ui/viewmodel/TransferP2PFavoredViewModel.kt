package br.com.digio.uber.transfer.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_BUTTON_DELETE_FAVORED
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.usecase.transfer.DeleteP2PFavoredUseCase
import br.com.digio.uber.domain.usecase.transfer.GetAllBanksUseCase
import br.com.digio.uber.domain.usecase.transfer.GetP2PFavoredUseCase
import br.com.digio.uber.model.transfer.BankDomain
import br.com.digio.uber.model.transfer.FavoredListDomainResponse
import br.com.digio.uber.transfer.base.spinner.model.SpinnerItem
import br.com.digio.uber.transfer.enums.TransferAccountType
import br.com.digio.uber.transfer.mapper.toDomain
import br.com.digio.uber.transfer.mapper.toUiModel
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.transfer.uiModel.FavoredEditionUIModel
import br.com.digio.uber.transfer.uiModel.FavoredUIModel
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.validateCNPJ

class TransferP2PFavoredViewModel(
    private val getFavoredUseCase: GetP2PFavoredUseCase,
    private val deleteFavoredUseCase: DeleteP2PFavoredUseCase,
    getAllBanksUseCase: GetAllBanksUseCase,
    private val resourceManager: ResourceManager,
    private val analytics: Analytics
) : BaseViewModel() {

    private var favoredDomain: FavoredListDomainResponse? = null
    private val mutableFavored = MutableLiveData<List<FavoredUIModel>>()
    private val mutableSelectedAccount = MutableLiveData<AccountUIModel>()
    private lateinit var banks: ArrayList<BankDomain>

    val mutableFavoredEdition = MutableLiveData<FavoredEditionUIModel?>()
    val favoredEditionUIModel = MutableLiveData<FavoredEditionUIModel?>()

    val favored: LiveData<List<FavoredUIModel>>
        get() = mutableFavored

    val selectedAccount: LiveData<AccountUIModel>
        get() = mutableSelectedAccount

    val hasFavored = MutableLiveData(true)

    init {
        showLoading()
        getAllBanksUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                banks = it.bankDomains
                getFavoredUseCase(Unit).singleExec(
                    onSuccessBaseViewModel = { response ->
                        favoredDomain = response.beneficiaries.toDomain(banks)
                        mutableFavored.value = favoredDomain?.toUiModel(
                            onFavoredSelected = ::onFavoredSelected,
                            onDeleteClick = ::onDeleteClick,
                            provider = resourceManager,
                            isEditable = true
                        )
                        hasFavored.value = !mutableFavored.value.isNullOrEmpty()
                        closeEdition()
                    }
                )
            }
        )
    }

    private fun onDeleteClick(position: Int) {
        analytics.pushSimpleEvent(TRANSFER_BUTTON_DELETE_FAVORED)
        val favoredId = favoredDomain?.favored?.get(position)?.uuid
        deleteFavoredUseCase(favoredId ?: "").singleExec(
            onSuccessBaseViewModel = {
                onDeleteClickSuccess(position)
            }
        )
    }

    private fun closeEdition() {
        mutableFavoredEdition.value = null
        favoredEditionUIModel.value = null
    }

    private fun onDeleteClickSuccess(position: Int? = null, id: String? = null) {
        val newList = favoredDomain?.favored?.toMutableList()?.apply {
            if (position != null) {
                removeAt(position)
            } else removeAll {
                it.uuid == id
            }
        }

        favoredDomain = favoredDomain?.copy(favored = newList ?: listOf())
        mutableFavored.value = favoredDomain?.toUiModel(
            onFavoredSelected = ::onFavoredSelected,
            onDeleteClick = ::onDeleteClick,
            provider = resourceManager,
            isEditable = true
        )
    }

    private fun onFavoredSelected(position: Int) {
        val favoredDomainSelected = favoredDomain?.favored?.get(position)
        mutableSelectedAccount.value = favoredDomainSelected?.toUiModel(resourceManager)
    }

    fun onAccountTypeSelected(code: Int) {
        updateAccountType(code)

        favoredEditionUIModel.value = favoredEditionUIModel.value?.copy()
    }

    private fun updateAccountType(code: Int) {
        val accountType = TransferAccountType.values().firstOrNull { it.ordinal == code }

        favoredEditionUIModel.value?.accountType = accountType

        favoredEditionUIModel.value?.accountType?.let { type ->
            favoredEditionUIModel.value?.accountTypeField = TransferAccountType.getStringId(
                type,
                favoredEditionUIModel.value?.document?.unmask() ?: ""
            )?.let { resourceManager.getString(it) } ?: ""
        }
    }

    private fun isCNPJAccountType(): Boolean {
        return favoredEditionUIModel.value?.document?.unmask()?.validateCNPJ() ?: false
    }

    fun getAccountTypes(): List<SpinnerItem> {
        val accountTypes: Array<TransferAccountType> = TransferAccountType.values()

        return if (isCNPJAccountType()) {
            accountTypes.filter {
                it.juridicResId != null
            }.map {
                SpinnerItem(resourceManager.getString(it.juridicResId!!), it.ordinal)
            }
        } else {
            accountTypes.map {
                SpinnerItem(resourceManager.getString(it.personalResId), it.ordinal)
            }
        }
    }
}