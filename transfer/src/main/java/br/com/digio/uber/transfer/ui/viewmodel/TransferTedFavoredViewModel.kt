package br.com.digio.uber.transfer.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_BUTTON_DELETE_FAVORED
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_BUTTON_EDIT_FAVORED
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.usecase.transfer.DeleteFavoredUseCase
import br.com.digio.uber.domain.usecase.transfer.EditFavoredDomainRequest
import br.com.digio.uber.domain.usecase.transfer.EditFavoredUseCase
import br.com.digio.uber.domain.usecase.transfer.GetAllBanksUseCase
import br.com.digio.uber.domain.usecase.transfer.GetFavoredUseCase
import br.com.digio.uber.model.transfer.BankDomain
import br.com.digio.uber.model.transfer.FavoredDomainResponse
import br.com.digio.uber.model.transfer.FavoredListDomainResponse
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.base.spinner.model.SpinnerItem
import br.com.digio.uber.transfer.enums.TransferAccountType
import br.com.digio.uber.transfer.enums.TransferRegisterType
import br.com.digio.uber.transfer.mapper.toDomain
import br.com.digio.uber.transfer.mapper.toUiModel
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.transfer.uiModel.FavoredEditionUIModel
import br.com.digio.uber.transfer.uiModel.FavoredUIModel
import br.com.digio.uber.util.getNameAbbreviation
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.validateCNPJ

class TransferTedFavoredViewModel(
    private val getFavoredUseCase: GetFavoredUseCase,
    private val editFavoredUseCase: EditFavoredUseCase,
    private val deleteFavoredUseCase: DeleteFavoredUseCase,
    getAllBanksUseCase: GetAllBanksUseCase,
    private val resourceManager: ResourceManager,
    private val analytics: Analytics
) : BaseViewModel() {

    private var favoredDomain: FavoredListDomainResponse? = null
    private val mutableFavored = MutableLiveData<List<FavoredUIModel>>()
    private val mutableSelectedAccount = MutableLiveData<AccountUIModel>()
    private lateinit var banks: ArrayList<BankDomain>

    val mutableFavoredEdition = MutableLiveData<FavoredEditionUIModel?>()
    val favoredEditionUIModel = MutableLiveData<FavoredEditionUIModel?>()

    val favored: LiveData<List<FavoredUIModel>>
        get() = mutableFavored

    val selectedAccount: LiveData<AccountUIModel>
        get() = mutableSelectedAccount

    val hasFavored = MutableLiveData(true)

    init {
        showLoading()
        getAllBanksUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                banks = it.bankDomains
                getFavoredUseCase(Unit).singleExec(
                    onSuccessBaseViewModel = { response ->
                        favoredDomain = response.beneficiaries.toDomain(banks)
                        mutableFavored.value = favoredDomain?.toUiModel(
                            onFavoredSelected = ::onFavoredSelected,
                            onDeleteClick = ::onDeleteClick,
                            provider = resourceManager,
                            isEditable = true
                        )
                        hasFavored.value = !mutableFavored.value.isNullOrEmpty()
                        closeEdition()
                    }
                )
            }
        )
    }

    private fun onDeleteClick(position: Int) {
        analytics.pushSimpleEvent(TRANSFER_BUTTON_DELETE_FAVORED)
        val favoredId = favoredDomain?.favored?.get(position)?.uuid
        deleteFavoredUseCase(favoredId ?: "").singleExec(
            onSuccessBaseViewModel = {
                onDeleteClickSuccess(position)
            }
        )
    }

    fun setEditFavored(position: Int) {
        analytics.pushSimpleEvent(TRANSFER_BUTTON_EDIT_FAVORED)
        val favoredDomainSelected = favoredDomain?.favored?.get(position)

        favoredDomainSelected?.let {
            favoredEditionUIModel.value = FavoredEditionUIModel(
                beneficiaryId = it.uuid,
                name = it.name,
                nameInitials = it.name.getNameAbbreviation(),
                bankName = it.bankName,
                document = it.document,
                documentType =
                    if (it.document.unmask().validateCNPJ()) {
                        resourceManager.getString(R.string.cnpj)
                    } else {
                        resourceManager.getString(R.string.cpf)
                    },
                agencyField = it.branch,
                accountField =
                    it.accountNumber.removeRange(it.accountNumber.lastIndex, it.accountNumber.length),
                digitField = it.accountNumber.last().toString(),
                accountTypeField = getAccountTypeField(it),
                accountType = TransferAccountType.fromType(it.transferAccountType),
                position = position,
                onCancelClick = ::closeEdition,
                onSaveClick = ::saveEdition
            )
        }
    }

    private fun getAccountTypeField(it: FavoredDomainResponse): String {
        val transferType = TransferAccountType.fromType(it.transferAccountType)
        return TransferAccountType.getStringId(transferType, it.document.unmask())?.let {
            resourceManager.getString(it)
        } ?: ""
    }

    private fun closeEdition() {
        mutableFavoredEdition.value = null
        favoredEditionUIModel.value = null
    }

    private fun saveEdition() {
        favoredEditionUIModel.value?.let { favoredEdition ->
            val request = EditFavoredDomainRequest(
                registerType = TransferRegisterType.TED.name,
                accountNumber = favoredEdition.accountField.toInt().toString() + favoredEdition.digitField,
                agencyCode = favoredEdition.agencyField,
                beneficiaryId = favoredEdition.beneficiaryId,
                transferAccountType = favoredEdition.accountType?.name ?: ""
            )

            editFavoredUseCase(request).singleExec(
                onSuccessBaseViewModel = { beneficiary ->
                    val bank = banks.firstOrNull { bankDomain ->
                        bankDomain.code.toInt() == beneficiary.bankCode.toInt()
                    }
                    val result = beneficiary.toDomain(bank)
                    val newList = favoredDomain?.favored?.map { domain ->
                        if (domain.uuid == result.uuid) {
                            result
                        } else domain
                    }
                    favoredDomain = favoredDomain?.copy(favored = newList ?: listOf())
                    mutableFavored.value = favoredDomain?.toUiModel(
                        onFavoredSelected = ::onFavoredSelected,
                        onDeleteClick = ::onDeleteClick,
                        provider = resourceManager,
                        isEditable = true
                    )
                    mutableFavoredEdition.value = favoredEditionUIModel.value
                    closeEdition()
                }
            )
        }
    }

    private fun onDeleteClickSuccess(position: Int? = null, id: String? = null) {
        val newList = favoredDomain?.favored?.toMutableList()?.apply {
            if (position != null) {
                removeAt(position)
            } else removeAll {
                it.uuid == id
            }
        }

        favoredDomain = favoredDomain?.copy(favored = newList ?: listOf())
        mutableFavored.value = favoredDomain?.toUiModel(
            onFavoredSelected = ::onFavoredSelected,
            onDeleteClick = ::onDeleteClick,
            provider = resourceManager,
            isEditable = true
        )
        hasFavored.value = !mutableFavored.value.isNullOrEmpty()
    }

    private fun onFavoredSelected(position: Int) {
        val favoredDomainSelected = favoredDomain?.favored?.get(position)
        mutableSelectedAccount.value = favoredDomainSelected?.toUiModel(resourceManager)
    }

    fun onAccountTypeSelected(code: Int) {
        updateAccountType(code)

        favoredEditionUIModel.value = favoredEditionUIModel.value?.copy()
    }

    private fun updateAccountType(code: Int) {
        val accountType = TransferAccountType.values().firstOrNull { it.ordinal == code }

        favoredEditionUIModel.value?.accountType = accountType

        favoredEditionUIModel.value?.accountType?.let { type ->
            favoredEditionUIModel.value?.accountTypeField = TransferAccountType.getStringId(
                type,
                favoredEditionUIModel.value?.document?.unmask() ?: ""
            )?.let { resourceManager.getString(it) } ?: ""
        }
    }

    private fun isCNPJAccountType(): Boolean {
        return favoredEditionUIModel.value?.document?.unmask()?.validateCNPJ() ?: false
    }

    fun getAccountTypes(): List<SpinnerItem> {
        val accountTypes: Array<TransferAccountType> = TransferAccountType.values()

        return if (isCNPJAccountType()) {
            accountTypes.filter {
                it.juridicResId != null
            }.map {
                SpinnerItem(resourceManager.getString(it.juridicResId!!), it.ordinal)
            }
        } else {
            accountTypes.map {
                SpinnerItem(resourceManager.getString(it.personalResId), it.ordinal)
            }
        }
    }
}