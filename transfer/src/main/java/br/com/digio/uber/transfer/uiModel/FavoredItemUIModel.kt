package br.com.digio.uber.transfer.uiModel

import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory

sealed class FavoredItemUIModel : AdapterViewModel<FavoredItemUIModel> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<FavoredItemUIModel>) = typesFactory.type(
        model = this
    )
}

private const val ZERO_VALUE = 0

data class FavoredItem(
    val favoredName: String = "",
    val favoredId: String = "",
    val bankDetails: String = "",
    val isEditable: Boolean = false,
    val onSelected: (position: Int) -> Unit = {},
    val onDeleteClick: (position: Int) -> Unit = {},
    val onEditClick: (position: Int) -> Unit = {},
    val position: Int = ZERO_VALUE
) : FavoredItemUIModel()