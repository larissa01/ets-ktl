package br.com.digio.uber.transfer.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.digio.uber.common.base.dialog.BaseDialog
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.databinding.DialogDeleteFavoredConfirmationBinding
import br.com.digio.uber.transfer.uiModel.FavoredItem

class DeleteFavoredDialogFragment : BaseDialog() {

    private lateinit var item: FavoredItem
    private lateinit var binding: DialogDeleteFavoredConfirmationBinding

    companion object {
        const val DELETE_FAVORED_DIALOG_TAG = "DELETE_FAVORED_DIALOG_TAG"

        fun newInstance(item: FavoredItem) = DeleteFavoredDialogFragment().apply {
            this.item = item
        }
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun tag(): String = DELETE_FAVORED_DIALOG_TAG

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() {
        setupListeners()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DialogDeleteFavoredConfirmationBinding.inflate(inflater)
        return binding.root
    }

    private fun setupListeners() {
        binding.btnConfirm.setOnClickListener {
            item.onDeleteClick.invoke(item.position)
            dismiss()
        }
        binding.btnCancel.setOnClickListener { dismiss() }
    }
}