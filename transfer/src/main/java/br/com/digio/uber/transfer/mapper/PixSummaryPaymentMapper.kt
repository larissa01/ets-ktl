package br.com.digio.uber.transfer.mapper

import br.com.digio.pix.repository.model.PixValidatePaymentResponse
import br.com.digio.pix.ui.payment.summary.model.PixSummaryPaymentUIModel
import br.com.digio.uber.transfer.uiModel.TransferUIModel
import br.com.digio.uber.util.mapper.AbstractMapper

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */
class PixSummaryPaymentMapper(private val transfer: TransferUIModel) :
    AbstractMapper<PixValidatePaymentResponse?, PixSummaryPaymentUIModel?> {

    override fun map(param: PixValidatePaymentResponse?): PixSummaryPaymentUIModel? {
        return param?.let {
            PixSummaryPaymentUIModel(
                it.transactionId,
                transfer.value,
                transfer.date,
                transfer.description,
                it.beneficiary,
                it.payer,
                it.payee,
                it.amountComposition
            )
        }
    }
}