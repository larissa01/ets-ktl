package br.com.digio.uber.transfer.enums

enum class PersonType {
    PF,
    PJ;

    companion object {
        fun fromType(type: String): PersonType {
            return values().firstOrNull {
                it.name == type
            } ?: throw IllegalArgumentException("TransferAccountType $type not mapped.")
        }
    }
}