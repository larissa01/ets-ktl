package br.com.digio.uber.transfer.base.spinner.view

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.base.spinner.model.SpinnerItem
import br.com.digio.uber.transfer.ui.adapter.SpinnerListAdapter
import kotlinx.android.synthetic.main.dialog_spinner_list.recyclerview_banks

class SpinnerListDialog(
    context: Context,
    private val list: List<SpinnerItem>,
    private val listener: OnItemSelectListener
) : Dialog(context),
    SpinnerListAdapter.OnItemSelectedListener {

    override fun onItemSelected(code: Int) {
        listener.onItemSelected(code)
        dismiss()
    }

    private lateinit var banksAdapter: SpinnerListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_spinner_list)

        window?.let {
            it.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            it.setGravity(Gravity.CENTER)
        }

        createRecyclerView()
    }

    private fun createRecyclerView() {
        banksAdapter =
            SpinnerListAdapter(ArrayList(list), this)
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        recyclerview_banks.adapter = banksAdapter
        recyclerview_banks.layoutManager = layoutManager
    }

    interface OnItemSelectListener {
        fun onItemSelected(code: Int)
    }
}