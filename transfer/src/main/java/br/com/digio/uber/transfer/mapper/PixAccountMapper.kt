package br.com.digio.uber.transfer.mapper

import br.com.digio.pix.repository.model.PixValidatePaymentResponse
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.util.getNameAbbreviation
import br.com.digio.uber.util.mapper.AbstractMapper

class PixAccountMapper : AbstractMapper<PixValidatePaymentResponse, AccountUIModel> {

    override fun map(param: PixValidatePaymentResponse): AccountUIModel =
        AccountUIModel(
            userName = param.beneficiary?.name,
            nameInitials = param.beneficiary?.name?.getNameAbbreviation(),
            bankDetails = param.beneficiary?.institutionName,
            agencyCode = param.beneficiary?.accountBranch,
            accountNumber = param.beneficiary?.accountNumber,
            hasImage = false,
            cpf = param.beneficiary?.document,
            bankName = param.beneficiary?.institutionName
        )
}