package br.com.digio.uber.transfer.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.digio.uber.common.base.dialog.BaseDialog
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.databinding.DialogP2pAccountChooseBinding
import br.com.digio.uber.transfer.ui.adapter.AccountChooseAdapter
import br.com.digio.uber.transfer.ui.viewmodel.TransferP2PAccountDataViewModel
import br.com.digio.uber.transfer.uiModel.AccountUIModel

class ChooseAccountDialogFragment : BaseDialog(), AccountChooseAdapter.OnItemSelectedListener {
    private lateinit var p2pViewModel: TransferP2PAccountDataViewModel
    private var binding: DialogP2pAccountChooseBinding? = null
    private lateinit var listener: AccountChooseAdapter.OnItemSelectedListener
    private lateinit var adapter: AccountChooseAdapter

    companion object {
        const val CHOOSE_ACCOUNT_DIALOG_TAG = "CHOOSE_ACCOUNT_DIALOG_TAG"

        fun newInstance(
            viewModel: TransferP2PAccountDataViewModel,
            listener: AccountChooseAdapter.OnItemSelectedListener
        ): ChooseAccountDialogFragment =
            ChooseAccountDialogFragment().apply {
                this.p2pViewModel = viewModel
                this.listener = listener
            }
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun tag(): String = CHOOSE_ACCOUNT_DIALOG_TAG

    override fun getStatusBarAppearance() = StatusBarColor.BLACK

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DialogP2pAccountChooseBinding.inflate(inflater)

        return binding?.root
    }

    override fun initialize() {
        bindItems()
    }

    private fun bindItems() {
        adapter = AccountChooseAdapter(this)
        binding?.recyclerAccounts?.adapter = adapter
        p2pViewModel.accounts.value?.let {
            adapter.replaceItems(it as ArrayList<AccountUIModel>)
        }
    }

    override fun onItemSelected(account: AccountUIModel) {
        listener.onItemSelected(account)
        dismiss()
    }
}