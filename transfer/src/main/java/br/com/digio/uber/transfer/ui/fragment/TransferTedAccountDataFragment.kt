package br.com.digio.uber.transfer.ui.fragment

import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.observe
import br.com.digio.pix.util.Const
import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.transfer.TransferMode
import br.com.digio.uber.transfer.BR
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.base.spinner.view.BankListDialog
import br.com.digio.uber.transfer.base.spinner.view.SpinnerListDialog
import br.com.digio.uber.transfer.databinding.FragmentTransferTedAccountDataBinding
import br.com.digio.uber.transfer.navigation.TransferOpKey
import br.com.digio.uber.transfer.ui.viewmodel.TransferTedAccountDataViewModel
import br.com.digio.uber.util.Const.MaskPattern.CNPJ_LENGTH
import br.com.digio.uber.util.Const.MaskPattern.CPF_LENGTH
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.validateCNPJ
import br.com.digio.uber.util.validateCpf
import org.koin.android.viewmodel.ext.android.viewModel

class TransferTedAccountDataFragment : BNWFViewModelFragment<FragmentTransferTedAccountDataBinding,
    TransferTedAccountDataViewModel>() {

    override fun tag(): String = TransferOpKey.TED_ACCOUNT_DATA.name

    override fun getStatusBarAppearance(): StatusBarColor = TransferOpKey.TED_ACCOUNT_DATA.theme

    override val bindingVariable: Int = BR.accountDataViewModel

    override val getLayoutId = R.layout.fragment_transfer_ted_account_data

    var bankListdialog: BankListDialog? = null

    companion object : FragmentNewInstance<TransferTedAccountDataFragment>(::TransferTedAccountDataFragment) {
        const val AGENCY_LIMIT_LENGTH = 4
        const val INSTITUTION_LIMIT_LENGTH = 4
        const val ACCOUNT_LIMIT_LENGTH = 12
        const val ZERO_VALUE = 0
        const val ACCOUNT = "ACCOUNT"
        const val ISP2P = "ISP2P"
        const val SAVE_FAVORED = "SAVE_FAVORED"
    }

    override val viewModel: TransferTedAccountDataViewModel by viewModel()

    override fun initialize() {
        super.initialize()

        setupViewModel()
        setupObservers()
        setupListeners()
        setupTextWatchers()
        if (getTransferMode() == TransferMode.PIX) {
            (activity as BaseActivity).getToolbar()?.setTitle(R.string.pix_title)
        }
    }

    private fun setupViewModel() {
        viewModel.initializer(getTransferMode()) {}
    }

    private fun getTransferMode(): TransferMode {
        val isPixTransfer = requireActivity().intent
            .extras?.getBoolean(Const.ManualPayment.ARG_TRANSFER_PIX, false) ?: false
        return if (isPixTransfer) TransferMode.PIX else TransferMode.TED
    }

    private fun setupTextWatchers() {
        binding?.let {
            it.edtAgency.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        if (s?.length ?: ZERO_VALUE >= AGENCY_LIMIT_LENGTH) {
                            it.edtAccount.requestFocus()
                        }
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
                }
            )
            it.edtAccount.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        if (s?.length ?: ZERO_VALUE >= ACCOUNT_LIMIT_LENGTH) {
                            it.edtDigit.requestFocus()
                        }
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
                }
            )

            it.edtInstitution.keyListener = null
            it.edtInstitution.addTextChangedListener(
                object : TextWatcher {
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        it.edtInstitution.removeTextChangedListener(this)
                        val bank = it.edtInstitution.text
                        if (Paint().measureText(bank.toString()) >= it.edtInstitution.width) {
                            val institution = "${bank?.substring(
                                ZERO_VALUE,
                                it.edtInstitution.length() - INSTITUTION_LIMIT_LENGTH
                            )} ..."
                            it.edtInstitution.setText(institution)
                        }
                        it.edtInstitution.setSelection(ZERO_VALUE)
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit

                    override fun afterTextChanged(p0: Editable?) = Unit
                }
            )

            it.edtCpf.addTextChangedListener(
                object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {
                        val document = s.toString().unmask()

                        if (document.length == CPF_LENGTH && !document.validateCpf()) {
                            it.inputCpf.error = getString(R.string.ac_invalid_cpf)
                            it.inputCpf.isErrorEnabled = true
                        } else if (document.length >= CNPJ_LENGTH && !document.validateCNPJ()) {
                            it.inputCpf.error = getString(R.string.ac_invalid_cnpj)
                            it.inputCpf.isErrorEnabled = true
                        } else {
                            it.inputCpf.isErrorEnabled = false
                        }
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
                }
            )
        }
    }

    private fun setupListeners() {
        binding?.let {
            it.edtInstitution.setOnClickListener {
                viewModel.getBanks()
            }

            it.edtTitularity.setOnClickListener {
                SpinnerListDialog(
                    requireContext(),
                    viewModel.getOwnershipOptions(),
                    object : SpinnerListDialog.OnItemSelectListener {
                        override fun onItemSelected(code: Int) {
                            viewModel.setOwnership(code)
                        }
                    }
                ).show()
            }

            it.edtAccountType.setOnClickListener {
                SpinnerListDialog(
                    requireContext(),
                    viewModel.getAccountTypes(),
                    object : SpinnerListDialog.OnItemSelectListener {
                        override fun onItemSelected(code: Int) {
                            viewModel.onAccountTypeSelected(code)
                        }
                    }
                ).show()
            }

            it.edtTitularity.onFocusChangeListener = onFocusChanged()
            it.edtCpf.onFocusChangeListener = onFocusChanged()
            it.edtName.onFocusChangeListener = onFocusChanged()
            it.edtAccountType.onFocusChangeListener = onFocusChanged()
        }
    }

    private fun setupObservers() {
        viewModel.account.observe(viewLifecycleOwner) {
            if (it != null) {
                arguments = Bundle().apply {
                    putParcelable(ACCOUNT, it)
                    putBoolean(SAVE_FAVORED, viewModel.shouldSaveFavored() ?: false)
                    putBoolean(ISP2P, false)
                }
                callGoTo(TransferOpKey.TED_VALUE.name)
            }
        }

        viewModel.pixAccount.observe(viewLifecycleOwner) {
            arguments = Bundle().apply {
                putParcelable(ACCOUNT, it.account)
                putParcelable(Const.Transfer.ARG_PIX_TRANSFER_DETAIL, it.pixTransferDetail)
                putBoolean(SAVE_FAVORED, viewModel.shouldSaveFavored() ?: false)
                putBoolean(ISP2P, false)
            }
            callGoTo(TransferOpKey.TED_VALUE.name)
        }

        viewModel.tedAccountUIModel.agencyField.observe(viewLifecycleOwner) { viewModel.checkAccountFields() }
        viewModel.tedAccountUIModel.accountField.observe(viewLifecycleOwner) { viewModel.checkAccountFields() }
        viewModel.tedAccountUIModel.digitField.observe(viewLifecycleOwner) { viewModel.checkAccountFields() }
        viewModel.tedAccountUIModel.nameField.observe(viewLifecycleOwner) { viewModel.checkAccountFields() }
        viewModel.tedAccountUIModel.ownershipTypeField.observe(viewLifecycleOwner) { viewModel.checkAccountFields() }
        viewModel.tedAccountUIModel.accountTypeField.observe(viewLifecycleOwner) { viewModel.checkAccountFields() }
        viewModel.tedAccountUIModel.institutionField.observe(viewLifecycleOwner) { viewModel.checkInstitutionField() }
        viewModel.tedAccountUIModel.documentField.observe(viewLifecycleOwner) { viewModel.checkDocumentField() }
        viewModel.bankList.observe(viewLifecycleOwner) {}
        viewModel.allBanksList.observe(viewLifecycleOwner) {}
        viewModel.mutableAccount.observe(viewLifecycleOwner) {}

        viewModel.mutableBankList.observe(viewLifecycleOwner) {
            it?.let {
                bankListdialog?.dismiss()
                BankListDialog(requireContext(), it, viewModel).apply {
                    bankListdialog = this
                    show()
                }
            }
        }
    }

    private fun onFocusChanged() = View.OnFocusChangeListener { view, hasFocus ->
        if (hasFocus) {
            binding?.scrollviewTransfer?.smoothScrollTo(0, view.bottom)
        }
    }
}