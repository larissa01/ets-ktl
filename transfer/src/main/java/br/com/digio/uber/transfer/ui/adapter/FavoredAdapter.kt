package br.com.digio.uber.transfer.ui.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.databinding.ItemFavoredBinding
import br.com.digio.uber.transfer.mapper.toItemUIModel
import br.com.digio.uber.transfer.ui.adapter.viewholder.FavoredViewHolder
import br.com.digio.uber.transfer.uiModel.FavoredEditionUIModel
import br.com.digio.uber.transfer.uiModel.FavoredItem
import br.com.digio.uber.transfer.uiModel.FavoredItemUIModel
import br.com.digio.uber.transfer.uiModel.FavoredUIModel
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

typealias OnClickFavoredItem = (item: FavoredItemUIModel) -> Unit

class FavoredAdapter(
    private var values: MutableList<FavoredItemUIModel> = mutableListOf(),
    private val listener: OnClickFavoredItem
) : BaseRecyclerViewAdapter<FavoredItemUIModel, FavoredAdapter.ViewTypesDataBindingFactoryImpl>() {

    var favoredEdition: FavoredEditionUIModel? = null

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl =
        ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): FavoredItemUIModel =
        values[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder<FavoredItemUIModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<FavoredItemUIModel>
    }

    override fun getItemCount(): Int =
        values.size

    override fun onBindViewHolder(holder: AbstractViewHolder<FavoredItemUIModel>, position: Int) =
        holder.bind(values[holder.adapterPosition])

    override fun replaceItems(list: List<Any>) {
        val items: List<FavoredUIModel> = list.safeHeritage()
        val itemsUI: List<FavoredItemUIModel> = items.toItemUIModel()
        addValues(itemsUI)
    }

    private fun addValues(values: List<FavoredItemUIModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }

    fun updateEdition(editionUiModel: FavoredEditionUIModel?) {
        val oldFavoredUiModel = favoredEdition

        favoredEdition = editionUiModel

        if (editionUiModel == null && oldFavoredUiModel != null) {
            val index = values.indexOfFirst {
                (it as FavoredItem).favoredId == editionUiModel?.beneficiaryId ||
                    it.favoredId == oldFavoredUiModel.beneficiaryId
            }
            notifyItemChanged(index)
        } else {
            values.mapIndexed { index, baseAdapterItem ->
                if (baseAdapterItem is FavoredItem && baseAdapterItem.favoredId == editionUiModel?.beneficiaryId) {
                    notifyItemChanged(index)
                }
            }
        }
    }

    class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<FavoredItemUIModel> {
        override fun type(model: FavoredItemUIModel): Int =
            when (model) {
                is FavoredItem -> R.layout.item_favored
            }

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<FavoredItemUIModel>
        ): AbstractViewHolder<*> =
            when (type) {
                R.layout.item_favored -> FavoredViewHolder(
                    view.safeHeritage<ItemFavoredBinding>()!!,
                    listener
                )
                else -> throw IndexOutOfBoundsException("Invalid view type")
            }
    }
}