package br.com.digio.uber.transfer.navigation

import br.com.digio.uber.common.kenum.StatusBarColor

enum class TransferOpKey(val theme: StatusBarColor) {
    HOME(StatusBarColor.BLACK),
    TED(StatusBarColor.BLACK),
    P2P(StatusBarColor.BLACK),
    TED_ACCOUNT_DATA(StatusBarColor.BLACK),
    TED_FAVORED(StatusBarColor.BLACK),
    TED_VALUE(StatusBarColor.BLACK),
    TED_REVIEW(StatusBarColor.BLACK),
    P2P_ACCOUNT_DATA(StatusBarColor.BLACK),
    P2P_FAVORED(StatusBarColor.BLACK)
}