package br.com.digio.uber.transfer.mapper

import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.model.transfer.BankDomain
import br.com.digio.uber.model.transfer.BeneficiaryResponse
import br.com.digio.uber.model.transfer.FavoredDomainResponse
import br.com.digio.uber.model.transfer.FavoredListDomainResponse
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.enums.OwnershipType
import br.com.digio.uber.transfer.enums.TransferAccountType
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.transfer.uiModel.BankUIModel
import br.com.digio.uber.transfer.uiModel.FavoredItem
import br.com.digio.uber.transfer.uiModel.FavoredItemUIModel
import br.com.digio.uber.transfer.uiModel.FavoredUIModel
import br.com.digio.uber.util.formatAsAgencyNumber
import br.com.digio.uber.util.formatAsDocument
import br.com.digio.uber.util.formatAsFullAccountNumber
import br.com.digio.uber.util.getNameAbbreviation

const val BANK_DIGIO_CODE = "335"

fun List<BeneficiaryResponse>.toDomain(banks: List<BankDomain>): FavoredListDomainResponse {
    return FavoredListDomainResponse(
        favored = map { beneficiary ->
            val bank = banks.firstOrNull {
                it.code.toInt() == beneficiary.bankCode.toInt()
            }
            beneficiary.toDomain(bank)
        }
    )
}

fun List<FavoredUIModel>.toItemUIModel(): List<FavoredItemUIModel> {
    return this.map {
        FavoredItem(
            it.favoredName,
            it.favoredId,
            it.bankDetails,
            it.isEditable,
            it.onSelected,
            it.onDeleteClick,
            it.onEditClick,
            this.indexOf(it)
        )
    }
}

fun BeneficiaryResponse.toDomain(bank: BankDomain?): FavoredDomainResponse {
    return FavoredDomainResponse(
        uuid = uuid,
        sameOwnership = sameOwnership,
        branch = branch,
        accountNumber = accountNumber,
        bankCode = bankCode,
        bankName = "${bank?.code} - ${bank?.name}",
        document = document.formatAsDocument(),
        name = name,
        personType = personType,
        transferAccountType = transferAccountType,
        registerType = registerType,
        status = status
    )
}

fun FavoredListDomainResponse.toUiModel(
    onFavoredSelected: (position: Int) -> Unit,
    onDeleteClick: (position: Int) -> Unit,
    provider: ResourceManager,
    isEditable: Boolean
): List<FavoredUIModel> {
    return favored.map {
        it.toUiModel(onFavoredSelected, onDeleteClick, provider, isEditable)
    }
}

fun FavoredDomainResponse.toUiModel(
    onFavoredSelected: (position: Int) -> Unit,
    onDeleteClick: (position: Int) -> Unit,
    provider: ResourceManager,
    isEditable: Boolean
): FavoredUIModel {
    return FavoredUIModel(
        favoredId = uuid,
        favoredName = name,
        bankDetails = getBankText(provider),
        onSelected = onFavoredSelected,
        onDeleteClick = onDeleteClick,
        isEditable = isEditable
    )
}

fun FavoredDomainResponse.toUiModel(provider: ResourceManager): AccountUIModel {
    return AccountUIModel(
        userName = name,
        bankDetails = getBankText(provider),
        agencyCode = branch.formatAsAgencyNumber(),
        accountNumber = accountNumber.formatAsFullAccountNumber(),
        accountPhoto = null,
        hasImage = false,
        nameInitials = name.getNameAbbreviation(),
        cpf = document.formatAsDocument(),
        bankName = bankName,
        accountType = TransferAccountType.fromType(transferAccountType),
        bank = BankUIModel(bankCode, bankName, bankName),
        ownership = if (sameOwnership) OwnershipType.SAME_OWNERSHIP else OwnershipType.OTHER_OWNERSHIP
    )
}

private fun FavoredDomainResponse.getBankText(provider: ResourceManager): String {
    val accountType = TransferAccountType.fromType(transferAccountType)
    if (bankCode == BANK_DIGIO_CODE) {
        return bankCode + provider.getString(R.string.bank_digio_sa) + " " +
            provider.getString(
                TransferAccountType.getStringId(accountType, document)!!
            )
    }
    return "$bankName | " + provider.getString(
        TransferAccountType.getStringId(
            accountType,
            document
        )!!
    )
}