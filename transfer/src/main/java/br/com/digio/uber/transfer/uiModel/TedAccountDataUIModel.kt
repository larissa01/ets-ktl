@file:Suppress(
    "LongParameterList"
)

package br.com.digio.uber.transfer.uiModel

import android.view.View
import androidx.lifecycle.MutableLiveData

class TedAccountDataUIModel(
    val institutionField: MutableLiveData<String> = MutableLiveData(""),
    val agencyField: MutableLiveData<String> = MutableLiveData(""),
    val accountField: MutableLiveData<String> = MutableLiveData(""),
    val digitField: MutableLiveData<String> = MutableLiveData(""),
    val documentField: MutableLiveData<String> = MutableLiveData(""),
    val nameField: MutableLiveData<String> = MutableLiveData(""),
    val accountTypeField: MutableLiveData<String> = MutableLiveData(""),
    val ownershipTypeField: MutableLiveData<String> = MutableLiveData(""),
    val continueActivation: MutableLiveData<Boolean> = MutableLiveData(false),
    val labelWarnDigitVisible: MutableLiveData<Boolean> = MutableLiveData(false),
    val saveFavored: MutableLiveData<Boolean> = MutableLiveData(false),
    val ownershipFieldsVisibility: MutableLiveData<Int> = MutableLiveData(View.GONE)
)