package br.com.digio.uber.transfer.uiModel

import android.os.Parcelable
import br.com.digio.uber.transfer.base.adapter.BaseAdapterItem
import br.com.digio.uber.transfer.enums.UiType
import kotlinx.android.parcel.Parcelize

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */
@Parcelize
data class BankUIModel(
    val code: String,
    val title: String = "",
    val name: String
) : BaseAdapterItem(UiType.SPINNER_ITEM), Parcelable