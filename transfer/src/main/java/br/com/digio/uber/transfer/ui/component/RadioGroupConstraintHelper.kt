package br.com.digio.uber.transfer.ui.component

import android.content.Context
import android.util.AttributeSet
import android.widget.CompoundButton
import android.widget.RadioButton
import androidx.annotation.IdRes
import androidx.constraintlayout.widget.ConstraintHelper
import androidx.constraintlayout.widget.ConstraintLayout

/**
 * Usage example:
 * <pre>{@code
 *   <android.support.constraint.ConstraintLayout
 *     ...
 *     <br.com.digio.account.component.RadioGroupConstraintHelper
 *         android:layout_width="wrap_content"
 *         android:layout_height="wrap_content"
 *         app:constraint_referenced_ids="radioButton1, radioButton2" />
 *   </android.support.constraint.ConstraintLayout>
 * }</pre>
 */
class RadioGroupConstraintHelper @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintHelper(context, attrs, defStyleAttr) {
    /**
     *
     * Interface definition for a callback to be invoked when the checked
     * radio button changed in this group.
     */
    interface OnCheckedChangeListener {
        /**
         *
         * Called when the checked radio button has changed. When the
         * selection is cleared, checkedId is -1.
         *
         * @param radioGroup the group in which the checked radio button has changed
         * @param checkedId the unique identifier of the newly checked radio button
         */
        fun onCheckedChanged(radioGroup: RadioGroupConstraintHelper, @IdRes checkedId: Int)
    }

    private var onCheckedChangeListener: OnCheckedChangeListener? = null

    private val radioButtons: ArrayList<RadioButton> = arrayListOf()
    private var skipCheckingViewsRecursively: Boolean = false
    private var currentSelectedViewId: Int = -1
    private var selectedViewIdBeforePreLayout: Int = -1

    private val radioButtonListener =
        CompoundButton.OnCheckedChangeListener { compoundButton, _ ->
            if (skipCheckingViewsRecursively) {
                return@OnCheckedChangeListener
            }

            if (currentSelectedViewId != -1) {
                // uncheck the checked button
                skipCheckingViewsRecursively = true
                for (v in radioButtons) {
                    if (v.id == currentSelectedViewId) {
                        v.isChecked = false
                        break
                    }
                }
                skipCheckingViewsRecursively = false
            }

            currentSelectedViewId = compoundButton.id

            onCheckedChangeListener?.onCheckedChanged(this, currentSelectedViewId)
        }

    override fun init(attrs: AttributeSet?) {
        super.init(attrs)

        mUseViewMeasure = false
    }

    override fun updatePreLayout(container: ConstraintLayout) {
        super.updatePreLayout(container)

        for (i in 0 until mCount) {
            val id = mIds[i]
            val view = container.getViewById(id)
            if (view is RadioButton) {
                radioButtons.add(view)
                if (view.isChecked) currentSelectedViewId = view.id
                view.setOnCheckedChangeListener(radioButtonListener)
            }
        }

        if (selectedViewIdBeforePreLayout != -1) check(selectedViewIdBeforePreLayout)
    }

    override fun updatePostLayout(container: ConstraintLayout?) {
        val params = layoutParams
        params.width = 0
        params.height = 0

        super.updatePostLayout(container)
    }

    fun clearSelection() {
        if (currentSelectedViewId != -1) {
            // uncheck the checked button
            skipCheckingViewsRecursively = true
            for (v in radioButtons) {
                if (v.id == currentSelectedViewId) {
                    v.isChecked = false
                    break
                }
            }
            skipCheckingViewsRecursively = false

            currentSelectedViewId = -1
        }
    }

    /**
     * @return selected RadioButton id, -1 if no checked button
     */
    @IdRes
    fun getCheckedRadioButtonId(): Int {
        return currentSelectedViewId
    }

    /**
     * @param radioButtonId set it as checked
     */
    fun check(@IdRes radioButtonId: Int) {
        selectedViewIdBeforePreLayout = if (radioButtons.isEmpty()) radioButtonId else -1

        skipCheckingViewsRecursively = true
        for (view in radioButtons) {
            view.isChecked = view.id == radioButtonId
        }
        skipCheckingViewsRecursively = false

        currentSelectedViewId = radioButtonId
    }

    /**
     *
     * Register a callback to be invoked when the checked radio button
     * changes in this group.
     *
     * @param listener the callback to call on checked state change
     */
    fun setOnCheckedChangeListener(listener: OnCheckedChangeListener?) {
        onCheckedChangeListener = listener
    }

    fun setOnCheckedChangeListener(block: (radioGroup: RadioGroupConstraintHelper, checkedId: Int) -> Unit) {
        onCheckedChangeListener = object : OnCheckedChangeListener {
            override fun onCheckedChanged(radioGroup: RadioGroupConstraintHelper, checkedId: Int) {
                block(radioGroup, checkedId)
            }
        }
    }
}