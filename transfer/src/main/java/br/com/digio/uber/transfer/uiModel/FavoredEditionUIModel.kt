package br.com.digio.uber.transfer.uiModel

import br.com.digio.uber.transfer.enums.TransferAccountType

data class FavoredEditionUIModel(
    var nameInitials: String,
    var name: String,
    var bankName: String,
    var document: String,
    var documentType: String,
    var agencyField: String,
    var accountField: String,
    var digitField: String,
    var accountTypeField: String,
    var beneficiaryId: String = "",
    var accountType: TransferAccountType? = null,
    val onCancelClick: () -> Unit = {},
    val onSaveClick: () -> Unit = {},
    val position: Int
)