package br.com.digio.uber.transfer.enums

enum class TransferRegisterType {
    TED,
    P2P;

    companion object {
        fun fromType(type: String): TransferRegisterType {
            return values().firstOrNull {
                it.name == type
            } ?: throw IllegalArgumentException("TransferAccountType $type not mapped.")
        }
    }
}