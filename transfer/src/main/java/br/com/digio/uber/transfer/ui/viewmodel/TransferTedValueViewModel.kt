@file:Suppress("TooManyFunctions")

package br.com.digio.uber.transfer.ui.viewmodel

import android.text.Spannable
import android.text.SpannableStringBuilder
import androidx.core.text.toSpannable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.pix.extensions.toMoneyNumber
import br.com.digio.pix.extensions.unmaskMoney
import br.com.digio.pix.repository.model.PixValidatePaymentResponse
import br.com.digio.pix.ui.payment.summary.model.PixSummaryPaymentUIModel
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.analytics.utils.Tags.TRANSFER_VALUE
import br.com.digio.uber.common.balancetoolbar.balance.mapper.BalanceMapper
import br.com.digio.uber.common.balancetoolbar.balance.uiModel.BalanceUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.balance.GetBalanceUseCase
import br.com.digio.uber.domain.usecase.transfer.CanTransferTodayUseCase
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.enums.TransferAccountType
import br.com.digio.uber.transfer.mapper.PixAccountMapper
import br.com.digio.uber.transfer.mapper.PixSummaryPaymentMapper
import br.com.digio.uber.transfer.mapper.TransferStatusMapper
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.transfer.uiModel.TransferStatusUIModel
import br.com.digio.uber.transfer.uiModel.TransferUIModel
import br.com.digio.uber.util.DateFormatter
import br.com.digio.uber.util.toPattern
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.validateCNPJ
import java.util.Calendar
import java.util.Date
import java.util.concurrent.TimeUnit

/**
 * @author Marlon D. Rocha
 * @since 05/11/20
 */
class TransferTedValueViewModel(
    getBalanceUseCase: GetBalanceUseCase,
    private val canTransferTodayUseCase: CanTransferTodayUseCase,
    private val resourceManager: ResourceManager,
    private val analytics: Analytics,
) : BaseViewModel() {

    companion object {
        private const val DATE_PATTERN = "dd/MM/yyyy"
        const val INITIAL_VALUE = "0,00"
        const val DATE_EMPTY_TEXT = "__/__/____"
    }

    val balanceUiModel: LiveData<BalanceUiModel> = getBalanceUseCase(Unit).exec(showLoadingFlag = true).map(
        BalanceMapper()
    )

    val continueActivation: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = false
    }

    private val _transfer = MutableLiveData<TransferUIModel>().apply {
        value = TransferUIModel()
    }

    val transferValueData = MutableLiveData<String>().apply {
        value = INITIAL_VALUE
    }

    val tedWarnTextVisibilityMutableLiveData = MutableLiveData<Boolean>().apply {
        value = true
    }

    private val _valueErrorText = MutableLiveData<Spannable>()
    private val _valueHelperText = MutableLiveData<Spannable>()

    val valueErrorText: LiveData<Spannable>
        get() = _valueErrorText

    val valueHelperText: LiveData<Spannable>
        get() = _valueHelperText

    val accountTypeText = MutableLiveData<String>()

    val transferDateData = MutableLiveData<String>().apply {
        value = DATE_EMPTY_TEXT
    }

    val enableTransferToday = MutableLiveData<Boolean>().apply {
        value = true
    }

    val transferToday = MutableLiveData<Boolean>().apply {
        value = true
    }

    val isP2P = MutableLiveData<Boolean>().apply {
        value = false
    }

    val documentType = MutableLiveData<String>().apply {
        value = ""
    }

    val continueLiveData = MutableLiveData<Boolean>().apply {
        value = false
    }

    val continuePixLiveData = MutableLiveData<PixSummaryPaymentUIModel>()

    val mayChangeAmount = MutableLiveData<Boolean>().apply {
        value = true
    }

    val showTedDisclaimer = MutableLiveData<Boolean>(false)
    val descriptionData = MutableLiveData<String>()
    private val statusUIModel = MutableLiveData<TransferStatusUIModel>()

    val transfer: LiveData<TransferUIModel>
        get() = _transfer

    private val balanceValue = MutableLiveData<String>()

    fun initializer(pixData: PixValidatePaymentResponse?, onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        pixData?.let {
            val account = PixAccountMapper().map(pixData)
            _transfer.postValue(
                _transfer.value?.copy(
                    account = account,
                    pixTransferDetail = pixData,
                    value = pixData.amount ?: INITIAL_VALUE,
                    date = DateFormatter.dateBRFormatter.format(Date())
                )
            )
            tedWarnTextVisibilityMutableLiveData.value = false
            descriptionData.value = it.additionalInfo?.description ?: ""
            transferValueData.value = pixData.amount?.unmaskMoney()?.toMoneyNumber() ?: INITIAL_VALUE
            mayChangeAmount.postValue(pixData.mayChangeAmount)
            documentType.value = getAccountType(account)
        }
    }

    fun onSelectTransferToday() {
        setCalendarDateEmpty()
        _transfer.value?.date = DateFormatter.dateBRFormatter.format(Date())
    }

    private fun setCalendarDateEmpty() {
        showTedDisclaimer.value = false
        transferDateData.value = DATE_EMPTY_TEXT
    }

    fun getToday(): String {
        return Calendar.getInstance().toPattern(DATE_PATTERN)
    }

    fun goNext() {
        if (isPixTransfer()) {
            transfer.value?.let {
                continuePixLiveData.value = PixSummaryPaymentMapper(it).map(it.pixTransferDetail)
            }
        } else {
            continueLiveData.value = true
        }
    }

    private fun checkTransferViabilityStatus() {
        canTransferTodayUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                statusUIModel.value = TransferStatusMapper().map(it)
                statusUIModel.value?.let { status ->
                    _valueHelperText.value = String.format(
                        resourceManager.getString(
                            R.string.amount_permission,
                            status.geralMinAmount,
                            status.geralMaxAmount
                        )
                    ).toSpannable()

                    if (status.canTransfer || isPixTransfer()) {
                        enableTransferToday.value = true
                        transferToday.value = true
                        setCalendarDateEmpty()
                    } else {
                        enableTransferToday.value = false
                        transferToday.value = false
                        setCalendarDate(Calendar.getInstance().timeInMillis + TimeUnit.DAYS.toMillis(1))
                    }
                }
            }
        )
    }

    fun setupData(account: AccountUIModel, saveFavored: Boolean, isP2P: Boolean) {
        balanceValue.value = balanceUiModel.value?.balance
        _transfer.value = _transfer.value?.copy(
            account = account.copy(
                bankName = if (isP2P) resourceManager.getString(R.string.bank_digio) else account.bankName
            ),
            saveBeneficiary = saveFavored,
            isP2P = isP2P
        )

        this.isP2P.value = isP2P

        if (isP2P || isPixTransfer()) {
            tedWarnTextVisibilityMutableLiveData.value = false
            enableTransferToday.value = true
            transferToday.value = true
            setCalendarDateEmpty()
        } else {
            checkTransferViabilityStatus()
        }

        accountTypeText.value =
            if (isP2P) resourceManager.getString(R.string.digio_account) else TransferAccountType.getStringId(
                account.accountType!!,
                account.cpf!!
            )?.let { resourceManager.getString(it) }

        documentType.value =
            getAccountType(account)
    }

    private fun getAccountType(account: AccountUIModel): String? {
        return if (account.cpf!!.unmask().validateCNPJ()) {
            resourceManager.getString(R.string.cnpj)
        } else {
            resourceManager.getString(R.string.cpf)
        }
    }

    fun checkTransferDateData(transferDate: String) {
        _transfer.value?.date =
            if (transferDate != DATE_EMPTY_TEXT) {
                transferDate
            } else {
                DateFormatter.dateBRFormatter.format(Date())
            }
        checkContinueActivation()
    }

    fun checkTransferValueData(dataValue: String) {
        _transfer.value?.value = dataValue
        checkContinueActivation()
    }

    fun checkDescriptionData(description: String) {
        _transfer.value?.description = description
        checkContinueActivation()
    }

    private fun checkContinueActivation() {
        continueActivation.value = checkValueBalance() &&
            _transfer.value!!.value.unmask().toInt() > 0 &&
            !_transfer.value!!.description.isBlank()
    }

    private fun checkValueBalance(): Boolean {
        if (_transfer.value?.value.isNullOrBlank()) _transfer.value?.value = INITIAL_VALUE
        val balanceAmount = balanceUiModel.value?.balance?.unmask()?.toDouble() ?: 0.0
        val tedValue = _transfer.value?.value?.unmask()?.toDouble() ?: 0.0
        val minValue = (statusUIModel.value?.geralMinAmount ?: INITIAL_VALUE).unmask().toDouble()
        val maxValue = (statusUIModel.value?.geralMaxAmount ?: INITIAL_VALUE).unmask().toDouble()

        val canTransfer =
            if (isPixTransfer() || isP2P.value == true) {
                tedValue <= balanceAmount
            } else {
                tedValue in minValue..maxValue && tedValue <= balanceAmount
            }
        return if (canTransfer) {
            hideBalanceError()
            true
        } else {
            val spannable =
                if (!canTransfer && tedValue > balanceAmount) {
                    SpannableStringBuilder().append(
                        resourceManager.getString(
                            R.string.value_too_big,
                            balanceUiModel.value?.balance ?: INITIAL_VALUE
                        )
                    )
                } else {
                    _valueHelperText.value
                }
            triggerBalanceError(spannable)
            false
        }
    }

    private fun isPixTransfer() = _transfer.value?.pixTransferDetail != null

    private fun hideBalanceError() {
        _valueErrorText.value = SpannableStringBuilder()
    }

    private fun triggerBalanceError(spannable: Spannable?) {
        _valueErrorText.value = spannable
    }

    fun clearValue() {
        transferValueData.value = INITIAL_VALUE
    }

    fun onBtnContinueClick() {
        analytics.pushSimpleEvent(TRANSFER_VALUE)
    }

    fun onScheduleClick() {
        analytics.pushSimpleEvent(Tags.TRANSFER_RADIO_SCHEDULE)
    }

    fun cancelTransferScheduling() {
        if (enableTransferToday.value == true) {
            transferToday.value = true
            setCalendarDateEmpty()
        }
    }

    fun setCalendarDate(year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)
        val dateStr = DateFormatter.dateBRFormatter.format(calendar.time)
        transferDateData.value = dateStr

        showTedDisclaimer.value = true
    }

    private fun setCalendarDate(time: Long) {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = time
        val dateStr = calendar.toPattern(DATE_PATTERN)
        transferDateData.value = dateStr

        showTedDisclaimer.value = true
    }
}