package br.com.digio.uber.transfer.enums

enum class FavoredStatus {
    ACTIVE,
    DELETED;

    companion object {
        fun fromType(type: String): FavoredStatus {
            return values().firstOrNull {
                it.name == type
            } ?: throw IllegalArgumentException("TransferAccountType $type not mapped.")
        }
    }
}