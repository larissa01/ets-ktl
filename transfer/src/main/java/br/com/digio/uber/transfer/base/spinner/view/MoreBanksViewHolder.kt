package br.com.digio.uber.transfer.base.spinner.view

import br.com.digio.uber.transfer.base.adapter.BaseViewHolder
import br.com.digio.uber.transfer.databinding.ItemMoreBanksBinding
import br.com.digio.uber.transfer.uiModel.BankUIModel

class MoreBanksViewHolder(override val binding: ItemMoreBanksBinding) :
    BaseViewHolder<BankUIModel>(binding) {

    override fun bind(item: BankUIModel) = Unit
}