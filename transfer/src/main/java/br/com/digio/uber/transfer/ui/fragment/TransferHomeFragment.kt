package br.com.digio.uber.transfer.ui.fragment

import android.view.View
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.transfer.BR
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.databinding.TransferHomeFragmentBinding
import br.com.digio.uber.transfer.navigation.TransferOpKey
import br.com.digio.uber.transfer.ui.viewmodel.TransferHomeViewModel
import org.koin.android.ext.android.inject

class TransferHomeFragment : BNWFViewModelFragment<TransferHomeFragmentBinding, TransferHomeViewModel>() {

    override val viewModel: TransferHomeViewModel by inject()

    override fun tag(): String = TransferOpKey.HOME.name

    override fun getStatusBarAppearance(): StatusBarColor = TransferOpKey.HOME.theme

    override val bindingVariable: Int? = BR.transferHomeViewModel

    override val getLayoutId = R.layout.transfer_home_fragment

    companion object : FragmentNewInstance<TransferHomeFragment>(::TransferHomeFragment)

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it) }
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.btn_ted -> callGoTo(TransferOpKey.TED.name, arguments)
            R.id.btn_p2p -> callGoTo(TransferOpKey.P2P.name, arguments)
        }
    }
}