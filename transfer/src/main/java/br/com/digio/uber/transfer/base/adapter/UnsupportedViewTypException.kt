package br.com.digio.uber.transfer.base.adapter

class UnsupportedViewTypException(viewType: Int) :
    IllegalArgumentException("Unsupported view type '$viewType'.")