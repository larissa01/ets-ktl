package br.com.digio.uber.transfer.uiModel

data class FavoredUIModel(
    val favoredName: String = "",
    val favoredId: String = "",
    val bankDetails: String = "",
    val isEditable: Boolean = false,
    val onSelected: (position: Int) -> Unit = {},
    val onDeleteClick: (position: Int) -> Unit = {},
    val onEditClick: (position: Int) -> Unit = {}
)