package br.com.digio.uber.transfer.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.transfer.databinding.ItemAccountChooseBinding
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.util.safeHeritage

class AccountChooseAdapter(
    val listener: OnItemSelectedListener
) : RecyclerView.Adapter<AccountChooseAdapter.AccountChooseViewHolder>(), AdapterItemsContract {
    private val accountList: ArrayList<AccountUIModel> = arrayListOf()

    override fun replaceItems(list: List<Any>) {
        addValues(list.safeHeritage())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountChooseViewHolder {
        val binding = ItemAccountChooseBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AccountChooseViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AccountChooseViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            listener.onItemSelected(accountList[position])
        }
        holder.bind(accountList[position])
    }

    override fun getItemCount() = accountList.size

    class AccountChooseViewHolder(private val binding: ItemAccountChooseBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(account: AccountUIModel) {
            binding.account = account
        }
    }

    interface OnItemSelectedListener {
        fun onItemSelected(account: AccountUIModel)
    }

    private fun addValues(values: List<AccountUIModel>) {
        this.accountList.clear()
        this.accountList.addAll(values)
        notifyDataSetChanged()
    }
}