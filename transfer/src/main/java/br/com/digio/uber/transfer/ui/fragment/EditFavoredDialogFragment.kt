package br.com.digio.uber.transfer.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.digio.uber.common.base.dialog.BaseDialog
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.transfer.R
import br.com.digio.uber.transfer.base.spinner.view.SpinnerListDialog
import br.com.digio.uber.transfer.databinding.DialogEditFavoredBinding
import br.com.digio.uber.transfer.ui.viewmodel.TransferP2PFavoredViewModel
import br.com.digio.uber.transfer.ui.viewmodel.TransferTedFavoredViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior

class EditFavoredDialogFragment : BaseDialog() {
    private lateinit var tedViewModel: TransferTedFavoredViewModel
    private lateinit var p2pViewModel: TransferP2PFavoredViewModel
    private lateinit var binding: DialogEditFavoredBinding
    private var isP2P: Boolean = false

    companion object {
        const val EDIT_FAVORED_DIALOG_TAG = "EDIT_FAVORED_DIALOG_TAG"

        fun newInstance(viewModel: BaseViewModel, isP2P: Boolean) =
            EditFavoredDialogFragment().apply {
                if (viewModel is TransferTedFavoredViewModel) {
                    this.tedViewModel = viewModel
                } else if ((viewModel is TransferP2PFavoredViewModel)) {
                    this.p2pViewModel = viewModel
                }

                this.isP2P = isP2P
            }
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun tag(): String = EDIT_FAVORED_DIALOG_TAG

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override fun initialize() {
        setupListeners()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogEditFavoredBinding.inflate(inflater)
        if (isP2P) {
            binding.favoredUiModel = p2pViewModel.favoredEditionUIModel.value
        } else {
            binding.favoredUiModel = tedViewModel.favoredEditionUIModel.value
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()

        val bottomSheet = dialog?.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
        bottomSheet?.let {
            val behavior = BottomSheetBehavior.from(it)
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    private fun setupListeners() {
        binding.edtAccountType.setOnClickListener {
            SpinnerListDialog(
                requireContext(),
                if (isP2P) {
                    p2pViewModel.getAccountTypes()
                } else {
                    tedViewModel.getAccountTypes()
                },

                object : SpinnerListDialog.OnItemSelectListener {
                    override fun onItemSelected(code: Int) {
                        if (isP2P) {
                            p2pViewModel.onAccountTypeSelected(code)
                        } else {
                            tedViewModel.onAccountTypeSelected(code)
                        }
                    }
                }
            ).show()
        }
        binding.btnSave.setOnClickListener {
            binding.favoredUiModel?.onSaveClick?.invoke()
            dismiss()
        }
        binding.btnCancel.setOnClickListener {
            binding.favoredUiModel?.onCancelClick?.invoke()
            dismiss()
        }

        if (isP2P) {
            p2pViewModel.favoredEditionUIModel.observe(
                viewLifecycleOwner,
                {
                    it?.let { favored ->
                        binding.favoredUiModel = favored
                        binding.executePendingBindings()
                    }
                }
            )
        } else {
            tedViewModel.favoredEditionUIModel.observe(
                viewLifecycleOwner,
                {
                    it?.let { favored ->
                        binding.favoredUiModel = favored
                        binding.executePendingBindings()
                    }
                }
            )
        }
    }
}