package br.com.digio.uber.transfer.enums

import br.com.digio.uber.transfer.R
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.validateCNPJ

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */
enum class TransferAccountType(val personalResId: Int, val juridicResId: Int?, val pixType: Int) {
    INDIVIDUAL_CHECKING_ACCOUNT(
        R.string.individual_checking_account,
        R.string.individual_checking_account_juridic,
        R.string.individual_checking_account_pix
    ),
    INDIVIDUAL_SAVINGS_ACCOUNT(
        R.string.individual_savings_account,
        R.string.individual_savings_account_juridic,
        R.string.individual_savings_account_pix
    ),
    JOINT_CHECKING_ACCOUNT(
        R.string.joint_checking_account,
        null,
        R.string.joint_checking_account_pix
    ),
    JOINT_SAVINGS_ACCOUNT(
        R.string.joint_savings_account,
        null,
        R.string.joint_savings_account_pix
    ),
    PAYMENT_ACCOUNT(
        R.string.payment_account,
        R.string.payment_account_juridic,
        R.string.payment_account_pix
    );

    companion object {
        fun fromType(type: String): TransferAccountType {
            return values().firstOrNull {
                it.name == type
            } ?: throw IllegalArgumentException("TransferAccountType $type not mapped.")
        }

        fun getStringId(accountType: TransferAccountType, document: String): Int? {
            return if (document.unmask().validateCNPJ()) {
                accountType.juridicResId
            } else {
                accountType.personalResId
            }
        }
    }
}