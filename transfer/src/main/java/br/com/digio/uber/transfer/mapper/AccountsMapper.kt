package br.com.digio.uber.transfer.mapper

import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.model.transfer.ThirdPartyAccountListResponse
import br.com.digio.uber.transfer.uiModel.AccountUIModel
import br.com.digio.uber.util.mapper.AbstractMapper

class AccountsMapper(private val resourceManager: ResourceManager) :
    AbstractMapper<ThirdPartyAccountListResponse, List<AccountUIModel>> {

    override fun map(param: ThirdPartyAccountListResponse): List<AccountUIModel> = with(param) {
        val items = mutableListOf<AccountUIModel>()

        accounts.map {
            items.add(
                AccountMapper(resourceManager).map(it)
            )
        }
        return items
    }
}