package br.com.digio.uber.transfer.ui.adapter.viewholder

import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.transfer.databinding.ItemFavoredBinding
import br.com.digio.uber.transfer.uiModel.FavoredItem
import br.com.digio.uber.transfer.uiModel.FavoredItemUIModel

class FavoredViewHolder(
    private val viewDataBinding: ItemFavoredBinding,
    private val listener: ViewTypesListener<FavoredItemUIModel>
) : AbstractViewHolder<FavoredItem>(viewDataBinding.root) {

    override fun bind(item: FavoredItem) {
        viewDataBinding.favoredItem = item
        viewDataBinding.viewHolder = this
        viewDataBinding.root.setOnClickListener {
            item.onSelected.invoke(item.position)
        }
        viewDataBinding.executePendingBindings()
    }

    fun onClick(item: FavoredItem) {
        listener(item)
    }
}