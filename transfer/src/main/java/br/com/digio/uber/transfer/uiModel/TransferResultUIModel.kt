package br.com.digio.uber.transfer.uiModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TransferResultUIModel(
    val code: String
) : Parcelable