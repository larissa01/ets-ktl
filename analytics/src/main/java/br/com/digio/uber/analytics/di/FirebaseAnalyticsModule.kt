package br.com.digio.uber.analytics.di

import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.AnalyticsImpl
import org.koin.dsl.module

val firebaseModule = module {
    single<Analytics> {
        AnalyticsImpl(
            get()
        )
    }
}