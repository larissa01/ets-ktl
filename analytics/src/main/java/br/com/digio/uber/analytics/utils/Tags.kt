package br.com.digio.uber.analytics.utils

object Tags {

    const val BANKSLIP_GENERATE = "receber_gerar_boleto"
    const val BANKSLIP_GENERATE_SUCCESS = "receber_boleto_sucesso"
    const val BANKSLIP_RECEIVE_OPTION = "receber_consulta_boleto_opcao"
    const val PARAMETER_BANKSLIP_OPTION = "opcao_selecionada"
    const val BANKSLIP_SEE_PDF = "bankslip_see_pdf"
    const val BANKSLIP_COPY_CODE = "bankslip_copy_code"

    const val TRANSFER_RADIO_SCHEDULE = "transfer_radio_schedule"
    const val TRANSFER_BUTTON_DELETE_FAVORED = "transfer_button_delete_favored"
    const val TRANSFER_BUTTON_EDIT_FAVORED = "transfer_button_edit_favored"
    const val TRANSFER_OPTION = "transferencia_opcao"
    const val TRANSFER_CHOOSED = "transferencia_selecionada"
    const val TRANSFER_CHOOSE_UBER = "para_uber_conta"
    const val TRANSFER_CHOOSE_ANOTHER = "para_outros"
    const val TRANSFER_ACCOUNT_DATA = "transferencia_dados_conta"
    const val TRANSFER_ACCOUNT_DATA_ERROR = "transferencia_dados_conta_erro"
    const val TRANSFER_VALUE = "transferencia_valor"
    const val TRANSFER_REVIEW = "transferencia_revisao"
    const val TRANSFER_RECEIPT = "transferencia_recibo"

    const val WITHDRAW_VALUE = "sacar_valor"
    const val WITHDRAW_SUCCESS = "sacar_sucesso"
    const val WITHDRAW_ERROR = "sacar_erro"
    const val WITHDRAW_ERROR_HAS_NOT = "terminal nao possui"
    const val WITHDRAW_NOT_DEBITED = "valor_nao_debitado"

    const val PAYMENT_ACCOUNT_PAGUE_PAGAR_CONTA_CLIQUE = "pagar_pague_pagar_conta_clique"
    const val PAYMENT_ACCOUNT_PAG_CONTAS_COD_BARRAS_ERRO = "pagar_pag_contas_cod_barras_erro"
    const val PAYMENT_ACCOUNT_PAG_CONTAS_COD_BARRAS_SEGUIR = "pagar_pag_contas_cod_barras_seguir"
    const val PAYMENT_ACCOUNT_PAG_CONTAS_DETALHE_CONTINUAR = "pagar_pag_contas_detalhe_continuar"
    const val PAYMENT_ACCOUNT_PAG_CONTAS_COM_CONTA_SUCESSO = "pagar_pag_contas_com_conta_sucesso"
    const val PAYMENT_ACCOUNT_PAG_CONTAS_COM_CONTA_INICIO = "pagar_pag_contas_com_conta_inicio"
    const val PAYMENT_ACCOUNT_PAG_CONTAS_COM_CONTA_COMPART = "pagar_pag_contas_com_conta_compart"

    const val INCOME_ACCOUNT_MONTH = "conta_rendimentos_mes"

    const val CARD_RECEIVED_ACTIVATION_ERROR = "ativar_cartao_erro"
    const val CARD_RECEIVED_ACTIVATION_SUCCESS = "ativar_cartao_sucesso"

    // LOGIN

    const val HAVE_ACCOUNT = "tenho_conta"
    const val OPEN_MY_ACCOUNT = "abrir_minha_conta"
    const val LOGIN_CPF = "login_cpf"
    const val LOGIN_PASSWORD_ACCESS = "login_senha_acesso"

    const val LOGIN_ERROS = "login_erros"

    const val PARAMETER_CPF_INVALID = "cpf_invalido"
    const val PARAMETER_PASSWORD_ACCESS_INVALID = "senha_acesso_invalida"

    // END LOGIN

    // CHANGE USER REGISTRATION

    const val DATA_REGISTERED_ITEM = "dados_cadastro_item"
    const val DATA_REGISTERED_CHANGED = "dados_cadastro_alterado"

    const val PARAMETER_ITEM_CHANGE_REGISTRATION_CLICKED = "item_clicado"

    // END CHANGE USER REGISTRATION

    // CHANGE PASSWORD

    const val LOGIN_CREATE_PASSWORD = "login_criar_senha"
    const val LOGIN_CHANGE_PASSWORD_SUCCESS = "login_senha_sucesso"

    const val PARAMETER_CREATE_PASSWORD = "criar_senha"

    // END CHANGE PASSWORD

    // FACIAL BIOMETRY

    const val LOGIN_BIOMETRY_ACTIVE = "login_biometria_ativa"
    const val LOGIN_BIOMETRY_CONTINUE = "login_biometria_continuar"
    const val PARAMETER_LOGIN_BIOMETRY_ACTIVE = "parametro_login_biometria_ativa"

    // END FACIAL BIOMETRY

    // RESET PASSWORD

    const val RESET_PASSWORD_BIRTHDATE = "esqueci_senha_nascimento"
    const val RESET_COD_SUCCESS = "esqueci_senha_cod_sucesso"

    const val PARAMETER_RESET_PASSWORD = "parametro_nascimento_invalido"

    // END RESET PASSWORD

    // SHOW CARD PASSWORD

    const val SHOW_CARD_PASSWORD_BIOMETRY = "ver_senha_do_cartao_continuar"
    const val SHOW_CARD_PASSWORD_OK = "ver_senha_cartao_ok"

    // END SHOW CARD PASSWORD
}