package br.com.digio.uber.analytics

import android.os.Bundle
import br.com.digio.uber.abstractFirebase.FirebaseAnalyticsAbstract
import br.com.digio.uber.util.BuildTypes

class AnalyticsImpl(
    private val firebaseAnalytics: FirebaseAnalyticsAbstract
) : Analytics {

    override fun pushSimpleEvent(event: String, pair: Pair<String, String>?) {
        firebaseAnalytics.logEvent(event.addPrefixInTag(), pair?.let { mapOf(it).toBundle() })
    }

    override fun pushComplexEvent(event: String, map: Map<String, String>?) {
        firebaseAnalytics.logEvent(event.addPrefixInTag(), map?.toBundle())
    }

    private fun String.addPrefixInTag(): String =
        when (BuildConfig.BUILD_TYPE) {
            BuildTypes.DEBUG.value -> "dev_$this"
            BuildTypes.HOMOL.value -> "hml_$this"
            BuildTypes.RELEASE.value -> this
            else -> this
        }

    private fun Map<String, String>.toBundle(): Bundle =
        entries.fold(Bundle()) { acc, entry ->
            acc.putString(entry.key, entry.value)
            acc
        }
}