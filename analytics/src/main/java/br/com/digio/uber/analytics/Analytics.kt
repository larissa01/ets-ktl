package br.com.digio.uber.analytics

interface Analytics {
    fun pushSimpleEvent(event: String, pair: Pair<String, String>? = null)
    fun pushComplexEvent(event: String, map: Map<String, String>? = null)
}