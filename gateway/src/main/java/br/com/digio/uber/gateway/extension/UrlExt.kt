package br.com.digio.uber.gateway.extension

import br.com.digio.uber.gateway.BuildConfig
import br.com.digio.uber.gateway.URLs
import br.com.digio.uber.util.BuildTypes

fun getUrlByBuildType(): URLs =
    when (BuildConfig.BUILD_TYPE) {
        BuildTypes.DEBUG.value -> URLs.URL_UBER_DEV
        BuildTypes.HOMOL.value -> URLs.URL_UBER_HML
        BuildTypes.RELEASE.value -> URLs.URL_UBER_PROD
        else -> URLs.URL_UBER_DEV
    }