package br.com.digio.uber.gateway.extension

import android.content.Context
import br.com.digio.uber.util.safeHeritage
import java.io.BufferedInputStream
import java.io.InputStream
import java.security.KeyStore
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager
import kotlin.jvm.Throws

@Throws
fun Int.getCertificateDerX509(context: Context): Pair<SSLSocketFactory, X509TrustManager?>? {
    // Generate the certificate using the certificate file under res/raw/cert.crt
    val ca = getCertificate(context)
    // create a KeyStore containing our trusted CAs
    val trusted = getKeyStore(ca)
    // create a TrustManager that trusts the CAs in our KeyStore
    val tmf = generateTrustManager(trusted)
    val sslContext = SSLContext.getInstance("TLS")
    sslContext.init(null, tmf?.trustManagers, null)
    return sslContext.socketFactory to tmf?.trustManagers?.getX509TrustManager()
}

private fun generateTrustManager(trusted: KeyStore?): TrustManagerFactory? {
    val tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm()
    val tmf = TrustManagerFactory.getInstance(tmfAlgorithm)
    tmf.init(trusted)
    return tmf
}

@Throws
private fun getKeyStore(ca: Certificate?): KeyStore? {
    val keyStoreType = KeyStore.getDefaultType()
    val trusted = KeyStore.getInstance(keyStoreType)
    trusted.load(null, null)
    trusted.setCertificateEntry("ca", ca)
    return trusted
}

@Throws
private fun Int.getCertificate(context: Context): Certificate? {
    val cf = CertificateFactory.getInstance("X.509")
    val caInput: InputStream = BufferedInputStream(context.resources.openRawResource(this))
    val ca = cf.generateCertificate(caInput)
    caInput.close()
    return ca
}

fun Array<TrustManager>.getX509TrustManager(): X509TrustManager? =
    mapNotNull { it.safeHeritage<X509TrustManager>() }.firstOrNull()