package br.com.digio.uber.gateway.exception

import okio.IOException

class ConverterEncryptException(message: String) : IOException(message)