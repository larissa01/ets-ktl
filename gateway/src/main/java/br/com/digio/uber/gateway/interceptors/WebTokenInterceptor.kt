package br.com.digio.uber.gateway.interceptors

import br.com.digio.uber.gateway.interceptors.endpoint.WebTokenRefreshTokenEndpoint.jsonContentType
import br.com.digio.uber.gateway.interceptors.endpoint.WebTokenRefreshTokenEndpoint.makeNewRequest
import br.com.digio.uber.util.SingletonHolder
import br.com.digio.uber.util.TokenPreferencesUtils
import okhttp3.Interceptor
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.HttpException
import java.net.HttpURLConnection
import java.util.Calendar
import java.util.Locale
import java.util.TimeZone

class WebTokenInterceptor private constructor(
    private val tokenPreferencesUtils: TokenPreferencesUtils
) : Interceptor {

    companion object :
        SingletonHolder<WebTokenInterceptor, TokenPreferencesUtils>(::WebTokenInterceptor) {
        private const val AUTH_HEADER = "Authorization"
        private const val UNIX_TIME_TO_JAVA_TIMESTAMP_MULTIPLY = 1000L
        private const val LOGOUT_ENDPOINT = "/auth/logout"
    }

    @Synchronized
    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        val request = request()
        if (!request.url.pathSegments.joinToString(String()).contains(LOGOUT_ENDPOINT)) {
            try {
                checkNeedUpdateTokenAndIfIsTrueMakeIt()
            } catch (e: HttpException) {
                return makeErrorLogoff(e)
            }
        }

        val tokenSave: String? = tokenPreferencesUtils.getToken()

        val newRequestWithToken = if (tokenSave != null) {
            applyToken(request, tokenSave)
        } else {
            request
        }

        chain.proceed(newRequestWithToken)
    }

    private fun Interceptor.Chain.makeErrorLogoff(e: HttpException): Response = Response.Builder()
        .request(request())
        .protocol(Protocol.HTTP_2)
        .code(HttpURLConnection.HTTP_UNAUTHORIZED)
        .body(String().toResponseBody(jsonContentType()))
        .message(e.message())
        .build()

    private fun Interceptor.Chain.checkNeedUpdateTokenAndIfIsTrueMakeIt() {
        val currencyCalendar = Calendar.getInstance(
            TimeZone.getTimeZone("GMT-0"),
            Locale.getDefault()
        )

        val tokenTime = tokenPreferencesUtils.getTimestampToken() ?: 0L

        val restTime =
            (tokenTime * UNIX_TIME_TO_JAVA_TIMESTAMP_MULTIPLY) - currencyCalendar.time.time

        if (restTime <= 0) {
            val loginRes = makeNewRequest(tokenPreferencesUtils)
            tokenPreferencesUtils.setToken(loginRes.accessToken)
            tokenPreferencesUtils.setRefreshToken(loginRes.refreshToken)
            tokenPreferencesUtils.setTimestampToken(loginRes.accessExpiresIn)
        }
    }

    private fun applyToken(request: Request, token: String): Request =
        request.newBuilder().addHeader(AUTH_HEADER, "Bearer $token").build()
}