package br.com.digio.uber.gateway.endpoint

import br.com.digio.uber.model.BalanceResponse
import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.account.AccountResponse
import br.com.digio.uber.model.account.AvatarRequest
import br.com.digio.uber.model.account.AvatarResponse
import br.com.digio.uber.model.bankSlip.BankSlipRequest
import br.com.digio.uber.model.bankSlip.BankSlipResponse
import br.com.digio.uber.model.bankSlip.BankSlipStatus
import br.com.digio.uber.model.bankSlip.CustomerLimitsResponse
import br.com.digio.uber.model.bankSlip.FileResponse
import br.com.digio.uber.model.bankSlip.GeneratedBankSlipResponse
import br.com.digio.uber.model.card.CardResponse
import br.com.digio.uber.model.card.VirtualCardResponse
import br.com.digio.uber.model.cardReceived.CardReceivedActivationRequest
import br.com.digio.uber.model.cardreissue.LostStolenRequest
import br.com.digio.uber.model.cardreissue.LostStolenResponse
import br.com.digio.uber.model.cashback.CashbackAmount
import br.com.digio.uber.model.cashback.CashbackComissionCategory
import br.com.digio.uber.model.cashback.CashbackCustomerResponse
import br.com.digio.uber.model.cashback.CashbackDetailsResponse
import br.com.digio.uber.model.cashback.CashbackOfferResponse
import br.com.digio.uber.model.cashback.CashbackPurchase
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.model.cashback.CashbackTerms
import br.com.digio.uber.model.cashback.SendCashbackRequest
import br.com.digio.uber.model.change.password.ChangePasswordAuthReq
import br.com.digio.uber.model.change.password.ChangePasswordReq
import br.com.digio.uber.model.change.registration.AddressRegistrationRequest
import br.com.digio.uber.model.change.registration.ChangeRegistrationData
import br.com.digio.uber.model.change.registration.ChangeRegistrationRequest
import br.com.digio.uber.model.change.registration.ZipCodeRes
import br.com.digio.uber.model.faq.FaqChatResponse
import br.com.digio.uber.model.faq.FaqChatUrlRequest
import br.com.digio.uber.model.faq.FaqResponse
import br.com.digio.uber.model.franchise.FranchiseResponse
import br.com.digio.uber.model.gemalto.Enroll
import br.com.digio.uber.model.gemalto.EnrollRequest
import br.com.digio.uber.model.income.AvailableMonthsResponse
import br.com.digio.uber.model.income.DetailDepositResponse
import br.com.digio.uber.model.income.ListIncomesResponse
import br.com.digio.uber.model.login.LoginReq
import br.com.digio.uber.model.login.LoginRes
import br.com.digio.uber.model.login.refresh.req.RefreshReq
import br.com.digio.uber.model.payment.BillBarcode
import br.com.digio.uber.model.payment.ConfirmPayResponse
import br.com.digio.uber.model.payment.PayWithQrCodeRequest
import br.com.digio.uber.model.payment.PaymentBarcodeRequest
import br.com.digio.uber.model.payment.QrCodeRequest
import br.com.digio.uber.model.payment.QrCodeResponse
import br.com.digio.uber.model.pid.PidReq
import br.com.digio.uber.model.pid.PidRes
import br.com.digio.uber.model.pid.PidStepReq
import br.com.digio.uber.model.receipt.Receipt
import br.com.digio.uber.model.recharge.FavoriteContact
import br.com.digio.uber.model.recharge.FavoriteContactList
import br.com.digio.uber.model.recharge.RechargeValues
import br.com.digio.uber.model.remuneration.IncomeIndexResponse
import br.com.digio.uber.model.resetpassword.ResetPasswordReq
import br.com.digio.uber.model.resetpassword.ResetPasswordRes
import br.com.digio.uber.model.show.card.password.CardPasswordReq
import br.com.digio.uber.model.show.card.password.CardPasswordRes
import br.com.digio.uber.model.statement.FutureEntriesResponse
import br.com.digio.uber.model.statement.PastEntriesResponse
import br.com.digio.uber.model.statement.StatementDetailResponse
import br.com.digio.uber.model.store.StoreCatalog
import br.com.digio.uber.model.store.StoreOrder
import br.com.digio.uber.model.store.StoreOrderRegisterResponse
import br.com.digio.uber.model.store.StoreOrderResponse
import br.com.digio.uber.model.tracking.TrackingLogResponse
import br.com.digio.uber.model.transfer.AccountDomainResponse
import br.com.digio.uber.model.transfer.BankDomainResponse
import br.com.digio.uber.model.transfer.BeneficiariesResponse
import br.com.digio.uber.model.transfer.BeneficiaryRequest
import br.com.digio.uber.model.transfer.BeneficiaryResponse
import br.com.digio.uber.model.transfer.ExternalTransferRequest
import br.com.digio.uber.model.transfer.InternalTransferRequest
import br.com.digio.uber.model.transfer.PixBankDomainResponse
import br.com.digio.uber.model.transfer.ThirdPartyAccountListResponse
import br.com.digio.uber.model.transfer.ThirdPartyAccountResponse
import br.com.digio.uber.model.transfer.TransferResponse
import br.com.digio.uber.model.transfer.TransferStatus
import br.com.digio.uber.model.withdraw.WithdrawReq
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query

@Suppress("TooManyFunctions")
interface UberEndpoints {

    @POST("/auth/login")
    suspend fun makeLogin(@Body body: LoginReq): LoginRes

    @GET("/account/balance")
    suspend fun getBalance(): BalanceResponse

    @GET("/remuneration/income/index")
    suspend fun getIncome(): IncomeIndexResponse

    @POST("auth/enroll")
    suspend fun getUserEnroll(@Body deviceInfo: EnrollRequest): Enroll

    @GET("/recharge/prices")
    suspend fun getOperatorPrices(@Query("provider") provider: String): RechargeValues

    @GET("/recharge/favorite")
    suspend fun getFavoritesContacts(): FavoriteContactList

    @POST("/recharge/favorite/register")
    suspend fun registerFavoriteContact(@Body favoriteContact: FavoriteContact)

    @PUT("/recharge/favorite/nickname")
    suspend fun updateFavoriteContact(@Body favoriteContact: FavoriteContact)

    @POST("/recharge/favorite/remove")
    suspend fun removeFavoriteContact(@Body favoriteContact: FavoriteContact)

    @GET("/store/catalog/products")
    suspend fun getStoreCatalog(): StoreCatalog

    @POST("/store/orders")
    suspend fun registerStoreOrder(@Body order: StoreOrder): StoreOrderRegisterResponse

    @GET("bankslip/status")
    suspend fun getBankSlipStatus(): BankSlipStatus

    @GET("/bankslip/{uuid}")
    suspend fun getBankSlipByUuid(@Path("uuid") uuid: String): FileResponse

    @POST("/bankslip/generate")
    suspend fun generateBankSlip(@Body bankSlipRequest: BankSlipRequest): BankSlipResponse

    @GET("/bankslip")
    suspend fun getGeneratedBankSlips(@Query("status") status: String?): GeneratedBankSlipResponse

    @GET("/card/visible/all")
    suspend fun getCardList(): List<CardResponse>

    @GET("/virtual-card/{cardId}")
    suspend fun getVirtualCard(
        @Path("cardId") id: Long,
        @Header("X-Pid-Hash") hash: String
    ): VirtualCardResponse

    @POST("/virtual-card/{cardId}/new-card")
    suspend fun postVirtualCardRegenerate(
        @Path("cardId") id: Long,
    ): VirtualCardResponse

    @POST("/virtual-card/{cardId}/block")
    suspend fun postVirtualCardBlock(
        @Path("cardId") id: Long,
    ): Response<Void>

    @POST("/virtual-card/{cardId}/unblock")
    suspend fun postVirtualCardunblock(
        @Path("cardId") id: Long,
    ): Response<Void>

    @POST("/payment/qrcode/read")
    suspend fun postQrCode(
        @Body qrCodeRequest: QrCodeRequest,
    ): QrCodeResponse

    @POST("payment/qrcode/execute")
    suspend fun postPayWithQrCode(
        @Body request: PayWithQrCodeRequest,
        @Header("X-Pid-Hash") hash: String
    ): Response<Void>

    @PUT("/auth/forgot-password")
    suspend fun forgotPassword(@Body resetPasswordReq: ResetPasswordReq): ResetPasswordRes

    @PUT("/auth/change-password")
    suspend fun changePassword(@Body changePasswordReq: ChangePasswordReq)

    @PUT("/auth/change-password-auth")
    suspend fun changePasswordAuth(@Body changePasswordReq: ChangePasswordAuthReq)

    @GET("/faq")
    suspend fun getFaq(@Header("faqversion") faqVersion: String): FaqResponse

    @POST("/chat")
    suspend fun getFaqChatUrl(
        @Body request: FaqChatUrlRequest,
        @Header("x-product-type") hash: String = "UBER"
    ): FaqChatResponse

    // < ----------- TED Endpoints ------------>
    @GET("/account")
    suspend fun getAccountTed(): AccountDomainResponse

    @GET("/bank")
    suspend fun getBanks(): BankDomainResponse

    @GET("/pix/settlement/institution")
    suspend fun getPixBanks(): PixBankDomainResponse

    @GET("/bank/popular")
    suspend fun getPopularBanks(): BankDomainResponse

    @GET("/pix/settlement/institution/popular")
    suspend fun getPopularPixBanks(): PixBankDomainResponse

    @GET("/transfer/external/status")
    suspend fun getTransferStatus(): TransferStatus

    @POST("transfer/external/same-ownership")
    suspend fun confirmTransferExternalSameOwnership(
        @Body transferRequest: ExternalTransferRequest
    ): TransferResponse

    @POST("transfer/external/third-party")
    suspend fun confirmTransferExternalThirdParty(
        @Body transferRequest: ExternalTransferRequest
    ): TransferResponse

    @POST("transfer/internal")
    suspend fun confirmTransferInternal(
        @Body transferRequest: InternalTransferRequest
    ): TransferResponse

    @GET("beneficiary/ted")
    suspend fun getTedBeneficiaries(): BeneficiariesResponse

    @DELETE("beneficiary/ted/{id}")
    suspend fun deleteTedBeneficiary(
        @Path("id") beneficiaryId: String
    ): BeneficiaryResponse

    @PUT("beneficiary/ted/{id}")
    suspend fun putTedBeneficiary(
        @Path("id") beneficiaryId: String,
        @Body request: BeneficiaryRequest
    ): BeneficiaryResponse

    @GET("beneficiary/p2p")
    suspend fun getP2pBeneficiaries(): BeneficiariesResponse

    @PUT("beneficiary/p2p/{id}")
    suspend fun putP2pBeneficiary(
        @Path("id") beneficiaryId: String,
        @Body request: BeneficiaryRequest
    ): BeneficiaryResponse

    @DELETE("beneficiary/p2p/{id}")
    suspend fun deleteP2pBeneficiary(
        @Path("id") beneficiaryId: String
    ): BeneficiaryResponse

    // < ----------- End of TED Endpoints ------------>

    @GET("/account/third-party/accounts/{identifier}")
    suspend fun getAccountListByCPF(
        @Path("identifier") identifier: String
    ): ThirdPartyAccountListResponse

    @GET("account/third-party/accounts/{agency}/{account}")
    suspend fun searchAccount(
        @Path("agency") agency: String,
        @Path("account") account: String
    ): ThirdPartyAccountResponse

    @GET("statement")
    suspend fun getStatement(
        @Query("startDate") startDate: String?,
        @Query("endDate") endDate: String?,
        @Query("days") days: Int?,
        @Query("lastDate") lastDate: String?
    ): PastEntriesResponse

    @GET("statement/resume")
    suspend fun getStatementResume(@Query("monthsBefore") monthsBefore: Int?)

    @GET("statement/future")
    suspend fun getFutureStatement(): FutureEntriesResponse

    @GET("/statement/{id}")
    suspend fun getStatementDetails(@Path("id") id: String): StatementDetailResponse

    @GET("/receipt/{id}")
    suspend fun getReceipt(@Path("id") id: String): Receipt

    @DELETE("/scheduling/{id}")
    suspend fun deleteScheduledEntry(@Path("id") id: String): Response<ResponseBody>

    @GET("/account/")
    suspend fun getAccount(): AccountResponse

    @GET("/profile/avatar")
    suspend fun getProfileAvatar(): AvatarResponse

    @GET("/profile/third-party/avatar/{documentId}")
    suspend fun getThirdPartyProfileAvatar(@Path("documentId") documentId: String): AvatarResponse

    @PUT("/profile/avatar")
    suspend fun putProfileAvatar(@Body avatarRequest: AvatarRequest)

    @POST("/pid/init")
    suspend fun firstPid(@Body req: PidReq): PidRes

    @GET("remuneration/income/available-year-months")
    suspend fun getAvailableMonths(): AvailableMonthsResponse

    @GET("remuneration/income/{yearMonth}/details")
    suspend fun getIncomeDetails(@Path("yearMonth") yearMonth: String): ListIncomesResponse

    @GET("remuneration/deposit/{depositId}")
    suspend fun getDepositDetails(@Path("depositId") depositId: Int): DetailDepositResponse

    @POST("/pid/step")
    suspend fun pidValidate(@Body pidStepReq: PidStepReq): PidRes

    @GET("/cashback/customers")
    suspend fun getCashbackCustomer(): CashbackCustomerResponse

    @GET("/cashback/purchases/amount")
    suspend fun getPurchasesAmount(@Query("status") status: String): CashbackAmount

    @GET("/cashback/stores/customer")
    suspend fun getCashbackStores(@Query("quantityItems") quantityItens: Int): ResponseList<CashbackStore>

    @GET("/cashback/customers/terms")
    suspend fun getCashbackTermsAccept(): Response<Void>

    @POST("/cashback/customers/terms")
    suspend fun acceptCashbackTerms(): Response<Void>

    @GET("/cashback/terms")
    suspend fun getCashbackTerms(): CashbackTerms

    @GET("/cashback/details/{type}")
    suspend fun getCashbackDetails(@Path("type") type: String): CashbackDetailsResponse

    @GET("/cashback/stores/{storeId}/offers")
    suspend fun getStoreOffers(@Path("storeId") storeId: Long): ResponseList<CashbackOfferResponse>

    @GET("/cashback/stores/{id}/commission")
    suspend fun getComissionCategory(@Path("id") id: Long): ResponseList<CashbackComissionCategory>

    @GET("/cashback/purchases")
    suspend fun getCashbackPurchases(
        @Query("status") status: String,
        @Query("page") page: Int
    ): ResponseList<CashbackPurchase>

    @POST("/cashback/purchases/effecting")
    suspend fun sendCashbackAccount(
        @Query("receiptForm") receiptForm: String,
        @Body purchases: SendCashbackRequest
    ): Response<Void>

    @GET("/store/orders")
    suspend fun getStoreOrders(@Query("page") page: Int): ResponseList<StoreOrderResponse>

    @POST("/card/block/prevention/{cardId}")
    suspend fun postCardBlock(
        @Path("cardId") id: Long,
        @Header("X-Pid-Hash") hash: String
    ): Response<Void>

    @POST("/card/unblock/prevention/{cardId}")
    suspend fun postCardUnblock(
        @Path("cardId") id: Long,
        @Header("X-Pid-Hash") hash: String
    ): Response<Void>

    @GET("withdraw/available-limits")
    suspend fun getWithdrawStatus(): CustomerLimitsResponse

    @GET("/card/tracking")
    suspend fun getTrackingLog(
        @Query("cardId") cardId: Long?
    ): TrackingLogResponse

    @GET("/bill/validate/barcode/{barcode}")
    suspend fun getBillBarcodeInformation(@Path("barcode") barcode: String): BillBarcode

    @POST("/bill/payment/authorize")
    suspend fun paymentAuthorize(
        @Header("X-Pid-Hash") hash: String,
        @Body request: PaymentBarcodeRequest
    ): ConfirmPayResponse

    @POST("/card/password")
    suspend fun showCardPassword(@Body cardPasswordReq: CardPasswordReq): CardPasswordRes

    @GET("/customer")
    suspend fun getRegistrationList(): ChangeRegistrationData

    @PATCH("/customer")
    suspend fun changeRegistration(@Body changeRegistrationItemList: ChangeRegistrationRequest): Response<ResponseBody>

    @PUT("/customer/address")
    suspend fun updateAddress(@Body addressRegistration: AddressRegistrationRequest): Response<ResponseBody>

    @GET("customer/address/zipcode/{cep}")
    suspend fun getZipCode(@Path("cep") cep: String): ZipCodeRes

    @POST("/card/activation")
    suspend fun postCardReceivedActivation(
        @Body activationRequest: CardReceivedActivationRequest
    ): Response<Void>

    @POST("withdraw")
    suspend fun doWithdraw(
        @Body withDrawRequest: WithdrawReq
    ): Response<ResponseBody>

    @POST("/card/theft-lost/{productName}")
    suspend fun postTheftlost(
        @Path("productName") productName: String,
        @Body lostStolenRequest: LostStolenRequest
    ): LostStolenResponse

    @PUT("/auth/logout")
    suspend fun logout(@Body refreshReq: RefreshReq): Response<ResponseBody>

    @GET("customer/profile")
    suspend fun getFranchise(): FranchiseResponse
}