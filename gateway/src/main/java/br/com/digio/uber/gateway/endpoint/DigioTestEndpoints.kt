package br.com.digio.uber.gateway.endpoint

import br.com.digio.uber.model.digioTests.ProductsList
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET

interface DigioTestEndpoints {

    @GET("sandbox/products")
    suspend fun getProducts(): ProductsList

    @GET("169dec9a-f2e7-4f89-82e3-6f7c4e7286cf")
    suspend fun getValues(): Response<ResponseBody>
}