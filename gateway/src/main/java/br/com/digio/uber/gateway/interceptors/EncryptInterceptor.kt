package br.com.digio.uber.gateway.interceptors

import br.com.digio.uber.gateway.BuildConfig
import br.com.digio.uber.util.GsonUtil.gson
import br.com.digio.uber.util.encrypt.decrypt
import br.com.digio.uber.util.encrypt.encrypt
import br.com.digio.uber.util.fromJsonOrNull
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import okio.Buffer
import timber.log.Timber
import java.nio.charset.Charset

class EncryptInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val newBody = convertRequestBody(request)

        val newRequest = request.newBuilder()
            .addHeader("x-api-key", BuildConfig.API_KEY)
            .method(request.method, newBody)
            .build()

        val response = chain.proceed(newRequest)

        return response.newBuilder()
            .body(
                response.body?.let { responseBody ->
                    convertResponseEncrypt(responseBody)
                }
            )
            .build()
    }

    private fun convertRequestBody(request: Request): RequestBody? =
        request.body?.let { body ->
            val buffer = Buffer()
            body.writeTo(buffer)
            val valueJson = buffer.readString(UTF_8)
            val payload = JsonObject().apply {
                val payloadEncrypted = valueJson.encrypt()
                if (BuildConfig.SHOW_HTTP_LOGGIN_ENCRYPTED) {
                    Timber.d("OkHttp: --> $payloadEncrypted")
                }
                addProperty(PAYLOAD_KEY, payloadEncrypted)
            }
            gson.toJson(payload).toRequestBody(MEDIA_TYPE)
        }

    private fun convertResponseEncrypt(value: ResponseBody): ResponseBody {
        val jsonElement: JsonElement? = value.charStream().fromJsonOrNull()
        return if (jsonElement?.isJsonObject == true && jsonElement.asJsonObject.has(PAYLOAD_KEY)) {
            val payload = jsonElement.asJsonObject.get(PAYLOAD_KEY).asString
            if (BuildConfig.SHOW_HTTP_LOGGIN_ENCRYPTED) {
                Timber.d("OkHttp: <-- $payload")
            }
            payload.decrypt().toResponseBody(value.contentType())
        } else {
            Timber.e(jsonElement.toString())
            Timber.e("Response is encrypt but not have payload element")
            value
        }
    }

    companion object {
        private val MEDIA_TYPE = "application/json; charset=UTF-8".toMediaType()
        private val UTF_8 = Charset.forName("UTF-8")
        private const val PAYLOAD_KEY = "payload"
    }
}