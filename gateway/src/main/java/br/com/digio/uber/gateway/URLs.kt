package br.com.digio.uber.gateway

enum class URLs constructor(val url: String, val certificate: Int?) {
    URL_TEST_DIGIO("https://run.mocky.io/v3/", null),
    URL_UBER_DEV("https://dev-api-mobile.digio.com.br", R.raw.certificado_api),
    URL_UBER_HML("https://hml-api-mobile.digio.com.br", R.raw.certificado_api),
    URL_UBER_PROD("https://api-mobile.digio.com.br", R.raw.certificado_api)
}