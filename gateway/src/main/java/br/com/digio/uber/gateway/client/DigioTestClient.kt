package br.com.digio.uber.gateway.client

import android.content.Context
import br.com.digio.uber.gateway.GatewayApiBuilder
import br.com.digio.uber.gateway.URLs
import br.com.digio.uber.gateway.endpoint.DigioTestEndpoints

class DigioTestClient constructor(
    context: Context
) : GatewayApiBuilder(context) {

    override fun getUrl(): URLs = URLs.URL_TEST_DIGIO

    fun getDigioTestEndpoint(): DigioTestEndpoints =
        getBaseRetrofit().create(DigioTestEndpoints::class.java)
}