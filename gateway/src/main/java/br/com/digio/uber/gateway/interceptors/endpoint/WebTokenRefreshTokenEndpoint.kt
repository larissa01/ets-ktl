package br.com.digio.uber.gateway.interceptors.endpoint

import br.com.digio.uber.gateway.extension.getUrlByBuildType
import br.com.digio.uber.model.login.LoginRes
import br.com.digio.uber.model.login.refresh.req.RefreshReq
import br.com.digio.uber.util.GsonUtil
import br.com.digio.uber.util.TokenPreferencesUtils
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.HttpException
import retrofit2.Response
import java.io.Reader
import java.net.HttpURLConnection

object WebTokenRefreshTokenEndpoint {

    private const val AUTH_ENDPOINT_REFRESH_TOKEN = "auth/refresh-token"
    private const val MEDIA_TYPE_JSON = "application/json; charset=utf-8"

    fun Interceptor.Chain.makeNewRequest(tokenPreferencesUtils: TokenPreferencesUtils): LoginRes {
        val newRequestRefreshToken = request()
            .newBuilder()
            .url(makeUrl())
            .post(makeBody(tokenPreferencesUtils))
            .build()

        val responseRefreshToken = proceed(newRequestRefreshToken)

        if (!responseRefreshToken.isSuccessful) {
            throw returnErrorFromBackend(responseRefreshToken)
        }

        return GsonUtil
            .gson
            .fromJson(
                getResponseAsStream(responseRefreshToken),
                LoginRes::class.java
            )
    }

    private fun returnErrorFromBackend(responseRefreshToken: okhttp3.Response): HttpException =
        HttpException(
            responseRefreshToken.body?.let { bodyLet ->
                Response.error<Any>(
                    responseRefreshToken.code,
                    bodyLet
                )
            } ?: makeResponseToLogoff()
        )

    private fun getResponseAsStream(responseRefreshToken: okhttp3.Response): Reader? =
        responseRefreshToken
            .body
            ?.charStream() ?: throw makeHttpExceptionLogoff()

    private fun makeUrl(): HttpUrl =
        HttpUrl.Builder()
            .host(getUrlByBuildType().url.replace("https://", ""))
            .scheme("https")
            .addPathSegments(AUTH_ENDPOINT_REFRESH_TOKEN)
            .build()

    private fun makeBody(tokenPreferencesUtils: TokenPreferencesUtils): RequestBody =
        GsonUtil.gson.toJson(
            RefreshReq(
                refreshToken = tokenPreferencesUtils.getTokenRefresh()
            )
        ).toRequestBody(jsonContentType())

    private fun TokenPreferencesUtils.getTokenRefresh(): String =
        getRefreshToken() ?: throw makeHttpExceptionLogoff()

    private fun makeHttpExceptionLogoff() =
        HttpException(
            makeResponseToLogoff()
        )

    private fun makeResponseToLogoff(): Response<Any> = Response.error<Any>(
        HttpURLConnection.HTTP_UNAUTHORIZED,
        String().toResponseBody(jsonContentType())
    )

    fun jsonContentType(): MediaType? =
        MEDIA_TYPE_JSON.toMediaTypeOrNull()
}