package br.com.digio.uber.gateway.endpoint

import br.com.digio.uber.model.DTOSample
import retrofit2.http.POST

interface MockServerEndpoints {
    @POST("/login")
    suspend fun simpleTest(): DTOSample
}