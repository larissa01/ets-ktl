package br.com.digio.uber.gateway.client

import android.content.Context
import br.com.digio.uber.gateway.GatewayApiBuilder
import br.com.digio.uber.gateway.URLs
import br.com.digio.uber.gateway.endpoint.UberEndpoints
import br.com.digio.uber.gateway.extension.getUrlByBuildType
import br.com.digio.uber.gateway.interceptors.WebTokenInterceptor

class UberClient constructor(
    context: Context,
    private val webTokenInterceptor: WebTokenInterceptor
) : GatewayApiBuilder(context) {

    override fun getUrl(): URLs =
        getUrlByBuildType()

    fun getUberNotLoggedInEndpoint(): UberEndpoints =
        getBaseRetrofit().create(UberEndpoints::class.java)

    fun getUberLoggedEndpoint(): UberEndpoints =
        getBaseRetrofit(
            customConfigBuilder = {
                addInterceptor(webTokenInterceptor)
            }
        ).create(UberEndpoints::class.java)

    override fun isEncrypt(): Boolean = true
}