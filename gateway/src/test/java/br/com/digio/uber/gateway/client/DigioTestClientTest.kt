package br.com.digio.uber.gateway.client

import android.content.Context
import io.mockk.mockk
import junit.framework.TestCase.assertNotNull
import org.junit.Before
import org.junit.Test

class DigioTestClientTest {

    private val context: Context = mockk()

    lateinit var digioTestClient: DigioTestClient

    @Before
    fun setUp() {
        digioTestClient = DigioTestClient(context)
    }

    @Test
    fun `on execute DigioTestClient and return client DigioTestEndpoints`() {
        val digioTestEndpoints = digioTestClient.getDigioTestEndpoint()
        assertNotNull(digioTestEndpoints)
    }
}