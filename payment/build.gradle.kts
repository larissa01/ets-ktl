/* Apply Module base configurations */
plugins {
    id("com.android.dynamic-feature")
    kotlin("android")
    kotlin("kapt")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.dynamic.library")
    id("kotlin-android")
}

dependencies {
    kapt(Dependencies.PERMISSIONS_PROCESSOR)

    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.ANDROIDXCORE)
    implementation(Dependencies.ACTIVITYKTX)
    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.FRAGMENTKTX)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.KOIN)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)
    implementation(Dependencies.NAV_FRAGMENT)
    implementation(Dependencies.NAV_UI)

    implementation(Dependencies.ROOM)
    implementation(Dependencies.ROOM_KAPT)
    implementation(Dependencies.ROOM_KTX)
    implementation(Dependencies.RETROFIT)
    implementation(Dependencies.CANARINHO)
    implementation(Dependencies.PERMISSIONS_DISPATCHER)
    implementation(Dependencies.ZXING_ANDROID_EMBEDDED)
    implementation(Dependencies.ZXING_ANDROID_EMBEDDED_OLD)
    implementation(Dependencies.MATERIAL_DATE_TIME_PICKER)

    implementation(project(":app"))
    implementation(project(":common"))
    implementation(project(":util"))
    implementation(project(":analytics"))
    implementation(project(":model"))
    implementation(project(":repository"))
    implementation(project(":domain"))
    implementation(project(":receipt"))
}