package br.com.digio.uber.payment.di

import br.com.digio.uber.payment.mapper.BillBarcodeMapper
import br.com.digio.uber.payment.mapper.BillBarcodeMapperImpl
import br.com.digio.uber.payment.ui.DatePickerMediator
import br.com.digio.uber.payment.ui.DatePickerMediatorImpl
import br.com.digio.uber.payment.ui.fragment.OnboardingQrCodeViewModel
import br.com.digio.uber.payment.ui.fragment.PaymentAccountBarcodeReaderViewModel
import br.com.digio.uber.payment.ui.fragment.PaymentAccountBarcodeTypeViewModel
import br.com.digio.uber.payment.ui.fragment.PaymentAccountMethodViewModel
import br.com.digio.uber.payment.ui.fragment.PaymentAccountViewModel
import br.com.digio.uber.payment.ui.fragment.PaymentReviewViewModel
import br.com.digio.uber.payment.ui.fragment.ReviewQrCodeViewModel
import br.com.digio.uber.receipt.mapper.ComputedValuesMapper
import br.com.digio.uber.receipt.mapper.ComputedValuesMapperImpl
import br.com.digio.uber.receipt.mapper.InfoNPCMapper
import br.com.digio.uber.receipt.mapper.InfoNPCMapperImpl
import br.com.digio.uber.receipt.mapper.InfoNormalMapper
import br.com.digio.uber.receipt.mapper.InfoNormalMapperImpl
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val paymentModule = module {
    viewModel { PaymentAccountViewModel() }
    viewModel { PaymentAccountMethodViewModel(get(), get()) }
    single<InfoNPCMapper> { InfoNPCMapperImpl(get()) }
    single<InfoNormalMapper> { InfoNormalMapperImpl() }
    single<BillBarcodeMapper> { BillBarcodeMapperImpl(get(), get()) }
    viewModel { PaymentAccountBarcodeReaderViewModel(get(), get()) }
    viewModel { PaymentAccountBarcodeTypeViewModel(get(), get(), get()) }
    viewModel { OnboardingQrCodeViewModel() }
    viewModel { ReviewQrCodeViewModel(get(), get(), get()) }

    single<ComputedValuesMapper> { ComputedValuesMapperImpl() }
    factory<DatePickerMediator> { DatePickerMediatorImpl(androidContext()) }
    viewModel { PaymentReviewViewModel(get(), get(), get(), get()) }
}