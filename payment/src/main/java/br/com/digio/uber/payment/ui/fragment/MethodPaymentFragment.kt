package br.com.digio.uber.payment.ui.fragment

import android.app.Activity
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.view.doOnLayout
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.payment.BR
import br.com.digio.uber.payment.R
import br.com.digio.uber.payment.databinding.RedesignFragmentPaymentAccountMethodBinding
import br.com.digio.uber.payment.navigation.PaymentAccountOpKey
import br.com.digio.uber.payment.ui.activity.PaymentAccountActivity
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.applyMask
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.regex.Pattern

class MethodPaymentFragment :
    BNWFViewModelFragment<RedesignFragmentPaymentAccountMethodBinding, PaymentAccountMethodViewModel>() {

    var pasteDataFinal: String? = null
    private val analytics: Analytics by inject()
    override fun getToolbar(): Toolbar? = binding?.methodPaymentAppbar?.paymentToolbar
    override val showDisplayShowTitle: Boolean = true
    override val showHomeAsUp: Boolean = true
    override fun tag(): String = PaymentAccountOpKey.PAYMENT_METHOD.value
    override fun getStatusBarAppearance(): StatusBarColor = PaymentAccountOpKey.PAYMENT_METHOD.theme
    override val viewModel: PaymentAccountMethodViewModel by viewModel()
    override val bindingVariable: Int = BR.viewModel
    override val getLayoutId: Int = R.layout.redesign_fragment_payment_account_method
    override fun getRequestCode(): MutableList<Int> = mutableListOf(REQUEST_CODE_QR_CODE)

    companion object : FragmentNewInstance<MethodPaymentFragment>(::MethodPaymentFragment) {
        const val REQUEST_CODE_QR_CODE = 201
    }

    override fun initialize() {
        super.initialize()
        viewModel.initializer(::onMenuItemClick)
    }

    override fun onResume() {
        super.onResume()

        view?.doOnLayout { // Used doOnLayout as a workaround for android 10 limitations
            checkIfHasClipboardBarCode()
        }
    }

    private fun checkIfHasClipboardBarCode() {
        val pasteData: StringBuffer

        if (activity?.getSystemService(Context.CLIPBOARD_SERVICE) is ClipboardManager) {
            val clipboard = activity?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            if (clipboard.hasPrimaryClip()) {
                val item = clipboard.primaryClip?.getItemAt(0)

                pasteData = StringBuffer(item?.text.toString())

                pasteDataFinal = Pattern.compile("[^0-9]").matcher(pasteData).replaceAll("")

                pasteDataFinal?.let {
                    if (it.length == Const.Utils.BARCODE_TAX_REVENUE_LENGTH && it.startsWith("8")) {
                        viewModel.barCodeClipboard.value = it.applyMask(Const.MaskPattern.BARCODE_ARRECADACAO)
                    } else if (it.length == Const.Utils.BARCODE_CIP_LENGTH) {
                        viewModel.barCodeClipboard.value = it.applyMask(Const.MaskPattern.BARCODE_CIP)
                    }
                }
            }
        }
    }

    private fun onMenuItemClick(v: View) {
        viewModel.barCodeClipboard.value = null

        when (v.id) {
            R.id.ll_account_payment_option_qr -> {
                callGoTo(PaymentAccountOpKey.QR_CODE_ONBOARDING.value, arguments)
            }
            R.id.ll_payment_method_pagarfatura -> {
                viewModel.pixItem.value?.let {
                    navigation.navigationToPixMenuPayment(requireContext(), it.cpf, it.incomeIndex)
                }
            }
            R.id.cl_payment_method_pagarcontas -> {
                analytics.pushSimpleEvent(Tags.PAYMENT_ACCOUNT_PAGUE_PAGAR_CONTA_CLIQUE)
                callGoTo(PaymentAccountOpKey.PAYMENT_BARCODE_READER.value, arguments)
            }
            R.id.linear_barcode_clipboard -> {
                callGoTo(
                    PaymentAccountOpKey.PAYMENT_BARCODE_TYPE.value,
                    Bundle().apply { this.putString(PaymentAccountActivity.BARCODE_VALUE, pasteDataFinal) }
                )
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_QR_CODE && resultCode == Activity.RESULT_OK) {
            arguments?.putString(PaymentAccountActivity.QRCODE_VALUE, data?.getStringExtra("SCAN_RESULT"))
            callGoTo(PaymentAccountOpKey.PAY_RESUME.value, arguments)
        }
    }
}