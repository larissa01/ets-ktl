package br.com.digio.uber.payment.navigation

import br.com.digio.uber.common.kenum.StatusBarColor

enum class PaymentAccountOpKey constructor(val value: String, val theme: StatusBarColor) {
    PAYMENT_METHOD("PAYMENT_METHOD", StatusBarColor.BLACK),
    PAYMENT_RECEIPTS("PAYMENT_RECEIPTS", StatusBarColor.BLACK),
    PAYMENT_BARCODE_READER("PAYMENT_BARCODE_READER", StatusBarColor.BLACK),
    PAYMENT_BARCODE_TYPE("PAYMENT_BARCODE_TYPE", StatusBarColor.BLACK),
    PAYMENT_DETAIL("PAYMENT_DETAIL", StatusBarColor.WHITE),
    PAYMENT_HOW_PAY("HOW_PAY", StatusBarColor.BLACK),
    PAYMENT_CONFIRM_PAY_ACCOUNT("CONFIRM_PAY_ACCOUNT", StatusBarColor.BLACK),
    PAY_CHOICE("MAIN_PAY_CHOICE", StatusBarColor.BLACK),
    PAY_RESUME("PAY_RESUME", StatusBarColor.BLACK),
    PAY_SUCCESS("PAY_SUCCESS", StatusBarColor.BLACK),
    PAY_ERROR("PAY_ERROR", StatusBarColor.BLACK),
    QR_CODE_ONBOARDING("QR_CODE_ONBOARDING", StatusBarColor.BLACK),
    QR_CODE_REVIEW("QR_CODE_REVIEW", StatusBarColor.BLACK),
}