package br.com.digio.uber.payment.ui.activity

import android.os.Bundle
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.navigation.GenericNavigation
import br.com.digio.uber.payment.BR
import br.com.digio.uber.payment.databinding.RedesignPaymentAccountActivityBinding
import br.com.digio.uber.payment.di.paymentModule
import br.com.digio.uber.payment.navigation.PaymentAccountNavigation
import br.com.digio.uber.payment.ui.fragment.PaymentAccountViewModel
import br.com.digio.uber.util.Const.RequestOnResult.Payment.FORCE_QRCODE
import org.koin.android.ext.android.inject
import org.koin.core.module.Module

class PaymentAccountActivity : BaseViewModelActivity<RedesignPaymentAccountActivityBinding, PaymentAccountViewModel>() {

    var paymentNavigation: PaymentAccountNavigation = PaymentAccountNavigation(this)

    override fun getLayoutId(): Int = LayoutIds.navigationLayoutActivity

    override fun getModule(): List<Module> = listOf(paymentModule)

    override val bindingVariable: Int = BR.paymentAccountViewModel

    override val viewModel: PaymentAccountViewModel by inject()

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        if (intent.extras?.getBoolean(FORCE_QRCODE, false) == true) {
            paymentNavigation.init(intent.extras, false)
        } else {
            paymentNavigation.init(intent.extras, true)
        }
    }

    override fun getNavigation(): GenericNavigation? = paymentNavigation

    companion object {
        const val QRCODE_VALUE = "qrcode_value"
        const val BARCODE_VALUE = "barcode_value"
    }
}