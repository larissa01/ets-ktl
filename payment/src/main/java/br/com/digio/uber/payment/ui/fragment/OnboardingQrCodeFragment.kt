package br.com.digio.uber.payment.ui.fragment

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import androidx.annotation.NonNull
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.payment.BR
import br.com.digio.uber.payment.R
import br.com.digio.uber.payment.databinding.QrCodeOnboardingFragmentBinding
import br.com.digio.uber.payment.navigation.PaymentAccountOpKey
import br.com.digio.uber.util.Const.Extras.RESULT_QR_CODE_KEY
import br.com.digio.uber.util.Const.Extras.RESULT_QR_CODE_VALUE
import br.com.digio.uber.util.Const.RequestOnResult.QRCode.QR_REQUEST_CODE
import org.koin.android.ext.android.inject

class OnboardingQrCodeFragment : BNWFViewModelFragment<QrCodeOnboardingFragmentBinding, OnboardingQrCodeViewModel>() {
    override val bindingVariable: Int? = BR.onboardingQrCodeViewModel
    override val getLayoutId: Int? = R.layout.qr_code_onboarding_fragment
    override val viewModel: OnboardingQrCodeViewModel by inject()

    override fun tag(): String = OnboardingQrCodeFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.BLACK

    override val showDisplayShowTitle: Boolean = true
    override val showHomeAsUp: Boolean = true
    override fun getToolbar(): Toolbar? =
        binding?.appBarMain?.toolbar?.apply {
            title = getString(R.string.qr_code_onboarding_fragment_title)
        }

    companion object : FragmentNewInstance<OnboardingQrCodeFragment>(::OnboardingQrCodeFragment) {
        const val REQUESTT_PERMISSION_CODE: Int = 5555
    }

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it) }
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.btnNext -> {
                requesttPermission()
            }
        }
    }

    override fun onNavigationClick(view: View) {
        activity?.finish()
    }

    private fun requesttPermission() {
        activity?.let { activityLet ->
            val notHavePermission = checkNotHavePermission(activityLet)
            if (notHavePermission) {
                requestPermissions(
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    REQUESTT_PERMISSION_CODE
                )
                return
            }
            navigation.navigationToQrCodeReaderActivity(
                activityLet,
                getString(R.string.message_qrcode_read),
                QR_REQUEST_CODE
            )
        }
    }

    private fun checkNotHavePermission(it: FragmentActivity): Boolean =
        ContextCompat.checkSelfPermission(
            it,
            Manifest.permission.CAMERA
        ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            it,
            Manifest.permission.READ_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            it,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        ) != PackageManager.PERMISSION_GRANTED

    override fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUESTT_PERMISSION_CODE && grantResults.isNotEmpty()) {
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                activity?.let { activityLet ->
                    navigation.navigationToQrCodeReaderActivity(
                        activityLet,
                        getString(R.string.message_qrcode_read),
                        QR_REQUEST_CODE
                    )
                }
            } else {
                showMessage(
                    ErrorObject().apply {
                        message = R.string.qrcod_permission_message
                    },
                    true
                )
            }
        }
    }

    override fun getRequestCode(): MutableList<Int> = mutableListOf(QR_REQUEST_CODE)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == QR_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_CANCELED) {
                activity?.finish()
            } else if (resultCode == Activity.RESULT_OK) {
                val b = arguments ?: Bundle()
                b.putString(RESULT_QR_CODE_KEY, data?.getStringExtra(RESULT_QR_CODE_VALUE))
                callGoTo(PaymentAccountOpKey.QR_CODE_REVIEW.value, b)
            }
        }
    }
}