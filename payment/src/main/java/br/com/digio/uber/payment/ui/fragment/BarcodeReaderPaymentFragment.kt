package br.com.digio.uber.payment.ui.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.view.View
import br.com.concrete.canarinho.formatador.Formatador
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.payment.BR
import br.com.digio.uber.payment.R
import br.com.digio.uber.payment.databinding.RedesignFragmentPaymentAccountBarcodereaderBinding
import br.com.digio.uber.payment.navigation.PaymentAccountOpKey
import br.com.digio.uber.util.Const
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import kotlinx.android.synthetic.main.redesign_fragment_payment_account_barcodereader.barcodeview_transfer_code
import org.koin.android.viewmodel.ext.android.viewModel
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.PermissionUtils
import timber.log.Timber

class BarcodeReaderPaymentFragment :
    BNWFViewModelFragment<
        RedesignFragmentPaymentAccountBarcodereaderBinding,
        PaymentAccountBarcodeReaderViewModel>() {

    companion object : FragmentNewInstance<BarcodeReaderPaymentFragment>(::BarcodeReaderPaymentFragment)

    override fun tag(): String =
        PaymentAccountOpKey.PAYMENT_BARCODE_READER.value

    override fun getStatusBarAppearance(): StatusBarColor =
        PaymentAccountOpKey.PAYMENT_BARCODE_READER.theme

    override val viewModel: PaymentAccountBarcodeReaderViewModel by viewModel()

    override val bindingVariable: Int = BR.paymentAccountBarcodeReaderViewModel

    override val getLayoutId: Int = R.layout.redesign_fragment_payment_account_barcodereader

    var showingAlert = false

    override fun onCreate(savedInstanceState: Bundle?) {
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        super.onCreate(savedInstanceState)
    }

    override fun initialize() {
        super.initialize()

        viewModel.initializer {
            onMenuItemClick(it)
        }

        viewModel.barCodePresentationModel.observe(
            viewLifecycleOwner,
            { billBarcodeUiModel ->
                if (billBarcodeUiModel.isScheduled) {
                    viewModel.showAlreadyScheduledWarning {
                        callGoTo(
                            PaymentAccountOpKey.PAYMENT_DETAIL.value,
                            Bundle().apply { this.putParcelable("barCodePresentation", billBarcodeUiModel) }
                        )
                    }
                } else {
                    callGoTo(
                        PaymentAccountOpKey.PAYMENT_DETAIL.value,
                        Bundle().apply { this.putParcelable("barCodePresentation", billBarcodeUiModel) }
                    )
                }
            }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        barcodeview_transfer_code.viewFinder.visibility = View.INVISIBLE
        barcodeview_transfer_code.setStatusText("")
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onDestroyView() {
        super.onDestroyView()
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onStart() {
        super.onStart()

        barcodeview_transfer_code.decodeContinuous(
            object : BarcodeCallback {
                override fun barcodeResult(result: BarcodeResult?) {
                    val loading = viewModel.showLoading.value ?: false
                    if (!loading && !showingAlert) {
                        if (Formatador.LINHA_DIGITAVEL.podeSerFormatado(result?.text!!)) {
                            val desformatado = Formatador.LINHA_DIGITAVEL.formata(result.text!!)
                            viewModel.checkBarCode(desformatado)
                        }
                    }
                }

                override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
                    Timber.d(resultPoints.toString())
                }
            }
        )

        Handler().postDelayed(
            {
                resumeBarcodeWithPermissionCheck()
            },
            Const.Utils.DELAY_MILLISECOND
        )
    }

    private fun resumeBarcodeWithPermissionCheck() {
        if (PermissionUtils.hasSelfPermissions(activity, Manifest.permission.CAMERA)) {
            resumeBarcode()
        } else {
            activity?.let {
                requestPermissions(
                    arrayOf(Manifest.permission.CAMERA),
                    Const.RequestOnResult.Payment.REQUEST_RESUMEBARCODE
                )
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            Const.RequestOnResult.Payment.REQUEST_RESUMEBARCODE -> {
                for (result: Int in grantResults) {
                    if (!PermissionUtils.verifyPermissions(result)) {
                        return
                    }
                }

                resumeBarcode()
            }
        }
    }

    @NeedsPermission(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun resumeBarcode() {
        barcodeview_transfer_code.resume()
    }

    private fun onMenuItemClick(v: View) {
        when (v.id) {
            R.id.iv_payment_method_close -> callGoBack()
            else -> {
                callGoTo(PaymentAccountOpKey.PAYMENT_BARCODE_TYPE.value, arguments)
            }
        }
    }
}