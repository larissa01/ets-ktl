package br.com.digio.uber.payment.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName
import br.com.digio.uber.payment.ui.fragment.BarcodeReaderPaymentFragment
import br.com.digio.uber.payment.ui.fragment.BarcodeTypePaymentFragment
import br.com.digio.uber.payment.ui.fragment.MethodPaymentFragment
import br.com.digio.uber.payment.ui.fragment.OnboardingQrCodeFragment
import br.com.digio.uber.payment.ui.fragment.PaymentReviewFragment
import br.com.digio.uber.payment.ui.fragment.ReviewQrCodeFragment

class PaymentAccountNavigation(
    private val activityScope: AppCompatActivity
) : GenericNavigationWithFlow(activityScope) {

    private var bundle: Bundle? = null

    fun init(bundle: Bundle?, home: Boolean) {
        this.bundle = bundle
        if (home) {
            goTo(GoTo(PaymentAccountOpKey.PAYMENT_METHOD.value, bundle))
        } else {
            goTo(GoTo(PaymentAccountOpKey.QR_CODE_ONBOARDING.value, bundle))
        }
    }

    override fun getMaxNavigationItems(): Int = PaymentAccountOpKey.values().size

    private fun methodPaymentFragment(): GenericNavigationWIthFlowGetFragmentByName = {
        MethodPaymentFragment.newInstance(this, it)
    }

    private fun barcodeReaderPaymentFragment(): GenericNavigationWIthFlowGetFragmentByName = {
        BarcodeReaderPaymentFragment.newInstance(this, it)
    }

    private fun onboardingQrCodeFragment(): GenericNavigationWIthFlowGetFragmentByName = {
        OnboardingQrCodeFragment.newInstance(this, it)
    }

    private fun reviewQrCodeFragment(): GenericNavigationWIthFlowGetFragmentByName = {
        ReviewQrCodeFragment.newInstance(this, it)
    }

    private fun barcodeTypePaymentFragment(): GenericNavigationWIthFlowGetFragmentByName = {
        activityScope.supportFragmentManager.popBackStack()
        BarcodeTypePaymentFragment.newInstance(this, it)
    }

    fun detailPaymentFragment(): GenericNavigationWIthFlowGetFragmentByName = {
        activityScope.supportFragmentManager.popBackStack()
        PaymentReviewFragment.newInstance(this, it)
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            PaymentAccountOpKey.PAYMENT_METHOD.value to methodPaymentFragment(),
            PaymentAccountOpKey.PAYMENT_BARCODE_READER.value to barcodeReaderPaymentFragment(),
            PaymentAccountOpKey.PAYMENT_BARCODE_TYPE.value to barcodeTypePaymentFragment(),
            PaymentAccountOpKey.PAYMENT_DETAIL.value to detailPaymentFragment(),
            PaymentAccountOpKey.QR_CODE_ONBOARDING.value to onboardingQrCodeFragment(),
            PaymentAccountOpKey.QR_CODE_REVIEW.value to reviewQrCodeFragment()
        )
}