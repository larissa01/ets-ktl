package br.com.digio.uber.payment.uimodel

data class PixItemUiModel(val cpf: String, val incomeIndex: String)