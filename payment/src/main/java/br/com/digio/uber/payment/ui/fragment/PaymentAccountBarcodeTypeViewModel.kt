package br.com.digio.uber.payment.ui.fragment

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.generics.makeChooseMessageObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.payment.CheckBillBarcodeUseCase
import br.com.digio.uber.payment.R
import br.com.digio.uber.payment.mapper.BillBarcodeMapper
import br.com.digio.uber.payment.uimodel.BillBarcodePresentationModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.unmask
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.net.ssl.HttpsURLConnection

class PaymentAccountBarcodeTypeViewModel(
    private val checkBarCodeUseCase: CheckBillBarcodeUseCase,
    private val barcodeMapper: BillBarcodeMapper,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    val barCodePresentationModel: MutableLiveData<BillBarcodePresentationModel> = MutableLiveData()
    var customErrorMessage = MutableLiveData<ErrorObject>()
    val barcodeText = MutableLiveData<String>()
    val updateCursorTo = MutableLiveData<Int>()
    val enableButton = MutableLiveData<Boolean>()
    var barcodeMask: String = String()

    fun init(onClickItem: OnClickItem) {
        super.initializer(onClickItem)
    }

    fun checkBarCode(barCode: String) {
        launch(Dispatchers.Main) {
            checkBarCodeUseCase(barCode).singleExec(
                onSuccessBaseViewModel = {
                    barCodePresentationModel.value = barcodeMapper.map(it)
                },
                onError = { statusCode, error, _ ->
                    error.apply {
                        icon = R.drawable.ic_response_warning
                        val httpStatusCode = statusCode ?: HttpsURLConnection.HTTP_INTERNAL_ERROR

                        if (httpStatusCode >= HttpsURLConnection.HTTP_INTERNAL_ERROR) {
                            title = R.string.payment_barcode_error_message
                        } else {
                            titleString = this.messageString
                        }

                        messageString = null
                        hideMessage = true
                    }
                }
            )
        }
    }

    fun showAlreadySchedulesWarning(callback: () -> Unit) {
        message.value = makeChooseMessageObject {
            icon = R.drawable.ic_payment_alert
            title = R.string.already_scheduled_dialog_title
            message = R.string.already_scheduled_dialog_message
            positiveOptionTextInt = R.string.already_scheduled_dialog_positive_button
            negativeOptionTextInt = R.string.already_scheduled_dialog_negative_button
            onPositiveButtonClick = {
                callback.invoke()
            }
        }
    }

    fun verifyTypeAndApplyMask() {
        val unmasked = barcodeText.value?.unmask()

        if (barcodeText.value.isNullOrEmpty()) {
            enableButton.value = false
            return
        }

        barcodeMask = getBarcodeType(unmasked)

        val maskedBarcodeText = barcodeText.value?.applyMask(barcodeMask)
        updateCursorTo.value = barcodeText.value?.length ?: 0

        if (maskedBarcodeText != barcodeText.value) {
            barcodeText.value = maskedBarcodeText
        }
    }

    private fun getBarcodeType(unmasked: String?) =
        if (barcodeText.value?.startsWith(Const.Utils.TAX_REVENUE_BARCODE_INITIAL_DIGIT) == true) {
            enableButton.value = (unmasked?.length ?: -1) >= Const.Utils.BARCODE_TAX_REVENUE_LENGTH
            resourceManager.getString(R.string.barcode_type_arrecadacao)
        } else {
            enableButton.value = (unmasked?.length ?: -1) >= Const.Utils.BARCODE_CIP_LENGTH
            resourceManager.getString(R.string.barcode_type_cip)
        }
}