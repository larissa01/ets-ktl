package br.com.digio.uber.payment.mapper

import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.model.payment.QrCodeResponse
import br.com.digio.uber.payment.R
import br.com.digio.uber.payment.uimodel.QrCodeUiModel
import br.com.digio.uber.util.FORMAT_US_DATE
import br.com.digio.uber.util.getDateBR
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.util.toCalendar
import br.com.digio.uber.util.toMoney

@Suppress("MagicNumber")
class QrCodeMapper(
    private val resourceManager: ResourceManager,
) : AbstractMapper<QrCodeResponse, QrCodeUiModel> {
    override fun map(param: QrCodeResponse): QrCodeUiModel {
        return QrCodeUiModel(
            uuid = param.uuid,
            merchantName = param.merchantName,
            amount = param.amount.toMoney(true),
            installments = "${param.installments}x",
            date = param.date.toCalendar(FORMAT_US_DATE)?.getDateBR() ?: "",
            time = param.time.substring(0, 5),
            mode = formatMode(param.mode),
            type = formatType(param.type),
            successType = formatSuccessType(param.type),
            errorType = formatErrorType(param.type),
            cardBlocked = param.cardBlocked
        )
    }

    private fun formatSuccessType(type: String): String {
        return when (type) {
            Type.SELL.name -> resourceManager.getString(R.string.format_success_type_payment_made)
            Type.CANCELLATION.name -> resourceManager.getString(R.string.format_success_type_cancel_lation_made)
            else -> ""
        }
    }

    private fun formatErrorType(type: String): String {
        return when (type) {
            Type.SELL.name -> resourceManager.getString(R.string.format_error_Type_payment_made)
            Type.CANCELLATION.name -> resourceManager.getString(R.string.format_error_Type_cancel_lation_made)
            else -> ""
        }
    }

    private fun formatType(type: String): String {
        return when (type) {
            Type.SELL.name -> resourceManager.getString(R.string.format_Type_payment_made)
            Type.CANCELLATION.name -> resourceManager.getString(R.string.format_Type_cancel_lation_made)
            else -> ""
        }
    }

    private fun formatMode(mode: String): String {
        return when (mode) {
            Mode.CREDIT.name -> resourceManager.getString(R.string.format_mode_payment_made)
            Mode.DEBIT.name -> resourceManager.getString(R.string.format_mode_cancel_lation_made)
            else -> ""
        }
    }

    enum class Mode {
        CREDIT,
        DEBIT
    }

    enum class Type {
        SELL,
        CANCELLATION
    }
}