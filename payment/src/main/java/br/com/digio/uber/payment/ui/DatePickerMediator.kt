package br.com.digio.uber.payment.ui

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import java.util.Calendar

interface DatePickerMediator {
    val selectedDate: MutableLiveData<Calendar>
    fun setNumberOfPermittedDays(numberOfDays: Long)
    fun show(calendar: Calendar, fragmentManager: FragmentManager)
}