package br.com.digio.uber.payment.mapper

import br.com.digio.uber.model.payment.BillBarcode
import br.com.digio.uber.payment.uimodel.BillBarcodePresentationModel
import br.com.digio.uber.receipt.mapper.InfoNPCMapper
import br.com.digio.uber.receipt.mapper.InfoNormalMapper
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.getDateBR
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.util.toCalendarOrNull
import br.com.digio.uber.util.toCurrencyValue

interface BillBarcodeMapper : AbstractMapper<BillBarcode, BillBarcodePresentationModel>

class BillBarcodeMapperImpl(
    private val infoNormalMapper: InfoNormalMapper,
    private val infoNPCMapper: InfoNPCMapper
) : BillBarcodeMapper {
    override fun map(param: BillBarcode): BillBarcodePresentationModel {
        val infoNPC = infoNPCMapper.map(param.infoNPC)
        val infoNormal = infoNormalMapper.map(param.infoNormal)
        val calculatedInterestAmount = infoNPC?.computedValues?.calculatedInterestAmount
        val calculatedFineValue = infoNPC?.computedValues?.calculatedFineValue
        val totalCharges = calculatedInterestAmount?.plus(calculatedFineValue ?: 0.0)
        val payeeDocument = param.infoNPC?.payeeDocument
        val payerDocument = param.infoNPC?.payerDocument

        return BillBarcodePresentationModel(
            uuid = param.uuid,
            amount = param.amount,
            amountString = param.amount.toCurrencyValue(true),
            amountWithCurrency = infoNPC?.computedValues?.totalAmountToCharge?.toCurrencyValue(true)
                ?: param.amount.toCurrencyValue(true),
            amountWithoutCurrency = infoNPC?.computedValues?.totalAmountToCharge?.toCurrencyValue(false)
                ?: param.amount.toCurrencyValue(false),
            description = param.description,
            barcode = getBarcode(param.barcode),
            dueDate = param.dueDate,
            dueDateFormatted = param.dueDate.toCalendarOrNull()?.getDateBR() ?: String(),
            type = param.type,
            infoNormal = infoNormalMapper.map(param.infoNormal),
            infoNPC = infoNPC,
            availableTransactionModes = param.availableTransactionModes,
            discount = param.infoNPC?.computedValues?.discountValueCalculated?.toCurrencyValue(true) ?: String(),
            interest = calculatedInterestAmount?.toCurrencyValue(true) ?: String(),
            fine = calculatedFineValue?.toCurrencyValue(true) ?: String(),
            totalCharges = totalCharges?.toCurrencyValue(true) ?: String(),
            payeeName = infoNPC?.payeeName ?: infoNormal?.payeeName ?: String(),
            payeeDocument = getPayeeDocument(payeeDocument),
            payerName = infoNPC?.payerName ?: String(),
            payerDocument = getPayerDocument(payerDocument),
            dateToSchedule = param.dateToSchedule,
            isScheduled = param.scheduledDate != null
        )
    }

    private fun getBarcode(barcode: String) =
        if (barcode.startsWith(Const.Utils.TAX_REVENUE_BARCODE_INITIAL_DIGIT)) {
            barcode.applyMask(Const.MaskPattern.BARCODE_ARRECADACAO)
        } else {
            barcode.applyMask(Const.MaskPattern.BARCODE_CIP)
        }

    private fun getPayeeDocument(payeeDocument: String?) =
        if (payeeDocument?.length == Const.MaskPattern.CPF_LENGTH) {
            payeeDocument.applyMask(Const.MaskPattern.CPF)
        } else {
            payeeDocument?.applyMask(Const.MaskPattern.CNPJ)
        } ?: ""

    private fun getPayerDocument(payerDocument: String?) =
        if (payerDocument?.length == Const.MaskPattern.CPF_LENGTH) {
            payerDocument.applyMask(Const.MaskPattern.CPF)
        } else {
            payerDocument?.applyMask(Const.MaskPattern.CNPJ)
        } ?: ""
}