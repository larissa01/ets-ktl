package br.com.digio.uber.payment.ui.fragment

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.makeChooseMessageObject
import br.com.digio.uber.domain.usecase.payment.CheckBillBarcodeUseCase
import br.com.digio.uber.payment.R
import br.com.digio.uber.payment.mapper.BillBarcodeMapper
import br.com.digio.uber.payment.uimodel.BillBarcodePresentationModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.net.ssl.HttpsURLConnection

class PaymentAccountBarcodeReaderViewModel(
    private val checkBarCodeUseCase: CheckBillBarcodeUseCase,
    private val barcodeMapper: BillBarcodeMapper
) : BaseViewModel() {

    val barCodePresentationModel: MutableLiveData<BillBarcodePresentationModel> = MutableLiveData()
    private var canRead: Boolean = true

    fun checkBarCode(barCode: String) {
        if (!canRead) return

        canRead = false

        launch(Dispatchers.Main) {
            checkBarCodeUseCase(barCode).singleExec(
                onSuccessBaseViewModel = {
                    barCodePresentationModel.value = barcodeMapper.map(it)
                },
                onError = { statusCode, error, _ ->
                    error.apply {
                        icon = R.drawable.ic_response_warning
                        val httpStatusCode = statusCode ?: HttpsURLConnection.HTTP_INTERNAL_ERROR

                        if (httpStatusCode >= HttpsURLConnection.HTTP_INTERNAL_ERROR) {
                            title = R.string.payment_barcode_error_message
                        } else {
                            titleString = this.messageString
                        }
                        onCloseDialog = {
                            canRead = true
                        }
                        messageString = ""
                        hideMessage = true
                    }
                },
            )
        }
    }

    fun showAlreadyScheduledWarning(callback: () -> Unit) {
        message.value = makeChooseMessageObject {
            icon = R.drawable.ic_payment_alert
            title = R.string.already_scheduled_dialog_title
            message = R.string.already_scheduled_dialog_message
            positiveOptionTextInt = R.string.already_scheduled_dialog_positive_button
            negativeOptionTextInt = R.string.already_scheduled_dialog_negative_button
            onPositiveButtonClick = {
                callback.invoke()
                canRead = true
            }
            onCloseDialog = {
                canRead = true
            }
        }
    }
}