package br.com.digio.uber.payment.ui.fragment

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.payment.PostPayWithQrCodeUseCase
import br.com.digio.uber.domain.usecase.payment.PostQrcodeReadUseCase
import br.com.digio.uber.model.payment.PayWithQrCodeRequest
import br.com.digio.uber.payment.R
import br.com.digio.uber.payment.mapper.QrCodeMapper
import br.com.digio.uber.payment.uimodel.QrCodeUiModel

class ReviewQrCodeViewModel(
    val postQrcodeReadUseCase: PostQrcodeReadUseCase,
    val postPayWithQrCodeUseCase: PostPayWithQrCodeUseCase,
    private val resourceManager: ResourceManager,
) : BaseViewModel() {
    val qrCodeUiModel = MutableLiveData<QrCodeUiModel>()
    var paySuccess = MutableLiveData<Boolean>().apply { value = false }
    val hashPid = MutableLiveData<String>()
    val erroLoadData = MutableLiveData<Boolean>().apply { value = false }

    fun initializer(onClickItem: OnClickItem, qrCodeValue: String) {
        super.initializer(onClickItem)
        postQrcodeReadUseCase(qrCodeValue).singleExec(
            onSuccessBaseViewModel = {
                qrCodeUiModel.value = QrCodeMapper(resourceManager).map(it)
            },
            onError = { _, error, _ ->
                error.apply {
                    close = true
                    onPositiveButtonClick = {
                        erroLoadData.value = true
                    }
                }
            },
            showLoadingFlag = true
        )
    }

    fun payWithQrCode() {
        hashPid.value?.let { hashPid ->
            qrCodeUiModel.value?.let { qrCodeUiModel ->
                postPayWithQrCodeUseCase(Pair(PayWithQrCodeRequest(qrCodeUiModel.uuid, hashPid), hashPid)).singleExec(
                    onError = { _, error, _ ->
                        ErrorObject().apply {
                            title = R.string.erro_pay_title
                            message = R.string.erro_pay_subtitle
                            close = false
                        }
                    },
                    onSuccessBaseViewModel = {
                        if (it.isSuccessful) {
                            paySuccess.value = true
                            message.value = ErrorObject().apply {
                                title = R.string.sucess_pay_title
                                message = R.string.sucess_pay_subTitle
                                close = false
                            }
                        } else {
                            message.value = ErrorObject().apply {
                                title = R.string.erro_pay_title
                                message = R.string.erro_pay_subtitle
                                close = false
                            }
                        }
                    }
                )
            }
        } ?: run { showErrorAccessDenied() }
    }

    fun showErrorAccessDenied() {
        message.value = ErrorObject().apply {
            title = br.com.digio.uber.common.R.string.access_denied_title
            message = br.com.digio.uber.common.R.string.access_denied_title
        }
    }
}