package br.com.digio.uber.payment.ui.fragment

import android.content.Intent
import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.payment.BR
import br.com.digio.uber.payment.R
import br.com.digio.uber.payment.databinding.ReviewFragmentBinding
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.Extras.RESULT_QR_CODE_KEY
import org.koin.android.ext.android.inject

class ReviewQrCodeFragment : BNWFViewModelFragment<ReviewFragmentBinding, ReviewQrCodeViewModel>() {
    override val bindingVariable: Int? = BR.reviewQrCodeViewModel
    override val getLayoutId: Int? = R.layout.review_fragment
    override val viewModel: ReviewQrCodeViewModel by inject()

    override fun tag(): String = ReviewQrCodeFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE

    override val showDisplayShowTitle: Boolean = true
    override val showHomeAsUp: Boolean = true
    override fun getToolbar(): Toolbar? =
        binding?.appBarMain?.toolbar?.apply {
            title = getString(R.string.qr_code_review_fragment_title)
        }

    companion object : FragmentNewInstance<ReviewQrCodeFragment>(::ReviewQrCodeFragment)

    override fun initialize() {
        super.initialize()
        requireArguments().getString(RESULT_QR_CODE_KEY)?.let { qrCodeResult ->
            viewModel.initializer({ onItemClicked(it) }, qrCodeResult)
        }
        setupObserve()
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.btnPay -> {
                activity?.let {
                    navigation.navigationToPidActivity(
                        it,
                        PidType.QR_CODE,
                        Const.RequestOnResult.PID.PID_REQUEST_CODE
                    )
                }
            }
        }
    }

    override fun onNavigationClick(view: View) {
        activity?.onBackPressed()
    }

    override fun getRequestCode(): MutableList<Int> = mutableListOf(Const.RequestOnResult.PID.PID_REQUEST_CODE)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Const.RequestOnResult.PID.PID_REQUEST_CODE) {
            if (resultCode == Const.RequestOnResult.PID.RESULT_PID_SUCCESS &&
                data != null && data.hasExtra(Const.RequestOnResult.PID.KEY_PID_HASH)
            ) {
                viewModel.hashPid.value = data.extras?.getString(Const.RequestOnResult.PID.KEY_PID_HASH)!!
            } else viewModel.showErrorAccessDenied()
        } else viewModel.showErrorAccessDenied()
    }

    private fun setupObserve() {
        viewModel.hashPid.observe(
            this,
            { viewModel.payWithQrCode() }
        )

        viewModel.erroLoadData.observe(
            this,
            {
                if (it) activity?.onBackPressed()
            }
        )
    }
}