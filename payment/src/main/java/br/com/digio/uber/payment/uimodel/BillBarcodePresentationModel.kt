package br.com.digio.uber.payment.uimodel

import android.os.Parcelable
import br.com.digio.uber.receipt.model.InfoNPCPresentationModel
import br.com.digio.uber.receipt.model.InfoNormalPresentationModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BillBarcodePresentationModel constructor(
    val uuid: String,
    val type: String,
    var description: String? = null,
    var amount: Double,
    var amountString: String,
    val amountWithCurrency: String,
    val amountWithoutCurrency: String,
    val dueDate: String,
    val dueDateFormatted: String,
    val barcode: String,
    val infoNormal: InfoNormalPresentationModel? = null,
    val infoNPC: InfoNPCPresentationModel? = null,
    val discount: String,
    val availableTransactionModes: List<String>,
    val interest: String,
    val fine: String,
    val totalCharges: String,
    val payeeName: String,
    val payeeDocument: String,
    val payerName: String,
    val payerDocument: String,
    val dateToSchedule: String?,
    val isScheduled: Boolean
) : Parcelable {

    val isNPC: Boolean = infoNPC != null
}