package br.com.digio.uber.payment.ui.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.account.GetAccountUseCase
import br.com.digio.uber.domain.usecase.remuneration.GetRemunerationIncomeUseCase
import br.com.digio.uber.payment.uimodel.PixItemUiModel
import br.com.digio.uber.util.combineWith
import br.com.digio.uber.util.safeLet

class PaymentAccountMethodViewModel(
    getAccountUseCase: GetAccountUseCase,
    getRemunerationIncomeUseCase: GetRemunerationIncomeUseCase
) : BaseViewModel() {

    val barCodeClipboard: MutableLiveData<String> = MutableLiveData()
    val pixItem = MutableLiveData<PixItemUiModel>()

    val loaded: LiveData<Boolean> = MutableLiveData(false).combineWith(
        getAccountUseCase(Unit).exec(),
        getRemunerationIncomeUseCase(Unit).exec()
    ) { _, account, income ->
        safeLet(account?.cpf, income?.incomeIndex) { cpf, incomeIndex ->
            pixItem.value = PixItemUiModel(cpf, incomeIndex)
        }
        true
    }
}