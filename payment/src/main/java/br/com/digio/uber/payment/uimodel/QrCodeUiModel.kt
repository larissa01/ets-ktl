package br.com.digio.uber.payment.uimodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class QrCodeUiModel(
    val uuid: String,
    val merchantName: String,
    val amount: String,
    val installments: String,
    val date: String,
    val time: String,
    val mode: String,
    val type: String,
    val successType: String,
    val errorType: String,
    val cardBlocked: Boolean
) : Parcelable