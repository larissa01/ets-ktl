package br.com.digio.uber.payment.ui

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.payment.R
import br.com.digio.uber.util.Const
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import java.util.Calendar

class DatePickerMediatorImpl(context: Context) : DatePickerMediator, DatePickerDialog.OnDateSetListener {

    private val datePickerDialog = DatePickerDialog()
    private val defaultColor = ContextCompat.getColor(context, R.color.black)
    override val selectedDate = MutableLiveData<Calendar>()

    init {
        datePickerDialog.accentColor = defaultColor
        datePickerDialog.setOkColor(defaultColor)
        datePickerDialog.setCancelColor(defaultColor)
    }

    override fun setNumberOfPermittedDays(numberOfDays: Long) {
        val weekdays = ArrayList<Calendar>()
        val day = Calendar.getInstance()

        for (i in 0..numberOfDays) {
            if (day[Calendar.DAY_OF_WEEK] != Calendar.SATURDAY && day[Calendar.DAY_OF_WEEK] != Calendar.SUNDAY) {
                val dayToPermit = day.clone() as Calendar
                weekdays.add(dayToPermit)
            }
            day.add(Calendar.DATE, Const.Utils.ONE_DAY_UNIT)
        }

        val weekdayDays: Array<Calendar> = weekdays.toArray(arrayOfNulls<Calendar>(weekdays.size))
        datePickerDialog.selectableDays = weekdayDays
    }

    override fun show(calendar: Calendar, fragmentManager: FragmentManager) {
        datePickerDialog.initialize(
            this,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        ).also {
            datePickerDialog.show(fragmentManager, this::class.java.name)
        }
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        Calendar.getInstance().run {
            set(year, monthOfYear, dayOfMonth)
            selectedDate.value = this
        }
    }
}