package br.com.digio.uber.payment.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.extensions.hideKeyboard
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.payment.BR
import br.com.digio.uber.payment.R
import br.com.digio.uber.payment.databinding.RedesignFragmentPaymentAccountTypeBarcodeBinding
import br.com.digio.uber.payment.navigation.PaymentAccountOpKey
import br.com.digio.uber.payment.ui.activity.PaymentAccountActivity
import br.com.digio.uber.payment.uimodel.BillBarcodePresentationModel
import br.com.digio.uber.util.unmask
import kotlinx.android.synthetic.main.redesign_fragment_payment_account_type_barcode.et_fragment_payment_barcode_type
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class BarcodeTypePaymentFragment : BNWFViewModelFragment<
    RedesignFragmentPaymentAccountTypeBarcodeBinding,
    PaymentAccountBarcodeTypeViewModel>() {

    private val analytics: Analytics by inject()

    companion object : FragmentNewInstance<BarcodeTypePaymentFragment>(::BarcodeTypePaymentFragment)

    override fun tag(): String =
        PaymentAccountOpKey.PAYMENT_BARCODE_READER.value

    override fun getStatusBarAppearance(): StatusBarColor =
        PaymentAccountOpKey.PAYMENT_BARCODE_READER.theme

    override val viewModel: PaymentAccountBarcodeTypeViewModel by viewModel()

    override val bindingVariable: Int = BR.paymentAccountBarcodeTypeViewModel

    override val getLayoutId: Int = R.layout.redesign_fragment_payment_account_type_barcode

    override fun getToolbar(): Toolbar? = binding?.barcodeTypePaymentAppbar?.paymentToolbar

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true

    override fun initialize() {
        super.initialize()

        setupObservers()

        viewModel.init(
            onClickItem = {
                onMenuItemClick(it)
            }
        )

        et_fragment_payment_barcode_type.setText("")
        et_fragment_payment_barcode_type.maxWidth = et_fragment_payment_barcode_type.width

        et_fragment_payment_barcode_type.requestFocus()

        arguments?.let { safeArguments ->
            val barCode = safeArguments.getString(PaymentAccountActivity.BARCODE_VALUE)
            barCode?.apply {
                et_fragment_payment_barcode_type.setText(this)
                viewModel.checkBarCode(this)
            }
        }
    }

    private fun setupObservers() {
        viewModel.updateCursorTo.observe(viewLifecycleOwner) {
            binding?.etFragmentPaymentBarcodeType?.setSelection(it)
        }

        viewModel.enableButton.observe(viewLifecycleOwner) {
            binding?.btnFragmentPaymentBarcodeType?.isEnabled = it
        }

        viewModel.barCodePresentationModel.observe(
            viewLifecycleOwner,
            { billBarcodeUiModel ->
                if (billBarcodeUiModel.isScheduled) {
                    viewModel.showAlreadySchedulesWarning {
                        goToPaymentReview(billBarcodeUiModel)
                    }
                } else {
                    goToPaymentReview(billBarcodeUiModel)
                }
            }
        )

        viewModel.customErrorMessage.observe(
            this,
            {
                analytics.pushSimpleEvent(Tags.PAYMENT_ACCOUNT_PAG_CONTAS_COD_BARRAS_ERRO)
            }
        )
    }

    private fun onMenuItemClick(v: View) {
        when (v.id) {
            R.id.iv_payment_method_close -> callGoBack()
            else -> {
                analytics.pushSimpleEvent(Tags.PAYMENT_ACCOUNT_PAG_CONTAS_COD_BARRAS_SEGUIR)

                binding?.etFragmentPaymentBarcodeType?.text?.let {
                    activity?.hideKeyboard()
                    viewModel.checkBarCode(it.toString().unmask())
                }
            }
        }
    }

    private fun goToPaymentReview(billBarCodeUiModel: BillBarcodePresentationModel) {
        callGoTo(
            PaymentAccountOpKey.PAYMENT_DETAIL.value,
            Bundle().apply { this.putParcelable("barCodePresentation", billBarCodeUiModel) }
        )
    }
}