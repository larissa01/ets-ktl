package br.com.digio.uber.payment.ui.fragment

import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.ReceiptOrigin
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.payment.BR
import br.com.digio.uber.payment.R
import br.com.digio.uber.payment.databinding.FragmentPaymentReviewBinding
import br.com.digio.uber.payment.navigation.PaymentAccountOpKey
import br.com.digio.uber.payment.uimodel.BillBarcodePresentationModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.DateFormatter
import br.com.digio.uber.util.getDateBR
import org.koin.android.ext.android.inject
import java.util.Date

class PaymentReviewFragment : BNWFViewModelFragment<FragmentPaymentReviewBinding, PaymentReviewViewModel>() {

    companion object : FragmentNewInstance<PaymentReviewFragment>(::PaymentReviewFragment)

    override fun tag(): String = PaymentAccountOpKey.PAYMENT_DETAIL.value

    override fun getStatusBarAppearance(): StatusBarColor = PaymentAccountOpKey.PAYMENT_DETAIL.theme

    override val viewModel: PaymentReviewViewModel by inject()

    override val bindingVariable: Int = BR.paymentReviewViewModel

    override val getLayoutId: Int = R.layout.fragment_payment_review

    private val analytics: Analytics by inject()

    override fun getToolbar(): Toolbar? = binding?.paymentReviewAppbar?.paymentToolbar

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding?.tvPaymentDate?.paintFlags?.let {
            binding?.tvPaymentDate?.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        }

        super.onViewCreated(view, savedInstanceState)
    }

    override fun getRequestCode(): MutableList<Int> = mutableListOf(Const.RequestOnResult.PID.PID_REQUEST_CODE)

    override fun initialize() {
        super.initialize()

        viewModel.confirmScheduling.observe(viewLifecycleOwner) {
            if (it) {
                continueToPID()
            }
        }

        viewModel.paymentDate.observe(viewLifecycleOwner) {
            viewModel.paymentDateFormatted.value = viewModel.paymentDate.value?.getDateBR()

            viewModel.isScheduling.value =
                DateFormatter.dateBRFormatter.format(Date()) != viewModel.paymentDate.value?.getDateBR()

            viewModel.isScheduling.value?.let { isScheduling ->
                if (isScheduling) {
                    viewModel.buttonLabel.value = getString(R.string.lbl_continue_scheduling)
                    viewModel.paymentDateLabel.value = getString(R.string.scheduling_label)
                } else {
                    viewModel.buttonLabel.value = getString(R.string.lbl_confirm)
                    viewModel.paymentDateLabel.value = getString(R.string.payment_date_label)
                }

                viewModel.showBalanceTooltip.value = !viewModel.hasEnoughBalance && !isScheduling
            }
        }

        val barCode = arguments?.getParcelable<BillBarcodePresentationModel>("barCodePresentation")

        barCode?.let {
            viewModel.init(
                onClickItem = {
                    onMenuItemClick(it)
                },
                barCodePresentationModel = barCode
            )
        }

        viewModel.receiptId.observe(
            viewLifecycleOwner,
            {
                analytics.pushSimpleEvent(Tags.PAYMENT_ACCOUNT_PAG_CONTAS_COM_CONTA_SUCESSO)

                Bundle().apply {
                    putString(Const.Extras.RECEIPT_ID, it)
                    putBoolean(Const.Extras.PAYMENT_RECEIPT, true)
                    putSerializable(Const.Extras.RECEIPT_ORIGIN, ReceiptOrigin.PAYMENT)
                    putBoolean(Const.Extras.PAYMENT_RESCHEDULED, viewModel.wasRescheduled)
                }.also {
                    navigation.navigationToDynamic(requireActivity(), Const.Activities.RECEIPT_ACTIVITY, it)
                }

                activity?.finish()
            }
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (isPidResultSuccess(requestCode, resultCode)) {
            var value: Double = viewModel.barCodePresentationModel.value?.amount ?: 0.0
            if (viewModel.barCodePresentationModel.value?.infoNPC != null) {
                viewModel.barCodePresentationModel.value?.infoNPC?.computedValues?.totalAmountToCharge?.let {
                    value = it
                }
            }

            viewModel.paymentAuthorize(
                data?.getStringExtra(Const.RequestOnResult.PID.KEY_PID_HASH),
                viewModel.barCodePresentationModel.value?.uuid,
                viewModel.descriptionText.value ?: getString(R.string.payment_default_description),
                value
            )
        }
    }

    private fun isPidResultSuccess(requestCode: Int, resultCode: Int) =
        requestCode == Const.RequestOnResult.PID.PID_REQUEST_CODE &&
            resultCode == Const.RequestOnResult.PID.RESULT_PID_SUCCESS

    private fun onMenuItemClick(v: View) {
        when (v) {
            binding?.tvPaymentDate -> {
                viewModel.openDatePicker(parentFragmentManager)
            }
            else -> {
                if (viewModel.isScheduling.value == true) {
                    viewModel.showBalanceWarning()
                } else {
                    continueToPID()
                }
            }
        }
    }

    private fun continueToPID() {
        analytics.pushSimpleEvent(Tags.PAYMENT_ACCOUNT_PAG_CONTAS_DETALHE_CONTINUAR)

        viewModel.barCodePresentationModel.value?.let {
            viewModel.barCodePresentationModel.value?.barcode?.let {
                navigation.navigationToPidActivity(
                    requireActivity(),
                    PidType.TRANSFER,
                    Const.RequestOnResult.PID.PID_REQUEST_CODE
                )
            }
        }
    }
}