package br.com.digio.uber.payment.ui.fragment

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.makeMessageSuccessObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.balance.GetBalanceUseCase
import br.com.digio.uber.domain.usecase.payment.PaymentAuthorizeUseCase
import br.com.digio.uber.model.payment.PaymentBarcodeRequest
import br.com.digio.uber.payment.R
import br.com.digio.uber.payment.ui.DatePickerMediator
import br.com.digio.uber.payment.uimodel.BillBarcodePresentationModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.FORMAT_US_DATE
import br.com.digio.uber.util.daysToMillis
import br.com.digio.uber.util.getDate
import br.com.digio.uber.util.millisToDays
import br.com.digio.uber.util.safeLet
import br.com.digio.uber.util.toCalendar
import br.com.digio.uber.util.toCalendarOrNull
import br.com.digio.uber.util.toDate
import br.com.digio.uber.util.toPattern
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.Calendar
import java.util.Date
import javax.net.ssl.HttpsURLConnection

class PaymentReviewViewModel(
    private val paymentUseCase: PaymentAuthorizeUseCase,
    private val datePickerMediator: DatePickerMediator,
    private val getBalanceUseCase: GetBalanceUseCase,
    resourceManager: ResourceManager
) : BaseViewModel() {

    var receiptId = MutableLiveData<String>()
    val barCodePresentationModel: MutableLiveData<BillBarcodePresentationModel> = MutableLiveData()
    val descriptionText = MutableLiveData<String>()
    val paymentDate = MediatorLiveData<Calendar>()
    val confirmScheduling = MutableLiveData<Boolean>()
    val isScheduling = MutableLiveData<Boolean>().apply { value = false }
    val buttonLabel = MutableLiveData<String>().apply { value = resourceManager.getString(R.string.lbl_confirm) }
    val paymentDateLabel = MutableLiveData<String>().apply {
        value = resourceManager.getString(R.string.payment_date_label)
    }
    val paymentDateFormatted = MutableLiveData<String>()
    var wasRescheduled: Boolean = false
    var hasEnoughBalance = true

    val showBalanceTooltip = MutableLiveData<Boolean>()

    fun init(onClickItem: OnClickItem, barCodePresentationModel: BillBarcodePresentationModel) {
        super.initializer(onClickItem)

        datePickerMediator.selectedDate.value =
            barCodePresentationModel.dateToSchedule?.toCalendarOrNull() ?: Calendar.getInstance()

        paymentDate.addSource(datePickerMediator.selectedDate) { selectDate ->
            paymentDate.value = selectDate
        }

        this.barCodePresentationModel.value = barCodePresentationModel

        launch {
            getBalanceUseCase(Unit).singleExec(
                onSuccessBaseViewModel = {
                    hasEnoughBalance = it.balance > barCodePresentationModel.amount.toBigDecimal()
                    showBalanceTooltip.value = !hasEnoughBalance && (isScheduling.value ?: false)
                }
            )
        }
    }

    fun paymentAuthorize(pidHash: String?, transactionIdentifier: String?, description: String?, amount: Double?) {
        safeLet(
            pidHash,
            transactionIdentifier,
            description,
            amount
        ) { pidHash, transactionIdentifier, description, amount ->
            launch {
                showLoading.value = true
                val request = PaymentBarcodeRequest(
                    transactionIdentifier,
                    description,
                    BigDecimal.valueOf(amount),
                    description,
                    "ACCOUNT",
                    if (isScheduling.value == true) paymentDate.value?.getDate() else null
                )

                paymentUseCase(Pair(pidHash, request)).singleExec(
                    onSuccessBaseViewModel = {
                        wasRescheduled = it.schedulingInfo?.wasRescheduled ?: false
                        receiptId.value =
                            it.schedulingInfo?.receiptId ?: "${Const.Utils.RECEIPT_SERVICE_CODE}${it.uuid}"
                    },
                    onError = { statusCode, error, _ ->
                        error.apply {
                            icon = R.drawable.ic_response_warning
                            val httpStatusCode = statusCode ?: HttpsURLConnection.HTTP_INTERNAL_ERROR

                            if (httpStatusCode >= HttpsURLConnection.HTTP_INTERNAL_ERROR) {
                                title = R.string.payment_barcode_error_message
                            } else {
                                titleString = this.messageString
                            }
                            messageString = null
                            hideMessage = true
                        }
                    }
                )
            }
        }
    }

    fun openDatePicker(fragmentManager: FragmentManager) {
        val nowMillis: Long = Calendar.getInstance().toPattern(FORMAT_US_DATE).toDate(FORMAT_US_DATE).time
        val dueDateMillis: Long = getScheduleLimitDate()
        val numberOfDaysToLimit = kotlin.math.abs(dueDateMillis - nowMillis).millisToDays()

        with(datePickerMediator) {
            setNumberOfPermittedDays(numberOfDaysToLimit)
            show(paymentDate.value ?: Calendar.getInstance(), fragmentManager)
        }
    }

    private fun getScheduleLimitDate(): Long {
        if (isTaxRevenueBill()) {
            return barCodePresentationModel.value?.dueDate?.toCalendar(FORMAT_US_DATE)?.timeInMillis
                ?: (Date().time + Const.Utils.PAYMENT_TAX_REVENUE_SCHEDULING_LIMIT_DAYS.toLong().daysToMillis())
        }
        return Date().time + Const.Utils.PAYMENT_TAX_REVENUE_SCHEDULING_LIMIT_DAYS.toLong().daysToMillis()
    }

    private fun isTaxRevenueBill() =
        barCodePresentationModel.value?.barcode?.startsWith(Const.Utils.TAX_REVENUE_BARCODE_INITIAL_DIGIT) == false

    fun showBalanceWarning() {
        message.value = makeMessageSuccessObject {
            icon = R.drawable.ic_payment_alert
            title = R.string.balance_dialog_title
            message = R.string.balance_dialog_message
            buttonText = R.string.balance_dialog_button
            onPositiveButtonClick = {
                confirmScheduling.value = true
            }
        }
    }
}