plugins {
    id("com.android.library")
    kotlin("android")
    id("kotlin-android-extensions")
    id("com.google.gms.google-services")
    id("br.com.digio.uber.plugin.android.library")
}

dependencies {
    implementation(Dependencies.KOIN)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINEXT)
    implementation(platform(Dependencies.FIREBASE_BOM))
    implementation(Dependencies.FIREBASE_MESSAGING)
    implementation(Dependencies.FIREBASE_ANALYTICS)
    implementation(Dependencies.GSON)

    implementation(Dependencies.TIMBER)

    implementation(project(":util"))
    implementation(project(":model"))
}