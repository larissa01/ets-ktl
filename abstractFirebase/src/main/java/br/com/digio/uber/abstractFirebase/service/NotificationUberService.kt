package br.com.digio.uber.abstractFirebase.service

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import br.com.digio.uber.abstractFirebase.R
import br.com.digio.uber.model.notification.NotificationRes
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.GsonUtil
import br.com.digio.uber.util.loadClassOrNull
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.JsonParseException
import com.google.gson.JsonSyntaxException
import timber.log.Timber

class NotificationUberService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        remoteMessage
            .data
            .convertToNotificationResOrNull()
            ?.let { notificationLet ->
                sendNotification(notificationLet)
            }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Timber.e("Token change but not have backend integration: $token")
    }

    private fun sendNotification(notificationRes: NotificationRes) {
        loadClassOrNull<Activity>(Const.Activities.SPLASH_ACTIVITY)?.also {
            val intent = Intent(this, it)

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val pendingIntent: PendingIntent = PendingIntent.getActivity(
                this,
                0 /* Request code */,
                intent,
                PendingIntent.FLAG_ONE_SHOT
            )

            val defaultSoundUri: Uri =
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder: NotificationCompat.Builder =
                NotificationCompat.Builder(this, Const.Notification.FCM_DEFAULT_CHANNEL)
                    .setSmallIcon(R.drawable.ic_uber_logo_notification)
                    .setContentTitle(notificationRes.title)
                    .setContentText(notificationRes.message)
                    .setAutoCancel(true)
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(notificationRes.message)
                            .setBigContentTitle(notificationRes.title)
                    )
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager

            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    Const.Notification.FCM_DEFAULT_CHANNEL,
                    getString(R.string.change_title),
                    NotificationManager.IMPORTANCE_DEFAULT
                )
                notificationManager?.createNotificationChannel(channel)
            }
            notificationManager?.notify(0 /* ID of notification */, notificationBuilder.build())
        }
    }

    private fun Map<String, String>.convertToNotificationResOrNull(): NotificationRes? =
        try {
            GsonUtil.gson.fromJson(GsonUtil.gson.toJsonTree(this), NotificationRes::class.java)
        } catch (ex: JsonParseException) {
            Timber.e(ex)
            null
        } catch (ex: JsonSyntaxException) {
            Timber.e(ex)
            null
        } catch (ex: IllegalStateException) {
            Timber.e(ex)
            null
        } catch (ex: NullPointerException) {
            Timber.e(ex)
            null
        } catch (ex: ClassCastException) {
            Timber.e(ex)
            null
        }
}