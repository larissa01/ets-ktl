package br.com.digio.uber.abstractFirebase.extension

import br.com.digio.uber.abstractFirebase.exception.FirebaseMessagingTokenCancelException
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.SendException
import timber.log.Timber
import java.lang.IllegalStateException
import kotlin.coroutines.suspendCoroutine

suspend fun getFirebaseMessagingToken(): String =
    suspendCoroutine { continuation ->
        FirebaseMessaging.getInstance().token
            .addOnSuccessListener {
                continuation.resumeWith(Result.success(it))
            }.addOnCanceledListener {
                continuation.resumeWith(Result.failure(FirebaseMessagingTokenCancelException()))
            }.addOnFailureListener {
                continuation.resumeWith(Result.failure(it))
            }
    }

@Suppress("TooGenericExceptionCaught")
suspend fun getFirebaseMessagingTokenOrNull(): String? =
    try {
        getFirebaseMessagingToken()
    } catch (ex: FirebaseMessagingTokenCancelException) {
        Timber.e(ex)
        null
    } catch (ex: SendException) {
        Timber.e(ex)
        null
    } catch (ex: IllegalStateException) {
        Timber.e(ex)
        null
    } catch (ex: Exception) {
        Timber.e(ex)
        null
    }