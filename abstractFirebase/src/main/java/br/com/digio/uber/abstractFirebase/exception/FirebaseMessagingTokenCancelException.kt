package br.com.digio.uber.abstractFirebase.exception

class FirebaseMessagingTokenCancelException : Exception()