package br.com.digio.uber.abstractFirebase.di

import br.com.digio.uber.abstractFirebase.FirebaseAnalyticsAbstract
import br.com.digio.uber.abstractFirebase.FirebaseAnalyticsAbstractImpl
import com.google.firebase.analytics.FirebaseAnalytics
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val firebaseAbstractModule = module {
    single { FirebaseAnalytics.getInstance(androidContext()) }
    single<FirebaseAnalyticsAbstract> { FirebaseAnalyticsAbstractImpl(get()) }
}