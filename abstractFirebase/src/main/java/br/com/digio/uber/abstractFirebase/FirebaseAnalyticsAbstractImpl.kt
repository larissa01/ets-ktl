package br.com.digio.uber.abstractFirebase

import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics

class FirebaseAnalyticsAbstractImpl constructor(
    private val firebaseAnalytics: FirebaseAnalytics
) : FirebaseAnalyticsAbstract {
    override fun logEvent(var1: String, var2: Bundle?) =
        firebaseAnalytics.logEvent(var1, var2)
}