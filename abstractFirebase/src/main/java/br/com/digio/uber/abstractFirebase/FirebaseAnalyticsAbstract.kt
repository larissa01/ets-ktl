package br.com.digio.uber.abstractFirebase

import android.os.Bundle
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.annotation.Size

interface FirebaseAnalyticsAbstract {
    fun logEvent(
        @NonNull @Size(min = 1L, max = 40L)
        var1: String,
        @Nullable var2: Bundle? = null
    )
}