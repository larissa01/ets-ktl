package br.com.digio.uber.domain.usecase.cashback

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.cashback.CashbackCustomerResponse
import br.com.digio.uber.repository.cashback.CashbackRepository

class GetCashbackCustomerUseCase(
    private val cashbackRepository: CashbackRepository
) : AbstractUseCase<Unit, CashbackCustomerResponse>() {
    override suspend fun execute(param: Unit) =
        cashbackRepository.getCashbackCustomer()
}