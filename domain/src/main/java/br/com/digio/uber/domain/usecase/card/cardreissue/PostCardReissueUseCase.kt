package br.com.digio.uber.domain.usecase.card.cardreissue

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.cardreissue.LostStolenResponse
import br.com.digio.uber.repository.card.CardReissueRepository

class PostCardReissueUseCase(
    private val cardReissueRepository: CardReissueRepository
) : AbstractUseCase<Pair<CardUseCaseTypeCardReissue, Long>, LostStolenResponse>() {
    override suspend fun execute(param: Pair<CardUseCaseTypeCardReissue, Long>) =
        when (param.first) {
            CardUseCaseTypeCardReissue.LOSS -> cardReissueRepository.postLoss(param.second)
            CardUseCaseTypeCardReissue.THEFT -> cardReissueRepository.postTheft(param.second)
        }
}

enum class CardUseCaseTypeCardReissue {
    LOSS,
    THEFT
}