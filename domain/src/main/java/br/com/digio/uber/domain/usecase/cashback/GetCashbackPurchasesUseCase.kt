package br.com.digio.uber.domain.usecase.cashback

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.cashback.CashbackPurchase
import br.com.digio.uber.repository.cashback.CashbackRepository

class GetCashbackPurchasesUseCase(
    private val cashbackRepository: CashbackRepository
) : AbstractUseCase<Pair<String, Int>, ResponseList<CashbackPurchase>>() {
    override suspend fun execute(param: Pair<String, Int>) =
        cashbackRepository.getCashbackPurchases(param.first, param.second)
}