package br.com.digio.uber.domain.usecase.franchise

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.franchise.FranchiseResponse
import br.com.digio.uber.repository.franchise.FranchiseRepository

class FranchiseUseCase(private val franchiseRepository: FranchiseRepository) :
    AbstractUseCase<Unit, FranchiseResponse>() {
    override suspend fun execute(param: Unit): FranchiseResponse {
        return franchiseRepository.getFranchise()
    }
}