package br.com.digio.uber.domain.usecase.virtualcard

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.card.VirtualCardResponse
import br.com.digio.uber.repository.virtualcard.VirtualCardRepository

class GetVirtualCardUseCase(
    private val virtualCardRepository: VirtualCardRepository
) : AbstractUseCase<Pair<Long, String>, VirtualCardResponse>() {
    override suspend fun execute(param: Pair<Long, String>): VirtualCardResponse =
        virtualCardRepository.getVirtualCard(param.first, param.second)
}