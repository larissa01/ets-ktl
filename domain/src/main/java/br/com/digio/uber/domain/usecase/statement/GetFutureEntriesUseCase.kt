package br.com.digio.uber.domain.usecase.statement

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.statement.FutureEntriesResponse
import br.com.digio.uber.repository.statement.StatementRepository

class GetFutureEntriesUseCase(
    private val statementRepository: StatementRepository
) : AbstractUseCase<Unit, FutureEntriesResponse>() {
    override suspend fun execute(param: Unit): FutureEntriesResponse =
        statementRepository.getFutureEntries()
}