package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.BeneficiariesResponse
import br.com.digio.uber.repository.transfer.TransferRepository

class GetFavoredUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<Unit, BeneficiariesResponse>() {

    override suspend fun execute(param: Unit): BeneficiariesResponse =
        repository.getTedBeneficiaries()
}

class GetP2PFavoredUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<Unit, BeneficiariesResponse>() {

    override suspend fun execute(param: Unit): BeneficiariesResponse =
        repository.getP2PBeneficiaries()
}