package br.com.digio.uber.domain.usecase.card.password

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.show.card.password.CardPasswordRes
import br.com.digio.uber.repository.account.AccountRepository

class GetCardPasswordUseCase constructor(
    private val accountRepository: AccountRepository
) : AbstractUseCase<String, CardPasswordRes>() {
    override suspend fun execute(param: String): CardPasswordRes =
        accountRepository.getCardPassword(param)
}