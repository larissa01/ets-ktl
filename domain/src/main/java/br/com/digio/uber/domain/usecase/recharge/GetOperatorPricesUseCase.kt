package br.com.digio.uber.domain.usecase.recharge

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.recharge.RechargeValues
import br.com.digio.uber.repository.recharge.RechargeRepository

class GetOperatorPricesUseCase(
    private val rechargeRepository: RechargeRepository
) : AbstractUseCase<String, RechargeValues>() {
    override suspend fun execute(param: String) =
        rechargeRepository.getOperatorPrices(param)
}