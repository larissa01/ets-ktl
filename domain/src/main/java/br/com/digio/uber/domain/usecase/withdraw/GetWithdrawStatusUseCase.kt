package br.com.digio.uber.domain.usecase.withdraw

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.bankSlip.CustomerLimitsResponse
import br.com.digio.uber.repository.withdraw.WithdrawRepository

class GetWithdrawStatusUseCase(
    private val withdrawRepository: WithdrawRepository
) : AbstractUseCase<Unit, CustomerLimitsResponse>() {
    override suspend fun execute(param: Unit): CustomerLimitsResponse =
        withdrawRepository.getWithdrawStatus()
}