package br.com.digio.uber.domain.usecase.balance

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.BalanceResponse
import br.com.digio.uber.repository.balance.BalanceRepository
import br.com.digio.uber.repository.cache.CoCache
import br.com.digio.uber.repository.cache.HawkCache
import br.com.digio.uber.util.Const

class GetBalanceUseCase(
    private val balanceRepository: BalanceRepository
) : AbstractUseCase<Unit, BalanceResponse>(CoCache(HawkCache()), Const.Cache.KEY_BALANCE_CACHE) {
    override suspend fun execute(param: Unit): BalanceResponse =
        balanceRepository.getBalance()
}