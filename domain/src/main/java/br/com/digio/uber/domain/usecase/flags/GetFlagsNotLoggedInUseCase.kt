package br.com.digio.uber.domain.usecase.flags

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.UberFlagsPersistNotLoggedIn
import br.com.digio.uber.repository.login.AccountNotLoggedInRepository

class GetFlagsNotLoggedInUseCase constructor(
    private val accountNotLoggedInRepository: AccountNotLoggedInRepository
) : AbstractUseCase<Unit, UberFlagsPersistNotLoggedIn>() {
    override suspend fun execute(param: Unit): UberFlagsPersistNotLoggedIn =
        accountNotLoggedInRepository.getUberFlagsPersistNotLoggedIn() ?: UberFlagsPersistNotLoggedIn()
}