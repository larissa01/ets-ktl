package br.com.digio.uber.domain.usecase.payment

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.payment.BillBarcode
import br.com.digio.uber.repository.payment.AccountPaymentRepository

class CheckBillBarcodeUseCase(
    private val accountPaymentRepository: AccountPaymentRepository
) : AbstractUseCase<String, BillBarcode>() {
    override suspend fun execute(param: String): BillBarcode =
        accountPaymentRepository.checkBillBarcode(param)
}