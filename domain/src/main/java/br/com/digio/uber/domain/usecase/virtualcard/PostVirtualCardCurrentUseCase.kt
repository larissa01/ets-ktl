package br.com.digio.uber.domain.usecase.virtualcard

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.virtualcard.VirtualCardRepository

class PostVirtualCardCurrentUseCase(
    private val virtualCardRepository: VirtualCardRepository
) : AbstractUseCase<Long, Boolean>() {
    override suspend fun execute(param: Long): Boolean =
        virtualCardRepository.postVirtualCardCurrent(param)
}