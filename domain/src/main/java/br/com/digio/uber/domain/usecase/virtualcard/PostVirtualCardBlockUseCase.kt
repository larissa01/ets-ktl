package br.com.digio.uber.domain.usecase.virtualcard

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.virtualcard.VirtualCardRepository
import retrofit2.Response

class PostVirtualCardBlockUseCase(
    private val virtualCardRepository: VirtualCardRepository
) : AbstractUseCase<Pair<VirtualCardUseCaseTypeBlock, Long>, Response<Void>>() {
    override suspend fun execute(param: Pair<VirtualCardUseCaseTypeBlock, Long>) =
        when (param.first) {
            VirtualCardUseCaseTypeBlock.BLOCK -> virtualCardRepository.postVirtualCardBlock(param.second)
            VirtualCardUseCaseTypeBlock.UNLOCK -> virtualCardRepository.postVirtualCardUnblock(param.second)
        }
}

enum class VirtualCardUseCaseTypeBlock {
    BLOCK,
    UNLOCK
}