package br.com.digio.uber.domain.usecase.faq

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.faq.FaqRepository

class UpdateFaqUseCase(val repository: FaqRepository) : AbstractUseCase<Unit, Unit>() {
    public override suspend fun execute(param: Unit) {
        repository.updateFaq()
    }
}