package br.com.digio.uber.domain.usecase.login

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.account.AccountRepository
import br.com.digio.uber.repository.cache.util.clearAllHawkCache
import br.com.digio.uber.util.TokenPreferencesUtils

class LogoutUseCase constructor(
    private val accountRepository: AccountRepository,
    private val tokenPreferencesUtils: TokenPreferencesUtils
) : AbstractUseCase<Unit, Unit>() {
    override suspend fun execute(param: Unit) {
        accountRepository.logout()
        accountRepository.nukedCredentialAllTables()
        tokenPreferencesUtils.clearToken()
        clearAllHawkCache()
    }
}