package br.com.digio.uber.domain.usecase.cardReceived

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.cardReceived.CardReceivedActivationRequest
import br.com.digio.uber.repository.cardReceived.CardReceivedRepository
import retrofit2.Response

class CardReceivedActivationUseCase(private val cardReceivedRepository: CardReceivedRepository) :
    AbstractUseCase<CardReceivedActivationRequest, Response<Void>>() {
    override suspend fun execute(param: CardReceivedActivationRequest): Response<Void> =
        cardReceivedRepository.postCardReceivedActivation(param)
}