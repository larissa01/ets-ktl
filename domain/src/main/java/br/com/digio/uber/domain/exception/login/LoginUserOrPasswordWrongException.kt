package br.com.digio.uber.domain.exception.login

class LoginUserOrPasswordWrongException : Exception()