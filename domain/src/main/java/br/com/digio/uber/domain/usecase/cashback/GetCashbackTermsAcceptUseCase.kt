package br.com.digio.uber.domain.usecase.cashback

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.cashback.CashbackRepository
import retrofit2.Response

class GetCashbackTermsAcceptUseCase(
    private val cashbackRepository: CashbackRepository
) : AbstractUseCase<Unit, Response<Void>>() {
    override suspend fun execute(param: Unit) =
        cashbackRepository.getCashbackTermsAccept()
}