package br.com.digio.uber.domain.exception.common

class CredentialModelNotFoundException : Exception()