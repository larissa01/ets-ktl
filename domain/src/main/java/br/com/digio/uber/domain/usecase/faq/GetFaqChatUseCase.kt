package br.com.digio.uber.domain.usecase.faq

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.faq.FaqChatResponse
import br.com.digio.uber.model.faq.FaqChatUrlRequest
import br.com.digio.uber.repository.faq.FaqRepository

class GetFaqChatUseCase(private val repositoryChat: FaqRepository) :
    AbstractUseCase<FaqChatUrlRequest, FaqChatResponse>() {

    override suspend fun execute(param: FaqChatUrlRequest): FaqChatResponse {
        return repositoryChat.getChatLink(param)
    }
}