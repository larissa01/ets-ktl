package br.com.digio.uber.domain.usecase.faq

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.faq.FaqPdfResponse
import br.com.digio.uber.repository.faq.FaqRepository

class FaqPdfUseCase(val repository: FaqRepository) : AbstractUseCase<Unit, FaqPdfResponse>() {
    override suspend fun execute(param: Unit): FaqPdfResponse {
        return repository.getPdf()
    }
}