package br.com.digio.uber.domain.exception.lexis

import com.threatmetrix.TrustDefender.TMXProfilingHandle

class TrustDefenderException constructor(
    result: TMXProfilingHandle.Result? = null
) : Exception(
    "Error in TrustDefender status = ${result?.status?.name ?: "not found status"}"
)