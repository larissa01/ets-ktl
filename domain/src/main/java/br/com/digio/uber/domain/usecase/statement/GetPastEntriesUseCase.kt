package br.com.digio.uber.domain.usecase.statement

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.statement.PastEntriesRequest
import br.com.digio.uber.model.statement.PastEntriesResponse
import br.com.digio.uber.repository.statement.StatementRepository

class GetPastEntriesUseCase(
    private val statementRepository: StatementRepository
) : AbstractUseCase<PastEntriesRequest, PastEntriesResponse>() {
    override suspend fun execute(param: PastEntriesRequest): PastEntriesResponse =
        statementRepository.getPastEntries(param)
}