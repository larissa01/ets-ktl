package br.com.digio.uber.domain.usecase.bankSlip

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.bankSlip.BankSlipRequest
import br.com.digio.uber.model.bankSlip.BankSlipResponse
import br.com.digio.uber.repository.bankSlip.BankSlipRepository

/**
 * @author Marlon D. Rocha
 * @since 16/10/20
 */
class GenerateBankSlipUseCase(
    private val bankSlipRepository: BankSlipRepository
) : AbstractUseCase<BankSlipRequest, BankSlipResponse>() {
    override suspend fun execute(param: BankSlipRequest): BankSlipResponse =
        bankSlipRepository.generateBankSlip(param)
}