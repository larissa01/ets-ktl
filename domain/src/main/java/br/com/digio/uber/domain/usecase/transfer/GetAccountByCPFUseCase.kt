package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.ThirdPartyAccountListResponse
import br.com.digio.uber.repository.transfer.TransferRepository

class GetAccountByCPFUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<String, ThirdPartyAccountListResponse>() {

    override suspend fun execute(param: String): ThirdPartyAccountListResponse =
        repository.getAccountByCPF(param)
}