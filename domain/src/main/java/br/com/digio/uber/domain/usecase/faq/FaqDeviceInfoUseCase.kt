package br.com.digio.uber.domain.usecase.faq

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.request.DeviceInfo
import kotlinx.coroutines.Deferred

class FaqDeviceInfoUseCase(
    private val repositoryDeviceInfo: Deferred<DeviceInfo>
) : AbstractUseCase<Unit, DeviceInfo>() {
    override suspend fun execute(param: Unit): DeviceInfo {
        return repositoryDeviceInfo.await()
    }
}