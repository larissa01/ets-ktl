package br.com.digio.uber.domain.usecase.location

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.location.LocationRepository

class IsLocationEnabledUseCase(
    private val locationRepository: LocationRepository
) : AbstractUseCase<Unit, Boolean>() {
    override suspend fun execute(param: Unit): Boolean =
        locationRepository.isGPSEnabled()
}