package br.com.digio.uber.domain.usecase.login

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.login.AccountNotLoggedInRepository

class GetCpfFromLocalUseCase constructor(
    private val accountNotLoggedInRepository: AccountNotLoggedInRepository
) : AbstractUseCase<Unit, String>() {
    override suspend fun execute(param: Unit): String =
        accountNotLoggedInRepository.getCpfSaved() ?: String()
}