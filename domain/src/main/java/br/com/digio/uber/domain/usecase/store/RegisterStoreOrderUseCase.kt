package br.com.digio.uber.domain.usecase.store

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.store.StoreOrder
import br.com.digio.uber.model.store.StoreOrderRegisterResponse
import br.com.digio.uber.repository.store.StoreRepository

class RegisterStoreOrderUseCase(
    private val storeRepository: StoreRepository
) : AbstractUseCase<StoreOrder, StoreOrderRegisterResponse>() {
    override suspend fun execute(param: StoreOrder) =
        storeRepository.registerStoreOrder(param)
}