package br.com.digio.uber.domain.usecase.card

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.card.CardRepository
import retrofit2.Response

class PostCardBlockUseCase(
    private val cardRepository: CardRepository
) : AbstractUseCase<Pair<CardUseCaseTypeBlock, Long>, Response<Void>>() {
    override suspend fun execute(param: Pair<CardUseCaseTypeBlock, Long>) =
        when (param.first) {
            CardUseCaseTypeBlock.BLOCK -> cardRepository.postCardBlock(param.second)
            CardUseCaseTypeBlock.UNLOCK -> cardRepository.postCardUnblock(param.second)
        }
}

enum class CardUseCaseTypeBlock {
    BLOCK,
    UNLOCK
}