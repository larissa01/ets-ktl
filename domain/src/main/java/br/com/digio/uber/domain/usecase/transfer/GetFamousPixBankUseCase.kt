package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.PixBankDomainResponse
import br.com.digio.uber.repository.transfer.TransferRepository
import br.com.digio.uber.util.enumTest.CachePolicy

class GetFamousPixBankUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<Unit, PixBankDomainResponse>() {

    override suspend fun execute(param: Unit): PixBankDomainResponse =
        repository.getPopularPixBanks(CachePolicy.LOCAL_FIRST)
}