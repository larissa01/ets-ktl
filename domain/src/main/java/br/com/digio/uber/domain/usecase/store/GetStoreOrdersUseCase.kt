package br.com.digio.uber.domain.usecase.store

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.store.StoreOrderResponse
import br.com.digio.uber.repository.store.StoreRepository

class GetStoreOrdersUseCase(
    private val storeRepository: StoreRepository
) : AbstractUseCase<Int, ResponseList<StoreOrderResponse>>() {
    override suspend fun execute(param: Int) =
        storeRepository.getStoreOrders(param)
}