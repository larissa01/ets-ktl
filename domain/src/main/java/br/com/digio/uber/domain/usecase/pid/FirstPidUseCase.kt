package br.com.digio.uber.domain.usecase.pid

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.pid.PidRes
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.repository.pid.PidRepository

class FirstPidUseCase constructor(
    private val pidRepository: PidRepository
) : AbstractUseCase<PidType, PidRes>() {
    override suspend fun execute(param: PidType): PidRes =
        pidRepository.firstPid(param)
}