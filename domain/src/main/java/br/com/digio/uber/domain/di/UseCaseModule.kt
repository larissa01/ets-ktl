package br.com.digio.uber.domain.di

import br.com.digio.uber.domain.usecase.account.GetAccountUseCase
import br.com.digio.uber.domain.usecase.account.GetCredentialsUseCase
import br.com.digio.uber.domain.usecase.balance.ClearHomeCacheUseCase
import br.com.digio.uber.domain.usecase.balance.GetBalanceUseCase
import br.com.digio.uber.domain.usecase.bankSlip.GenerateBankSlipUseCase
import br.com.digio.uber.domain.usecase.bankSlip.GeneratedBankSlipUseCase
import br.com.digio.uber.domain.usecase.bankSlip.GetBankSlipPdfUseCase
import br.com.digio.uber.domain.usecase.bankSlip.GetBankSlipsAvailableUseCase
import br.com.digio.uber.domain.usecase.card.GetCardListUseCase
import br.com.digio.uber.domain.usecase.card.GetCardTypeUseCase
import br.com.digio.uber.domain.usecase.card.PostCardBlockUseCase
import br.com.digio.uber.domain.usecase.card.cardreissue.PostCardReissueUseCase
import br.com.digio.uber.domain.usecase.card.password.GetCardPasswordUseCase
import br.com.digio.uber.domain.usecase.cardReceived.CardReceivedActivationUseCase
import br.com.digio.uber.domain.usecase.cashback.AcceptCashbackTermsUseCase
import br.com.digio.uber.domain.usecase.cashback.GetAmountCashbackUseCase
import br.com.digio.uber.domain.usecase.cashback.GetCashbackCustomerUseCase
import br.com.digio.uber.domain.usecase.cashback.GetCashbackDetailsUseCase
import br.com.digio.uber.domain.usecase.cashback.GetCashbackOffersUseCase
import br.com.digio.uber.domain.usecase.cashback.GetCashbackPurchasesUseCase
import br.com.digio.uber.domain.usecase.cashback.GetCashbackStoresUseCase
import br.com.digio.uber.domain.usecase.cashback.GetCashbackTermsAcceptUseCase
import br.com.digio.uber.domain.usecase.cashback.GetCashbackTermsUseCase
import br.com.digio.uber.domain.usecase.cashback.GetComissionCategoryUseCase
import br.com.digio.uber.domain.usecase.cashback.SendCashbackAccountUseCase
import br.com.digio.uber.domain.usecase.change.registration.GetChangeRegistrationDataUseCase
import br.com.digio.uber.domain.usecase.change.registration.GetZipCodeUseCase
import br.com.digio.uber.domain.usecase.change.registration.SetChangeRegistrationItemList
import br.com.digio.uber.domain.usecase.change.registration.UpdateAddressUseCase
import br.com.digio.uber.domain.usecase.digioTest.DigioTestUseCase
import br.com.digio.uber.domain.usecase.eyes.GetEyesUseCase
import br.com.digio.uber.domain.usecase.eyes.InsertEyesUseCase
import br.com.digio.uber.domain.usecase.faq.FaqDeviceInfoUseCase
import br.com.digio.uber.domain.usecase.faq.FaqPdfUseCase
import br.com.digio.uber.domain.usecase.faq.GetFaqChatUseCase
import br.com.digio.uber.domain.usecase.faq.GetFaqSubjectsUseCase
import br.com.digio.uber.domain.usecase.faq.GetFrequentQuestionsUseCase
import br.com.digio.uber.domain.usecase.faq.GetQuestionByTagUseCase
import br.com.digio.uber.domain.usecase.faq.UpdateFaqUseCase
import br.com.digio.uber.domain.usecase.flags.GetFlagsNotLoggedInLiveDataUseCase
import br.com.digio.uber.domain.usecase.flags.GetFlagsNotLoggedInUseCase
import br.com.digio.uber.domain.usecase.flags.UpdateFlagsNotLoggedInUseCase
import br.com.digio.uber.domain.usecase.franchise.FranchiseUseCase
import br.com.digio.uber.domain.usecase.home.ConfigFeatureUseCase
import br.com.digio.uber.domain.usecase.income.GetDepositDetailsUseCase
import br.com.digio.uber.domain.usecase.income.GetIncomeDetailsUseCase
import br.com.digio.uber.domain.usecase.income.GetMonthsUseCase
import br.com.digio.uber.domain.usecase.location.GetLastKnowLocationUseCase
import br.com.digio.uber.domain.usecase.location.IsLocationEnabledUseCase
import br.com.digio.uber.domain.usecase.login.ChangePasswordUseCase
import br.com.digio.uber.domain.usecase.login.GetCpfFromLocalUseCase
import br.com.digio.uber.domain.usecase.login.LogoutUseCase
import br.com.digio.uber.domain.usecase.login.MakeLoginUseCase
import br.com.digio.uber.domain.usecase.login.SaveCpfInLocalUseCase
import br.com.digio.uber.domain.usecase.menu.ChangePasswordAuthUseCase
import br.com.digio.uber.domain.usecase.payment.CheckBillBarcodeUseCase
import br.com.digio.uber.domain.usecase.payment.PaymentAuthorizeUseCase
import br.com.digio.uber.domain.usecase.payment.PostPayWithQrCodeUseCase
import br.com.digio.uber.domain.usecase.payment.PostQrcodeReadUseCase
import br.com.digio.uber.domain.usecase.pid.FirstPidUseCase
import br.com.digio.uber.domain.usecase.pid.ValidatePidUseCase
import br.com.digio.uber.domain.usecase.receipt.GetReceiptUseCase
import br.com.digio.uber.domain.usecase.recharge.GetFavoritesContactsUseCase
import br.com.digio.uber.domain.usecase.recharge.GetOperatorPricesUseCase
import br.com.digio.uber.domain.usecase.recharge.RegisterFavoriteContactUseCase
import br.com.digio.uber.domain.usecase.recharge.RemoveFavoriteContactUseCase
import br.com.digio.uber.domain.usecase.recharge.UpdateFavoriteContactUseCase
import br.com.digio.uber.domain.usecase.remuneration.GetRemunerationIncomeUseCase
import br.com.digio.uber.domain.usecase.resetpassword.ResetPasswordUseCase
import br.com.digio.uber.domain.usecase.splash.CheckUserHasLoggedInUseCase
import br.com.digio.uber.domain.usecase.statement.DeleteScheduledEntryUseCase
import br.com.digio.uber.domain.usecase.statement.GetDetailStatementUseCase
import br.com.digio.uber.domain.usecase.statement.GetFutureEntriesUseCase
import br.com.digio.uber.domain.usecase.statement.GetPastEntriesUseCase
import br.com.digio.uber.domain.usecase.store.GetStoreCatalogUseCase
import br.com.digio.uber.domain.usecase.store.GetStoreOrdersUseCase
import br.com.digio.uber.domain.usecase.store.RegisterStoreOrderUseCase
import br.com.digio.uber.domain.usecase.tracking.GetTrackingLogsUseCase
import br.com.digio.uber.domain.usecase.transfer.CanTransferTodayUseCase
import br.com.digio.uber.domain.usecase.transfer.ConfirmExternalTransferUseCase
import br.com.digio.uber.domain.usecase.transfer.ConfirmInternalTransferUseCase
import br.com.digio.uber.domain.usecase.transfer.DeleteFavoredUseCase
import br.com.digio.uber.domain.usecase.transfer.DeleteP2PFavoredUseCase
import br.com.digio.uber.domain.usecase.transfer.EditFavoredUseCase
import br.com.digio.uber.domain.usecase.transfer.EditP2PFavoredUseCase
import br.com.digio.uber.domain.usecase.transfer.GetAccountByCPFUseCase
import br.com.digio.uber.domain.usecase.transfer.GetAccountTedUseCase
import br.com.digio.uber.domain.usecase.transfer.GetAllBanksUseCase
import br.com.digio.uber.domain.usecase.transfer.GetAllPixBanksUseCase
import br.com.digio.uber.domain.usecase.transfer.GetFamousBankUseCase
import br.com.digio.uber.domain.usecase.transfer.GetFamousPixBankUseCase
import br.com.digio.uber.domain.usecase.transfer.GetFavoredUseCase
import br.com.digio.uber.domain.usecase.transfer.GetP2PFavoredUseCase
import br.com.digio.uber.domain.usecase.transfer.SearchAccountUseCase
import br.com.digio.uber.domain.usecase.virtualcard.GetCardOnboardingFirstAccessUseCase
import br.com.digio.uber.domain.usecase.virtualcard.GetVerifyVirtualCardOnboardingUseCase
import br.com.digio.uber.domain.usecase.virtualcard.GetVirtualCardCurrentUseCase
import br.com.digio.uber.domain.usecase.virtualcard.GetVirtualCardOnboardingUseCase
import br.com.digio.uber.domain.usecase.virtualcard.GetVirtualCardUseCase
import br.com.digio.uber.domain.usecase.virtualcard.PostCardOnboardingFirstAccessUseCase
import br.com.digio.uber.domain.usecase.virtualcard.PostVerifyVirtualCardOnboardingUseCase
import br.com.digio.uber.domain.usecase.virtualcard.PostVirtualCardBlockUseCase
import br.com.digio.uber.domain.usecase.virtualcard.PostVirtualCardCurrentUseCase
import br.com.digio.uber.domain.usecase.virtualcard.PostVirtualCardRegenerateUseCase
import br.com.digio.uber.domain.usecase.withdraw.DoWithdrawUseCase
import br.com.digio.uber.domain.usecase.withdraw.GetWithdrawStatusUseCase
import org.koin.dsl.module

val useCaseModule = module {
    single { DigioTestUseCase(get()) }
    single { GetBalanceUseCase(get()) }
    single { ClearHomeCacheUseCase(get()) }
    single { GetEyesUseCase(get()) }
    single { InsertEyesUseCase(get()) }
    single { GenerateBankSlipUseCase(get()) }
    single { GetBankSlipsAvailableUseCase(get()) }
    single { GetBankSlipPdfUseCase(get()) }
    single { GetCpfFromLocalUseCase(get()) }
    single { SaveCpfInLocalUseCase(get()) }
    single { GetFavoritesContactsUseCase(get()) }
    single { GetOperatorPricesUseCase(get()) }
    single { RegisterFavoriteContactUseCase(get()) }
    single { RemoveFavoriteContactUseCase(get()) }
    single { GetFlagsNotLoggedInUseCase(get()) }
    single { GetFlagsNotLoggedInLiveDataUseCase(get()) }
    single { UpdateFlagsNotLoggedInUseCase(get()) }
    single { GetAllBanksUseCase(get()) }
    single { GetAllPixBanksUseCase(get()) }
    single { GetFamousBankUseCase(get()) }
    single { GetFamousPixBankUseCase(get()) }
    single { CanTransferTodayUseCase(get()) }
    single { ConfirmExternalTransferUseCase(get(), get()) }
    single { ConfirmInternalTransferUseCase(get(), get()) }
    single { GetFavoredUseCase(get()) }
    single { EditFavoredUseCase(get()) }
    single { DeleteFavoredUseCase(get()) }
    single { ConfigFeatureUseCase(get()) }
    single { GetFrequentQuestionsUseCase(get()) }
    single { GetQuestionByTagUseCase(get()) }
    single { GetFaqSubjectsUseCase(get()) }
    single { UpdateFaqUseCase(get()) }
    single { FaqDeviceInfoUseCase(get()) }
    single { GetFaqChatUseCase(get()) }
    single { GetReceiptUseCase(get()) }
    single { GetStoreCatalogUseCase(get()) }
    single { RegisterStoreOrderUseCase(get()) }
    single { UpdateFavoriteContactUseCase(get()) }
    single { GetCardListUseCase(get()) }
    single { GetCardTypeUseCase(get()) }
    single { GetVirtualCardUseCase(get()) }
    single { PostVirtualCardBlockUseCase(get()) }
    single { PostVirtualCardRegenerateUseCase(get()) }
    single { PostQrcodeReadUseCase(get()) }
    single { PostPayWithQrCodeUseCase(get()) }
    single { GetRemunerationIncomeUseCase(get()) }
    single {
        MakeLoginUseCase(
            get(),
            get(),
            get(),
            get(),
            get(),
            get()
        )
    }
    single { GeneratedBankSlipUseCase(get()) }
    single { ResetPasswordUseCase(get()) }
    single { CheckUserHasLoggedInUseCase(get()) }
    single { ChangePasswordUseCase(get()) }
    single { GetP2PFavoredUseCase(get()) }
    single { EditP2PFavoredUseCase(get()) }
    single { DeleteP2PFavoredUseCase(get()) }
    single { ChangePasswordAuthUseCase(get()) }
    single { GetCredentialsUseCase(get()) }
    single { LogoutUseCase(get(), get()) }
    single { GetAccountByCPFUseCase(get()) }
    single { SearchAccountUseCase(get()) }
    single { GetPastEntriesUseCase(get()) }
    single { GetFutureEntriesUseCase(get()) }
    single { DeleteScheduledEntryUseCase(get()) }
    single { GetAccountTedUseCase(get()) }
    single { FirstPidUseCase(get()) }
    single { GetMonthsUseCase(get()) }
    single { GetIncomeDetailsUseCase(get()) }
    single { GetDepositDetailsUseCase(get()) }
    single { ValidatePidUseCase(get()) }
    single { CheckBillBarcodeUseCase(get()) }
    single { GetAccountUseCase(get()) }
    single { GetAmountCashbackUseCase(get()) }
    single { GetCashbackStoresUseCase(get()) }
    single { GetStoreOrdersUseCase(get()) }
    single { PostCardBlockUseCase(get()) }
    single { GetTrackingLogsUseCase(get()) }
    single { PostVirtualCardCurrentUseCase(get()) }
    single { GetVirtualCardCurrentUseCase(get()) }
    single { GetCardPasswordUseCase(get()) }
    single { GetWithdrawStatusUseCase(get()) }
    single { IsLocationEnabledUseCase(get()) }
    single { FaqPdfUseCase(get()) }
    single { GetCashbackTermsUseCase(get()) }
    single { AcceptCashbackTermsUseCase(get()) }
    single { PaymentAuthorizeUseCase(get()) }
    single {
        GetChangeRegistrationDataUseCase(
            get()
        )
    }
    single { SetChangeRegistrationItemList(get()) }
    single { GetZipCodeUseCase(get()) }
    single { GetCashbackTermsAcceptUseCase(get()) }
    single { UpdateAddressUseCase(get()) }
    single { GetCashbackCustomerUseCase(get()) }
    single { GetCashbackDetailsUseCase(get()) }
    single { CardReceivedActivationUseCase(get()) }
    single { DoWithdrawUseCase(get()) }
    single { GetLastKnowLocationUseCase(get()) }
    single { GetCashbackOffersUseCase(get()) }
    single { PostCardReissueUseCase(get()) }
    single { GetComissionCategoryUseCase(get()) }
    single { GetCashbackPurchasesUseCase(get()) }
    single { SendCashbackAccountUseCase(get()) }
    single { GetVirtualCardOnboardingUseCase(get()) }
    single { GetVerifyVirtualCardOnboardingUseCase(get()) }
    single { PostVerifyVirtualCardOnboardingUseCase(get()) }
    single { GetCardOnboardingFirstAccessUseCase(get()) }
    single { PostCardOnboardingFirstAccessUseCase(get()) }
    single { FranchiseUseCase(get()) }
    single { GetDetailStatementUseCase(get()) }
    // single { GetLastKnowLocationUseCase(get()) }
}