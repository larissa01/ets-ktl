package br.com.digio.uber.domain.usecase.receipt

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.receipt.Receipt
import br.com.digio.uber.repository.repository.ReceiptRepository

class GetReceiptUseCase(
    private val receiptRepository: ReceiptRepository
) : AbstractUseCase<String, Receipt>() {
    override suspend fun execute(param: String): Receipt =
        receiptRepository.requestReceipt(param)
}