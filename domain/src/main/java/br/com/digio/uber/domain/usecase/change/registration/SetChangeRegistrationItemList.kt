package br.com.digio.uber.domain.usecase.change.registration

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.change.registration.ChangeRegistrationRequest
import br.com.digio.uber.repository.account.AccountChangeRegistrationRepository

class SetChangeRegistrationItemList constructor(
    private val accountRepository: AccountChangeRegistrationRepository
) : AbstractUseCase<ChangeRegistrationRequest, Unit>() {
    override suspend fun execute(param: ChangeRegistrationRequest) {
        val registrationItem = param.registrationItem.copy(
            value = param.registrationItem.value ?: String(),
            valueNumber = param.registrationItem.valueNumber ?: 0f
        )
        accountRepository.changeRegistration(
            param.copy(
                registrationItem = registrationItem
            )
        )
    }
}