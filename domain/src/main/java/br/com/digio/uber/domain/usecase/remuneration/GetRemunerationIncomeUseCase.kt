package br.com.digio.uber.domain.usecase.remuneration

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.remuneration.IncomeIndexResponse
import br.com.digio.uber.repository.cache.CoCache
import br.com.digio.uber.repository.cache.HawkCache
import br.com.digio.uber.repository.remuneration.RemunerationRepository
import br.com.digio.uber.util.Const

class GetRemunerationIncomeUseCase(
    private val remunerationRepository: RemunerationRepository
) : AbstractUseCase<Unit, IncomeIndexResponse>(
    CoCache(HawkCache()),
    Const.Cache.KEY_REMUNERATION_INCOME_CACHE
) {
    override suspend fun execute(param: Unit): IncomeIndexResponse =
        remunerationRepository.getIncome()
}