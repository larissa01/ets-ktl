package br.com.digio.uber.domain.usecase.virtualcard

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.virtualcard.VirtualCardOnboardingRepository

class GetCardOnboardingFirstAccessUseCase constructor(
    private val repository: VirtualCardOnboardingRepository
) : AbstractUseCase<Unit, Boolean>() {
    override suspend fun execute(param: Unit): Boolean =
        repository.getFirstAccessOnboarding()
}