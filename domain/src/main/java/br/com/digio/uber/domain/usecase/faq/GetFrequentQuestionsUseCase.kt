package br.com.digio.uber.domain.usecase.faq

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.faq.FaqQuestionResponse
import br.com.digio.uber.repository.faq.FaqRepository
import br.com.digio.uber.util.unaccent

class GetFrequentQuestionsUseCase(val repository: FaqRepository) :
    AbstractUseCase<String?, List<FaqQuestionResponse>>() {
    private var faqContent: List<FaqQuestionResponse> = listOf()
    private var faqSearch: List<FaqQuestionResponse> = listOf()

    override suspend fun execute(param: String?): List<FaqQuestionResponse> {
        if (faqContent.isEmpty()) {
            faqContent = repository.getFaqCategories()
                .map { it.questions }
                .reduce { a, b -> a.union(b).toList() }
                .filter { question ->
                    question.isFrequentQuestion == true
                }
            faqSearch = faqContent
        }

        param?.let {
            faqSearch = if (it.isNotEmpty()) {
                searchInFaqQuestions(faqContent, it)
            } else {
                faqContent
            }
        }

        return faqSearch
    }

    private fun searchInFaqQuestions(texts: List<FaqQuestionResponse>, search: String): List<FaqQuestionResponse> {
        val searchArray = search.split(" ")
        val questionResult = arrayListOf<FaqQuestionResponse>()
        texts.forEach {
            var contains = true

            searchArray.forEach searchForeach@{ search ->
                if (it.title?.unaccent()?.contains(search.unaccent(), ignoreCase = true) == false) {
                    contains = false
                    return@searchForeach
                }
            }
            if (contains) {
                questionResult.add(it)
            }
        }

        return questionResult
    }
}