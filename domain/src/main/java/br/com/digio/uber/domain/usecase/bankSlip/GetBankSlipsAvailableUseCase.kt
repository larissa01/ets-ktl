package br.com.digio.uber.domain.usecase.bankSlip

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.bankSlip.BankSlipStatus
import br.com.digio.uber.repository.bankSlip.BankSlipRepository

/**
 * @author Marlon D. Rocha
 * @since 16/10/20
 */
class GetBankSlipsAvailableUseCase(
    private val bankSlipRepository: BankSlipRepository
) : AbstractUseCase<Unit, BankSlipStatus>() {
    override suspend fun execute(param: Unit): BankSlipStatus =
        bankSlipRepository.getBankSlipStatus()
}