package br.com.digio.uber.domain.usecase.pid

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.domain.Error
import br.com.digio.uber.domain.ResultWrapper
import br.com.digio.uber.domain.asFailure
import br.com.digio.uber.domain.exception.pid.PidHashExpirationException
import br.com.digio.uber.domain.exception.pid.PidInvalidException
import br.com.digio.uber.model.pid.PidRes
import br.com.digio.uber.model.pid.PidStepReq
import br.com.digio.uber.repository.error.ServerException
import br.com.digio.uber.repository.error.tryConvertError
import br.com.digio.uber.repository.pid.PidRepository
import br.com.digio.uber.util.castOrNull
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapNotNull

class ValidatePidUseCase constructor(
    private val pidRepository: PidRepository
) : AbstractUseCase<PidStepReq, PidRes>() {

    override suspend fun execute(param: PidStepReq): PidRes =
        pidRepository.pidValidate(param)

    override fun invoke(value: PidStepReq, forceLoad: Boolean?): Flow<ResultWrapper<PidRes>> =
        super.invoke(value, forceLoad).mapNotNull { resultWrapper ->
            when (resultWrapper) {
                is ResultWrapper.Failure -> resolveErrorsFromCode(resultWrapper)
                else -> resultWrapper
            }
        }

    override suspend fun onError(throwable: Throwable): ResultWrapper.Failure =
        throwable.tryConvertError<PidRes>().asFailure()

    @Suppress("TooGenericExceptionCaught")
    private fun resolveErrorsFromCode(
        result: ResultWrapper.Failure
    ): ResultWrapper.Failure =
        result.error.castOrNull<Error.Server>()?.cause?.castOrNull<ServerException>()
            ?.let { serverException ->
                if (serverException.checkContainsCode(PL_002)) {
                    ResultWrapper.Failure(
                        Error.Business(
                            serverException.error?.error?.message,
                            PidHashExpirationException()
                        )
                    )
                } else {
                    resolvePidError(serverException, result)
                }
            } ?: result

    private fun resolvePidError(
        serverException: ServerException,
        result: ResultWrapper.Failure
    ): ResultWrapper.Failure =
        serverException.error?.error?.data?.castOrNull<PidRes>()?.let { pidRes ->
            ResultWrapper.Failure(
                Error.Business(
                    serverException.error?.error?.message,
                    PidInvalidException(pidRes)
                )
            )
        } ?: result

    private fun ServerException.checkContainsCode(code: String): Boolean =
        error?.error?.code?.contains(code) == true

    companion object {
        private const val PL_002 = "PID-002"
    }
}