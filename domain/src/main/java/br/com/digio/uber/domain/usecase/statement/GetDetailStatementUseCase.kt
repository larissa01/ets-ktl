package br.com.digio.uber.domain.usecase.statement

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.statement.StatementDetailResponse
import br.com.digio.uber.repository.statement.StatementRepository

class GetDetailStatementUseCase(
    private val statementRepository: StatementRepository
) : AbstractUseCase<String, StatementDetailResponse>() {
    override suspend fun execute(param: String): StatementDetailResponse =
        statementRepository.getStatementDetails(param)
}