package br.com.digio.uber.domain.usecase.virtualcard

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.virtualcard.VirtualCardRepository

class GetVirtualCardCurrentUseCase(
    private val virtualCardRepository: VirtualCardRepository
) : AbstractUseCase<String, Long>() {
    override suspend fun execute(param: String): Long =
        virtualCardRepository.getVirtualCardCurrent()
}