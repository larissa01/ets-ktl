package br.com.digio.uber.domain.usecase.splash

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.util.TokenPreferencesUtils

class CheckUserHasLoggedInUseCase constructor(
    private val tokenPreferencesUtils: TokenPreferencesUtils
) : AbstractUseCase<Unit, Boolean>() {
    override suspend fun execute(param: Unit): Boolean =
        tokenPreferencesUtils.getToken() != null
}