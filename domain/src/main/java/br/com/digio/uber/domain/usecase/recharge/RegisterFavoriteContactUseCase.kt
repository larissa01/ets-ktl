package br.com.digio.uber.domain.usecase.recharge

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.recharge.FavoriteContact
import br.com.digio.uber.repository.recharge.RechargeRepository

class RegisterFavoriteContactUseCase(
    private val rechargeRepository: RechargeRepository
) : AbstractUseCase<FavoriteContact, Unit>() {
    override suspend fun execute(param: FavoriteContact) =
        rechargeRepository.registerFavoriteContact(param)
}