package br.com.digio.uber.domain.usecase.eyes

import androidx.lifecycle.LiveData
import br.com.digio.uber.domain.AbstractLiveDataUseCase
import br.com.digio.uber.repository.eyes.EyesRepository

class GetEyesUseCase(
    private val eyesRepository: EyesRepository
) : AbstractLiveDataUseCase<Unit, Boolean>() {
    override fun execute(param: Unit): LiveData<Boolean> = eyesRepository.getEyesVisibility()
}