package br.com.digio.uber.domain.usecase.payment

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.payment.PayWithQrCodeRequest
import br.com.digio.uber.repository.payment.QrCodeRepository
import retrofit2.Response

class PostPayWithQrCodeUseCase(
    private val qrCodeRepository: QrCodeRepository
) : AbstractUseCase<Pair<PayWithQrCodeRequest, String>, Response<Void>>() {
    override suspend fun execute(param: Pair<PayWithQrCodeRequest, String>): Response<Void> =
        qrCodeRepository.postPayWithQrCode(param.first, param.second)
}