package br.com.digio.uber.domain.usecase.menu

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.change.password.ChangePasswordAuthReq
import br.com.digio.uber.repository.account.AccountRepository

class ChangePasswordAuthUseCase constructor(
    private val accountRepository: AccountRepository
) : AbstractUseCase<ChangePasswordAuthReq, Unit>() {
    override suspend fun execute(param: ChangePasswordAuthReq) {
        accountRepository.changePasswordAuth(param)
    }
}