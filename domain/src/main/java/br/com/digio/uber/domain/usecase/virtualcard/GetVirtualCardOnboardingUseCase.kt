package br.com.digio.uber.domain.usecase.virtualcard

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.virtualCard.VirtualCardOnboardingResponse
import br.com.digio.uber.repository.virtualcard.VirtualCardOnboardingRepository

class GetVirtualCardOnboardingUseCase constructor(
    private val repository: VirtualCardOnboardingRepository
) : AbstractUseCase<Unit, VirtualCardOnboardingResponse>() {
    override suspend fun execute(param: Unit): VirtualCardOnboardingResponse =
        repository.getOnboardingContent()
}