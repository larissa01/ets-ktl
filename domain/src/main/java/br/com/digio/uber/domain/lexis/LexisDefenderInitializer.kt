package br.com.digio.uber.domain.lexis

import android.content.Context
import br.com.digio.uber.domain.BuildConfig
import br.com.digio.uber.domain.exception.lexis.TrustDefenderException
import com.threatmetrix.TrustDefender.TMXConfig
import com.threatmetrix.TrustDefender.TMXProfiling
import com.threatmetrix.TrustDefender.TMXProfilingConnections.TMXProfilingConnections
import com.threatmetrix.TrustDefender.TMXProfilingOptions
import com.threatmetrix.TrustDefender.TMXStatusCode
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.coroutines.suspendCoroutine

class LexisDefenderInitializer constructor(
    private val context: Context
) {

    private fun initializer() {
        TMXProfiling.getInstance().init(
            TMXConfig()
                .setOrgId(BuildConfig.LEXIS_ORG_ID)
                .setFPServer(BuildConfig.LEXIS_FPSERVER)
                .setProfileTimeout(PROFILE_TIMEOUT, TimeUnit.SECONDS)
                .setProfilingConnections(
                    TMXProfilingConnections()
                        .setConnectionTimeout(PROFILE_TIMEOUT, TimeUnit.SECONDS)
                )
                .setContext(context)
        )
    }

    @Suppress("TooGenericExceptionCaught")
    suspend fun doProfile(): String =
        withContext(Default) {
            suspendCoroutine<String> { coroutineScope ->
                try {
                    initializer()
                    val list: MutableList<String> = ArrayList()
                    val options = TMXProfilingOptions().setCustomAttributes(list)

                    TMXProfiling.getInstance().profile(options) { resultProfilling ->
                        if (resultProfilling?.status != TMXStatusCode.TMX_OK) {
                            Timber.e(
                                TrustDefenderException(
                                    resultProfilling
                                )
                            )
                        }
                        coroutineScope.resumeWith(
                            Result.success(
                                resultProfilling?.sessionID ?: String()
                            )
                        )
                    }
                } catch (e: IllegalStateException) {
                    Timber.e(e)
                    coroutineScope.resumeWith(Result.failure(TrustDefenderException()))
                } catch (e: IllegalArgumentException) {
                    Timber.e(e)
                    coroutineScope.resumeWith(Result.failure(TrustDefenderException()))
                } catch (e: Throwable) {
                    Timber.e(e)
                    coroutineScope.resumeWith(Result.failure(TrustDefenderException()))
                }
            }
        }

    companion object {
        private const val PROFILE_TIMEOUT = 30
    }
}