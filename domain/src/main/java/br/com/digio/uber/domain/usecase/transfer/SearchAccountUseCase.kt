package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.AccountSearchDomainRequest
import br.com.digio.uber.model.transfer.ThirdPartyAccountResponse
import br.com.digio.uber.repository.transfer.TransferRepository

class SearchAccountUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<AccountSearchDomainRequest, ThirdPartyAccountResponse>() {

    override suspend fun execute(param: AccountSearchDomainRequest): ThirdPartyAccountResponse =
        repository.searchAccount(param.agency, getAccountRequestFormat(param.account, param.digit))

    private fun getAccountRequestFormat(account: String, digit: String): String {
        return account.plus(digit)
    }
}