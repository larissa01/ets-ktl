package br.com.digio.uber.domain.usecase.faq

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.faq.FaqSubjectResponse
import br.com.digio.uber.repository.faq.FaqRepository

class GetFaqSubjectsUseCase(val repository: FaqRepository) : AbstractUseCase<Unit, List<FaqSubjectResponse>>() {
    override suspend fun execute(param: Unit): List<FaqSubjectResponse> {
        return repository.getFaqCategories().filter { it.questions.isNotEmpty() }
    }
}