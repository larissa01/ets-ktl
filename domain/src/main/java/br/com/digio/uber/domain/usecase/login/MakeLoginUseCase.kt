package br.com.digio.uber.domain.usecase.login

import android.graphics.Bitmap
import br.com.digio.uber.domain.lexis.LexisDefenderInitializer
import br.com.digio.uber.model.common.CredentialModel
import br.com.digio.uber.model.login.LoginRes
import br.com.digio.uber.model.login.RiskAnalysisInfo
import br.com.digio.uber.model.login.TokenizantionInfo
import br.com.digio.uber.model.login.repository.model.Login
import br.com.digio.uber.model.request.DeviceInfo
import br.com.digio.uber.repository.account.AccountRepository
import br.com.digio.uber.repository.gemalto.GemaltoRepository
import br.com.digio.uber.repository.gemalto.exception.GemaltoEnrollException
import br.com.digio.uber.repository.login.AccountNotLoggedInRepository
import br.com.digio.uber.util.TokenPreferencesUtils
import br.com.digio.uber.util.convertToBase64
import br.com.digio.uber.util.convertToFile
import br.com.digio.uber.util.encrypt.encrypt
import kotlinx.coroutines.Deferred
import timber.log.Timber

class MakeLoginUseCase constructor(
    private val accountNotLoggedInRepository: AccountNotLoggedInRepository,
    private val accountRepository: AccountRepository,
    private val lexisDefenderInitializer: LexisDefenderInitializer,
    private val gemaltoRepository: GemaltoRepository,
    private val deviceInfoAsync: Deferred<DeviceInfo>,
    private val preference: TokenPreferencesUtils
) : AbstractMakeLoginErrorResolveUseCase(gemaltoRepository) {

    private var sessionIdByTrustDefender: String? = null

    override suspend fun execute(param: Login): LoginRes {
        val loginRes = accountNotLoggedInRepository.makeLogin(
            param.createAuthenticationRequest().checkHaveFacialRecognitionAndConvertToBase()
        )

        saveAndUpdateDataUser(param, loginRes)

        return loginRes
    }

    private suspend fun saveAndUpdateDataUser(
        login: Login,
        loginRes: LoginRes
    ) {
        preference.setToken(loginRes.accessToken)
        preference.setRefreshToken(loginRes.refreshToken)
        preference.setTimestampToken(loginRes.accessExpiresIn)
        accountRepository.saveCredential(
            CredentialModel(
                cpfCrypted = login.userName.encrypt(),
                password = login.password.encrypt()
            )
        )

        accountRepository.sendContactKey(login.userName)

        val gemaltoActivate = gemaltoRepository.checkHaveARegiseredUser(login.userName)
        if (!gemaltoActivate) {
            try {
                gemaltoRepository.getUserEroll(login.userName)
            } catch (e: GemaltoEnrollException) {
                clearGemaltoCredentials(login)
            }
        }
    }

    private fun Login.checkHaveFacialRecognitionAndConvertToBase(): Login =
        copy(
            pidInfo = pidInfo?.copy(
                facialRecognition = pidInfo
                    ?.facialRecognition
                    ?.convertToFile()
                    ?.convertToBase64(format = Bitmap.CompressFormat.JPEG)
            )
        )

    private suspend fun Login.createAuthenticationRequest(): Login {
        val gemaltoActivate = gemaltoRepository.checkHaveARegiseredUser(userName)
        return if (gemaltoActivate) {
            val deviceInfo = deviceInfoAsync.await()
            getTokenizationInfo(deviceInfo)
        } else {
            getRiskAnalysisInfo()
        }
    }

    private suspend fun Login.getRiskAnalysisInfo(): Login {
        if (sessionIdByTrustDefender == null) {
            sessionIdByTrustDefender = lexisDefenderInitializer.doProfile()
        }
        return copy(
            riskAnalysisInfo = RiskAnalysisInfo(
                sessionId = sessionIdByTrustDefender
                    ?: throw IllegalStateException("SessionID is null")
            )
        )
    }

    private suspend fun Login.getTokenizationInfo(
        deviceInfo: DeviceInfo
    ): Login =
        try {
            copy(
                tokenizationInfo = TokenizantionInfo(
                    otp = gemaltoRepository.getOtp(
                        mapOf(
                            "value" to userName,
                            "deviceId" to deviceInfo.deviceId,
                            "appVersion" to deviceInfo.appVersion,
                            "brand" to deviceInfo.brand,
                            "model" to deviceInfo.deviceModel
                        ),
                        userName
                    )
                )
            )
        } catch (e: GemaltoEnrollException) {
            Timber.e(e)
            getRiskAnalysisInfo()
        }
}