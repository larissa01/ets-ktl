package br.com.digio.uber.domain.usecase.cashback

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.cashback.CashbackComissionCategory
import br.com.digio.uber.repository.cashback.CashbackRepository

class GetComissionCategoryUseCase(
    private val cashbackRepository: CashbackRepository
) : AbstractUseCase<Long, ResponseList<CashbackComissionCategory>>() {
    override suspend fun execute(param: Long) =
        cashbackRepository.getComissionCategory(param)
}