package br.com.digio.uber.domain.usecase.flags

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.UberFlagsPersistNotLoggedIn
import br.com.digio.uber.repository.login.AccountNotLoggedInRepository

class UpdateFlagsNotLoggedInUseCase constructor(
    private val accountNotLoggedInRepository: AccountNotLoggedInRepository
) : AbstractUseCase<UberFlagsPersistNotLoggedIn, UberFlagsPersistNotLoggedIn>() {
    override suspend fun execute(param: UberFlagsPersistNotLoggedIn): UberFlagsPersistNotLoggedIn =
        accountNotLoggedInRepository.updateUberFlagsPersistNotLoggedIn(param) ?: UberFlagsPersistNotLoggedIn()
}