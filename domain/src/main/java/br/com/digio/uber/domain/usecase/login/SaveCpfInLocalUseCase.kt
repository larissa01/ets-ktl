package br.com.digio.uber.domain.usecase.login

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.login.AccountNotLoggedInRepository

class SaveCpfInLocalUseCase constructor(
    private val accountNotLoggedInRepository: AccountNotLoggedInRepository
) : AbstractUseCase<String, Unit>() {
    override suspend fun execute(param: String) {
        accountNotLoggedInRepository.saveCpf(param)
    }
}