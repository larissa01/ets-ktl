package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.TransferStatus
import br.com.digio.uber.repository.transfer.TransferRepository

/**
 * @author Marlon D. Rocha
 * @since 06/11/20
 */

class CanTransferTodayUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<Unit, TransferStatus>() {

    override suspend fun execute(param: Unit): TransferStatus =
        repository.getTransferStatus()
}