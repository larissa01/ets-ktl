package br.com.digio.uber.domain

import androidx.lifecycle.LiveData

abstract class AbstractLiveDataUseCase<in PARAM, RESPONSE> {

    protected abstract fun execute(param: PARAM): LiveData<RESPONSE>

    @Suppress("TooGenericExceptionCaught")
    open operator fun invoke(value: PARAM): LiveData<RESPONSE> = execute(value)
}