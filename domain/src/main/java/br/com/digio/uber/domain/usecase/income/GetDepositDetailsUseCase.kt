package br.com.digio.uber.domain.usecase.income

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.income.DetailDepositResponse
import br.com.digio.uber.repository.income.IncomeRepository

class GetDepositDetailsUseCase(
    private val repository: IncomeRepository
) : AbstractUseCase<Int, DetailDepositResponse>() {

    override suspend fun execute(param: Int): DetailDepositResponse =
        repository.getDepositDetails(param)
}