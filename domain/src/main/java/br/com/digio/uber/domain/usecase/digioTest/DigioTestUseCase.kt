package br.com.digio.uber.domain.usecase.digioTest

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.digioTests.ProductsList
import br.com.digio.uber.repository.digioTestRepository.DigioTestRepository

class DigioTestUseCase(
    private val digioTestRepository: DigioTestRepository
) : AbstractUseCase<Unit, ProductsList>() {
    override suspend fun execute(param: Unit): ProductsList =
        digioTestRepository.requestDigioTestPayload()
}