package br.com.digio.uber.domain.exception.pid

import br.com.digio.uber.model.pid.PidRes

class PidInvalidException constructor(
    val pidRes: PidRes
) : Exception()