package br.com.digio.uber.domain.usecase.cashback

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.cashback.SendCashbackRequest
import br.com.digio.uber.repository.cashback.CashbackRepository
import retrofit2.Response

class SendCashbackAccountUseCase(
    private val cashbackRepository: CashbackRepository
) : AbstractUseCase<SendCashbackRequest, Response<Void>>() {
    override suspend fun execute(param: SendCashbackRequest) =
        cashbackRepository.sendCashbackAccount(param.receiptForm, param)
}