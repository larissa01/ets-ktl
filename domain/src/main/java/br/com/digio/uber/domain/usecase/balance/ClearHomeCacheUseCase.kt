package br.com.digio.uber.domain.usecase.balance

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.home.ConfigFeatureRepository

class ClearHomeCacheUseCase constructor(
    private val configFeatureRepository: ConfigFeatureRepository
) : AbstractUseCase<Unit, Unit>() {
    override suspend fun execute(param: Unit) {
        configFeatureRepository.clearHomeCache()
    }
}