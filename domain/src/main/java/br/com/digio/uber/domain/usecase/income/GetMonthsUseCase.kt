package br.com.digio.uber.domain.usecase.income

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.income.AvailableMonthsResponse
import br.com.digio.uber.repository.income.IncomeRepository

class GetMonthsUseCase(
    private val repository: IncomeRepository
) : AbstractUseCase<Unit, AvailableMonthsResponse>() {
    override suspend fun execute(param: Unit): AvailableMonthsResponse =
        repository.getAvailableMonths()
}