package br.com.digio.uber.domain.usecase.location

import android.location.Location
import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.location.LocationRepository

class GetLastKnowLocationUseCase(
    private val locationRepository: LocationRepository
) : AbstractUseCase<Unit, Location>() {
    override suspend fun execute(param: Unit): Location =
        locationRepository.getLastKnowLocation()
}