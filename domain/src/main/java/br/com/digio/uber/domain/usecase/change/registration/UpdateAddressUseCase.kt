package br.com.digio.uber.domain.usecase.change.registration

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.change.registration.AddressRegistrationRequest
import br.com.digio.uber.repository.account.AccountChangeRegistrationRepository

class UpdateAddressUseCase constructor(
    private val accountChangeRegistrationRepository: AccountChangeRegistrationRepository
) : AbstractUseCase<AddressRegistrationRequest, Unit>() {
    override suspend fun execute(param: AddressRegistrationRequest) {
        accountChangeRegistrationRepository.updateAddress(param)
    }
}