package br.com.digio.uber.domain.usecase.tracking

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.tracking.TrackingLogResponse
import br.com.digio.uber.repository.tracking.TrackingRepository

class GetTrackingLogsUseCase(
    private val trackingRepository: TrackingRepository
) : AbstractUseCase<Long, TrackingLogResponse>() {
    override suspend fun execute(param: Long): TrackingLogResponse =
        trackingRepository.getTrackingLog(param)
}