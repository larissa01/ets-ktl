package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.BankDomainResponse
import br.com.digio.uber.repository.transfer.TransferRepository
import br.com.digio.uber.util.enumTest.CachePolicy

/**
 * @author Marlon D. Rocha
 * @since 03/11/20
 */
class GetFamousBankUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<Unit, BankDomainResponse>() {

    override suspend fun execute(param: Unit): BankDomainResponse {
        return repository.getPopularBanks(CachePolicy.LOCAL_FIRST)
    }
}