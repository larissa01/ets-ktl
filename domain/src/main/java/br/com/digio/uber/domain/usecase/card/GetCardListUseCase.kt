package br.com.digio.uber.domain.usecase.card

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.card.CardResponse
import br.com.digio.uber.repository.card.CardRepository

class GetCardListUseCase(
    private val cardRepository: CardRepository
) : AbstractUseCase<Unit, List<CardResponse>>() {
    override suspend fun execute(param: Unit): List<CardResponse> =
        cardRepository.getCardList()
}