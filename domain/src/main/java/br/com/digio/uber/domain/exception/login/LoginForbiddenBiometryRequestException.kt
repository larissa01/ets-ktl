package br.com.digio.uber.domain.exception.login

import br.com.digio.uber.model.error.ErrorResponse
import br.com.digio.uber.model.login.PidTrackingInfo

class LoginForbiddenBiometryRequestException(
    val responseErrorLexisLogin: ErrorResponse<PidTrackingInfo>
) : Exception()