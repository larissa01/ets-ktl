package br.com.digio.uber.domain.usecase.account

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.account.AccountResponse
import br.com.digio.uber.repository.account.AccountRepository
import br.com.digio.uber.repository.cache.CoCache
import br.com.digio.uber.repository.cache.HawkCache
import br.com.digio.uber.util.Const

class GetAccountUseCase(
    private val accountRepository: AccountRepository
) : AbstractUseCase<Unit, AccountResponse>(CoCache(HawkCache()), Const.Cache.KEY_ACCOUNT_CACHE) {
    override suspend fun execute(param: Unit): AccountResponse =
        accountRepository.getAccount()
}