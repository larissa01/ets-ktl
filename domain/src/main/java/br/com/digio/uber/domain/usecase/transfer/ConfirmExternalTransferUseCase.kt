package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.ExternalTransferDomainRequest
import br.com.digio.uber.model.transfer.ExternalTransferRequest
import br.com.digio.uber.model.transfer.TransferResponse
import br.com.digio.uber.repository.home.ConfigFeatureRepository
import br.com.digio.uber.repository.transfer.TransferRepository
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.unmaskMoney

class ConfirmExternalTransferUseCase(
    private val repository: TransferRepository,
    private val configFeatureRepository: ConfigFeatureRepository
) : AbstractUseCase<ExternalTransferDomainRequest, TransferResponse>() {

    override suspend fun execute(param: ExternalTransferDomainRequest): TransferResponse {
        configFeatureRepository.clearHomeCache()
        return if (param.isSameOwner) {
            repository.confirmTransferExternalSameOwner(
                ExternalTransferRequest(
                    accountBranch = param.agency,
                    accountType = param.accountType,
                    accountNumber = param.account,
                    amount = param.amount.unmaskMoney(),
                    saveBeneficiary = param.saveBeneficiary,
                    comment = param.description,
                    password = param.password,
                    scheduleDate = param.date,
                    bankCode = param.bankCode
                )
            )
        } else {
            repository.confirmTransferExternalThirdParty(
                ExternalTransferRequest(
                    accountBranch = param.agency,
                    accountHolderDocument = param.cpf.unmask(),
                    accountHolderName = param.name,
                    accountType = param.accountType,
                    accountNumber = param.account,
                    amount = param.amount.unmaskMoney(),
                    saveBeneficiary = param.saveBeneficiary,
                    comment = param.description,
                    password = param.password,
                    scheduleDate = param.date,
                    bankCode = param.bankCode
                )
            )
        }
    }
}