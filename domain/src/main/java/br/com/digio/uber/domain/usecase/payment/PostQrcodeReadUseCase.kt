package br.com.digio.uber.domain.usecase.payment

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.payment.QrCodeResponse
import br.com.digio.uber.repository.payment.QrCodeRepository

class PostQrcodeReadUseCase(
    private val qrCodeRepository: QrCodeRepository
) : AbstractUseCase<String, QrCodeResponse>() {
    override suspend fun execute(param: String): QrCodeResponse =
        qrCodeRepository.postQrCode(param)
}