package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.AccountDomainResponse
import br.com.digio.uber.repository.transfer.TransferRepository

/**
 * @author Marlon D. Rocha
 * @since 04/11/20
 */
class GetAccountTedUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<Unit, AccountDomainResponse>() {

    override suspend fun execute(param: Unit): AccountDomainResponse =
        repository.getAccount()
}