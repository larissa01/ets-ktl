package br.com.digio.uber.domain.usecase.account

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.domain.exception.common.CredentialModelNotFoundException
import br.com.digio.uber.model.common.CredentialModel
import br.com.digio.uber.repository.account.AccountRepository

class GetCredentialsUseCase(
    private val accountRepository: AccountRepository
) : AbstractUseCase<Unit, CredentialModel>() {
    override suspend fun execute(param: Unit): CredentialModel =
        accountRepository.getCredential() ?: throw CredentialModelNotFoundException()
}