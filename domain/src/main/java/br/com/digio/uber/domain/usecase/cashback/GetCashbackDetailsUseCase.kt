package br.com.digio.uber.domain.usecase.cashback

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.cashback.CashbackDetailsResponse
import br.com.digio.uber.repository.cashback.CashbackRepository

class GetCashbackDetailsUseCase(
    private val cashbackRepository: CashbackRepository
) : AbstractUseCase<String, CashbackDetailsResponse>() {
    override suspend fun execute(param: String) =
        cashbackRepository.getCashbackDetails(param)
}