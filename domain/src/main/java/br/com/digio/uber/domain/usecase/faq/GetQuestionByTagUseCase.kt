package br.com.digio.uber.domain.usecase.faq

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.faq.FaqQuestionResponse
import br.com.digio.uber.repository.faq.FaqRepository

class GetQuestionByTagUseCase(val repository: FaqRepository) :
    AbstractUseCase<String, List<FaqQuestionResponse>>() {

    override suspend fun execute(param: String): List<FaqQuestionResponse> {
        return repository.getFaqCategories()
            .map { it.questions }
            .reduce { a, b -> a.union(b).toList() }
            .filter { question ->
                question.tag == param
            }
    }
}