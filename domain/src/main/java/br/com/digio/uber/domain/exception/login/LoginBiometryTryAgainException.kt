package br.com.digio.uber.domain.exception.login

class LoginBiometryTryAgainException : Exception()