package br.com.digio.uber.domain.usecase.card

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.card.CardResponse
import br.com.digio.uber.model.card.TypeCard
import br.com.digio.uber.repository.card.CardRepository

class GetCardTypeUseCase(
    private val cardRepository: CardRepository
) : AbstractUseCase<TypeCard, CardResponse>() {
    override suspend fun execute(param: TypeCard): CardResponse =
        cardRepository.getCardType(param)!!
}