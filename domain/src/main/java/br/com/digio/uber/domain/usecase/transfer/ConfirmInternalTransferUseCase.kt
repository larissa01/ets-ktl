package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.Beneficiary
import br.com.digio.uber.model.transfer.InternalTransferDomainRequest
import br.com.digio.uber.model.transfer.InternalTransferRequest
import br.com.digio.uber.model.transfer.TransferResponse
import br.com.digio.uber.repository.home.ConfigFeatureRepository
import br.com.digio.uber.repository.transfer.TransferRepository
import br.com.digio.uber.util.unmask
import br.com.digio.uber.util.unmaskMoney

class ConfirmInternalTransferUseCase(
    private val repository: TransferRepository,
    private val configFeatureRepository: ConfigFeatureRepository
) : AbstractUseCase<InternalTransferDomainRequest, TransferResponse>() {

    override suspend fun execute(param: InternalTransferDomainRequest): TransferResponse = with(param) {
        configFeatureRepository.clearHomeCache()
        repository.confirmTransferInternal(
            InternalTransferRequest(
                beneficiary = Beneficiary(
                    accountBranch = agency,
                    accountNumber = account.toInt().toString(),
                    cpf = cpf.unmask(),
                    name = name
                ),
                amount = amount.unmaskMoney(),
                saveBeneficiary = saveBeneficiary,
                comment = description,
                password = password,
                scheduleDate = date
            )
        )
    }
}