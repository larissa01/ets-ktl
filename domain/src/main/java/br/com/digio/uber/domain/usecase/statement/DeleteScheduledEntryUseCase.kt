package br.com.digio.uber.domain.usecase.statement

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.statement.StatementRepository

class DeleteScheduledEntryUseCase(
    private val statementRepository: StatementRepository
) : AbstractUseCase<String, Unit>() {
    override suspend fun execute(param: String): Unit =
        statementRepository.deleteScheduledEntry(param)
}