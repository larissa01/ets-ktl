package br.com.digio.uber.domain.usecase.bankSlip

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.bankSlip.GeneratedBankSlipResponse
import br.com.digio.uber.repository.bankSlip.BankSlipRepository

class GeneratedBankSlipUseCase(
    private val bankSlipRepository: BankSlipRepository
) : AbstractUseCase<String, GeneratedBankSlipResponse>() {
    override suspend fun execute(param: String): GeneratedBankSlipResponse =
        bankSlipRepository.getGeneratedBankSlips(param)
}