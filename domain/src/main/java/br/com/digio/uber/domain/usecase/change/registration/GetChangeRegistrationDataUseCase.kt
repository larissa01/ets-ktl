package br.com.digio.uber.domain.usecase.change.registration

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.change.registration.ChangeRegistrationData
import br.com.digio.uber.repository.account.AccountChangeRegistrationRepository
import br.com.digio.uber.repository.cache.CoCache
import br.com.digio.uber.repository.cache.HawkCache

class GetChangeRegistrationDataUseCase constructor(
    private val accountRepository: AccountChangeRegistrationRepository
) : AbstractUseCase<Unit, ChangeRegistrationData>(
    CoCache(HawkCache()),
    GetChangeRegistrationDataUseCase::class.java.name
) {
    override suspend fun execute(param: Unit): ChangeRegistrationData =
        accountRepository.getChangeRegistrationData()
}