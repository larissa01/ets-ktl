package br.com.digio.uber.domain.usecase.cashback

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.cashback.CashbackAmount
import br.com.digio.uber.repository.cashback.CashbackRepository

class GetAmountCashbackUseCase(
    private val cashbackRepository: CashbackRepository
) : AbstractUseCase<String, CashbackAmount>() {
    override suspend fun execute(param: String) =
        cashbackRepository.getPurchasesAmount(param)
}