package br.com.digio.uber.domain.usecase.withdraw

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.domain.Error
import br.com.digio.uber.domain.ResultWrapper
import br.com.digio.uber.domain.asFailure
import br.com.digio.uber.domain.exception.withdraw.NoNotesException
import br.com.digio.uber.model.withdraw.CustomWithdrawError
import br.com.digio.uber.model.withdraw.WithdrawReq
import br.com.digio.uber.repository.error.ServerException
import br.com.digio.uber.repository.error.tryConvertError
import br.com.digio.uber.repository.withdraw.WithdrawRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapNotNull

class DoWithdrawUseCase(
    private val withdrawRepository: WithdrawRepository
) : AbstractUseCase<WithdrawReq, Unit>() {
    override suspend fun execute(param: WithdrawReq) =
        withdrawRepository.makeWithdraw(param)

    override suspend fun onError(throwable: Throwable): ResultWrapper.Failure {
        return throwable.tryConvertError<CustomWithdrawError>().asFailure()
    }

    override fun invoke(value: WithdrawReq, forceLoad: Boolean?): Flow<ResultWrapper<Unit>> =
        super.invoke(value, forceLoad).mapNotNull { resultWrapper ->
            when (resultWrapper) {
                is ResultWrapper.Failure -> resolveErrorsFromCode(resultWrapper, value)
                else -> resultWrapper
            }
        }

    @Suppress("TooGenericExceptionCaught")
    private suspend fun resolveErrorsFromCode(
        result: ResultWrapper.Failure,
        value: WithdrawReq
    ): ResultWrapper.Failure {
        try {
            return if (result.error is Error.Server) {
                val cause = result.error.cause
                if (cause is ServerException) {
                    if (cause.error?.error?.code?.contains(CODE_NO_NOTES) == true) result.copy(
                        error = Error.Business(
                            message = cause.error?.error?.message,
                            cause = (cause.error?.error?.data as? CustomWithdrawError)?.let {
                                NoNotesException(it)
                            } ?: cause.cause
                        )
                    )
                    else result
                } else result
            } else result
        } catch (e: Throwable) {
            return ResultWrapper.Failure(Error.UnknownException(e))
        }
    }

    companion object {
        const val CODE_NO_NOTES = "AWWI-003"
    }
}