package br.com.digio.uber.domain.usecase.resetpassword

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.resetpassword.ResetPasswordReq
import br.com.digio.uber.model.resetpassword.ResetPasswordRes
import br.com.digio.uber.repository.login.AccountNotLoggedInRepository

class ResetPasswordUseCase constructor(
    private val accountNotLoggedInRepository: AccountNotLoggedInRepository
) : AbstractUseCase<ResetPasswordReq, ResetPasswordRes>() {
    override suspend fun execute(param: ResetPasswordReq): ResetPasswordRes =
        accountNotLoggedInRepository.resetPassword(param)
}