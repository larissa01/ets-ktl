package br.com.digio.uber.domain.usecase.login

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.domain.Error
import br.com.digio.uber.domain.ResultWrapper
import br.com.digio.uber.domain.asFailure
import br.com.digio.uber.domain.exception.login.LoginBiometryTryAgainException
import br.com.digio.uber.domain.exception.login.LoginChangePasswordException
import br.com.digio.uber.domain.exception.login.LoginFirstChangePasswordException
import br.com.digio.uber.domain.exception.login.LoginForbiddenBiometryRequestException
import br.com.digio.uber.domain.exception.login.LoginGemaltoException
import br.com.digio.uber.domain.exception.login.LoginLexisRejectException
import br.com.digio.uber.domain.exception.login.LoginMaxAttemptsException
import br.com.digio.uber.domain.exception.login.LoginUserOrPasswordWrongException
import br.com.digio.uber.model.error.ErrorResponse
import br.com.digio.uber.model.login.LoginRes
import br.com.digio.uber.model.login.PidTrackingInfo
import br.com.digio.uber.model.login.repository.model.Login
import br.com.digio.uber.repository.error.ServerException
import br.com.digio.uber.repository.error.tryConvertError
import br.com.digio.uber.repository.gemalto.GemaltoRepository
import br.com.digio.uber.util.castOrNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.withContext

abstract class AbstractMakeLoginErrorResolveUseCase constructor(
    private val gemaltoRepository: GemaltoRepository
) : AbstractUseCase<Login, LoginRes>() {

    override suspend fun onError(throwable: Throwable): ResultWrapper.Failure =
        throwable.tryConvertError<PidTrackingInfo>().asFailure()

    override fun invoke(value: Login, forceLoad: Boolean?): Flow<ResultWrapper<LoginRes>> =
        super.invoke(value, forceLoad).mapNotNull { resultWrapper ->
            when (resultWrapper) {
                is ResultWrapper.Failure -> resolveErrorsFromCode(resultWrapper, value)
                else -> resultWrapper
            }
        }

    @Suppress("TooGenericExceptionCaught")
    protected suspend fun resolveErrorsFromCode(
        result: ResultWrapper.Failure,
        login: Login
    ): ResultWrapper.Failure =
        try {
            result.error.castOrNull<Error.Server>()?.cause?.castOrNull<ServerException>()
                ?.let { cause ->
                    when {
                        cause.checkContainsCode(RA_002) -> result.copy(
                            error = Error.Business(
                                message = cause.error?.error?.message,
                                cause = getResponseErrorLexisLogin(result) ?: cause
                            )
                        )
                        checkIsPl006OrPl004(cause) -> result.copy(
                            error = Error.Business(
                                message = cause.error?.error?.message,
                                cause = LoginChangePasswordException()
                            )
                        )
                        cause.checkContainsCode(PL_007) -> result.copy(
                            error = Error.Business(
                                message = cause.error?.error?.message,
                                cause = LoginFirstChangePasswordException()
                            )
                        )
                        cause.checkContainsCode(PL_008) -> result.copy(
                            error = Error.Business(
                                message = cause.error?.error?.message,
                                cause = LoginMaxAttemptsException()
                            )
                        )
                        cause.checkContainsCode(PID_001) -> result.copy(
                            error = Error.Business(
                                message = cause.error?.error?.message,
                                cause = LoginBiometryTryAgainException()
                            )
                        )
                        cause.checkContainsCode(RA_001) -> result.copy(
                            error = Error.Business(
                                message = cause.error?.error?.message,
                                cause = LoginLexisRejectException()
                            )
                        )
                        cause.checkContainsCode(PL_001) -> result.copy(
                            error = Error.Business(
                                message = cause.error?.error?.message,
                                cause = LoginUserOrPasswordWrongException()
                            )
                        )
                        cause.checkContainsCode(TO_001) -> makeGemaltoException(
                            login,
                            result
                        )
                        else -> result
                    }
                } ?: result
        } catch (e: Throwable) {
            ResultWrapper.Failure(Error.UnknownException(e))
        }

    private fun checkIsPl006OrPl004(cause: ServerException) =
        cause.checkContainsCode(PL_006) || cause.checkContainsCode(PL_004)

    private suspend fun makeGemaltoException(
        login: Login,
        result: ResultWrapper.Failure
    ): ResultWrapper.Failure {
        clearGemaltoCredentials(login)

        return result.copy(
            error = Error.Business(
                cause = LoginGemaltoException()
            )
        )
    }

    internal suspend fun clearGemaltoCredentials(login: Login) {
        gemaltoRepository.clearTokenCpf(login.userName)
    }

    private suspend fun getResponseErrorLexisLogin(
        result: ResultWrapper.Failure
    ): LoginForbiddenBiometryRequestException? =
        withContext(Dispatchers.Default) {
            getResponseErrorLexisLoginFromJson(result)
                ?.let { responseErrorLexisLogin ->
                    LoginForbiddenBiometryRequestException(
                        responseErrorLexisLogin
                    )
                }
        }

    private fun getResponseErrorLexisLoginFromJson(result: ResultWrapper.Failure): ErrorResponse<PidTrackingInfo>? =
        result
            .error
            .castOrNull<Error.Server>()
            ?.cause
            ?.castToHttpOrServerError()

    private fun Throwable.castToHttpOrServerError(): ErrorResponse<PidTrackingInfo>? =
        castOrNull<ServerException>()
            ?.error
            ?.castOrNull<ErrorResponse<PidTrackingInfo>>()

    private fun ServerException.checkContainsCode(code: String): Boolean =
        error?.error?.code?.contains(code) == true

    companion object {
        private const val PL_001 = "PL-001"
        private const val RA_001 = "RA-001"
        private const val RA_002 = "RA-002"
        private const val PL_004 = "PL-004"
        private const val PL_006 = "PL-006"
        private const val PL_007 = "PL-007"
        private const val PL_008 = "PL-008"
        private const val TO_001 = "TO-001"
        private const val PID_001 = "PID-001"
    }
}