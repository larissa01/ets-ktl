package br.com.digio.uber.domain.usecase.login

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.change.password.ChangePasswordReq
import br.com.digio.uber.repository.login.AccountNotLoggedInRepository

class ChangePasswordUseCase constructor(
    private val accountNotLoggedInRepository: AccountNotLoggedInRepository
) : AbstractUseCase<ChangePasswordReq, Unit>() {
    override suspend fun execute(param: ChangePasswordReq) =
        accountNotLoggedInRepository.changePassword(param)
}