package br.com.digio.uber.domain.usecase.change.registration

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.change.registration.ZipCodeRes
import br.com.digio.uber.repository.account.AccountChangeRegistrationRepository

class GetZipCodeUseCase constructor(
    private val accountRepository: AccountChangeRegistrationRepository
) : AbstractUseCase<String, ZipCodeRes>() {
    override suspend fun execute(param: String): ZipCodeRes =
        accountRepository.getZipCode(param)
}