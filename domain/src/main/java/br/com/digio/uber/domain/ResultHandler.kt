package br.com.digio.uber.domain

import br.com.digio.uber.repository.error.ServerException
import br.com.digio.uber.util.isNetworkConnected
import java.net.HttpURLConnection

fun <T> T.asSuccess(): ResultWrapper.Success<T> = ResultWrapper.Success(this)

suspend fun Throwable.asFailure(): ResultWrapper.Failure = when (this) {
    is ServerException -> {
        when (code) {
            HttpURLConnection.HTTP_UNAUTHORIZED -> {
                if (error?.error?.code == null) {
                    ResultWrapper.Failure(
                        Error.Logout(this)
                    )
                } else {
                    resultResolve()
                }
            }
            in HttpURLConnection.HTTP_BAD_REQUEST until HttpURLConnection.HTTP_INTERNAL_ERROR -> {
                resultResolve()
            }
            else -> ResultWrapper.Failure(Error.UnknownException(this))
        }
    }
    else -> {
        if (isNetworkConnected()) {
            ResultWrapper.Failure(Error.UnknownException(this))
        } else {
            ResultWrapper.Failure(Error.NetworkException(this))
        }
    }
}

private fun ServerException.resultResolve(): ResultWrapper.Failure =
    ResultWrapper.Failure(
        Error.Server(
            code = code,
            message = error?.error?.message,
            cause = this
        )
    )