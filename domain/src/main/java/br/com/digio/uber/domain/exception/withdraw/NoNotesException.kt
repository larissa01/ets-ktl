package br.com.digio.uber.domain.exception.withdraw

import br.com.digio.uber.model.withdraw.CustomWithdrawError

class NoNotesException(
    val customWithdrawError: CustomWithdrawError
) : Exception()