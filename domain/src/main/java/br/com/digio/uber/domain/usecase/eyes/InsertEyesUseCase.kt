package br.com.digio.uber.domain.usecase.eyes

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.eyes.EyesRepository

class InsertEyesUseCase(
    private val eyesRepository: EyesRepository
) : AbstractUseCase<Boolean, Unit>() {
    override suspend fun execute(param: Boolean) {
        eyesRepository.insertEyesVisibility(param)
    }
}