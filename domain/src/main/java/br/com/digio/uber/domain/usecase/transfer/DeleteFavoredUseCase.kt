package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.BeneficiaryResponse
import br.com.digio.uber.repository.transfer.TransferRepository

class DeleteFavoredUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<String, BeneficiaryResponse>() {
    override suspend fun execute(param: String): BeneficiaryResponse =
        repository.deleteTedBeneficiary(param)
}

class DeleteP2PFavoredUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<String, BeneficiaryResponse>() {
    override suspend fun execute(param: String): BeneficiaryResponse =
        repository.deleteP2PBeneficiary(param)
}