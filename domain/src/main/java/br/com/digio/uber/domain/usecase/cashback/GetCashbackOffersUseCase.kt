package br.com.digio.uber.domain.usecase.cashback

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.cashback.CashbackOfferResponse
import br.com.digio.uber.repository.cashback.CashbackRepository

class GetCashbackOffersUseCase(
    private val cashbackRepository: CashbackRepository
) : AbstractUseCase<Long, ResponseList<CashbackOfferResponse>>() {
    override suspend fun execute(param: Long) =
        cashbackRepository.getStoreOffers(param)
}