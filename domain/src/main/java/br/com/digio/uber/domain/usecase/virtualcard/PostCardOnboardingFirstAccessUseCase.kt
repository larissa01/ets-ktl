package br.com.digio.uber.domain.usecase.virtualcard

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.repository.virtualcard.VirtualCardOnboardingRepository

class PostCardOnboardingFirstAccessUseCase constructor(
    private val repository: VirtualCardOnboardingRepository
) : AbstractUseCase<Boolean, Boolean>() {
    override suspend fun execute(param: Boolean): Boolean =
        repository.postFirstAccessOnboarding(param)
}