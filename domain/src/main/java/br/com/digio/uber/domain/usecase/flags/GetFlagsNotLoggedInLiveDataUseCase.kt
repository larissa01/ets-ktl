package br.com.digio.uber.domain.usecase.flags

import androidx.lifecycle.LiveData
import br.com.digio.uber.domain.AbstractLiveDataUseCase
import br.com.digio.uber.model.UberFlagsPersistNotLoggedIn
import br.com.digio.uber.repository.login.AccountNotLoggedInRepository

class GetFlagsNotLoggedInLiveDataUseCase constructor(
    private val accountNotLoggedInRepository: AccountNotLoggedInRepository
) : AbstractLiveDataUseCase<Unit, UberFlagsPersistNotLoggedIn>() {
    override fun execute(param: Unit): LiveData<UberFlagsPersistNotLoggedIn> =
        accountNotLoggedInRepository.getUberFlagsPersistNotLoggedInLiveData()
}