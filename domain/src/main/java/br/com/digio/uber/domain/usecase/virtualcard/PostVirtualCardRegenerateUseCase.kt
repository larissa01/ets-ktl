package br.com.digio.uber.domain.usecase.virtualcard

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.card.VirtualCardResponse
import br.com.digio.uber.repository.virtualcard.VirtualCardRepository

class PostVirtualCardRegenerateUseCase(
    private val virtualCardRepository: VirtualCardRepository
) : AbstractUseCase<Long, VirtualCardResponse>() {
    override suspend fun execute(param: Long): VirtualCardResponse =
        virtualCardRepository.postVirtualCardRegenerate(param)
}