package br.com.digio.uber.domain.usecase.store

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.store.StoreCatalog
import br.com.digio.uber.repository.store.StoreRepository

class GetStoreCatalogUseCase(
    private val storeRepository: StoreRepository
) : AbstractUseCase<Unit, StoreCatalog>() {
    override suspend fun execute(param: Unit) =
        storeRepository.getStoreCatalog()
}