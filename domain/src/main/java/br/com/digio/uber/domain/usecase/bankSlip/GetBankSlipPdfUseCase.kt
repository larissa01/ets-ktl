package br.com.digio.uber.domain.usecase.bankSlip

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.bankSlip.BankSlipPdfRequest
import br.com.digio.uber.model.bankSlip.FileResponse
import br.com.digio.uber.repository.bankSlip.BankSlipRepository

/**
 * @author Marlon D. Rocha
 * @since 28/10/20
 */
class GetBankSlipPdfUseCase(
    private val bankSlipRepository: BankSlipRepository
) : AbstractUseCase<BankSlipPdfRequest, FileResponse>() {

    override suspend fun execute(param: BankSlipPdfRequest): FileResponse =
        with(param) {
            bankSlipRepository.getBankSlipByUuid(uuid)
        }
}