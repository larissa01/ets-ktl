package br.com.digio.uber.domain.usecase.recharge

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.recharge.FavoriteContactList
import br.com.digio.uber.repository.recharge.RechargeRepository

class GetFavoritesContactsUseCase(
    private val rechargeRepository: RechargeRepository
) : AbstractUseCase<Unit, FavoriteContactList>() {
    override suspend fun execute(param: Unit) =
        rechargeRepository.getFavoritesContacts()
}