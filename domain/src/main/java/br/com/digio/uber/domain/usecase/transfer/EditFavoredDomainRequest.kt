package br.com.digio.uber.domain.usecase.transfer

data class EditFavoredDomainRequest(
    val registerType: String,
    val beneficiaryId: String,
    val accountNumber: String,
    val agencyCode: String,
    val transferAccountType: String
)