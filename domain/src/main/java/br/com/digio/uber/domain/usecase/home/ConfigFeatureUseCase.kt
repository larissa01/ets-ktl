package br.com.digio.uber.domain.usecase.home

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.config.FeatureItem
import br.com.digio.uber.repository.home.ConfigFeatureRepository

class ConfigFeatureUseCase(
    private val configFeatureRepository: ConfigFeatureRepository
) : AbstractUseCase<ConfigFeatureType, List<FeatureItem>>() {
    override suspend fun execute(param: ConfigFeatureType): List<FeatureItem> =
        when (param) {
            ConfigFeatureType.CONFIG_FEATURE_TYPE_FEATURE_LIST -> configFeatureRepository.getFeatureList()
            ConfigFeatureType.CONFIG_FEATURE_TYPE_TAG_LIST -> configFeatureRepository.getTagList()
            ConfigFeatureType.CONFIG_FEATURE_TYPE_SHORTCUT_LIST -> configFeatureRepository.getShortcutList()
            ConfigFeatureType.CONFIG_FEATURE_TYPE_SHORTCUT_TRACKING_CARD_LIST ->
                configFeatureRepository.getShortcutTrackingCardList()
        }
}

enum class ConfigFeatureType {
    CONFIG_FEATURE_TYPE_TAG_LIST,
    CONFIG_FEATURE_TYPE_FEATURE_LIST,
    CONFIG_FEATURE_TYPE_SHORTCUT_LIST,
    CONFIG_FEATURE_TYPE_SHORTCUT_TRACKING_CARD_LIST
}