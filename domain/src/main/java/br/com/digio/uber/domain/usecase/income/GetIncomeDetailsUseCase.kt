package br.com.digio.uber.domain.usecase.income

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.income.ListIncomesResponse
import br.com.digio.uber.repository.income.IncomeRepository

class GetIncomeDetailsUseCase(
    private val repository: IncomeRepository
) : AbstractUseCase<String, ListIncomesResponse>() {

    override suspend fun execute(param: String): ListIncomesResponse =
        repository.getIncomeDetails(param)
}