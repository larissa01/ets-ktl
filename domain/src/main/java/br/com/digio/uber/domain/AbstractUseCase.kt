package br.com.digio.uber.domain

import br.com.digio.uber.repository.cache.CoCache
import br.com.digio.uber.repository.error.tryConvertError
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext

abstract class AbstractUseCase<in PARAM, out RESPONSE> constructor(
    private val cache: CoCache<RESPONSE>? = null,
    private val key: String? = null
) {

    protected abstract suspend fun execute(param: PARAM): RESPONSE

    open suspend fun onError(throwable: Throwable): ResultWrapper.Failure =
        throwable.tryConvertError<Any>().asFailure()

    @Suppress("TooGenericExceptionCaught")
    open operator fun invoke(
        value: PARAM,
        forceLoad: Boolean? = false
    ): Flow<ResultWrapper<RESPONSE>> = flow {
        emit(ResultWrapper.Loading)
        try {
            val result = (
                cache?.get(
                    key,
                    forceLoad
                ) {
                    execute(value)
                }
                ) ?: withContext(IO) {
                execute(value)
            }
            emit(result.asSuccess())
        } catch (e: Throwable) {
            emit(onError(e))
        } finally {
            emit(ResultWrapper.DismissLoading)
        }
    }

    suspend fun clearThisCache(key: String?) {
        cache?.clearThisCache(key)
    }
}