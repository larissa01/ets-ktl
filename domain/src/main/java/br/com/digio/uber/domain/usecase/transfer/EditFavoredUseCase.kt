package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.BeneficiaryRequest
import br.com.digio.uber.model.transfer.BeneficiaryResponse
import br.com.digio.uber.repository.transfer.TransferRepository

class EditFavoredUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<EditFavoredDomainRequest, BeneficiaryResponse>() {

    override suspend fun execute(param: EditFavoredDomainRequest): BeneficiaryResponse {
        val beneficiaryRequest = BeneficiaryRequest(
            accountNumber = param.accountNumber,
            branch = param.agencyCode,
            transferAccountType = param.transferAccountType
        )
        return repository.putTedBeneficiary(param.beneficiaryId, beneficiaryRequest)
    }
}

class EditP2PFavoredUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<EditFavoredDomainRequest, BeneficiaryResponse>() {

    override suspend fun execute(param: EditFavoredDomainRequest): BeneficiaryResponse {
        val beneficiaryRequest = BeneficiaryRequest(
            accountNumber = param.accountNumber,
            branch = param.agencyCode,
            transferAccountType = param.transferAccountType
        )
        return repository.putP2PBeneficiary(param.beneficiaryId, beneficiaryRequest)
    }
}