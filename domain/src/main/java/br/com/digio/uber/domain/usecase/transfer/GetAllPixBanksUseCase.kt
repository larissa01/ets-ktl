package br.com.digio.uber.domain.usecase.transfer

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.transfer.PixBankDomainResponse
import br.com.digio.uber.repository.transfer.TransferRepository
import br.com.digio.uber.util.enumTest.CachePolicy

class GetAllPixBanksUseCase(
    private val repository: TransferRepository
) : AbstractUseCase<Unit, PixBankDomainResponse>() {

    override suspend fun execute(param: Unit): PixBankDomainResponse =
        repository.getPixBanks(CachePolicy.LOCAL_FIRST)
}