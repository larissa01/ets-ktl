package br.com.digio.uber.domain.usecase.cashback

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.cashback.CashbackTerms
import br.com.digio.uber.repository.cashback.CashbackRepository

class GetCashbackTermsUseCase(
    private val cashbackRepository: CashbackRepository
) : AbstractUseCase<Unit, CashbackTerms>() {
    override suspend fun execute(param: Unit) =
        cashbackRepository.getCashbackTerms()
}