package br.com.digio.uber.domain.usecase.cashback

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.repository.cashback.CashbackRepository

class GetCashbackStoresUseCase(
    private val cashbackRepository: CashbackRepository
) : AbstractUseCase<Int, ResponseList<CashbackStore>>() {
    override suspend fun execute(param: Int) =
        cashbackRepository.getCashbackStores(param)
}