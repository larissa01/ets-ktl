package br.com.digio.uber.domain.usecase.payment

import br.com.digio.uber.domain.AbstractUseCase
import br.com.digio.uber.model.payment.ConfirmPayResponse
import br.com.digio.uber.model.payment.PaymentBarcodeRequest
import br.com.digio.uber.repository.payment.AccountPaymentRepository

class PaymentAuthorizeUseCase(
    private val paymentRepository: AccountPaymentRepository
) : AbstractUseCase<Pair<String, PaymentBarcodeRequest>, ConfirmPayResponse>() {
    override suspend fun execute(param: Pair<String, PaymentBarcodeRequest>) =
        paymentRepository.paymentAuthorize(param.first, param.second)
}