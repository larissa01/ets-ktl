package br.com.digio.uber.domain.usecase.digiotest

import br.com.digio.uber.domain.ResultWrapper
import br.com.digio.uber.domain.usecase.digioTest.DigioTestUseCase
import br.com.digio.uber.model.digioTests.ProductsList
import br.com.digio.uber.repository.digioTestRepository.DigioTestRepository
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class DigioTestUseCaseTest {

    private val mockRepository: DigioTestRepository = mockk(relaxed = true)
    private val digioTestUseCase by lazy { DigioTestUseCase(mockRepository) }

    @Test
    @ExperimentalCoroutinesApi
    fun `on get list products return success`() = runBlocking {
        val expectedResponse = mockk<ProductsList>()
        coEvery { mockRepository.requestDigioTestPayload() } returns expectedResponse
        val response = digioTestUseCase.invoke(Unit).toList()

        Assert.assertTrue(response[0] is ResultWrapper.Loading)
        Assert.assertEquals(expectedResponse, (response[1] as ResultWrapper.Success).value)
        Assert.assertTrue(response[2] is ResultWrapper.DismissLoading)
        coVerify { mockRepository.requestDigioTestPayload() }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `on get list products return fail`() = runBlocking {
        coEvery { mockRepository.requestDigioTestPayload() } throws Exception()
        val response = digioTestUseCase.invoke(Unit).toList()

        Assert.assertTrue(response[0] is ResultWrapper.Loading)
        Assert.assertTrue(response[1] is ResultWrapper.Failure)
        Assert.assertTrue(response[2] is ResultWrapper.DismissLoading)
        coVerify(atLeast = 1) { mockRepository.requestDigioTestPayload() }
    }
}