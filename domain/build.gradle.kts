plugins {
    id("com.android.library")
    kotlin("android")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.library")
}

val lexisOrgIdHomol: String by project
val lexisOrgIdProd: String by project
val lexisFpServer: String by project

configureAndroidLibrary(
    releaseBuildTypeConfig = {
        buildConfigField("String", "LEXIS_ORG_ID", lexisOrgIdProd)
        buildConfigField("String", "LEXIS_FPSERVER", lexisFpServer)
        buildConfigField("Boolean", "ENABLE_CACHE_REQUEST", "true")
    },
    homolBuildTypeConfig = {
        buildConfigField("String", "LEXIS_ORG_ID", lexisOrgIdHomol)
        buildConfigField("String", "LEXIS_FPSERVER", lexisFpServer)
        buildConfigField("Boolean", "ENABLE_CACHE_REQUEST", "true")
    },
    debugBuildTypeConfig = {
        buildConfigField("String", "LEXIS_ORG_ID", lexisOrgIdHomol)
        buildConfigField("String", "LEXIS_FPSERVER", lexisFpServer)
        buildConfigField("Boolean", "ENABLE_CACHE_REQUEST", "true")
    }
)

dependencies {
    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.COROUTINES)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.RETROFIT)
    implementation(Dependencies.GSON)

    implementation("", "TMXProfilingConnections-6.0-138", ext = "aar")
    implementation("", "TMXProfiling-6.0-138", ext = "aar")

    testImplementation(TestDependencies.JUNIT)
    testImplementation(TestDependencies.MOCKK)
    testImplementation(TestDependencies.COROUTINESTEST)

    implementation(project(":model"))
    implementation(project(":repository"))
    implementation(project(":gemalto"))
    implementation(project(":util"))
}