package br.com.digio.uber.qrcode.activity

import br.com.digio.uber.common.base.activity.BaseJetNavigationActivity
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.qrcode.R
import br.com.digio.uber.qrcode.di.QrCodeModule
import org.koin.core.module.Module

class QRCodeActivity : BaseJetNavigationActivity() {
    override fun getLayoutId(): Int? = LayoutIds.navigationLayoutActivity
    override fun navGraphId(): Int? = R.navigation.nav_qrcod
    override fun getModule(): List<Module>? = listOf(QrCodeModule)
    override fun onBackPressed() { finish() }
}