package br.com.digio.uber.qrcode.fragment

import android.app.Activity
import android.content.Intent
import android.view.View
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.qrcode.BR
import br.com.digio.uber.qrcode.R
import br.com.digio.uber.qrcode.databinding.ReadQrCodeFragmentBinding
import br.com.digio.uber.qrcode.viewmodel.ReadQrCodeViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.safeLet
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import kotlinx.android.synthetic.main.read_qr_code_fragment.*
import org.koin.android.ext.android.inject

class ReadQrCodeFragment :
    BaseViewModelFragment<ReadQrCodeFragmentBinding, ReadQrCodeViewModel>() {
    override val bindingVariable: Int? = BR.readQrCodeViewModel
    override val getLayoutId: Int? = R.layout.read_qr_code_fragment
    override val viewModel: ReadQrCodeViewModel by inject()
    private val message: String by lazy {
        ReadQrCodeFragmentArgs
            .fromBundle(checkNotNull(activity?.intent?.extras)).messageqrcode
    }

    override fun tag(): String = ReadQrCodeFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.BLACK

    override fun initialize() {
        super.initialize()
        viewModel.initializer(message) { onItemClicked(it) }
        onStartQrCode()
    }

    override fun onPause() {
        onPauseQrCode()
        super.onPause()
    }

    override fun onStart() {
        super.onStart()
        barcodeViewTransferCode.decodeContinuous(
            object : BarcodeCallback {
                override fun barcodeResult(result: BarcodeResult?) {
                    safeLet(result?.text, activity) { resultText, activityLet ->
                        activityLet.setResult(
                            Activity.RESULT_OK,
                            Intent().apply {
                                putExtra(Const.Extras.RESULT_QR_CODE_VALUE, resultText)
                            }
                        )
                        activityLet.finish()
                    }
                }

                @Suppress("EmptyFunctionBlock")
                override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
                }
            }
        )
    }

    fun onPauseQrCode() {
        if (barcodeViewTransferCode != null) {
            barcodeViewTransferCode.pause()
        }
    }

    fun onStartQrCode() {
        if (barcodeViewTransferCode != null) {
            barcodeViewTransferCode.resume()
        }
    }

    override fun onResume() {
        onStartQrCode()
        super.onResume()
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.txtQrcodeCancel -> {
                activity?.let { activityLet ->
                    Activity.RESULT_CANCELED
                    activityLet.finish()
                }
            }
        }
    }
}