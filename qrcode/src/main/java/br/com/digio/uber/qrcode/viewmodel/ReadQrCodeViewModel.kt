package br.com.digio.uber.qrcode.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.OnClickItem

class ReadQrCodeViewModel : BaseViewModel() {

    val messageOnScreen = MutableLiveData<String>()

    fun initializer(message: String, onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        messageOnScreen.value = message
    }
}