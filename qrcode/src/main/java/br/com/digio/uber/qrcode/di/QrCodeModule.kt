package br.com.digio.uber.qrcode.di

import br.com.digio.uber.qrcode.viewmodel.ReadQrCodeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val QrCodeModule = module {
    viewModel { ReadQrCodeViewModel() }
}