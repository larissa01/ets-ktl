package br.com.digio.uber.dataset.digioTest

import br.com.digio.uber.gateway.client.DigioTestClient
import br.com.digio.uber.gateway.endpoint.DigioTestEndpoints
import br.com.digio.uber.model.digioTests.ProductsList
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import retrofit2.HttpException

class DigioTestDataSetTest {

    private val sampleGatewayClient: DigioTestClient = mockk()

    private val sampleDataset: DigioTestDataSet = DigioTestDataSetImpl(sampleGatewayClient)

    @ExperimentalCoroutinesApi
    @Test
    fun `on execute getProducts and return products values`() = runBlockingTest {
        val digioTestEndpoints: DigioTestEndpoints = mockk()
        val products: ProductsList = mockk()

        every { sampleGatewayClient.getDigioTestEndpoint() } returns digioTestEndpoints
        coEvery { digioTestEndpoints.getProducts() } returns products

        val result = sampleDataset.getProducts()

        assertNotNull(result)

        verify { sampleGatewayClient.getDigioTestEndpoint() }
        coVerify { digioTestEndpoints.getProducts() }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `on execute getProducts and return http exception`() = runBlockingTest {
        val digioTestEndpoints: DigioTestEndpoints = mockk()
        val httpException: HttpException = mockk()

        every { sampleGatewayClient.getDigioTestEndpoint() } returns digioTestEndpoints
        coEvery { digioTestEndpoints.getProducts() } throws httpException

        try {
            val result = sampleDataset.getProducts()
            assertNull(result)
        } catch (http: HttpException) {
            assertNotNull(http)
        } catch (e: Throwable) {
            assertNull(e)
        }

        verify { sampleGatewayClient.getDigioTestEndpoint() }
        coVerify { digioTestEndpoints.getProducts() }
    }
}