package br.com.digio.uber.dataset.faq

import br.com.digio.uber.model.faq.FaqChatResponse
import br.com.digio.uber.model.faq.FaqChatUrlRequest
import br.com.digio.uber.model.faq.FaqResponse

interface FaqDataset {
    suspend fun getFaq(faqVersion: String): FaqResponse
    suspend fun getFaqChatUrl(faqChatRequestBody: FaqChatUrlRequest): FaqChatResponse
}