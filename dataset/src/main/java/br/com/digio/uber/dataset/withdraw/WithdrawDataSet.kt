package br.com.digio.uber.dataset.withdraw

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.bankSlip.CustomerLimitsResponse
import br.com.digio.uber.model.withdraw.WithdrawReq
import retrofit2.HttpException

interface WithdrawDataSet {
    suspend fun getWithdrawStatus(): CustomerLimitsResponse
    suspend fun makeWithdraw(withDrawRequest: WithdrawReq)
}

class WithdrawDataSetImpl(
    val client: UberClient,
) : WithdrawDataSet {
    override suspend fun getWithdrawStatus(): CustomerLimitsResponse =
        client.getUberLoggedEndpoint().getWithdrawStatus()

    override suspend fun makeWithdraw(withDrawRequest: WithdrawReq) {
        val response = client.getUberLoggedEndpoint().doWithdraw(withDrawRequest)
        if (!response.isSuccessful) {
            throw HttpException(response)
        }
    }
}