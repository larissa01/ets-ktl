package br.com.digio.uber.dataset.login

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.change.password.ChangePasswordAuthReq

class LoginLoggedDatasetImpl constructor(
    private val uberClient: UberClient
) : LoginLoggedDataset {
    override suspend fun changePassword(changePasswordAuthReq: ChangePasswordAuthReq) {
        uberClient.getUberLoggedEndpoint().changePasswordAuth(changePasswordAuthReq)
    }
}