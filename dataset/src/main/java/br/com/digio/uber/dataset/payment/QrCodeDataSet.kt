package br.com.digio.uber.dataset.payment

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.payment.PayWithQrCodeRequest
import br.com.digio.uber.model.payment.QrCodeRequest
import br.com.digio.uber.model.payment.QrCodeResponse
import retrofit2.Response

interface QrCodeDataSet {
    suspend fun postQrCode(qrCodeRequest: QrCodeRequest): QrCodeResponse
    suspend fun postPayWithQrCode(qrCodeRequest: PayWithQrCodeRequest, hash: String): Response<Void>
}

class QrCodeDataSetImpl(
    private val uberClient: UberClient
) : QrCodeDataSet {

    override suspend fun postQrCode(qrCodeRequest: QrCodeRequest): QrCodeResponse =
        uberClient.getUberLoggedEndpoint().postQrCode(qrCodeRequest)

    override suspend fun postPayWithQrCode(qrCodeRequest: PayWithQrCodeRequest, hash: String): Response<Void> =
        uberClient.getUberLoggedEndpoint().postPayWithQrCode(qrCodeRequest, hash)
}