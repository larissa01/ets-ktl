package br.com.digio.uber.dataset.login

import br.com.digio.uber.model.change.password.ChangePasswordReq
import br.com.digio.uber.model.login.LoginReq
import br.com.digio.uber.model.login.LoginRes
import br.com.digio.uber.model.resetpassword.ResetPasswordReq
import br.com.digio.uber.model.resetpassword.ResetPasswordRes

interface LoginNotLoggedInDataset {
    suspend fun makeLogin(req: LoginReq): LoginRes
    suspend fun resetPassword(req: ResetPasswordReq): ResetPasswordRes
    suspend fun changePassword(changePasswordReq: ChangePasswordReq)
}