package br.com.digio.uber.dataset.cashback

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.cashback.CashbackAmount
import br.com.digio.uber.model.cashback.CashbackComissionCategory
import br.com.digio.uber.model.cashback.CashbackCustomerResponse
import br.com.digio.uber.model.cashback.CashbackDetailsResponse
import br.com.digio.uber.model.cashback.CashbackOfferResponse
import br.com.digio.uber.model.cashback.CashbackPurchase
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.model.cashback.CashbackTerms
import br.com.digio.uber.model.cashback.SendCashbackRequest
import retrofit2.Response

interface CashbackDataSet {
    suspend fun getPurchasesAmount(status: String): CashbackAmount
    suspend fun getCashbackStores(quantityItens: Int): ResponseList<CashbackStore>
    suspend fun getCashbackTerms(): CashbackTerms
    suspend fun acceptCashbackTerms(): Response<Void>
    suspend fun getCashbackTermsAccept(): Response<Void>
    suspend fun getCashbackCustomer(): CashbackCustomerResponse
    suspend fun getCashbackDetails(type: String): CashbackDetailsResponse
    suspend fun getStoreOffers(storeId: Long): ResponseList<CashbackOfferResponse>
    suspend fun getComissionCategory(id: Long): ResponseList<CashbackComissionCategory>
    suspend fun getCashbackPurchases(status: String, page: Int): ResponseList<CashbackPurchase>
    suspend fun sendCashbackAccount(receiptForm: String, purchases: SendCashbackRequest): Response<Void>
}

class CashbackDataSetImpl(
    private val uberClient: UberClient
) : CashbackDataSet {

    override suspend fun getPurchasesAmount(status: String) =
        uberClient.getUberLoggedEndpoint().getPurchasesAmount(status)

    override suspend fun getCashbackStores(quantityItens: Int) =
        uberClient.getUberLoggedEndpoint().getCashbackStores(quantityItens)

    override suspend fun getCashbackTermsAccept() =
        uberClient.getUberLoggedEndpoint().getCashbackTermsAccept()

    override suspend fun acceptCashbackTerms() =
        uberClient.getUberLoggedEndpoint().acceptCashbackTerms()

    override suspend fun getCashbackTerms() =
        uberClient.getUberLoggedEndpoint().getCashbackTerms()

    override suspend fun getCashbackCustomer() =
        uberClient.getUberLoggedEndpoint().getCashbackCustomer()

    override suspend fun getCashbackDetails(type: String) =
        uberClient.getUberLoggedEndpoint().getCashbackDetails(type)

    override suspend fun getStoreOffers(storeId: Long) =
        uberClient.getUberLoggedEndpoint().getStoreOffers(storeId)

    override suspend fun getComissionCategory(id: Long) =
        uberClient.getUberLoggedEndpoint().getComissionCategory(id)

    override suspend fun getCashbackPurchases(status: String, page: Int) =
        uberClient.getUberLoggedEndpoint().getCashbackPurchases(status, page)

    override suspend fun sendCashbackAccount(receiptForm: String, purchases: SendCashbackRequest) =
        uberClient.getUberLoggedEndpoint().sendCashbackAccount(receiptForm, purchases)
}