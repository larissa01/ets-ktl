package br.com.digio.uber.dataset.faq

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.faq.FaqChatResponse
import br.com.digio.uber.model.faq.FaqChatUrlRequest
import br.com.digio.uber.model.faq.FaqResponse

class FaqDatasetImpl(
    private val api: UberClient
) : FaqDataset {

    override suspend fun getFaq(faqVersion: String): FaqResponse {
        return api.getUberLoggedEndpoint().getFaq(faqVersion)
    }

    override suspend fun getFaqChatUrl(faqChatRequestBody: FaqChatUrlRequest): FaqChatResponse {
        return api.getUberLoggedEndpoint().getFaqChatUrl(faqChatRequestBody)
    }
}