package br.com.digio.uber.dataset.statement

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.statement.FutureEntriesResponse
import br.com.digio.uber.model.statement.PastEntriesRequest
import br.com.digio.uber.model.statement.PastEntriesResponse
import br.com.digio.uber.model.statement.StatementDetailResponse
import retrofit2.HttpException
import java.net.HttpURLConnection.HTTP_NO_CONTENT

class StatementDataSetImpl constructor(
    val client: UberClient
) : StatementDataSet {

    override suspend fun getPastEntries(req: PastEntriesRequest): PastEntriesResponse =
        client.getUberLoggedEndpoint().getStatement(
            req.startDate,
            req.endDate,
            req.days,
            req.lastDate
        )

    override suspend fun getFutureEntries(): FutureEntriesResponse =
        client.getUberLoggedEndpoint().getFutureStatement()

    override suspend fun deleteScheduledEntry(param: String) {
        val response = client.getUberLoggedEndpoint().deleteScheduledEntry(param)

        if (response.code() != HTTP_NO_CONTENT) {
            throw HttpException(response)
        }
    }

    override suspend fun getStatementDetails(id: String): StatementDetailResponse =
        client.getUberLoggedEndpoint().getStatementDetails(id)
}