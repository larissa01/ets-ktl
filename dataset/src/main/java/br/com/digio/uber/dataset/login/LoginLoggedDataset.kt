package br.com.digio.uber.dataset.login

import br.com.digio.uber.model.change.password.ChangePasswordAuthReq

interface LoginLoggedDataset {
    suspend fun changePassword(changePasswordAuthReq: ChangePasswordAuthReq)
}