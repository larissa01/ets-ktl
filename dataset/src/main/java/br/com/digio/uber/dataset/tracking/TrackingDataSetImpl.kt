package br.com.digio.uber.dataset.tracking

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.tracking.TrackingLogResponse

class TrackingDataSetImpl constructor(
    val client: UberClient
) : TrackingDataSet {
    override suspend fun getTrackingLog(cardId: Long): TrackingLogResponse =
        client.getUberLoggedEndpoint().getTrackingLog(cardId)
}