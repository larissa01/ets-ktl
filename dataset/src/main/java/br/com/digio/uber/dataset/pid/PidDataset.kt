package br.com.digio.uber.dataset.pid

import br.com.digio.uber.model.pid.PidReq
import br.com.digio.uber.model.pid.PidRes
import br.com.digio.uber.model.pid.PidStepReq

interface PidDataset {
    suspend fun firstPid(pidReq: PidReq): PidRes
    suspend fun pidValidate(pidRes: PidStepReq): PidRes
}