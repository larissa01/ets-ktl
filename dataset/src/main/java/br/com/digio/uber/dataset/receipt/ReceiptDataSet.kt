package br.com.digio.uber.dataset.receipt

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.receipt.Receipt

interface ReceiptDataSet {
    suspend fun getReceipt(id: String): Receipt
}

class ReceiptDataSetImpl(
    private val uberClient: UberClient
) : ReceiptDataSet {
    override suspend fun getReceipt(id: String): Receipt {
        return uberClient.getUberLoggedEndpoint().getReceipt(id)
    }
}