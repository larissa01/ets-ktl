package br.com.digio.uber.dataset.remuneration

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.remuneration.IncomeIndexResponse

interface RemunerationDataSet {
    suspend fun getIncome(): IncomeIndexResponse
}

class RemunerationDataSetImpl(
    private val uberClient: UberClient
) : RemunerationDataSet {
    override suspend fun getIncome(): IncomeIndexResponse =
        uberClient.getUberLoggedEndpoint().getIncome()
}