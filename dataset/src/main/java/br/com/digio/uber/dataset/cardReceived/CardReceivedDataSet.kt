package br.com.digio.uber.dataset.cardReceived

import br.com.digio.uber.model.cardReceived.CardReceivedActivationRequest
import retrofit2.Response

interface CardReceivedDataSet {
    suspend fun postValidateCardActivation(activationRequest: CardReceivedActivationRequest):
        Response<Void>
}