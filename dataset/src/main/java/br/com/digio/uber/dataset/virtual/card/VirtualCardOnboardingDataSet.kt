package br.com.digio.uber.dataset.virtual.card

import br.com.digio.uber.model.virtualCard.VirtualCardOnboardingResponse

interface VirtualCardOnboardingDataSet {
    suspend fun getOnboardingContent(): VirtualCardOnboardingResponse
}