package br.com.digio.uber.dataset.account

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.account.AccountResponse
import br.com.digio.uber.model.account.AvatarRequest
import br.com.digio.uber.model.account.AvatarResponse
import br.com.digio.uber.model.change.registration.AddressRegistrationRequest
import br.com.digio.uber.model.change.registration.ChangeRegistrationData
import br.com.digio.uber.model.change.registration.ChangeRegistrationRequest
import br.com.digio.uber.model.change.registration.ZipCodeRes
import br.com.digio.uber.model.gemalto.Enroll
import br.com.digio.uber.model.gemalto.EnrollRequest
import br.com.digio.uber.model.login.refresh.req.RefreshReq
import br.com.digio.uber.model.request.DeviceInfo
import br.com.digio.uber.model.show.card.password.CardPasswordReq
import br.com.digio.uber.model.show.card.password.CardPasswordRes
import retrofit2.HttpException

interface AccountDataSet {
    suspend fun getAccount(): AccountResponse
    suspend fun getProfileAvatar(): AvatarResponse
    suspend fun getThirdPartyProfileAvatar(documentId: String): AvatarResponse
    suspend fun putProfileAvatar(avatarRequest: AvatarRequest)
    suspend fun getUserEnroll(deviceInfo: DeviceInfo): Enroll
    suspend fun getCardPassword(cardPasswordReq: CardPasswordReq): CardPasswordRes
    suspend fun getChangeRegistrationData(): ChangeRegistrationData
    suspend fun getZipCode(cep: String): ZipCodeRes
    suspend fun changeRegistration(changeRegistrationItemList: ChangeRegistrationRequest)
    suspend fun updateAddress(param: AddressRegistrationRequest)
    suspend fun logout(refreshReq: RefreshReq)
}

class AccountDataSetImpl(
    private val uberClient: UberClient
) : AccountDataSet {
    override suspend fun getAccount(): AccountResponse =
        uberClient.getUberLoggedEndpoint().getAccount()

    override suspend fun getProfileAvatar(): AvatarResponse =
        uberClient.getUberLoggedEndpoint().getProfileAvatar()

    override suspend fun getThirdPartyProfileAvatar(documentId: String): AvatarResponse =
        uberClient.getUberLoggedEndpoint().getThirdPartyProfileAvatar(documentId)

    override suspend fun putProfileAvatar(avatarRequest: AvatarRequest) =
        uberClient.getUberLoggedEndpoint().putProfileAvatar(avatarRequest)

    override suspend fun getUserEnroll(deviceInfo: DeviceInfo): Enroll =
        uberClient.getUberLoggedEndpoint().getUserEnroll(EnrollRequest(deviceInfo))

    override suspend fun getCardPassword(cardPasswordReq: CardPasswordReq): CardPasswordRes =
        uberClient.getUberLoggedEndpoint().showCardPassword(cardPasswordReq)

    override suspend fun getChangeRegistrationData(): ChangeRegistrationData =
        uberClient.getUberLoggedEndpoint().getRegistrationList()

    override suspend fun getZipCode(cep: String): ZipCodeRes =
        uberClient.getUberLoggedEndpoint().getZipCode(cep)

    override suspend fun changeRegistration(changeRegistrationItemList: ChangeRegistrationRequest) {
        val request =
            uberClient.getUberLoggedEndpoint().changeRegistration(changeRegistrationItemList)
        if (!request.isSuccessful) {
            throw HttpException(request)
        }
    }

    override suspend fun updateAddress(param: AddressRegistrationRequest) {
        val request = uberClient.getUberLoggedEndpoint().updateAddress(param)
        if (!request.isSuccessful) {
            throw HttpException(request)
        }
    }

    override suspend fun logout(refreshReq: RefreshReq) {
        uberClient.getUberLoggedEndpoint().logout(refreshReq)
    }
}