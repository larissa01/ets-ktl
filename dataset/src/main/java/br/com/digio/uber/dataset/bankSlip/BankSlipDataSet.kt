package br.com.digio.uber.dataset.bankSlip

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.bankSlip.BankSlipRequest
import br.com.digio.uber.model.bankSlip.BankSlipResponse
import br.com.digio.uber.model.bankSlip.BankSlipStatus
import br.com.digio.uber.model.bankSlip.FileResponse
import br.com.digio.uber.model.bankSlip.GeneratedBankSlipResponse

interface BankSlipDataSet {
    suspend fun getBankSlipStatus(): BankSlipStatus
    suspend fun getBankSlipByUuid(uuid: String): FileResponse
    suspend fun generateBankSlip(bankSlipRequest: BankSlipRequest): BankSlipResponse
    suspend fun getGeneratedBankSlips(status: String?): GeneratedBankSlipResponse
}

class BankSlipDataSetImpl(
    private val uberClient: UberClient
) : BankSlipDataSet {

    override suspend fun getBankSlipStatus(): BankSlipStatus =
        uberClient.getUberLoggedEndpoint().getBankSlipStatus()

    override suspend fun getBankSlipByUuid(uuid: String): FileResponse =
        uberClient.getUberLoggedEndpoint().getBankSlipByUuid(uuid)

    override suspend fun generateBankSlip(bankSlipRequest: BankSlipRequest): BankSlipResponse =
        uberClient.getUberLoggedEndpoint().generateBankSlip(bankSlipRequest)

    override suspend fun getGeneratedBankSlips(status: String?): GeneratedBankSlipResponse =
        uberClient.getUberLoggedEndpoint().getGeneratedBankSlips(status)
}