package br.com.digio.uber.dataset.login

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.change.password.ChangePasswordReq
import br.com.digio.uber.model.login.LoginReq
import br.com.digio.uber.model.login.LoginRes
import br.com.digio.uber.model.resetpassword.ResetPasswordReq
import br.com.digio.uber.model.resetpassword.ResetPasswordRes

class LoginNotLoggedInDatasetImpl constructor(
    private val uberClient: UberClient
) : LoginNotLoggedInDataset {

    override suspend fun makeLogin(req: LoginReq): LoginRes =
        uberClient.getUberNotLoggedInEndpoint().makeLogin(req)

    override suspend fun resetPassword(req: ResetPasswordReq): ResetPasswordRes =
        uberClient.getUberNotLoggedInEndpoint().forgotPassword(req)

    override suspend fun changePassword(changePasswordReq: ChangePasswordReq) {
        uberClient.getUberNotLoggedInEndpoint().changePassword(changePasswordReq)
    }
}