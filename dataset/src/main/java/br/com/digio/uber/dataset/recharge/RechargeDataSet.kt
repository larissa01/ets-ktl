package br.com.digio.uber.dataset.recharge

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.recharge.FavoriteContact
import br.com.digio.uber.model.recharge.FavoriteContactList
import br.com.digio.uber.model.recharge.RechargeValues

interface RechargeDataSet {
    suspend fun getOperatorPrices(provider: String): RechargeValues
    suspend fun getFavoritesContacts(): FavoriteContactList
    suspend fun registerFavoriteContact(favoriteContact: FavoriteContact): Unit
    suspend fun updateFavoriteContact(favoriteContact: FavoriteContact): Unit
    suspend fun removeFavoriteContact(favoriteContact: FavoriteContact): Unit
}

class RechargeDataSetImpl(
    private val uberClient: UberClient
) : RechargeDataSet {

    override suspend fun getOperatorPrices(provider: String) =
        uberClient.getUberLoggedEndpoint().getOperatorPrices(provider)

    override suspend fun getFavoritesContacts() =
        uberClient.getUberLoggedEndpoint().getFavoritesContacts()

    override suspend fun registerFavoriteContact(favoriteContact: FavoriteContact) =
        uberClient.getUberLoggedEndpoint().registerFavoriteContact(favoriteContact)

    override suspend fun updateFavoriteContact(favoriteContact: FavoriteContact) =
        uberClient.getUberLoggedEndpoint().updateFavoriteContact(favoriteContact)

    override suspend fun removeFavoriteContact(favoriteContact: FavoriteContact) =
        uberClient.getUberLoggedEndpoint().removeFavoriteContact(favoriteContact)
}