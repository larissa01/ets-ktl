package br.com.digio.uber.dataset.income

import br.com.digio.uber.model.income.AvailableMonthsResponse
import br.com.digio.uber.model.income.DetailDepositResponse
import br.com.digio.uber.model.income.ListIncomesResponse

interface IncomeDataSet {
    suspend fun getAvailableMonths(): AvailableMonthsResponse
    suspend fun getIncomeDetails(yearMonth: String): ListIncomesResponse
    suspend fun getDepositDetails(depositId: Int): DetailDepositResponse
}