package br.com.digio.uber.dataset.income

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.income.AvailableMonthsResponse
import br.com.digio.uber.model.income.DetailDepositResponse
import br.com.digio.uber.model.income.ListIncomesResponse

class IncomeDataSetImpl(
    private val uberClient: UberClient
) : IncomeDataSet {

    override suspend fun getAvailableMonths(): AvailableMonthsResponse =
        uberClient.getUberLoggedEndpoint().getAvailableMonths()

    override suspend fun getIncomeDetails(yearMonth: String): ListIncomesResponse =
        uberClient.getUberLoggedEndpoint().getIncomeDetails(yearMonth)

    override suspend fun getDepositDetails(depositId: Int): DetailDepositResponse =
        uberClient.getUberLoggedEndpoint().getDepositDetails(depositId)
}