package br.com.digio.uber.dataset.balance

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.BalanceResponse

interface BalanceDataSet {
    suspend fun getBalance(): BalanceResponse
}

class BalanceDataSetImpl(
    private val uberClient: UberClient
) : BalanceDataSet {
    override suspend fun getBalance(): BalanceResponse =
        uberClient.getUberLoggedEndpoint().getBalance()
}