package br.com.digio.uber.dataset.card

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.cardreissue.LostStolenRequest
import br.com.digio.uber.model.cardreissue.LostStolenResponse

interface CardReissueDataSet {
    suspend fun postLossAndTheft(productName: String, lostStolenRequest: LostStolenRequest): LostStolenResponse
}

class CardReissueDataSetImpl(
    private val uberClient: UberClient
) : CardReissueDataSet {
    override suspend fun postLossAndTheft(
        productName: String,
        lostStolenRequest: LostStolenRequest
    ): LostStolenResponse =
        uberClient.getUberLoggedEndpoint().postTheftlost(productName, lostStolenRequest)
}