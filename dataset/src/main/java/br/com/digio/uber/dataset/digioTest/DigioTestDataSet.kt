package br.com.digio.uber.dataset.digioTest

import br.com.digio.uber.gateway.client.DigioTestClient
import br.com.digio.uber.model.digioTests.ProductsList
import okhttp3.ResponseBody
import retrofit2.Response

interface DigioTestDataSet {
    suspend fun getProducts(): ProductsList
    suspend fun getValues(): Response<ResponseBody>
}

class DigioTestDataSetImpl(
    private val digioTestClient: DigioTestClient
) : DigioTestDataSet {
    override suspend fun getProducts(): ProductsList =
        digioTestClient.getDigioTestEndpoint().getProducts()

    override suspend fun getValues(): Response<ResponseBody> =
        digioTestClient.getDigioTestEndpoint().getValues()
}