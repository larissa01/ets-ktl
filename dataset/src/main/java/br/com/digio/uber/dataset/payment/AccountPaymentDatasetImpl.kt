package br.com.digio.uber.dataset.payment

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.payment.BillBarcode
import br.com.digio.uber.model.payment.ConfirmPayResponse
import br.com.digio.uber.model.payment.PaymentBarcodeRequest

class AccountPaymentDatasetImpl(
    private val uberClient: UberClient
) : AccountPaymentDataset {

    override suspend fun checkBillBarcode(barcode: String): BillBarcode =
        uberClient.getUberLoggedEndpoint().getBillBarcodeInformation(barcode)

    override suspend fun paymentAuthorize(pidHash: String, request: PaymentBarcodeRequest): ConfirmPayResponse =
        uberClient.getUberLoggedEndpoint().paymentAuthorize(pidHash, request)
}