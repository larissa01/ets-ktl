package br.com.digio.uber.dataset.virtual.card

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.card.VirtualCardResponse
import retrofit2.HttpException
import retrofit2.Response

interface VirtualCardDataSet {
    suspend fun getVirtualCard(cardId: Long, hash: String): VirtualCardResponse
    suspend fun postVirtualCardRegenerate(cardId: Long): VirtualCardResponse
    suspend fun postVirtualCardBlock(cardId: Long): Response<Void>
    suspend fun postVirtualCardUnblock(cardId: Long): Response<Void>
}

class VirtualCardDataSetImpl(
    private val uberClient: UberClient
) : VirtualCardDataSet {

    override suspend fun getVirtualCard(cardId: Long, hash: String): VirtualCardResponse =
        uberClient.getUberLoggedEndpoint().getVirtualCard(cardId, hash)

    override suspend fun postVirtualCardRegenerate(cardId: Long): VirtualCardResponse =
        uberClient.getUberLoggedEndpoint().postVirtualCardRegenerate(cardId)

    override suspend fun postVirtualCardBlock(cardId: Long): Response<Void> =
        uberClient.getUberLoggedEndpoint().postVirtualCardBlock(cardId).apply {
            if (!this.isSuccessful) {
                throw HttpException(this)
            }
        }

    override suspend fun postVirtualCardUnblock(cardId: Long): Response<Void> =
        uberClient.getUberLoggedEndpoint().postVirtualCardunblock(cardId).apply {
            if (!this.isSuccessful) {
                throw HttpException(this)
            }
        }
}