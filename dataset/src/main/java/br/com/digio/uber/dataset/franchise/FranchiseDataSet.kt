package br.com.digio.uber.dataset.franchise

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.franchise.FranchiseResponse

interface FranchiseDataSet {
    suspend fun getFranchise(): FranchiseResponse
}

class FranchiseDataSetImpl(private val uberClient: UberClient) : FranchiseDataSet {
    override suspend fun getFranchise(): FranchiseResponse {
        return uberClient.getUberLoggedEndpoint().getFranchise()
    }
}