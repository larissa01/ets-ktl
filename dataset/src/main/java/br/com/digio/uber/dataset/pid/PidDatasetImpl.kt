package br.com.digio.uber.dataset.pid

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.pid.PidReq
import br.com.digio.uber.model.pid.PidRes
import br.com.digio.uber.model.pid.PidStepReq

class PidDatasetImpl constructor(
    private val uberClient: UberClient
) : PidDataset {
    override suspend fun firstPid(pidReq: PidReq): PidRes =
        uberClient.getUberLoggedEndpoint().firstPid(pidReq)

    override suspend fun pidValidate(pidRes: PidStepReq): PidRes =
        uberClient.getUberLoggedEndpoint().pidValidate(pidRes)
}