package br.com.digio.uber.dataset.payment

import br.com.digio.uber.model.payment.BillBarcode
import br.com.digio.uber.model.payment.ConfirmPayResponse
import br.com.digio.uber.model.payment.PaymentBarcodeRequest

interface AccountPaymentDataset {
    suspend fun checkBillBarcode(barcode: String): BillBarcode
    suspend fun paymentAuthorize(pidHash: String, request: PaymentBarcodeRequest): ConfirmPayResponse
}