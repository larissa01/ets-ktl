@file:Suppress("TooManyFunctions")

package br.com.digio.uber.dataset.transfer

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.transfer.AccountDomainResponse
import br.com.digio.uber.model.transfer.BankDomainResponse
import br.com.digio.uber.model.transfer.BeneficiariesResponse
import br.com.digio.uber.model.transfer.BeneficiaryRequest
import br.com.digio.uber.model.transfer.BeneficiaryResponse
import br.com.digio.uber.model.transfer.ExternalTransferRequest
import br.com.digio.uber.model.transfer.InternalTransferRequest
import br.com.digio.uber.model.transfer.PixBankDomainResponse
import br.com.digio.uber.model.transfer.ThirdPartyAccountListResponse
import br.com.digio.uber.model.transfer.ThirdPartyAccountResponse
import br.com.digio.uber.model.transfer.TransferResponse
import br.com.digio.uber.model.transfer.TransferStatus

/**
 * @author Marlon D. Rocha
 * @since 30/10/20
 */
class TransferDataSetImpl(
    private val uberClient: UberClient
) : TransferDataSet {

    override suspend fun getAccount(): AccountDomainResponse =
        uberClient.getUberLoggedEndpoint().getAccountTed()

    override suspend fun getPopularBanks(): BankDomainResponse =
        uberClient.getUberLoggedEndpoint().getPopularBanks()

    override suspend fun getPopularPixBanks(): PixBankDomainResponse =
        uberClient.getUberLoggedEndpoint().getPopularPixBanks()

    override suspend fun getBanks(): BankDomainResponse =
        uberClient.getUberLoggedEndpoint().getBanks()

    override suspend fun getPixBanks(): PixBankDomainResponse =
        uberClient.getUberLoggedEndpoint().getPixBanks()

    override suspend fun getTransferStatus(): TransferStatus =
        uberClient.getUberLoggedEndpoint().getTransferStatus()

    override suspend fun confirmTransferExternalSameOwner(transferRequest: ExternalTransferRequest): TransferResponse =
        uberClient.getUberLoggedEndpoint().confirmTransferExternalSameOwnership(transferRequest)

    override suspend fun confirmTransferExternalThirdParty(transferRequest: ExternalTransferRequest):
        TransferResponse = uberClient.getUberLoggedEndpoint().confirmTransferExternalThirdParty(transferRequest)

    override suspend fun confirmTransferInternal(transferRequest: InternalTransferRequest): TransferResponse =
        uberClient.getUberLoggedEndpoint().confirmTransferInternal(transferRequest)

    override suspend fun getTedBeneficiaries(): BeneficiariesResponse =
        uberClient.getUberLoggedEndpoint().getTedBeneficiaries()

    override suspend fun deleteTedBeneficiary(beneficiaryId: String): BeneficiaryResponse =
        uberClient.getUberLoggedEndpoint().deleteTedBeneficiary(beneficiaryId)

    override suspend fun putTedBeneficiary(
        beneficiaryId: String,
        beneficiaryRequest: BeneficiaryRequest
    ): BeneficiaryResponse =
        uberClient.getUberLoggedEndpoint().putTedBeneficiary(beneficiaryId, beneficiaryRequest)

    override suspend fun getP2PBeneficiaries(): BeneficiariesResponse =
        uberClient.getUberLoggedEndpoint().getP2pBeneficiaries()

    override suspend fun deleteP2PBeneficiary(beneficiaryId: String): BeneficiaryResponse =
        uberClient.getUberLoggedEndpoint().deleteP2pBeneficiary(beneficiaryId)

    override suspend fun putP2PBeneficiary(
        beneficiaryId: String,
        beneficiaryRequest: BeneficiaryRequest
    ): BeneficiaryResponse =
        uberClient.getUberLoggedEndpoint().putP2pBeneficiary(beneficiaryId, beneficiaryRequest)

    override suspend fun getAccountByCPF(cpf: String): ThirdPartyAccountListResponse =
        uberClient.getUberLoggedEndpoint().getAccountListByCPF(cpf)

    override suspend fun searchAccount(agency: String, account: String): ThirdPartyAccountResponse =
        uberClient.getUberLoggedEndpoint().searchAccount(agency, account)
}