package br.com.digio.uber.dataset.card

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.card.CardResponse
import br.com.digio.uber.model.card.TypeCard
import retrofit2.HttpException
import retrofit2.Response

interface CardDataSet {
    suspend fun getCardList(): List<CardResponse>
    suspend fun getCardByType(typeCard: TypeCard): CardResponse?
    suspend fun postCardBlock(cardId: Long, hash: String = "aaa"): Response<Void>
    suspend fun postCardUnblock(cardId: Long, hash: String = "aaa"): Response<Void>
}

class CardDataSetImpl(
    private val uberClient: UberClient
) : CardDataSet {

    override suspend fun getCardList(): List<CardResponse> =
        uberClient.getUberLoggedEndpoint().getCardList()

    override suspend fun getCardByType(typeCard: TypeCard): CardResponse? =
        getCardList().firstOrNull {
            it.productName == typeCard.productNameCard.name && it.isVirtualCard == typeCard.isVirtual
        }

    override suspend fun postCardBlock(cardId: Long, hash: String): Response<Void> =
        uberClient.getUberLoggedEndpoint().postCardBlock(cardId, hash).apply {
            if (!this.isSuccessful) {
                throw HttpException(this)
            }
        }

    override suspend fun postCardUnblock(cardId: Long, hash: String): Response<Void> =
        uberClient.getUberLoggedEndpoint().postCardUnblock(cardId, hash).apply {
            if (!this.isSuccessful) {
                throw HttpException(this)
            }
        }
}