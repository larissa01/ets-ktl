package br.com.digio.uber.dataset.virtual.card

import br.com.digio.uber.model.virtualCard.VirtualCardOnboarding
import br.com.digio.uber.model.virtualCard.VirtualCardOnboardingResponse

class VirtualCardOnboardingDataSetImpl : VirtualCardOnboardingDataSet {
    override suspend fun getOnboardingContent(): VirtualCardOnboardingResponse =
        VirtualCardOnboardingResponse(
            listOf(
                VirtualCardOnboarding(
                    screen = 0,
                    title = "Cartão virtual para compras online, pagas à vista.",
                    description = "A função usada deve ser crédito, mas a cobrança será à vista. Suas compras online" +
                        " serão debitadas, no momento da compra, do saldo disponível na sua Uber Conta "
                ),
                VirtualCardOnboarding(
                    screen = 1,
                    title = "",
                    description = "Para utilizar, basta inserir os dados no momento da compra. Copie o número do " +
                        "cartão selecionando o ícone “copiar”."
                ),
                VirtualCardOnboarding(
                    screen = 2,
                    title = "",
                    description = "Você pode bloquear ou gerar um novo cartão virtual a qualquer momento pelo app."
                )
            )
        )
}