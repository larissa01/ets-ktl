package br.com.digio.uber.dataset.store

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.store.StoreCatalog
import br.com.digio.uber.model.store.StoreOrder
import br.com.digio.uber.model.store.StoreOrderRegisterResponse
import br.com.digio.uber.model.store.StoreOrderResponse

interface StoreDataSet {
    suspend fun getStoreCatalog(): StoreCatalog
    suspend fun registerStoreOrder(order: StoreOrder): StoreOrderRegisterResponse
    suspend fun getStoreOrders(page: Int): ResponseList<StoreOrderResponse>
}

class StoreDataSetImpl(
    private val uberClient: UberClient
) : StoreDataSet {

    override suspend fun getStoreCatalog() =
        uberClient.getUberLoggedEndpoint().getStoreCatalog()

    override suspend fun registerStoreOrder(order: StoreOrder) =
        uberClient.getUberLoggedEndpoint().registerStoreOrder(order)

    override suspend fun getStoreOrders(page: Int) =
        uberClient.getUberLoggedEndpoint().getStoreOrders(page)
}