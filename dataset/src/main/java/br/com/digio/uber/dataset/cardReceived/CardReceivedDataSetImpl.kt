package br.com.digio.uber.dataset.cardReceived

import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.model.cardReceived.CardReceivedActivationRequest
import retrofit2.HttpException
import retrofit2.Response

class CardReceivedDataSetImpl constructor(
    val client: UberClient
) : CardReceivedDataSet {
    override suspend fun postValidateCardActivation(activationRequest: CardReceivedActivationRequest): Response<Void> =
        client.getUberLoggedEndpoint().postCardReceivedActivation(activationRequest).apply {
            if (!this.isSuccessful) {
                throw HttpException(this)
            }
        }
}