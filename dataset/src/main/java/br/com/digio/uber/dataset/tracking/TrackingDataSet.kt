package br.com.digio.uber.dataset.tracking

import br.com.digio.uber.model.tracking.TrackingLogResponse

interface TrackingDataSet {
    suspend fun getTrackingLog(cardId: Long): TrackingLogResponse
}