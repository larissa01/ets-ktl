/* Apply Module base configurations */
plugins {
    id("com.android.library")
    kotlin("android")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.library")
}

dependencies {

    implementation(Dependencies.TIMBER)
    implementation(Dependencies.GSON)
    implementation(Dependencies.RETROFIT)

    testImplementation(TestDependencies.JUNIT)
    testImplementation(TestDependencies.MOCKK)
    testImplementation(TestDependencies.COROUTINESTEST)
    testImplementation(Dependencies.RETROFIT)

    implementation(project(":gateway"))
    implementation(project(":model"))
}