package br.com.digio.uber.login.di.viewmodel

import br.com.digio.uber.login.viewmodel.LoginCpfViewModel
import br.com.digio.uber.login.viewmodel.LoginPasswordViewModel
import br.com.digio.uber.login.viewmodel.LoginWelcomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginViewModelModule = module {
    viewModel { LoginWelcomeViewModel() }
    viewModel { LoginCpfViewModel(get(), get(), get(), get(), get()) }
    viewModel { LoginPasswordViewModel(get(), get(), get()) }
}