package br.com.digio.uber.login.activity

import android.os.Build
import android.os.Bundle
import br.com.digio.uber.common.androidUtils.PermissionUtils
import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.login.R
import br.com.digio.uber.login.di.viewmodel.loginViewModelModule
import br.com.digio.uber.resetpassword.di.viewmodel.resetPasswordViewModelModule
import br.com.digio.uber.util.GPSUtil
import br.com.digio.uber.util.hasGpsPermission
import org.koin.core.module.Module

class LoginActivity : BaseActivity() {
    override fun getLayoutId(): Int? =
        R.layout.login_activity

    override fun getModule(): List<Module>? =
        listOf(loginViewModelModule, resetPasswordViewModelModule)

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        callGPS()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionUtils.apiInstance.notifyPermissionResult(this, permissions, grantResults)
    }

    private fun callGPS() {
        if (!hasGpsPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                PermissionUtils.apiInstance.requestPermissions(
                    this,
                    GPSUtil.PERMS,
                    GPSUtil.PERMS_REQUEST_CODE
                )
            }
        }
    }
}