package br.com.digio.uber.login.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel

class LoginWelcomeViewModel : BaseViewModel() {
    val goToAnimation: MutableLiveData<Boolean> = MutableLiveData()
}