package br.com.digio.uber.login.uimodel

enum class LoginRouterFinish {
    GO_TO_MAIN_ACTIVITY
}