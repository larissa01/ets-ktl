package br.com.digio.uber.login.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.exception.lexis.TrustDefenderException
import br.com.digio.uber.domain.exception.login.LoginBiometryTryAgainException
import br.com.digio.uber.domain.exception.login.LoginChangePasswordException
import br.com.digio.uber.domain.exception.login.LoginFirstChangePasswordException
import br.com.digio.uber.domain.exception.login.LoginForbiddenBiometryRequestException
import br.com.digio.uber.domain.exception.login.LoginGemaltoException
import br.com.digio.uber.domain.exception.login.LoginLexisRejectException
import br.com.digio.uber.domain.exception.login.LoginMaxAttemptsException
import br.com.digio.uber.domain.exception.login.LoginUserOrPasswordWrongException
import br.com.digio.uber.domain.usecase.login.MakeLoginUseCase
import br.com.digio.uber.login.BuildConfig
import br.com.digio.uber.login.R
import br.com.digio.uber.login.uimodel.LoginRouterFinish
import br.com.digio.uber.model.login.repository.model.Login

class LoginPasswordViewModel constructor(
    private val makeLoginUseCase: MakeLoginUseCase,
    private val analytics: Analytics,
    private val resourceManager: ResourceManager
) : LoginPasswordGenericErrosResolversViewModel(analytics, resourceManager) {

    val password: MutableLiveData<String> = MutableLiveData()
    val enableButton: MutableLiveData<Boolean> = MutableLiveData()
    val login: MutableLiveData<String> = MutableLiveData()
    val loginRouterFinish: MutableLiveData<LoginRouterFinish> = MutableLiveData()

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer {
            when (it.id) {
                R.id.btnNext -> {
                    analytics.pushSimpleEvent(Tags.LOGIN_PASSWORD_ACCESS)
                    makeLogin()
                    onClickItem(it)
                }
                else -> onClickItem(it)
            }
        }

        addDebugPasswordValue()
    }

    fun onTextChanged(password: String?) {
        enableButton.value = isValidatedInputData(password)
    }

    private fun isValidatedInputData(value: String?): Boolean {
        return if ((value?.length ?: 0) >= MAX_LIMIT_PASSWORD) {
            return isValidatedPassword(value)
        } else {
            passwordError.value = null
            false
        }
    }

    private fun isValidatedPassword(value: String?): Boolean {
        return when {
            value.isNullOrEmpty() || value.length < MAX_LIMIT_PASSWORD -> {
                passwordError.value =
                    resourceManager.getString(br.com.digio.uber.common.R.string.please_enter_a_valid_password)
                false
            }
            else -> {
                passwordError.value = null
                true
            }
        }
    }

    fun retryRequestLogin(filePath: String?) {
        pidInfo.value = pidInfo.value?.copy(facialRecognition = filePath)
        makeLogin()
    }

    private fun makeLogin() {
        val password = password.value
        val cpf = login.value

        if (password != null && cpf != null) {
            val login = Login(
                password = password,
                userName = cpf,
                pidInfo = pidInfo.value
            )
            login(login)
        }
    }

    override fun login(login: Login) {
        makeLoginUseCase(login).singleExec(
            onError = { _, error, throwable ->
                when (throwable) {
                    is LoginForbiddenBiometryRequestException ->
                        makeLoginForbiddenBiometryError(throwable)
                    is LoginChangePasswordException ->
                        makeLoginChangePasswordError()
                    is LoginFirstChangePasswordException ->
                        makeLoginFirstChangePasswordError()
                    is TrustDefenderException ->
                        makeLoginLexisError(throwable)
                    is LoginMaxAttemptsException ->
                        makeLoginMaxAttempsError(error)
                    is LoginGemaltoException ->
                        makeLoginGemaltoError(login)
                    is LoginBiometryTryAgainException ->
                        makeLoginBiometryTryAgain(error)
                    is LoginLexisRejectException ->
                        makeLoginLexisRejectException(error)
                    is LoginUserOrPasswordWrongException ->
                        makeLoginUserOrPasswordWrongException(error)
                    else -> error
                }
            },
            onSuccessBaseViewModel = {
                loginRouterFinish.value = LoginRouterFinish.GO_TO_MAIN_ACTIVITY
            }
        )
    }

    private fun addDebugPasswordValue() {
        if (BuildConfig.BUILD_TYPE == "debug" || BuildConfig.BUILD_TYPE == "homol") {
            password.value = BuildConfig.MOCK_USER_PASS
        }
    }

    companion object {
        private const val MAX_LIMIT_PASSWORD = 6
    }
}