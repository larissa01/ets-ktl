package br.com.digio.uber.login.fragment

import android.os.Bundle
import androidx.lifecycle.Observer
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.stateGetLiveData
import br.com.digio.uber.common.extensions.stateSetValue
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.login.R
import br.com.digio.uber.login.databinding.LoginPasswordFragmentBinding
import br.com.digio.uber.login.uimodel.LoginRouterFinish
import br.com.digio.uber.login.viewmodel.LoginPasswordViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.CREATE_PASSWORD_SCREEN
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.FIRST_LOGIN_SCREEN
import org.koin.android.ext.android.inject

abstract class LoginSetupObserveFragment :
    LoginAbstractFragment<LoginPasswordFragmentBinding, LoginPasswordViewModel>() {

    override val viewModel: LoginPasswordViewModel? by inject()

    val login: String by lazy {
        LoginPasswordFragmentArgs.fromBundle(requireArguments()).login
    }

    protected fun setupObserve() {
        viewModel?.password?.observe(
            this,
            Observer { pass ->
                viewModel?.onTextChanged(pass)
            }
        )
        viewModel?.loginRouterFinish?.observe(
            this,
            Observer { loginRouterFinish ->
                handleLoginSuccessFinish(loginRouterFinish)
            }
        )
        viewModel?.startBiometryCapture?.observe(
            this,
            Observer {
                if (it == true) {
                    navigateToBiometry()
                }
            }
        )
        viewModel?.startChangePassword?.observe(
            this,
            Observer {
                if (it == true) {
                    navigateToChangePassword(CREATE_PASSWORD_SCREEN)
                }
            }
        )
        viewModel?.startFirstChangePassword?.observe(
            this,
            Observer {
                if (it == true) {
                    navigateToChangePassword(FIRST_LOGIN_SCREEN)
                }
            }
        )
        viewModel?.goToResetPassword?.observe(
            this,
            Observer {
                handleGoToResetPassword(it)
            }
        )
        viewModel?.goToAttendance?.observe(
            this,
            Observer {
                handleGoToAttendance(it)
            }
        )
        stateGetLiveData<Boolean>(Const.RequestOnResult.ResetPassword.RESULT_SUCCESS)?.observe(
            this,
            Observer {
                handleSuccessInResetPassword(it)
            }
        )
    }

    private fun handleGoToAttendance(it: Boolean?) {
        if (it == true) {
            activity?.let { activityLet ->
                navigation.navigationToFaqDeepLinkActivity(
                    activityLet,
                    Const.DeepLink.DEEPLINK_FAQ_SAC,
                    Bundle().apply {
                        putString(Const.RequestOnResult.FaqSac.KEY_TOOLBAR_TITLE, getString(R.string.login_title))
                    }
                )
            }
            viewModel?.goToAttendance?.value = false
        }
    }

    private fun handleLoginSuccessFinish(loginRouterFinish: LoginRouterFinish?) {
        when (loginRouterFinish) {
            LoginRouterFinish.GO_TO_MAIN_ACTIVITY -> activity?.let { activityLet ->
                navigation.navigationToMainActivity(activityLet)
                activityLet.finish()
            }
            else -> showMessage(ErrorObject())
        }
    }

    private fun handleGoToResetPassword(value: Boolean?) {
        if (value == true) {
            goToResetPassword()
            viewModel?.goToResetPassword?.value = false
        }
    }

    private fun handleSuccessInResetPassword(value: Boolean?) {
        if (value == true) {
            viewModel?.password?.value = null
            stateSetValue(Const.RequestOnResult.ResetPassword.RESULT_SUCCESS, false)
        }
    }

    private fun goToResetPassword() {
        navigateTo(
            LoginPasswordFragmentDirections
                .actionLoginPasswordFragmentToIncludeResetPassword(),
            Bundle().apply {
                putString(Const.RequestOnResult.ResetPassword.KEY_USER_LOGIN, login)
            }
        )
    }

    private fun navigateToChangePassword(screen: String) {
        navigation
            .navigateToChangePasswordActivity(
                this@LoginSetupObserveFragment,
                screen,
                viewModel?.password?.value,
                viewModel?.login?.value
            )
        viewModel?.startChangePassword?.value = false
        viewModel?.startFirstChangePassword?.value = false
    }

    private fun navigateToBiometry() {
        navigation.navigateToFacialBiometryCaptureActivity(this, Tags.LOGIN_BIOMETRY_CONTINUE)
        viewModel?.startBiometryCapture?.value = false
    }
}