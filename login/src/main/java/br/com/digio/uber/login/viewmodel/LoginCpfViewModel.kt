package br.com.digio.uber.login.viewmodel

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.common.typealiases.OnTextChanged
import br.com.digio.uber.domain.usecase.flags.GetFlagsNotLoggedInLiveDataUseCase
import br.com.digio.uber.domain.usecase.flags.UpdateFlagsNotLoggedInUseCase
import br.com.digio.uber.domain.usecase.login.GetCpfFromLocalUseCase
import br.com.digio.uber.domain.usecase.login.SaveCpfInLocalUseCase
import br.com.digio.uber.login.BuildConfig
import br.com.digio.uber.login.R
import br.com.digio.uber.model.UberFlagsPersistNotLoggedIn
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.onlyNumbers
import br.com.digio.uber.util.validateCpf

class LoginCpfViewModel constructor(
    private val getCpfFromLocalUseCase: GetCpfFromLocalUseCase,
    private val saveCpfInLocalUseCase: SaveCpfInLocalUseCase,
    private val updateFlagsNotLoggedInUseCase: UpdateFlagsNotLoggedInUseCase,
    getFlagsNotLoggedInUseCase: GetFlagsNotLoggedInLiveDataUseCase,
    private val analytics: Analytics
) : BaseViewModel() {

    val mask: String = Const.MaskPattern.CPF

    val cpfError: MutableLiveData<Int> = MutableLiveData()
    val enableButton: MutableLiveData<Boolean> = MutableLiveData()
    val cpf: MutableLiveData<String> = MutableLiveData()
    private val databaseSaveCpf: LiveData<UberFlagsPersistNotLoggedIn> =
        getFlagsNotLoggedInUseCase(Unit)

    val saveCpf: MediatorLiveData<Boolean> = MediatorLiveData<Boolean>().apply {
        addSource(this) { valueChanged ->
            if (valueChanged != databaseSaveCpf.value?.saveCpfInLogin) {
                updateFlagsNotLoggedInUseCase(
                    databaseSaveCpf.value?.copy(saveCpfInLogin = valueChanged)
                        ?: UberFlagsPersistNotLoggedIn(saveCpfInLogin = valueChanged)
                ).singleExec(showLoadingFlag = false)
            }
        }
        addSource(databaseSaveCpf) { flags ->
            value = flags.saveCpfInLogin
        }
    }

    val onCpfChange: OnTextChanged = { value, _, _, _ ->
        enableButton.value = isCpfValid(value.toString())
    }

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer { view ->
            when (view.id) {
                R.id.btnNext -> checkCpfExistInBackendAndSaveIfRequired(onClickItem, view)
                else -> onClickItem(view)
            }
        }
        setupCpf()
    }

    private fun checkCpfExistInBackendAndSaveIfRequired(
        onClickItem: OnClickItem,
        view: View
    ) {
        cpf.value?.onlyNumbers()?.let { cpfLet ->
            if (saveCpf.value == true) {
                saveCpfInLocalUseCase(cpfLet).singleExec(showLoadingFlag = false)
            }
        }
        onClickItem(view)
    }

    private fun setupCpf() {
        getCpfFromLocalUseCase(Unit).singleExec(
            onSuccessBaseViewModel = { cpfSaved ->
                if (cpfSaved.isNotEmpty() && saveCpf.value == true) {
                    cpf.postValue(cpfSaved.onlyNumbers().applyMask(mask))
                } else if (BuildConfig.BUILD_TYPE == "debug" || BuildConfig.BUILD_TYPE == "homol") {
                    cpf.postValue(BuildConfig.MOCK_USER_LOGIN.applyMask(mask))
                }
            }
        )
    }

    private fun isCpfValid(value: String): Boolean =
        when {
            value.length < MAX_TEXT_SIZE_WITH_MASK -> {
                cpfError.value = null
                false
            }
            value.validateCpf() -> {
                cpfError.value = null
                true
            }
            else -> {
                analytics.pushSimpleEvent(
                    Tags.LOGIN_ERROS,
                    Tags.PARAMETER_CPF_INVALID to "cpf_incorreto"
                )
                cpfError.value = R.string.cpf_invalid
                false
            }
        }

    companion object {
        private const val MAX_TEXT_SIZE_WITH_MASK = 14
    }
}