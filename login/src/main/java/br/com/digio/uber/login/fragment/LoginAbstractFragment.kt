package br.com.digio.uber.login.fragment

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.base.viewmodel.BaseViewModel

abstract class LoginAbstractFragment<T : ViewDataBinding, VM : BaseViewModel> :
    BaseViewModelFragment<T, VM>() {

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true
}