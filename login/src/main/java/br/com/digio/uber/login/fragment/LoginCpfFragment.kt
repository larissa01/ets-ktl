package br.com.digio.uber.login.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.login.BR
import br.com.digio.uber.login.R
import br.com.digio.uber.login.databinding.LoginCpfFragmentBinding
import br.com.digio.uber.login.viewmodel.LoginCpfViewModel
import br.com.digio.uber.util.onlyNumbers
import org.koin.android.ext.android.inject

class LoginCpfFragment : LoginAbstractFragment<LoginCpfFragmentBinding, LoginCpfViewModel>() {

    override val bindingVariable: Int? = BR.loginHomeViewModel
    override val getLayoutId: Int? = R.layout.login_cpf_fragment
    override val viewModel: LoginCpfViewModel? by inject()

    private val analytics: Analytics by inject()

    override fun tag(): String = LoginCpfFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun initialize() {
        super.initialize()
        setupViewModel()
    }

    override fun getToolbar(): Toolbar? =
        binding?.appBarLoginCpf?.toolbarLogin

    private fun setupViewModel() {
        viewModel?.initializer { view ->
            when (view.id) {
                R.id.btnNext -> goToPasswordFragment()
            }
        }
    }

    private fun goToPasswordFragment() {
        viewModel?.cpf?.value?.onlyNumbers()?.let { cpf ->
            analytics.pushSimpleEvent(Tags.LOGIN_CPF)
            navigateTo(
                LoginCpfFragmentDirections.actionLoginHomeFragmentToLoginPasswordFragment(
                    cpf
                )
            )
        }
    }

    override fun onNavigationClick(view: View) {
        pop()
    }
}