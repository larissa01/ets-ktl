package br.com.digio.uber.login.fragment

import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.openInBrowserLink
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.login.BR
import br.com.digio.uber.login.BuildConfig
import br.com.digio.uber.login.R
import br.com.digio.uber.login.databinding.LoginWelcomeFragmentBinding
import br.com.digio.uber.login.viewmodel.LoginWelcomeViewModel
import br.com.digio.uber.util.BuildTypes
import br.com.digio.uber.util.Const
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject

class LoginWelcomeFragment :
    BaseViewModelFragment<LoginWelcomeFragmentBinding, LoginWelcomeViewModel>() {

    override val bindingVariable: Int? = BR.loginWelcomeViewModel
    override val getLayoutId: Int? = R.layout.login_welcome_fragment
    override val viewModel: LoginWelcomeViewModel? by inject()

    private val analytics: Analytics by inject()

    override fun tag(): String =
        LoginWelcomeFragment::class.java.name

    override fun onResume() {
        super.onResume()
        viewModel?.goToAnimation?.value = true
    }

    override fun initialize() {
        super.initialize()
        viewModel?.initializer {
            when (it.id) {
                R.id.btnNext -> goToLoginCpfFragment()
                R.id.btnOpenAccount -> goToAcquisition()
            }
        }
        viewModel?.goToAnimation?.value = true
    }

    private fun goToAcquisition() {
        analytics.pushSimpleEvent(Tags.OPEN_MY_ACCOUNT)
        val url = when (BuildConfig.BUILD_TYPE) {
            BuildTypes.DEBUG.value -> Const.URI.ACQUISITION_DEV_URI
            BuildTypes.HOMOL.value -> Const.URI.ACQUISITION_HML_URI
            BuildTypes.RELEASE.value -> Const.URI.ACQUISITION_PROD_URI
            else -> Const.URI.ACQUISITION_HML_URI
        }
        activity?.openInBrowserLink(url)
    }

    private fun goToLoginCpfFragment() {
        viewModel?.goToAnimation?.value = false
        launch(Main) {
            withContext(Default) {
                delay(DELAY_FOR_END_ANIMATION)
            }
            analytics.pushSimpleEvent(Tags.HAVE_ACCOUNT)
            navigateTo(
                LoginWelcomeFragmentDirections
                    .actionLoginWelcomeFragmentToLoginCpfFragment()
            )
        }
    }

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE

    companion object {
        const val DELAY_FOR_END_ANIMATION = 400L
    }
}