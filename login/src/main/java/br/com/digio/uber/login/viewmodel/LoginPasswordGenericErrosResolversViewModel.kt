package br.com.digio.uber.login.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.generics.MessageGenericObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.exception.lexis.TrustDefenderException
import br.com.digio.uber.domain.exception.login.LoginForbiddenBiometryRequestException
import br.com.digio.uber.login.R
import br.com.digio.uber.model.error.ErrorResponse
import br.com.digio.uber.model.login.PidInfo
import br.com.digio.uber.model.login.PidTrackingInfo
import br.com.digio.uber.model.login.repository.model.Login

/**
 * In the site of detekt have a description of lint TooManyFunctions, and in description have
 * a reference of single responsibility principle. This class have it, because this class have
 * only methods of erros, and flags to retport view of this error.
 * So i won't verify this lint again, only for this class.
 */
@Suppress("TooManyFunctions")
abstract class LoginPasswordGenericErrosResolversViewModel constructor(
    private val analytics: Analytics,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    protected val pidInfo: MutableLiveData<PidInfo> = MutableLiveData()
    val passwordError: MutableLiveData<String> = MutableLiveData()
    val startBiometryCapture: MutableLiveData<Boolean> = MutableLiveData()
    val startChangePassword: MutableLiveData<Boolean> = MutableLiveData()
    val startFirstChangePassword: MutableLiveData<Boolean> = MutableLiveData()
    val goToResetPassword: MutableLiveData<Boolean> = MutableLiveData()
    val goToAttendance: MutableLiveData<Boolean> = MutableLiveData()

    protected abstract fun login(login: Login)

    protected fun makeLoginGemaltoError(login: Login): EmptyErrorObject {
        // #Danger
        analytics.pushSimpleEvent(
            Tags.LOGIN_ERROS,
            Tags.PARAMETER_PASSWORD_ACCESS_INVALID to "gemalto_erro"
        )
        login(login)
        return EmptyErrorObject
    }

    protected fun makeLoginBiometryTryAgain(error: MessageGenericObject): MessageGenericObject =
        error.apply {
            onCloseDialog = {
                pidInfo.value = pidInfo.value?.copy(facialRecognition = null)
                startBiometryCapture.value = true
            }
        }

    protected fun makeLoginLexisRejectException(error: MessageGenericObject): MessageGenericObject =
        error.apply {
            title = R.string.title_error_lexis_reject
            buttonText = R.string.error_lexis_reject
            onPositiveButtonClick = {
                goToAttendance.value = true
            }
        }

    protected fun makeLoginMaxAttempsError(error: MessageGenericObject): MessageGenericObject {
        analytics.pushSimpleEvent(
            Tags.LOGIN_ERROS,
            Tags.PARAMETER_PASSWORD_ACCESS_INVALID to "limite_de_tentativas_excedida"
        )
        return error.apply {
            onCloseDialog = {
                goToResetPassword.value = true
            }
        }
    }

    protected fun makeLoginLexisError(throwable: TrustDefenderException): ErrorObject {
        analytics.pushSimpleEvent(
            Tags.LOGIN_ERROS,
            Tags.PARAMETER_PASSWORD_ACCESS_INVALID to "lexis_erro"
        )
        return makeTrustDefenderErrorObject(throwable)
    }

    protected fun makeLoginFirstChangePasswordError(): EmptyErrorObject {
        analytics.pushSimpleEvent(
            Tags.LOGIN_ERROS,
            Tags.PARAMETER_PASSWORD_ACCESS_INVALID to "mudança_de_senha_primeiro_acesso"
        )
        startFirstChangePassword.value = true
        return EmptyErrorObject
    }

    protected fun makeLoginChangePasswordError(): EmptyErrorObject {
        analytics.pushSimpleEvent(
            Tags.LOGIN_ERROS,
            Tags.PARAMETER_PASSWORD_ACCESS_INVALID to "mudança_de_senha"
        )
        startChangePassword.value = true
        return EmptyErrorObject
    }

    protected fun makeLoginForbiddenBiometryError(throwable: LoginForbiddenBiometryRequestException): EmptyErrorObject {
        analytics.pushSimpleEvent(
            Tags.LOGIN_ERROS,
            Tags.PARAMETER_PASSWORD_ACCESS_INVALID to "lexis_biometria"
        )
        handleLoginBiometry(throwable.responseErrorLexisLogin)
        return EmptyErrorObject
    }

    protected fun makeLoginUserOrPasswordWrongException(error: MessageGenericObject): EmptyErrorObject {
        passwordError.value = error.messageString
            ?: resourceManager.getString(br.com.digio.uber.common.R.string.please_enter_a_valid_password)
        return EmptyErrorObject
    }

    private fun handleLoginBiometry(errorResponse: ErrorResponse<PidTrackingInfo>) {
        errorResponse.error?.data?.let { pidTrakingInfo ->
            pidInfo.value = PidInfo(
                pidTrackingInfo = pidTrakingInfo
            )
        }
        startBiometryCapture.value = true
    }

    private fun makeTrustDefenderErrorObject(throwable: TrustDefenderException) =
        ErrorObject(throwable).apply {
            message = R.string.error_validation
            callGoBack = true
        }
}