package br.com.digio.uber.login.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.extensions.hideKeyboard
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.login.BR
import br.com.digio.uber.login.R
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.CHANGE_PASSWORD_REQUEST_CODE
import br.com.digio.uber.util.Const.RequestOnResult.FacialBiometry.REQUEST_FACIAL_CAPTURE
import br.com.digio.uber.util.Const.RequestOnResult.FacialBiometry.REQUEST_FACIAL_CAPTURE_SUCCESS
import br.com.digio.uber.util.Const.RequestOnResult.FacialBiometry.REQUEST_FACIAL_VALUE
import timber.log.Timber

class LoginPasswordFragment :
    LoginSetupObserveFragment() {

    override val bindingVariable: Int? = BR.loginPasswordViewModel
    override val getLayoutId: Int? = R.layout.login_password_fragment

    override fun tag(): String = LoginPasswordFragment::class.java.name

    override fun getToolbar(): Toolbar? =
        binding?.appBarLoginPass?.toolbarLogin

    override fun initialize() {
        super.initialize()
        viewModel?.login?.value = login
        viewModel?.initializer {
            when (it.id) {
                R.id.btnForgotPassword ->
                    navigateToForgotPassword()
            }
        }
        setupObserve()
    }

    private fun navigateToForgotPassword() {
        navigateTo(
            LoginPasswordFragmentDirections
                .actionLoginPasswordFragmentToIncludeResetPassword(),
            Bundle().apply {
                putString(Const.RequestOnResult.ResetPassword.KEY_USER_LOGIN, login)
            }
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_FACIAL_CAPTURE -> handleFacialBiometryActivity(resultCode, data)
            CHANGE_PASSWORD_REQUEST_CODE -> {
                viewModel?.password?.value = null
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun handleFacialBiometryActivity(resultCode: Int, data: Intent?) {
        when (resultCode) {
            REQUEST_FACIAL_CAPTURE_SUCCESS -> handleFacialBiometryActivitySuccess(data)
        }
    }

    private fun handleFacialBiometryActivitySuccess(data: Intent?) {
        try {
            viewModel?.retryRequestLogin(
                data?.getStringExtra(REQUEST_FACIAL_VALUE)
            )
        } catch (e: IllegalStateException) {
            viewModel?.message?.value = ErrorObject()
            Timber.e(e)
        }
    }

    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE

    override fun onNavigationClick(view: View) {
        hideKeyboard()
        pop()
    }

    override fun getRequestCode(): MutableList<Int> = mutableListOf(
        REQUEST_FACIAL_CAPTURE,
        CHANGE_PASSWORD_REQUEST_CODE
    )
}