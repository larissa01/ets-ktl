plugins {
    id("com.android.dynamic-feature")
    kotlin("android")
    kotlin("kapt")
    id("kotlin-android-extensions")
    id("androidx.navigation.safeargs")
    id("br.com.digio.uber.plugin.android.dynamic.library")
}

val mockUserLogin: String by project
val mockUserPass: String by project

configureAndroidDynamicLibrary(
    releaseBuildTypeConfig = {
        buildConfigField("String", "MOCK_USER_LOGIN", "null")
        buildConfigField("String", "MOCK_USER_PASS", "null")
    },
    homolBuildTypeConfig = {
        buildConfigField("String", "MOCK_USER_LOGIN", "null")
        buildConfigField("String", "MOCK_USER_PASS", mockUserPass)
    },
    debugBuildTypeConfig = {
        buildConfigField("String", "MOCK_USER_LOGIN", mockUserLogin)
        buildConfigField("String", "MOCK_USER_PASS", mockUserPass)
    }
)

dependencies {

    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.FRAGMENTKTX)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.KOIN)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)
    implementation(Dependencies.NAV_FRAGMENT)
    implementation(Dependencies.NAV_UI)
    implementation(Dependencies.NAV_DYNAMIC_FEATURE)
    implementation(Dependencies.LIFECYCLE_EXTENSIONS)
    implementation(Dependencies.LIFECYCLE_VIEWMODEL)
    implementation(Dependencies.ANNOTATION)
    implementation(Dependencies.TIMBER)

    implementation(project(":app"))
    implementation(project(":analytics"))
    implementation(project(":facialbiometry"))
    implementation(project(":resetpassword"))
    implementation(project(":common"))
    implementation(project(":domain"))
    implementation(project(":util"))
    implementation(project(":model"))
    implementation(project(":marketingCloud"))
}