package br.com.digio.uber.veloe.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.store.RegisterStoreOrderUseCase
import br.com.digio.uber.model.store.PaymentType
import br.com.digio.uber.model.store.StoreOrder
import br.com.digio.uber.model.store.StoreOrderRegisterResponse
import br.com.digio.uber.model.store.StoreProduct

class VeloeTagViewModel constructor(
    private val registerStoreOrderUseCase: RegisterStoreOrderUseCase
) : BaseViewModel() {

    val tagQuantitySelected: MutableLiveData<String> = MutableLiveData()
    val buttonEnable = MutableLiveData<Boolean>(false)
    val orderRegisterResponse: MutableLiveData<StoreOrderRegisterResponse> = MutableLiveData()

    fun setTagQuantity(quantityTags: String, enableButton: Boolean) {
        tagQuantitySelected.value = quantityTags
        buttonEnable.value = enableButton
    }

    fun orderRequest(product: StoreProduct, quantityTags: Int) {
        registerStoreOrderUseCase(setOrder(product, quantityTags)).singleExec(
            onError = { _, error, _ ->
                error
            },
            onSuccessBaseViewModel = { response ->
                orderRegisterResponse.value = response
            }
        )
    }

    private fun setOrder(product: StoreProduct, quantityTags: Int) = StoreOrder(
        subproductId = product.subproducts?.get(0)?.subproductId,
        value = 0f,
        installment = 1,
        quantity = quantityTags,
        paymentMethod = PaymentType.ACCOUNT,
        cardId = null,
        cardDueDate = null,
        birthdate = null,
        ddd = null,
        phoneNumber = null,
        carrierLabel = null
    )
}