package br.com.digio.uber.veloe.navigation

import br.com.digio.uber.common.kenum.StatusBarColor

enum class VeloeOpKey(val theme: StatusBarColor) {
    VELOE_HOME(StatusBarColor.GREEN),
    VELOE_TAG(StatusBarColor.GREY),
    VELOE_SUCCESS(StatusBarColor.GREY),
    VELOE_TAG_DIALOG(StatusBarColor.GREY)
}