package br.com.digio.uber.veloe.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel

class VeloeHomeViewModel : BaseViewModel() {
    val monthTitle: MutableLiveData<String> = MutableLiveData()

    fun setMonthTitle(title: String) {
        monthTitle.value = title
    }
}