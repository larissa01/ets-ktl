package br.com.digio.uber.veloe.ui.dialogs

import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.dialog.BNWFViewModelDialog
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.veloe.BR
import br.com.digio.uber.veloe.R
import br.com.digio.uber.veloe.databinding.DialogQuantityTagsBinding
import br.com.digio.uber.veloe.interfaces.TagQuantitySelectListener
import br.com.digio.uber.veloe.navigation.VeloeOpKey
import br.com.digio.uber.veloe.ui.adapter.VeloeQuantityTagListAdapter
import br.com.digio.uber.veloe.ui.viewmodel.VeloeQuantityTagsViewModel
import org.koin.android.ext.android.inject

class VeloeQuantityTagsDialog : BNWFViewModelDialog<DialogQuantityTagsBinding, VeloeQuantityTagsViewModel>() {

    private val quantity by lazy {
        arguments?.getInt(QUANTITY)
    }

    private var tagQuantitySelectListener: TagQuantitySelectListener? = null

    override val bindingVariable: Int = BR.veloeQuantityTagsViewModel

    override val getLayoutId: Int = R.layout.dialog_quantity_tags

    override val viewModel: VeloeQuantityTagsViewModel by inject()

    override fun tag(): String = VeloeOpKey.VELOE_TAG_DIALOG.name

    override fun getStatusBarAppearance(): StatusBarColor = VeloeOpKey.VELOE_TAG_DIALOG.theme

    override fun initialize() {
        super.initialize()
        viewModel.initializer { }

        setAdapter()

        viewModel.setChoice(setListChoice(quantity))
    }

    private fun setAdapter() {
        binding?.listChoicerechargeRecycler?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = VeloeQuantityTagListAdapter(
                listener = { value ->
                    dismiss()
                    tagQuantitySelectListener?.quantitySelected(value)
                }
            )
        }
    }

    private fun setListChoice(quantityTags: Int?): List<Int> {
        val listTags = mutableListOf<Int>()

        quantityTags?.let {
            for (quantity in 1..it)
                listTags.add(quantity)
        }

        return listTags
    }

    companion object {
        const val QUANTITY = "quantity"

        fun newInstance(tagQuantitySelectListener: TagQuantitySelectListener, quantityTags: Int) =
            VeloeQuantityTagsDialog().apply {
                arguments = bundleOf(
                    QUANTITY to quantityTags
                )
                this.tagQuantitySelectListener = tagQuantitySelectListener
            }
    }
}