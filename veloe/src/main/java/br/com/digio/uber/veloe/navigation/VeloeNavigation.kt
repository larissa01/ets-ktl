package br.com.digio.uber.veloe.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName
import br.com.digio.uber.veloe.ui.fragment.VeloeHomeFragment
import br.com.digio.uber.veloe.ui.fragment.VeloeSuccessFragment
import br.com.digio.uber.veloe.ui.fragment.VeloeTagFragment

class VeloeNavigation(
    activity: AppCompatActivity
) : GenericNavigationWithFlow(activity) {

    fun init(bundle: Bundle?) {
        goTo(GoTo(VeloeOpKey.VELOE_HOME.name, bundle))
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            VeloeOpKey.VELOE_HOME.name to showHome(),
            VeloeOpKey.VELOE_TAG.name to showTagQuantity(),
            VeloeOpKey.VELOE_SUCCESS.name to showSuccess()
        )

    private fun showHome(): GenericNavigationWIthFlowGetFragmentByName = {
        VeloeHomeFragment.newInstance(this, it)
    }

    private fun showTagQuantity(): GenericNavigationWIthFlowGetFragmentByName = {
        VeloeTagFragment.newInstance(this, it)
    }

    private fun showSuccess(): GenericNavigationWIthFlowGetFragmentByName = {
        VeloeSuccessFragment.newInstance(this, it)
    }
}