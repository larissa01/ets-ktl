package br.com.digio.uber.veloe.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.store.StoreProduct
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.safeHeritage
import br.com.digio.uber.veloe.BR
import br.com.digio.uber.veloe.R
import br.com.digio.uber.veloe.databinding.FragmentVeloeHomeBinding
import br.com.digio.uber.veloe.navigation.VeloeOpKey
import br.com.digio.uber.veloe.ui.viewmodel.VeloeHomeViewModel
import org.koin.android.ext.android.inject

class VeloeHomeFragment :
    BNWFViewModelFragment<FragmentVeloeHomeBinding, VeloeHomeViewModel>() {

    val product: StoreProduct? by lazy {
        arguments?.getSerializable(Const.Extras.STORE_PRODUCT)?.safeHeritage<StoreProduct>()
    }

    override val bindingVariable: Int = BR.veloeHomeViewModel

    override val getLayoutId: Int = R.layout.fragment_veloe_home

    override val viewModel: VeloeHomeViewModel by inject()

    override fun tag(): String = VeloeOpKey.VELOE_HOME.name

    override fun getStatusBarAppearance(): StatusBarColor = VeloeOpKey.VELOE_HOME.theme

    override fun getToolbar(): Toolbar? = binding?.veloeHomeAppBar?.navToolbar.apply {
        this?.navigationIcon =
            context?.let { ContextCompat.getDrawable(it, br.com.digio.uber.common.R.drawable.ic_back_arrow_black) }
        this?.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                br.com.digio.uber.common.R.color.green_E6F2ED
            )
        )
        this?.setTitleTextColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.black))
    }

    override fun onNavigationClick(view: View) {
        callGoBack()
    }

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp = true

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it, arguments) }
        product?.subproducts?.firstOrNull()?.description?.let {
            viewModel.setMonthTitle(it)
        }
    }

    private fun onItemClicked(v: View, bundle: Bundle?) {
        when (v.id) {
            R.id.veloe_home_btn_continue -> callGoTo(VeloeOpKey.VELOE_TAG.name, bundle)
        }
    }

    companion object : FragmentNewInstance<VeloeHomeFragment>(::VeloeHomeFragment)
}