package br.com.digio.uber.veloe.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.util.safeHeritage
import br.com.digio.uber.veloe.R
import br.com.digio.uber.veloe.databinding.ItemTagQuantityBinding

typealias OnClickQuantityTagsItem = (item: Int) -> Unit

class VeloeQuantityTagListAdapter(
    private val listener: OnClickQuantityTagsItem
) : RecyclerView.Adapter<VeloeQuantityTagListAdapter.QuantityViewHolder>(), AdapterItemsContract {
    private var quantityTags: List<Int> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuantityViewHolder {
        val binding = ItemTagQuantityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return QuantityViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: QuantityViewHolder, position: Int) {
        holder.bind(quantityTags[position], listener)
    }

    override fun getItemCount() = quantityTags.size

    override fun replaceItems(list: List<Any>) {
        quantityTags = list.safeHeritage()
        notifyDataSetChanged()
    }

    class QuantityViewHolder(
        private val binding: ItemTagQuantityBinding,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(quantity: Int, listener: OnClickQuantityTagsItem) {
            binding.itemTagQuantityTxt.text =
                if (quantity == 1) String.format(context.getString(R.string.quantity_tag, quantity.toString()))
                else String.format(context.getString(R.string.quantity_tags, quantity.toString()))

            binding.itemTagQuantityContent.setOnClickListener { listener.invoke(quantity) }
        }
    }
}