package br.com.digio.uber.veloe.ui.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.veloe.BR
import br.com.digio.uber.veloe.R
import br.com.digio.uber.veloe.databinding.FragmentVeloeSuccessBinding
import br.com.digio.uber.veloe.navigation.VeloeOpKey
import br.com.digio.uber.veloe.ui.viewmodel.VeloeSuccessViewModel
import org.koin.android.ext.android.inject

class VeloeSuccessFragment :
    BNWFViewModelFragment<FragmentVeloeSuccessBinding, VeloeSuccessViewModel>() {

    override val bindingVariable: Int = BR.veloeSuccessViewModel

    override val getLayoutId: Int = R.layout.fragment_veloe_success

    override val viewModel: VeloeSuccessViewModel by inject()

    override fun tag(): String = VeloeOpKey.VELOE_SUCCESS.name

    override fun getStatusBarAppearance(): StatusBarColor = VeloeOpKey.VELOE_SUCCESS.theme

    override fun getToolbar(): Toolbar? = binding?.veloeSuccessAppBar?.navToolbar.apply {
        this?.setBackgroundColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.grey_1F1F1F))
        this?.setTitleTextColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.white))
    }

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp = false

    override fun onNavigationClick(view: View) {
        callGoBack()
    }

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it) }
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.recharge_success_btn_back -> callGoBack()
        }
    }

    companion object : FragmentNewInstance<VeloeSuccessFragment>(::VeloeSuccessFragment)
}