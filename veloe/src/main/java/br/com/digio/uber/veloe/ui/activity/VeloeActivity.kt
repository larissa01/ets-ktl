package br.com.digio.uber.veloe.ui.activity

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.navigation.GenericNavigation
import br.com.digio.uber.veloe.di.veloeViewModelModule
import br.com.digio.uber.veloe.navigation.VeloeNavigation
import org.koin.core.module.Module

class VeloeActivity : BaseViewModelActivity<ViewDataBinding, BaseViewModel>() {

    private val veloeNavigation = VeloeNavigation(this)

    override val bindingVariable: Int? = null

    override val viewModel: BaseViewModel? = null

    override fun getLayoutId(): Int? = LayoutIds.navigationLayoutActivity

    override fun getModule(): List<Module>? = listOf(veloeViewModelModule)

    override fun getNavigation(): GenericNavigation? = veloeNavigation

    override fun showHomeAsUp(): Boolean = true

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)

        veloeNavigation.init(intent.extras)
    }
}