package br.com.digio.uber.veloe.ui.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.store.StoreProduct
import br.com.digio.uber.model.store.SubproductOrderResponseEnum
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.safeHeritage
import br.com.digio.uber.util.safeLet
import br.com.digio.uber.veloe.BR
import br.com.digio.uber.veloe.R
import br.com.digio.uber.veloe.databinding.FragmentVeloeTagBinding
import br.com.digio.uber.veloe.interfaces.TagQuantitySelectListener
import br.com.digio.uber.veloe.navigation.VeloeOpKey
import br.com.digio.uber.veloe.ui.dialogs.VeloeQuantityTagsDialog
import br.com.digio.uber.veloe.ui.viewmodel.VeloeTagViewModel
import org.koin.android.ext.android.inject

class VeloeTagFragment :
    BNWFViewModelFragment<FragmentVeloeTagBinding, VeloeTagViewModel>(), TagQuantitySelectListener {

    private val product by lazy {
        checkNotNull(arguments?.getSerializable(Const.Extras.STORE_PRODUCT)?.safeHeritage<StoreProduct>())
    }

    private var quantityTag: Int? = null

    override val bindingVariable: Int = BR.veloeTagViewModel

    override val getLayoutId: Int = R.layout.fragment_veloe_tag

    override val viewModel: VeloeTagViewModel by inject()

    override fun tag(): String = VeloeOpKey.VELOE_TAG.name

    override fun getStatusBarAppearance(): StatusBarColor = VeloeOpKey.VELOE_TAG.theme

    override fun getToolbar(): Toolbar? = binding?.veloeTagAppBar?.navToolbar.apply {
        this?.navigationIcon = context?.let {
            ContextCompat.getDrawable(
                it,
                br.com.digio.uber.common.R.drawable.ic_back_arrow
            )
        }
        this?.setBackgroundColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.grey_1F1F1F))
        this?.setTitleTextColor(ContextCompat.getColor(requireContext(), br.com.digio.uber.common.R.color.white))
    }

    override fun onNavigationClick(view: View) {
        callGoBack()
    }

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp = true

    override fun initialize() {
        super.initialize()

        setObserver()

        viewModel.initializer { onItemClicked(it) }

        viewModel.setTagQuantity(resources.getString(R.string.select_quantity), false)
    }

    private fun setObserver() {
        viewModel.orderRegisterResponse.observe(
            this,
            Observer {
                if (it.responseCode == SubproductOrderResponseEnum.ACCEPTED.code) {
                    clearAllBackStack()
                    callGoTo(VeloeOpKey.VELOE_SUCCESS.name)
                }
            }
        )
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.veloe_tag_card_carrier -> callQuantityTagDialog()
            R.id.veloe_tag_btn_continue -> setTagOrder()
        }
    }

    private fun callQuantityTagDialog() {
        safeLet(activity?.supportFragmentManager, product.subproducts?.get(0)?.maxTagAmount) { activityLet, maxTag ->
            VeloeQuantityTagsDialog.newInstance(this, maxTag).show(activityLet, null)
        }
    }

    override fun quantitySelected(quantity: Int) {
        quantityTag = quantity

        val quantityText =
            if (quantity == 1) String.format(resources.getString(R.string.quantity_tag, quantity.toString()))
            else String.format(resources.getString(R.string.quantity_tags, quantity.toString()))

        viewModel.setTagQuantity(quantityText, true)
    }

    private fun setTagOrder() {
        quantityTag?.let { viewModel.orderRequest(product, it) }
    }

    companion object : FragmentNewInstance<VeloeTagFragment>(::VeloeTagFragment)
}