package br.com.digio.uber.veloe.di

import br.com.digio.uber.veloe.ui.viewmodel.VeloeHomeViewModel
import br.com.digio.uber.veloe.ui.viewmodel.VeloeQuantityTagsViewModel
import br.com.digio.uber.veloe.ui.viewmodel.VeloeSuccessViewModel
import br.com.digio.uber.veloe.ui.viewmodel.VeloeTagViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val veloeViewModelModule = module {
    viewModel { VeloeHomeViewModel() }
    viewModel { VeloeTagViewModel(get()) }
    viewModel { VeloeSuccessViewModel() }
    viewModel { VeloeQuantityTagsViewModel() }
}