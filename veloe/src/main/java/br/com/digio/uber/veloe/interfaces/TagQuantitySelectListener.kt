package br.com.digio.uber.veloe.interfaces

interface TagQuantitySelectListener {
    fun quantitySelected(quantity: Int)
}