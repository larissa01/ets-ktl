package br.com.digio.uber.veloe.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel

class VeloeQuantityTagsViewModel : BaseViewModel() {
    val listChoice: MutableLiveData<List<Int>> = MutableLiveData()

    fun setChoice(values: List<Int>) {
        listChoice.value = values
    }
}