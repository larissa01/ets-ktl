plugins {
    id("com.android.library")
    kotlin("android")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.library")
}

configureAndroidLibrary(
    releaseBuildTypeConfig = {
        buildConfigField("Boolean", "ENABLE_CACHE_REQUEST", "true")
    },
    homolBuildTypeConfig = {
        buildConfigField("Boolean", "ENABLE_CACHE_REQUEST", "true")
    },
    debugBuildTypeConfig = {
        buildConfigField("Boolean", "ENABLE_CACHE_REQUEST", "true")
    }
)

dependencies {
    implementation(Dependencies.ROOM)
    implementation(Dependencies.RETROFIT)
    implementation(Dependencies.GSON)
    implementation(Dependencies.LIFECYCLE_LIVEDATA)
    implementation(Dependencies.HAWK)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.GOOGLE_PLAY_SERVICES_LOCATION)
    implementation(Dependencies.SALESFORCE_MARKETING_CLOUD)

    implementation(project(":gateway"))
    implementation(project(":dataset"))
    implementation(project(":db"))
    implementation(project(":model"))
    implementation(project(":util"))
    implementation(project(":abstractFirebase"))
    implementation(project(":gemalto"))
    implementation(project(":marketingCloud"))

    testImplementation(TestDependencies.JUNIT)
    testImplementation(TestDependencies.MOCKK)
    testImplementation(TestDependencies.COROUTINESTEST)
}