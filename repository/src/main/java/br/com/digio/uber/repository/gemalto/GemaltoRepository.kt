package br.com.digio.uber.repository.gemalto

import br.com.digio.uber.model.gemalto.Enroll

interface GemaltoRepository {
    suspend fun getUserEroll(cpf: String): Enroll?
    suspend fun clearTokenCpf(cpf: String)
    suspend fun getOtp(values: Map<String, Any>, cpf: String?): String
    suspend fun checkHaveARegiseredUser(cpf: String): Boolean
}