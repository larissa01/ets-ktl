package br.com.digio.uber.repository.store

import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.store.StoreCatalog
import br.com.digio.uber.model.store.StoreOrder
import br.com.digio.uber.model.store.StoreOrderRegisterResponse
import br.com.digio.uber.model.store.StoreOrderResponse

interface StoreRepository {
    suspend fun getStoreCatalog(): StoreCatalog
    suspend fun registerStoreOrder(order: StoreOrder): StoreOrderRegisterResponse
    suspend fun getStoreOrders(page: Int): ResponseList<StoreOrderResponse>
}