package br.com.digio.uber.repository.virtualcard

import br.com.digio.uber.dataset.virtual.card.VirtualCardDataSet
import br.com.digio.uber.model.card.VirtualCardResponse
import com.orhanobut.hawk.Hawk
import retrofit2.Response

interface VirtualCardRepository {
    suspend fun getVirtualCard(cardId: Long, hash: String): VirtualCardResponse
    suspend fun postVirtualCardRegenerate(cardId: Long): VirtualCardResponse
    suspend fun postVirtualCardBlock(cardId: Long): Response<Void>
    suspend fun postVirtualCardUnblock(cardId: Long): Response<Void>
    suspend fun postVirtualCardCurrent(cardId: Long): Boolean
    suspend fun getVirtualCardCurrent(): Long
}

class VirtualCardRepositoryImpl(
    private val virtualCardDataSet: VirtualCardDataSet
) : VirtualCardRepository {
    override suspend fun getVirtualCard(cardId: Long, hash: String): VirtualCardResponse =
        virtualCardDataSet.getVirtualCard(cardId, hash)

    override suspend fun postVirtualCardRegenerate(cardId: Long): VirtualCardResponse =
        virtualCardDataSet.postVirtualCardRegenerate(cardId)

    override suspend fun postVirtualCardBlock(cardId: Long) =
        virtualCardDataSet.postVirtualCardBlock(cardId)

    override suspend fun postVirtualCardUnblock(cardId: Long) =
        virtualCardDataSet.postVirtualCardUnblock(cardId)

    override suspend fun postVirtualCardCurrent(cardId: Long) =
        Hawk.put(KEY_PARAMETER_VIRTUAL_CARD_CURRENT, cardId)

    override suspend fun getVirtualCardCurrent(): Long =
        Hawk.get(KEY_PARAMETER_VIRTUAL_CARD_CURRENT, -1)

    companion object {
        private const val KEY_PARAMETER_VIRTUAL_CARD_CURRENT: String = "KEY_PARAMETER_VIRTUAL_CARD_CURRENT"
    }
}