package br.com.digio.uber.repository.digioTestRepository

import br.com.digio.uber.dataset.digioTest.DigioTestDataSet
import br.com.digio.uber.model.digioTests.ProductsList

interface DigioTestRepository {
    suspend fun requestDigioTestPayload(): ProductsList
}

class DigioTestRepositoryImpl(
    private val digioTestDataSet: DigioTestDataSet
) : DigioTestRepository {

    override suspend fun requestDigioTestPayload(): ProductsList =
        digioTestDataSet.getProducts()
}