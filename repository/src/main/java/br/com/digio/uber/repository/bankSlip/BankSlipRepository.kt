package br.com.digio.uber.repository.bankSlip

import br.com.digio.uber.model.bankSlip.BankSlipRequest
import br.com.digio.uber.model.bankSlip.BankSlipResponse
import br.com.digio.uber.model.bankSlip.BankSlipStatus
import br.com.digio.uber.model.bankSlip.FileResponse
import br.com.digio.uber.model.bankSlip.GeneratedBankSlipResponse

/**
 * @author Marlon D. Rocha
 * @since 16/10/20
 */
interface BankSlipRepository {
    suspend fun getBankSlipStatus(): BankSlipStatus
    suspend fun getBankSlipByUuid(uuid: String): FileResponse
    suspend fun generateBankSlip(bankSlipRequest: BankSlipRequest): BankSlipResponse
    suspend fun getGeneratedBankSlips(status: String?): GeneratedBankSlipResponse
}