package br.com.digio.uber.repository.location

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import br.com.digio.uber.util.Const.Utils.MIN_DISTANCE_GPS
import br.com.digio.uber.util.Const.Utils.MIN_TIME_GPS
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.lang.IllegalStateException
import kotlin.coroutines.suspendCoroutine

class LocationRepositoryImpl(
    private val context: Context
) : LocationRepository, LocationListener {

    override suspend fun isGPSEnabled(): Boolean =
        (context.getSystemService(Context.LOCATION_SERVICE) as LocationManager)
            .isProviderEnabled(LocationManager.GPS_PROVIDER)

    @SuppressLint("MissingPermission")
    override fun onLocationChanged(location: Location) {
        Timber.i("Location Changed ${location.latitude},${location.longitude}")
    }

    @SuppressLint("MissingPermission")
    override suspend fun getLastKnowLocation(): Location =
        withContext(IO) {
            suspendCoroutine {
                LocationServices.getFusedLocationProviderClient(context)
                    .lastLocation
                    .addOnSuccessListener { location: Location? ->
                        val finalLocation = location ?: getLastKnownLocationOld()
                            ?: throw IllegalStateException("could not get gps location!")
                        it.resumeWith(Result.success(finalLocation))
                    }
                    .addOnFailureListener { e ->
                        it.resumeWith(Result.failure(e))
                    }
            }
        }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocationOld(): Location? {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            MIN_TIME_GPS,
            MIN_DISTANCE_GPS,
            this
        )
        locationManager.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER,
            MIN_TIME_GPS,
            MIN_DISTANCE_GPS,
            this
        )

        val providers = locationManager.getProviders(true)
        var bestLocation: Location? = null
        for (provider in providers) {
            val lastLocation = locationManager.getLastKnownLocation(provider) ?: continue
            if (bestLocation == null || lastLocation.accuracy < bestLocation.accuracy) {
                bestLocation = lastLocation
            }
        }
        return bestLocation.also {
            locationManager.removeUpdates(this)
        }
    }
}