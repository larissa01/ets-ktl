package br.com.digio.uber.repository.cardReceived

import br.com.digio.uber.dataset.cardReceived.CardReceivedDataSet
import br.com.digio.uber.model.cardReceived.CardReceivedActivationRequest
import retrofit2.Response

class CardReceivedRepositoryImpl constructor(
    private val cardReceivedDataSet: CardReceivedDataSet
) : CardReceivedRepository {
    override suspend fun postCardReceivedActivation(activationRequest: CardReceivedActivationRequest):
        Response<Void> = cardReceivedDataSet.postValidateCardActivation(activationRequest)
}