package br.com.digio.uber.repository.card

import br.com.digio.uber.dataset.card.CardDataSet
import br.com.digio.uber.model.card.CardResponse
import br.com.digio.uber.model.card.TypeCard
import br.com.digio.uber.repository.cache.util.getFromHawkCache
import br.com.digio.uber.util.Const
import retrofit2.Response

interface CardRepository {
    suspend fun getCardList(): List<CardResponse>
    suspend fun getCardType(typeCard: TypeCard): CardResponse?
    suspend fun postCardBlock(cardId: Long): Response<Void>
    suspend fun postCardUnblock(cardId: Long): Response<Void>
}
class CardRepositoryImpl(
    private val cardDataSet: CardDataSet
) : CardRepository {
    override suspend fun getCardList(): List<CardResponse> =
        cardDataSet.getCardList()

    override suspend fun getCardType(typeCard: TypeCard): CardResponse? =
        Const.Cache.KEY_CARD_BY_TYPE_CACHE.getFromHawkCache {
            cardDataSet.getCardByType(typeCard)
        }

    override suspend fun postCardBlock(cardId: Long): Response<Void> =
        cardDataSet.postCardBlock(cardId)

    override suspend fun postCardUnblock(cardId: Long): Response<Void> =
        cardDataSet.postCardUnblock(cardId)
}