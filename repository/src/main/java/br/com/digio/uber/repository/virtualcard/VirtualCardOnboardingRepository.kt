package br.com.digio.uber.repository.virtualcard

import br.com.digio.uber.model.virtualCard.VirtualCardOnboardingResponse

interface VirtualCardOnboardingRepository {
    suspend fun getOnboardingContent(): VirtualCardOnboardingResponse
    suspend fun getVirtualCardOnboarding(): Boolean
    suspend fun postVirtualCardOnboarding(value: Boolean): Boolean
    suspend fun getFirstAccessOnboarding(): Boolean
    suspend fun postFirstAccessOnboarding(value: Boolean): Boolean
}