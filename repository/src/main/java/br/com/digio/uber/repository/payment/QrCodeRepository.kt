package br.com.digio.uber.repository.payment

import br.com.digio.uber.dataset.payment.QrCodeDataSet
import br.com.digio.uber.model.payment.PayWithQrCodeRequest
import br.com.digio.uber.model.payment.QrCodeRequest
import br.com.digio.uber.model.payment.QrCodeResponse
import retrofit2.Response

interface QrCodeRepository {
    suspend fun postQrCode(qrCodeValue: String): QrCodeResponse
    suspend fun postPayWithQrCode(qrCodeRequest: PayWithQrCodeRequest, hash: String): Response<Void>
}

class QrCodeRepositoryImpl(
    private val qrCodeDataSet: QrCodeDataSet
) : QrCodeRepository {
    override suspend fun postQrCode(qrCodeValue: String): QrCodeResponse =
        qrCodeDataSet.postQrCode(QrCodeRequest(qrCodeValue))

    override suspend fun postPayWithQrCode(qrCodeRequest: PayWithQrCodeRequest, hash: String): Response<Void> =
        qrCodeDataSet.postPayWithQrCode(qrCodeRequest, hash)
}