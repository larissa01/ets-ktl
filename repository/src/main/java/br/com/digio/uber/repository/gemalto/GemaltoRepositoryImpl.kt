package br.com.digio.uber.repository.gemalto

import br.com.digio.uber.dataset.account.AccountDataSet
import br.com.digio.uber.db.UberDatabase
import br.com.digio.uber.gemalto.GemaltoSetupLogic
import br.com.digio.uber.gemalto.model.GemaltoCredential
import br.com.digio.uber.model.gemalto.Enroll
import br.com.digio.uber.model.request.DeviceInfo
import br.com.digio.uber.repository.gemalto.exception.GemaltoEnrollException
import br.com.digio.uber.util.encrypt.decryptOrNull
import kotlinx.coroutines.Deferred
import retrofit2.HttpException
import java.net.HttpURLConnection

class GemaltoRepositoryImpl constructor(
    private val gemaltoSetupLogic: GemaltoSetupLogic,
    private val accountDataset: AccountDataSet,
    private val uberDatabase: UberDatabase,
    private val deviceInfoAsync: Deferred<DeviceInfo>
) : GemaltoRepository {

    override suspend fun getUserEroll(cpf: String): Enroll? {
        val gemaltoCredential = gemaltoSetupLogic.getCredentials(cpf)
        return if (gemaltoCredential != null) {
            gemaltoSetupLogic.getTokenWithoutResult(cpf, gemaltoCredential.registrationCode)
            gemaltoCredential.getEnrollFromGemaltoCredential()
        } else {
            val enroll = try {
                accountDataset.getUserEnroll(deviceInfoAsync.await())
            } catch (e: HttpException) {
                if (e.code() in HttpURLConnection.HTTP_INTERNAL_ERROR..HttpURLConnection.HTTP_VERSION) {
                    throw GemaltoEnrollException(e)
                } else {
                    throw e
                }
            }
            gemaltoSetupLogic.getTokenWithoutResult(cpf, enroll.registrationCode)
            gemaltoSetupLogic.saveCredentials(
                GemaltoCredential(
                    cpf,
                    enroll.pin,
                    enroll.registrationCode
                )
            )
            enroll
        }
    }

    override suspend fun clearTokenCpf(cpf: String) =
        gemaltoSetupLogic.clearTokenCpf(cpf)

    override suspend fun getOtp(values: Map<String, Any>, cpf: String?): String {
        val cpfChecked = cpf ?: uberDatabase
            .credentialsDao()
            .getCredentials()
            ?.cpfCrypted
            ?.decryptOrNull()
        val newValueInOrder = values.toSortedMap()
        return if (cpfChecked != null) {
            val gemaltoCredential = gemaltoSetupLogic.getCredentials(cpfChecked)
            if (gemaltoCredential != null) {
                val otp = gemaltoSetupLogic.generateOTP(newValueInOrder, gemaltoCredential)
                otp
            } else {
                val errol = getUserEroll(cpfChecked)
                if (errol != null) {
                    gemaltoSetupLogic
                        .generateOTP(
                            newValueInOrder,
                            GemaltoCredential(cpfChecked, errol.pin, errol.registrationCode)
                        )
                } else {
                    throw IllegalStateException("Erro in eroll")
                }
            }
        } else {
            throw IllegalStateException("cpf is null")
        }
    }

    override suspend fun checkHaveARegiseredUser(cpf: String): Boolean =
        gemaltoSetupLogic.checkCpfHasToken(cpf)
}