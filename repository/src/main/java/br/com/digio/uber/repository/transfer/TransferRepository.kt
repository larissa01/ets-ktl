@file:Suppress("TooManyFunctions")

package br.com.digio.uber.repository.transfer

import br.com.digio.uber.model.transfer.AccountDomainResponse
import br.com.digio.uber.model.transfer.BankDomainResponse
import br.com.digio.uber.model.transfer.BeneficiariesResponse
import br.com.digio.uber.model.transfer.BeneficiaryRequest
import br.com.digio.uber.model.transfer.BeneficiaryResponse
import br.com.digio.uber.model.transfer.ExternalTransferRequest
import br.com.digio.uber.model.transfer.InternalTransferRequest
import br.com.digio.uber.model.transfer.PixBankDomainResponse
import br.com.digio.uber.model.transfer.ThirdPartyAccountListResponse
import br.com.digio.uber.model.transfer.ThirdPartyAccountResponse
import br.com.digio.uber.model.transfer.TransferResponse
import br.com.digio.uber.model.transfer.TransferStatus
import br.com.digio.uber.util.enumTest.CachePolicy

/**
 * @author Marlon D. Rocha
 * @since 30/10/20
 */
interface TransferRepository {
    suspend fun getAccount(): AccountDomainResponse
    suspend fun getBanks(cachePolicy: CachePolicy): BankDomainResponse
    suspend fun getPixBanks(cachePolicy: CachePolicy): PixBankDomainResponse
    suspend fun getPopularBanks(cachePolicy: CachePolicy): BankDomainResponse
    suspend fun getPopularPixBanks(cachePolicy: CachePolicy): PixBankDomainResponse
    suspend fun getTransferStatus(): TransferStatus
    suspend fun confirmTransferExternalSameOwner(transferRequest: ExternalTransferRequest): TransferResponse
    suspend fun confirmTransferExternalThirdParty(transferRequest: ExternalTransferRequest): TransferResponse
    suspend fun confirmTransferInternal(transferRequest: InternalTransferRequest): TransferResponse
    suspend fun getTedBeneficiaries(): BeneficiariesResponse
    suspend fun deleteTedBeneficiary(beneficiaryId: String): BeneficiaryResponse
    suspend fun putTedBeneficiary(
        beneficiaryId: String,
        beneficiaryRequest: BeneficiaryRequest
    ): BeneficiaryResponse
    suspend fun getP2PBeneficiaries(): BeneficiariesResponse
    suspend fun deleteP2PBeneficiary(beneficiaryId: String): BeneficiaryResponse
    suspend fun putP2PBeneficiary(
        beneficiaryId: String,
        beneficiaryRequest: BeneficiaryRequest
    ): BeneficiaryResponse
    suspend fun getAccountByCPF(cpf: String): ThirdPartyAccountListResponse
    suspend fun searchAccount(agency: String, account: String): ThirdPartyAccountResponse
}