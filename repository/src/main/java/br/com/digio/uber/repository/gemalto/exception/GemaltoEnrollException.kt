package br.com.digio.uber.repository.gemalto.exception

import retrofit2.HttpException

class GemaltoEnrollException constructor(
    httpException: HttpException
) : Exception(httpException)