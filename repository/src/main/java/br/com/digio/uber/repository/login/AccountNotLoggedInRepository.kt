package br.com.digio.uber.repository.login

import androidx.lifecycle.LiveData
import br.com.digio.uber.model.UberFlagsPersistNotLoggedIn
import br.com.digio.uber.model.change.password.ChangePasswordReq
import br.com.digio.uber.model.login.LoginRes
import br.com.digio.uber.model.login.repository.model.Login
import br.com.digio.uber.model.resetpassword.ResetPasswordReq
import br.com.digio.uber.model.resetpassword.ResetPasswordRes

interface AccountNotLoggedInRepository {
    suspend fun getCpfSaved(): String?
    suspend fun saveCpf(login: String)
    suspend fun deleteCpf()
    suspend fun getUberFlagsPersistNotLoggedIn(): UberFlagsPersistNotLoggedIn?
    fun getUberFlagsPersistNotLoggedInLiveData(): LiveData<UberFlagsPersistNotLoggedIn>
    suspend fun updateUberFlagsPersistNotLoggedIn(
        uberFlagsPersistNotLoggedIn: UberFlagsPersistNotLoggedIn
    ): UberFlagsPersistNotLoggedIn?
    suspend fun makeLogin(login: Login): LoginRes
    suspend fun resetPassword(req: ResetPasswordReq): ResetPasswordRes
    suspend fun changePassword(changePasswordReq: ChangePasswordReq)
}