package br.com.digio.uber.repository.repository

import br.com.digio.uber.dataset.receipt.ReceiptDataSet
import br.com.digio.uber.model.receipt.Receipt

interface ReceiptRepository {
    suspend fun requestReceipt(id: String): Receipt
}

class ReceiptRepositoryImpl(
    private val receiptDataSet: ReceiptDataSet
) : ReceiptRepository {

    override suspend fun requestReceipt(id: String): Receipt =
        receiptDataSet.getReceipt(id)
}