package br.com.digio.uber.repository.income

import br.com.digio.uber.dataset.income.IncomeDataSet
import br.com.digio.uber.model.income.AvailableMonthsResponse
import br.com.digio.uber.model.income.DetailDepositResponse
import br.com.digio.uber.model.income.ListIncomesResponse

class IncomeRepositoryImpl(
    private val incomeDataSet: IncomeDataSet
) : IncomeRepository {
    override suspend fun getAvailableMonths(): AvailableMonthsResponse =
        incomeDataSet.getAvailableMonths()

    override suspend fun getIncomeDetails(yearMonth: String): ListIncomesResponse =
        incomeDataSet.getIncomeDetails(yearMonth)

    override suspend fun getDepositDetails(depositId: Int): DetailDepositResponse =
        incomeDataSet.getDepositDetails(depositId)
}