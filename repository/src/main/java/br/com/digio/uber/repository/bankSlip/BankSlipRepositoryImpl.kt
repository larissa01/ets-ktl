package br.com.digio.uber.repository.bankSlip

import br.com.digio.uber.dataset.bankSlip.BankSlipDataSet
import br.com.digio.uber.model.bankSlip.BankSlipRequest
import br.com.digio.uber.model.bankSlip.BankSlipResponse
import br.com.digio.uber.model.bankSlip.BankSlipStatus
import br.com.digio.uber.model.bankSlip.FileResponse
import br.com.digio.uber.model.bankSlip.GeneratedBankSlipResponse

class BankSlipRepositoryImpl(
    private val bankSlipDataSet: BankSlipDataSet
) : BankSlipRepository {

    override suspend fun getBankSlipStatus(): BankSlipStatus =
        bankSlipDataSet.getBankSlipStatus()

    override suspend fun getBankSlipByUuid(uuid: String): FileResponse =
        bankSlipDataSet.getBankSlipByUuid(uuid)

    override suspend fun generateBankSlip(bankSlipRequest: BankSlipRequest): BankSlipResponse =
        bankSlipDataSet.generateBankSlip(bankSlipRequest)

    override suspend fun getGeneratedBankSlips(status: String?): GeneratedBankSlipResponse =
        bankSlipDataSet.getGeneratedBankSlips(status)
}