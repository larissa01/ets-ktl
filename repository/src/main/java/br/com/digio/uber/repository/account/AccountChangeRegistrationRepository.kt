package br.com.digio.uber.repository.account

import br.com.digio.uber.model.change.registration.AddressRegistrationRequest
import br.com.digio.uber.model.change.registration.ChangeRegistrationData
import br.com.digio.uber.model.change.registration.ChangeRegistrationRequest
import br.com.digio.uber.model.change.registration.ZipCodeRes

interface AccountChangeRegistrationRepository {
    suspend fun getChangeRegistrationData(): ChangeRegistrationData
    suspend fun getZipCode(cep: String): ZipCodeRes
    suspend fun changeRegistration(changeRegistrationItemList: ChangeRegistrationRequest)
    suspend fun updateAddress(param: AddressRegistrationRequest)
}