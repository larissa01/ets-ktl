package br.com.digio.uber.repository.withdraw

import br.com.digio.uber.model.bankSlip.CustomerLimitsResponse
import br.com.digio.uber.model.withdraw.WithdrawReq

interface WithdrawRepository {
    suspend fun getWithdrawStatus(): CustomerLimitsResponse
    suspend fun makeWithdraw(withDrawRequest: WithdrawReq)
}