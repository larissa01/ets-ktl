package br.com.digio.uber.repository.cashback

import br.com.digio.uber.dataset.cashback.CashbackDataSet
import br.com.digio.uber.model.cashback.SendCashbackRequest

class CashbackRepositoryImpl(
    private val cashbackDataSet: CashbackDataSet
) : CashbackRepository {

    override suspend fun getPurchasesAmount(status: String) =
        cashbackDataSet.getPurchasesAmount(status)

    override suspend fun getCashbackStores(quantityItens: Int) =
        cashbackDataSet.getCashbackStores(quantityItens)

    override suspend fun getCashbackTerms() =
        cashbackDataSet.getCashbackTerms()

    override suspend fun acceptCashbackTerms() =
        cashbackDataSet.acceptCashbackTerms()

    override suspend fun getCashbackTermsAccept() =
        cashbackDataSet.getCashbackTermsAccept()

    override suspend fun getCashbackCustomer() =
        cashbackDataSet.getCashbackCustomer()

    override suspend fun getCashbackDetails(type: String) =
        cashbackDataSet.getCashbackDetails(type)

    override suspend fun getStoreOffers(storeId: Long) =
        cashbackDataSet.getStoreOffers(storeId)

    override suspend fun getComissionCategory(id: Long) =
        cashbackDataSet.getComissionCategory(id)

    override suspend fun getCashbackPurchases(status: String, page: Int) =
        cashbackDataSet.getCashbackPurchases(status, page)

    override suspend fun sendCashbackAccount(receiptForm: String, purchases: SendCashbackRequest) =
        cashbackDataSet.sendCashbackAccount(receiptForm, purchases)
}