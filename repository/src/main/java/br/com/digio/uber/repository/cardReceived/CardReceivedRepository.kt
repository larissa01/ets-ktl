package br.com.digio.uber.repository.cardReceived

import br.com.digio.uber.model.cardReceived.CardReceivedActivationRequest
import retrofit2.Response

interface CardReceivedRepository {
    suspend fun postCardReceivedActivation(activationRequest: CardReceivedActivationRequest): Response<Void>
}