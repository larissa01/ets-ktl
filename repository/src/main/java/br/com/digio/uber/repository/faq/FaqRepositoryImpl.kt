package br.com.digio.uber.repository.faq

import br.com.digio.uber.dataset.faq.FaqDataset
import br.com.digio.uber.db.UberDatabase
import br.com.digio.uber.db.file.FaqFileReader
import br.com.digio.uber.db.tables.FaqMetadata
import br.com.digio.uber.model.faq.FaqChatResponse
import br.com.digio.uber.model.faq.FaqChatUrlRequest
import br.com.digio.uber.model.faq.FaqPdfResponse
import br.com.digio.uber.model.faq.FaqSubjectResponse
import br.com.digio.uber.repository.faq.mapper.SubjectListMapper
import br.com.digio.uber.repository.faq.mapper.SubjectResponseMapper
import br.com.digio.uber.util.Const
import java.io.IOException
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class FaqRepositoryImpl(
    private val uberDatabase: UberDatabase,
    private val remoteFaqDataset: FaqDataset,
    private val faqFileReader: FaqFileReader
) : FaqRepository {

    companion object {
        private const val STATUS_CODE_OK = 200
    }

    override suspend fun getFaqCategories(): List<FaqSubjectResponse> {
        val subjects = uberDatabase.faqDao().getSubjects()
        return if (subjects.isNotEmpty()) {
            SubjectListMapper().map(subjects)
        } else {
            faqFileReader.readFaqFile()?.faqContent ?: listOf()
        }
    }

    override suspend fun updateFaq() {
        val faqVersion =
            uberDatabase.faqDao().getFaqMetadata()?.version ?: faqFileReader.readFaqFile()?.faqVersion ?: ""
        val response = remoteFaqDataset.getFaq(faqVersion)
        if (response.update) {
            uberDatabase.faqDao().nukedTable()
            uberDatabase.faqDao().saveFaqMetadata(FaqMetadata(response.faqVersion ?: ""))
            uberDatabase.faqDao().saveSubjects(response.faqContent?.map { SubjectResponseMapper().map(it) } ?: listOf())
        }
    }

    override suspend fun getPdf(): FaqPdfResponse {
        return try {
            val url = URL(Const.URI.URL_PDF)
            val connection = url.openConnection() as HttpsURLConnection
            if (connection.responseCode == STATUS_CODE_OK) {
                FaqPdfResponse(true, connection.inputStream)
            } else {
                FaqPdfResponse(false, null)
            }
        } catch (e: IOException) {
            FaqPdfResponse(false, null)
        }
    }

    override suspend fun getChatLink(request: FaqChatUrlRequest): FaqChatResponse {
        return remoteFaqDataset.getFaqChatUrl(request)
    }
}