package br.com.digio.uber.repository.statement

import br.com.digio.uber.dataset.statement.StatementDataSet
import br.com.digio.uber.model.statement.FutureEntriesResponse
import br.com.digio.uber.model.statement.PastEntriesRequest
import br.com.digio.uber.model.statement.PastEntriesResponse
import br.com.digio.uber.model.statement.StatementDetailResponse

class StatementRepositoryImpl constructor(
    private val statementDataSet: StatementDataSet
) : StatementRepository {
    override suspend fun getPastEntries(req: PastEntriesRequest): PastEntriesResponse =
        statementDataSet.getPastEntries(req)

    override suspend fun getFutureEntries(): FutureEntriesResponse =
        statementDataSet.getFutureEntries()

    override suspend fun deleteScheduledEntry(param: String) =
        statementDataSet.deleteScheduledEntry(param)

    override suspend fun getStatementDetails(id: String): StatementDetailResponse =
        statementDataSet.getStatementDetails(id)
}