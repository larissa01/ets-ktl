package br.com.digio.uber.repository.withdraw

import br.com.digio.uber.dataset.withdraw.WithdrawDataSet
import br.com.digio.uber.model.bankSlip.CustomerLimitsResponse
import br.com.digio.uber.model.withdraw.WithdrawReq

class WithdrawRepositoryImpl(
    private val withdrawDataSet: WithdrawDataSet
) : WithdrawRepository {
    override suspend fun getWithdrawStatus(): CustomerLimitsResponse =
        withdrawDataSet.getWithdrawStatus()

    override suspend fun makeWithdraw(withDrawRequest: WithdrawReq) =
        withdrawDataSet.makeWithdraw(withDrawRequest)
}