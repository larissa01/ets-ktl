package br.com.digio.uber.repository.transfer

import br.com.digio.uber.db.tables.BankTable
import br.com.digio.uber.db.tables.PixBankTable
import br.com.digio.uber.db.tables.PopularBankTable
import br.com.digio.uber.db.tables.PopularPixBankTable
import br.com.digio.uber.model.transfer.BankDomain
import br.com.digio.uber.model.transfer.PixBankDomain

/**
 * @author Marlon D. Rocha
 * @since 04/11/20
 */

fun BankDomain.toDAO(): BankTable =
    BankTable(this.code, this.name)

fun BankDomain.toPopularDAO(): PopularBankTable =
    PopularBankTable(this.code, this.name)

fun BankTable.toDomain(): BankDomain =
    BankDomain(this.name, this.code)

fun PopularBankTable.toDomain(): BankDomain =
    BankDomain(this.name, this.code)

fun PixBankDomain.toPixBankDAO(): PixBankTable =
    PixBankTable(this.code, this.name)

fun PixBankDomain.toPopularPixBankDAO(): PopularPixBankTable =
    PopularPixBankTable(this.code, this.name)

fun PixBankTable.toDomain(): PixBankDomain =
    PixBankDomain(this.name, this.code)

fun PopularPixBankTable.toDomain(): PixBankDomain =
    PixBankDomain(this.name, this.code)