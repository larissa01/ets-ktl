package br.com.digio.uber.repository.home

import br.com.digio.uber.model.config.ConfigFeature
import br.com.digio.uber.model.config.FeatureItem
import br.com.digio.uber.repository.cache.util.invalidateHawkCache
import br.com.digio.uber.util.Const
import com.orhanobut.hawk.Hawk

interface ConfigFeatureRepository {
    suspend fun getFeatureConfig(): ConfigFeature
    suspend fun getFeatureList(): List<FeatureItem>
    suspend fun getTagList(): List<FeatureItem>
    suspend fun getShortcutList(): List<FeatureItem>
    suspend fun getShortcutTrackingCardList(): List<FeatureItem>
    suspend fun clearHomeCache()
}

class ConfigFeatureRepositoryImpl : ConfigFeatureRepository {

    // Todo temporary get dataset API
    private val configFeatureDefault: ConfigFeature = ConfigFeature(
        vesion = VERSION_FEATURE_CONFG_TEMP,

        tagList = listOf(
            FeatureItem(
                id = "EXTRACT",
                label = "Extrato"
            ),
            FeatureItem(
                id = "INCOME",
                label = "Rendimentos"
            ),
            FeatureItem(
                id = "SPENDING_CONTROL",
                label = "Controle de gastos",
                visible = false
            )
        ),
        featureList = listOf(
            FeatureItem(
                id = "PAY",
                label = "Pagar"
            ),
            FeatureItem(
                id = "PIX",
                label = "Pix"
            ),
            FeatureItem(
                id = "TRANSFER",
                label = "Transferir"
            ),
            FeatureItem(
                id = "RECHARGE",
                label = "Recarga"
            ),
            FeatureItem(
                id = "RECEIVE",
                label = "Receber"
            ),
            FeatureItem(
                id = "WITHDRAW",
                label = "Sacar"
            )
        ),
        shortcutList = listOf(
            FeatureItem(
                id = "CASHBACK",
                label = "Cashback"
            ),
            FeatureItem(
                id = "PAY_WITH_QR_CODE",
                label = "Pagar com\nCielo QR Code"
            ),
            FeatureItem(
                id = "FRANCHISE",
                label = "Pacote\nde serviços"
            )
        ),
        shortcutTrackingCardList = listOf(
            FeatureItem(
                id = "TRACK_CARD",
                label = "Rastrear cartão"
            ),
            FeatureItem(
                id = "RECEIVED_MY_CARD",
                label = "Recebi\nmeu cartão"
            )
        )
    )

    override suspend fun getFeatureConfig(): ConfigFeature {
        return (
            if (Hawk.contains(KEY_PARAMETER_NAME)) {
                val configFeatureValue: ConfigFeature? = Hawk.get(KEY_PARAMETER_NAME, null)
                if (configFeatureValue != null && configFeatureValue.vesion == VERSION_FEATURE_CONFG_TEMP) {
                    configFeatureValue
                } else {
                    Hawk.put(KEY_PARAMETER_NAME, configFeatureDefault)
                    Hawk.get(KEY_PARAMETER_NAME)
                }
            } else {
                Hawk.put(KEY_PARAMETER_NAME, configFeatureDefault)
                Hawk.get(KEY_PARAMETER_NAME)
            }
            )
    }

    override suspend fun getFeatureList(): List<FeatureItem> =
        getFeatureConfig().featureList.filter { it.visible == true }

    override suspend fun getTagList(): List<FeatureItem> =
        getFeatureConfig().tagList.filter { it.visible == true }

    override suspend fun getShortcutList(): List<FeatureItem> =
        getFeatureConfig().shortcutList.filter { it.visible == true }

    override suspend fun getShortcutTrackingCardList(): List<FeatureItem> =
        getFeatureConfig().shortcutTrackingCardList.filter { it.visible == true }

    override suspend fun clearHomeCache() {
        Const.Cache.KEY_BALANCE_CACHE.invalidateHawkCache<Any>()
        Const.Cache.KEY_ACCOUNT_CACHE.invalidateHawkCache<Any>()
        Const.Cache.KEY_REMUNERATION_INCOME_CACHE.invalidateHawkCache<Any>()
    }

    companion object {
        private const val KEY_PARAMETER_NAME: String = "FEATURE_CONFIG"
        private const val VERSION_FEATURE_CONFG_TEMP: String = "V03"
    }
}