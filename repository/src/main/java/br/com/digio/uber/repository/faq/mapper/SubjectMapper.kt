package br.com.digio.uber.repository.faq.mapper

import br.com.digio.uber.db.tables.FaqSubjectWithQuestions
import br.com.digio.uber.model.faq.FaqSubjectResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class SubjectMapper : AbstractMapper<FaqSubjectWithQuestions, FaqSubjectResponse> {

    override fun map(param: FaqSubjectWithQuestions): FaqSubjectResponse {
        return FaqSubjectResponse(
            param.subject.id,
            param.subject.subject,
            param.subject.icon,
            param.subject.client,
            param.questions.map { QuestionMapper().map(it) }
        )
    }
}