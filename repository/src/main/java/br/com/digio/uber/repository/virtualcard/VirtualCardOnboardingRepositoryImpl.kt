package br.com.digio.uber.repository.virtualcard

import br.com.digio.uber.dataset.virtual.card.VirtualCardOnboardingDataSet
import br.com.digio.uber.model.virtualCard.VirtualCardOnboardingResponse
import br.com.digio.uber.util.Const.KeyVirtualCardOnboarding.KEY_NAME_CARD_ONBOARDING_FIRST_ACCESS
import br.com.digio.uber.util.Const.KeyVirtualCardOnboarding.KEY_NAME_VIRTUAL_CARD_ONBOARDING_IS_DONE
import com.orhanobut.hawk.Hawk

class VirtualCardOnboardingRepositoryImpl constructor(
    private val virtualCardOnboardingDataSet: VirtualCardOnboardingDataSet
) : VirtualCardOnboardingRepository {
    override suspend fun getOnboardingContent(): VirtualCardOnboardingResponse =
        virtualCardOnboardingDataSet.getOnboardingContent()

    override suspend fun getVirtualCardOnboarding(): Boolean =
        if (Hawk.contains(KEY_NAME_VIRTUAL_CARD_ONBOARDING_IS_DONE)) {
            Hawk.get(KEY_NAME_VIRTUAL_CARD_ONBOARDING_IS_DONE, false)
        } else false

    override suspend fun postVirtualCardOnboarding(value: Boolean): Boolean {
        Hawk.put(KEY_NAME_VIRTUAL_CARD_ONBOARDING_IS_DONE, value)
        return Hawk.get(KEY_NAME_VIRTUAL_CARD_ONBOARDING_IS_DONE, false)
    }

    override suspend fun getFirstAccessOnboarding(): Boolean =
        if (Hawk.contains(KEY_NAME_CARD_ONBOARDING_FIRST_ACCESS)) {
            Hawk.get(KEY_NAME_CARD_ONBOARDING_FIRST_ACCESS, false)
        } else false

    override suspend fun postFirstAccessOnboarding(value: Boolean): Boolean {
        Hawk.put(KEY_NAME_CARD_ONBOARDING_FIRST_ACCESS, value)
        return Hawk.get(KEY_NAME_CARD_ONBOARDING_FIRST_ACCESS, false)
    }
}