package br.com.digio.uber.repository.statement

import br.com.digio.uber.model.statement.FutureEntriesResponse
import br.com.digio.uber.model.statement.PastEntriesRequest
import br.com.digio.uber.model.statement.PastEntriesResponse
import br.com.digio.uber.model.statement.StatementDetailResponse

interface StatementRepository {
    suspend fun getPastEntries(req: PastEntriesRequest): PastEntriesResponse
    suspend fun getFutureEntries(): FutureEntriesResponse
    suspend fun deleteScheduledEntry(param: String)
    suspend fun getStatementDetails(id: String): StatementDetailResponse
}