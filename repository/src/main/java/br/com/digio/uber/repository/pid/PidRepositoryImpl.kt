package br.com.digio.uber.repository.pid

import br.com.digio.uber.dataset.pid.PidDataset
import br.com.digio.uber.model.pid.PidReq
import br.com.digio.uber.model.pid.PidRes
import br.com.digio.uber.model.pid.PidStepReq
import br.com.digio.uber.model.pid.PidType

class PidRepositoryImpl constructor(
    private val pidDataset: PidDataset
) : PidRepository {
    override suspend fun firstPid(pidType: PidType): PidRes =
        pidDataset.firstPid(PidReq(pidType))

    override suspend fun pidValidate(pidRes: PidStepReq): PidRes =
        pidDataset.pidValidate(pidRes)
}