package br.com.digio.uber.repository.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import br.com.digio.uber.dataset.login.LoginNotLoggedInDataset
import br.com.digio.uber.db.UberDatabase
import br.com.digio.uber.db.tables.AccountNotLoggedTable
import br.com.digio.uber.db.tables.UberFlagsPersistNotLoggedInTable
import br.com.digio.uber.model.UberFlagsPersistNotLoggedIn
import br.com.digio.uber.model.change.password.ChangePasswordReq
import br.com.digio.uber.model.login.LoginRes
import br.com.digio.uber.model.login.repository.model.Login
import br.com.digio.uber.model.request.DeviceInfo
import br.com.digio.uber.model.resetpassword.ResetPasswordReq
import br.com.digio.uber.model.resetpassword.ResetPasswordRes
import kotlinx.coroutines.Deferred

class AccountNotLoggedInRepositoryImpl constructor(
    private val uberDatabase: UberDatabase,
    private val loginNotLoggedInDataset: LoginNotLoggedInDataset,
    private val deviceInfo: Deferred<DeviceInfo>
) : AccountNotLoggedInRepository {

    override suspend fun getCpfSaved(): String? =
        uberDatabase.accountNotLoggedDAO().getAccountNotLoggedTable()?.login

    override suspend fun saveCpf(login: String) {
        uberDatabase.accountNotLoggedDAO().insert(
            AccountNotLoggedTable(login = login)
        )
    }

    override suspend fun deleteCpf() {
        uberDatabase.accountNotLoggedDAO().getAccountNotLoggedTable()
            ?.let { accountNotLoggedTable ->
                uberDatabase.accountNotLoggedDAO()
                    .deleteAccountNotLoggedTable(accountNotLoggedTable)
            }
    }

    override suspend fun getUberFlagsPersistNotLoggedIn(): UberFlagsPersistNotLoggedIn? =
        uberDatabase.uberFlagsPersistNotLoggedInDao().getUberFlags()?.let { table ->
            UberFlagsPersistNotLoggedIn(
                saveCpfInLogin = table.saveCpfInLogin
            )
        }

    override fun getUberFlagsPersistNotLoggedInLiveData(): LiveData<UberFlagsPersistNotLoggedIn> =
        Transformations.map(
            uberDatabase.uberFlagsPersistNotLoggedInDao().getUberFlagsLiveData()
        ) { table ->
            UberFlagsPersistNotLoggedIn(
                saveCpfInLogin = table?.saveCpfInLogin
            )
        }

    override suspend fun updateUberFlagsPersistNotLoggedIn(
        uberFlagsPersistNotLoggedIn: UberFlagsPersistNotLoggedIn
    ): UberFlagsPersistNotLoggedIn? {
        val table = getUberFlagsPersistNotLoggedIn()
        uberDatabase.uberFlagsPersistNotLoggedInDao().insert(
            UberFlagsPersistNotLoggedInTable(
                saveCpfInLogin = uberFlagsPersistNotLoggedIn.saveCpfInLogin ?: table?.saveCpfInLogin == true
            )
        )
        return getUberFlagsPersistNotLoggedIn()
    }

    override suspend fun makeLogin(login: Login): LoginRes =
        loginNotLoggedInDataset.makeLogin(
            login.toLoginReq(
                deviceInfo.await()
            )
        )

    override suspend fun resetPassword(req: ResetPasswordReq): ResetPasswordRes =
        loginNotLoggedInDataset.resetPassword(req)

    override suspend fun changePassword(changePasswordReq: ChangePasswordReq) =
        loginNotLoggedInDataset.changePassword(changePasswordReq)
}