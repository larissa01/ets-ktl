package br.com.digio.uber.repository.cache.util

import br.com.digio.uber.repository.cache.CoCache
import br.com.digio.uber.repository.cache.HawkCache

suspend fun <T> String.getFromHawkCache(forceLoad: Boolean? = false, asyncValue: suspend () -> T): T =
    CoCache<T>(HawkCache()).get(this, forceLoad, asyncValue)

suspend fun <T> String.invalidateHawkCache() =
    CoCache<T>(HawkCache()).clearThisCache(this)

suspend fun clearAllHawkCache() =
    CoCache<Any>(HawkCache()).clearAllCache()