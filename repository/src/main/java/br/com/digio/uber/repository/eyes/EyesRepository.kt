package br.com.digio.uber.repository.eyes

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import br.com.digio.uber.db.UberDatabase
import br.com.digio.uber.db.tables.EyesVisibility

interface EyesRepository {
    suspend fun insertEyesVisibility(eyesVisibility: Boolean)
    fun getEyesVisibility(): LiveData<Boolean>
}
class EyesRepositoryImpl(
    private val db: UberDatabase
) : EyesRepository {

    override suspend fun insertEyesVisibility(eyesVisibility: Boolean) =
        db.eyesVisibilityDAO().insert(EyesVisibility(isVisible = eyesVisibility))

    override fun getEyesVisibility(): LiveData<Boolean> =
        Transformations.map(db.eyesVisibilityDAO().getEyesVisibility()) {
            it?.isVisible == true
        }
}