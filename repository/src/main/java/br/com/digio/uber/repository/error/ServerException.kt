package br.com.digio.uber.repository.error

import br.com.digio.uber.model.error.ErrorResponse

class ServerException(val code: Int, val error: ErrorResponse<*>?) : Exception()