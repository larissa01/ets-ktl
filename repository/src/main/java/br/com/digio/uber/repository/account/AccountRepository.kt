package br.com.digio.uber.repository.account

import br.com.digio.uber.dataset.account.AccountDataSet
import br.com.digio.uber.dataset.card.CardDataSet
import br.com.digio.uber.dataset.login.LoginLoggedDataset
import br.com.digio.uber.db.UberDatabase
import br.com.digio.uber.db.tables.Credentials
import br.com.digio.uber.model.account.AccountResponse
import br.com.digio.uber.model.account.AvatarRequest
import br.com.digio.uber.model.account.AvatarResponse
import br.com.digio.uber.model.card.TypeCard
import br.com.digio.uber.model.change.password.ChangePasswordAuthReq
import br.com.digio.uber.model.common.CredentialModel
import br.com.digio.uber.model.login.refresh.req.RefreshReq
import br.com.digio.uber.model.show.card.password.CardPasswordReq
import br.com.digio.uber.model.show.card.password.CardPasswordRes
import br.com.digio.uber.repository.cache.util.getFromHawkCache
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.TokenPreferencesUtils
import br.com.digio.uber.util.encrypt.encrypt
import com.salesforce.marketingcloud.MarketingCloudSdk
import retrofit2.http.Body
import retrofit2.http.Path

interface AccountRepository {
    suspend fun getAccount(): AccountResponse
    suspend fun getProfileAvatar(): AvatarResponse
    suspend fun getThirdPartyProfileAvatar(@Path("documentId") documentId: String): AvatarResponse
    suspend fun putProfileAvatar(@Body avatarRequest: AvatarRequest)
    suspend fun saveCredential(login: CredentialModel)
    suspend fun getCredential(): CredentialModel?
    suspend fun nukedCredentialAllTables()
    suspend fun changePasswordAuth(changePasswordAuthReq: ChangePasswordAuthReq)
    suspend fun getCardPassword(hash: String): CardPasswordRes
    suspend fun sendContactKey(cpf: String)
    suspend fun logout()
}

class AccountRepositoryImpl(
    private val accountDataSet: AccountDataSet,
    private val uberDatabase: UberDatabase,
    private val cardDataSet: CardDataSet,
    private val loginLoggedDataset: LoginLoggedDataset,
    private val tokenPreferencesUtils: TokenPreferencesUtils
) : AccountRepository {
    override suspend fun getAccount(): AccountResponse =
        accountDataSet.getAccount()

    override suspend fun getProfileAvatar(): AvatarResponse =
        accountDataSet.getProfileAvatar()

    override suspend fun getThirdPartyProfileAvatar(documentId: String): AvatarResponse =
        accountDataSet.getThirdPartyProfileAvatar(documentId)

    override suspend fun putProfileAvatar(avatarRequest: AvatarRequest) =
        accountDataSet.putProfileAvatar(avatarRequest)

    override suspend fun saveCredential(login: CredentialModel) {
        uberDatabase.credentialsDao().insert(
            Credentials(
                cpfCrypted = login.cpfCrypted,
                password = login.password
            )
        )
    }

    override suspend fun getCredential(): CredentialModel? =
        uberDatabase.credentialsDao().getCredentials()?.let { credentials ->
            CredentialModel(
                cpfCrypted = credentials.cpfCrypted,
                password = credentials.password
            )
        }

    override suspend fun nukedCredentialAllTables() {
        uberDatabase.deleteTablesLogout()
    }

    override suspend fun changePasswordAuth(changePasswordAuthReq: ChangePasswordAuthReq) {
        loginLoggedDataset.changePassword(changePasswordAuthReq)
        uberDatabase.credentialsDao().getCredentials()?.copy(
            password = changePasswordAuthReq.newPassword.encrypt()
        )?.let { credentials ->
            uberDatabase.credentialsDao().updateCredentials(credentials)
        }
    }

    override suspend fun getCardPassword(hash: String): CardPasswordRes =
        accountDataSet.getCardPassword(
            CardPasswordReq(
                cardId = Const.Cache.KEY_CARD_BY_TYPE_CACHE.getFromHawkCache {
                    cardDataSet.getCardByType(
                        TypeCard.DEBIT_ELO
                    )
                }?.cardId,
                hash = hash
            )
        )

    override suspend fun sendContactKey(cpf: String) {
        MarketingCloudSdk.requestSdk { sdk ->
            sdk.registrationManager.edit().apply {
                setContactKey(cpf)
                commit()
            }
        }
    }

    override suspend fun logout() {
        accountDataSet.logout(
            RefreshReq(
                refreshToken = tokenPreferencesUtils.getRefreshToken() ?: String()
            )
        )
    }
}