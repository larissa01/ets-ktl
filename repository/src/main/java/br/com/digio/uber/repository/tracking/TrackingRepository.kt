package br.com.digio.uber.repository.tracking

import br.com.digio.uber.model.tracking.TrackingLogResponse

interface TrackingRepository {
    suspend fun getTrackingLog(cardId: Long): TrackingLogResponse
}