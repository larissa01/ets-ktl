package br.com.digio.uber.repository.faq

import br.com.digio.uber.model.faq.FaqChatResponse
import br.com.digio.uber.model.faq.FaqChatUrlRequest
import br.com.digio.uber.model.faq.FaqPdfResponse
import br.com.digio.uber.model.faq.FaqSubjectResponse

interface FaqRepository {
    suspend fun getFaqCategories(): List<FaqSubjectResponse>
    suspend fun updateFaq()
    suspend fun getPdf(): FaqPdfResponse
    suspend fun getChatLink(request: FaqChatUrlRequest): FaqChatResponse
}