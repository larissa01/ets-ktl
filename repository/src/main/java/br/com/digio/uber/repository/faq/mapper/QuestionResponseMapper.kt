package br.com.digio.uber.repository.faq.mapper

import br.com.digio.uber.db.tables.FaqQuestion
import br.com.digio.uber.model.faq.FaqQuestionResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class QuestionResponseMapper : AbstractMapper<FaqQuestionResponse, FaqQuestion> {
    override fun map(param: FaqQuestionResponse): FaqQuestion {
        return FaqQuestion(
            param.id ?: "",
            param.title,
            param.tag,
            param.isFrequentQuestion,
            param.body,
            param.textButton,
            param.textHelpButton,
            param.typeButton,
            param.semantics,
            param.externalLink,
            param.helpful,
            param.client,
            ""
        )
    }
}