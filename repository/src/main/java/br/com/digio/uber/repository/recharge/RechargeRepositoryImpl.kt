package br.com.digio.uber.repository.recharge

import br.com.digio.uber.dataset.recharge.RechargeDataSet
import br.com.digio.uber.model.recharge.FavoriteContact

class RechargeRepositoryImpl(
    private val rechargeDataSet: RechargeDataSet
) : RechargeRepository {

    override suspend fun getOperatorPrices(provider: String) =
        rechargeDataSet.getOperatorPrices(provider)

    override suspend fun getFavoritesContacts() =
        rechargeDataSet.getFavoritesContacts()

    override suspend fun registerFavoriteContact(favoriteContact: FavoriteContact) =
        rechargeDataSet.registerFavoriteContact(favoriteContact)

    override suspend fun updateFavoriteContact(favoriteContact: FavoriteContact) =
        rechargeDataSet.updateFavoriteContact(favoriteContact)

    override suspend fun removeFavoriteContact(favoriteContact: FavoriteContact) =
        rechargeDataSet.removeFavoriteContact(favoriteContact)
}