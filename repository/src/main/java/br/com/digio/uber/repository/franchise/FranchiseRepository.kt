package br.com.digio.uber.repository.franchise

import br.com.digio.uber.dataset.franchise.FranchiseDataSet
import br.com.digio.uber.model.franchise.FranchiseResponse

interface FranchiseRepository {
    suspend fun getFranchise(): FranchiseResponse
}

class FranchiseRepositoryImpl(private val franchiseDataSet: FranchiseDataSet) : FranchiseRepository {
    override suspend fun getFranchise(): FranchiseResponse {
        return franchiseDataSet.getFranchise()
    }
}