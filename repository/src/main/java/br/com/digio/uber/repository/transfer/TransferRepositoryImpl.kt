@file:Suppress("TooManyFunctions")

package br.com.digio.uber.repository.transfer

import br.com.digio.uber.dataset.transfer.TransferDataSet
import br.com.digio.uber.db.UberDatabase
import br.com.digio.uber.model.transfer.AccountDomainResponse
import br.com.digio.uber.model.transfer.BankDomain
import br.com.digio.uber.model.transfer.BankDomainResponse
import br.com.digio.uber.model.transfer.BeneficiariesResponse
import br.com.digio.uber.model.transfer.BeneficiaryRequest
import br.com.digio.uber.model.transfer.BeneficiaryResponse
import br.com.digio.uber.model.transfer.ExternalTransferRequest
import br.com.digio.uber.model.transfer.InternalTransferRequest
import br.com.digio.uber.model.transfer.PixBankDomain
import br.com.digio.uber.model.transfer.PixBankDomainResponse
import br.com.digio.uber.model.transfer.ThirdPartyAccountListResponse
import br.com.digio.uber.model.transfer.ThirdPartyAccountResponse
import br.com.digio.uber.model.transfer.TransferResponse
import br.com.digio.uber.model.transfer.TransferStatus
import br.com.digio.uber.util.enumTest.CachePolicy

/**
 * @author Marlon D. Rocha
 * @since 30/10/20
 */
class TransferRepositoryImpl(
    private val transferDataSet: TransferDataSet,
    private val uberDatabase: UberDatabase
) : TransferRepository {

    override suspend fun getAccount(): AccountDomainResponse =
        transferDataSet.getAccount()

    override suspend fun getBanks(cachePolicy: CachePolicy): BankDomainResponse =
        when (cachePolicy) {
            CachePolicy.LOCAL_FIRST -> getLocalBanks(true)
            CachePolicy.LOCAL_ONLY -> getLocalBanks(false)
            CachePolicy.NETWORK_FIRST,
            CachePolicy.NETWORK_ONLY -> getRemoteBanks()
        }

    override suspend fun getPixBanks(cachePolicy: CachePolicy): PixBankDomainResponse {
        return when (cachePolicy) {
            CachePolicy.LOCAL_FIRST -> getLocalPixBanks(true)
            CachePolicy.LOCAL_ONLY -> getLocalPixBanks(false)
            CachePolicy.NETWORK_FIRST,
            CachePolicy.NETWORK_ONLY -> getRemotePixBanks()
        }
    }

    override suspend fun getPopularBanks(cachePolicy: CachePolicy): BankDomainResponse {
        return when (cachePolicy) {
            CachePolicy.LOCAL_FIRST -> getLocalPopularBanks(true)
            CachePolicy.LOCAL_ONLY -> getLocalPopularBanks(false)
            CachePolicy.NETWORK_FIRST,
            CachePolicy.NETWORK_ONLY -> getRemotePopularBanks()
        }
    }

    override suspend fun getPopularPixBanks(cachePolicy: CachePolicy): PixBankDomainResponse {
        return when (cachePolicy) {
            CachePolicy.LOCAL_FIRST -> getLocalPopularPixBanks(true)
            CachePolicy.LOCAL_ONLY -> getLocalPopularPixBanks(false)
            CachePolicy.NETWORK_FIRST,
            CachePolicy.NETWORK_ONLY -> getRemotePopularPixBanks()
        }
    }

    override suspend fun getTransferStatus(): TransferStatus =
        transferDataSet.getTransferStatus()

    override suspend fun confirmTransferExternalSameOwner(transferRequest: ExternalTransferRequest): TransferResponse =
        transferDataSet.confirmTransferExternalSameOwner(transferRequest)

    override suspend fun confirmTransferExternalThirdParty(transferRequest: ExternalTransferRequest): TransferResponse =
        transferDataSet.confirmTransferExternalThirdParty(transferRequest)

    override suspend fun confirmTransferInternal(transferRequest: InternalTransferRequest): TransferResponse =
        transferDataSet.confirmTransferInternal(transferRequest)

    private suspend fun getRemoteBanks(): BankDomainResponse {
        val banks = transferDataSet.getBanks()
        saveBanks(banks.bankDomains, false)
        return banks
    }

    private suspend fun getRemotePixBanks(): PixBankDomainResponse {
        val banks = transferDataSet.getPixBanks()
        savePixBanks(banks.bankDomains, false)
        return banks
    }

    private suspend fun getLocalBanks(withNetwork: Boolean): BankDomainResponse {
        val banks = arrayListOf<BankDomain>()
        val dtoBanks = uberDatabase.bankDAO().getBanks()

        return if (!dtoBanks.isNullOrEmpty()) {
            for (bankDAO in dtoBanks) {
                banks.add(bankDAO.toDomain())
            }
            BankDomainResponse(banks)
        } else {
            if (withNetwork) {
                getRemoteBanks()
            } else {
                BankDomainResponse(banks)
            }
        }
    }

    private suspend fun getLocalPixBanks(withNetwork: Boolean): PixBankDomainResponse {
        val banks = arrayListOf<PixBankDomain>()
        val dtoBanks = uberDatabase.pixBankDAO().getBanks()

        return if (!dtoBanks.isNullOrEmpty()) {
            for (bankDAO in dtoBanks) {
                banks.add(bankDAO.toDomain())
            }
            PixBankDomainResponse(banks)
        } else {
            if (withNetwork) {
                getRemotePixBanks()
            } else {
                PixBankDomainResponse(banks)
            }
        }
    }

    private suspend fun getRemotePopularBanks(): BankDomainResponse {
        val banks = transferDataSet.getPopularBanks()
        saveBanks(banks.bankDomains, true)
        return banks
    }

    private suspend fun getRemotePopularPixBanks(): PixBankDomainResponse {
        val banks = transferDataSet.getPopularPixBanks()
        savePixBanks(banks.bankDomains, true)
        return banks
    }

    private suspend fun getLocalPopularBanks(withNetwork: Boolean): BankDomainResponse {
        val banks = arrayListOf<BankDomain>()
        val dtoBanks = uberDatabase.popularBankDAO().getBanks()
        return if (!dtoBanks.isNullOrEmpty()) {
            for (dtoBank in dtoBanks) {
                banks.add(dtoBank.toDomain())
            }
            BankDomainResponse(banks)
        } else {
            if (withNetwork) getRemotePopularBanks() else BankDomainResponse(banks)
        }
    }

    private suspend fun getLocalPopularPixBanks(withNetwork: Boolean): PixBankDomainResponse {
        val banks = arrayListOf<PixBankDomain>()
        val dtoBanks = uberDatabase.popularPixBankDAO().getBanks()
        return if (!dtoBanks.isNullOrEmpty()) {
            for (dtoBank in dtoBanks) {
                banks.add(dtoBank.toDomain())
            }
            PixBankDomainResponse(banks)
        } else {
            if (withNetwork) getRemotePopularPixBanks() else PixBankDomainResponse(banks)
        }
    }

    private suspend fun saveBanks(banks: ArrayList<BankDomain>, isPopularBank: Boolean) {
        for (bank in banks) {
            if (isPopularBank) {
                uberDatabase.popularBankDAO().insert(bank.toPopularDAO())
            } else {
                uberDatabase.bankDAO().insert(bank.toDAO())
            }
        }
    }

    private suspend fun savePixBanks(banks: ArrayList<PixBankDomain>, isPopularBank: Boolean) {
        for (bank in banks) {
            if (isPopularBank) {
                uberDatabase.popularPixBankDAO().insert(bank.toPopularPixBankDAO())
            } else {
                uberDatabase.pixBankDAO().insert(bank.toPixBankDAO())
            }
        }
    }

    override suspend fun getTedBeneficiaries(): BeneficiariesResponse =
        transferDataSet.getTedBeneficiaries()

    override suspend fun deleteTedBeneficiary(beneficiaryId: String): BeneficiaryResponse =
        transferDataSet.deleteTedBeneficiary(beneficiaryId)

    override suspend fun putTedBeneficiary(
        beneficiaryId: String,
        beneficiaryRequest: BeneficiaryRequest
    ): BeneficiaryResponse = transferDataSet.putTedBeneficiary(beneficiaryId, beneficiaryRequest)

    override suspend fun getP2PBeneficiaries(): BeneficiariesResponse =
        transferDataSet.getP2PBeneficiaries()

    override suspend fun deleteP2PBeneficiary(beneficiaryId: String): BeneficiaryResponse =
        transferDataSet.deleteP2PBeneficiary(beneficiaryId)

    override suspend fun putP2PBeneficiary(
        beneficiaryId: String,
        beneficiaryRequest: BeneficiaryRequest
    ): BeneficiaryResponse = transferDataSet.putP2PBeneficiary(beneficiaryId, beneficiaryRequest)

    override suspend fun getAccountByCPF(cpf: String): ThirdPartyAccountListResponse =
        transferDataSet.getAccountByCPF(cpf)

    override suspend fun searchAccount(agency: String, account: String): ThirdPartyAccountResponse =
        transferDataSet.searchAccount(agency, account)
}