package br.com.digio.uber.repository.faq.mapper

import br.com.digio.uber.db.tables.FaqSubjectWithQuestions
import br.com.digio.uber.model.faq.FaqSubjectResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class SubjectListMapper : AbstractMapper<List<FaqSubjectWithQuestions>, List<FaqSubjectResponse>> {

    override fun map(param: List<FaqSubjectWithQuestions>): List<FaqSubjectResponse> {
        return param.map { SubjectMapper().map(it) }
    }
}