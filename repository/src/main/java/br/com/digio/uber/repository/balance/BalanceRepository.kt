package br.com.digio.uber.repository.balance

import br.com.digio.uber.dataset.balance.BalanceDataSet
import br.com.digio.uber.model.BalanceResponse

interface BalanceRepository {
    suspend fun getBalance(): BalanceResponse
}
class BalanceRepositoryImpl(
    private val balanceDataSet: BalanceDataSet
) : BalanceRepository {
    override suspend fun getBalance(): BalanceResponse =
        balanceDataSet.getBalance()
}