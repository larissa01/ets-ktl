package br.com.digio.uber.repository.recharge

import br.com.digio.uber.model.recharge.FavoriteContact
import br.com.digio.uber.model.recharge.FavoriteContactList
import br.com.digio.uber.model.recharge.RechargeValues

interface RechargeRepository {
    suspend fun getOperatorPrices(provider: String): RechargeValues
    suspend fun getFavoritesContacts(): FavoriteContactList
    suspend fun registerFavoriteContact(favoriteContact: FavoriteContact): Unit
    suspend fun updateFavoriteContact(favoriteContact: FavoriteContact): Unit
    suspend fun removeFavoriteContact(favoriteContact: FavoriteContact): Unit
}