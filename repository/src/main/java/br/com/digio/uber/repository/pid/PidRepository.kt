package br.com.digio.uber.repository.pid

import br.com.digio.uber.model.pid.PidRes
import br.com.digio.uber.model.pid.PidStepReq
import br.com.digio.uber.model.pid.PidType

interface PidRepository {
    suspend fun firstPid(pidType: PidType): PidRes
    suspend fun pidValidate(pidRes: PidStepReq): PidRes
}