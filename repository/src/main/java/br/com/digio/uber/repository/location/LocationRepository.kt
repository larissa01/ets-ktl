package br.com.digio.uber.repository.location

import android.location.Location

interface LocationRepository {
    suspend fun isGPSEnabled(): Boolean
    suspend fun getLastKnowLocation(): Location
}