package br.com.digio.uber.repository.store

import br.com.digio.uber.dataset.store.StoreDataSet
import br.com.digio.uber.model.store.StoreOrder

class StoreRepositoryImpl(
    private val storeDataSet: StoreDataSet
) : StoreRepository {

    override suspend fun getStoreCatalog() =
        storeDataSet.getStoreCatalog()

    override suspend fun registerStoreOrder(order: StoreOrder) =
        storeDataSet.registerStoreOrder(order)

    override suspend fun getStoreOrders(page: Int) =
        storeDataSet.getStoreOrders(page)
}