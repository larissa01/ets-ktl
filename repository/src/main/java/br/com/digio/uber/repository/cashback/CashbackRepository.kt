package br.com.digio.uber.repository.cashback

import br.com.digio.uber.model.ResponseList
import br.com.digio.uber.model.cashback.CashbackAmount
import br.com.digio.uber.model.cashback.CashbackComissionCategory
import br.com.digio.uber.model.cashback.CashbackCustomerResponse
import br.com.digio.uber.model.cashback.CashbackDetailsResponse
import br.com.digio.uber.model.cashback.CashbackOfferResponse
import br.com.digio.uber.model.cashback.CashbackPurchase
import br.com.digio.uber.model.cashback.CashbackStore
import br.com.digio.uber.model.cashback.CashbackTerms
import br.com.digio.uber.model.cashback.SendCashbackRequest
import retrofit2.Response

interface CashbackRepository {
    suspend fun getPurchasesAmount(status: String): CashbackAmount
    suspend fun getCashbackStores(quantityItens: Int): ResponseList<CashbackStore>
    suspend fun getCashbackTerms(): CashbackTerms
    suspend fun acceptCashbackTerms(): Response<Void>
    suspend fun getCashbackTermsAccept(): Response<Void>
    suspend fun getCashbackCustomer(): CashbackCustomerResponse
    suspend fun getCashbackDetails(type: String): CashbackDetailsResponse
    suspend fun getStoreOffers(storeId: Long): ResponseList<CashbackOfferResponse>
    suspend fun getComissionCategory(id: Long): ResponseList<CashbackComissionCategory>
    suspend fun getCashbackPurchases(status: String, page: Int): ResponseList<CashbackPurchase>
    suspend fun sendCashbackAccount(receiptForm: String, purchases: SendCashbackRequest): Response<Void>
}