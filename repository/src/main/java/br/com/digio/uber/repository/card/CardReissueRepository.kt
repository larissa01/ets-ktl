package br.com.digio.uber.repository.card

import br.com.digio.uber.dataset.card.CardReissueDataSet
import br.com.digio.uber.model.card.TypeCard
import br.com.digio.uber.model.cardreissue.LostStolenRequest
import br.com.digio.uber.model.cardreissue.LostStolenRequestStatusCancel
import br.com.digio.uber.model.cardreissue.LostStolenResponse

interface CardReissueRepository {
    suspend fun postLoss(cardId: Long): LostStolenResponse
    suspend fun postTheft(cardId: Long): LostStolenResponse
}

class CardReissueRepositoryImpl(
    private val cardReissueDataSet: CardReissueDataSet
) : CardReissueRepository {
    override suspend fun postLoss(cardId: Long): LostStolenResponse =
        cardReissueDataSet.postLossAndTheft(
            TypeCard.VIRTUAL_DEBIT_ELO.productNameCard.name,
            LostStolenRequest(
                cardId = cardId.toInt(),
                chargeType = TypeCard.DEBIT_ELO.productNameCard.name,
                isCancelCard = true,
                isChargeCard = false,
                cancelStatus = LostStolenRequestStatusCancel.CANCELADO_PERDA.id
            )
        )

    override suspend fun postTheft(cardId: Long): LostStolenResponse =
        cardReissueDataSet.postLossAndTheft(
            TypeCard.VIRTUAL_DEBIT_ELO.productNameCard.name,
            LostStolenRequest(
                cardId = cardId.toInt(),
                chargeType = TypeCard.DEBIT_ELO.productNameCard.name,
                isCancelCard = true,
                isChargeCard = false,
                cancelStatus = LostStolenRequestStatusCancel.CANCELADO_ROUBO.id
            )
        )
}