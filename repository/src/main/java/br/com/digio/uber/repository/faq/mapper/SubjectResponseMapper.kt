package br.com.digio.uber.repository.faq.mapper

import br.com.digio.uber.db.tables.FaqSubject
import br.com.digio.uber.db.tables.FaqSubjectWithQuestions
import br.com.digio.uber.model.faq.FaqSubjectResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class SubjectResponseMapper : AbstractMapper<FaqSubjectResponse, FaqSubjectWithQuestions> {

    override fun map(param: FaqSubjectResponse): FaqSubjectWithQuestions {
        return FaqSubjectWithQuestions(
            FaqSubject(
                param.id ?: "",
                param.subject,
                param.icon,
                param.client
            ),
            param.questions.map { QuestionResponseMapper().map(it) }
        )
    }
}