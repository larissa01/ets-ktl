package br.com.digio.uber.repository.tracking

import br.com.digio.uber.dataset.tracking.TrackingDataSet
import br.com.digio.uber.model.tracking.TrackingLogResponse

class TrackingRepositoryImpl constructor(
    private val trackingDataSet: TrackingDataSet
) : TrackingRepository {
    override suspend fun getTrackingLog(cardId: Long): TrackingLogResponse =
        trackingDataSet.getTrackingLog(cardId)
}