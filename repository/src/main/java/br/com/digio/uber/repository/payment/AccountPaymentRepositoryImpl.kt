package br.com.digio.uber.repository.payment

import br.com.digio.uber.dataset.payment.AccountPaymentDataset
import br.com.digio.uber.model.payment.BillBarcode
import br.com.digio.uber.model.payment.ConfirmPayResponse
import br.com.digio.uber.model.payment.PaymentBarcodeRequest

class AccountPaymentRepositoryImpl(
    private val accountPaymentDataset: AccountPaymentDataset
) : AccountPaymentRepository {
    override suspend fun checkBillBarcode(barcode: String): BillBarcode = accountPaymentDataset.checkBillBarcode(
        barcode
    )

    override suspend fun paymentAuthorize(pidHash: String, param: PaymentBarcodeRequest): ConfirmPayResponse =
        accountPaymentDataset.paymentAuthorize(pidHash, param)
}