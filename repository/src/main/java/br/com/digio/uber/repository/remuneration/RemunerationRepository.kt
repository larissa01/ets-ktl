package br.com.digio.uber.repository.remuneration

import br.com.digio.uber.dataset.remuneration.RemunerationDataSet
import br.com.digio.uber.model.remuneration.IncomeIndexResponse

interface RemunerationRepository {
    suspend fun getIncome(): IncomeIndexResponse
}
class RemunerationRepositoryImpl(
    private val remunerationDataSet: RemunerationDataSet
) : RemunerationRepository {
    override suspend fun getIncome(): IncomeIndexResponse =
        remunerationDataSet.getIncome()
}