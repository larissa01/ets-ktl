package br.com.digio.uber.repository.account

import br.com.digio.uber.dataset.account.AccountDataSet
import br.com.digio.uber.model.change.registration.AddressRegistrationRequest
import br.com.digio.uber.model.change.registration.ChangeRegistrationData
import br.com.digio.uber.model.change.registration.ChangeRegistrationRequest
import br.com.digio.uber.model.change.registration.ZipCodeRes

class AccountChangeRegistrationRepositoryImpl constructor(
    private val accountDataSet: AccountDataSet
) : AccountChangeRegistrationRepository {
    override suspend fun getChangeRegistrationData(): ChangeRegistrationData =
        accountDataSet.getChangeRegistrationData()

    override suspend fun getZipCode(cep: String): ZipCodeRes =
        accountDataSet.getZipCode(cep)

    override suspend fun changeRegistration(changeRegistrationItemList: ChangeRegistrationRequest) {
        accountDataSet.changeRegistration(changeRegistrationItemList)
    }

    override suspend fun updateAddress(param: AddressRegistrationRequest) {
        accountDataSet.updateAddress(param)
    }
}