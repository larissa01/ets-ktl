package br.com.digio.uber.repository.error

import br.com.digio.uber.model.error.ErrorResponse
import br.com.digio.uber.util.castOrNull
import br.com.digio.uber.util.cloneErrorResponseBody
import br.com.digio.uber.util.fromJsonOrNull
import retrofit2.HttpException

inline fun <reified T : Any> Throwable.tryConvertError(): Throwable =
    asServerExceptionOrNull<T>() ?: this

inline fun <reified T : Any> Throwable.asServerExceptionOrNull(): ServerException? {
    val httpException = castOrNull<HttpException>()

    return httpException?.let { http ->
        val responseBody = http.response()?.cloneErrorResponseBody()
        val responseBodyString = responseBody?.string()

        ServerException(http.code(), responseBodyString?.fromJsonOrNull<ErrorResponse<T>>())
    }
}