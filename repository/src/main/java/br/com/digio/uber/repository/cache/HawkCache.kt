package br.com.digio.uber.repository.cache

import br.com.digio.uber.util.localeDefault
import com.orhanobut.hawk.Hawk
import timber.log.Timber
import java.util.Calendar

class HawkCache<T> constructor(
    private val lifeTimeMs: Long = DEFAULT_LIFE_TIME
) : Cache<T> {

    override suspend fun save(key: String, value: T) {
        Timber.d("Save cache of key = $key")
        Hawk.put("${key}_$CREATE_AT", Calendar.getInstance(localeDefault).time.time)
        Hawk.put(key, value)
    }

    override suspend fun get(key: String): T? {
        Timber.d("Get cache = $key")
        val value: T? = Hawk.get<T>(key)
        val createAt = Hawk.get<Long>("${key}_$CREATE_AT", 0L)
        val deference = Calendar.getInstance(localeDefault).time.time - (createAt ?: 0L)
        return if (deference < lifeTimeMs) {
            value
        } else {
            null
        }
    }

    override suspend fun clear(key: String) {
        Timber.d("clear cache = $key")
        Hawk.delete(key)
    }

    override suspend fun clearAll() {
        Timber.d("Clear all")
        Hawk.deleteAll()
    }

    companion object {
        const val CREATE_AT = "create_at"

        const val DEFAULT_LIFE_TIME = 60L * 60L * 1000L
    }
}