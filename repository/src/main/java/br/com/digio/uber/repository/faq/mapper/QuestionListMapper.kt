package br.com.digio.uber.repository.faq.mapper

import br.com.digio.uber.db.tables.FaqQuestion
import br.com.digio.uber.model.faq.FaqQuestionResponse
import br.com.digio.uber.util.mapper.AbstractMapper

class QuestionListMapper : AbstractMapper<List<FaqQuestion>, List<FaqQuestionResponse>> {

    override fun map(param: List<FaqQuestion>): List<FaqQuestionResponse> {
        val mapper = QuestionMapper()
        return param.map { mapper.map(it) }
    }
}