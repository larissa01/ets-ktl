package br.com.digio.uber.repository.cache

import br.com.digio.uber.repository.BuildConfig
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class CoCache<T> constructor(
    private val cache: Cache<T>
) {

    suspend fun get(key: String?, forceLoad: Boolean? = false, asyncValue: suspend () -> T): T =
        withContext(IO) {
            val resultCache = getCacheResult(key)
            if (forceLoad == false && resultCache != null) {
                resultCache
            } else {
                val value = asyncValue()
                saveCache(value, key)
                value
            }
        }

    private suspend fun saveCache(result: T, key: String?) {
        if (BuildConfig.ENABLE_CACHE_REQUEST) {
            key?.let { cache.save(key, result) }
        }
    }

    private suspend fun getCacheResult(key: String?): T? =
        if (BuildConfig.ENABLE_CACHE_REQUEST) {
            key?.let { cache.get(it) }
        } else {
            null
        }

    suspend fun clearThisCache(key: String?) {
        key?.let { cache.clear(it) }
    }

    suspend fun clearAllCache() {
        cache.clearAll()
    }
}