package br.com.digio.uber.repository.digioTestRepository

import br.com.digio.uber.dataset.digioTest.DigioTestDataSet
import br.com.digio.uber.model.digioTests.ProductsList
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import retrofit2.HttpException

class DigioTestRepositoryTest {

    private val digioTestDataset: DigioTestDataSet = mockk()

    private val digioTestRepository: DigioTestRepository = DigioTestRepositoryImpl(digioTestDataset)

    @ExperimentalCoroutinesApi
    @Test
    fun `on request products and return a object products`() = runBlockingTest {
        val productsList: ProductsList = mockk()

        coEvery { digioTestDataset.getProducts() } returns productsList

        val result = digioTestRepository.requestDigioTestPayload()

        assertEquals(result, productsList)

        coVerify { digioTestDataset.getProducts() }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `on request products and return http error`() = runBlockingTest {
        val httpException: HttpException = mockk()

        coEvery { digioTestDataset.getProducts() } throws httpException

        try {
            val result = digioTestRepository.requestDigioTestPayload()
            assertNull(result)
        } catch (http: HttpException) {
            assertEquals(http, httpException)
        } catch (e: Throwable) {
            assertNull(e)
        }

        coVerify { digioTestDataset.getProducts() }
    }
}