package br.com.digio.uber.changeregistration.fragment

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.changeregistration.BR
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.databinding.ChangeRegistrationAbstractEdittextFragmentBinding
import br.com.digio.uber.changeregistration.uimodel.ChangeRegistrationItemListUiModel
import br.com.digio.uber.changeregistration.viewmodel.CRAbstractEditTextViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.hideKeyboard
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.extensions.stateSetValue
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.change.registration.ChangeRegistrationItemType
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.moneyToFloat
import br.com.digio.uber.util.toMoney
import org.koin.android.ext.android.inject

class CRAbstractEditTextFragment :
    BaseViewModelFragment<ChangeRegistrationAbstractEdittextFragmentBinding, CRAbstractEditTextViewModel>() {

    override fun tag(): String =
        CRAbstractEditTextFragment::class.java.name

    override val bindingVariable: Int =
        BR.abstractEditTextViewModel

    override val getLayoutId: Int =
        R.layout.change_registration_abstract_edittext_fragment

    override val viewModel: CRAbstractEditTextViewModel? by inject()

    private val changeRegistrationItemListUiModel: ChangeRegistrationItemListUiModel by lazy {
        CRAbstractEditTextFragmentArgs.fromBundle(requireArguments()).changeRegistrationItemListUiModelKey
    }

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE

    private val invoiceTextWatch: TextWatcher = object : TextWatcher {
        private var current = ""

        override fun afterTextChanged(s: Editable?) {
            /*nothgin*/
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            /*nothgin*/
        }

        override fun onTextChanged(value: CharSequence?, start: Int, before: Int, count: Int) {
            if (value != current) {
                binding?.edtAbstractValue?.removeTextChangedListener(this)

                val valueWithoutCharacter = if (count == 0) {
                    value.toString().replace(",.+".toRegex(), "").dropLast(1)
                } else {
                    value.toString().replace(",00".toRegex(), "")
                }
                val newValue =
                    if (
                        valueWithoutCharacter.isEmpty() ||
                        valueWithoutCharacter.trim() == getString(R.string.money_symbol)
                    ) {
                        String()
                    } else {
                        valueWithoutCharacter.moneyToFloat().toMoney(true)
                    }

                current = newValue
                binding?.edtAbstractValue?.setText(newValue)
                binding?.edtAbstractValue?.setSelection(newValue.length)
                viewModel?.validateValue(newValue)
                binding?.edtAbstractValue?.addTextChangedListener(this)
            }
        }
    }

    override fun initialize() {
        super.initialize()
        viewModel?.initializer(changeRegistrationItemListUiModel)
        setupEditTextIfIsInvoiceSelection()
        setupObservers()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Const.RequestOnResult.PID.PID_REQUEST_CODE &&
            resultCode == Const.RequestOnResult.PID.RESULT_PID_SUCCESS
        ) {
            data?.getStringExtra(Const.RequestOnResult.PID.KEY_PID_HASH)?.let {
                viewModel?.manageItemClicked(it)
            }
        }
    }

    private fun setupEditTextIfIsInvoiceSelection() {
        if (changeRegistrationItemListUiModel.type == ChangeRegistrationItemType.INCOME) {
            binding?.edtAbstractValue?.addTextChangedListener(invoiceTextWatch)
        }
    }

    private fun setupObservers() {
        if (changeRegistrationItemListUiModel.type == ChangeRegistrationItemType.EMAIL ||
            changeRegistrationItemListUiModel.type == ChangeRegistrationItemType.PHONE
        ) {
            viewModel?.value?.observe(
                this,
                {
                    viewModel?.validateValue(it)
                }
            )
        }

        viewModel?.invokePID?.observe(
            this,
            {
                if (it) {
                    navigation.navigationToPidActivity(this, PidType.USER_DATA_MANAGER)
                }
            }
        )

        viewModel?.successChange?.observe(
            this,
            {
                if (it == true) {
                    finishPop()
                    viewModel?.successChange?.value = false
                }
            }
        )
    }

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePasswordLayout?.toolbarChangePassword

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true

    override fun onNavigationClick(view: View) {
        super.onNavigationClick(view)
        hideKeyboard()
        pop()
    }

    private fun finishPop() {
        hideKeyboard()
        pop()
        stateSetValue(Const.RequestOnResult.ChangeRegistration.KEY_CHANGE_REGISTRATION_BACK, true)
    }
}