package br.com.digio.uber.changeregistration.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.mapper.ZipCodeResMapper
import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.changeregistration.uimodel.ZipCodeResUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.common.typealiases.OnTextChanged
import br.com.digio.uber.domain.usecase.change.registration.GetZipCodeUseCase
import br.com.digio.uber.model.change.registration.ChangeRegistrationItemType
import br.com.digio.uber.model.change.registration.ZipCodeRes
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.unmask

class CRAddressCepViewModel constructor(
    private val getZipCodeUseCase: GetZipCodeUseCase,
    private val zipCodeResMapper: ZipCodeResMapper
) : BaseViewModel() {

    val errorHint: MutableLiveData<Int> = MutableLiveData()
    val value: MutableLiveData<String> = MutableLiveData()
    val enableButton: MutableLiveData<Boolean> = MutableLiveData()
    val mask: String = Const.MaskPattern.CEP
    val finishGoToQuestion: MutableLiveData<ZipCodeResUiModel> = MutableLiveData()
    val title: MutableLiveData<Int> = MutableLiveData()

    val onValueChange: OnTextChanged = { value, _, _, _ ->
        enableButton.value = validateValue(value.toString())
    }

    fun initializer(
        addressRegistrationUiModel: AddressRegistrationUiModel,
        onClickItem: OnClickItem
    ) {
        setupTitle(addressRegistrationUiModel)
        super.initializer {
            when (it.id) {
                R.id.btnNext -> findZipCode()
                else -> onClickItem(it)
            }
        }
    }

    private fun setupTitle(addressRegistrationUiModel: AddressRegistrationUiModel) {
        title.value =
            if (addressRegistrationUiModel.type == ChangeRegistrationItemType.RESIDENTIAL) {
                R.string.whats_is_your_cep
            } else {
                R.string.whats_is_your_cep_for_delivery
            }
    }

    private fun findZipCode() {
        value.value?.unmask()?.let { zipCode ->
            getZipCodeUseCase(zipCode).singleExec(
                onSuccessBaseViewModel = { zipCodeRes ->
                    if (!validateZipCodeRes(zipCodeRes)) {
                        errorHint.value = R.string.zip_code_invalid
                    } else {
                        finishGoToQuestion.value = zipCodeResMapper.map(zipCodeRes)
                    }
                }
            )
        }
    }

    private fun validateZipCodeRes(zipCode: ZipCodeRes): Boolean =
        zipCode.city != null &&
            zipCode.state != null &&
            zipCode.zipCode != null

    private fun validateValue(value: String): Boolean {
        errorHint.value = null
        return value.length >= CEP_MAX_CHARACTERS
    }

    companion object {
        const val CEP_MAX_CHARACTERS = 9
    }
}