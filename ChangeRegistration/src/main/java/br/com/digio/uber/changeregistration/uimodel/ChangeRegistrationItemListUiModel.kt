package br.com.digio.uber.changeregistration.uimodel

import android.os.Parcelable
import br.com.digio.uber.model.change.registration.ChangeRegistrationItemType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChangeRegistrationItemListUiModel(
    val patrimony: Float?,
    val patrimonyFormat: String?,
    val text: String?,
    val title: String,
    val phoneNumberFormated: String?,
    val type: ChangeRegistrationItemType,
    val order: Int?,
    val isEditable: Boolean = false,
    val pidHash: String
) : Parcelable