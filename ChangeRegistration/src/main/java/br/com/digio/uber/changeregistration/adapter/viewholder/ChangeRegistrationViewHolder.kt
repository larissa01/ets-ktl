package br.com.digio.uber.changeregistration.adapter.viewholder

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModelItem
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.model.change.registration.ChangeRegistrationItemType
import br.com.digio.uber.util.enums.EducationFilterType

class ChangeRegistrationViewHolder(
    private val viewDataBinding: ViewDataBinding,
    listener: ViewTypesListener<ChangeRegistrationModelItem>
) : AbstractChangeRegistrationViewHolder<ChangeRegistrationModelItem>(viewDataBinding, listener) {

    @Suppress("ComplexMethod")
    override fun getValue(itemBody: ChangeRegistrationModelItem): String? =
        when (itemBody.changeRegistrationItemListUiModel.type) {
            ChangeRegistrationItemType.NAME -> getTextFromChangeRegistrationItemList(itemBody)
            ChangeRegistrationItemType.BIRTHDATE -> getTextFromChangeRegistrationItemList(itemBody)
            ChangeRegistrationItemType.PROFESSION -> getTextFromChangeRegistrationItemList(itemBody)
            ChangeRegistrationItemType.INCOME -> itemBody.changeRegistrationItemListUiModel.patrimonyFormat
            ChangeRegistrationItemType.EMAIL -> getTextFromChangeRegistrationItemList(itemBody)
            ChangeRegistrationItemType.SCHOLARITY ->
                EducationFilterType.fromString(getTextFromChangeRegistrationItemList(itemBody))?.value
                    ?: viewDataBinding.root.context.getString(R.string.unassigned_profession)
            ChangeRegistrationItemType.PHONE ->
                itemBody.changeRegistrationItemListUiModel.phoneNumberFormated
                    ?: viewDataBinding.root.context.getString(R.string.unassigned_profession)
            ChangeRegistrationItemType.NATIONALITY -> getTextFromChangeRegistrationItemList(itemBody)
            ChangeRegistrationItemType.MOTHER_NAME -> getTextFromChangeRegistrationItemList(itemBody)
            ChangeRegistrationItemType.DOCUMENT -> getTextFromChangeRegistrationItemList(itemBody)
            ChangeRegistrationItemType.SECONDARY_DOC -> getTextFromChangeRegistrationItemList(
                itemBody
            )
            ChangeRegistrationItemType.GENDER -> getTextFromChangeRegistrationItemList(itemBody)
            else -> null
        }

    private fun getTextFromChangeRegistrationItemList(itemBody: ChangeRegistrationModelItem) =
        itemBody.changeRegistrationItemListUiModel.text
}