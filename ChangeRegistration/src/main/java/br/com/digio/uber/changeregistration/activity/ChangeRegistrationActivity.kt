package br.com.digio.uber.changeregistration.activity

import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.di.mapper.changeRegistrationMapperModule
import br.com.digio.uber.changeregistration.di.viewmodel.changeRegistrationViewModelModule
import br.com.digio.uber.common.base.activity.BaseActivity
import org.koin.core.module.Module

class ChangeRegistrationActivity : BaseActivity() {
    override fun getLayoutId(): Int? =
        R.layout.change_registration_activity

    override fun getModule(): List<Module>? =
        listOf(changeRegistrationViewModelModule, changeRegistrationMapperModule)
}