package br.com.digio.uber.changeregistration.di.mapper

import br.com.digio.uber.changeregistration.mapper.AddressRegistrationMapper
import br.com.digio.uber.changeregistration.mapper.AddressRegistrationMapperImpl
import br.com.digio.uber.changeregistration.mapper.AddressRegistrationReverseMapper
import br.com.digio.uber.changeregistration.mapper.AddressRegistrationReverseMapperImpl
import br.com.digio.uber.changeregistration.mapper.ChangeRegistrationDataListMapper
import br.com.digio.uber.changeregistration.mapper.ChangeRegistrationDataListMapperImpl
import br.com.digio.uber.changeregistration.mapper.ChangeRegistrationDataMapper
import br.com.digio.uber.changeregistration.mapper.ChangeRegistrationDataMapperImpl
import br.com.digio.uber.changeregistration.mapper.ChangeRegistrationItemListMapper
import br.com.digio.uber.changeregistration.mapper.ChangeRegistrationItemListMapperImpl
import br.com.digio.uber.changeregistration.mapper.ChangeRegistrationItemListReverseMapper
import br.com.digio.uber.changeregistration.mapper.ChangeRegistrationItemListReverseMapperImpl
import br.com.digio.uber.changeregistration.mapper.ZipCodeResMapper
import br.com.digio.uber.changeregistration.mapper.ZipCodeResMapperImpl
import org.koin.dsl.module

val changeRegistrationMapperModule = module {
    single<AddressRegistrationReverseMapper> { AddressRegistrationReverseMapperImpl() }
    single<AddressRegistrationMapper> { AddressRegistrationMapperImpl() }
    single<ChangeRegistrationItemListReverseMapper> { ChangeRegistrationItemListReverseMapperImpl(get()) }
    single<ChangeRegistrationItemListMapper> { ChangeRegistrationItemListMapperImpl(get()) }
    single<ChangeRegistrationDataMapper> { ChangeRegistrationDataMapperImpl(get(), get()) }
    single<ChangeRegistrationDataListMapper> { ChangeRegistrationDataListMapperImpl(get()) }
    single<ZipCodeResMapper> { ZipCodeResMapperImpl() }
}