package br.com.digio.uber.changeregistration.fragment

import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.changeregistration.BR
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.databinding.ChangeRegistrationAddressShowInfoFragmentBinding
import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.changeregistration.uimodel.ZipCodeResUiModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressShowInfoViewModel
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.pop
import org.koin.android.ext.android.inject

class CRAddressShowInfoFragment :
    CRAddressAbstractFragment<ChangeRegistrationAddressShowInfoFragmentBinding, CRAddressShowInfoViewModel>() {

    override val bindingVariable: Int? = BR.changeRegistrationAddressShowInfoViewModel

    override val getLayoutId: Int? = R.layout.change_registration_address_show_info_fragment

    override val viewModel: CRAddressShowInfoViewModel? by inject()

    private val addressRegistrationUiModel: AddressRegistrationUiModel by lazy {
        CRAddressShowInfoFragmentArgs.fromBundle(requireArguments()).addressRegistrationUiModelKey
    }

    private val zipCodeResUiModel: ZipCodeResUiModel by lazy {
        CRAddressShowInfoFragmentArgs.fromBundle(requireArguments()).zipCodeResUiModel
    }

    override fun tag(): String =
        CRAddressShowInfoFragment::class.java.name

    override fun initialize() {
        super.initialize()
        viewModel?.initializer(addressRegistrationUiModel, zipCodeResUiModel) {
            when (it.id) {
                R.id.btnBack -> pop()
                R.id.btnNext -> navigateToNumberOrStreet()
            }
        }
    }

    private fun navigateToNumberOrStreet() {
        if (zipCodeResUiModel.street == null) {
            navigateTo(
                CRAddressShowInfoFragmentDirections
                    .actionChangeRegistrationAddressShowInfoFragmentToCRAddressStreetFragment(
                        concatInformation()
                    )
            )
        } else {
            navigateTo(
                CRAddressShowInfoFragmentDirections
                    .actionChangeRegistrationAddressShowInfoFragmentToChangeRegistrationAddressNumberFragment(
                        concatInformation()
                    )
            )
        }
    }

    private fun concatInformation(): AddressRegistrationUiModel =
        addressRegistrationUiModel.copy(
            zipCode = zipCodeResUiModel.zipCode,
            state = zipCodeResUiModel.state,
            street = zipCodeResUiModel.street,
            neighborhood = zipCodeResUiModel.neighborhood,
            city = zipCodeResUiModel.city
        )

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePasswordLayout?.toolbarChangePassword
}