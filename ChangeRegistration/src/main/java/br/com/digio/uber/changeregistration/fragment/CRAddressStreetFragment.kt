package br.com.digio.uber.changeregistration.fragment

import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.changeregistration.BR
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.databinding.ChangeRegistrationAddressStreetFragmentBinding
import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressStreetViewModel
import br.com.digio.uber.common.extensions.navigateTo
import org.koin.android.ext.android.inject

class CRAddressStreetFragment :
    CRAddressAbstractFragment<ChangeRegistrationAddressStreetFragmentBinding, CRAddressStreetViewModel>() {
    override val bindingVariable: Int? = BR.crAddressStreetViewModel

    override val getLayoutId: Int? = R.layout.change_registration_address_street_fragment

    override val viewModel: CRAddressStreetViewModel? by inject()

    private val addressRegistrationUiModel: AddressRegistrationUiModel by lazy {
        CRAddressStreetFragmentArgs.fromBundle(requireArguments()).addressRegistrationUiModelKey
    }

    override fun initialize() {
        super.initialize()

        viewModel?.initializer {
            when (it.id) {
                R.id.btnNext -> {
                    navigateToNumberOrNeighborhood()
                }
            }
        }

        setupObserver()
    }

    private fun navigateToNumberOrNeighborhood() {
        if (addressRegistrationUiModel.neighborhood != null) {
            navigateTo(
                CRAddressStreetFragmentDirections
                    .actionCRAddressStreetFragmentToChangeRegistrationAddressNumberFragment(
                        addStreetValue()
                    )
            )
        } else {
            navigateTo(
                CRAddressStreetFragmentDirections
                    .actionCRAddressStreetFragmentToCRAddressNeighborhoodFragment(
                        addStreetValue()
                    )
            )
        }
    }

    private fun addStreetValue(): AddressRegistrationUiModel {
        return addressRegistrationUiModel.copy(
            street = viewModel?.value?.value
        )
    }

    private fun setupObserver() {
        viewModel?.value?.observe(
            this,
            Observer {
                viewModel?.onValueChange(it)
            }
        )
    }

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePasswordLayout?.toolbarChangePassword

    override fun tag(): String =
        CRAddressStreetFragment::class.java.name
}