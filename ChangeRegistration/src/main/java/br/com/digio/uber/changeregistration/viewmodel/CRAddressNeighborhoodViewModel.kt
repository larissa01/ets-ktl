package br.com.digio.uber.changeregistration.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel

class CRAddressNeighborhoodViewModel : BaseViewModel() {
    val value: MutableLiveData<String> = MutableLiveData()
    val enableButton: MutableLiveData<Boolean> = MutableLiveData()

    fun onValueChange(value: String?) {
        enableButton.value = value?.isNotEmpty()
    }
}