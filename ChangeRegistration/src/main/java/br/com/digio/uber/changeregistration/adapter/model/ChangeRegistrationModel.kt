package br.com.digio.uber.changeregistration.adapter.model

import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.changeregistration.uimodel.ChangeRegistrationItemListUiModel
import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory

sealed class ChangeRegistrationModel : AdapterViewModel<ChangeRegistrationModel> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<ChangeRegistrationModel>) = typesFactory.type(
        model = this
    )
}

open class ChangeRegistrationModelItem(
    open val changeRegistrationItemListUiModel: ChangeRegistrationItemListUiModel
) : ChangeRegistrationModel()

data class ChangeRegistrationModelNotEditableItem(
    override val changeRegistrationItemListUiModel: ChangeRegistrationItemListUiModel
) : ChangeRegistrationModelItem(changeRegistrationItemListUiModel)

data class ChangeRegistrationModelAddressItem(
    val addressRegistrationUiModel: AddressRegistrationUiModel
) : ChangeRegistrationModel()

object ChangeRegistrationSeparatorItem : ChangeRegistrationModel()

data class ChangeRegistrationDeliveryAddressItem(
    val addressRegistrationUiModel: AddressRegistrationUiModel
) : ChangeRegistrationModel()