package br.com.digio.uber.changeregistration.fragment

import android.view.View
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor

abstract class CRAddressAbstractFragment<T : ViewDataBinding, VM : BaseViewModel> :
    BaseViewModelFragment<T, VM>() {
    override val showDisplayShowTitle: Boolean = true

    override fun onNavigationClick(view: View) {
        super.onNavigationClick(view)
        pop()
    }

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE
}