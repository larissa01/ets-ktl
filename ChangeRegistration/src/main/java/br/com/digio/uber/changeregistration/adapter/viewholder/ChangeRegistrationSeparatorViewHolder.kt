package br.com.digio.uber.changeregistration.adapter.viewholder

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationSeparatorItem
import br.com.digio.uber.common.base.adapter.AbstractViewHolder

class ChangeRegistrationSeparatorViewHolder(
    private val viewDataBinding: ViewDataBinding
) : AbstractViewHolder<ChangeRegistrationSeparatorItem>(viewDataBinding.root) {
    override fun bind(item: ChangeRegistrationSeparatorItem) {
        viewDataBinding.executePendingBindings()
    }
}