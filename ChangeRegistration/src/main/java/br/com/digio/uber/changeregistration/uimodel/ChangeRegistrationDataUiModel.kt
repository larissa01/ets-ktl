package br.com.digio.uber.changeregistration.uimodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChangeRegistrationDataUiModel(
    val unchangeable: List<ChangeRegistrationItemListUiModel>,
    val changeable: List<ChangeRegistrationItemListUiModel>,
    val changeableAddresses: List<AddressRegistrationUiModel>?,
    val unchangeableAddresses: List<AddressRegistrationUiModel>?
) : Parcelable