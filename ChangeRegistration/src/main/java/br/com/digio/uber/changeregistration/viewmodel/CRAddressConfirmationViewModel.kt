package br.com.digio.uber.changeregistration.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.mapper.AddressRegistrationReverseMapper
import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.makeMessageSuccessObject
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.change.registration.UpdateAddressUseCase
import br.com.digio.uber.model.change.registration.ChangeRegistrationItemType
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.localeDefault

class CRAddressConfirmationViewModel constructor(
    private val updateAddressUseCase: UpdateAddressUseCase,
    private val addressRegistrationReverseMapper: AddressRegistrationReverseMapper,
    private val analytics: Analytics
) : BaseViewModel() {

    val title: MutableLiveData<Int> = MutableLiveData()
    val addressRegistrationUiModel: MutableLiveData<AddressRegistrationUiModel> = MutableLiveData()
    val showLiveHere: MutableLiveData<Boolean> = MutableLiveData()
    val invokePID: MutableLiveData<Boolean> = MutableLiveData()
    val sucessUpdateAddress: MutableLiveData<Boolean> = MutableLiveData()

    fun initializer(
        addressRegistrationUiModel: AddressRegistrationUiModel,
        onClickItem: OnClickItem
    ) {
        this.addressRegistrationUiModel.value = addressRegistrationUiModel
        setupTitle(addressRegistrationUiModel)
        super.initializer { view ->
            when (view.id) {
                R.id.btnNext -> invokePID.value = true // updateAddress(addressRegistrationUiModel, onClickItem, view)
                else -> onClickItem(view)
            }
        }
    }

    fun updateAddress(
        pidHash: String,
        view: View
    ) {
        addressRegistrationUiModel.value?.let { addressItem ->
            addressItem.pidHash = pidHash
            updateAddressUseCase(
                addressRegistrationReverseMapper.map(
                    addressItem
                )
            ).singleExec(
                onSuccessBaseViewModel = {
                    message.value = makeMessageSuccessObject {
                        icon = -1
                        title = R.string.address_changed
                        message = R.string.address_changed_message
                        onCloseDialog = {
                            pushTagToFirebase(addressItem.type)
                            sucessUpdateAddress.value = true
                        }
                    }
                }
            )
        }
    }

    private fun pushTagToFirebase(type: ChangeRegistrationItemType) {
        analytics.pushSimpleEvent(
            Tags.DATA_REGISTERED_CHANGED,
            Tags.PARAMETER_ITEM_CHANGE_REGISTRATION_CLICKED to
                type.name
                    .toLowerCase(
                        localeDefault
                    )
        )
    }

    private fun setupTitle(addressRegistrationUiModel: AddressRegistrationUiModel) {
        title.value =
            if (addressRegistrationUiModel.type == ChangeRegistrationItemType.RESIDENTIAL) {
                showLiveHere.value = true
                R.string.you_address_is_correct
            } else {
                showLiveHere.value = false
                R.string.you_address_of_delivery_is_correct
            }
    }

    fun getZipCodeFormatted(): String =
        addressRegistrationUiModel.value?.zipCode?.applyMask(Const.MaskPattern.CEP) ?: String()

    fun getComplement(): String =
        addressRegistrationUiModel.value?.complement?.let {
            "$it -"
        } ?: String()
}