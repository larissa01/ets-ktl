package br.com.digio.uber.changeregistration.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.changeregistration.uimodel.ZipCodeResUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.model.change.registration.ChangeRegistrationItemType

class CRAddressShowInfoViewModel : BaseViewModel() {
    val zipCodeRes: MutableLiveData<ZipCodeResUiModel> = MutableLiveData()
    val title: MutableLiveData<Int> = MutableLiveData()

    fun initializer(
        addressRegistrationUiModel: AddressRegistrationUiModel,
        zipCodeResUiModel: ZipCodeResUiModel,
        onClickItem: OnClickItem
    ) {
        zipCodeRes.value = zipCodeResUiModel
        setupTitle(addressRegistrationUiModel)
        super.initializer(onClickItem)
    }

    private fun setupTitle(addressRegistrationUiModel: AddressRegistrationUiModel) {
        title.value =
            if (addressRegistrationUiModel.type == ChangeRegistrationItemType.RESIDENTIAL) {
                R.string.this_is_your_address
            } else {
                R.string.this_is_your_address_for_delivery
            }
    }
}