package br.com.digio.uber.changeregistration.adapter.viewholder

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModelAddressItem
import br.com.digio.uber.common.base.adapter.ViewTypesListener

class ChangeRegistrationAddressViewHolder(
    viewDataBinding: ViewDataBinding,
    listener: ViewTypesListener<ChangeRegistrationModelAddressItem>
) : AbstractChangeRegistrationViewHolder<ChangeRegistrationModelAddressItem>(viewDataBinding, listener)