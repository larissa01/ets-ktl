package br.com.digio.uber.changeregistration.fragment

import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.changeregistration.BR
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.databinding.ChangeRegistrationAddressNeighborhoodFragmentBinding
import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressNeighborhoodViewModel
import br.com.digio.uber.common.extensions.navigateTo
import org.koin.android.ext.android.inject

class CRAddressNeighborhoodFragment :
    CRAddressAbstractFragment<ChangeRegistrationAddressNeighborhoodFragmentBinding, CRAddressNeighborhoodViewModel>() {
    override val bindingVariable: Int? = BR.crAddressNeighborhoodViewModel

    override val getLayoutId: Int? = R.layout.change_registration_address_neighborhood_fragment

    override val viewModel: CRAddressNeighborhoodViewModel? by inject()

    private val addressRegistrationUiModel: AddressRegistrationUiModel by lazy {
        CRAddressNeighborhoodFragmentArgs.fromBundle(requireArguments()).addressRegistrationUiModelKey
    }

    override fun initialize() {
        super.initialize()
        viewModel?.initializer {
            when (it.id) {
                R.id.btnNext -> navigateToNumber()
            }
        }
        setupObserver()
    }

    private fun navigateToNumber() {
        navigateTo(
            CRAddressNeighborhoodFragmentDirections
                .actionCRAddressNeighborhoodFragmentToChangeRegistrationAddressNumberFragment(
                    addressRegistrationUiModel.copy(neighborhood = viewModel?.value?.value)
                )
        )
    }

    private fun setupObserver() {
        viewModel?.value?.observe(
            this,
            Observer {
                viewModel?.onValueChange(it)
            }
        )
    }

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePasswordLayout?.toolbarChangePassword

    override fun tag(): String =
        CRAddressNeighborhoodFragment::class.java.name
}