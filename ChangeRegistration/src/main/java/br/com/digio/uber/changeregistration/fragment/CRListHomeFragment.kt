package br.com.digio.uber.changeregistration.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.changeregistration.BR
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.adapter.ChangeRegistrationAdapter
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationDeliveryAddressItem
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModel
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModelAddressItem
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModelItem
import br.com.digio.uber.changeregistration.databinding.ChangeRegistrationListHomeFragmentBinding
import br.com.digio.uber.changeregistration.viewmodel.CRListHomeViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.stateGetLiveData
import br.com.digio.uber.common.extensions.stateSetValue
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.change.registration.ChangeRegistrationItemType
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.RequestOnResult.FAQ.KEY_VALUE_FAQ_CHAT_DELIVERY_ADDRESS
import br.com.digio.uber.util.localeDefault
import org.koin.android.ext.android.inject

class CRListHomeFragment :
    BaseViewModelFragment<ChangeRegistrationListHomeFragmentBinding, CRListHomeViewModel>() {

    override val bindingVariable: Int = BR.changeRegistrationListHomeViewModel
    override val getLayoutId: Int = R.layout.change_registration_list_home_fragment
    override val viewModel: CRListHomeViewModel? by inject()

    private val analytics: Analytics by inject()

    override fun initialize() {
        super.initialize()
        setupObserver()
        binding?.recyclerChangeRegistration?.layoutManager = LinearLayoutManager(context)
        binding?.recyclerChangeRegistration?.adapter =
            ChangeRegistrationAdapter(
                listener = {
                    handleItemAdapter(it)
                }
            )
    }

    private fun handleItemAdapter(changeRegistrationModel: ChangeRegistrationModel) {
        when (changeRegistrationModel) {
            is ChangeRegistrationModelItem -> {
                pushDataTag(changeRegistrationModel)

                when (changeRegistrationModel.changeRegistrationItemListUiModel.type) {
                    ChangeRegistrationItemType.PHONE -> goToAbstractEditTextFragment(
                        changeRegistrationModel
                    )
                    ChangeRegistrationItemType.EMAIL -> goToAbstractEditTextFragment(
                        changeRegistrationModel
                    )
                    ChangeRegistrationItemType.INCOME -> goToAbstractEditTextFragment(
                        changeRegistrationModel
                    )
                    else -> {}
                }
            }
            is ChangeRegistrationModelAddressItem -> {
                pushAddressTag(changeRegistrationModel)
                navigateTo(
                    CRListHomeFragmentDirections
                        .actionChangeRegistrationListHomeFragmentToChangeRegistrationAddressCepFragment(
                            changeRegistrationModel.addressRegistrationUiModel
                        )
                )
            }
            is ChangeRegistrationDeliveryAddressItem -> context?.let { contextLet ->
                navigation.navigationToFaqChatActivity(contextLet, KEY_VALUE_FAQ_CHAT_DELIVERY_ADDRESS)
            }
            else -> {
                /* nothing */
            }
        }
    }

    private fun pushDataTag(changeRegistrationModelItem: ChangeRegistrationModelItem) {
        analytics.pushSimpleEvent(
            Tags.DATA_REGISTERED_ITEM,
            Tags.PARAMETER_ITEM_CHANGE_REGISTRATION_CLICKED to changeRegistrationModelItem
                .changeRegistrationItemListUiModel
                .type
                .name
                .toLowerCase(
                    localeDefault
                )
        )
    }

    private fun pushAddressTag(changeRegistrationModelAddressItem: ChangeRegistrationModelAddressItem) {
        analytics.pushSimpleEvent(
            Tags.DATA_REGISTERED_ITEM,
            Tags.PARAMETER_ITEM_CHANGE_REGISTRATION_CLICKED to changeRegistrationModelAddressItem
                .addressRegistrationUiModel
                .type
                .name
                .toLowerCase(
                    localeDefault
                )
        )
    }

    private fun setupObserver() {
        val liveData =
            stateGetLiveData<Boolean>(Const.RequestOnResult.ChangeRegistration.KEY_CHANGE_REGISTRATION_BACK)
        if (liveData?.hasActiveObservers() == false) {
            liveData.observe(
                this,
                Observer {
                    if (it == true) {
                        viewModel?.updateItems(true)
                        stateSetValue(
                            Const.RequestOnResult.ChangeRegistration.KEY_CHANGE_REGISTRATION_BACK,
                            false
                        )
                    }
                }
            )
        }

        viewModel?.finishProcess?.observe(
            this,
            {
                if (it == true) {
                    activity?.finish()
                    viewModel?.finishProcess?.value = false
                }
            }
        )
    }

    private fun goToAbstractEditTextFragment(it: ChangeRegistrationModelItem) {
        navigateTo(
            CRListHomeFragmentDirections
                .actionChangeRegistrationListHomeFragmentToAbstractEditTextFragment(
                    it.changeRegistrationItemListUiModel
                )
        )
    }

    override fun tag(): String =
        CRListHomeFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.BLACK

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePasswordLayout?.toolbarChangePassword

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true

    override fun onNavigationClick(view: View) {
        super.onNavigationClick(view)
        activity?.finish()
    }

    override fun onBackPressed() {
        activity?.finish()
    }
}