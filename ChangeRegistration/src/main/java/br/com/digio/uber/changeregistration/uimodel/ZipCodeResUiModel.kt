package br.com.digio.uber.changeregistration.uimodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ZipCodeResUiModel constructor(
    val zipCode: String? = null,
    val zipCodeFormated: String? = null,
    val state: String? = null,
    val street: String? = null,
    val neighborhood: String? = null,
    val city: String? = null
) : Parcelable