package br.com.digio.uber.changeregistration.uimodel

import android.os.Parcelable
import br.com.digio.uber.model.change.registration.ChangeRegistrationItemType
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddressRegistrationUiModel(
    val id: Long?,
    val street: String?,
    val addressFormatted: String?,
    val city: String?,
    val complement: String?,
    val country: String?,
    val neighborhood: String?,
    val number: Int?,
    var state: String?,
    val zipCode: String?,
    val addressTypeId: Long?,
    val title: String,
    val type: ChangeRegistrationItemType,
    val order: Int?,
    var pidHash: String? = null
) : Parcelable