package br.com.digio.uber.changeregistration.di.viewmodel

import br.com.digio.uber.changeregistration.viewmodel.CRAbstractEditTextViewModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressCepViewModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressComplementViewModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressConfirmationViewModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressNeighborhoodViewModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressNumberViewModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressShowInfoViewModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressStreetViewModel
import br.com.digio.uber.changeregistration.viewmodel.CRListHomeViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val changeRegistrationViewModelModule = module {
    viewModel { CRListHomeViewModel(get(), get()) }
    viewModel { CRAbstractEditTextViewModel(get(), get(), get()) }
    viewModel { CRAddressCepViewModel(get(), get()) }
    viewModel { CRAddressShowInfoViewModel() }
    viewModel { CRAddressNumberViewModel() }
    viewModel { CRAddressComplementViewModel() }
    viewModel { CRAddressConfirmationViewModel(get(), get(), get()) }
    viewModel { CRAddressStreetViewModel() }
    viewModel { CRAddressNeighborhoodViewModel() }
}