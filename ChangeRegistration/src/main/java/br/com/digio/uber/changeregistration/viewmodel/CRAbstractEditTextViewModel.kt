@file:Suppress("TooManyFunctions")

package br.com.digio.uber.changeregistration.viewmodel

import android.text.InputType
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.mapper.ChangeRegistrationItemListReverseMapper
import br.com.digio.uber.changeregistration.uimodel.ChangeRegistrationItemListUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.common.typealiases.OnTextChanged
import br.com.digio.uber.domain.usecase.change.registration.SetChangeRegistrationItemList
import br.com.digio.uber.model.change.registration.ChangeRegistrationItemType
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.FieldsRegex
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.localeDefault
import br.com.digio.uber.util.moneyToFloat
import br.com.digio.uber.util.moneyToFloatOrNull
import br.com.digio.uber.util.safeLet

class CRAbstractEditTextViewModel constructor(
    private val setChangeRegistrationItemList: SetChangeRegistrationItemList,
    private val changeRegistrationItemListReverseMapper: ChangeRegistrationItemListReverseMapper,
    private val analytics: Analytics
) : BaseViewModel() {

    private val changeRegistration: MutableLiveData<ChangeRegistrationItemListUiModel> =
        MutableLiveData()
    val title: MutableLiveData<Int> = MutableLiveData()
    val hint: MutableLiveData<Int> = MutableLiveData()
    val errorHint: MutableLiveData<Int> = MutableLiveData()
    val inputTextType: MutableLiveData<Int> = MutableLiveData()
    val value: MutableLiveData<String> = MutableLiveData()
    val mask: MutableLiveData<String> = MutableLiveData()

    val enableButton: MutableLiveData<Boolean> = MutableLiveData()

    val successChange: MutableLiveData<Boolean> = MutableLiveData()

    val invokePID: MutableLiveData<Boolean> = MutableLiveData()

    fun initializer(
        changeRegistrationItemListUiModel: ChangeRegistrationItemListUiModel,
        onClickItem: OnClickItem? = null
    ) {
        this.changeRegistration.value = changeRegistrationItemListUiModel
        super.initializer {
            when (it.id) {
                R.id.btnNext -> invokePID.value = true
                else -> onClickItem?.invoke(it)
            }
        }

        when (changeRegistrationItemListUiModel.type) {
            ChangeRegistrationItemType.PHONE -> makeScreeToPhone()
            ChangeRegistrationItemType.EMAIL -> makeScreeToEmail()
            ChangeRegistrationItemType.INCOME -> makeScreeToInvoice()
        }
    }

    fun manageItemClicked(pidHash: String) {
        changeRegistration.value?.let {
            when (it.type) {
                ChangeRegistrationItemType.PHONE -> updatePhoneOrEmail(
                    value.value,
                    it.type,
                    pidHash
                )
                ChangeRegistrationItemType.EMAIL -> updatePhoneOrEmail(
                    value.value,
                    it.type,
                    pidHash
                )
                ChangeRegistrationItemType.INCOME -> updateInfoIncome(
                    value.value?.moneyToFloatOrNull(),
                    it.type,
                    pidHash
                )
                else -> {}
            }
        }
    }

    private fun updateInfoIncome(
        value: Float?,
        type: ChangeRegistrationItemType,
        pidHash: String
    ) {
        safeLet(value, changeRegistration.value) { valueLet, changeRegistrationLet ->
            setChangeRegistrationItemList(
                changeRegistrationItemListReverseMapper.map(
                    changeRegistrationLet.copy(
                        patrimony = valueLet,
                        pidHash = pidHash
                    )
                )
            ).singleExec(
                onSuccessBaseViewModel = {
                    pushTagToFirebase(type)
                    successChange.value = true
                }
            )
        }
    }

    private fun updatePhoneOrEmail(
        value: String?,
        type: ChangeRegistrationItemType,
        pidHash: String
    ) {
        safeLet(value, changeRegistration.value) { valueLet, changeRegistrationLet ->
            setChangeRegistrationItemList(
                changeRegistrationItemListReverseMapper.map(
                    changeRegistrationLet.copy(
                        text = valueLet,
                        pidHash = pidHash
                    )
                )
            ).singleExec(
                onSuccessBaseViewModel = {
                    pushTagToFirebase(type)
                    successChange.value = true
                }
            )
        }
    }

    private fun pushTagToFirebase(type: ChangeRegistrationItemType) {
        analytics.pushSimpleEvent(
            Tags.DATA_REGISTERED_CHANGED,
            Tags.PARAMETER_ITEM_CHANGE_REGISTRATION_CLICKED to
                type.name
                    .toLowerCase(
                        localeDefault
                    )
        )
    }

    private fun makeScreeToInvoice() {
        title.value = R.string.whats_is_you_invoice_title
        hint.value = R.string.invoice_hint
        inputTextType.value = InputType.TYPE_CLASS_NUMBER
        value.value = changeRegistration.value?.patrimonyFormat
    }

    private fun makeScreeToEmail() {
        title.value = R.string.whats_is_you_email_title
        hint.value = R.string.email_hint
        inputTextType.value = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        value.value = changeRegistration.value?.text
    }

    private fun makeScreeToPhone() {
        title.value = R.string.whats_is_you_cel_number_title
        hint.value = R.string.number_of_and_ddd_hint
        inputTextType.value = InputType.TYPE_CLASS_NUMBER
        mask.value = Const.MaskPattern.PHONE
        value.value = changeRegistration.value?.text?.applyMask(Const.MaskPattern.PHONE)
    }

    private fun validatePhone(value: String): Boolean =
        value.length >= PHONE_MAX_CHARACTERS

    private fun validateEmail(email: String): Boolean =
        if (email.isNotEmpty()) {
            if (email.matches(FieldsRegex.EMAIL.regex.toRegex())) {
                errorHint.value = null
                true
            } else {
                errorHint.value = R.string.email_invalid
                false
            }
        } else {
            errorHint.value = null
            false
        }

    private fun validateIncome(income: String): Boolean {
        return if (income.isNotEmpty()) {
            val number = income.moneyToFloat()
            if (number in MIN_INCOME..MAX_INCOME) {
                errorHint.value = null
                true
            } else {
                errorHint.value = R.string.income_invalid
                false
            }
        } else {
            errorHint.value = null
            false
        }
    }

    val onValueChange: OnTextChanged = { value, _, _, _ ->
        validateValue(value)
    }

    fun validateValue(value: CharSequence?) {
        enableButton.value = when (changeRegistration.value?.type) {
            ChangeRegistrationItemType.PHONE -> validatePhone(value.toString())
            ChangeRegistrationItemType.EMAIL -> validateEmail(value.toString())
            ChangeRegistrationItemType.INCOME -> validateIncome(value.toString())
            else -> false
        }
    }

    companion object {
        const val PHONE_MAX_CHARACTERS = 15
        const val MIN_INCOME = 1f
        const val MAX_INCOME = 1000000f
    }
}