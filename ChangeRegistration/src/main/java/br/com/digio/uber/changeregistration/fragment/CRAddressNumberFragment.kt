package br.com.digio.uber.changeregistration.fragment

import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.changeregistration.BR
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.databinding.ChangeRegistrationAddressNumberFragmentBinding
import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressNumberViewModel
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.util.unmask
import org.koin.android.ext.android.inject

class CRAddressNumberFragment :
    CRAddressAbstractFragment<ChangeRegistrationAddressNumberFragmentBinding, CRAddressNumberViewModel>() {
    override val bindingVariable: Int? = BR.changeRegistrationAddressNumberViewModel

    override val getLayoutId: Int? = R.layout.change_registration_address_number_fragment

    override val viewModel: CRAddressNumberViewModel? by inject()

    private val addressRegistrationUiModel: AddressRegistrationUiModel by lazy {
        CRAddressCepFragmentArgs.fromBundle(requireArguments()).addressRegistrationUiModelKey
    }

    override fun tag(): String = CRAddressNumberFragment::class.java.name

    override fun initialize() {
        super.initialize()

        viewModel?.initializer {
            when (it.id) {
                R.id.btnNext -> navigateToComplement()
            }
        }

        viewModel?.value?.observe(
            this,
            Observer {
                viewModel?.onTextChange(it)
            }
        )
    }

    private fun navigateToComplement() {
        navigateTo(
            CRAddressNumberFragmentDirections
                .actionChangeRegistrationAddressNumberFragmentToChangeRegistrationAddressComplementFragment(
                    addressRegistrationUiModel.copy(
                        number = viewModel?.value?.value?.unmask()?.toIntOrNull()
                    )
                )
        )
    }

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePasswordLayout?.toolbarChangePassword
}