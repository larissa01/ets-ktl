package br.com.digio.uber.changeregistration.fragment

import android.content.Intent
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.changeregistration.BR
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.databinding.ChangeRegistrationAddressConfirmationFragmentBinding
import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressConfirmationViewModel
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.stateSetValue
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.util.Const
import kotlinx.android.synthetic.main.change_registration_address_cep_fragment.*
import org.koin.android.ext.android.inject

class CRAddressConfirmationFragment :
    CRAddressAbstractFragment<ChangeRegistrationAddressConfirmationFragmentBinding, CRAddressConfirmationViewModel>() {
    override val bindingVariable: Int? = BR.crAddressConfirmationViewModel

    override val getLayoutId: Int? = R.layout.change_registration_address_confirmation_fragment

    override val viewModel: CRAddressConfirmationViewModel? by inject()

    private val addressRegistrationUiModel: AddressRegistrationUiModel by lazy {
        CRAddressConfirmationFragmentArgs.fromBundle(requireArguments()).addressRegistrationUiModelKey
    }

    override fun tag(): String = CRAddressConfirmationFragment::class.java.name

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePasswordLayout?.toolbarChangePassword

    override fun initialize() {
        super.initialize()
        viewModel?.initializer(addressRegistrationUiModel) {
            when (it.id) {
                R.id.btnNext -> navigateToHome()
                R.id.btnBack -> navigateToCep()
            }
        }
        setupObservers()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Const.RequestOnResult.PID.PID_REQUEST_CODE &&
            resultCode == Const.RequestOnResult.PID.RESULT_PID_SUCCESS
        ) {
            data?.getStringExtra(Const.RequestOnResult.PID.KEY_PID_HASH)?.let {
                viewModel?.updateAddress(it, btnNext)
            }
        }
    }

    private fun setupObservers() {
        viewModel?.invokePID?.observe(
            this,
            Observer {
                if (it) {
                    navigation.navigationToPidActivity(this, PidType.USER_DATA_MANAGER)
                }
            }
        )

        viewModel?.sucessUpdateAddress?.observe(
            this,
            Observer { updateSuccess ->
                if (updateSuccess) {
                    navigateToHome()
                }
            }
        )
    }

    private fun navigateToHome() {
        navigateTo(
            CRAddressConfirmationFragmentDirections
                .actionCRAddressConfirmationFragmentToChangeRegistrationListHomeFragment()
        )
        stateSetValue(Const.RequestOnResult.ChangeRegistration.KEY_CHANGE_REGISTRATION_BACK, true)
    }

    private fun navigateToCep() {
        navigateTo(
            CRAddressConfirmationFragmentDirections
                .actionCRAddressConfirmationFragmentToChangeRegistrationAddressCepFragment(
                    addressRegistrationUiModel
                )
        )
    }
}