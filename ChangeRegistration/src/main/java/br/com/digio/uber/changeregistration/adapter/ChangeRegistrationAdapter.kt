package br.com.digio.uber.changeregistration.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationDeliveryAddressItem
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModel
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModelAddressItem
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModelItem
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModelNotEditableItem
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationSeparatorItem
import br.com.digio.uber.changeregistration.adapter.viewholder.ChangeRegistrationAddressViewHolder
import br.com.digio.uber.changeregistration.adapter.viewholder.ChangeRegistrationDeliveryAddressViewHolder
import br.com.digio.uber.changeregistration.adapter.viewholder.ChangeRegistrationSeparatorViewHolder
import br.com.digio.uber.changeregistration.adapter.viewholder.ChangeRegistrationViewHolder
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

typealias OnChangeRegistrationAdapterItemClick = (item: ChangeRegistrationModel) -> Unit

class ChangeRegistrationAdapter(
    private var values: MutableList<ChangeRegistrationModel> = mutableListOf(),
    private val listener: OnChangeRegistrationAdapterItemClick
) : BaseRecyclerViewAdapter<ChangeRegistrationModel, ChangeRegistrationAdapter.ViewTypesDataBindingFactoryImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl =
        ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): ChangeRegistrationModel =
        values[position]

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<ChangeRegistrationModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder =
            typeFactory.holder(type = viewType, view = viewDataBinding, listener = listener)

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<ChangeRegistrationModel>
    }

    override fun getItemCount(): Int =
        values.size

    override fun onBindViewHolder(
        holder: AbstractViewHolder<ChangeRegistrationModel>,
        position: Int
    ) =
        holder.bind(values[holder.adapterPosition])

    fun setValue(values: List<ChangeRegistrationModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }

    override fun replaceItems(list: List<Any>) =
        setValue(list.safeHeritage())

    class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<ChangeRegistrationModel> {
        override fun type(model: ChangeRegistrationModel): Int =
            when (model) {
                is ChangeRegistrationModelNotEditableItem -> R.layout.change_registration_model_not_editable_item
                is ChangeRegistrationModelItem -> R.layout.change_registration_model_item
                is ChangeRegistrationSeparatorItem -> R.layout.change_registration_separator_item
                is ChangeRegistrationModelAddressItem -> R.layout.change_registration_model_address_item
                is ChangeRegistrationDeliveryAddressItem -> R.layout.change_registration_model_delivery_address
            }

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<ChangeRegistrationModel>
        ): AbstractViewHolder<*> =
            when (type) {
                R.layout.change_registration_model_item -> ChangeRegistrationViewHolder(
                    view,
                    listener
                )
                R.layout.change_registration_model_not_editable_item -> ChangeRegistrationViewHolder(
                    view,
                    listener
                )
                R.layout.change_registration_separator_item -> ChangeRegistrationSeparatorViewHolder(
                    view
                )
                R.layout.change_registration_model_address_item -> ChangeRegistrationAddressViewHolder(
                    view,
                    listener
                )
                R.layout.change_registration_model_delivery_address -> ChangeRegistrationDeliveryAddressViewHolder(
                    view,
                    listener
                )
                else -> throw IllegalStateException("Invalid view type")
            }
    }
}