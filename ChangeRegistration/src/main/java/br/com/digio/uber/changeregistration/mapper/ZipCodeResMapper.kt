package br.com.digio.uber.changeregistration.mapper

import br.com.digio.uber.changeregistration.uimodel.ZipCodeResUiModel
import br.com.digio.uber.model.change.registration.ZipCodeRes
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.mapper.AbstractMapper

interface ZipCodeResMapper : AbstractMapper<ZipCodeRes, ZipCodeResUiModel>

class ZipCodeResMapperImpl : ZipCodeResMapper {
    override fun map(param: ZipCodeRes): ZipCodeResUiModel =
        ZipCodeResUiModel(
            zipCode = param.zipCode,
            zipCodeFormated = param.zipCode?.applyMask(Const.MaskPattern.CEP),
            state = param.state,
            street = param.street,
            neighborhood = param.neighborhood,
            city = param.city
        )
}