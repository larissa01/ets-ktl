package br.com.digio.uber.changeregistration.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModel
import br.com.digio.uber.changeregistration.mapper.ChangeRegistrationDataListMapper
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.change.registration.GetChangeRegistrationDataUseCase

class CRListHomeViewModel constructor(
    private val getChangeRegistrationDataUseCase: GetChangeRegistrationDataUseCase,
    private val changeRegistrationDataMapper: ChangeRegistrationDataListMapper
) : BaseViewModel() {

    val items: MutableLiveData<List<ChangeRegistrationModel>> = MutableLiveData()
    val finishProcess: MutableLiveData<Boolean> = MutableLiveData()

    init {
        updateItems()
    }

    fun updateItems(forceReload: Boolean = false) {
        getChangeRegistrationDataUseCase(Unit, forceReload).singleExec(
            onSuccessBaseViewModel = {
                items.value = changeRegistrationDataMapper.map(it)
            },
            onError = { _, error, _ ->
                error.apply {
                    onCloseDialog = {
                        finishProcess.value = true
                    }
                }
            }
        )
    }
}