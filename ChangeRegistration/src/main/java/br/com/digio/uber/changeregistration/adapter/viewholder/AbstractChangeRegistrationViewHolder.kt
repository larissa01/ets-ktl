package br.com.digio.uber.changeregistration.adapter.viewholder

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.changeregistration.BR
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModel
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.ViewTypesListener

abstract class AbstractChangeRegistrationViewHolder<T : ChangeRegistrationModel> constructor(
    private val viewDataBinding: ViewDataBinding,
    private val listener: ViewTypesListener<T>
) : AbstractViewHolder<T>(viewDataBinding.root) {
    override fun bind(item: T) {
        viewDataBinding.setVariable(BR.changeRegistrationModelItem, item)
        viewDataBinding.setVariable(BR.changeRegistrationViewHolder, this)
        viewDataBinding.executePendingBindings()
    }

    fun onClickItem(itemBody: T) {
        listener.invoke(itemBody)
    }

    open fun getValue(itemBody: T): String? = null
}