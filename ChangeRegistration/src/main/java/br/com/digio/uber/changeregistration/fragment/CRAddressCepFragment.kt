package br.com.digio.uber.changeregistration.fragment

import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.changeregistration.BR
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.databinding.ChangeRegistrationAddressCepFragmentBinding
import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.changeregistration.uimodel.ZipCodeResUiModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressCepViewModel
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.extensions.openInBrowserLink
import br.com.digio.uber.util.Const
import org.koin.android.ext.android.inject

class CRAddressCepFragment :
    CRAddressAbstractFragment<ChangeRegistrationAddressCepFragmentBinding, CRAddressCepViewModel>() {

    override val bindingVariable: Int? = BR.changeRegistrationAddressCepViewModel
    override val getLayoutId: Int? = R.layout.change_registration_address_cep_fragment
    override val viewModel: CRAddressCepViewModel? by inject()

    private val addressRegistrationUiModel: AddressRegistrationUiModel by lazy {
        CRAddressCepFragmentArgs.fromBundle(requireArguments()).addressRegistrationUiModelKey
    }

    override fun initialize() {
        super.initialize()
        viewModel?.value?.value = addressRegistrationUiModel.zipCode
        viewModel?.initializer(addressRegistrationUiModel) {
            when (it.id) {
                R.id.btnForgotCep ->
                    activity?.openInBrowserLink(Const.URI.URL_CORREIOS)
            }
        }
        setupObserver()
    }

    private fun setupObserver() {
        viewModel?.finishGoToQuestion?.observe(
            this,
            Observer { zipCodeUi ->
                if (zipCodeUi != null) {
                    navigateToInfoOrStreet(zipCodeUi)
                    viewModel?.finishGoToQuestion?.value = null
                }
            }
        )
    }

    private fun navigateToInfoOrStreet(zipCodeUi: ZipCodeResUiModel) {
        navigateTo(
            CRAddressCepFragmentDirections
                .actionChangeRegistrationAddressCepFragmentToChangeRegistrationAddressShowInfoFragment2(
                    addressRegistrationUiModel,
                    zipCodeUi
                )
        )
    }

    override fun tag(): String = CRAddressCepFragment::class.java.name

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePasswordLayout?.toolbarChangePassword
}