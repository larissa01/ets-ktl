package br.com.digio.uber.changeregistration.mapper

import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationDeliveryAddressItem
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModel
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModelAddressItem
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModelItem
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationModelNotEditableItem
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationSeparatorItem
import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.changeregistration.uimodel.ChangeRegistrationDataUiModel
import br.com.digio.uber.changeregistration.uimodel.ChangeRegistrationItemListUiModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.model.change.registration.AddressRegistration
import br.com.digio.uber.model.change.registration.AddressRegistrationRequest
import br.com.digio.uber.model.change.registration.ChangeRegistrationData
import br.com.digio.uber.model.change.registration.ChangeRegistrationItemList
import br.com.digio.uber.model.change.registration.ChangeRegistrationItemType
import br.com.digio.uber.model.change.registration.ChangeRegistrationRequest
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.FORMAT_PT_BR_DATE
import br.com.digio.uber.util.FORMAT_US_DATE
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.capitalizeWords
import br.com.digio.uber.util.getDate
import br.com.digio.uber.util.getDateBR
import br.com.digio.uber.util.localeDefault
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.util.toCalendar
import br.com.digio.uber.util.toMoney
import br.com.digio.uber.util.unmask

interface ChangeRegistrationDataListMapper :
    AbstractMapper<ChangeRegistrationData, List<ChangeRegistrationModel>>

class ChangeRegistrationDataListMapperImpl constructor(
    private val changeRegistrationDataMapper: ChangeRegistrationDataMapper
) : ChangeRegistrationDataListMapper {
    override fun map(param: ChangeRegistrationData): List<ChangeRegistrationModel> {
        val changeRegistrationUiModel = changeRegistrationDataMapper.map(param)
        return mutableListOf<ChangeRegistrationModel>().apply {
            add(ChangeRegistrationSeparatorItem)
            addAll(changeRegistrationUiModel.unchangeable.map(::ChangeRegistrationModelNotEditableItem))
            add(ChangeRegistrationSeparatorItem)
            addAll(changeRegistrationUiModel.changeable.map(::ChangeRegistrationModelItem))
            add(ChangeRegistrationSeparatorItem)
            addAll(
                changeRegistrationUiModel
                    .changeableAddresses
                    ?.map(::ChangeRegistrationModelAddressItem) ?: emptyList()
            )
            changeRegistrationUiModel.unchangeableAddresses
                ?.firstOrNull()
                ?.let { firstUnchangeDeliveryAddress ->
                    add(ChangeRegistrationDeliveryAddressItem(firstUnchangeDeliveryAddress))
                }
        }
    }
}

interface ChangeRegistrationDataMapper :
    AbstractMapper<ChangeRegistrationData, ChangeRegistrationDataUiModel>

class ChangeRegistrationDataMapperImpl constructor(
    private val changeRegistrationItemListMapper: ChangeRegistrationItemListMapper,
    private val addressRegistrationMapper: AddressRegistrationMapper
) : ChangeRegistrationDataMapper {
    override fun map(param: ChangeRegistrationData): ChangeRegistrationDataUiModel =
        ChangeRegistrationDataUiModel(
            unchangeable = param.unchangeable.sortedBy { it.order }
                .map { changeRegistrationItemListMapper.map(it) },
            changeable = param.changeable.sortedBy { it.order }
                .map { changeRegistrationItemListMapper.map(it).copy(isEditable = true) },
            changeableAddresses = param.changeableAddress?.sortedBy { it.order }
                ?.mapNotNull { addressRegistrationMapper.map(it) },
            unchangeableAddresses = param.unchangeableAddresses?.sortedBy { it.order }
                ?.mapNotNull { addressRegistrationMapper.map(it) }
        )
}

interface ChangeRegistrationItemListMapper :
    AbstractMapper<ChangeRegistrationItemList, ChangeRegistrationItemListUiModel>

class ChangeRegistrationItemListMapperImpl constructor(
    private val resourceManager: ResourceManager
) : ChangeRegistrationItemListMapper {
    override fun map(param: ChangeRegistrationItemList): ChangeRegistrationItemListUiModel =
        ChangeRegistrationItemListUiModel(
            patrimony = checkIfIsTypeIncomeToPatrimony(param),
            patrimonyFormat = checkIsTypeIncomeToPatrimonyFormat(param),
            text = when {
                (
                    param.type == ChangeRegistrationItemType.PROFESSION ||
                        param.type == ChangeRegistrationItemType.NAME ||
                        param.type == ChangeRegistrationItemType.BIRTHDATE
                    ) &&
                    param.value == null -> resourceManager.getString(R.string.unassigned_profession)

                param.type == ChangeRegistrationItemType.BIRTHDATE -> param.value?.toCalendar(
                    FORMAT_US_DATE
                )?.getDateBR()
                param.type == ChangeRegistrationItemType.PROFESSION -> param.value?.toLowerCase(
                    localeDefault
                )?.capitalizeWords()
                param.type == ChangeRegistrationItemType.NAME -> param.value?.toLowerCase(
                    localeDefault
                )?.capitalizeWords()
                param.type == ChangeRegistrationItemType.DOCUMENT -> param.value?.applyMask(
                    Const.MaskPattern.CPF
                )
                param.type == ChangeRegistrationItemType.SECONDARY_DOC -> param.value?.applyMask(
                    Const.MaskPattern.RG
                )
                else -> param.value
            },
            phoneNumberFormated = if (param.type == ChangeRegistrationItemType.PHONE) {
                param.value?.applyMask(Const.MaskPattern.PHONE)
            } else {
                null
            },
            title = param.title,
            type = param.type,
            order = param.order,
            pidHash = param.pidHash ?: ""
        )

    private fun checkIsTypeIncomeToPatrimonyFormat(param: ChangeRegistrationItemList): String? {
        return if (param.type == ChangeRegistrationItemType.INCOME) {
            param.valueNumber?.toMoney(true)
        } else {
            null
        }
    }

    private fun checkIfIsTypeIncomeToPatrimony(param: ChangeRegistrationItemList): Float? {
        return if (
            param.type == ChangeRegistrationItemType.INCOME &&
            (param.valueNumber ?: 0f) > 0f
        ) {
            param.valueNumber
        } else {
            null
        }
    }
}

interface ChangeRegistrationItemListReverseMapper :
    AbstractMapper<ChangeRegistrationItemListUiModel, ChangeRegistrationRequest>

class ChangeRegistrationItemListReverseMapperImpl constructor(
    private val resourceManager: ResourceManager
) : ChangeRegistrationItemListReverseMapper {
    override fun map(param: ChangeRegistrationItemListUiModel): ChangeRegistrationRequest =
        ChangeRegistrationRequest(
            param.pidHash,
            ChangeRegistrationItemList(
                value = when {
                    param.type == ChangeRegistrationItemType.BIRTHDATE -> param.text?.toCalendar(
                        FORMAT_PT_BR_DATE
                    )?.getDate(FORMAT_US_DATE)
                    (
                        param.type == ChangeRegistrationItemType.NAME ||
                            param.type == ChangeRegistrationItemType.BIRTHDATE
                        ) &&
                        param.text == resourceManager.getString(R.string.unassigned_profession) -> null
                    param.type == ChangeRegistrationItemType.DOCUMENT ||
                        param.type == ChangeRegistrationItemType.SECONDARY_DOC -> param.text?.unmask()
                    param.type == ChangeRegistrationItemType.PHONE -> param.text?.unmask()
                    else -> param.text
                },
                valueNumber = param.patrimony,
                title = param.title,
                type = param.type,
                order = param.order
            )
        )
}

interface AddressRegistrationMapper :
    AbstractMapper<AddressRegistration, AddressRegistrationUiModel?>

class AddressRegistrationMapperImpl : AddressRegistrationMapper {

    private fun checkHaveComplementAndReturnIt(param: AddressRegistration): String =
        if (!param.complement.isNullOrEmpty()) {
            "- ${param.complement}, "
        } else {
            String()
        }

    override fun map(param: AddressRegistration): AddressRegistrationUiModel =
        AddressRegistrationUiModel(
            id = param.id,
            street = param.street.toString(),
            addressFormatted = "${param.street.toString().toLowerCase(localeDefault)
                .capitalizeWords()}, " +
                "${param.number ?: 0} ${checkHaveComplementAndReturnIt(param)}" +
                "${param.neighborhood}, " +
                "${param.city?.toLowerCase(localeDefault)?.capitalizeWords() ?: String()}, " +
                "${param.state}\n" +
                param.zipCode.toString().applyMask(Const.MaskPattern.CEP),
            city = param.city.toString(),
            complement = param.complement.toString(),
            country = param.country.toString(),
            neighborhood = param.neighborhood.toString(),
            number = param.number ?: 0,
            state = param.state.toString(),
            zipCode = param.zipCode.toString(),
            addressTypeId = param.addressTypeId,
            type = param.type ?: ChangeRegistrationItemType.UNKNOWN,
            order = param.order,
            title = param.title
        )
}

interface AddressRegistrationReverseMapper :
    AbstractMapper<AddressRegistrationUiModel, AddressRegistrationRequest>

class AddressRegistrationReverseMapperImpl :
    AddressRegistrationReverseMapper {
    override fun map(param: AddressRegistrationUiModel): AddressRegistrationRequest =
        AddressRegistrationRequest(
            param.pidHash ?: "",
            AddressRegistration(
                id = param.id,
                street = param.street,
                city = param.city,
                complement = param.complement,
                country = param.country,
                neighborhood = param.neighborhood,
                number = param.number,
                state = param.state,
                zipCode = param.zipCode,
                addressTypeId = param.addressTypeId ?: -1,
                title = param.title,
                order = param.order,
                type = param.type
            )
        )
}