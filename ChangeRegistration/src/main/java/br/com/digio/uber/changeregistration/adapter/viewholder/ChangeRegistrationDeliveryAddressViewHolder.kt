package br.com.digio.uber.changeregistration.adapter.viewholder

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.changeregistration.BR
import br.com.digio.uber.changeregistration.adapter.model.ChangeRegistrationDeliveryAddressItem
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.ViewTypesListener

class ChangeRegistrationDeliveryAddressViewHolder(
    private val viewDataBinding: ViewDataBinding,
    private val listener: ViewTypesListener<ChangeRegistrationDeliveryAddressItem>
) : AbstractViewHolder<ChangeRegistrationDeliveryAddressItem>(viewDataBinding.root) {
    override fun bind(item: ChangeRegistrationDeliveryAddressItem) {
        viewDataBinding.setVariable(BR.changeRegistrationDeliveryAddressItem, item)
        viewDataBinding.setVariable(BR.changeRegistrationDeliveryAddressViewHolder, this)
        viewDataBinding.executePendingBindings()
    }

    fun onClickItem(item: ChangeRegistrationDeliveryAddressItem) =
        listener(item)
}