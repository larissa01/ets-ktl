package br.com.digio.uber.changeregistration.fragment

import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.changeregistration.BR
import br.com.digio.uber.changeregistration.R
import br.com.digio.uber.changeregistration.databinding.ChangeRegistrationAddressComplementFragmentBinding
import br.com.digio.uber.changeregistration.uimodel.AddressRegistrationUiModel
import br.com.digio.uber.changeregistration.viewmodel.CRAddressComplementViewModel
import br.com.digio.uber.common.extensions.navigateTo
import org.koin.android.ext.android.inject

class CRAddressComplementFragment :
    CRAddressAbstractFragment<ChangeRegistrationAddressComplementFragmentBinding, CRAddressComplementViewModel>() {
    override val bindingVariable: Int? = BR.changeRegistrationAddressComplementViewModel

    override val getLayoutId: Int? = R.layout.change_registration_address_complement_fragment

    override val viewModel: CRAddressComplementViewModel? by inject()

    private val addressRegistrationUiModel: AddressRegistrationUiModel by lazy {
        CRAddressComplementFragmentArgs.fromBundle(requireArguments()).addressRegistrationUiModelKey
    }

    override fun tag(): String =
        CRAddressComplementFragment::class.java.name

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePasswordLayout?.toolbarChangePassword

    override fun initialize() {
        super.initialize()

        viewModel?.initializer {
            when (it.id) {
                R.id.btnNext -> navigateToConfirmation(viewModel?.value?.value)
                R.id.btnNotHaveComplement -> navigateToConfirmation()
            }
        }

        setupObserver()
    }

    private fun navigateToConfirmation(complement: String? = null) {
        navigateTo(
            CRAddressComplementFragmentDirections
                .actionChangeRegistrationAddressComplementFragmentToCRAddressConfirmationFragment(
                    addressRegistrationUiModel.copy(complement = complement)
                )
        )
    }

    private fun setupObserver() {
        viewModel?.value?.observe(
            this,
            Observer {
                viewModel?.onChangeValue(it)
            }
        )
    }
}