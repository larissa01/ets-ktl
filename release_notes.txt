Versão 1.0.7  10/02/2020

Novo

Melhorias

Correções
rollback botao receber home
Invertido TED e P2P na tela de escolhas
Aab packing ok

————————————————————————————————————————————————————————————————————————————————————————————————————
Versão 1.0.3  10/02/2020

Novo

Melhorias

Correções
Dexguard ok

————————————————————————————————————————————————————————————————————————————————————————————————————
Versão 1.0.2  09/02/2020

Novo

Melhorias

Correções
Bugfixes extrato(PIX)
Bugfix botão receber(PIX)
————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.1  09/02/2020

Novo

Melhorias

Correções
Bugfix texto cartão pré pago
Bugfix Botao transferencias
Bugfix Comprovante PIX
————————————————————————————————————————————————————————————————————————————————————————————————————
Versão 1.0.0 (1.0.20-homol) 31/01/2020

Novo

Melhorias

Correções
Regra p2p opensbp false
Retirados 0 sobressalentes no header da home
Corrigida string meus rendimentos
Corrigido voltar comprovante vindo do extrato
Corrigido Franquias
Corrigido Taxa de saque

————————————————————————————————————————————————————————————————————————————————————————————————————
Versão 1.0.0 (1.0.19-homol) 30/01/2020

Novo

Melhorias
- Adicionado label agendamento TED
- Franquia add back-end

Correções
- Range limites ted
- Tela de transferencia PIX
- Retirado perda/roubo
- Mudança String alteração de senha


————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.18-homol) 29/01/2020

Novo
Franquias da conta

Melhorias

Correções
- Fix P2P errors
- Data futura extrato
- bugfixes faq
- Titulo qrCode
- Lembrar cpf
- Ordenação store
- Numero de telefone atendimento

————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.17-homol) 28/01/2020

Novo

Melhorias

Correções
- Adicionado pid para alteração de celular, email e renda
- Detalhes do perfil
- Botão "Novo Pagamento" em Recibo
- Layout tela login
- Permissão de agendamento de pagamento sem saldo
- Correção no botão voltar do cashback
- Correção no fluxo de reemissão de cartão
- Correção de textos no cashback da Veloe
- Alteração PID alteração cadastral

————————————————————————————————————————————————————————————————————————————————————————————————————
Versão 1.0.0 (1.0.15-homol) 27/01/2020

Novo

Melhorias

Correções
- Dúvida por assunto - Tópicos não estão centralizados com a aba
- Ajuste - não tem opção de voltar do App , apenas a opção nativa do Android
- Layout telas de ativação do cartão
- Mensagem quando há 0 boletos não está cabendo no campo
- Alteração cadastral
- Alteração Cadastral - Endereço
- Alteração cadastral - não há ação sistêmica.
- Meus cartões - Cartão Virtual - Ao clicar botão voltar está retornando para tela Home
- Ver senha do cartão não funcionando o voltar nativo do Android [Squad Confio]
- Informação de texto errado no reset de senha na tela de informar senha
- Massa sem selfie cadastrada na acesso para ver senha do cartão está trazendo tela de sair
- Após alterar a senha não está sendo possível logar com a mesma.
- Botão voltar nativo
- Card cashback disponível - Apresenta Indisponível ao invés do valor
- Logo Digio do contrato está fora de alinhamento e no término se possível retirar excesso de espaço em branco no término do contrato.
- Gerar Boleto - Campo descrição oculta a linha ao ser clicada
- Apresentando data não correspondente com o dia da semana
- Componentes não estão responsivos
- Ícone do Pagamento de Contas está errado.
- Não está direcionado | botão sem Ação
- Botão de alteração cadastral não funciona
- Botão do X está deslocado.
- Botões da home continuam clicáveis quando a parte preta do Header é ativada.
- Mudar a máscara para exibir apenas os dígitos existentes
- Corrigir tamanho da fonte dos botões "Minha Chave PIX" e "Alteração cadastral"
- Tempo de seção não está sendo de 15 min verificar com menos de 10min sessão encerra
- Ajudo - Atalho na Home
- Trocar o texto "até 100% CDI" por "100% CDI"
- Home da loja - Front End
- Primeiro acesso login - senha já vem preenchida
- Fontes erradas, não estão respeitando o padrão determinado no device
- Botão voltar nativo
- Ao filtrar Pets apresenta Ponto Frio
- Time de expiração de login muito rápido.
- Quebra de layout - não apresenta os botões <Sair> e <Voltar>.
- No menu do IOS quando faz o scroll, devido a cor do título ser da cor branca o texto: "Menu" não consegue ser visto quando faz o scroll.
- Fio do rastreio tem que ficar grudado na bolinha (não tem espaço)
- Alteração de Renda | crash ao tentar alterar renda.
- Rendimento - os valores estão divergentes.
- Voltar a home após fluxo
- Adicionar linha
- Não é possível seguir o fluxo com dados que vem da aquisição.
- Ao apresentar mensagem de erro, não retorna para o fluxo correspondente.
- Revisão traz campo fixo como CPF para o destinatário
- Função AGENDAR também está ficando riscada quando a janela do SPB está fechada
- Texto de cancelar agendamento está sendo exibido na tela de revisão mesmo quando não é agendamento
- Retirar ":" após o campo nome
- DADOS DA CONTA ou FAVORECIDOS não ficam com o sublinhado cinza quando não estão selecionados.
- Não está debitando valor ao fazer uma transferência agendada
- Transferência entre Uber conta
- Outros Bancos - Agendamento aparece com data riscada
- o valor não atualiza automaticamente.
- Transferência para Uber conta - Nova conta - Não está sendo possível inserir o número da agência.
- Botão voltar do dispositivo apaga dados da tela
- Mensagem de alerta de Agendamento pro próximo dia útil não é emitida
- Informações sobre bloqueio e desbloqueio
- Textos incoerentes
- Quebra do layout
- Dados Cadastrais - Divergência de comportamento dos botões
- Dados Cadastrais - Erros de ortografia
- Logo de parceiros de Cashback está sem o logo
- Quando não há compras realizadas deve ser exibida uma mensagem de erro

————————————————————————————————————————————————————————————————————————————————————————————————————
Versão 1.0.0 (1.0.14-homol) 22/01/2020

Novo

Melhorias

Correções
- [PIX] Redirecionar para Receber via PIX no Menu Receber;
- [PIX] Ícones das Chaves em Nova Chave / Pagar
- [PIX] ImeOptions da Tela de Inserir Valores no Recebimento
- Add icon scroll list home
- Fixed padding error on ReceiptViewModel
- add new host to acquisition

————————————————————————————————————————————————————————————————————————————————————————————————————
Versão 1.0.0 (1.0.13-homol) 21/01/2020

Novo

Melhorias
- Mudança do logo na tela Welcome (Confio)
- Agendamento do Pagamento (Recebo)
- Adicionado endereço de entrega para visualização (não editável) (Confio)

Correções
- Ajuste de texto na Veloe (Fico)
- Texto de sucesso na recarga algerado (Fico)

- Adicionado tagueamento nas telas de pagamento (Recebo)
- Nome Transferência errado (Recebo)

- Cor do botão no QRCode (Gosto)
- Cartão com rebarbas na home (Gosto)
- Caso não tenha foto do cliente, colocar as iniciais (Gosto)
- Alterar texto da tela de sucesso QRCode (Gosto)
- Removido bloquei temporario do menu (Gosto)
- Alterado cor do botão na tela de QRCode (Gosto)
- Ocultar opção de carteira virtual (Gosto)
- Area do click dos botões no header da home esta muito pequena (Gosto)

- Adicionado dialog de erro para o usuário tentar novamente no campo do PID caso de hash expirate (Confio)
- Adicionado tratativa de erro para hash expirate no PID (Confio)
- Removido verificação de senha, no campo da alteração de senha (Confio)
- PID Olho com ação invertida (Confio)
- Resolvido problema de mensagem errada no reconhecimento fácil do PID (Confio)
- Adicionado tag para abrir o chat na alteração de endereço (Confio)
- Adicionado evento nativo de voltar a tela anterior, na mudança de senha (Confio)
- Alterado a cor do Hint no campo de senha (Confio)
- Adicionado o underline verde do componente de senha, mesmo se o usuário não esta com ele em foco (Confio)

PS: Versão apontada para o ambiente de homologação

————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.12-homol) 18/01/2020

Novo

Melhorias
PIX - Pagamento manual
Filtro pesquisa cashback
Primeiro Tagueamento FAQ
Tag cartao recebido
Tier Home Cashback

Correções
Endpoint QRCode
Texto Sucesso na tela de recarga
Bugfix titulo Transferencia
Bugfix layout gosto

PS: Versão apontada para o ambiente de homologação

————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.11-homol) 14/01/2020

Novo

Melhorias
- Endpoint de logout
- Endpoint de refresh token
- Adicionado a nova licença e a ofuscação do Dexguard
- Adicionado botão para a FAQ Chat ao alterar o endereço de entrega

Correções
- Adicionado ofuscação para as credenciais
- Corrigido texto na feature Veloe
- Ajuste de imagem na tela do SAC


PS: Versão apontada para o ambiente de homologação

————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.10-homol) 12/01/2020

Novo
PIX

Melhorias
Mudado ícone da aplicação
Mostra de CEP
Adição complemento no endereço
Cashback tier
Novo ícone transferencia
Valor da tarifa boleto cash-in

Correções
Fix tela de cpf gemalto
Fix troca de endereço
Fix layout pagamento de contas
Fix layout cashback
Fix Cartões
Fonte FAQ
Mudado texto para recarga e veloe
Mudado texto para mostrar endereço

PS: Versão não ofuscada pelo Dexguard
PS2: Versão apontada para o ambiente de homologação

————————————————————————————————————————————————————————————————————————————————————————————————————
Versão 1.0.0 (1.0.9-homol) 05/01/2020

Novo

Melhorias

Correções
Bugfix layout pagamento de contas
Bugfix layout TED
Bugfix gps desativado saque
Bugfix msg erro biometria pid
Bugfix empty state meus pedidos loja
Bugfix msg erro login lexis


PS: Versão não ofuscada pelo Dexguard
PS2: Versão apontada para o ambiente de homologação
————————————————————————————————————————————————————————————————————————————————————————————————————
Versão 1.0.0 (1.0.8-homol) 31/12/2020

Novo

Melhorias
- Removido botão controle de gastos
- Aumentada área de toque Expandir Ocultar Home
- Swipe refresh Home
- Mensagem quando usuário é deslogado
- Adicionado certificado

Correções
- Fix layout extrato
- Fix layout TED
- Fix layout P2P
- Fix layout Comprovante
- Fix Layout PID
- Fix Layout CashBack
- Fix Layout QRCode Cielo
- Fix Layout Boleto cash-in
- Tela de escolhas direcionada em cima
- Corrigido texto Veloe
- Fix Cartoes recebidos

PS: Versão não ofuscada pelo Dexguard
PS2: Versão apontada para o ambiente de homologação

————————————————————————————————————————————————————————————————————————————————————————————————————
Versão 1.0.0 (1.0.7-homol) 29/12/2020

Novo
- Cashback
- Comprovante pagamento de contas
- Adicionado submódulo pix(desativado)

Melhorias
- Tagueamentp TED, P2P, Saque digital

Correções
- Fix PID
- Fix ícone sacar e receber na home
- Fix ícone saldo na home
- Fix layout boleto cash in
- Fix layout para pagamento de contas
- Fix layout rastreamento de cartão
- Fix layout comprovante
- Fix layout TED
- Fix layout FAQ
- Fix layout cartões
- Fix layout reconhecimento facial
- Fix layout veloe
- Fix tamanho da fonte nas toolbars
- Fix tamanho da fonte nos botões

PS: Versão não ofuscada pelo Dexguard
PS2: Versão apontada para o ambiente de homologação

————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.6-homol) 22/12/2020

Novo
- Mudanças cadastrais
- Store
- Rastreio de Cartão
- PID
- Saque digital
- Cashback primeiras implementações
- Reemissão de cartão
- Notification Push
- Remuneração
- Welcome Screen
- P2P
- Aquisição

Melhorias
- Faq na home
- Novo Loading para o Uber
- Firebase Tags
- Paginação no extrato

Correções
- Bugfix para a FAQ
- Bugfix desmocado cardId rastreio
- Bugfix botões da home

PS: Versão não ofuscada pelo Dexguard
PS2: Versão apontada para o ambiente de homologação

————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.5-homol) 07/12/2020

Novo
- Tela da store

Melhorias
- Adicionado o PID no QRCode

Correções
- Mensagens de erro default e por falta de internet
- Bugfix nas telas do Login

PS: Versão não ofuscada pelo Dexguard
PS2: Versão apontada para o ambiente de homologação

————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.4-homol) 03/12/2020

Novo
[mocado] Primeira tela saque
[mocado] Mostrar senha do cartão

Melhorias
Remoção do atalho de rastreio na home
Pid cartão virtual
App renomeado para Uber Conta

Correções
- Bugfix telas recebo
- Bugfix na FAQ

PS: Versão não ofuscada pelo Dexguard

————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.3-homol) 30/11/2020

Novo
- PID
- Tela leitor de código de barras
- Profile account na home
- Bloqueio temporário cartão físico
- Inclusão função pagar com QRcode no botão de atalho na home

Melhorias
- Mostrar cartão fisico na home
- Cache em algumas requisições
- Mostrando hint na busca da FAQ
- Recarga com PID

Correções
- Bugfix na FAQ

PS: Versão não ofuscada pelo Dexguard

————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.2-homol) 25/11/2020

Novo
- TED
- Extrato
- Comprovante
- Veloe
- Pagamento QR code cielo
- Menu
- Reset de senha

Melhorias
- Remoção mock FAQ
- Logoff
- Mudança de senha
- Recarga de celular
- Perfil da conta Home (Integração API)

Correções

————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.1-homol) 18/11/2020

Novo

Melhorias
- Adição do QR code

Correções

————————————————————————————————————————————————————————————————————————————————————————————————————

Versão 1.0.0 (1.0.0-homol) 17/11/2020

Novo
- TUDO

Melhorias

Correções

————————————————————————————————————————————————————————————————————————————————————————————————————