/* Apply Module base configurations */
plugins {
    id("com.android.library")
    kotlin("android")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.library")
    kotlin("kapt")
}

val releaseAccessToken: String by project
val releaseAppID: String by project
val releaseMid: String by project
val releaseEndPoint: String by project
val homologDevAccessToken: String by project
val homologDevAppID: String by project
val homologDevMid: String by project
val homologDevEndPoint: String by project
val senderId: String by project

configureAndroidLibrary(
    releaseBuildTypeConfig = {
        buildConfigField("String", "ACCESS_TOKEN", releaseAccessToken)
        buildConfigField("String", "APP_ID", releaseAppID)
        buildConfigField("String", "MID", releaseMid)
        buildConfigField("String", "APP_ENDPOINT", releaseEndPoint)
        buildConfigField("String", "SENDER_ID", senderId)
    },
    homolBuildTypeConfig = {
        buildConfigField("String", "ACCESS_TOKEN", homologDevAccessToken)
        buildConfigField("String", "APP_ID", homologDevAppID)
        buildConfigField("String", "MID", homologDevMid)
        buildConfigField("String", "APP_ENDPOINT", homologDevEndPoint)
        buildConfigField("String", "SENDER_ID", senderId)
    },
    debugBuildTypeConfig = {
        buildConfigField("String", "ACCESS_TOKEN", homologDevAccessToken)
        buildConfigField("String", "APP_ID", homologDevAppID)
        buildConfigField("String", "MID", homologDevMid)
        buildConfigField("String", "APP_ENDPOINT", homologDevEndPoint)
        buildConfigField("String", "SENDER_ID", senderId)
    }
)

dependencies {
    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.ANDROIDXCORE)
    implementation(Dependencies.ACTIVITYKTX)
    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.FRAGMENTKTX)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.KOIN)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)
    implementation(Dependencies.NAV_FRAGMENT)
    implementation(Dependencies.NAV_UI)
    implementation(Dependencies.SALESFORCE_MARKETING_CLOUD)
}