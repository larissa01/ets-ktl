package br.com.digio.uber.marketingCloud.config

import android.content.Context
import br.com.digio.uber.marketingCloud.BuildConfig
import br.com.digio.uber.marketingCloud.R
import com.salesforce.marketingcloud.MCLogListener
import com.salesforce.marketingcloud.MarketingCloudConfig
import com.salesforce.marketingcloud.MarketingCloudSdk
import com.salesforce.marketingcloud.notifications.NotificationCustomizationOptions
import timber.log.Timber

class MarketingCloudConfigurationImpl : MarketingCloudConfiguration {
    override fun initConfiguration(context: Context) {
        MarketingCloudSdk.setLogLevel(MCLogListener.VERBOSE)
        MarketingCloudSdk.setLogListener(MCLogListener.AndroidLogListener())

        MarketingCloudSdk.init(
            context,
            MarketingCloudConfig.builder().apply
            {
                setApplicationId(BuildConfig.APP_ID)
                setAccessToken(BuildConfig.ACCESS_TOKEN)
                setSenderId(BuildConfig.SENDER_ID)
                setMarketingCloudServerUrl(BuildConfig.APP_ENDPOINT)
                setMid(BuildConfig.MID)
                setNotificationCustomizationOptions(
                    NotificationCustomizationOptions.create(R.drawable.ic_launcher_marketing)
                )
            }.build(context)
        ) { status ->
            Timber.i(status.toString())
        }
    }

    override fun sendContactKey(cpf: String) {
        MarketingCloudSdk.requestSdk { sdk ->
            sdk.registrationManager.edit().apply {
                setContactKey(cpf)
                commit()
            }
        }
    }
}