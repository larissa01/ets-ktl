package br.com.digio.uber.marketingCloud.di

import br.com.digio.uber.marketingCloud.config.MarketingCloudConfiguration
import br.com.digio.uber.marketingCloud.config.MarketingCloudConfigurationImpl
import org.koin.dsl.module

val marketingCloudModule = module {
    single<MarketingCloudConfiguration> { MarketingCloudConfigurationImpl() }
}