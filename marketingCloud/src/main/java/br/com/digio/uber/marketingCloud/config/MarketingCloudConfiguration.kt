package br.com.digio.uber.marketingCloud.config

import android.content.Context

interface MarketingCloudConfiguration {
    fun initConfiguration(context: Context)
    fun sendContactKey(cpf: String)
}