plugins {
    id("com.android.dynamic-feature")
    kotlin("android")
    kotlin("kapt")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.dynamic.library")
}

val mockUserLogin: String by project
val mockUserPass: String by project

configureAndroidDynamicLibrary(
    releaseBuildTypeConfig = {
        buildConfigField("String", "MOCK_USER_LOGIN", "null")
        buildConfigField("String", "MOCK_USER_PASS", "null")
    },
    homolBuildTypeConfig = {
        buildConfigField("String", "MOCK_USER_LOGIN", "null")
        buildConfigField("String", "MOCK_USER_PASS", mockUserPass)
    },
    debugBuildTypeConfig = {
        buildConfigField("String", "MOCK_USER_LOGIN", mockUserLogin)
        buildConfigField("String", "MOCK_USER_PASS", mockUserPass)
    }
)

dependencies {
    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.ANDROIDXCORE)
    implementation(Dependencies.ACTIVITYKTX)
    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.FRAGMENTKTX)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.KOIN)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)

    implementation(project(":app"))
    implementation(project(":common"))
    implementation(project(":domain"))
    implementation(project(":model"))
    implementation(project(":util"))
}