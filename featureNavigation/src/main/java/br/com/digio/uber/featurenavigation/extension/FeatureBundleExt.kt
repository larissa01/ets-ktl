package br.com.digio.uber.featurenavigation.extension

import android.os.Bundle
import br.com.digio.uber.common.R
import br.com.digio.uber.common.choice.Choice
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.util.Const

fun Bundle.generateBundleToPid(): Bundle =
    apply {
        putString(Const.RequestOnResult.PID.KEY_PID_TYPE, PidType.USER_DATA_MANAGER.name)
    }

fun Bundle.generateBundleToPidFacialRecognition(): Bundle =
    apply {
        putString(
            Const.RequestOnResult.PID.KEY_PID_TYPE,
            PidType.SHOW_CARD_PASSWORD.name
        )
    }

fun Bundle.generateTransferBundle(): Bundle =
    apply {
        val choices = arrayListOf(
            Choice(
                R.drawable.ic_receive_with_pix,
                R.string.to_a_uber_account,
                R.string.to_a_uber_account_text,
                Const.Activities.TRANSFER_ACTIVITY
            ),
            Choice(
                R.drawable.ic_receive_with_pix,
                R.string.to_another_banks,
                R.string.to_another_banks_text,
                Const.Activities.TRANSFER_ACTIVITY
            )
        )
        putInt(
            Const.Extras.TOOLBAR_ID,
            br.com.digio.uber.common.R.string.feature_transfer_title
        )
        putParcelableArrayList(Const.Extras.CHOICES, choices)
    }

fun Bundle.generateReceiveBundle(): Bundle =
    apply {
        val choices = arrayListOf(
            Choice(
                R.drawable.ic_receive_with_pix,
                br.com.digio.uber.featurenavigation.R.string.receive_with_pix,
                br.com.digio.uber.featurenavigation.R.string.receive_with_pix_subtitle,
                Const.Activities.BANK_SLIP_ACTIVITY
            ),
            Choice(
                R.drawable.ic_generate_bankslip,
                br.com.digio.uber.featurenavigation.R.string.generate_bankslip,
                br.com.digio.uber.featurenavigation.R.string.generate_bankslip_subtitle,
                Const.Activities.BANK_SLIP_ACTIVITY
            ),
            Choice(
                R.drawable.ic_generated_bankslips,
                br.com.digio.uber.featurenavigation.R.string.generated_bankslips,
                br.com.digio.uber.featurenavigation.R.string.generated_bankslips_subtitle,
                Const.Activities.BANK_SLIP_GENERATED_ACTIVITY
            )
        )

        putInt(Const.Extras.TOOLBAR_ID, R.string.feature_receive)
        putParcelableArrayList(Const.Extras.CHOICES, choices)
    }

fun Bundle.generateReceiptBundle(): Bundle =
    apply {
        putString(Const.Extras.RECEIPT_ID, Const.Extras.RECEIPT_ID)
        putSerializable(Const.Extras.RECEIPT_ORIGIN, Const.Extras.RECEIPT_ORIGIN)
    }

fun Bundle.generateInvoicePlusChoiceBundle(): Bundle =
    apply {
        val choices = arrayListOf(
            Choice(
                R.drawable.ic_receive_with_pix,
                R.string.to_a_uber_account,
                R.string.to_a_uber_account_text,
                Const.Activities.TRANSFER_ACTIVITY
            ),
            Choice(
                R.drawable.ic_receive_with_pix,
                R.string.to_another_banks,
                R.string.to_another_banks_text,
                Const.Activities.TRANSFER_ACTIVITY
            )
        )
        putInt(Const.Extras.TOOLBAR_ID, R.string.feature_income)
        putParcelableArrayList(Const.Extras.CHOICES, choices)
    }

fun Bundle.generateCIP(): Bundle =
    apply {
        putString("arg_cpf", "11122233344")
        putString("arg_income", "10")
    }