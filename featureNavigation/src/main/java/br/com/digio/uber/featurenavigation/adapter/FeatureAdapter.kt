package br.com.digio.uber.featurenavigation.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.featurenavigation.R
import br.com.digio.uber.featurenavigation.adapter.viewholder.FeatureNavigationSeparatorViewHolder
import br.com.digio.uber.featurenavigation.adapter.viewholder.FeatureNavigationViewHolder
import br.com.digio.uber.featurenavigation.uimodel.FeatureNavigationItem
import br.com.digio.uber.featurenavigation.uimodel.FeatureNavigationItemUiModel
import br.com.digio.uber.featurenavigation.uimodel.FeatureSeparatorItem
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage
import java.lang.IndexOutOfBoundsException

typealias OnClickFeatureNavigationItem = (item: FeatureNavigationItemUiModel) -> Unit

class FeatureAdapter constructor(
    private var values: MutableList<FeatureNavigationItemUiModel> = mutableListOf(),
    private val listener: OnClickFeatureNavigationItem
) : BaseRecyclerViewAdapter<FeatureNavigationItemUiModel, FeatureAdapter.ViewTypesDataBindingFactoryImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl =
        ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): FeatureNavigationItemUiModel =
        values[position]

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<FeatureNavigationItemUiModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<FeatureNavigationItemUiModel>
    }

    override fun getItemCount(): Int =
        values.size

    override fun onBindViewHolder(
        holder: AbstractViewHolder<FeatureNavigationItemUiModel>,
        position: Int
    ) = holder.bind(values[holder.adapterPosition])

    class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<FeatureNavigationItemUiModel> {
        override fun type(model: FeatureNavigationItemUiModel): Int =
            when (model) {
                is FeatureNavigationItem -> R.layout.feature_navigation_item
                is FeatureSeparatorItem -> R.layout.feature_navigation_separator_item
            }

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<FeatureNavigationItemUiModel>
        ): AbstractViewHolder<*> =
            when (type) {
                R.layout.feature_navigation_item -> FeatureNavigationViewHolder(view, listener)
                R.layout.feature_navigation_separator_item -> FeatureNavigationSeparatorViewHolder(view)
                else -> throw IndexOutOfBoundsException("Invalid view type")
            }
    }

    override fun replaceItems(list: List<Any>) {
        addValues(list.safeHeritage())
    }

    private fun addValues(values: List<FeatureNavigationItemUiModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }
}