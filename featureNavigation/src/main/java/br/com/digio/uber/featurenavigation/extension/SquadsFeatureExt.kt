package br.com.digio.uber.featurenavigation.extension

import android.app.Activity
import android.os.Bundle
import br.com.digio.uber.featurenavigation.R
import br.com.digio.uber.featurenavigation.uimodel.FeatureNavigationItem
import br.com.digio.uber.featurenavigation.uimodel.FeatureNavigationItemUiModel
import br.com.digio.uber.featurenavigation.uimodel.FeatureSeparatorItem
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.util.Const

fun Activity.getMainActivityMangers(): Array<FeatureNavigationItemUiModel> =
    listOf(
        FeatureSeparatorItem(
            squadName = getString(R.string.squad_gosto_fico_ajudo_confio)
        ),
        FeatureNavigationItem(
            name = getString(R.string.main_activity_item_title),
            activity = Const.Activities.MAIN_ACTIVITY,
            logged = true
        )
    ).toTypedArray()

fun Activity.getSamplesFeatures(): Array<FeatureNavigationItemUiModel> =
    listOf(
        FeatureSeparatorItem(
            squadName = getString(R.string.all_squads_samples)
        ),
        FeatureNavigationItem(
            name = getString(R.string.digio_test_item_title),
            activity = Const.Activities.DIGIO_TEST_ACTIVITY
        ),
        FeatureNavigationItem(
            name = getString(R.string.sample_item_title),
            activity = Const.Activities.SAMPLE_ACTIVITY
        ),
        FeatureNavigationItem(
            name = getString(R.string.sample_navigation_item_title),
            activity = Const.Activities.SAMPLE_NAVIGATION_ACTIVITY
        ),
        FeatureNavigationItem(
            name = getString(R.string.customs_guides_title),
            activity = Const.Activities.GUIDE_CUSTOMS_ACTIVITY
        )
    ).toTypedArray()

fun Activity.getConfio(): Array<FeatureNavigationItemUiModel> =
    listOf(
        FeatureSeparatorItem(
            squadName = getString(R.string.squad_confio)
        ),
        FeatureNavigationItem(
            name = getString(R.string.login_title),
            activity = Const.Activities.LOGIN_ACTIVITY
        ),
        FeatureNavigationItem(
            name = getString(R.string.title_facialbiometry),
            activity = Const.Activities.FACIAL_BIOMETRY_ACTIVITY
        ),
        FeatureNavigationItem(
            name = getString(R.string.title_change_password),
            activity = Const.Activities.CHANGE_PASSWORD_ACTIVITY,
            logged = true
        ),
        FeatureNavigationItem(
            name = getString(R.string.pid),
            activity = Const.Activities.PID_ACTIVITY,
            bundle = Bundle().generateBundleToPid(),
            logged = true
        ),
        FeatureNavigationItem(
            name = getString(R.string.pid_facial_recognition),
            activity = Const.Activities.PID_ACTIVITY,
            bundle = Bundle().generateBundleToPidFacialRecognition(),
            logged = true
        ),
        FeatureNavigationItem(
            requirePidType = PidType.USER_DATA_MANAGER,
            name = getString(R.string.show_card_password),
            activity = Const.Activities.SHOW_CARD_PASSWORD_ACTIVITY,
            logged = true
        ),
        FeatureNavigationItem(
            requirePidType = PidType.USER_DATA_MANAGER,
            name = getString(R.string.change_registration),
            activity = Const.Activities.CHANGE_REGISTRATION_ACTIVITY,
            logged = true
        )
    ).toTypedArray()

fun Activity.getConfioAndAjudo(): Array<FeatureNavigationItemUiModel> =
    listOf(
        FeatureSeparatorItem(
            squadName = getString(R.string.squad_confio_ajudo)
        ),
        FeatureNavigationItem(
            name = getString(R.string.title_menu),
            activity = Const.Activities.MENU_ACTIVITY,
            logged = true
        )
    ).toTypedArray()

fun Activity.getGosto(): Array<FeatureNavigationItemUiModel> =
    listOf(
        FeatureSeparatorItem(
            squadName = getString(R.string.squad_gosto)
        ),
        FeatureNavigationItem(
            name = getString(R.string.virtual_card),
            activity = Const.Activities.VIRTUAL_CARD_ACTIVITY,
            logged = true
        ),
        FeatureNavigationItem(
            name = getString(R.string.home_temporary),
            activity = Const.Activities.HOME_TEMPORARY_ACTIVITY
        ),
        FeatureNavigationItem(
            name = getString(R.string.tracking),
            activity = Const.Activities.TRACKING_ACTIVITY,
            logged = true
        ),
        FeatureNavigationItem(
            name = getString(R.string.card_received),
            activity = Const.Activities.CARD_RECEIVED_ACTIVITY,
            logged = true
        )
    ).toTypedArray()

fun Activity.getRecebo(): Array<FeatureNavigationItemUiModel> =
    listOf(
        FeatureSeparatorItem(
            squadName = getString(R.string.squad_recebo)
        ),
        FeatureNavigationItem(
            name = getString(R.string.transfer_item_title),
            activity = Const.Activities.CHOICE_ACTIVITY,
            bundle = Bundle().generateTransferBundle(),
            logged = true
        ),
        FeatureNavigationItem(
            name = getString(R.string.receive),
            activity = Const.Activities.CHOICE_ACTIVITY,
            bundle = Bundle().generateReceiveBundle(),
            logged = true
        ),
        FeatureNavigationItem(
            name = getString(R.string.title_receipt),
            activity = Const.Activities.RECEIPT_ACTIVITY,
            bundle = Bundle().generateReceiptBundle()
        ),
        FeatureNavigationItem(
            name = getString(R.string.payment),
            activity = Const.Activities.PAYMENT_ACTIVITY,
            logged = true
        ),
        FeatureNavigationItem(
            name = getString(R.string.statement),
            activity = Const.Activities.STATEMENT_ACTIVITY
        ),
        FeatureNavigationItem(
            getString(R.string.income),
            Const.Activities.INCOME_ACTIVITY,
            logged = true
        ),
        FeatureNavigationItem(
            name = getString(R.string.income_plus_choice),
            activity = Const.Activities.CHOICE_ACTIVITY,
            bundle = Bundle().generateInvoicePlusChoiceBundle(),
            logged = true
        )
    ).toTypedArray()

fun Activity.getFico(): Array<FeatureNavigationItemUiModel> =
    listOf(
        FeatureSeparatorItem(
            squadName = getString(R.string.squad_fico)
        ),
        FeatureNavigationItem(
            name = getString(R.string.recharge),
            activity = Const.Activities.RECHARGE_ACTIVITY,
            logged = true
        ),
        FeatureNavigationItem(
            name = getString(R.string.veloe),
            activity = Const.Activities.VELOE_ACTIVITY,
            logged = true
        ),
        FeatureNavigationItem(
            name = getString(R.string.title_cashback),
            activity = Const.Activities.CASHBACK_ACTIVITY,
            logged = true
        )
    ).toTypedArray()

fun Activity.getAjudo(): Array<FeatureNavigationItemUiModel> =
    listOf(
        FeatureSeparatorItem(
            squadName = getString(R.string.squad_ajudo)
        ),
        FeatureNavigationItem(
            name = getString(R.string.faq),
            activity = Const.Activities.FAQ_ACTIVITY,
            logged = true
        )
    ).toTypedArray()

fun Activity.getCIP(): Array<FeatureNavigationItemUiModel> =
    listOf(
        FeatureSeparatorItem(
            squadName = getString(R.string.squad_cip)
        ),
        FeatureNavigationItem(
            name = getString(R.string.pix),
            activity = Const.Activities.PIX_MENU_ACTIVITY,
            logged = true,
            bundle = Bundle().generateCIP()
        )
    ).toTypedArray()