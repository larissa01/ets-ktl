package br.com.digio.uber.featurenavigation.adapter.viewholder

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.featurenavigation.BR
import br.com.digio.uber.featurenavigation.uimodel.FeatureNavigationItemUiModel

abstract class FeatureNavigationAbstractViewHolder<T : FeatureNavigationItemUiModel>(
    private val viewDataBinding: ViewDataBinding,
    private val listener: ViewTypesListener<FeatureNavigationItemUiModel>? = null
) : AbstractViewHolder<T>(viewDataBinding.root) {
    override fun bind(item: T) {
        viewDataBinding.setVariable(BR.featureNavigationItem, item)
        viewDataBinding.setVariable(BR.featureNavigationViewHolder, this)
        viewDataBinding.executePendingBindings()
    }

    open fun onClick(item: T) {
        listener?.invoke(item)
    }
}