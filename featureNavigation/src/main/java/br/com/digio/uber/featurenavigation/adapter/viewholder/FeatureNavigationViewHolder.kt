package br.com.digio.uber.featurenavigation.adapter.viewholder

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.featurenavigation.uimodel.FeatureNavigationItem
import br.com.digio.uber.featurenavigation.uimodel.FeatureNavigationItemUiModel

class FeatureNavigationViewHolder(
    viewDataBinding: ViewDataBinding,
    listener: ViewTypesListener<FeatureNavigationItemUiModel>
) : FeatureNavigationAbstractViewHolder<FeatureNavigationItem>(viewDataBinding, listener)