package br.com.digio.uber.featurenavigation.adapter.viewholder

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.featurenavigation.uimodel.FeatureSeparatorItem

class FeatureNavigationSeparatorViewHolder(
    viewDataBinding: ViewDataBinding,
) : FeatureNavigationAbstractViewHolder<FeatureSeparatorItem>(viewDataBinding)