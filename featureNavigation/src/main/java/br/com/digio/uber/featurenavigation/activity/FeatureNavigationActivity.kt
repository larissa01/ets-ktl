package br.com.digio.uber.featurenavigation.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.featurenavigation.BR
import br.com.digio.uber.featurenavigation.R
import br.com.digio.uber.featurenavigation.adapter.FeatureAdapter
import br.com.digio.uber.featurenavigation.databinding.FeatureNavigationActivityBinding
import br.com.digio.uber.featurenavigation.di.viewmodel.viewModelModule
import br.com.digio.uber.featurenavigation.extension.getAjudo
import br.com.digio.uber.featurenavigation.extension.getCIP
import br.com.digio.uber.featurenavigation.extension.getConfio
import br.com.digio.uber.featurenavigation.extension.getConfioAndAjudo
import br.com.digio.uber.featurenavigation.extension.getFico
import br.com.digio.uber.featurenavigation.extension.getGosto
import br.com.digio.uber.featurenavigation.extension.getMainActivityMangers
import br.com.digio.uber.featurenavigation.extension.getRecebo
import br.com.digio.uber.featurenavigation.extension.getSamplesFeatures
import br.com.digio.uber.featurenavigation.uimodel.FeatureNavigationItem
import br.com.digio.uber.featurenavigation.uimodel.FeatureNavigationItemUiModel
import br.com.digio.uber.featurenavigation.viewmodel.FeatureNavigationViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.safeLet
import org.koin.android.ext.android.inject
import org.koin.core.module.Module

class FeatureNavigationActivity :
    BaseViewModelActivity<FeatureNavigationActivityBinding, FeatureNavigationViewModel>() {

    override val bindingVariable: Int? = BR.viewModel

    override val viewModel: FeatureNavigationViewModel by inject()

    @Suppress("SpreadOperator")
    private val listFeatures: MutableList<FeatureNavigationItemUiModel> by lazy {
        mutableListOf(
            *getMainActivityMangers(),
            *getSamplesFeatures(),
            *getConfio(),
            *getConfioAndAjudo(),
            *getGosto(),
            *getRecebo(),
            *getFico(),
            *getAjudo(),
            *getCIP(),
        )
    }

    private val adapter: FeatureAdapter by lazy {
        FeatureAdapter(listFeatures) { featureUiModel ->
            if (featureUiModel is FeatureNavigationItem) {
                if (featureUiModel.logged) {
                    viewModel.featureSelected.value = featureUiModel
                    viewModel.login()
                } else {
                    navigateToFeature(featureUiModel)
                }
            }
        }
    }

    override fun getLayoutId(): Int? = R.layout.feature_navigation_activity

    override fun getModule(): List<Module>? = listOf(viewModelModule)

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        setupAdapter()
        viewModel.goToFeature.observe(
            this,
            Observer {
                if (it == true) {
                    viewModel.featureSelected.value?.let { featureSelectedLet ->
                        onLoginFinish(featureSelectedLet)
                    }
                    viewModel.goToFeature.value = false
                }
            }
        )
    }

    private fun onLoginFinish(
        featureSelectedLet: FeatureNavigationItem
    ) {
        if (featureSelectedLet.requirePidType != null) {
            navigation.navigationToPidActivity(
                this,
                featureSelectedLet.requirePidType
            )
        } else {
            navigateToFeature(featureSelectedLet)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Const.RequestOnResult.PID.PID_REQUEST_CODE -> handlePidRequest(resultCode, data)
        }
    }

    private fun handlePidRequest(resultCode: Int, data: Intent?) {
        when (resultCode) {
            Const.RequestOnResult.PID.RESULT_PID_SUCCESS ->
                safeLet(
                    viewModel.featureSelected.value,
                    data?.extras?.getString(Const.RequestOnResult.PID.KEY_PID_HASH)
                ) { featureSelected, pidHash ->
                    handlePidSuccess(featureSelected, pidHash)
                } ?: showToastLong(getString(R.string.pid_toast_error_value_null))
            else -> showToastLong(getString(R.string.pid_faild))
        }
    }

    private fun showToastLong(valueText: String) {
        Toast.makeText(
            this,
            valueText,
            Toast.LENGTH_LONG
        ).show()
    }

    private fun handlePidSuccess(featureSlected: FeatureNavigationItem, pidHash: String) {
        when (featureSlected.activity) {
            Const.Activities.SHOW_CARD_PASSWORD_ACTIVITY -> navigateToFeature(
                featureSlected.copy(
                    bundle = Bundle().apply {
                        putString(
                            Const.RequestOnResult.ShowCardPassword.KEY_SHOW_CARD_PASSWORD_HASH,
                            pidHash
                        )
                    }
                )
            )
            else -> navigateToFeature(featureSlected)
        }
    }

    private fun navigateToFeature(featureUiModel: FeatureNavigationItem) {
        navigation.navigationToDynamic(
            this,
            featureUiModel.activity,
            featureUiModel.bundle
        )
        viewModel.featureSelected.value = null
    }

    private fun setupAdapter() {
        binding?.root?.post {
            binding?.recyclerFeatures?.adapter = adapter
            binding?.recyclerFeatures?.layoutManager = LinearLayoutManager(this)
        }
    }
}