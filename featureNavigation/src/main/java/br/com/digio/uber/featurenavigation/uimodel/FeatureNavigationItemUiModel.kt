package br.com.digio.uber.featurenavigation.uimodel

import android.os.Bundle
import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.model.pid.PidType

sealed class FeatureNavigationItemUiModel : AdapterViewModel<FeatureNavigationItemUiModel> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<FeatureNavigationItemUiModel>) = typesFactory.type(
        model = this
    )
}

data class FeatureNavigationItem(
    val name: String,
    val activity: String,
    val bundle: Bundle? = null,
    val logged: Boolean = false,
    val requirePidType: PidType? = null
) : FeatureNavigationItemUiModel()

data class FeatureSeparatorItem(
    val squadName: String,
) : FeatureNavigationItemUiModel()