package br.com.digio.uber.featurenavigation.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.login.MakeLoginUseCase
import br.com.digio.uber.featurenavigation.BuildConfig
import br.com.digio.uber.featurenavigation.uimodel.FeatureNavigationItem
import br.com.digio.uber.model.login.repository.model.Login

class FeatureNavigationViewModel constructor(
    private val makeLogin: MakeLoginUseCase
) : BaseViewModel() {

    val goToFeature: MutableLiveData<Boolean> = MutableLiveData()
    var featureSelected: MutableLiveData<FeatureNavigationItem> = MutableLiveData()

    fun login() {
        makeLogin(
            Login(
                userName = BuildConfig.MOCK_USER_LOGIN,
                password = BuildConfig.MOCK_USER_PASS,
                pidInfo = null
            )
        ).singleExec(
            onSuccessBaseViewModel = {
                goToFeature.value = true
            }
        )
    }
}