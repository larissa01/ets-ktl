package br.com.digio.uber.featurenavigation.di.viewmodel

import br.com.digio.uber.featurenavigation.viewmodel.FeatureNavigationViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { FeatureNavigationViewModel(get()) }
}