package br.com.digio.uber.showcardpassword.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.card.password.GetCardPasswordUseCase

class ShowCardPasswordViewModel constructor(
    private val getCardPasswordUseCase: GetCardPasswordUseCase
) : BaseViewModel() {
    val password: MutableLiveData<String> = MutableLiveData()

    fun getPassword(hash: String) {
        getCardPasswordUseCase(hash).singleExec(
            onSuccessBaseViewModel = {
                password.value = it.cardPassword
            },
            onError = { _, error, _ ->
                error.apply {
                    close = true
                }
            }
        )
    }
}