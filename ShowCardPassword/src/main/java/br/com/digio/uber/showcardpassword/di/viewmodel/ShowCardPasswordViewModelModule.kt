package br.com.digio.uber.showcardpassword.di.viewmodel

import br.com.digio.uber.showcardpassword.viewmodel.ShowCardPasswordViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val showCardPasswordViewModelModule = module {
    viewModel { ShowCardPasswordViewModel(get()) }
}