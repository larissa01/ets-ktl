package br.com.digio.uber.showcardpassword.fragment

import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.showcardpassword.BR
import br.com.digio.uber.showcardpassword.R
import br.com.digio.uber.showcardpassword.databinding.ShowCardPasswordFragmentBinding
import br.com.digio.uber.showcardpassword.viewmodel.ShowCardPasswordViewModel
import org.koin.android.ext.android.inject

class ShowCardPasswordFragment :
    BaseViewModelFragment<ShowCardPasswordFragmentBinding, ShowCardPasswordViewModel>() {

    override val bindingVariable: Int? = BR.showCardPasswordViewModel
    override val getLayoutId: Int? = R.layout.show_card_password_fragment
    override val viewModel: ShowCardPasswordViewModel? by inject()

    private val analytics: Analytics by inject()

    private val hashPid: String by lazy {
        ShowCardPasswordFragmentArgs.fromBundle(checkNotNull(requireActivity().intent.extras))
            .keyShowCardPasswordHash
    }

    override fun initialize() {
        super.initialize()

        viewModel?.getPassword(hashPid)

        viewModel?.initializer {
            when (it.id) {
                R.id.btnNext -> {
                    analytics.pushSimpleEvent(Tags.SHOW_CARD_PASSWORD_OK)
                    activity?.finish()
                }
            }
        }
    }

    override fun tag(): String =
        ShowCardPasswordFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE
}