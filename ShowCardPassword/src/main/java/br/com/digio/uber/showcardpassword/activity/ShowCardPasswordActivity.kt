package br.com.digio.uber.showcardpassword.activity

import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.showcardpassword.R
import br.com.digio.uber.showcardpassword.di.viewmodel.showCardPasswordViewModelModule
import org.koin.core.module.Module

class ShowCardPasswordActivity : BaseActivity() {
    override fun getLayoutId(): Int? =
        R.layout.show_card_password_activity

    override fun getModule(): List<Module>? =
        listOf(showCardPasswordViewModelModule)
}