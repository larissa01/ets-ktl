package br.com.digio.uber.changepassword.uimodel

import androidx.annotation.StringRes
import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory

sealed class StepUiModel : AdapterViewModel<StepUiModel> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<StepUiModel>) = typesFactory.type(
        model = this
    )
}

data class StepItemUiModel(
    @StringRes
    val title: Int,
    val challenge: () -> Boolean
) : StepUiModel() {
    fun isChecked(): Boolean =
        challenge()
}