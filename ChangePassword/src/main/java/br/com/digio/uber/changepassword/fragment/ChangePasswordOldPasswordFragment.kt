package br.com.digio.uber.changepassword.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.changepassword.BR
import br.com.digio.uber.changepassword.R
import br.com.digio.uber.changepassword.databinding.ChangePasswordOldPasswordFragmentBinding
import br.com.digio.uber.changepassword.viewmodel.ChangePasswordOldPasswordViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.safeLet
import org.koin.android.ext.android.inject

class ChangePasswordOldPasswordFragment :
    BaseViewModelFragment<ChangePasswordOldPasswordFragmentBinding, ChangePasswordOldPasswordViewModel>() {

    override val bindingVariable: Int? = BR.changePasswordOldPasswordViewModel

    override val getLayoutId: Int? = R.layout.change_password_old_password_fragment

    override val viewModel: ChangePasswordOldPasswordViewModel? by inject()

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePassword?.toolbarChangePassword?.apply {
            setTitle(R.string.change_password)
        }

    private fun simpleDirection() =
        safeLet(
            viewModel?.password?.value,
            viewModel?.credential?.value
        ) { oldPassword, credential ->
            ChangePasswordOldPasswordFragmentDirections
                .actionChangePasswordFirstLoginFragmentToChangePasswordCreatePasswordFragment(
                    oldPassword,
                    credential.cpf
                ).apply {
                    isInitStarted = false
                }
        }

    override fun tag(): String =
        ChangePasswordOldPasswordFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE

    override fun initialize() {
        super.initialize()
        viewModel?.initializer {
            when (it.id) {
                R.id.btnNext -> simpleDirection()?.let { directionLet ->
                    navigateTo(directionLet)
                }
            }
        }
        viewModel?.password?.observe(
            this,
            Observer {
                viewModel?.onTextChanged(it)
            }
        )
        viewModel?.credential?.observe(this, Observer {})
    }

    override fun onBackPressed() {
        activity?.finish()
    }

    override fun onNavigationClick(view: View) {
        activity?.finish()
    }

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true
}