package br.com.digio.uber.changepassword.activity

import android.os.Bundle
import br.com.digio.uber.changepassword.NavHostChangePasswordDirections
import br.com.digio.uber.changepassword.R
import br.com.digio.uber.changepassword.di.changePasswordViewModelModule
import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.CREATE_PASSWORD_SCREEN
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.FIRST_LOGIN_SCREEN
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.KEY_DOCUMENT
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.KEY_OLD_PASSWORD
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.KEY_SCREEN
import br.com.digio.uber.util.Const.RequestOnResult.ChangePassword.OLD_PASSWORD_SCREEN
import org.koin.core.module.Module

class ChangePasswordActivity : BaseActivity() {
    override fun getLayoutId(): Int? =
        R.layout.change_password_activity

    private val screen: String? by lazy {
        intent.extras?.getString(KEY_SCREEN, null)
    }

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)

        when (screen) {
            FIRST_LOGIN_SCREEN -> {
                val oldPassword = checkNotNull(intent.extras?.getString(KEY_OLD_PASSWORD))
                val document = checkNotNull(intent.extras?.getString(KEY_DOCUMENT))
                navigateTo(
                    R.id.nav_host_change_password,
                    NavHostChangePasswordDirections.firstLogin(oldPassword, document)
                )
            }
            CREATE_PASSWORD_SCREEN -> {
                val oldPassword = checkNotNull(intent.extras?.getString(KEY_OLD_PASSWORD))
                val document = checkNotNull(intent.extras?.getString(KEY_DOCUMENT))
                navigateTo(
                    R.id.nav_host_change_password,
                    NavHostChangePasswordDirections.createPassword(oldPassword, document)
                        .apply {
                            isInitStarted = true
                        }
                )
            }
            OLD_PASSWORD_SCREEN -> navigateTo(
                R.id.nav_host_change_password,
                NavHostChangePasswordDirections.oldPassword()
            )
            else -> navigateTo(
                R.id.nav_host_change_password,
                NavHostChangePasswordDirections.oldPassword()
            )
        }
    }

    override fun getModule(): List<Module>? =
        listOf(changePasswordViewModelModule)
}