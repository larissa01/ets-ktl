package br.com.digio.uber.changepassword.di

import br.com.digio.uber.changepassword.viewmodel.ChangePasswordCreatePasswordViewModel
import br.com.digio.uber.changepassword.viewmodel.ChangePasswordFirstLoginViewModel
import br.com.digio.uber.changepassword.viewmodel.ChangePasswordOldPasswordViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val changePasswordViewModelModule = module {
    viewModel { ChangePasswordFirstLoginViewModel() }
    viewModel { ChangePasswordCreatePasswordViewModel(get(), get(), get()) }
    viewModel { ChangePasswordOldPasswordViewModel(get()) }
}