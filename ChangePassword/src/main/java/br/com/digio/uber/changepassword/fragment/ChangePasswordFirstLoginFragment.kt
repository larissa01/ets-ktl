package br.com.digio.uber.changepassword.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.changepassword.BR
import br.com.digio.uber.changepassword.R
import br.com.digio.uber.changepassword.databinding.ChangePasswordFirstLoginFragmentBinding
import br.com.digio.uber.changepassword.viewmodel.ChangePasswordFirstLoginViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.kenum.StatusBarColor
import org.koin.android.ext.android.inject

class ChangePasswordFirstLoginFragment :
    BaseViewModelFragment<ChangePasswordFirstLoginFragmentBinding, ChangePasswordFirstLoginViewModel>() {

    override val bindingVariable: Int? = BR.changePasswordFirstLoginViewModel

    override val getLayoutId: Int? = R.layout.change_password_first_login_fragment

    override val viewModel: ChangePasswordFirstLoginViewModel? by inject()

    private val analytics: Analytics by inject()

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePassword?.toolbarChangePassword

    private val oldPassword: String by lazy {
        ChangePasswordFirstLoginFragmentArgs.fromBundle(requireArguments()).oldPassword
    }

    private val document: String by lazy {
        ChangePasswordFirstLoginFragmentArgs.fromBundle(requireArguments()).document
    }

    private val simpleDirection by lazy {
        ChangePasswordFirstLoginFragmentDirections
            .actionChangePasswordFirstLoginFragmentToChangePasswordCreatePasswordFragment(
                oldPassword,
                document
            ).apply {
                isInitStarted = true
            }
    }

    override fun tag(): String =
        ChangePasswordFirstLoginFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE

    override fun initialize() {
        super.initialize()
        viewModel?.initializer {
            when (it.id) {
                R.id.btnNext -> navigateToCreatePassword()
            }
        }
    }

    private fun navigateToCreatePassword() {
        analytics.pushSimpleEvent(Tags.LOGIN_CREATE_PASSWORD)
        navigateTo(
            simpleDirection
        )
    }

    override fun onNavigationClick(view: View) {
        activity?.finish()
    }

    override fun onBackPressed() {
        activity?.finish()
    }

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true
}