package br.com.digio.uber.changepassword.adapter.viewholder

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.changepassword.BR
import br.com.digio.uber.changepassword.uimodel.StepUiModel
import br.com.digio.uber.common.base.adapter.AbstractViewHolder

class StepViewHolder(
    private val viewDataBinding: ViewDataBinding
) : AbstractViewHolder<StepUiModel>(viewDataBinding.root) {
    override fun bind(item: StepUiModel) {
        viewDataBinding.setVariable(BR.stepItem, item)
        viewDataBinding.setVariable(BR.stepViewHolder, this)
        viewDataBinding.executePendingBindings()
    }
}