package br.com.digio.uber.changepassword.fragment

import android.app.Activity.RESULT_OK
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.uber.changepassword.BR
import br.com.digio.uber.changepassword.R
import br.com.digio.uber.changepassword.adapter.StepsAdapter
import br.com.digio.uber.changepassword.databinding.ChangePasswordCreatePasswordFragmentBinding
import br.com.digio.uber.changepassword.viewmodel.ChangePasswordCreatePasswordViewModel
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.extensions.showKeyboard
import br.com.digio.uber.common.kenum.StatusBarColor
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject

class ChangePasswordCreatePasswordFragment :
    BaseViewModelFragment<ChangePasswordCreatePasswordFragmentBinding, ChangePasswordCreatePasswordViewModel>() {

    override val bindingVariable: Int? = BR.changePasswordCreatePasswordViewModel

    override val getLayoutId: Int? = R.layout.change_password_create_password_fragment

    override val viewModel: ChangePasswordCreatePasswordViewModel? by inject()

    private val isInitStarted: Boolean by lazy {
        ChangePasswordCreatePasswordFragmentArgs.fromBundle(requireArguments()).isInitStarted
    }

    private val oldPassword: String by lazy {
        ChangePasswordCreatePasswordFragmentArgs.fromBundle(requireArguments()).oldPassword
    }

    private val document: String by lazy {
        ChangePasswordCreatePasswordFragmentArgs.fromBundle(requireArguments()).document
    }

    override fun getToolbar(): Toolbar? =
        binding?.appBarChangePassword?.toolbarChangePassword?.apply {
            if (!isInitStarted) {
                setTitle(R.string.change_password)
            }
        }

    override fun tag(): String =
        ChangePasswordCreatePasswordFragment::class.java.name

    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE

    override fun initialize() {
        super.initialize()
        viewModel?.initializer(oldPassword, document, isInitStarted) {
        }
        setupObservers(setupAdapter())
    }

    private fun setupAdapter(): StepsAdapter =
        StepsAdapter().apply {
            binding?.recyConditions?.adapter = this
            binding?.recyConditions?.layoutManager = LinearLayoutManager(context)
        }

    private fun setupObservers(adapter: StepsAdapter) {
        viewModel?.goToAnimation?.observe(
            this,
            Observer {
                changeKeyboardFocus(it)
            }
        )

        viewModel?.newPassword?.observe(
            this,
            Observer {
                adapter.notifyDataSetChanged()
                viewModel?.onChangeNewPassword()
            }
        )

        viewModel?.confirmationPassword?.observe(
            this,
            Observer {
                viewModel?.onConfirmationPassword(it)
            }
        )

        viewModel?.onSuccessInChangePassword?.observe(
            this,
            Observer {
                if (it == true) {
                    activity?.setResult(RESULT_OK)
                    activity?.finish()
                }
            }
        )

        binding?.edtNewPassword?.setOnEditorActionListener { _, keyCode, _ ->
            if (keyCode == EditorInfo.IME_ACTION_NEXT && viewModel?.goToAnimation?.value == true) {
                binding?.edtConfirmationPassword?.showKeyboard()
                binding?.nestedScrollChangePassword?.fullScroll(View.FOCUS_DOWN)
                false
            } else {
                true
            }
        }
    }

    private fun changeKeyboardFocus(it: Boolean?) {
        if (it == false) {
            launch {
                withContext(Default) {
                    delay(DELAY_OF_ANIMATION)
                }
                binding?.edtNewPassword?.showKeyboard()
            }
        }
    }

    override fun onNavigationClick(view: View) {
        if (isInitStarted) {
            activity?.finish()
        } else {
            pop()
        }
    }

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp: Boolean = true

    companion object {
        private const val DELAY_OF_ANIMATION = 100L
    }
}