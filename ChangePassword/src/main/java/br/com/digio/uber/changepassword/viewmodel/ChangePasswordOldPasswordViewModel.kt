package br.com.digio.uber.changepassword.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.changepassword.mapper.CredentialMapper
import br.com.digio.uber.changepassword.uimodel.CredentialModelUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.domain.usecase.account.GetCredentialsUseCase

class ChangePasswordOldPasswordViewModel constructor(
    getCredentialsUseCase: GetCredentialsUseCase
) : BaseViewModel() {
    val passwordError: MutableLiveData<Int> = MutableLiveData()
    val password: MutableLiveData<String> = MutableLiveData()
    val enableButton: MutableLiveData<Boolean> = MutableLiveData()

    val credential: LiveData<CredentialModelUiModel> = getCredentialsUseCase(Unit).exec(
        showLoadingFlag = false,
        onError = { _, _, _ -> EmptyErrorObject }
    ).map(CredentialMapper())

    fun onTextChanged(password: String?) {
        enableButton.value = isValidatedInputData(password)
    }

    private fun isValidatedInputData(value: String?): Boolean =
        if ((value?.length ?: 0) >= MAX_LIMIT_PASSWORD) {
            isValidatedPassword(value)
        } else {
            passwordError.value = null
            false
        }

    private fun isValidatedPassword(value: String?): Boolean {
        return when {
            value.isNullOrEmpty() ||
                value.length < MAX_LIMIT_PASSWORD ||
                value != credential.value?.passsword -> {
                passwordError.value = br.com.digio.uber.common.R.string.please_enter_a_valid_password
                false
            }
            else -> {
                passwordError.value = null
                true
            }
        }
    }

    companion object {
        private const val MAX_LIMIT_PASSWORD = 6
    }
}