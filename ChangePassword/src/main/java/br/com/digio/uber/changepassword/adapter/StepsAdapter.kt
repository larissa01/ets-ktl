package br.com.digio.uber.changepassword.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.changepassword.R
import br.com.digio.uber.changepassword.adapter.viewholder.StepViewHolder
import br.com.digio.uber.changepassword.uimodel.StepItemUiModel
import br.com.digio.uber.changepassword.uimodel.StepUiModel
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

class StepsAdapter constructor(
    private var values: MutableList<StepUiModel> = mutableListOf()
) : BaseRecyclerViewAdapter<StepUiModel, StepsAdapter.ViewTypesDataBindingFactoryImpl>() {

    class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<StepUiModel> {
        override fun type(model: StepUiModel): Int =
            when (model) {
                is StepItemUiModel -> R.layout.step_item
            }

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<StepUiModel>
        ): AbstractViewHolder<*> =
            when (type) {
                R.layout.step_item -> StepViewHolder(view)
                else -> throw IndexOutOfBoundsException("Invalid view type")
            }
    }

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl =
        ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): StepUiModel =
        values[position]

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<StepUiModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = {}
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<StepUiModel>
    }

    override fun getItemCount(): Int =
        values.size

    override fun onBindViewHolder(holder: AbstractViewHolder<StepUiModel>, position: Int) =
        holder.bind(values[holder.adapterPosition])

    override fun replaceItems(list: List<Any>) {
        addValues(list.safeHeritage())
    }

    private fun addValues(values: List<StepUiModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }
}