package br.com.digio.uber.changepassword.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.changepassword.R
import br.com.digio.uber.changepassword.uimodel.StepItemUiModel
import br.com.digio.uber.changepassword.uimodel.StepUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.makeMessageSuccessObject
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.login.ChangePasswordUseCase
import br.com.digio.uber.domain.usecase.menu.ChangePasswordAuthUseCase
import br.com.digio.uber.model.change.password.ChangePasswordAuthReq
import br.com.digio.uber.model.change.password.ChangePasswordReq
import br.com.digio.uber.util.safeHeritage

class ChangePasswordCreatePasswordViewModel(
    private val changePasswordUseCase: ChangePasswordUseCase,
    private val changePasswordAuthUseCase: ChangePasswordAuthUseCase,
    private val analytics: Analytics
) : BaseViewModel() {

    val enableButton: MutableLiveData<Boolean> = MutableLiveData()
    val newPassword: MutableLiveData<String> = MutableLiveData()
    val confirmationPassword: MutableLiveData<String> = MutableLiveData()
    val goToAnimation: MutableLiveData<Boolean> = MutableLiveData()
    val passwordError: MutableLiveData<Int> = MutableLiveData()
    private val oldPassword: MutableLiveData<String> = MutableLiveData()
    val onSuccessInChangePassword: MutableLiveData<Boolean> = MutableLiveData()

    val steps = MutableLiveData<List<StepUiModel>>().apply {
        value = listOf(
            StepItemUiModel(
                title = R.string.six_numbers,
                challenge = {
                    (newPassword.value?.length ?: 0) >= MAX_LENGTH_PASSWORD
                }
            )
        )
    }

    val isInitStarted: MutableLiveData<Boolean> = MutableLiveData()

    fun initializer(
        oldPassword: String,
        document: String,
        isInitStarted: Boolean,
        onClickItem: OnClickItem
    ) {
        this.oldPassword.value = oldPassword
        this.isInitStarted.value = isInitStarted
        super.initializer {
            when (it.id) {
                R.id.btnNext -> makeChangePassword(oldPassword, document)
                else -> onClickItem(it)
            }
        }
    }

    private fun makeChangePassword(oldPassword: String, document: String) {
        confirmationPassword.value?.let { newPassword ->
            if (isInitStarted.value == true) {
                changePassword(document, oldPassword, newPassword)
            } else {
                changePasswordAuth(oldPassword, newPassword)
            }
        }
    }

    private fun changePassword(
        document: String,
        oldPassword: String,
        newPassword: String
    ) {
        changePasswordUseCase(
            ChangePasswordReq(
                document = document,
                oldPassword = oldPassword,
                newPassword = newPassword
            )
        ).singleExec(
            onError = { _, error, _ ->
                error.apply {
                    title = R.string.error_title
                    message = R.string.error_message
                    buttonText = R.string.error_button_text
                    onPositiveButtonClick = {
                        changePassword(document, oldPassword, newPassword)
                    }
                }
            },
            onSuccessBaseViewModel = {
                analytics.pushSimpleEvent(Tags.LOGIN_CHANGE_PASSWORD_SUCCESS)
                message.value = makeMessageSuccessObject {
                    title = R.string.success_title
                    message = R.string.success_message
                    buttonText = R.string.success_button_text
                    onCloseDialog = {
                        onSuccessInChangePassword.value = true
                    }
                }
            }
        )
    }

    private fun changePasswordAuth(
        oldPassword: String,
        newPassword: String
    ) {
        changePasswordAuthUseCase(
            ChangePasswordAuthReq(
                oldPassword = oldPassword,
                newPassword = newPassword
            )
        ).singleExec(
            onError = { _, error, _ ->
                error.apply {
                    title = R.string.error_title
                    message = R.string.error_message
                    buttonText = R.string.error_button_text
                    onPositiveButtonClick = {
                        changePasswordAuth(oldPassword, newPassword)
                    }
                }
            },
            onSuccessBaseViewModel = {
                analytics.pushSimpleEvent(Tags.LOGIN_CHANGE_PASSWORD_SUCCESS)
                message.value = makeMessageSuccessObject {
                    title = R.string.success_title_auth
                    message = R.string.success_message_auth
                    buttonText = br.com.digio.uber.common.R.string.Ok
                    onCloseDialog = {
                        onSuccessInChangePassword.value = true
                    }
                }
            }
        )
    }

    fun onChangeNewPassword() {
        val stepsChecked = steps.value?.safeHeritage<StepItemUiModel>()?.all { it.isChecked() }
        goToAnimation.value = stepsChecked
        enableButton.value = stepsChecked == true &&
            isValidatedPassword(newPassword.value, confirmationPassword.value)
    }

    fun onConfirmationPassword(password: String?) {
        enableButton.value = isValidatedInputData(password)
    }

    private fun isValidatedInputData(confirmationPassword: String?): Boolean {
        return if (confirmationPassword?.isNotEmpty() == true) {
            isValidatedPassword(newPassword.value, confirmationPassword)
        } else {
            passwordError.value = null
            false
        }
    }

    private fun isValidatedPassword(newPassword: String?, confirmationPassword: String?): Boolean =
        if (validateTwoPassword(confirmationPassword, newPassword)) {
            passwordError.value = null
            true
        } else {
            if (!confirmationPassword.isNullOrEmpty()) {
                analytics.pushSimpleEvent(
                    Tags.LOGIN_ERROS,
                    Tags.PARAMETER_CREATE_PASSWORD to "senha_incorreta"
                )
                passwordError.value = R.string.is_password_have_identical
            } else {
                passwordError.value = null
            }
            false
        }

    private fun validateTwoPassword(
        confirmationPassword: String?,
        newPassword: String?
    ) = confirmationPassword == newPassword

    companion object {
        const val MAX_LENGTH_PASSWORD = 6
    }
}