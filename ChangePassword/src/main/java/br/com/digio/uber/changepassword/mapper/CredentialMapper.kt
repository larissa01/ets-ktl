package br.com.digio.uber.changepassword.mapper

import br.com.digio.uber.changepassword.uimodel.CredentialModelUiModel
import br.com.digio.uber.model.common.CredentialModel
import br.com.digio.uber.util.encrypt.decrypt
import br.com.digio.uber.util.mapper.AbstractMapper

class CredentialMapper : AbstractMapper<CredentialModel, CredentialModelUiModel> {
    override fun map(param: CredentialModel): CredentialModelUiModel =
        CredentialModelUiModel(
            cpf = param.cpfCrypted.decrypt(),
            passsword = param.password.decrypt()
        )
}