package br.com.digio.uber.bankslip.ui.viewmodel

import android.text.Spannable
import android.text.SpannableStringBuilder
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.bankslip.mapper.GenerateBankSlipMapper
import br.com.digio.uber.bankslip.uiModel.GenerateBankSlipUiModel
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.bankSlip.GenerateBankSlipUseCase
import br.com.digio.uber.domain.usecase.bankSlip.GetBankSlipsAvailableUseCase
import br.com.digio.uber.model.bankSlip.BankSlipRequest
import br.com.digio.uber.model.bankSlip.BankSlipResponse
import br.com.digio.uber.util.isInRange
import br.com.digio.uber.util.safeLet
import br.com.digio.uber.util.unmaskMoney
import java.math.BigDecimal

class GenerateBankSlipViewModel constructor(
    val generateBankSlipUseCase: GenerateBankSlipUseCase,
    getBankSlipsAvailableUseCase: GetBankSlipsAvailableUseCase,
    private val analytics: Analytics,
    resourceManager: ResourceManager
) : BaseViewModel() {

    companion object {
        private const val DEFAULT_BANKSLIP_VALUE = "0,00"
    }

    private val _bankSlipValueErrorEnabled = MutableLiveData<Boolean>()
    private val _bankSlipValueErrorText = MutableLiveData<Spannable>()
    private val _buttonGenerateEnabled = MutableLiveData<Boolean>()
    val onSuccessGenerate = MutableLiveData<BankSlipResponse?>().apply { value = null }
    val bankSlipDescription = MutableLiveData<String>()
    val bankSlipValue = MutableLiveData<String>().apply { value = DEFAULT_BANKSLIP_VALUE }
    val bankSlipValueErrorText: LiveData<Spannable> get() = _bankSlipValueErrorText
    val buttonGenerateEnabled: LiveData<Boolean> get() = _buttonGenerateEnabled

    val generateBankSlipUiModel: LiveData<GenerateBankSlipUiModel> = getBankSlipsAvailableUseCase(Unit).exec(
        showLoadingFlag = true
    ).map(GenerateBankSlipMapper(resourceManager))

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)
    }

    fun validateFields() {
        val hasBankSlipsAvailable: Boolean = generateBankSlipUiModel.value?.hasBankSlipsAvailable == true
        val bankSlipValue: BigDecimal? = bankSlipValue.value?.unmaskMoney()
        val bankSlipDescription: String? = bankSlipDescription.value

        val minimumValue = generateBankSlipUiModel.value?.bankSlipMinimumValue
        val maximumValue = generateBankSlipUiModel.value?.bankSlipMaximumValue

        val isBankSlipValueValid = bankSlipValue?.isInRange(minimumValue, maximumValue) == true

        if (isBankSlipValueValid) {
            _bankSlipValueErrorText.value = SpannableStringBuilder()
        } else {
            _bankSlipValueErrorText.value = generateBankSlipUiModel.value?.bankSlipValueErrorText
        }
        _bankSlipValueErrorEnabled.value = !isBankSlipValueValid

        _buttonGenerateEnabled.value = hasBankSlipsAvailable &&
            isBankSlipValueValid &&
            !bankSlipDescription.isNullOrBlank()
    }

    fun onGenerateClick() {
        analytics.pushSimpleEvent(Tags.BANKSLIP_GENERATE)
        safeLet(bankSlipValue.value?.unmaskMoney(), bankSlipDescription.value) { valueLet, descriptionLet ->
            generateBankSlipUseCase(BankSlipRequest(valueLet, descriptionLet)).singleExec(
                onSuccessBaseViewModel = {
                    onSuccessGenerate.value = it
                }
            )
        }
    }

    fun onClearBankSlipValue() {
        bankSlipValue.value = DEFAULT_BANKSLIP_VALUE
    }
}