package br.com.digio.uber.bankslip.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.bankslip.ui.fragment.GeneratedBankSlipFragment
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName

class BankSlipGeneratedNavigation(activity: AppCompatActivity) : GenericNavigationWithFlow(activity) {

    fun init(bundle: Bundle?) {
        goTo(GoTo(BankSlipOpKey.BANK_SLIP_GENERATED.name, bundle))
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            BankSlipOpKey.BANK_SLIP_GENERATED.name to showBankSlipGenerated()
        )

    private fun showBankSlipGenerated(): GenericNavigationWIthFlowGetFragmentByName = {
        GeneratedBankSlipFragment.newInstance(this, it)
    }
}