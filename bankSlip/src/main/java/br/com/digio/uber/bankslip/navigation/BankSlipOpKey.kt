package br.com.digio.uber.bankslip.navigation

import br.com.digio.uber.common.kenum.StatusBarColor

/**
 * @author Marlon D. Rocha
 * @since 14/10/20
 */
enum class BankSlipOpKey(val theme: StatusBarColor) {
    BANK_SLIP(StatusBarColor.BLACK),
    BANK_SLIP_SUCCESS(StatusBarColor.WHITE),
    BANK_SLIP_GENERATED(StatusBarColor.BLACK)
}