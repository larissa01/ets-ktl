package br.com.digio.uber.bankslip.uiModel

import android.os.Parcelable
import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import kotlinx.android.parcel.Parcelize

sealed class GeneratedBankSlipUiModel : AdapterViewModel<GeneratedBankSlipUiModel> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<GeneratedBankSlipUiModel>) = typesFactory.type(
        model = this
    )
}

@Parcelize
data class GeneratedBankSlipItemUi(
    val bankSlipUuid: String,
    val bankSlipNumberText: String,
    val bankSlipDescription: String,
    val bankSlipValue: String,
    val bankSlipDueDate: String,
) : Parcelable, GeneratedBankSlipUiModel()