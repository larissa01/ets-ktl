package br.com.digio.uber.bankslip.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.bankslip.ui.fragment.GenerateBankSlipFragment
import br.com.digio.uber.bankslip.ui.fragment.GenerateBankSlipSuccessFragment
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName

/**
 * @author Marlon D. Rocha
 * @since 14/10/20
 */
class BankSlipNavigation(activity: AppCompatActivity) : GenericNavigationWithFlow(activity) {

    fun init(bundle: Bundle?) {
        goTo(GoTo(BankSlipOpKey.BANK_SLIP.name, bundle))
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            BankSlipOpKey.BANK_SLIP.name to showBankSlip(),
            BankSlipOpKey.BANK_SLIP_SUCCESS.name to showBankSlipSuccess()
        )

    private fun showBankSlip(): GenericNavigationWIthFlowGetFragmentByName = {
        GenerateBankSlipFragment.newInstance(this, it)
    }

    private fun showBankSlipSuccess(): GenericNavigationWIthFlowGetFragmentByName = {
        GenerateBankSlipSuccessFragment.newInstance(this, it)
    }
}