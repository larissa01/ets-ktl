@file:Suppress("TooManyFunctions")

package br.com.digio.uber.bankslip.ui.fragment

import android.Manifest
import android.content.Intent
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.FileProvider
import br.com.digio.uber.bankslip.BR
import br.com.digio.uber.bankslip.BuildConfig
import br.com.digio.uber.bankslip.R
import br.com.digio.uber.bankslip.databinding.FragmentBankslipSuccessBinding
import br.com.digio.uber.bankslip.navigation.BankSlipOpKey
import br.com.digio.uber.bankslip.ui.fragment.GenerateBankSlipFragment.Companion.BANKSLIP_DESCRIPTION
import br.com.digio.uber.bankslip.ui.fragment.GenerateBankSlipFragment.Companion.BANKSLIP_ID
import br.com.digio.uber.bankslip.ui.fragment.GenerateBankSlipFragment.Companion.BANKSLIP_NUMBER
import br.com.digio.uber.bankslip.ui.viewmodel.GenerateBankSlipSuccessViewModel
import br.com.digio.uber.common.extensions.openDeviceSettings
import br.com.digio.uber.common.extensions.toast
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.bankSlip.BankSlipAction
import br.com.digio.uber.util.Const.Extras.DOT_PROVIDER
import br.com.digio.uber.util.Const.MimeTypes.PDF_MIME_TYPE
import br.com.digio.uber.util.setInClipboard
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions
import java.io.File

@RuntimePermissions
class GenerateBankSlipSuccessFragment : BaseBankSlipFragment<FragmentBankslipSuccessBinding,
    GenerateBankSlipSuccessViewModel>() {

    override val showHomeAsUp: Boolean = false

    override val bindingVariable: Int? = BR.generateBankSlipSuccessViewModel

    override val getLayoutId: Int? = R.layout.fragment_bankslip_success

    override fun getStatusBarAppearance(): StatusBarColor = BankSlipOpKey.BANK_SLIP_SUCCESS.theme

    override fun tag(): String = BankSlipOpKey.BANK_SLIP_SUCCESS.name

    override fun getToolbar(): Toolbar? =
        binding?.toolbarBankslipSuccess?.navToolbar

    companion object : FragmentNewInstance<GenerateBankSlipSuccessFragment>(::GenerateBankSlipSuccessFragment)

    private val slipId: String by lazy { checkNotNull(arguments?.getString(BANKSLIP_ID, "")) }
    private val slipNumber: String by lazy { checkNotNull(arguments?.getString(BANKSLIP_NUMBER, "")) }
    private val slipDescription: String by lazy { checkNotNull(arguments?.getString(BANKSLIP_DESCRIPTION, "")) }

    override val viewModel: GenerateBankSlipSuccessViewModel
    by viewModel { (parametersOf(slipNumber, slipId, slipDescription)) }

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onBtnClicked(it) }

        setupObservers()
    }

    private fun onBtnClicked(v: View) {
        when (v.id) {
            R.id.btn_copy -> {
                viewModel.onCopyClick()
            }
            R.id.btn_go_back -> {
                activity?.finish()
            }
        }
    }

    override val menu: Int? = R.menu.menu_success_screen

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.btn_see_bankslip -> {
                onSeeBankSlipClickedWithPermissionCheck()
                true
            }
            R.id.btn_share -> {
                onShareBankSlipClickedWithPermissionCheck()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }

    private fun setupObservers() {
        viewModel.cashInBankSlipAction.observe(
            this,
            {
                when (it) {
                    is BankSlipAction.Copy -> {
                        copyBankSlip(it.transferText)
                    }
                    is BankSlipAction.SeeBankSlip -> {
                        seeBankSlip(it.pdfFile)
                    }
                    is BankSlipAction.Share -> {
                        shareBankSlip(it.pdfFile)
                    }
                }
            }
        )
    }

    private fun copyBankSlip(transferText: String) {
        context?.let { contextLet ->
            transferText.setInClipboard(getString(R.string.showbankslip_copy_clipboard_label), contextLet)
            contextLet.toast(getString(R.string.toast_copy_message))
        }
    }

    @ExperimentalCoroutinesApi
    @NeedsPermission(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun onSeeBankSlipClicked() = viewModel.onSeeBankSlipClick()

    private fun seeBankSlip(pdfFile: File) {
        val path = FileProvider.getUriForFile(
            requireContext(),
            BuildConfig.APPLICATION_ID + DOT_PROVIDER,
            pdfFile
        )

        Intent(Intent.ACTION_VIEW).apply {
            setDataAndType(path, PDF_MIME_TYPE)
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        }.run {
            requireContext().startActivity(
                Intent.createChooser(
                    this,
                    getString(R.string.showbankslip_open_bankslip_title)
                )
            )
        }
    }

    @ExperimentalCoroutinesApi
    @NeedsPermission(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun onShareBankSlipClicked() = viewModel.onShareClick()

    private fun shareBankSlip(pdfFile: File) {
        Intent(Intent.ACTION_SEND).apply {
            type = PDF_MIME_TYPE
            putExtra(
                Intent.EXTRA_STREAM,
                FileProvider.getUriForFile(
                    requireContext(),
                    BuildConfig.APPLICATION_ID + DOT_PROVIDER,
                    pdfFile
                )
            )
            putExtra(Intent.EXTRA_SUBJECT, getString(R.string.showbankslip_share_title))
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        }.run {
            startActivity(
                Intent.createChooser(
                    this,
                    getString(R.string.showbankslip_share_title)
                )
            )
        }
    }

    @OnPermissionDenied(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun showOnPermissionDeniedDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setPositiveButton(R.string.showbankslip_ok) { _, _ -> }
            .setNegativeButton(R.string.showbankslip_cancel) { _, _ -> }
            .setCancelable(false)
            .setMessage(R.string.showbankslip_rationale_dialog_message)
            .show()
    }

    @OnNeverAskAgain(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun onNeverAskAgain() {
        MaterialAlertDialogBuilder(requireContext())
            .setPositiveButton(R.string.showbankslip_open_settings) { _, _ ->
                requireContext().openDeviceSettings()
            }
            .setNegativeButton(R.string.showbankslip_cancel) { _, _ -> }
            .setCancelable(false)
            .setMessage(R.string.showbankslip_never_ask_permissions_again)
            .show()
    }
}