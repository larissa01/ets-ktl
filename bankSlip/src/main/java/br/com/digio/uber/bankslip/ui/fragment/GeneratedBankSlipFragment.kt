package br.com.digio.uber.bankslip.ui.fragment

import android.Manifest
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.uber.bankslip.BR
import br.com.digio.uber.bankslip.BuildConfig
import br.com.digio.uber.bankslip.R
import br.com.digio.uber.bankslip.adapter.GeneratedBankSlipAdapter
import br.com.digio.uber.bankslip.adapter.OnClickGeneratedBankSlipItem
import br.com.digio.uber.bankslip.databinding.GeneratedBankSlipFragmentBinding
import br.com.digio.uber.bankslip.navigation.BankSlipOpKey
import br.com.digio.uber.bankslip.ui.viewmodel.GeneratedBankSlipViewModel
import br.com.digio.uber.bankslip.uiModel.GeneratedBankSlipItemUi
import br.com.digio.uber.bankslip.uiModel.GeneratedBankSlipUiModel
import br.com.digio.uber.common.extensions.openDeviceSettings
import br.com.digio.uber.common.extensions.seeFile
import br.com.digio.uber.common.extensions.shareFile
import br.com.digio.uber.common.extensions.toast
import br.com.digio.uber.common.generics.makeThreeOptionsMessageObject
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.bankSlip.BankSlipAction
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.Utils.BANKSLIP_STATUS_CREATED
import br.com.digio.uber.util.setInClipboard
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.viewmodel.ext.android.viewModel
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions

@RuntimePermissions
class GeneratedBankSlipFragment :
    BaseBankSlipFragment<GeneratedBankSlipFragmentBinding, GeneratedBankSlipViewModel>(),
    OnClickGeneratedBankSlipItem {

    private lateinit var adapter: GeneratedBankSlipAdapter

    override val bindingVariable: Int? = BR.generatedBankSlipItem
    override val getLayoutId: Int? = R.layout.generated_bank_slip_fragment
    override val viewModel: GeneratedBankSlipViewModel by viewModel()
    override fun tag(): String = BankSlipOpKey.BANK_SLIP_GENERATED.name
    override fun getStatusBarAppearance(): StatusBarColor = BankSlipOpKey.BANK_SLIP_GENERATED.theme

    companion object : FragmentNewInstance<GeneratedBankSlipFragment>(::GeneratedBankSlipFragment)

    override fun initialize() {
        super.initialize()
        viewModel.initializer(BANKSLIP_STATUS_CREATED)
        setupAdapter()
        setupObservers()
    }

    private fun setupAdapter() {
        viewModel.listGeneratedBankSlipItemUi.observe(
            this,
            {
                if (it != null) {
                    binding?.recyclerviewGeneratedBankslip?.layoutManager = LinearLayoutManager(
                        requireContext()
                    )
                    adapter = GeneratedBankSlipAdapter(it as MutableList<GeneratedBankSlipItemUi>, this)
                    binding?.recyclerviewGeneratedBankslip?.adapter = adapter
                }
            }
        )
    }

    private fun setupObservers() {
        viewModel.showBankSlipAction.observe(
            this,
            {
                when (it) {
                    is BankSlipAction.Copy -> {
                        copyBankSlip(it.transferText)
                    }
                    is BankSlipAction.SeeBankSlip -> {
                        activity?.seeFile(
                            it.pdfFile,
                            BuildConfig.APPLICATION_ID + Const.Extras.DOT_PROVIDER,
                            getString(R.string.showbankslip_open_bankslip_title)
                        )
                    }
                    is BankSlipAction.Share -> {
                        activity?.shareFile(
                            it.pdfFile,
                            BuildConfig.APPLICATION_ID + Const.Extras.DOT_PROVIDER,
                            Const.MimeTypes.PDF_MIME_TYPE,
                            getString(R.string.showbankslip_share_title),
                            getString(R.string.showbankslip_share_title)
                        )
                    }
                }
            }
        )
    }

    private fun copyBankSlip(transferText: String) {
        context?.let { contextLet ->
            transferText.setInClipboard(getString(R.string.showbankslip_copy_clipboard_label), contextLet)
            contextLet.toast(getString(R.string.toast_copy_message))
        }
    }

    override fun invoke(item: GeneratedBankSlipUiModel) {
        if (item is GeneratedBankSlipItemUi) {
            showMessage(
                makeThreeOptionsMessageObject {
                    hideIcon = true
                    hideTitle = true
                    hideMessage = true
                    positiveOptionTextInt = R.string.generated_bank_slip_copy_button
                    middleOptionTextInt = R.string.generated_bank_slip_see_button
                    negativeOptionTextInt = R.string.generated_bank_slip_share
                    onPositiveButtonClick = {
                        viewModel.onCopyClick(item.bankSlipNumberText)
                    }
                    onMiddleButtonClick =
                        { onSeeBankSlipClickedWithPermissionCheck(item.bankSlipUuid, item.bankSlipDescription) }
                    onNegativeButtonClick =
                        { onShareBankSlipClickedWithPermissionCheck(item.bankSlipUuid, item.bankSlipDescription) }
                }
            )
        }
    }

    @ExperimentalCoroutinesApi
    @NeedsPermission(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun onSeeBankSlipClicked(bankSlipNumberText: String, bankSlipDescription: String) =
        viewModel.onSeeBankSlipClick(bankSlipNumberText, bankSlipDescription)

    @ExperimentalCoroutinesApi
    @NeedsPermission(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun onShareBankSlipClicked(bankSlipNumberText: String, bankSlipDescription: String) =
        viewModel.onShareClick(bankSlipNumberText, bankSlipDescription)

    @OnPermissionDenied(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun showOnPermissionDeniedDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setPositiveButton(R.string.showbankslip_ok) { _, _ -> }
            .setNegativeButton(R.string.showbankslip_cancel) { _, _ -> }
            .setCancelable(false)
            .setMessage(R.string.showbankslip_rationale_dialog_message)
            .show()
    }

    @OnNeverAskAgain(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    fun onNeverAskAgain() {
        MaterialAlertDialogBuilder(requireContext())
            .setPositiveButton(R.string.showbankslip_open_settings) { _, _ ->
                requireContext().openDeviceSettings()
            }
            .setNegativeButton(R.string.showbankslip_cancel) { _, _ -> }
            .setCancelable(false)
            .setMessage(R.string.showbankslip_never_ask_permissions_again)
            .show()
    }
}