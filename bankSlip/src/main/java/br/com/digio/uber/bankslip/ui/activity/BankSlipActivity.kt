package br.com.digio.uber.bankslip.ui.activity

import android.os.Bundle
import br.com.digio.uber.bankslip.di.bankSlipViewModelModule
import br.com.digio.uber.bankslip.navigation.BankSlipNavigation
import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.navigation.GenericNavigation
import org.koin.core.module.Module

/**
 * @author Marlon D. Rocha
 * @since 14/10/20
 */
class BankSlipActivity : BaseActivity() {

    private val bankSlipNavigation = BankSlipNavigation(this)
    override fun getLayoutId(): Int? = LayoutIds.navigationLayoutActivity
    override fun getModule(): List<Module>? = listOf(bankSlipViewModelModule)
    override fun getNavigation(): GenericNavigation? = bankSlipNavigation

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        bankSlipNavigation.init(intent.extras)
    }
}