package br.com.digio.uber.bankslip.mapper

import android.util.Base64
import br.com.digio.uber.bankslip.R
import br.com.digio.uber.bankslip.uiModel.BankSlipPdfUiModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.model.bankSlip.FileResponse
import br.com.digio.uber.util.saveAsDownload

/**
 * @author Marlon D. Rocha
 * @since 19/10/20
 */

fun FileResponse.toDownloadedFile(
    bankSlipDescription: String,
    resourceManager: ResourceManager
): BankSlipPdfUiModel {
    return BankSlipPdfUiModel(
        pdfFile = Base64.decode(base64, Base64.DEFAULT)
            .saveAsDownload(createBankSlipFileName(bankSlipDescription, resourceManager))
    )
}

private fun createBankSlipFileName(
    bankSlipDescription: String,
    resourceManager: ResourceManager
): String {
    return resourceManager.getString(R.string.generatebankslip_bankslipFileName)
        .plus(bankSlipDescription)
        .plus(System.currentTimeMillis().toString())
        .plus(resourceManager.getString(R.string.dot_pdf))
}