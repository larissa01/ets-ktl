package br.com.digio.uber.bankslip.uiModel

import java.io.File

/**
 * @author Marlon D. Rocha
 * @since 19/10/20
 */
data class BankSlipPdfUiModel(
    val pdfFile: File
)