package br.com.digio.uber.bankslip.ui.fragment

import android.view.MenuItem
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.base.viewmodel.BaseViewModel

abstract class BaseBankSlipFragment<T : ViewDataBinding, VM : BaseViewModel> : BNWFViewModelFragment<T, VM>() {

    override val showHomeAsUp: Boolean = true
    override val showDisplayShowTitle: Boolean = true

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> {
                callGoBack()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}