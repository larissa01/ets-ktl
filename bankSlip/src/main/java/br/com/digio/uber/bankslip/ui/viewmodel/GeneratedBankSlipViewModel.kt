package br.com.digio.uber.bankslip.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.bankslip.mapper.GeneratedBankSlipListMapper
import br.com.digio.uber.bankslip.mapper.toDownloadedFile
import br.com.digio.uber.bankslip.uiModel.GeneratedBankSlipItemUi
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.domain.usecase.bankSlip.GeneratedBankSlipUseCase
import br.com.digio.uber.domain.usecase.bankSlip.GetBankSlipPdfUseCase
import br.com.digio.uber.model.bankSlip.BankSlipAction
import br.com.digio.uber.model.bankSlip.BankSlipPdfRequest
import java.io.File

class GeneratedBankSlipViewModel constructor(
    private val generatedBankSlipUseCase: GeneratedBankSlipUseCase,
    private val getBankSlipPdfUseCase: GetBankSlipPdfUseCase,
    private val resourceManager: ResourceManager,
    private val analytics: Analytics
) : BaseViewModel() {

    val listGeneratedBankSlipItemUi = MutableLiveData<List<GeneratedBankSlipItemUi>>()
    val showBankSlipAction = MutableLiveData<BankSlipAction>()
    private var pdfFile: File? = null

    fun initializer(status: String) {
        generatedBankSlipUseCase(status).singleExec(
            onSuccessBaseViewModel = {
                listGeneratedBankSlipItemUi.value = GeneratedBankSlipListMapper().map(it)
            }
        )
    }

    fun onShareClick(bankSlipNumberText: String, bankSlipDescription: String) {
        getBankSlipPdf(bankSlipNumberText, bankSlipDescription) {
            BankSlipAction.Share(it)
        }
    }

    fun onSeeBankSlipClick(bankSlipNumberText: String, bankSlipDescription: String) {
        analytics.pushSimpleEvent(
            Tags.BANKSLIP_RECEIVE_OPTION,
            Tags.PARAMETER_BANKSLIP_OPTION to Tags.BANKSLIP_SEE_PDF
        )
        getBankSlipPdf(bankSlipNumberText, bankSlipDescription) {
            BankSlipAction.SeeBankSlip(it)
        }
    }

    fun onCopyClick(bankSlipNumberText: String) {
        analytics.pushSimpleEvent(
            Tags.BANKSLIP_RECEIVE_OPTION,
            Tags.PARAMETER_BANKSLIP_OPTION to Tags.BANKSLIP_COPY_CODE
        )
        showBankSlipAction.value = BankSlipAction.Copy(bankSlipNumberText)
    }

    fun getBankSlipPdf(
        bankSlipId: String,
        bankSlipDescription: String,
        successBlock: (pdfFile: File) -> BankSlipAction
    ) {
        pdfFile?.let {
            onGetBankSlipPdfSuccess(it, successBlock)
            return
        }
        getBankSlipPdfUseCase(BankSlipPdfRequest(bankSlipId)).singleExec(
            showLoadingFlag = true,
            onSuccessBaseViewModel = {
                onGetBankSlipPdfSuccess(
                    it.toDownloadedFile(bankSlipDescription, resourceManager).pdfFile,
                    successBlock
                )
            }
        )
    }

    private fun onGetBankSlipPdfSuccess(
        pdfFile: File,
        successBlock: (pdfFile: File) -> BankSlipAction
    ) {
        this.pdfFile = pdfFile
        showBankSlipAction.value = successBlock(pdfFile)
    }
}