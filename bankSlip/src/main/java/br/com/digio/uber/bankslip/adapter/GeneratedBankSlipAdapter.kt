package br.com.digio.uber.bankslip.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.bankslip.R
import br.com.digio.uber.bankslip.adapter.viewholder.GeneratedBankSlipViewHolder
import br.com.digio.uber.bankslip.uiModel.GeneratedBankSlipItemUi
import br.com.digio.uber.bankslip.uiModel.GeneratedBankSlipUiModel
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

typealias OnClickGeneratedBankSlipItem = (item: GeneratedBankSlipUiModel) -> Unit

class GeneratedBankSlipAdapter constructor(
    private var values: MutableList<GeneratedBankSlipItemUi> = mutableListOf(),
    private val listener: OnClickGeneratedBankSlipItem
) : BaseRecyclerViewAdapter<GeneratedBankSlipUiModel, GeneratedBankSlipAdapter.ViewTypesDataBindingFactoryImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl = ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): GeneratedBankSlipItemUi = values[position]

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<GeneratedBankSlipUiModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<GeneratedBankSlipUiModel>
    }

    override fun onBindViewHolder(holder: AbstractViewHolder<GeneratedBankSlipUiModel>, position: Int) =
        values[holder.adapterPosition].let { holder.bind(it) }

    override fun getItemCount(): Int = values.size

    override fun replaceItems(list: List<Any>) = addValues(list.safeHeritage())

    private fun addValues(values: List<GeneratedBankSlipItemUi>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }

    class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<GeneratedBankSlipUiModel> {
        override fun type(model: GeneratedBankSlipUiModel): Int =
            when (model) {
                is GeneratedBankSlipUiModel -> R.layout.generated_bank_slip_item
            }

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<GeneratedBankSlipUiModel>
        ): AbstractViewHolder<*> =
            when (type) {
                R.layout.generated_bank_slip_item -> GeneratedBankSlipViewHolder(view, listener)
                else -> throw IndexOutOfBoundsException("Invalid view type")
            }
    }
}