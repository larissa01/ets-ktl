package br.com.digio.uber.bankslip.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import br.com.digio.uber.analytics.Analytics
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.bankslip.mapper.toDownloadedFile
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.bankSlip.GetBankSlipPdfUseCase
import br.com.digio.uber.model.bankSlip.BankSlipAction
import br.com.digio.uber.model.bankSlip.BankSlipPdfRequest
import br.com.digio.uber.util.Const.MaskPattern.CASH_IN_BANKSLIP_NUMBER
import br.com.digio.uber.util.applyMask
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.File

class GenerateBankSlipSuccessViewModel constructor(
    private val analytics: Analytics,
    private val bankSlipNumberText: String,
    private val bankSlipId: String,
    private val bankSlipDescription: String,
    private val getBankSlipPdfUseCase: GetBankSlipPdfUseCase,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private var pdfFile: File? = null

    private val _bankSlipNumber = MutableLiveData<String>().apply { value = bankSlipNumberText }

    private val _showBankSlipAction = MutableLiveData<BankSlipAction>()

    val bankSlipNumber: LiveData<String>
        get() = Transformations.map(_bankSlipNumber) {
            it.applyMask(CASH_IN_BANKSLIP_NUMBER)
        }

    val cashInBankSlipAction: LiveData<BankSlipAction>
        get() = _showBankSlipAction

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        analytics.pushSimpleEvent(Tags.BANKSLIP_GENERATE_SUCCESS)
    }

    fun onCopyClick() {
        _showBankSlipAction.value = BankSlipAction.Copy(bankSlipNumberText)
    }

    @ExperimentalCoroutinesApi
    fun onShareClick() {
        getBankSlipPdf {
            BankSlipAction.Share(it)
        }
    }

    @ExperimentalCoroutinesApi
    fun onSeeBankSlipClick() {
        getBankSlipPdf {
            BankSlipAction.SeeBankSlip(it)
        }
    }

    @ExperimentalCoroutinesApi
    private fun getBankSlipPdf(
        successBlock: (pdfFile: File) -> BankSlipAction
    ) {
        pdfFile?.let {
            onGetBankSlipPdfSuccess(it, successBlock)
            return
        }
        getBankSlipPdfUseCase(BankSlipPdfRequest(bankSlipId)).singleExec(
            showLoadingFlag = true,
            onSuccessBaseViewModel = {
                onGetBankSlipPdfSuccess(
                    it.toDownloadedFile(bankSlipDescription, resourceManager).pdfFile,
                    successBlock
                )
            }
        )
    }

    private fun onGetBankSlipPdfSuccess(
        pdfFile: File,
        successBlock: (pdfFile: File) -> BankSlipAction
    ) {
        this.pdfFile = pdfFile
        _showBankSlipAction.value = successBlock(pdfFile)
    }
}