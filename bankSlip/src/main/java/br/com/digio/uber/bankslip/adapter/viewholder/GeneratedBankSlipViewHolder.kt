package br.com.digio.uber.bankslip.adapter.viewholder

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.bankslip.BR
import br.com.digio.uber.bankslip.uiModel.GeneratedBankSlipItemUi
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.ViewTypesListener

class GeneratedBankSlipViewHolder(
    private val viewDataBinding: ViewDataBinding,
    private val listener: ViewTypesListener<GeneratedBankSlipItemUi>
) : AbstractViewHolder<GeneratedBankSlipItemUi>(viewDataBinding.root) {

    override fun bind(item: GeneratedBankSlipItemUi) {
        viewDataBinding.setVariable(BR.generatedBankSlipItem, item)
        viewDataBinding.setVariable(BR.generatedBankSlipViewHolder, this)
        viewDataBinding.executePendingBindings()
    }

    fun onClick(item: GeneratedBankSlipItemUi) {
        listener(item)
    }
}