package br.com.digio.uber.bankslip.ui.viewmodel

import br.com.digio.uber.common.base.viewmodel.BaseViewModel

/**
 * @author Marlon D. Rocha
 * @since 14/10/20
 */
class BankSlipActivityViewModel : BaseViewModel()