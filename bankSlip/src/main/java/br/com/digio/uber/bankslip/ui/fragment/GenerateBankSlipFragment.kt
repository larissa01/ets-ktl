package br.com.digio.uber.bankslip.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.bankslip.BR
import br.com.digio.uber.bankslip.R
import br.com.digio.uber.bankslip.databinding.GenerateBankSlipFragmentBinding
import br.com.digio.uber.bankslip.navigation.BankSlipOpKey
import br.com.digio.uber.bankslip.ui.viewmodel.GenerateBankSlipViewModel
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import org.koin.android.viewmodel.ext.android.viewModel

class GenerateBankSlipFragment : BaseBankSlipFragment<GenerateBankSlipFragmentBinding, GenerateBankSlipViewModel>() {

    override val bindingVariable: Int = BR.generateBankSlipViewModel

    override val getLayoutId: Int = R.layout.generate_bank_slip_fragment

    override val viewModel: GenerateBankSlipViewModel by viewModel()

    override fun getStatusBarAppearance(): StatusBarColor = BankSlipOpKey.BANK_SLIP.theme

    override fun tag(): String = BankSlipOpKey.BANK_SLIP.name

    override fun getToolbar(): Toolbar? =
        binding?.toolbarBankslip?.navToolbar

    companion object : FragmentNewInstance<GenerateBankSlipFragment>(::GenerateBankSlipFragment) {
        const val BANKSLIP_ID = "bankslip_id"
        const val BANKSLIP_NUMBER = "bankslip_number"
        const val BANKSLIP_DESCRIPTION = "bankslip_description"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.inputBankSlipValue?.setEndIconOnClickListener {
            viewModel.onClearBankSlipValue()
        }
    }

    override fun initialize() {
        super.initialize()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.bankSlipValue.observe(
            this,
            {
                viewModel.validateFields()
            }
        )

        viewModel.bankSlipDescription.observe(
            this,
            {
                viewModel.validateFields()
            }
        )
        viewModel.onSuccessGenerate.observe(
            this,
            { response ->
                if (response != null) {
                    arguments = Bundle().apply {
                        putString(BANKSLIP_ID, response.uuid)
                        putString(BANKSLIP_NUMBER, response.typeableLine)
                        putString(BANKSLIP_DESCRIPTION, response.description)
                    }
                    callGoTo(BankSlipOpKey.BANK_SLIP_SUCCESS.name)
                }
            }
        )
    }
}