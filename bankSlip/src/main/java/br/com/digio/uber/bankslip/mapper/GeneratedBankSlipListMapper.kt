package br.com.digio.uber.bankslip.mapper

import br.com.digio.uber.bankslip.uiModel.GeneratedBankSlipItemUi
import br.com.digio.uber.common.extensions.toMoneyNumber
import br.com.digio.uber.model.bankSlip.GeneratedBankSlipResponse
import br.com.digio.uber.util.DatePattern
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.util.toCalendar
import br.com.digio.uber.util.toPattern

class GeneratedBankSlipListMapper : AbstractMapper<GeneratedBankSlipResponse, List<GeneratedBankSlipItemUi>> {
    override fun map(param: GeneratedBankSlipResponse) = with(param) {
        data.map {
            GeneratedBankSlipItemUi(
                bankSlipUuid = it.uuid,
                bankSlipNumberText = it.typeableLine,
                bankSlipDescription = it.description ?: "",
                bankSlipValue = it.amount.toMoneyNumber(),
                bankSlipDueDate = it.dueDate.toCalendar().toPattern(DatePattern.DD_MM_BR),
            )
        }
    }
}