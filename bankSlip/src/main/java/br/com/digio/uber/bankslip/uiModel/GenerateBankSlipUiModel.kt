package br.com.digio.uber.bankslip.uiModel

import android.text.Spannable
import java.math.BigDecimal

/**
 * @author Marlon D. Rocha
 * @since 15/10/20
 */
data class GenerateBankSlipUiModel(
    val bankSlipsAvailable: String,
    val bankSlipsAvailableDescription: String,
    val hasBankSlipsAvailable: Boolean,
    val bankSlipValidUntil: String,
    val bankSlipMaximumValue: BigDecimal,
    val bankSlipMinimumValue: BigDecimal,
    val bankSlipValueHelperText: Spannable,
    val bankSlipValueErrorText: Spannable,
    val bankSlipFeeValue: String
)