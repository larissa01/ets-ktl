package br.com.digio.uber.bankslip.mapper

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import br.com.digio.uber.bankslip.R
import br.com.digio.uber.bankslip.uiModel.GenerateBankSlipUiModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.model.bankSlip.BankSlipStatus
import br.com.digio.uber.util.DatePattern
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.util.toCalendar
import br.com.digio.uber.util.toMoney
import br.com.digio.uber.util.toPattern
import br.com.digio.uber.util.withTypeface

/**
 * @author Marlon D. Rocha
 * @since 15/10/20
 */
class GenerateBankSlipMapper(private val resourceManager: ResourceManager) :

    AbstractMapper<BankSlipStatus, GenerateBankSlipUiModel> {

    override fun map(param: BankSlipStatus): GenerateBankSlipUiModel = with(param) {
        val bankSlipDescriptionText: String = when (availableLimits.userLimits.maxQtdForMonth) {
            0 -> resourceManager.getString(R.string.generatebankslip_zeroavailablebankslips)
            1 -> resourceManager.getString(R.string.generatebankslip_oneavailablebankslips)
            else -> resourceManager.getString(R.string.generatebankslip_availablebankslips)
        }

        val bankSlipHelperText: Spannable = SpannableStringBuilder()
            .append(resourceManager.getString(R.string.generatebankslip_valueHelperText))
            .append(" ")
            .append(availableLimits.generalLimits.minAmount.toMoney().withTypeface(Typeface.BOLD))
            .append(" ")
            .append(resourceManager.getString(R.string.all_and))
            .append(" ")
            .append(availableLimits.generalLimits.maxAmount.toMoney().withTypeface(Typeface.BOLD))

        GenerateBankSlipUiModel(
            availableLimits.userLimits.maxQtdForMonth.toString(),
            bankSlipDescriptionText,
            availableLimits.userLimits.maxQtdForMonth > 0,
            dueDate.toCalendar().toPattern(DatePattern.DD_MM_YYYY_BR),
            availableLimits.generalLimits.maxAmount,
            availableLimits.generalLimits.minAmount,
            bankSlipHelperText,
            bankSlipHelperText,
            availableLimits.overlimitTax?.toMoney() ?: resourceManager.getString(R.string.value_free)

        )
    }
}