package br.com.digio.uber.bankslip.di

import br.com.digio.uber.bankslip.ui.viewmodel.BankSlipActivityViewModel
import br.com.digio.uber.bankslip.ui.viewmodel.GenerateBankSlipSuccessViewModel
import br.com.digio.uber.bankslip.ui.viewmodel.GenerateBankSlipViewModel
import br.com.digio.uber.bankslip.ui.viewmodel.GeneratedBankSlipViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * @author Marlon D. Rocha
 * @since 14/10/20
 */

val bankSlipViewModelModule = module {
    viewModel { BankSlipActivityViewModel() }
    viewModel { GenerateBankSlipViewModel(get(), get(), get(), get()) }
    viewModel { (slipNumber: String, slipId: String, slipDescription: String) ->
        GenerateBankSlipSuccessViewModel(
            get(),
            slipNumber,
            slipId,
            slipDescription,
            get(),
            get()
        )
    }
    viewModel { GeneratedBankSlipViewModel(get(), get(), get(), get()) }
}