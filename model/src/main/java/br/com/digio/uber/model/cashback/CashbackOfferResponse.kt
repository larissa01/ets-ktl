package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CashbackOfferResponse(
    @SerializedName("promotionId")
    val promotionId: Long? = null,
    @SerializedName("offerType")
    val offerType: String? = null,
    @SerializedName("codeVoucher")
    val codeVoucher: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("starts")
    val starts: String? = null,
    @SerializedName("ends")
    val ends: String? = null,
    @SerializedName("terms")
    val terms: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("partnerId")
    val partnerId: Long? = null
) : Serializable