package br.com.digio.uber.model.recharge

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class FavoriteContact(
    @SerializedName("phoneNumber")
    val phoneNumber: String?,
    @SerializedName("ddd")
    val ddd: String?,
    @SerializedName("subproductId")
    val subproductId: Long?,
    @SerializedName("nickname")
    val nickname: String?,
    @SerializedName("carrier")
    val carrier: String?,
    @SerializedName("carrierIcon")
    val carrierIcon: String?,
    @SerializedName("document")
    val document: String?,
    @SerializedName("productType")
    val productType: String?
) : Serializable