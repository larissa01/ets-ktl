package br.com.digio.uber.model.payment

import com.google.gson.annotations.SerializedName

data class PayWithQrCodeRequest(

    @SerializedName("transactionIdentifier")
    val transactionIdentifier: String,
    @Transient
    val pidHash: String
)