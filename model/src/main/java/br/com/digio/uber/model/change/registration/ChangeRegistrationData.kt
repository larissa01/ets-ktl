package br.com.digio.uber.model.change.registration

import com.google.gson.annotations.SerializedName

data class ChangeRegistrationData(
    @SerializedName("unchangeable")
    val unchangeable: List<ChangeRegistrationItemList>,
    @SerializedName("changeable")
    val changeable: List<ChangeRegistrationItemList>,
    @SerializedName("changeableAddresses")
    val changeableAddress: List<AddressRegistration>?,
    @SerializedName("unchangeableAddresses")
    val unchangeableAddresses: List<AddressRegistration>?
)

data class ChangeRegistrationRequest(
    @SerializedName("hash")
    val pidHash: String,
    @SerializedName("infoUpdate")
    val registrationItem: ChangeRegistrationItemList
)

data class ChangeRegistrationItemList(
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: ChangeRegistrationItemType,
    @SerializedName("order")
    val order: Int?,
    @SerializedName("value")
    val value: String? = null,
    @SerializedName("valueNumber")
    val valueNumber: Float? = null,
    @SerializedName("hash")
    val pidHash: String? = null
)

data class AddressRegistrationRequest(
    @SerializedName("hash")
    val pidHash: String,
    @SerializedName("address")
    val registrationItem: AddressRegistration
)

data class AddressRegistration(
    @SerializedName("id")
    val id: Long?,
    @SerializedName("street")
    val street: String?,
    @SerializedName("uuid")
    val uuid: String? = null,
    @SerializedName("conductorAddressId")
    val conductorAddressId: String? = null,
    @SerializedName("city")
    val city: String?,
    @SerializedName("complement")
    val complement: String?,
    @SerializedName("country")
    val country: String?,
    @SerializedName("neighborhood")
    val neighborhood: String?,
    @SerializedName("number")
    val number: Int?,
    @SerializedName("state")
    val state: String?,
    @SerializedName("zipCode")
    val zipCode: String?,
    @SerializedName("addressTypeId")
    val addressTypeId: Long,
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: ChangeRegistrationItemType?,
    @SerializedName("order")
    val order: Int?,
    @SerializedName("hash")
    val pidHash: String? = null
)

enum class ChangeRegistrationItemType {
    NAME,
    BIRTHDATE,
    NATIONALITY,
    DOCUMENT,
    MOTHER_NAME,
    PROFESSION,
    RESIDENTIAL,
    DELIVERY,
    PHONE,
    EMAIL,
    SCHOLARITY,
    INCOME,
    GENDER,
    SECONDARY_DOC,
    UNKNOWN
}