package br.com.digio.uber.model.payment

import com.google.gson.annotations.SerializedName

data class PayWithQrCodeResponse(
    @SerializedName("error")
    val error: Error,
    @SerializedName("path")
    val path: String,
    @SerializedName("statusCode")
    val statusCode: Int,
    @SerializedName("statusError")
    val statusError: String
)
data class Error(
    @SerializedName("code")
    val code: String,
    @SerializedName("data")
    val data: Any,
    @SerializedName("fields")
    val fields: Any,
    @SerializedName("message")
    val message: String
)