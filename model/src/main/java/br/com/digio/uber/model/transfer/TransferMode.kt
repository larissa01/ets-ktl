package br.com.digio.uber.model.transfer

enum class TransferMode {
    TED, PIX
}