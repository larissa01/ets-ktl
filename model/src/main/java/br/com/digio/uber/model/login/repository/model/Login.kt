package br.com.digio.uber.model.login.repository.model

import br.com.digio.uber.model.login.LoginReq
import br.com.digio.uber.model.login.PidInfo
import br.com.digio.uber.model.login.RiskAnalysisInfo
import br.com.digio.uber.model.login.TokenizantionInfo
import br.com.digio.uber.model.request.DeviceInfo

data class Login(
    val password: String,
    val pidInfo: PidInfo?,
    val riskAnalysisInfo: RiskAnalysisInfo? = null,
    val tokenizationInfo: TokenizantionInfo? = null,
    val userName: String
) {
    fun toLoginReq(deviceInfo: DeviceInfo): LoginReq =
        LoginReq(
            deviceInfo = deviceInfo,
            userName = userName,
            password = password,
            pidInfo = pidInfo,
            riskAnalysisInfo = riskAnalysisInfo,
            tokenizationInfo = tokenizationInfo
        )
}