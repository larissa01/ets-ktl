package br.com.digio.uber.model.cardReceived

import com.google.gson.annotations.SerializedName

data class CardReceivedActivationRequest(
    @SerializedName("cvv")
    val cvv: String,
    @SerializedName("cryptoKeyId")
    val cryptoKeyId: Int?,
    @SerializedName("cardLastDigits")
    val cardLastDigits: String,
    @SerializedName("cardId")
    val cardId: String
)