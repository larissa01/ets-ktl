package br.com.digio.uber.model.request

import com.google.gson.annotations.SerializedName

data class DeviceInfo(
    @SerializedName("appVersion")
    val appVersion: String,
    @SerializedName("brand")
    val brand: String,
    @SerializedName("deviceToken")
    val deviceToken: String,
    @SerializedName("deviceId")
    val deviceId: String,
    @SerializedName("deviceModel")
    val deviceModel: String,
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double,
    @SerializedName("osName")
    val osName: String,
    @SerializedName("osVersion")
    val osVersion: String,
    @SerializedName("root")
    val root: Boolean
)