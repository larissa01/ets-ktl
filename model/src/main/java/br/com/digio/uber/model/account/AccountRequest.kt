package br.com.digio.uber.model.account

import com.google.gson.annotations.SerializedName

data class AccountRequest(
    @SerializedName("contractAccepted")
    val contractAccepted: Boolean,
    @SerializedName("geolocation")
    val geolocation: GeolocationRequest,
    @SerializedName("picture")
    val picture: String,
    @SerializedName("password")
    val password: String
)