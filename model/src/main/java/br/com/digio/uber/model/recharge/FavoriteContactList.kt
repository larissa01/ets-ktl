package br.com.digio.uber.model.recharge

import com.google.gson.annotations.SerializedName

data class FavoriteContactList(
    @SerializedName("favoritePhoneResponses")
    val favoritePhoneResponses: List<FavoriteContact>?
)