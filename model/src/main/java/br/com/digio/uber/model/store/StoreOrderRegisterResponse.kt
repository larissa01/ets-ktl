package br.com.digio.uber.model.store

import com.google.gson.annotations.SerializedName

data class StoreOrderRegisterResponse(
    @SerializedName("responseCode")
    val responseCode: Int,
    @SerializedName("description")
    val description: String?
)

@Suppress("MagicNumber")
enum class SubproductOrderResponseEnum constructor(val code: Int, val description: String) {
    ACCEPTED(0, "Accepted"),
    INVALID_CREDENTIAL(1, "Invalid credential"),
    INACTIVE_SUBPRODUCT(2, "Inactive subproduct"),
    INEXISTENT_SUBPRODUCT(3, "Inexistent subproduct"),
    INEXISTENT_ORDER(4, "Inexistent order"),
    INEXISTENT_PRODUCT(5, "Inexistent product"),
    AGE_OUT_OF_LIMIT(6, "Age out of limit"),
    AGE_FIELD_EMPTY(7, "Age field is empty or null"),
    SAME_INSURANCE_ACQUISITION(8, "User already has this insurance"),
    HAVE_INSURANCE(9, "User already has insurance"),
    SAME_LIVELO_ACQUISITION(10, "User already has this livelo product"),
    INTERNAL_ERROR(99, "Internal error")
}