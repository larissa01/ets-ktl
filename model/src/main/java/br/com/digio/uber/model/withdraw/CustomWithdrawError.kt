package br.com.digio.uber.model.withdraw

import com.google.gson.annotations.SerializedName

data class CustomWithdrawError(
    @SerializedName("possibleAmounts")
    val possibleAmounts: List<Int>?
)