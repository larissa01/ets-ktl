package br.com.digio.uber.model.pid

import com.google.gson.annotations.SerializedName

data class PidReq(
    @SerializedName("name")
    val name: PidType
)