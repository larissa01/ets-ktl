package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName

class CashbackCutomerTier(
    @SerializedName("id")
    val id: Long,
    @SerializedName("code")
    val code: Tiers? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("productType")
    val productType: String? = null,
    @SerializedName("color")
    val color: String? = null
)