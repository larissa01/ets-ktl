package br.com.digio.uber.model.transfer

data class InternalTransferDomainRequest(
    val agency: String,
    val account: String,
    val cpf: String,
    val name: String,
    val amount: String,
    val saveBeneficiary: Boolean,
    val description: String,
    val password: String,
    val date: String?
)