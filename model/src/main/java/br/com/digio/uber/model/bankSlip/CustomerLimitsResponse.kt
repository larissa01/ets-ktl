package br.com.digio.uber.model.bankSlip

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * @author Marlon D. Rocha
 * @since 16/10/20
 */
data class CustomerLimitsResponse(
    @SerializedName("generalLimits")
    val generalLimits: GeneralLimits,
    @SerializedName("userLimits")
    val userLimits: UserLimits,
    @SerializedName("userUsage")
    val userUsage: UserUsage,
    @SerializedName("overlimitTax")
    val overlimitTax: BigDecimal?,
    @SerializedName("allowOverlimit")
    val allowOverlimit: Boolean,
    @SerializedName("mustChargeOverlimitTax")
    val mustChargeOverlimitTax: Boolean
)