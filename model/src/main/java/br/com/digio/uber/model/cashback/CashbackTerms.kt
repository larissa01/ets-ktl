package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName

data class CashbackTerms(
    @SerializedName("term")
    val term: String? = null
)