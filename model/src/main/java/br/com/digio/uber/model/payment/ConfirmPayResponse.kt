package br.com.digio.uber.model.payment

import com.google.gson.annotations.SerializedName

data class ConfirmPayResponse(
    @SerializedName("uuid")
    val uuid: String,
    @SerializedName("schedulingInfo")
    val schedulingInfo: SchedulingInfo?,
)