package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName

data class TransferResponse(
    @SerializedName("status")
    val status: String,
    @SerializedName("transactionId")
    val transactionId: String,
    @SerializedName("scheduleDate")
    val scheduleDate: String?,
    @SerializedName("dateTime")
    val dateTime: String?,
    @SerializedName("httpStatusCode")
    val httpStatusCode: Int?
)