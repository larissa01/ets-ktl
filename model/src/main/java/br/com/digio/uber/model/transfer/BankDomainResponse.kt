package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName

/**
 * @author Marlon D. Rocha
 * @since 30/10/20
 */
data class BankDomainResponse(
    @SerializedName("banks")
    val bankDomains: ArrayList<BankDomain>
)

data class BankDomain(
    @SerializedName("name")
    val name: String,
    @SerializedName("code")
    val code: String
)