package br.com.digio.uber.model.store

import com.google.gson.annotations.SerializedName

data class StoreOrderResponse(
    @SerializedName("id")
    val id: Long,
    @SerializedName("customerId")
    val customerId: Long,
    @SerializedName("customerDocument")
    val customerDocument: String? = null,
    @SerializedName("value")
    val value: Float? = 0f,
    @SerializedName("installments")
    val installments: Int? = null,
    @SerializedName("quantity")
    val quantity: Int? = null,
    @SerializedName("orderStatusDescription")
    val orderStatusDescription: String? = null,
    @SerializedName("orderStatus")
    val orderStatus: OrderStatusEnum? = null,
    @SerializedName("orderCode")
    val orderCode: String? = null,
    @SerializedName("paymentStatus")
    val paymentStatus: PaymentRequestStatusEnum? = null,
    @SerializedName("partnerStatus")
    val partnerStatus: PartnerRequestStatusEnum? = null,
    @SerializedName("history")
    val history: List<StoreOrderhistory> = listOf(),
    @SerializedName("inputDate")
    val inputDate: String? = null,
    @SerializedName("lastUpdatedDate")
    val lastUpdatedDate: String? = null,
    @SerializedName("category")
    val category: ProductCategory? = null,
    @SerializedName("orderDetail")
    val orderDetail: Map<String, String>? = null,
    @SerializedName("usageInstructions")
    val usageInstructions: String? = null,
    @SerializedName("activationCode")
    val activationCode: String? = null,
    @SerializedName("shareText")
    val shareText: String? = null,
    @SerializedName("message")
    val message: String? = null,
    @SerializedName("statusColor")
    val statusColor: String? = null
)

enum class PaymentRequestStatusEnum {
    PENDING, AUTHORIZED, ERROR, CANCELLED, TIMEOUT, CONFIRMED
}

enum class PartnerRequestStatusEnum {
    PENDING, AUTHORIZED, RETRY, TO_REVERSE, REVERSED, NOT_REVERSED, ERROR, CANCELLED
}

enum class OrderStatusEnum(val statusDescription: String) {
    PENDING("Solicitado"),
    WAITING_PAYMENT_ACQUIRER("Solicitado"),
    WAITING_PAYMENT_PARTNER("Aguardando Aprovação"),
    CANCELED("Cancelado"),
    CANCELED_BLACKLIST("Cancelado"),
    CONFIRMED("Concluído"),
    WAITING_REFUND_ACQUIRER("Em Cancelamento"),
    WAITING_CANCELED_ACQUIRER("Aguardando Aprovação"),
    REFUNDED("Estornado"),
    ERROR_PAYMENT_PROCESS("Cancelado"),
    PENDING_REFUND("Em Cancelamento"),
    WAITING_CONFIRMATION("Aguardando Aprovação"),
    ERROR_INSURANCE("Cancelado"),
    ERROR_LOYALTY("Cancelado"),
    ERROR_MOBILITY("Cancelado"),
    ERROR_EGIFT("Cancelado"),
    PROCESSING_CANCELLATION("Processando Cancelamento");
}