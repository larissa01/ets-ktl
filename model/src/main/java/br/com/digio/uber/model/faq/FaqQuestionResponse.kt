package br.com.digio.uber.model.faq

import com.google.gson.annotations.SerializedName

data class FaqQuestionResponse(
    @SerializedName("id")
    val id: String?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("tag")
    val tag: String?,
    @SerializedName("is_frequent_question")
    val isFrequentQuestion: Boolean?,
    @SerializedName("body")
    val body: String?,
    @SerializedName("text_button")
    val textButton: String?,
    @SerializedName("text_help_button")
    val textHelpButton: String?,
    @SerializedName("type_button")
    val typeButton: String?,
    @SerializedName("semantics")
    val semantics: String?,
    @SerializedName("external_link")
    val externalLink: String?,
    @SerializedName("helpful")
    val helpful: Boolean?,
    @SerializedName("client")
    val client: String?
)