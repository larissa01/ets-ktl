package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName

data class PixBankDomainResponse(
    @SerializedName("institutions")
    val bankDomains: ArrayList<PixBankDomain>
)