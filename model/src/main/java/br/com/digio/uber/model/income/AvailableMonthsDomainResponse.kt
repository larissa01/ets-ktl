package br.com.digio.uber.model.income

data class AvailableMonthsDomainResponse(
    val yearMonths: List<String>
)