package br.com.digio.uber.model.card

import com.google.gson.annotations.SerializedName

@Suppress("LongParameterList")
class VirtualCardResponse(
    @SerializedName("accountId")
    val accountId: Int,
    @SerializedName("cardNumber")
    val cardNumber: String,
    @SerializedName("cardholderName")
    val cardholderName: String? = null,
    @SerializedName("cvv2")
    val cvv2: String? = null,
    @SerializedName("expiry")
    val expiry: Expiry,
    @SerializedName("flagDebitTokenization")
    val flagDebitTokenization: Boolean? = false,
    @SerializedName("id")
    val id: Long,
    @SerializedName("isVirtual")
    val isVirtual: Boolean,
    @SerializedName("status")
    val status: String,
    @SerializedName("statusId")
    val statusId: Int
)