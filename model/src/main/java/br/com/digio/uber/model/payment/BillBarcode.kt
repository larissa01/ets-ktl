package br.com.digio.uber.model.payment

import br.com.digio.uber.model.receipt.Receipt
import com.google.gson.annotations.SerializedName

data class BillBarcode(
    @SerializedName("uuid")
    val uuid: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("dueDate")
    val dueDate: String,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("barcode")
    val barcode: String,
    @SerializedName("amount")
    val amount: Double,
    @SerializedName("infoNormal")
    val infoNormal: Receipt.InfoNormal? = null,
    @SerializedName("infoNPC")
    val infoNPC: Receipt.InfoNPC? = null,
    @SerializedName("availableTransactionModes")
    val availableTransactionModes: List<String>,
    @SerializedName("dateToSchedule")
    val dateToSchedule: String,
    @SerializedName("scheduledDate")
    val scheduledDate: String?
)