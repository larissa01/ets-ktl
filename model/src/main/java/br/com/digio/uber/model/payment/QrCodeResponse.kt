package br.com.digio.uber.model.payment

import com.google.gson.annotations.SerializedName

data class QrCodeResponse(
    @SerializedName("amount")
    val amount: Double,
    @SerializedName("cardBlocked")
    val cardBlocked: Boolean,
    @SerializedName("date")
    val date: String,
    @SerializedName("installments")
    val installments: Int,
    @SerializedName("merchantName")
    val merchantName: String,
    @SerializedName("mode")
    val mode: String,
    @SerializedName("time")
    val time: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("uuid")
    val uuid: String
)