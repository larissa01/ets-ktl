package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class InternalTransferRequest(
    @SerializedName("amount")
    var amount: BigDecimal?,
    @SerializedName("beneficiary")
    var beneficiary: Beneficiary?,
    @SerializedName("comment")
    var comment: String?,
    @SerializedName("password")
    var password: String?,
    @SerializedName("saveBeneficiary")
    var saveBeneficiary: Boolean?,
    @SerializedName("scheduleDate")
    var scheduleDate: String?
)

data class Beneficiary(
    @SerializedName("accountBranch")
    var accountBranch: String?,
    @SerializedName("accountNumber")
    var accountNumber: String?,
    @SerializedName("cpf")
    var cpf: String?,
    @SerializedName("name")
    var name: String?
)