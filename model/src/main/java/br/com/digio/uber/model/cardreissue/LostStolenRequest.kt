package br.com.digio.uber.model.cardreissue

import com.google.gson.annotations.SerializedName

data class LostStolenRequest(
    @SerializedName("cancelStatus")
    val cancelStatus: Int = 1,
    @SerializedName("cardId")
    val cardId: Int,
    @SerializedName("chargeType")
    val chargeType: String,
    @SerializedName("isCancelCard")
    val isCancelCard: Boolean = false,
    @SerializedName("isChargeCard")
    val isChargeCard: Boolean = false
)

@Suppress("MagicNumber")
enum class LostStolenRequestStatusCancel(val id: Int, val description: String) {
    CANCELADO_ROUBO(5, "Cancelado Roubo"),
    CANCELADO_PERDA(4, "Cancelado Perda"),
    CANCELADO_CLIENTE(6, "Cancelado Cliente"),
    CANCELADO_SUSP_FALSO(12, "Cancelado Susp.Falso")
}