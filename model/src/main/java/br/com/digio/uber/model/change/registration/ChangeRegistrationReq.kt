package br.com.digio.uber.model.change.registration

import com.google.gson.annotations.SerializedName

data class ChangeRegistrationReq(
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("grossMonthlyIncome")
    val grossMonthlyIncome: Float? = null,
    @SerializedName("mobileNumber")
    val mobileNumber: String? = null
)