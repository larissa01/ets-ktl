package br.com.digio.uber.model.login

import br.com.digio.uber.model.request.DeviceInfo
import com.google.gson.annotations.SerializedName

data class LoginReq(
    @SerializedName("deviceInfo")
    val deviceInfo: DeviceInfo,
    @SerializedName("password")
    val password: String,
    @SerializedName("pidInfo")
    val pidInfo: PidInfo? = null,
    @SerializedName("riskAnalysisInfo")
    val riskAnalysisInfo: RiskAnalysisInfo? = null,
    @SerializedName("tokenizationInfo")
    val tokenizationInfo: TokenizantionInfo? = null,
    @SerializedName("username")
    val userName: String
)