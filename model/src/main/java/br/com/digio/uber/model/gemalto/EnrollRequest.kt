package br.com.digio.uber.model.gemalto

import br.com.digio.uber.model.request.DeviceInfo
import com.google.gson.annotations.SerializedName

data class EnrollRequest(
    @SerializedName("deviceInfo")
    val deviceInfo: DeviceInfo
)