package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName

data class PixBankDomain(
    @SerializedName("name")
    val name: String,
    @SerializedName("identification")
    val code: String
)