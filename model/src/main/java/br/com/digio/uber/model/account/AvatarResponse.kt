package br.com.digio.uber.model.account

import com.google.gson.annotations.SerializedName

data class AvatarResponse(
    @SerializedName("type")
    val type: String,
    @SerializedName("base64")
    val base64: String
)