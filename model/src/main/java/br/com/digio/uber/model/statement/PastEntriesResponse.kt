package br.com.digio.uber.model.statement

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class PastEntriesResponse(
    @SerializedName("isInitial")
    val isInitial: Boolean? = null,
    @SerializedName("data")
    val data: List<Data>
)

data class Data(
    @SerializedName("balance")
    val balance: BigDecimal?,
    @SerializedName("date")
    val date: String,
    @SerializedName("entries")
    val entries: List<Entry>
)

data class Entry(
    @SerializedName("amount")
    val amount: BigDecimal,
    @SerializedName("category")
    val category: String?,
    @SerializedName("entryDate")
    val entryDate: String,
    @SerializedName("creditDate")
    val creditDate: String,
    @SerializedName("description")
    val description: String?,
    @SerializedName("originalDescription")
    val originalDescription: String?,
    @SerializedName("entryName")
    val entryName: String,
    @SerializedName("hasAttachment")
    val hasAttachment: Boolean,
    @SerializedName("hasDetails")
    val hasDetails: Boolean,
    @SerializedName("id")
    val id: String?,
    @SerializedName("type")
    val type: String
)