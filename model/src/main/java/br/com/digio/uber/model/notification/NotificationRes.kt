package br.com.digio.uber.model.notification

import com.google.gson.annotations.SerializedName

data class NotificationRes(
    @SerializedName("message")
    val message: String,
    @SerializedName("payload")
    val payload: String?,
    @SerializedName("title")
    val title: String
)