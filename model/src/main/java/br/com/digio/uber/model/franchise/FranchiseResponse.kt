package br.com.digio.uber.model.franchise

import com.google.gson.annotations.SerializedName

class FranchiseResponse(
    @SerializedName("uuid")
    val uuid: String,
    @SerializedName("productType")
    val productType: String,
    @SerializedName("code")
    val code: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("transactionalLimits")
    val transactionalLimits: List<FranchiseLimitsData>
)