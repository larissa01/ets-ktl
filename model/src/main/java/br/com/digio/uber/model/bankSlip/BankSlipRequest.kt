package br.com.digio.uber.model.bankSlip

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * @author Marlon D. Rocha
 * @since 16/10/20
 */
data class BankSlipRequest(
    @SerializedName("amount")
    val amount: BigDecimal,
    @SerializedName("description")
    val description: String
)