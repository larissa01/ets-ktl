package br.com.digio.uber.model.digioTests

import com.google.gson.annotations.SerializedName

data class Spotlight(
    @SerializedName("bannerURL")
    val bannerURL: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("name")
    val name: String
)