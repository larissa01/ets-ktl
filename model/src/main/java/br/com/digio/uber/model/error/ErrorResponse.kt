package br.com.digio.uber.model.error

import com.google.gson.annotations.SerializedName

data class ErrorResponse<T>(
    @SerializedName("statusError")
    val statusError: String? = null,
    @SerializedName("path")
    val path: String? = null,
    @SerializedName("statusCode")
    val statusCode: Int? = null,
    @SerializedName("error")
    val error: BackendError<T>? = null
)

data class BackendError<T>(
    @SerializedName("code")
    val code: String? = null,
    @SerializedName("message")
    val message: String? = null,
    @SerializedName("data")
    val data: T?
)