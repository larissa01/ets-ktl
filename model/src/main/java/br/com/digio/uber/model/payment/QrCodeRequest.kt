package br.com.digio.uber.model.payment

import com.google.gson.annotations.SerializedName

data class QrCodeRequest(
    @SerializedName("qrCode")
    val qrCode: String
)