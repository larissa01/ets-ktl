package br.com.digio.uber.model.cashback

enum class Tiers constructor(var tier: String) {
    DEFAULT("Default Profile"),
    TIER_1("Azul"),
    TIER_2("Ouro"),
    TIER_3("Platina"),
    TIER_4("Diamante"),
    COURIER("Verde")
}