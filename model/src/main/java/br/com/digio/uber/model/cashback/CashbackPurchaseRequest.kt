package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName

data class CashbackPurchaseRequest(
    @SerializedName("id")
    val id: Long? = null,
    @SerializedName("storeIdentifierId")
    val storeIdentifierId: Long? = null,
    @SerializedName("partnerId")
    val partnerId: Long? = null
)