package br.com.digio.uber.model.card

enum class CardSituation {
    VALID,
    BLOCKED_CARD,
    BLOCKED_ACCOUNT,
    BLOCKED_CARD_INVALID_PASSWORD,
    BLOCKED_CARD_OVERDUE,
    BLOCKED_CARD_FRAUD,
    BLOCKED_CARD_CHANGE_REGISTRATION_DATA,
    ERROR_ON_SIMULATION,
    UNKNOWN,
    CARD_NOT_FOUND;

    companion object {
        fun getSituationCard(cardStatus: CardStatus): CardSituation {
            return when (cardStatus) {
                CardStatus.NORMAL -> VALID
                CardStatus.BLOQUEADO, CardStatus.BLOQUEADO_PREVENCAO -> BLOCKED_CARD
                CardStatus.SUSPEITA_DE_FRAUDE_PREVENTIVO,
                CardStatus.BLOQUEIO_PREVENTIVO_FALCON -> BLOCKED_CARD_FRAUD
                CardStatus.SUSPEITA_DE_FRAUDE_CADASTRAL,
                CardStatus.BLOQUEIO_INFO_INVALIDAS -> BLOCKED_CARD_CHANGE_REGISTRATION_DATA
                CardStatus.BLOQUEADO_SENHA_INCORRETA -> BLOCKED_CARD_INVALID_PASSWORD
                else -> UNKNOWN
            }
        }
    }
}