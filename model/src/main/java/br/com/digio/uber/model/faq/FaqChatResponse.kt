package br.com.digio.uber.model.faq

import com.google.gson.annotations.SerializedName

data class FaqChatResponse(@SerializedName("url") val url: String)