package br.com.digio.uber.model.resetpassword

import com.google.gson.annotations.SerializedName

data class ResetPasswordRes(
    @SerializedName("email")
    val email: String?
)