package br.com.digio.uber.model.change.registration

import com.google.gson.annotations.SerializedName

data class ZipCodeRes constructor(
    @SerializedName("zipCode")
    val zipCode: String? = null,
    @SerializedName("state")
    val state: String? = null,
    @SerializedName("street")
    val street: String? = null,
    @SerializedName("neighborhood")
    val neighborhood: String? = null,
    @SerializedName("city")
    val city: String? = null
)