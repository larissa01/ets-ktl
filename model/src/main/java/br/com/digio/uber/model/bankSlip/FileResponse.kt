package br.com.digio.uber.model.bankSlip

import com.google.gson.annotations.SerializedName

/**
 * @author Marlon D. Rocha
 * @since 16/10/20
 */
data class FileResponse(
    @SerializedName("type")
    val type: String,
    @SerializedName("base64")
    val base64: String
)