package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.math.BigDecimal

data class CashbackPurchase(
    @SerializedName("id")
    val id: Long? = null,
    @SerializedName("storeId")
    val storeId: Long? = null,
    @SerializedName("storeIdentifierId")
    val storeIdentifierId: Long? = null,
    @SerializedName("storeName")
    val storeName: String? = null,
    @SerializedName("partnerId")
    val partnerId: Long? = null,
    @SerializedName("image")
    val image: String? = null,
    @SerializedName("amount")
    val amount: BigDecimal? = null,
    @SerializedName("transactionValue")
    val transactionValue: BigDecimal? = null,
    @SerializedName("customerDiscount")
    val customerDiscount: BigDecimal? = null,
    @SerializedName("transactionCurrency")
    val transactionCurrency: String? = null,
    @SerializedName("typeDiscount")
    val typeDiscount: TypeDiscount? = null,
    @SerializedName("transactionDate")
    val transactionDate: String? = null,
    @SerializedName("color")
    val color: String? = null,
    @SerializedName("status")
    val status: String? = null,
    @SerializedName("formReceipt")
    val formReceipt: String? = null,
    @SerializedName("creditDate")
    val creditDate: String? = null,
    @SerializedName("checked")
    var checked: Boolean = false
) : Serializable

enum class MyCashbackStatus {
    RELEASED,
    WAITING_APPROVED,
    RECEIVED,
    CANCELED,
    LOST
}