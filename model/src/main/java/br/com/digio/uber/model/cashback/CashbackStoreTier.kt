package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CashbackStoreTier(
    @SerializedName("tierCode")
    val tierCode: Tiers,
    @SerializedName("tierDescription")
    val tierDescription: String,
    @SerializedName("minTierCustomerCommission")
    val minTierCustomerCommission: Float,
    @SerializedName("maxTierCustomerCommission")
    val maxTierCustomerCommission: Float,
    @SerializedName("tierColor")
    val tierColor: String
) : Serializable