package br.com.digio.uber.model.statement

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class FutureEntriesResponse(
    @SerializedName("data")
    val futureData: List<FutureDataResponse>
)

data class FutureDataResponse(
    @SerializedName("date")
    val date: String,
    @SerializedName("entries")
    val futureEntriesResponse: List<FutureEntryResponse>
)

data class FutureEntryResponse(
    @SerializedName("id")
    val id: String?,
    @SerializedName("amount")
    val amount: BigDecimal,
    @SerializedName("category")
    val category: String?,
    @SerializedName("date")
    val date: String,
    @SerializedName("description")
    val description: String?,
    @SerializedName("entryName")
    val entryName: String,
    @SerializedName("hasAttachment")
    val hasAttachment: Boolean,
    @SerializedName("hasDetails")
    val hasDetails: Boolean
)