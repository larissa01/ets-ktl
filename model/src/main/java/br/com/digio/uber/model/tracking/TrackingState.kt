package br.com.digio.uber.model.tracking

import com.google.gson.annotations.SerializedName

data class TrackingState(
    @SerializedName("date")
    val date: String?,
    @SerializedName("description")
    val description: String,
    @SerializedName("stage")
    val stage: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: Int
) {
    companion object {
        const val INACTIVE: Int = 0
        const val FINISHED: Int = 1
        const val CURRENT: Int = 2
        const val FAILURE: Int = 3
        const val LAST: Int = 100
        const val HIDE: Int = 404
    }
}