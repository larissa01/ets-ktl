package br.com.digio.uber.model.login

import com.google.gson.annotations.SerializedName

data class LoginRes(
    @SerializedName("accessToken")
    val accessToken: String,
    @SerializedName("refreshToken")
    val refreshToken: String,
    @SerializedName("accessExpiresIn")
    val accessExpiresIn: Long,
    @SerializedName("refreshExpiresIn")
    val refreshExpiresIn: Long,
    @SerializedName("tokenType")
    val tokenType: String,
    @SerializedName("userSecurityValidationTypes")
    val userSecurityValidationTypes: List<String>
)