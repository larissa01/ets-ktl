package br.com.digio.uber.model.gemalto

import com.google.gson.annotations.SerializedName

data class Enroll(
    @SerializedName("pin")
    val pin: String,
    @SerializedName("registrationCode")
    val registrationCode: String
)