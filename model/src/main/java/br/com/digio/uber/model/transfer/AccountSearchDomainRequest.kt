package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName

data class AccountSearchDomainRequest(
    @SerializedName("agency")
    val agency: String,
    @SerializedName("account")
    val account: String,
    @SerializedName("digit")
    val digit: String
)