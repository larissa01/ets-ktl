package br.com.digio.uber.model.config

import com.google.gson.annotations.SerializedName

data class ConfigFeature(
    @SerializedName("version")
    val vesion: String,
    @SerializedName("tag_list")
    val tagList: List<FeatureItem>,
    @SerializedName("feature_list")
    val featureList: List<FeatureItem>,
    @SerializedName("shortcut_List")
    val shortcutList: List<FeatureItem>,
    @SerializedName("shortcut_tracking_card_list")
    val shortcutTrackingCardList: List<FeatureItem>
)