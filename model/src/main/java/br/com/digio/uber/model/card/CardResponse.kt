package br.com.digio.uber.model.card

import com.google.gson.annotations.SerializedName

data class CardResponse constructor(
    @SerializedName("cardId")
    val cardId: Long,
    @SerializedName("cardMaskedNumber")
    val cardMaskedNumber: String?,
    @SerializedName("cardSequential")
    val cardSequential: Int?,
    @SerializedName("expirationDate")
    val expirationDate: String?,
    @SerializedName("isVirtualCard")
    val isVirtualCard: Int?,
    @SerializedName("issueDate")
    val issueDate: String?,
    @SerializedName("personId")
    val personId: Int?,
    @SerializedName("productId")
    val productId: Int?,
    @SerializedName("productName")
    val productName: String,
    @SerializedName("productType")
    val stageId: Int,
    @SerializedName("statusId")
    val statusId: Long,
    @SerializedName("accountId")
    val accountId: Long?
)