package br.com.digio.uber.model.bankSlip

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * @author Marlon D. Rocha
 * @since 16/10/20
 */
data class GeneratedBankSlipResponse(
    @SerializedName("data")
    val data: List<Data>
) {
    data class Data(
        @SerializedName("amount")
        val amount: BigDecimal,
        @SerializedName("description")
        val description: String?,
        @SerializedName("dueDate")
        val dueDate: String,
        @SerializedName("issueDate")
        val issueDate: String,
        @SerializedName("status")
        val status: String,
        @SerializedName("typeableLine")
        val typeableLine: String,
        @SerializedName("uuid")
        val uuid: String
    )
}