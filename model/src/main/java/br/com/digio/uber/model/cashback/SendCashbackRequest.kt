package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName

data class SendCashbackRequest(
    @SerializedName("receiptForm")
    val receiptForm: String,
    @SerializedName("purchases")
    val purchases: List<CashbackPurchaseRequest>
)