package br.com.digio.uber.model.card

import com.google.gson.annotations.SerializedName

data class CardModel(
    @SerializedName("id")
    var id: Long,
    @SerializedName("personId")
    var personId: Long,
    @SerializedName("statusId")
    var statusId: Int,
    @SerializedName("cardNumber")
    var cardNumber: String? = null,
    @SerializedName("cardholderName")
    var cardholderName: String? = null,
    @SerializedName("isVirtual")
    var isVirtual: Boolean,
    @SerializedName("productId")
    var productId: Long
)