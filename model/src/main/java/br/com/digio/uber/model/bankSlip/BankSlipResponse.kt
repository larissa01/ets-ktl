package br.com.digio.uber.model.bankSlip

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * @author Marlon D. Rocha
 * @since 16/10/20
 */
data class BankSlipResponse(
    @SerializedName("uuid")
    val uuid: String,
    @SerializedName("typeableLine")
    val typeableLine: String,
    @SerializedName("dueDate")
    val dueDate: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("amount")
    val amount: BigDecimal
)