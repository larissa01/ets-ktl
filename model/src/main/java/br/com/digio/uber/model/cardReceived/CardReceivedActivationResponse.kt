package br.com.digio.uber.model.cardReceived

import com.google.gson.annotations.SerializedName

data class CardReceivedActivationResponse(
    @SerializedName("externalIdentifier")
    val externalIdentifier: String?,
    @SerializedName("status")
    val status: String?
)