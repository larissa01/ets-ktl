package br.com.digio.uber.model.home

import com.google.gson.annotations.SerializedName

data class Balance(
    @SerializedName("balance")
    val balance: Double,
    @SerializedName("blockedBalance")
    val blockedBalance: Int
)