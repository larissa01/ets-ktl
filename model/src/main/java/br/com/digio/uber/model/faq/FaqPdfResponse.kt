package br.com.digio.uber.model.faq

import java.io.InputStream

data class FaqPdfResponse(
    val isSuccess: Boolean,
    val pdfInputStream: InputStream? = null
)