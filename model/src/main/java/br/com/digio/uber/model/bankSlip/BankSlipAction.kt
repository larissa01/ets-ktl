package br.com.digio.uber.model.bankSlip

import java.io.File

sealed class BankSlipAction {

    data class Share(val pdfFile: File) : BankSlipAction()
    data class Copy(val transferText: String) : BankSlipAction()
    data class SeeBankSlip(val pdfFile: File) : BankSlipAction()
}