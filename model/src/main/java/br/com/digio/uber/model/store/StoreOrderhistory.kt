package br.com.digio.uber.model.store

import com.google.gson.annotations.SerializedName

data class StoreOrderhistory(
    @SerializedName("timestamp")
    val timestamp: Long?,
    @SerializedName("detail")
    val detail: String?
)