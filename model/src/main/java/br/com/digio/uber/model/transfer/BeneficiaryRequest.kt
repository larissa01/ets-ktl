package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName

data class BeneficiaryRequest(
    @SerializedName("accountNumber")
    val accountNumber: String,
    @SerializedName("branch")
    val branch: String,
    @SerializedName("transferAccountType")
    val transferAccountType: String
)