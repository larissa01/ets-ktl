package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class ExternalTransferRequest(
    @SerializedName("accountBranch")
    val accountBranch: String,
    @SerializedName("accountHolderDocument")
    val accountHolderDocument: String? = null,
    @SerializedName("accountHolderName")
    val accountHolderName: String? = null,
    @SerializedName("accountNumber")
    val accountNumber: String,
    @SerializedName("accountType")
    val accountType: String,
    @SerializedName("amount")
    val amount: BigDecimal,
    @SerializedName("bankCode")
    val bankCode: String,
    @SerializedName("comment")
    val comment: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("scheduleDate")
    val scheduleDate: String?,
    @SerializedName("saveBeneficiary")
    val saveBeneficiary: Boolean
)