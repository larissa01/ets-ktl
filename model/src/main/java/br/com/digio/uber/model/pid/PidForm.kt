package br.com.digio.uber.model.pid

import com.google.gson.annotations.SerializedName

data class PidForm(
    @SerializedName("crypto")
    val crypto: Boolean,
    @SerializedName("errorMessage")
    val errorMessage: String?,
    @SerializedName("fieldLength")
    val fieldLength: Int?,
    @SerializedName("hint")
    val title: String,
    @SerializedName("index")
    val index: Int,
    @SerializedName("key")
    val key: String,
    @SerializedName("mask")
    val mask: String?,
    @SerializedName("placeholder")
    val placeholder: String?,
    @SerializedName("regex")
    val regex: String?,
    @SerializedName("required")
    val required: Boolean,
    @SerializedName("type")
    val type: String?,
    @SerializedName("value")
    val value: String?
)