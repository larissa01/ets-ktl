package br.com.digio.uber.model.faq

import com.google.gson.annotations.SerializedName

data class FaqSubjectResponse(
    @SerializedName("id")
    val id: String?,
    @SerializedName("subject")
    val subject: String?,
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("client")
    val client: String?,
    @SerializedName("questions")
    val questions: List<FaqQuestionResponse>
)