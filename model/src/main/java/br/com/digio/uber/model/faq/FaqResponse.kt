package br.com.digio.uber.model.faq

import com.google.gson.annotations.SerializedName

data class FaqResponse(
    @SerializedName("update") val update: Boolean,
    @SerializedName("faqVersion") val faqVersion: String?,
    @SerializedName("faq") val faqContent: List<FaqSubjectResponse>?
)