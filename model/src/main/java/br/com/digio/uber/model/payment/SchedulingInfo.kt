package br.com.digio.uber.model.payment

import com.google.gson.annotations.SerializedName

data class SchedulingInfo(
    @SerializedName("schedulingDate")
    val schedulingDate: String,
    @SerializedName("wasRescheduled")
    val wasRescheduled: Boolean,
    @SerializedName("receiptId")
    val receiptId: String,
)