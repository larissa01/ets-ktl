package br.com.digio.uber.model.remuneration

import com.google.gson.annotations.SerializedName

data class IncomeIndexResponse(

    @SerializedName("incomeIndex")
    val incomeIndex: String? = "",

    @SerializedName("isAvailable")
    val isAvailable: Boolean?

)