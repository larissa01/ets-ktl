package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName

data class ThirdPartyAccountResponse(
    @SerializedName("accountBranch")
    val accountBranch: String,
    @SerializedName("accountNumber")
    val accountNumber: String,
    @SerializedName("avatar")
    val avatar: AvatarResponse,
    @SerializedName("cpf")
    val cpf: String,
    @SerializedName("fullName")
    val fullName: String,
    @SerializedName("productType")
    val productType: String
)

data class AvatarResponse(
    @SerializedName("type")
    val type: String,
    @SerializedName("base64")
    val base64: String
)