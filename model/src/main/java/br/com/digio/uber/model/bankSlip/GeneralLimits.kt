package br.com.digio.uber.model.bankSlip

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * @author Marlon D. Rocha
 * @since 16/10/20
 */
data class GeneralLimits(
    @SerializedName("maxAmount")
    val maxAmount: BigDecimal,
    @SerializedName("maxQtdPerDay")
    val maxQtdPerDay: Int,
    @SerializedName("maxQtdOverlimit")
    val maxQtdOverlimit: Int,
    @SerializedName("maxQtdPerMonth")
    val maxQtdPerMonth: Int,
    @SerializedName("minAmount")
    val minAmount: BigDecimal
)