package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName

data class CashbackComissionCategory(
    @SerializedName("commissionGroup")
    val commissionGroup: String?,
    @SerializedName("commission")
    val commission: Float
)