package br.com.digio.uber.model.cardreissue

import com.google.gson.annotations.SerializedName

data class LostStolenResponse(
    @SerializedName("accountId")
    val accountId: Int,
    @SerializedName("cardId")
    val cardId: Int,
    @SerializedName("cpf")
    val cpf: String,
    @SerializedName("document")
    val document: String,
    @SerializedName("personId")
    val personId: Int,
    @SerializedName("productType")
    val productType: Any
)