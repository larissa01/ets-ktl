package br.com.digio.uber.model.account

import com.google.gson.annotations.SerializedName

data class GeolocationRequest(
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double
)