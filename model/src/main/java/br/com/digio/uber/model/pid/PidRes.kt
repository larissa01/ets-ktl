package br.com.digio.uber.model.pid

import com.google.gson.annotations.SerializedName

data class PidRes(
    @SerializedName("form")
    val pidForm: PidForm?,
    @SerializedName("hash")
    val hash: String?,
    @SerializedName("last")
    val last: Boolean?
)