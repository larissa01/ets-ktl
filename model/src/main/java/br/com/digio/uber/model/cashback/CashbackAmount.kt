package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName

data class CashbackAmount(
    @SerializedName("amount")
    val amount: Float?
)

enum class CashbackStatus {
    RELEASED,
    WAITING_APPROVED,
    RECEIVED,
    CANCELED,
    LOSTED
}