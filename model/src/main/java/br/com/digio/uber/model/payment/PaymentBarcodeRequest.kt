package br.com.digio.uber.model.payment

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class PaymentBarcodeRequest(

    @SerializedName("transactionIdentifier")
    val transactionIdentifier: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("amount")
    val amount: BigDecimal,
    @SerializedName("installmentIdentifier")
    val installmentIdentifier: String,
    @SerializedName("transactionMode")
    val transactionMode: String,
    @SerializedName("scheduleDate")
    val scheduleDate: String?
)