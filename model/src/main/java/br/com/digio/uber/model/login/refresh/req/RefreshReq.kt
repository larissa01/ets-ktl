package br.com.digio.uber.model.login.refresh.req

import com.google.gson.annotations.SerializedName

data class RefreshReq(
    @SerializedName("refreshToken")
    val refreshToken: String
)