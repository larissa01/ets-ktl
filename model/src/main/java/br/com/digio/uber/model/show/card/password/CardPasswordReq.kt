package br.com.digio.uber.model.show.card.password

import com.google.gson.annotations.SerializedName

data class CardPasswordReq(
    @SerializedName("cardId")
    val cardId: Long?,
    @SerializedName("hash")
    val hash: String
)