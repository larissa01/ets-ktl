package br.com.digio.uber.model.franchise

import com.google.gson.annotations.SerializedName

data class FranchiseLimitsData(
    @SerializedName("uuid")
    val uuid: String = "",
    @SerializedName("product")
    val product: String = "",
    @SerializedName("limitCategory")
    val limitCategory: String = "",
    @SerializedName("minQuantity")
    val minQuantity: Long = 0,
    @SerializedName("maxQuantityDay")
    val maxQuantityDay: Long = 0,
    @SerializedName("maxQuantityMonth")
    val maxQuantityMonth: String? = null,
    @SerializedName("maxQuantityTax")
    val maxQuantityTax: String? = null,
    @SerializedName("minAmount")
    val minAmount: Double = 0.0,
    @SerializedName("maxAmount")
    val maxAmount: Double = 0.0,
    @SerializedName("taxAmount")
    val taxAmount: Double = 0.0
)