package br.com.digio.uber.model.resetpassword

import com.google.gson.annotations.SerializedName

data class ResetPasswordReq(
    @SerializedName("birthDate")
    val birthDate: String,
    @SerializedName("username")
    val username: String
)