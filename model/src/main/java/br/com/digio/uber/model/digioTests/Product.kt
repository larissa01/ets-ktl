package br.com.digio.uber.model.digioTests

import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("description")
    val description: String,
    @SerializedName("imageURL")
    val imageURL: String,
    @SerializedName("name")
    val name: String
)