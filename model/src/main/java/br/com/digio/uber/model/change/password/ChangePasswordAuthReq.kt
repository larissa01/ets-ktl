package br.com.digio.uber.model.change.password

import com.google.gson.annotations.SerializedName

data class ChangePasswordAuthReq(
    @SerializedName("newPassword")
    val newPassword: String,
    @SerializedName("oldPassword")
    val oldPassword: String
)