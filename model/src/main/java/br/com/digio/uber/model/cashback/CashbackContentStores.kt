package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName

data class CashbackContentStores(
    @SerializedName("totalElements")
    val totalElements: Int? = null,
    @SerializedName("number")
    val number: Int? = null,
    @SerializedName("size")
    val size: Int? = null,
    @SerializedName("totalPages")
    val totalPages: Int? = null,
    @SerializedName("numberOfElements")
    val numberOfElements: Int? = null,
    @SerializedName("firstPage")
    val firstPage: Boolean? = null,
    @SerializedName("hasPreviousPage")
    val hasPreviousPage: Boolean? = null,
    @SerializedName("hasNextPage")
    val hasNextPage: Boolean? = null,
    @SerializedName("hasContent")
    val hasContent: Boolean? = null,
    @SerializedName("first")
    val first: Boolean? = null,
    @SerializedName("last")
    val last: Boolean? = true,
    @SerializedName("nextPage")
    val nextPage: Int? = null,
    @SerializedName("previousPage")
    val previousPage: Int? = null,
    @SerializedName("content")
    val content: List<CashbackStore>?
)