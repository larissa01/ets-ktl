package br.com.digio.uber.model.pid

import com.google.gson.annotations.SerializedName

data class PidStepReq(
    @SerializedName("hash")
    val hash: String?,
    @SerializedName("value")
    val value: String?
)