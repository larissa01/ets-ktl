package br.com.digio.uber.model.account

data class AvatarRequest(val picture: String = "")