package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CashbackStore(
    @SerializedName("id")
    val id: Long,
    @SerializedName("identifierId")
    val identifierId: Long? = null,
    @SerializedName("storeName")
    val storeName: String? = null,
    @SerializedName("partnerId")
    val partnerId: Long? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("image")
    val image: String? = null,
    @SerializedName("customerCommission")
    val customerCommission: Float? = null,
    @SerializedName("typeDiscount")
    val typeDiscount: TypeDiscount? = null,
    @SerializedName("category")
    val category: CashbackStoreCategory? = null,
    @SerializedName("tierStoreInfo")
    val storeTierStoreInfo: CashbackStoreTier? = null,
    @SerializedName("nextTierStoreInfo")
    val nextStoreTierStoreInfo: CashbackStoreTier? = null
) : Serializable

enum class TypeDiscount {
    PERCENTAGE,
    AMOUNT
}