package br.com.digio.uber.model

data class UberFlagsPersistNotLoggedIn(
    val saveCpfInLogin: Boolean? = null
)