package br.com.digio.uber.model.store

import com.google.gson.annotations.SerializedName
import java.util.Date

data class StoreOrderMyRequests(
    @SerializedName("id")
    val id: Long,
    @SerializedName("customerId")
    val customerId: Long,
    @SerializedName("customerDocument")
    val customerDocument: String?,
    @SerializedName("value")
    val value: Float?,
    @SerializedName("installments")
    val installments: Int?,
    @SerializedName("quantity")
    val quantity: Int,
    @SerializedName("orderStatusDescription")
    val orderStatusDescription: String?,
    @SerializedName("orderStatus")
    val orderStatus: StoreOrderStatus,
    @SerializedName("orderCode")
    val orderCode: String?,
    @SerializedName("paymentStatus")
    val paymentStatus: String?,
    @SerializedName("partnerStatus")
    val partnerStatus: String?,
    @SerializedName("history")
    val history: List<StoreOrderhistory>?,
    @SerializedName("inputDate")
    val inputDate: Date?,
    @SerializedName("lastUpdatedDate")
    val lastUpdatedDate: Date,
    @SerializedName("category")
    val category: ProductCategory?,
    @SerializedName("orderDetail")
    val orderDetail: StoreOrder?,
    @SerializedName("usageInstructions")
    val usageInstructions: String?,
    @SerializedName("activationCode")
    val activationCode: String?,
    @SerializedName("shareText")
    val shareText: String?,
    @SerializedName("message")
    val message: String?
)

enum class StoreOrderStatus(val theme: String) {
    PENDING("Solicitado"),
    WAITING_PAYMENT_ACQUIRER("Solicitado"),
    WAITING_PAYMENT_PARTNER("Processando"),
    CANCELED("Cancelado"),
    CANCELED_BLACKLIST("Cancelado"),
    CONFIRMED("Concluído"),
    WAITING_REFUND_ACQUIRER("Em Cancelamento"),
    WAITING_CANCELED_ACQUIRER("Processando"),
    REFUNDED("Estornado"),
    ERROR_PAYMENT_PROCESS("Cancelado"),
    PENDING_REFUND("Em Cancelamento"),
    WAITING_CONFIRMATION("Processando"),
    ERROR_INSURANCE("Cancelado"),
    ERROR_LOYALTY("Cancelado"),
    ERROR_MOBILITY("Cancelado"),
    ERROR_EGIFT("Cancelado"),
    PROCESSING_CANCELLATION("Processando Cancelamento");
}