package br.com.digio.uber.model.show.card.password

import com.google.gson.annotations.SerializedName

data class CardPasswordRes(
    @SerializedName("password")
    val cardPassword: String
)