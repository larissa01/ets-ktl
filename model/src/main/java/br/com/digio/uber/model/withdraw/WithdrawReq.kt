package br.com.digio.uber.model.withdraw

import com.google.gson.annotations.SerializedName

data class WithdrawReq(
    @SerializedName("qrcode")
    val qrCode: String,
    @SerializedName("amount")
    val amount: Int,
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double,
    @SerializedName("password")
    val password: String
)