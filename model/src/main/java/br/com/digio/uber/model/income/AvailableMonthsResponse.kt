package br.com.digio.uber.model.income

import com.google.gson.annotations.SerializedName

data class AvailableMonthsResponse(
    @SerializedName("yearMonths")
    val yearMonths: List<String>
)