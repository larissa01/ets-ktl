package br.com.digio.uber.model.account

import com.google.gson.annotations.SerializedName

data class AccountResponse(
    @SerializedName("cpf")
    val cpf: String,
    @SerializedName("fullName")
    val fullName: String,
    @SerializedName("branch")
    val branch: String,
    @SerializedName("accountNumber")
    val accountNumber: String,
    @SerializedName("bank")
    val bank: String,
    @SerializedName("statusCode")
    val statusCode: String,
    @SerializedName("productType")
    val productType: String,
    @SerializedName("dateTime")
    val dateTime: String,
    @SerializedName("httpStatusCode")
    val httpStatusCode: Int
)