package br.com.digio.uber.model.digioTests

import com.google.gson.annotations.SerializedName

data class Cash(
    @SerializedName("bannerURL")
    val bannerURL: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("title")
    val title: String
)