package br.com.digio.uber.model.income

import java.math.BigDecimal

data class DetailDepositDomainResponse(
    var closed: Boolean,
    var currentAmount: String,
    var currentAmountVl: BigDecimal,
    var depositAmount: String,
    var depositAmountVl: BigDecimal,
    var depositEndDate: String,
    var depositId: Int,
    var depositStartDate: String,
    var incomeIndex: String,
    var iofValue: String,
    var iofValueVl: BigDecimal,
    var irValue: String,
    var irValueVl: BigDecimal,
    var grossIncome: String,
    var grossIncomeVl: BigDecimal,
    var remunerationType: String,
    var withdrawAmount: String,
    var withdrawAmountVl: BigDecimal,
    var liquidIncome: String?,
    var liquidIncomeVl: BigDecimal?
)