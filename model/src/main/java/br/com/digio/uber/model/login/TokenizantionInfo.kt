package br.com.digio.uber.model.login

import com.google.gson.annotations.SerializedName

data class TokenizantionInfo(
    @SerializedName("otp")
    val otp: String
)