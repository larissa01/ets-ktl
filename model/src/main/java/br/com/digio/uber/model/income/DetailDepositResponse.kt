package br.com.digio.uber.model.income

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class DetailDepositResponse(
    @SerializedName("depositId")
    val depositId: Int,
    @SerializedName("closed")
    val closed: Boolean,
    @SerializedName("depositStartDate")
    val depositStartDate: String,
    @SerializedName("depositEndDate")
    val depositEndDate: String,
    @SerializedName("depositAmount")
    val depositAmount: BigDecimal,
    @SerializedName("withdrawAmount")
    val withdrawAmount: BigDecimal,
    @SerializedName("currentAmount")
    val currentAmount: BigDecimal,
    @SerializedName("grossIncome")
    val grossIncome: BigDecimal,
    @SerializedName("irValue")
    val irValue: BigDecimal,
    @SerializedName("iofValue")
    val iofValue: BigDecimal,
    @SerializedName("incomeIndex")
    val incomeIndex: String,
    @SerializedName("remunerationType")
    val remunerationType: String,
    @SerializedName("liquidIncome")
    val liquidIncome: BigDecimal?
)