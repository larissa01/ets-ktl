package br.com.digio.uber.model.tracking

import com.google.gson.annotations.SerializedName

data class TrackingLogResponse(
    @SerializedName("expectedDeliveryDate")
    val expectedDeliveryDate: String,
    @SerializedName("trackingCode")
    val trackingCode: String?,
    @SerializedName("trackingStates")
    val trackingStates: List<TrackingState>
)