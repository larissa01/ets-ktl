package br.com.digio.uber.model.bankSlip

import com.google.gson.annotations.SerializedName

/**
 * @author Marlon D. Rocha
 * @since 16/10/20
 */
data class UserUsage(
    @SerializedName("qtdForMonth")
    val qtdForMonth: Int,
    @SerializedName("qtdForToday")
    val qtdForToday: Int
)