package br.com.digio.uber.model.store

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class StoreSubProduct constructor(
    @SerializedName("subproductId")
    val subproductId: Long,
    @SerializedName("subproductDetailId")
    val subproductDetailId: Long,
    @SerializedName("productType")
    val productType: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("nickname")
    val nickname: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("redemptionText")
    val redemptionText: String? = null,
    @SerializedName("image")
    val image: String? = null,
    @SerializedName("link")
    val link: String? = null,
    @SerializedName("contract")
    val contract: String? = null,
    @SerializedName("currency")
    val currency: String? = null,
    @SerializedName("value")
    val value: Float? = null,
    @SerializedName("commissionType")
    val commissionType: String? = null,
    @SerializedName("commissionValue")
    val commissionValue: Float? = null,
    @SerializedName("status")
    val status: StatusSubProduct? = null,
    @SerializedName("partnerProductId")
    val partnerProductId: String,
    @SerializedName("maxTagAmount")
    val maxTagAmount: Int
) : Serializable

enum class StatusSubProduct(val type: String) {
    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE");

    companion object {
        fun getByString(type: String?): StatusSubProduct? =
            values().firstOrNull { it.type == type }
    }
}