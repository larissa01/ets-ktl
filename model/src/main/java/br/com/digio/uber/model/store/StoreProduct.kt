package br.com.digio.uber.model.store

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class StoreProduct(
    @SerializedName("id")
    val id: Long,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("subtitle")
    val subtitle: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("image")
    val image: String? = null,
    @SerializedName("icon")
    val icon: String? = null,
    @SerializedName("category")
    val category: ProductCategory? = null,
    @SerializedName("spotlight")
    val spotlight: Boolean? = null,
    @SerializedName("priority")
    val priority: Int,
    @SerializedName("subproducts")
    val subproducts: List<StoreSubProduct>? = null
) : Serializable

enum class ProductCategory(val type: String) {
    EGIFT("Egift"),
    RECHARGE("Recarga de celular"),
    LINK("Link"),
    LIFE_INSURANCE("Seguro de vida"),
    CARDS("Cartões"),
    POINTS_PROGRAM("Programa de pontos"),
    CASHBACK("Cashback"),
    MOBILITY("Veloe");

    companion object {
        fun getByString(type: String?): ProductCategory? =
            values().firstOrNull { it.type == type }
    }
}