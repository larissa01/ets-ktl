package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName

data class BeneficiariesResponse(
    @SerializedName("beneficiaries")
    val beneficiaries: ArrayList<BeneficiaryResponse>
)