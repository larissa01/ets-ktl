package br.com.digio.uber.model.card

import com.google.gson.annotations.SerializedName

data class Expiry(
    @SerializedName("month")
    val month: String,
    @SerializedName("year")
    val year: String
)