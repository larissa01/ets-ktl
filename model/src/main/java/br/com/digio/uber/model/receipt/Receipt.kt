package br.com.digio.uber.model.receipt

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class Receipt(
    @SerializedName("identifier")
    val identifier: String,
    @SerializedName("operation")
    val operation: String,
    @SerializedName("authentication")
    val authentication: String,
    @SerializedName("transactionDate")
    val transactionDate: String,
    @SerializedName("schedulingDate")
    val schedulingDate: String?,
    @SerializedName("transactionValue")
    val transactionValue: BigDecimal,
    @SerializedName("transactionOperationType")
    val transactionOperationType: TransactionOperationType,
    @SerializedName("comment")
    val comment: String?,
    @SerializedName("statusTransaction")
    val statusTransaction: String,
    @SerializedName("debitant")
    val debitant: Debitant,
    @SerializedName("beneficiary")
    val beneficiary: Beneficiary?,
    @SerializedName("billData")
    val billData: BillData?,
    val description: String?
) {

    data class BillData(
        @SerializedName("type")
        val type: String,
        @SerializedName("dueDate")
        val dueDate: String,
        @SerializedName("barcode")
        val barcode: String,
        @SerializedName("transactionMode")
        val transactionMode: String,
        @SerializedName("status")
        val status: String,
        @SerializedName("infoNormal")
        val infoNormal: InfoNormal?,
        @SerializedName("infoNPC")
        val infoNPC: InfoNPC?
    )

    data class InfoNormal(
        @SerializedName("payeeName")
        val payeeName: String,
        @SerializedName("finalPayerName")
        val finalPayerName: String,
        @SerializedName("finalPayerDocument")
        val finalPayerDocument: String
    )

    data class InfoNPC(
        @SerializedName("payeeName")
        val payeeName: String,
        @SerializedName("finalPayerName")
        val finalPayerName: String,
        @SerializedName("finalPayerDocument")
        val finalPayerDocument: String,
        @SerializedName("payerName")
        val payerName: String,
        @SerializedName("payerDocument")
        val payerDocument: String,
        @SerializedName("payeeDocument")
        val payeeDocument: String,
        @SerializedName("computedValues")
        val computedValues: ComputedValues
    )

    data class ComputedValues(
        @SerializedName("calculatedInterestAmount")
        var calculatedInterestAmount: Double,
        @SerializedName("calculatedFineValue")
        var calculatedFineValue: Double,
        @SerializedName("discountValueCalculated")
        var discountValueCalculated: Double,
        @SerializedName("totalAmountToCharge")
        var totalAmountToCharge: Double,
        @SerializedName("paymentAmountUpdated")
        var paymentAmountUpdated: Double,
        @SerializedName("computedDate")
        var computedDate: String
    )

    data class Beneficiary(
        @SerializedName("account")
        val account: String?,
        @SerializedName("bankCode")
        val bankCode: String?,
        @SerializedName("bankDescription")
        val bankDescription: String,
        @SerializedName("branch")
        val branch: String?,
        @SerializedName("document")
        val document: String,
        @SerializedName("name")
        val name: String
    )

    data class Debitant(
        @SerializedName("account")
        val account: String?,
        @SerializedName("bankCode")
        val bankCode: String?,
        @SerializedName("bankDescription")
        val bankDescription: String,
        @SerializedName("branch")
        val branch: String?,
        @SerializedName("document")
        val document: String,
        @SerializedName("name")
        val name: String
    )

    data class TransactionOperationType(
        @SerializedName("code")
        val code: String,
        @SerializedName("type")
        val type: String,
        @SerializedName("goal")
        val goal: String
    )
}