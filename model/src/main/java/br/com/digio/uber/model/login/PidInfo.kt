package br.com.digio.uber.model.login

import com.google.gson.annotations.SerializedName

data class PidInfo(
    @SerializedName("facialRecognition")
    val facialRecognition: String? = null,
    @SerializedName("pidTrackingInfo")
    val pidTrackingInfo: PidTrackingInfo
)

data class PidTrackingInfo constructor(
    @SerializedName("requestId")
    var requestId: String,
    @SerializedName("requisitionHistoryId")
    var requisitionHistoryId: Int,
)