package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName

data class BeneficiaryResponse(
    @SerializedName("accountNumber")
    val accountNumber: String,
    @SerializedName("bankCode")
    val bankCode: String,
    @SerializedName("branch")
    val branch: String,
    @SerializedName("document")
    val document: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("personType")
    val personType: String,
    @SerializedName("registerType")
    val registerType: String,
    @SerializedName("sameOwnership")
    val sameOwnership: Boolean,
    @SerializedName("status")
    val status: String,
    @SerializedName("transferAccountType")
    val transferAccountType: String,
    @SerializedName("uuid")
    val uuid: String
)