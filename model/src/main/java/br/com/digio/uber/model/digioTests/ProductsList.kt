package br.com.digio.uber.model.digioTests

import com.google.gson.annotations.SerializedName

data class ProductsList(
    @SerializedName("cash")
    val cash: Cash,
    @SerializedName("products")
    val products: List<Product>,
    @SerializedName("spotlight")
    val spotlight: List<Spotlight>
)