package br.com.digio.uber.model.virtualCard

data class VirtualCardOnboarding(
    val screen: Int,
    val title: String,
    val description: String
)