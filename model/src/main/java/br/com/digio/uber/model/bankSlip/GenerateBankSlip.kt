package br.com.digio.uber.model.bankSlip

import java.math.BigDecimal
import java.util.Calendar

/**
 * @author Marlon D. Rocha
 * @since 15/10/20
 */
data class GenerateBankSlip(
    val availableBankSlips: Int,
    val dueDate: Calendar,
    val minimumValue: BigDecimal,
    val maximumValue: BigDecimal,
    val bankFeeValue: BigDecimal
)