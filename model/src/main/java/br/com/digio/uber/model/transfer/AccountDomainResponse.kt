package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName

/**
 * @author Marlon D. Rocha
 * @since 30/10/20
 */
data class AccountDomainResponse(
    @SerializedName("cpf")
    val cpf: String,
    @SerializedName("fullName")
    val fullName: String,
    @SerializedName("branch")
    val branch: String,
    @SerializedName("accountNumber")
    val accountNumber: String,
    @SerializedName("bank")
    val bank: String,
    @SerializedName("statusCode")
    val statusCode: String,
    @SerializedName("productType")
    val productType: String
)