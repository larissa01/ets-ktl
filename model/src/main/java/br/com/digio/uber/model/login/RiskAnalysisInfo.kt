package br.com.digio.uber.model.login

import com.google.gson.annotations.SerializedName

data class RiskAnalysisInfo(
    @SerializedName("sessionId")
    val sessionId: String
)