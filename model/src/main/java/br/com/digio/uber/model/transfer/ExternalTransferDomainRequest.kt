package br.com.digio.uber.model.transfer

data class ExternalTransferDomainRequest(
    val agency: String,
    val account: String,
    val accountType: String,
    val cpf: String,
    val name: String,
    val amount: String,
    val saveBeneficiary: Boolean,
    val description: String,
    val password: String,
    val date: String?,
    val bankCode: String,
    val isSameOwner: Boolean
)