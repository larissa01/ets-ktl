package br.com.digio.uber.model.virtualCard

data class VirtualCardOnboardingResponse(
    val onboardingList: List<VirtualCardOnboarding>
)