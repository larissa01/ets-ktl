package br.com.digio.uber.model.config

import com.google.gson.annotations.SerializedName

data class FeatureItem(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("label")
    val label: String,
    @SerializedName("visible")
    val visible: Boolean? = true,
    @SerializedName("image")
    val image: String? = "-1",
    @SerializedName("contentDescription")
    val contentDescription: String? = "",
    @SerializedName("group")
    val group: String? = ""

)