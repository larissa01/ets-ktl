package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName

data class CashbackDetailsResponse(
    @SerializedName("text")
    val text: String? = null
)