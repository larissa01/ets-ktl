package br.com.digio.uber.model.login

import com.google.gson.annotations.SerializedName

data class ErrorLexisLogin(
    @SerializedName("requestResult")
    val requestResult: String,
    @SerializedName("reviewStatus")
    val reviewStatus: String,
    @SerializedName("riskRating")
    val riskRating: String,
    @SerializedName("requestId")
    val requestId: String,
    @SerializedName("requisitionHistoryId")
    val requisitionHistoryId: String
)

data class ResponseErrorLexisLogin(
    @SerializedName("response")
    val response: ErrorLexisLogin
)