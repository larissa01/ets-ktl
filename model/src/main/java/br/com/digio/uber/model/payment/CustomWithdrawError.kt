package br.com.digio.uber.model.payment

import com.google.gson.annotations.SerializedName

data class CustomWithdrawError(
    @SerializedName("error")
    val error: ErrorContent,
    @SerializedName("path")
    val path: String,
    @SerializedName("statusCode")
    val statusCode: Int,
    @SerializedName("statusError")
    val statusError: String
) : RuntimeException()

data class ErrorContent(
    @SerializedName("code")
    val code: String,
    @SerializedName("data")
    val errorData: ErrorData?,
    @SerializedName("fields")
    val fields: Any?,
    @SerializedName("message")
    val message: String
)

data class ErrorData(
    @SerializedName("possibleAmounts")
    val possibleAmounts: List<Int>?
)