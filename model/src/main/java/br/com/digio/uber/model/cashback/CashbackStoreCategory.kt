package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CashbackStoreCategory(
    @SerializedName("id")
    val id: Long? = null,
    @SerializedName("name")
    val name: String? = null
) : Serializable