package br.com.digio.uber.model.transfer

data class FavoredListDomainResponse(
    val favored: List<FavoredDomainResponse>
)

data class FavoredDomainResponse(
    val uuid: String,
    val sameOwnership: Boolean,
    val branch: String,
    val accountNumber: String,
    val bankCode: String,
    val document: String,
    val name: String,
    val personType: String,
    val transferAccountType: String,
    val registerType: String,
    val status: String,
    val bankName: String
)