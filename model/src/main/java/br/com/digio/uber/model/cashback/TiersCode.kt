package br.com.digio.uber.model.cashback

enum class TiersCode constructor(var tier: String) {
    Azul("TIER_1"),
    Ouro("TIER_2"),
    Platina("TIER_3"),
    Diamante("TIER_4")
}