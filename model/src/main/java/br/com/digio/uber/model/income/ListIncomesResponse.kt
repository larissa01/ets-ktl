package br.com.digio.uber.model.income

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class ListIncomesResponse(
    @SerializedName("yearMonth")
    val yearMonth: String,
    @SerializedName("grossIncome")
    val grossIncome: BigDecimal,
    @SerializedName("netIncome")
    val netIncome: BigDecimal,
    @SerializedName("irValue")
    val irValue: BigDecimal,
    @SerializedName("iofValue")
    val iofValue: BigDecimal,
    @SerializedName("incomeIndex")
    val incomeIndex: String,
    @SerializedName("deposits")
    val deposits: List<Deposit>

)

data class Deposit(
    @SerializedName("yearMonth")
    val yearMonth: String,
    @SerializedName("deposits")
    val internalDeposits: List<InternDeposit>
)

data class InternDeposit(
    @SerializedName("depositId")
    val depositId: Int,
    @SerializedName("startDate")
    val startDate: String,
    @SerializedName("depositAmount")
    val depositAmount: BigDecimal,
    @SerializedName("currentAmount")
    val currentAmount: BigDecimal
)