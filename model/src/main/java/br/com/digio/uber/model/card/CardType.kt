package br.com.digio.uber.model.card

enum class TypeCard(
    val productNameCard: ProductNameCard,
    val isVirtual: Int,
    val flag: TypeFlagCard
) {
    CREDIT_ELO(ProductNameCard.CREDIT_ELO, 0, TypeFlagCard.ELO),
    DEBIT_ELO(ProductNameCard.DEBIT_ELO, 0, TypeFlagCard.ELO),
    VIRTUAL_CREDIT_ELO(ProductNameCard.CREDIT_ELO, 1, TypeFlagCard.ELO),
    VIRTUAL_DEBIT_ELO(ProductNameCard.DEBIT_ELO, 1, TypeFlagCard.ELO),
    VIRTUAL_DEBIT_VISA(ProductNameCard.DEBIT_ELO, 1, TypeFlagCard.VISA);

    companion object {
        const val TYPE_CARD_PARAMETER_NAME: String = "TYPE_CARD"

        fun getListValidStatus(): List<Int> = listOf(
            CardStatus.NORMAL.id,
            CardStatus.BLOQUEADO.id,
            CardStatus.BLOQUEADO_PREVENCAO.id,
            CardStatus.SUSPEITA_DE_FRAUDE_PREVENTIVO.id,
            CardStatus.BLOQUEIO_INFO_INVALIDAS.id,
            CardStatus.BLOQUEADO_SENHA_INCORRETA.id,
            CardStatus.BLOQUEIO_PREVENTIVO_FALCON.id
        )

        fun getListProductNameAll(): List<String> = listOf(
            ProductNameCard.CREDIT_ELO.name,
            ProductNameCard.CREDIT_VISA.name,
            ProductNameCard.DEBIT_ELO.name,
            ProductNameCard.DEBIT_VISA.name
        )

        fun getCurrentType(cardResponse: CardResponse): TypeCard {
            return if (cardResponse.isVirtualCard == 1) {
                when (cardResponse.productName) {
                    ProductNameCard.CREDIT_ELO.name -> VIRTUAL_CREDIT_ELO
                    ProductNameCard.DEBIT_ELO.name -> VIRTUAL_DEBIT_ELO
                    ProductNameCard.DEBIT_VISA.name -> VIRTUAL_DEBIT_VISA
                    else -> throw TypeCardError(
                        "Type virtual not found (TypeCard) productId =" + cardResponse.productId
                    )
                }
            } else {
                when (cardResponse.productName) {
                    ProductNameCard.CREDIT_ELO.name -> CREDIT_ELO
                    ProductNameCard.DEBIT_ELO.name -> DEBIT_ELO
                    else -> throw TypeCardError("Type not found (TypeCard) productId = " + cardResponse.productId)
                }
            }
        }
    }
}

enum class TypeFlagCard(val img: Int? = null) {
    ELO,
    VISA
}

enum class ProductNameCard {
    CREDIT_ELO,
    CREDIT_VISA,
    DEBIT_ELO,
    DEBIT_VISA
}

@Suppress("MagicNumber")
enum class ProductCardId(val id: Int, val label: String) {
    DEBIT_ELO(1, "DEBIT_ELO"),
    DEBIT_VIRTUAL_ELO(13, "DEBIT_VIRTUAL_ELO")
}

typealias TypeCardError = java.lang.RuntimeException