package br.com.digio.uber.model.bankSlip

/**
 * @author Marlon D. Rocha
 * @since 28/10/20
 */
data class BankSlipPdfRequest(
    val uuid: String
)