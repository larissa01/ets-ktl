package br.com.digio.uber.model

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class BalanceResponse(
    @SerializedName("balance")
    val balance: BigDecimal,
    @SerializedName("blockedBalance")
    val blockedBalance: BigDecimal?
)