package br.com.digio.uber.model

import java.util.ArrayList

enum class State constructor(
    val abbreviation: String,
    val nameOfState: String
) {
    AC("AC", "Acre"),
    AL("AL", "Alagoas"),
    AM("AM", "Amazonas"),
    AP("AP", "Amapá"),
    BA("BA", "Bahia"),
    CE("CE", "Ceará"),
    DF("DF", "Distrito Federal"),
    ES("ES", "Espírito Santo"),
    GO("GO", "Goiás"),
    MA("MA", "Maranhão"),
    MT("MT", "Mato Grosso"),
    MS("MS", "Mato Grosso do Sul"),
    MG("MG", "Minas Gerais"),
    PA("PA", "Pará"),
    PB("PB", "Paraíba"),
    PR("PR", "Paraná"),
    PE("PE", "Pernambuco"),
    PI("PI", "Piauí"),
    RG("RJ", "Rio de Janeiro"),
    RN("RN", "Rio Grande do Norte"),
    RO("RO", "Rondônia"),
    RS("RS", "Rio Grande do Sul"),
    RR("RR", "Roraima"),
    SC("SC", "Santa Catarina"),
    SE("SE", "Sergipe"),
    SP("SP", "São Paulo"),
    TO("TO", "Tocantins");

    fun nameAndAbbreviation(): String {
        val builder = StringBuilder()
        builder.append(nameOfState)
        if (abbreviation.isNotEmpty()) {
            builder.append(" - ")
        }
        builder.append(abbreviation)
        return builder.toString()
    }

    companion object {

        fun statesAndAbbreviation(): List<String> {
            val states = ArrayList<String>()
            for (state in values()) {
                states.add(state.nameAndAbbreviation())
            }
            return states
        }

        fun getNameOfStateFromAbreviation(abreviation: String): String? =
            values().firstOrNull { it.abbreviation == abreviation }?.nameOfState

        fun getAbreviationFromNameOfState(nameOfState: String): String? =
            values().firstOrNull { it.nameOfState == nameOfState }?.abbreviation
    }
}