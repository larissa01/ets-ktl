package br.com.digio.uber.model.common

data class CredentialModel(
    var cpfCrypted: String,
    var password: String
)