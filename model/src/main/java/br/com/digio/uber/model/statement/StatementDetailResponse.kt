package br.com.digio.uber.model.statement

import com.google.gson.annotations.SerializedName

data class StatementDetailResponse(
    @SerializedName("counterpartName")
    val counterpartName: String,

    @SerializedName("counterpartDocument")
    val counterpartDocument: String,

    @SerializedName("counterpartBank")
    val counterpartBank: String?,

    @SerializedName("counterpartBankBranch")
    val counterpartBankBranch: String?,

    @SerializedName("counterpartBankAccount")
    val counterpartBankAccount: String?,

    @SerializedName("action")
    val action: String?,

    @SerializedName("refundObservation")
    val refundObservation: String?

)