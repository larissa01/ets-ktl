package br.com.digio.uber.model.login.checkCpf

import com.google.gson.annotations.SerializedName

data class CheckCpfMockReq(
    @SerializedName("document")
    val document: String
)