package br.com.digio.uber.model.cashback

import com.google.gson.annotations.SerializedName

data class CashbackCustomerResponse(
    @SerializedName("document")
    val document: String? = null,
    @SerializedName("referralId")
    val referralId: String? = null,
    @SerializedName("productType")
    val productType: String? = null,
    @SerializedName("tier")
    val tier: CashbackCutomerTier? = null,
    @SerializedName("nextTier")
    val nextTier: CashbackCutomerTier? = null
)