package br.com.digio.uber.model.faq

import com.google.gson.annotations.SerializedName

data class FaqChatUrlRequest(

    @SerializedName("appVersion") val appVersion: String,
    @SerializedName("carrier") val carrier: String,
    @SerializedName("connectionType") val connectionType: String,
    @SerializedName("countryCode") val countryCode: String,
    @SerializedName("identifier") val identifier: String,
    @SerializedName("language") val language: String,
    @SerializedName("nameIdentifier") val nameIdentifier: String,
    @SerializedName("osVersion") val osVersion: String,
    @SerializedName("phoneModel") val phoneModel: String,
    @SerializedName("platform") val platfom: String,
    @SerializedName("tag") val tag: String,
    @SerializedName("token") val token: String? = ""
)