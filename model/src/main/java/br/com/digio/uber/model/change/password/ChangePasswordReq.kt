package br.com.digio.uber.model.change.password

import com.google.gson.annotations.SerializedName

data class ChangePasswordReq(
    @SerializedName("document")
    val document: String,
    @SerializedName("newPassword")
    val newPassword: String,
    @SerializedName("oldPassword")
    val oldPassword: String
)