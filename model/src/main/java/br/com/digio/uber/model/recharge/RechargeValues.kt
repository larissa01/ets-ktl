package br.com.digio.uber.model.recharge

import com.google.gson.annotations.SerializedName

data class RechargeValues(
    @SerializedName("content")
    val content: List<RechargeValueItem>?
)

data class RechargeValueItem(
    @SerializedName("value")
    val value: Double
)