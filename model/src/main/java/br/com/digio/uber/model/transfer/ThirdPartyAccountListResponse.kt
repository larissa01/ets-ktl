package br.com.digio.uber.model.transfer

import com.google.gson.annotations.SerializedName

data class ThirdPartyAccountListResponse(
    @SerializedName("accounts")
    val accounts: List<ThirdPartyAccountResponse>
)