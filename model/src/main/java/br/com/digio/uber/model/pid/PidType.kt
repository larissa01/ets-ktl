package br.com.digio.uber.model.pid

enum class PidType {
    TRANSFER,
    SHOW_CARD_PASSWORD,
    USER_DATA_MANAGER,
    QR_CODE,
    INVOICE_PAYMENT,
    RECHARGE,
    VIRTUAL_CARD,
    WITHDRAW,
    INSTANT_PAYMENT
}