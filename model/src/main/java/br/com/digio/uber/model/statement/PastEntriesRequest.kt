package br.com.digio.uber.model.statement

data class PastEntriesRequest(
    val days: Int? = null,
    val startDate: String? = null,
    val endDate: String? = null,
    val lastDate: String? = null
)