package br.com.digio.uber.model.store

import com.google.gson.annotations.SerializedName

data class StoreCatalog(
    @SerializedName("totalElements")
    val totalElements: Int?,
    @SerializedName("number")
    val number: Int?,
    @SerializedName("size")
    val size: Int?,
    @SerializedName("totalPages")
    val totalPages: Int?,
    @SerializedName("numberOfElements")
    val numberOfElements: Int?,
    @SerializedName("firstPage")
    val firstPage: Boolean?,
    @SerializedName("hasPreviousPage")
    val hasPreviousPage: Boolean?,
    @SerializedName("hasNextPage")
    val hasNextPage: Boolean?,
    @SerializedName("hasContent")
    val hasContent: Boolean?,
    @SerializedName("first")
    val first: Boolean?,
    @SerializedName("last")
    val last: Boolean? = true,
    @SerializedName("nextPage")
    val nextPage: Int?,
    @SerializedName("previousPage")
    val previousPage: Int?,
    @SerializedName("content")
    val content: List<StoreProduct>
)