package br.com.digio.uber.model.store

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class StoreOrder(
    @SerializedName("subproductId")
    val subproductId: Long?,
    @SerializedName("value")
    val value: Float?,
    @SerializedName("installment")
    val installment: Int?,
    @SerializedName("quantity")
    val quantity: Int?,
    @SerializedName("paymentMethod")
    val paymentMethod: PaymentType?,
    @SerializedName("cardId")
    val cardId: Long?,
    @SerializedName("cardDueDate")
    val cardDueDate: String?,
    @SerializedName("birthdate")
    val birthdate: String?,
    @SerializedName("ddd")
    val ddd: Int?,
    @SerializedName("phoneNumber")
    val phoneNumber: String?,
    @SerializedName("carrierLabel")
    val carrierLabel: String?
) : Serializable

enum class PaymentType {
    CARD, ACCOUNT
}