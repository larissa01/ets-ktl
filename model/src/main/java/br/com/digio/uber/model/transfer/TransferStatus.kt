package br.com.digio.uber.model.transfer

import br.com.digio.uber.model.bankSlip.CustomerLimitsResponse
import com.google.gson.annotations.SerializedName

/**
 * @author Marlon D. Rocha
 * @since 06/11/20
 */
data class TransferStatus(
    @SerializedName("openSpb")
    val openSpb: Boolean,
    @SerializedName("availableLimits")
    val availableLimits: CustomerLimitsResponse
)