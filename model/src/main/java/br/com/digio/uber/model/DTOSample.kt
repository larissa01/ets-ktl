package br.com.digio.uber.model

import com.google.gson.annotations.SerializedName

data class DTOSample constructor(
    @SerializedName("jwt")
    val jwt: String
)