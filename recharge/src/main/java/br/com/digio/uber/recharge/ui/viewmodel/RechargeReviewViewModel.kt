package br.com.digio.uber.recharge.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.recharge.RegisterFavoriteContactUseCase
import br.com.digio.uber.domain.usecase.recharge.UpdateFavoriteContactUseCase
import br.com.digio.uber.domain.usecase.store.RegisterStoreOrderUseCase
import br.com.digio.uber.model.store.StoreOrder
import br.com.digio.uber.model.store.StoreOrderRegisterResponse
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.toMoney

class RechargeReviewViewModel constructor(
    private val registerFavoriteContactUseCase: RegisterFavoriteContactUseCase,
    private val updateFavoriteContactUseCase: UpdateFavoriteContactUseCase,
    private val registerStoreOrderUseCase: RegisterStoreOrderUseCase
) : BaseViewModel() {

    val contact: MutableLiveData<FavoriteContactUiModel> = MutableLiveData()
    val phoneNumber: MutableLiveData<String> = MutableLiveData()
    val nickname: MutableLiveData<String> = MutableLiveData()
    val hideView: MutableLiveData<Boolean> = MutableLiveData()
    val value: MutableLiveData<String> = MutableLiveData()
    val orderRegisterResponse: MutableLiveData<StoreOrderRegisterResponse> = MutableLiveData()
    private val saveCheck = MutableLiveData<Boolean>().apply {
        value = true
    }

    fun setContact(favoriteContactUiModel: FavoriteContactUiModel) {
        contact.value = favoriteContactUiModel
        phoneNumber.value =
            "${favoriteContactUiModel.ddd}${favoriteContactUiModel.phoneNumber}".applyMask(
                Const.MaskPattern.CELLPHONE
            )
        hideView.value = favoriteContactUiModel.nickname.isNullOrEmpty()
    }

    fun setValue(order: StoreOrder) {
        value.value = order.value?.toBigDecimal()?.toMoney()
    }

    fun checkIfSaveContact(checked: Boolean) {
        saveCheck.value = checked
    }

    fun saveFavorite(callBack: () -> Unit) {
        contact.value?.let { contact ->
            val favorite = if (this.nickname.value != null) {
                contact.copy(nickname = this.nickname.value)
            } else {
                contact
            }

            when {
                saveCheck.value == true && favorite.isNewContact ->
                    registerFavoriteContactUseCase(favorite.toFavoriteContact()).singleExec(
                        onError = { _, error, _ ->
                            callBack.invoke()
                            error
                        },
                        onSuccessBaseViewModel = { _ ->
                            callBack.invoke()
                        }
                    )

                saveCheck.value == true && !favorite.isNewContact ->
                    updateFavoriteContactUseCase(favorite.toFavoriteContact()).singleExec(
                        onError = { _, error, _ ->
                            callBack.invoke()
                            error
                        },
                        onSuccessBaseViewModel = { _ ->
                            callBack.invoke()
                        }
                    )

                else -> callBack.invoke()
            }
        }
    }

    fun finishRecharge(order: StoreOrder) {
        registerStoreOrderUseCase(order).singleExec(
            onError = { _, error, _ ->
                error
            },
            onSuccessBaseViewModel = { response ->
                orderRegisterResponse.value = response
            }
        )
    }
}