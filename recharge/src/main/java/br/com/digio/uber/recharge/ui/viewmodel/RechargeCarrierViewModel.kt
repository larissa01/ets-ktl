package br.com.digio.uber.recharge.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel

class RechargeCarrierViewModel : BaseViewModel() {

    val carrierSelected: MutableLiveData<String> = MutableLiveData()
    val buttonEnable = MutableLiveData<Boolean>().apply {
        value = false
    }

    fun setCarrier(carrier: String, enableButton: Boolean) {
        carrierSelected.value = carrier
        buttonEnable.value = enableButton
    }
}