package br.com.digio.uber.recharge.di.viewmodel

import br.com.digio.uber.recharge.ui.viewmodel.RechargeCarrierViewModel
import br.com.digio.uber.recharge.ui.viewmodel.RechargeContactsRegisteredViewModel
import br.com.digio.uber.recharge.ui.viewmodel.RechargeDetailsViewModel
import br.com.digio.uber.recharge.ui.viewmodel.RechargeHomeViewModel
import br.com.digio.uber.recharge.ui.viewmodel.RechargeListChoiceViewModel
import br.com.digio.uber.recharge.ui.viewmodel.RechargeNewViewModel
import br.com.digio.uber.recharge.ui.viewmodel.RechargeReviewViewModel
import br.com.digio.uber.recharge.ui.viewmodel.RechargeSuccessViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val rechargeHomeViewModelModule = module {
    viewModel { RechargeHomeViewModel(get()) }
    viewModel { RechargeContactsRegisteredViewModel(get(), get(), get()) }
    viewModel { RechargeCarrierViewModel() }
    viewModel { RechargeDetailsViewModel(get(), get()) }
    viewModel { RechargeNewViewModel() }
    viewModel { RechargeReviewViewModel(get(), get(), get()) }
    viewModel { RechargeListChoiceViewModel() }
    viewModel { RechargeSuccessViewModel() }
}