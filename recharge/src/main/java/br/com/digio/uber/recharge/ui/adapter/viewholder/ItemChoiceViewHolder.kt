package br.com.digio.uber.recharge.ui.adapter.viewholder

import android.annotation.SuppressLint
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.recharge.databinding.ItemListChoiceBinding
import br.com.digio.uber.recharge.model.ListChoiceItemUiModel
import br.com.digio.uber.recharge.model.ListChoiceUiModel

class ItemChoiceViewHolder(
    private val viewDataBinding: ItemListChoiceBinding,
    private val listener: ViewTypesListener<ListChoiceItemUiModel>
) : AbstractViewHolder<ListChoiceUiModel>(viewDataBinding.root) {

    @SuppressLint("DefaultLocale")
    override fun bind(item: ListChoiceUiModel) {
        viewDataBinding.listChoiceUiModel = item
        viewDataBinding.itemChoiceViewHolder = this
        viewDataBinding.executePendingBindings()
    }

    fun onClick(item: ListChoiceUiModel) {
        listener(item)
    }
}