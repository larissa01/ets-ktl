package br.com.digio.uber.recharge.interfaces

import br.com.digio.uber.recharge.model.FavoriteContactUiModel

interface FavoriteNickNameListener {
    fun nickNameEdit(favoriteContactUiModel: FavoriteContactUiModel, position: Int)
}