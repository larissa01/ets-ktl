package br.com.digio.uber.recharge.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.store.GetStoreCatalogUseCase
import br.com.digio.uber.model.store.StoreCatalog

class RechargeHomeViewModel constructor(
    private val getStoreCatalogUseCase: GetStoreCatalogUseCase
) : BaseViewModel() {

    val storeCatalog: MutableLiveData<StoreCatalog> = MutableLiveData()

    fun getStoreCatalog() {
        getStoreCatalogUseCase(Unit).singleExec(
            onError = { _, error, _ ->
                error
            },
            onSuccessBaseViewModel = { catalog ->
                storeCatalog.value = catalog
            }
        )
    }
}