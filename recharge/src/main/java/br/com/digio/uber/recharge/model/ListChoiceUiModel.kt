package br.com.digio.uber.recharge.model

import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import java.io.Serializable

sealed class ListChoiceItemUiModel : AdapterViewModel<ListChoiceItemUiModel> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<ListChoiceItemUiModel>) = typesFactory.type(
        model = this
    )
}

data class ListChoiceUiModel(
    val title: String?,
    val subtitle: String?,
    val icon: Int?
) : Serializable, ListChoiceItemUiModel()

enum class ListChoiceType {
    CARRIER, VALUE
}