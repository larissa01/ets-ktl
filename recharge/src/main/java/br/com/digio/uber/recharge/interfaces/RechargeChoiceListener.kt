package br.com.digio.uber.recharge.interfaces

interface RechargeChoiceListener {
    fun choiceClick(choice: String?)
}