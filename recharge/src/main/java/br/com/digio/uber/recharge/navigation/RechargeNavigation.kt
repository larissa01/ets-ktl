package br.com.digio.uber.recharge.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName
import br.com.digio.uber.recharge.ui.fragment.RechargeCarrierFragment
import br.com.digio.uber.recharge.ui.fragment.RechargeDetailsFragment
import br.com.digio.uber.recharge.ui.fragment.RechargeHomeFragment
import br.com.digio.uber.recharge.ui.fragment.RechargeReviewFragment
import br.com.digio.uber.recharge.ui.fragment.RechargeSuccessFragment

class RechargeNavigation(
    activity: AppCompatActivity
) : GenericNavigationWithFlow(activity) {

    fun init(bundle: Bundle?) {
        goTo(GoTo(RechargeOpKey.RECHARGE_HOME.name, bundle))
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            RechargeOpKey.RECHARGE_HOME.name to showHome(),
            RechargeOpKey.RECHARGE_CARRIER.name to showCarrier(),
            RechargeOpKey.RECHARGE_DETAILS.name to showDetails(),
            RechargeOpKey.RECHARGE_REVIEW.name to showReview(),
            RechargeOpKey.RECHARGE_SUCCESS.name to showSuccess()
        )

    private fun showHome(): GenericNavigationWIthFlowGetFragmentByName = {
        RechargeHomeFragment.newInstance(this, it)
    }

    private fun showCarrier(): GenericNavigationWIthFlowGetFragmentByName = {
        RechargeCarrierFragment.newInstance(this, it)
    }

    private fun showDetails(): GenericNavigationWIthFlowGetFragmentByName = {
        RechargeDetailsFragment.newInstance(this, it)
    }

    private fun showReview(): GenericNavigationWIthFlowGetFragmentByName = {
        RechargeReviewFragment.newInstance(this, it)
    }

    private fun showSuccess(): GenericNavigationWIthFlowGetFragmentByName = {
        RechargeSuccessFragment.newInstance(this, it)
    }
}