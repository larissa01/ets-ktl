package br.com.digio.uber.recharge.ui.fragment

import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.extensions.hide
import br.com.digio.uber.common.extensions.show
import br.com.digio.uber.common.generics.makeChooseMessageObject
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.recharge.BR
import br.com.digio.uber.recharge.R
import br.com.digio.uber.recharge.databinding.FragmentRechargeContactsRegisteredBinding
import br.com.digio.uber.recharge.interfaces.CallFragment
import br.com.digio.uber.recharge.interfaces.FavoriteNickNameListener
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.recharge.navigation.RechargeOpKey
import br.com.digio.uber.recharge.ui.adapter.FavoriteContactAdapter
import br.com.digio.uber.recharge.ui.dialog.RechargeFavoriteEditDialog
import br.com.digio.uber.recharge.ui.viewmodel.RechargeContactsRegisteredViewModel
import org.koin.android.ext.android.inject

class RechargeContactsRegisteredFragment :
    BaseRechargeFragment<FragmentRechargeContactsRegisteredBinding, RechargeContactsRegisteredViewModel>(),
    FavoriteNickNameListener {

    private var callFragment: CallFragment? = null

    private val favoriteAdapter: FavoriteContactAdapter by lazy {
        FavoriteContactAdapter(
            {
                callFragment?.callFragment(RechargeOpKey.RECHARGE_DETAILS.name, it)
            },
            { value, action, position ->
                if (action == EDIT) {
                    showEditDialog(value, position)
                } else {
                    showDeleteDialog(value)
                }
            }
        )
    }

    override val bindingVariable: Int = BR.rechargeContactsRegisteredViewModel

    override val getLayoutId: Int = R.layout.fragment_recharge_contacts_registered

    override val viewModel: RechargeContactsRegisteredViewModel by inject()

    override fun tag(): String = RechargeOpKey.RECHARGE_CONTACTS_REGISTERED.name

    override fun getStatusBarAppearance(): StatusBarColor = RechargeOpKey.RECHARGE_CONTACTS_REGISTERED.theme

    override fun initialize() {
        super.initialize()

        setupAdapter()

        setObserver()

        viewModel.getFavoriteList()
    }

    private fun setObserver() {
        viewModel.emptyList.observe(
            this,
            Observer {
                if (it) {
                    binding?.rechargeContactsRegisteredEmpty?.emptyScreenContent?.show()
                } else {
                    binding?.rechargeContactsRegisteredEmpty?.emptyScreenContent?.hide()
                }
            }
        )
    }

    private fun setupAdapter() {
        binding?.rechargeContactsRegisteredRecycler?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = favoriteAdapter
        }
    }

    private fun showDeleteDialog(favoriteContactUiModel: FavoriteContactUiModel) {
        showMessage(
            makeChooseMessageObject {
                icon = br.com.digio.uber.common.R.drawable.ic_alert
                title = R.string.are_you_sure_you_want_to_delete_the_registered_number
                messageString = ""
                positiveOptionTextInt = R.string.delete_number
                negativeOptionTextInt = R.string.cancel
                onPositiveButtonClick = {
                    deleteFavorite(favoriteContactUiModel)
                }
            }
        )
    }

    private fun deleteFavorite(favoriteContactUiModel: FavoriteContactUiModel) {
        favoriteAdapter.removeItem(favoriteContactUiModel)

        if (favoriteAdapter.itemCount == 0) {
            viewModel.showEmptyScreen()
        }

        viewModel.removeContact(favoriteContactUiModel.toFavoriteContact())
    }

    private fun showEditDialog(favoriteContactUiModel: FavoriteContactUiModel, position: Int) {
        activity?.supportFragmentManager?.let {
            RechargeFavoriteEditDialog.newInstance(this, favoriteContactUiModel, position).show(it, null)
        }
    }

    override fun nickNameEdit(favoriteContactUiModel: FavoriteContactUiModel, position: Int) {
        favoriteAdapter.editItem(favoriteContactUiModel, position)

        viewModel.editContact(favoriteContactUiModel.toFavoriteContact())
    }

    companion object {
        const val DELETE = "delete"
        const val EDIT = "edit"

        fun newInstance(callFragment: CallFragment) =
            RechargeContactsRegisteredFragment().apply {
                this.callFragment = callFragment
            }
    }
}