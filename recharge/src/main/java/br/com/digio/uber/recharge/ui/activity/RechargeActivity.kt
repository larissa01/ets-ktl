package br.com.digio.uber.recharge.ui.activity

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.navigation.GenericNavigation
import br.com.digio.uber.recharge.di.viewmodel.rechargeHomeViewModelModule
import br.com.digio.uber.recharge.navigation.RechargeNavigation
import org.koin.core.module.Module

class RechargeActivity : BaseViewModelActivity<ViewDataBinding, BaseViewModel>() {

    private val rechargeNavigation = RechargeNavigation(this)

    override val bindingVariable: Int? = null

    override val viewModel: BaseViewModel? = null

    override fun getLayoutId(): Int? = LayoutIds.navigationLayoutActivity

    override fun getModule(): List<Module>? = listOf(rechargeHomeViewModelModule)

    override fun getNavigation(): GenericNavigation? = rechargeNavigation

    override fun showHomeAsUp(): Boolean = true

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        rechargeNavigation.init(intent.extras)
    }
}