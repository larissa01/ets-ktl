package br.com.digio.uber.recharge.ui.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.recharge.BR
import br.com.digio.uber.recharge.R
import br.com.digio.uber.recharge.databinding.FragmentRechargeSuccessBinding
import br.com.digio.uber.recharge.navigation.RechargeOpKey
import br.com.digio.uber.recharge.ui.viewmodel.RechargeSuccessViewModel
import org.koin.android.ext.android.inject

class RechargeSuccessFragment :
    BNWFViewModelFragment<FragmentRechargeSuccessBinding, RechargeSuccessViewModel>() {

    override val bindingVariable: Int = BR.rechargeSuccessViewModel

    override val getLayoutId: Int = R.layout.fragment_recharge_success

    override val viewModel: RechargeSuccessViewModel by inject()

    override fun tag(): String = RechargeOpKey.RECHARGE_SUCCESS.name

    override fun getStatusBarAppearance(): StatusBarColor = RechargeOpKey.RECHARGE_SUCCESS.theme

    override fun getToolbar(): Toolbar? = binding?.rechargeSuccessAppBar?.toolbarRechargeSuccess

    override val showDisplayShowTitle: Boolean = true

    override val showHomeAsUp = false

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it) }
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.recharge_success_btn_back -> callGoBack()
        }
    }

    companion object : FragmentNewInstance<RechargeSuccessFragment>(::RechargeSuccessFragment)
}