package br.com.digio.uber.recharge.ui.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.recharge.R
import br.com.digio.uber.recharge.databinding.ItemListChoiceBinding
import br.com.digio.uber.recharge.model.ListChoiceItemUiModel
import br.com.digio.uber.recharge.ui.adapter.viewholder.ItemChoiceViewHolder
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

typealias OnClickChoiceItem = (item: ListChoiceItemUiModel) -> Unit

class RechargeListChoiceAdapter(
    private var values: MutableList<ListChoiceItemUiModel> = mutableListOf(),
    private val listener: OnClickChoiceItem
) : BaseRecyclerViewAdapter<ListChoiceItemUiModel, RechargeListChoiceAdapter.ViewTypesDataBindingFactoryImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl =
        ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): ListChoiceItemUiModel =
        values[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder<ListChoiceItemUiModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<ListChoiceItemUiModel>
    }

    override fun getItemCount(): Int = values.size

    override fun onBindViewHolder(
        holder: AbstractViewHolder<ListChoiceItemUiModel>,
        position: Int
    ) = holder.bind(values[holder.adapterPosition])

    override fun replaceItems(list: List<Any>) {
        addValues(list.safeHeritage())
    }

    private fun addValues(values: List<ListChoiceItemUiModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }

    inner class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<ListChoiceItemUiModel> {

        override fun type(model: ListChoiceItemUiModel): Int = R.layout.item_list_choice

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<ListChoiceItemUiModel>
        ): AbstractViewHolder<*> =
            when (type) {
                R.layout.item_list_choice -> ItemChoiceViewHolder(
                    view.safeHeritage<ItemListChoiceBinding>()!!,
                    listener
                )
                else -> throw IndexOutOfBoundsException("Invalid view type")
            }
    }
}