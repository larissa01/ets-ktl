package br.com.digio.uber.recharge.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.store.StoreProduct
import br.com.digio.uber.recharge.BR
import br.com.digio.uber.recharge.R
import br.com.digio.uber.recharge.databinding.FragmentRechargeCarrierBinding
import br.com.digio.uber.recharge.interfaces.RechargeChoiceListener
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.recharge.model.ListChoiceType
import br.com.digio.uber.recharge.navigation.RechargeOpKey
import br.com.digio.uber.recharge.ui.dialog.RechargeListChoiceDialog
import br.com.digio.uber.recharge.ui.fragment.RechargeHomeFragment.Companion.FAVORITE_CONTACT
import br.com.digio.uber.recharge.ui.viewmodel.RechargeCarrierViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.safeHeritage
import org.koin.android.ext.android.inject

class RechargeCarrierFragment :
    BaseRechargeFragment<FragmentRechargeCarrierBinding, RechargeCarrierViewModel>(), RechargeChoiceListener {

    private val favoriteContact by lazy {
        arguments?.getSerializable(FAVORITE_CONTACT)?.safeHeritage<FavoriteContactUiModel>()
    }

    private val product by lazy {
        arguments?.getSerializable(Const.Extras.STORE_PRODUCT)?.safeHeritage<StoreProduct>()
    }

    override val bindingVariable: Int = BR.rechargeCarrierViewModel

    override val getLayoutId: Int = R.layout.fragment_recharge_carrier

    override val viewModel: RechargeCarrierViewModel by inject()

    override fun tag(): String = RechargeOpKey.RECHARGE_CARRIER.name

    override fun getStatusBarAppearance(): StatusBarColor = RechargeOpKey.RECHARGE_CARRIER.theme

    override fun getToolbar(): Toolbar? = binding?.rechargeCarrierAppBar?.toolbarRechargeMain

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it) }

        viewModel.setCarrier(requireContext().getString(R.string.select_one_carrier), false)
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.recharge_carrier_btn_continue -> goToRechargeDetails()
            R.id.recharge_carrier_card_carrier -> goToRechargeListChoice()
        }
    }

    private fun goToRechargeDetails() {
        callGoTo(
            RechargeOpKey.RECHARGE_DETAILS.name,
            Bundle().apply {
                putSerializable(FAVORITE_CONTACT, updateFavorite())
                putSerializable(Const.Extras.STORE_PRODUCT, product)
            }
        )
    }

    private fun goToRechargeListChoice() {
        val carriers = mutableListOf<String>()
        product?.subproducts?.map { it.partnerProductId }?.toTypedArray()?.forEach {
            carriers.add(it)
        }

        activity?.supportFragmentManager?.let { activityLet ->
            RechargeListChoiceDialog.newInstance(carriers.toTypedArray(), ListChoiceType.CARRIER.name, this)
                .show(activityLet, null)
        }
    }

    private fun updateFavorite() = favoriteContact?.copy(
        carrier = viewModel.carrierSelected.value,
        carrierIcon = product?.subproducts?.find { it.partnerProductId == viewModel.carrierSelected.value }?.image
    )

    override fun choiceClick(choice: String?) {
        viewModel.setCarrier(choice ?: requireContext().getString(R.string.select_one_carrier), true)
    }

    companion object : FragmentNewInstance<RechargeCarrierFragment>(::RechargeCarrierFragment)
}