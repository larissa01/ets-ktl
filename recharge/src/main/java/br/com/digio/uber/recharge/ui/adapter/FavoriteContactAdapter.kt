package br.com.digio.uber.recharge.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.recharge.databinding.ItemRechargeFavoriteContactBinding
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.recharge.ui.fragment.RechargeContactsRegisteredFragment.Companion.DELETE
import br.com.digio.uber.recharge.ui.fragment.RechargeContactsRegisteredFragment.Companion.EDIT
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.safeHeritage

typealias OnClickFavoriteContactItem = (item: FavoriteContactUiModel) -> Unit
typealias OnActionFavoriteContactItem = (item: FavoriteContactUiModel, action: String, position: Int) -> Unit

class FavoriteContactAdapter(
    private val listener: OnClickFavoriteContactItem,
    private val actionListener: OnActionFavoriteContactItem
) : RecyclerView.Adapter<FavoriteContactAdapter.FavoriteContactViewHolder>(), AdapterItemsContract {

    private var contact: List<FavoriteContactUiModel> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteContactViewHolder {
        val binding =
            ItemRechargeFavoriteContactBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FavoriteContactViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FavoriteContactViewHolder, position: Int) {
        holder.bind(contact[position], position, listener, actionListener)
    }

    override fun getItemCount() = contact.size

    override fun replaceItems(list: List<Any>) {
        contact = list.safeHeritage()
        notifyDataSetChanged()
    }

    fun removeItem(favoriteContactUiModel: FavoriteContactUiModel) {
        val list = contact.toMutableList()
        list.remove(favoriteContactUiModel)

        replaceItems(list)
    }

    fun editItem(favorite: FavoriteContactUiModel, position: Int) {
        val list = contact.toMutableList()
        list.removeAt(position)
        list.add(favorite)
        list.sortBy { it.nickname }

        replaceItems(list)
    }

    class FavoriteContactViewHolder(private val binding: ItemRechargeFavoriteContactBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            contact: FavoriteContactUiModel,
            position: Int,
            listener: OnClickFavoriteContactItem,
            actionListener: OnActionFavoriteContactItem
        ) {
            binding.itemRechargeContactsTxtNickname.text =
                if (contact.nickname.isNullOrEmpty()) {
                    "${contact.ddd}${contact.phoneNumber}".applyMask(Const.MaskPattern.CELLPHONE)
                } else {
                    contact.nickname
                }

            binding.itemRechargeContactsTxtCarrier.text = contact.carrier

            binding.itemRechargeContactsImgDelete.setOnClickListener {
                actionListener.invoke(
                    contact,
                    DELETE,
                    position
                )
            }
            binding.itemRechargeContactsImgEdit.setOnClickListener { actionListener.invoke(contact, EDIT, position) }
            binding.itemRechargeContactsContent.setOnClickListener { listener.invoke(contact) }
        }
    }
}