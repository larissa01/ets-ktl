package br.com.digio.uber.recharge.mapper

import br.com.digio.uber.model.recharge.FavoriteContactList
import br.com.digio.uber.recharge.model.FavoriteContactListUiModel
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.util.mapper.AbstractMapper

class FavoriteContactListMapper :
    AbstractMapper<FavoriteContactList, FavoriteContactListUiModel> {
    override fun map(param: FavoriteContactList): FavoriteContactListUiModel = with(param) {
        FavoriteContactListUiModel(
            favoritePhoneResponses = favoritePhoneResponses?.map {
                FavoriteContactUiModel(
                    phoneNumber = it.phoneNumber,
                    ddd = it.ddd,
                    subproductId = it.subproductId,
                    nickname = it.nickname,
                    carrier = it.carrier,
                    carrierIcon = it.carrierIcon,
                    productType = it.productType,
                    document = it.document,
                    isNewContact = false
                )
            }
        )
    }
}