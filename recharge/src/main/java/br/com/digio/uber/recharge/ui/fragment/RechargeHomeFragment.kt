package br.com.digio.uber.recharge.ui.fragment

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.store.ProductCategory
import br.com.digio.uber.model.store.StoreProduct
import br.com.digio.uber.recharge.BR
import br.com.digio.uber.recharge.R
import br.com.digio.uber.recharge.databinding.FragmentRechargeHomeBinding
import br.com.digio.uber.recharge.interfaces.CallFragment
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.recharge.navigation.RechargeOpKey
import br.com.digio.uber.recharge.ui.adapter.RechargePagerAdapter
import br.com.digio.uber.recharge.ui.viewmodel.RechargeHomeViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.safeHeritage
import kotlinx.android.synthetic.main.fragment_recharge_home.recharge_pager
import org.koin.android.ext.android.inject

class RechargeHomeFragment :
    BaseRechargeFragment<FragmentRechargeHomeBinding, RechargeHomeViewModel>(),
    CallFragment {

    private var product: StoreProduct? = null

    override val bindingVariable: Int = BR.rechargeHomeViewModel

    override val getLayoutId: Int = R.layout.fragment_recharge_home

    override val viewModel: RechargeHomeViewModel by inject()

    override fun tag(): String = RechargeOpKey.RECHARGE_HOME.name

    override fun getStatusBarAppearance(): StatusBarColor = RechargeOpKey.RECHARGE_HOME.theme

    override fun getToolbar(): Toolbar? = binding?.rechargeHomeAppBar?.toolbarRechargeMain

    override fun initialize() {
        super.initialize()

        setObservers()

        product = arguments?.getSerializable(Const.Extras.STORE_PRODUCT)?.safeHeritage<StoreProduct>()

        if (product != null) {
            recharge_pager.adapter =
                activity?.supportFragmentManager?.let { suportFragmentLet ->
                    RechargePagerAdapter(suportFragmentLet, requireContext(), this)
                }
        } else {
            viewModel.getStoreCatalog()
        }
    }

    private fun setObservers() {
        viewModel.storeCatalog.observe(
            this,
            Observer { storeCatalog ->
                product = storeCatalog.content.findLast { it.category == ProductCategory.RECHARGE }

                recharge_pager.adapter =
                    activity?.supportFragmentManager?.let { suportFragmentLet ->
                        RechargePagerAdapter(suportFragmentLet, requireContext(), this)
                    }
            }
        )
    }

    override fun callFragment(fragment: String, favoriteContactUiModel: FavoriteContactUiModel) {
        callGoTo(
            fragment,
            Bundle().apply {
                putSerializable(FAVORITE_CONTACT, favoriteContactUiModel)
                putSerializable(Const.Extras.STORE_PRODUCT, product)
            }
        )
    }

    companion object : FragmentNewInstance<RechargeHomeFragment>(::RechargeHomeFragment) {
        const val FAVORITE_CONTACT = "favorite_contact"
    }
}