package br.com.digio.uber.recharge.ui.dialog

import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.dialog.BNWFViewModelDialog
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.recharge.BR
import br.com.digio.uber.recharge.R
import br.com.digio.uber.recharge.databinding.DialogListChoiceBinding
import br.com.digio.uber.recharge.interfaces.RechargeChoiceListener
import br.com.digio.uber.recharge.model.ListChoiceType
import br.com.digio.uber.recharge.model.ListChoiceUiModel
import br.com.digio.uber.recharge.navigation.RechargeOpKey
import br.com.digio.uber.recharge.ui.adapter.RechargeListChoiceAdapter
import br.com.digio.uber.recharge.ui.viewmodel.RechargeListChoiceViewModel
import br.com.digio.uber.util.safeHeritage
import org.koin.android.ext.android.inject

class RechargeListChoiceDialog :
    BNWFViewModelDialog<DialogListChoiceBinding, RechargeListChoiceViewModel>() {

    private val listChoiceString by lazy {
        arguments?.getStringArray(CHOICE_RECEIVED)
    }

    private val listChoiceType by lazy {
        arguments?.getString(LIST_CHOICE_TYPE)
    }

    private var rechargeChoiceListener: RechargeChoiceListener? = null

    override val bindingVariable: Int = BR.rechargeListChoiceViewModel

    override val getLayoutId: Int = R.layout.dialog_list_choice

    override val viewModel: RechargeListChoiceViewModel by inject()

    override fun tag(): String = RechargeOpKey.RECHARGE_LIST_CHOICE.name

    override fun getStatusBarAppearance(): StatusBarColor = RechargeOpKey.RECHARGE_LIST_CHOICE.theme

    override fun initialize() {
        super.initialize()
        viewModel.initializer { }

        setAdapter()

        viewModel.setListChoice(setListChoice())
    }

    private fun setAdapter() {
        binding?.listChoicerechargeRecycler?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = RechargeListChoiceAdapter(
                listener = { value ->
                    dismiss()
                    rechargeChoiceListener?.choiceClick(value.safeHeritage<ListChoiceUiModel>()?.title)
                }
            )
        }
    }

    private fun setListChoice(): List<ListChoiceUiModel> {
        val list = mutableListOf<ListChoiceUiModel>()
        listChoiceString?.forEach {
            list.add(
                ListChoiceUiModel(
                    title = it,
                    subtitle = null,
                    icon = if (listChoiceType == ListChoiceType.CARRIER.name) {
                        br.com.digio.uber.common.R.drawable.ic_phone
                    } else {
                        br.com.digio.uber.common.R.drawable.ic_phone_value
                    }
                )
            )
        }
        return list
    }

    companion object {
        const val CHOICE_RECEIVED = "choice_received"
        const val LIST_CHOICE_TYPE = "list_choice_type"

        fun newInstance(
            listChoise: Array<String>,
            listChoiseType: String,
            rechargeChoiceListener: RechargeChoiceListener
        ) =
            RechargeListChoiceDialog().apply {
                arguments = bundleOf(
                    CHOICE_RECEIVED to listChoise,
                    LIST_CHOICE_TYPE to listChoiseType
                )
                this.rechargeChoiceListener = rechargeChoiceListener
            }
    }
}