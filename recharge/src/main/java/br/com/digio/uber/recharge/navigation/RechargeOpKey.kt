package br.com.digio.uber.recharge.navigation

import br.com.digio.uber.common.kenum.StatusBarColor

enum class RechargeOpKey(val theme: StatusBarColor) {
    RECHARGE_HOME(StatusBarColor.GREY),
    RECHARGE_NEW(StatusBarColor.GREY),
    RECHARGE_CONTACTS_REGISTERED(StatusBarColor.GREY),
    RECHARGE_CARRIER(StatusBarColor.GREY),
    RECHARGE_DETAILS(StatusBarColor.GREY),
    RECHARGE_REVIEW(StatusBarColor.GREY),
    RECHARGE_LIST_CHOICE(StatusBarColor.GREY),
    RECHARGE_SUCCESS(StatusBarColor.WHITE),
    RECHARGE_EDIT_FAVORITE(StatusBarColor.GREY)
}