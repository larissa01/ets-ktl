package br.com.digio.uber.recharge.ui.fragment

import android.view.View
import android.view.WindowManager
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.recharge.BR
import br.com.digio.uber.recharge.R
import br.com.digio.uber.recharge.databinding.FragmentRechargeNewBinding
import br.com.digio.uber.recharge.interfaces.CallFragment
import br.com.digio.uber.recharge.navigation.RechargeOpKey
import br.com.digio.uber.recharge.ui.viewmodel.RechargeNewViewModel
import org.koin.android.ext.android.inject

class RechargeNewFragment : BaseRechargeFragment<FragmentRechargeNewBinding, RechargeNewViewModel>() {

    private var callFragment: CallFragment? = null

    override val bindingVariable: Int = BR.rechargeNewViewModel

    override val getLayoutId: Int = R.layout.fragment_recharge_new

    override val viewModel: RechargeNewViewModel by inject()

    override fun tag(): String = RechargeOpKey.RECHARGE_NEW.name

    override fun getStatusBarAppearance(): StatusBarColor = RechargeOpKey.RECHARGE_NEW.theme

    override fun getSoftInputMode() = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it) }
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.recharge_new_btn_continue ->
                callFragment?.callFragment(RechargeOpKey.RECHARGE_CARRIER.name, viewModel.getContact())
        }
    }

    companion object {
        fun newInstance(callFragment: CallFragment) =
            RechargeNewFragment().apply {
                this.callFragment = callFragment
            }
    }
}