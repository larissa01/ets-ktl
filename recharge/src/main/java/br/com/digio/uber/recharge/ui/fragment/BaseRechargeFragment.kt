package br.com.digio.uber.recharge.ui.fragment

import android.view.View
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.base.viewmodel.BaseViewModel

abstract class BaseRechargeFragment<T : ViewDataBinding, VM : BaseViewModel> : BNWFViewModelFragment<T, VM>() {

    override val showHomeAsUp: Boolean = true
    override val showDisplayShowTitle: Boolean = true

    override fun onNavigationClick(view: View) {
        callGoBack()
    }
}