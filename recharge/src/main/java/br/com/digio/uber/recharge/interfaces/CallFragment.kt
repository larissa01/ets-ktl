package br.com.digio.uber.recharge.interfaces

import br.com.digio.uber.recharge.model.FavoriteContactUiModel

interface CallFragment {
    fun callFragment(fragment: String, favoriteContactUiModel: FavoriteContactUiModel)
}