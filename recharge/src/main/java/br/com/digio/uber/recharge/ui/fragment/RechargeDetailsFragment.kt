package br.com.digio.uber.recharge.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.store.StoreProduct
import br.com.digio.uber.recharge.BR
import br.com.digio.uber.recharge.R
import br.com.digio.uber.recharge.databinding.FragmentRechargeDetailsBinding
import br.com.digio.uber.recharge.interfaces.RechargeChoiceListener
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.recharge.model.ListChoiceType
import br.com.digio.uber.recharge.navigation.RechargeOpKey
import br.com.digio.uber.recharge.ui.dialog.RechargeListChoiceDialog
import br.com.digio.uber.recharge.ui.viewmodel.RechargeDetailsViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.safeHeritage
import org.koin.android.ext.android.inject

class RechargeDetailsFragment :
    BaseRechargeFragment<FragmentRechargeDetailsBinding, RechargeDetailsViewModel>(), RechargeChoiceListener {

    private val favoriteContact by lazy {
        arguments?.getSerializable(RechargeHomeFragment.FAVORITE_CONTACT)?.safeHeritage<FavoriteContactUiModel>()
    }

    private val product by lazy {
        arguments?.getSerializable(Const.Extras.STORE_PRODUCT)?.safeHeritage<StoreProduct>()
    }

    override val bindingVariable: Int = BR.rechargeDetailsViewModel

    override val getLayoutId: Int = R.layout.fragment_recharge_details

    override val viewModel: RechargeDetailsViewModel by inject()

    override fun tag(): String = RechargeOpKey.RECHARGE_DETAILS.name

    override fun getStatusBarAppearance(): StatusBarColor = RechargeOpKey.RECHARGE_DETAILS.theme

    override fun getToolbar(): Toolbar? = binding?.rechargeReviewAppBar?.toolbarRechargeMain

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it) }

        favoriteContact?.let {
            viewModel.setContact(it)
            it.carrier?.let { it1 -> viewModel.getCarrierPrices(it1) }
        }

        viewModel.setValue(requireContext().getString(R.string.select_on_value), false)
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.recharge_details_btn_continue -> goToRechargeReview()
            R.id.recharge_details_card_value -> goToRechargeListChoice()
        }
    }

    private fun goToRechargeReview() {
        callGoTo(
            RechargeOpKey.RECHARGE_REVIEW.name,
            Bundle().apply {
                putSerializable(RechargeHomeFragment.FAVORITE_CONTACT, favoriteContact)
                putSerializable(RechargeReviewFragment.ORDER, product?.let { viewModel.setOrder(it) })
            }
        )
    }

    private fun goToRechargeListChoice() {
        activity?.supportFragmentManager?.let {
            RechargeListChoiceDialog.newInstance(
                viewModel.getFormatPrices(),
                ListChoiceType.VALUE.name,
                this
            ).show(it, null)
        }
    }

    override fun choiceClick(choice: String?) {
        viewModel.setValue(
            choice ?: requireContext().getString(R.string.select_on_value),
            true
        )
    }

    companion object : FragmentNewInstance<RechargeDetailsFragment>(::RechargeDetailsFragment)
}