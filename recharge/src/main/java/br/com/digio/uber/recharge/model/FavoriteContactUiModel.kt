package br.com.digio.uber.recharge.model

import br.com.digio.uber.model.recharge.FavoriteContact
import java.io.Serializable

data class FavoriteContactUiModel(
    val phoneNumber: String?,
    val ddd: String?,
    val subproductId: Long?,
    val nickname: String?,
    val carrier: String?,
    val carrierIcon: String?,
    val document: String?,
    val productType: String? = "UBER",
    val isNewContact: Boolean = true
) : Serializable {

    fun toFavoriteContact(): FavoriteContact =
        FavoriteContact(
            phoneNumber = phoneNumber,
            ddd = ddd,
            subproductId = subproductId,
            nickname = nickname,
            carrier = carrier,
            carrierIcon = carrierIcon,
            document = document,
            productType = productType
        )
}