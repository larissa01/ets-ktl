package br.com.digio.uber.recharge.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.OnTextChanged
import br.com.digio.uber.common.util.MaskUtils
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.util.Const

class RechargeNewViewModel : BaseViewModel() {

    val cellPhoneMask: String = Const.MaskPattern.CELLPHONE

    val enableButton = MutableLiveData<Boolean>()

    val phoneNumber = MutableLiveData<String>()

    val onCellChange: OnTextChanged = { value, _, _, _ ->
        value?.length?.let {
            if (it >= MINIMUN_PHONE_NUMBER) {
                enableButton.value = true
                phoneNumber.value = MaskUtils.unmaskPhone(value.toString())
            } else {
                enableButton.value = false
            }
        }
    }

    fun getContact() =
        FavoriteContactUiModel(
            phoneNumber = getPhoneNumberAndDdd(),
            ddd = getDdd(),
            subproductId = null,
            nickname = null,
            carrier = null,
            carrierIcon = null,
            document = null,
            isNewContact = true
        )

    private fun getPhoneNumberAndDdd() = phoneNumber.value?.length?.let { phoneNumber.value?.substring(2, it) }

    private fun getDdd() = phoneNumber.value?.substring(0, 2)

    companion object {
        const val MINIMUN_PHONE_NUMBER = 13
    }
}