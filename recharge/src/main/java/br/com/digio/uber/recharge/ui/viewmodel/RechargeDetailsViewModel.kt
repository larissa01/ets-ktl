package br.com.digio.uber.recharge.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.balancetoolbar.balance.mapper.BalanceMapper
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.balance.GetBalanceUseCase
import br.com.digio.uber.domain.usecase.recharge.GetOperatorPricesUseCase
import br.com.digio.uber.model.recharge.RechargeValues
import br.com.digio.uber.model.store.PaymentType
import br.com.digio.uber.model.store.StoreOrder
import br.com.digio.uber.model.store.StoreProduct
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.applyMask
import br.com.digio.uber.util.toCurrencyValue
import br.com.digio.uber.util.unmaskMoney

class RechargeDetailsViewModel constructor(
    getBalanceUseCase: GetBalanceUseCase,
    private val getOperatorPricesUseCase: GetOperatorPricesUseCase
) : BaseViewModel() {

    val balanceUiModel = getBalanceUseCase(Unit).exec().map(BalanceMapper())

    private val rechargePrices: MutableLiveData<RechargeValues> = MutableLiveData()

    val contact: MutableLiveData<FavoriteContactUiModel> = MutableLiveData()
    val phoneNumber: MutableLiveData<String> = MutableLiveData()
    val valueSelected: MutableLiveData<String> = MutableLiveData()
    val buttonEnable = MutableLiveData<Boolean>().apply {
        value = false
    }

    fun getCarrierPrices(carrier: String) {
        getOperatorPricesUseCase(carrier).singleExec(
            onError = { _, error, _ ->
                error
            },
            onSuccessBaseViewModel = { values ->
                rechargePrices.value = values
            }
        )
    }

    fun setContact(favoriteContact: FavoriteContactUiModel) {
        contact.value = favoriteContact
        phoneNumber.value =
            "${favoriteContact.ddd}${favoriteContact.phoneNumber}".applyMask(Const.MaskPattern.CELLPHONE)
    }

    fun setValue(value: String, enableButton: Boolean) {
        valueSelected.value = value
        buttonEnable.value = enableButton
    }

    fun getFormatPrices(): Array<String> {
        val prices = mutableListOf<String>()
        rechargePrices.value?.content?.forEach {
            prices.add(it.value.toCurrencyValue(true))
        }

        return prices.toTypedArray()
    }

    fun setOrder(product: StoreProduct) = StoreOrder(
        subproductId = product.subproducts?.find { it.partnerProductId == contact.value?.carrier }?.subproductId,
        value = valueSelected.value?.unmaskMoney()?.toFloat(),
        installment = 1,
        quantity = 1,
        paymentMethod = PaymentType.ACCOUNT,
        cardId = null,
        cardDueDate = null,
        birthdate = null,
        ddd = contact.value?.ddd?.toInt(),
        phoneNumber = contact.value?.phoneNumber,
        carrierLabel = contact.value?.carrier
    )
}