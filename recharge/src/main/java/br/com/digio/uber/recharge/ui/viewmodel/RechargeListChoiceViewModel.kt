package br.com.digio.uber.recharge.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.recharge.model.ListChoiceUiModel

class RechargeListChoiceViewModel : BaseViewModel() {
    val listChoice: MutableLiveData<List<ListChoiceUiModel>> = MutableLiveData()

    fun setListChoice(values: List<ListChoiceUiModel>) {
        listChoice.value = values
    }
}