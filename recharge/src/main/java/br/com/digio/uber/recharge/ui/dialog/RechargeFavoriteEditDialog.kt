package br.com.digio.uber.recharge.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import br.com.digio.uber.common.base.dialog.BaseDialog
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.recharge.R
import br.com.digio.uber.recharge.interfaces.FavoriteNickNameListener
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.recharge.navigation.RechargeOpKey
import br.com.digio.uber.util.safeHeritage
import br.com.digio.uber.util.safeLet
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.dialog_favorite_edit.view.recharge_edit_btn_save
import kotlinx.android.synthetic.main.dialog_favorite_edit.view.recharge_edit_edit_nickname

class RechargeFavoriteEditDialog : BaseDialog(), View.OnClickListener {

    private var position: Int? = null
    private var favoriteContactUiModel: FavoriteContactUiModel? = null

    override fun tag() = RechargeOpKey.RECHARGE_EDIT_FAVORITE.name

    override fun getStatusBarAppearance(): StatusBarColor = RechargeOpKey.RECHARGE_EDIT_FAVORITE.theme

    override fun initialize() {
        position = arguments?.getInt(POSITION)
        favoriteContactUiModel = arguments?.getSerializable(FAVORITE)?.safeHeritage<FavoriteContactUiModel>()
    }

    private lateinit var favoriteNickNameListener: FavoriteNickNameListener

    private lateinit var editNickname: TextInputEditText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_favorite_edit, container, false)

        view.recharge_edit_btn_save.setOnClickListener(this)

        editNickname = view.recharge_edit_edit_nickname

        return view
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.recharge_edit_btn_save -> {
                safeLet(position, favoriteContactUiModel) { position, favorite ->
                    favoriteNickNameListener.nickNameEdit(
                        favorite.copy(nickname = editNickname.text.toString()),
                        position
                    )
                }

                dismiss()
            }
        }
    }

    companion object {
        const val POSITION = "position"
        const val FAVORITE = "favorite"

        fun newInstance(
            favoriteNickNameListener: FavoriteNickNameListener,
            favoriteContactUiModel: FavoriteContactUiModel,
            position: Int
        ): RechargeFavoriteEditDialog {
            return RechargeFavoriteEditDialog().apply {
                arguments = bundleOf(
                    FAVORITE to favoriteContactUiModel,
                    POSITION to position
                )
                this.favoriteNickNameListener = favoriteNickNameListener
            }
        }
    }
}