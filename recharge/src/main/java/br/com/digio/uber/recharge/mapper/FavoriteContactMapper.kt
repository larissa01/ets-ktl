package br.com.digio.uber.recharge.mapper

import br.com.digio.uber.model.recharge.FavoriteContact
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.util.mapper.AbstractMapper

class FavoriteContactMapper :
    AbstractMapper<FavoriteContact, FavoriteContactUiModel> {
    override fun map(param: FavoriteContact): FavoriteContactUiModel = with(param) {
        FavoriteContactUiModel(
            phoneNumber = phoneNumber,
            ddd = ddd,
            subproductId = subproductId,
            nickname = nickname,
            carrier = carrier,
            carrierIcon = carrierIcon,
            document = document,
            productType = productType
        )
    }
}