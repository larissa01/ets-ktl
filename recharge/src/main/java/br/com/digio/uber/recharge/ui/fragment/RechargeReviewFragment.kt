package br.com.digio.uber.recharge.ui.fragment

import android.content.Intent
import android.view.View
import android.view.WindowManager
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.model.store.StoreOrder
import br.com.digio.uber.model.store.SubproductOrderResponseEnum
import br.com.digio.uber.recharge.BR
import br.com.digio.uber.recharge.R
import br.com.digio.uber.recharge.databinding.FragmentRechargeReviewBinding
import br.com.digio.uber.recharge.model.FavoriteContactUiModel
import br.com.digio.uber.recharge.navigation.RechargeOpKey
import br.com.digio.uber.recharge.ui.viewmodel.RechargeReviewViewModel
import br.com.digio.uber.util.Const.RequestOnResult.PID.PID_REQUEST_CODE
import br.com.digio.uber.util.Const.RequestOnResult.PID.RESULT_PID_SUCCESS
import br.com.digio.uber.util.safeHeritage
import org.koin.android.ext.android.inject

class RechargeReviewFragment : BaseRechargeFragment<FragmentRechargeReviewBinding, RechargeReviewViewModel>() {

    private val favoriteContact by lazy {
        arguments?.getSerializable(RechargeHomeFragment.FAVORITE_CONTACT)?.safeHeritage<FavoriteContactUiModel>()
    }

    private val order by lazy {
        arguments?.getSerializable(ORDER)?.safeHeritage<StoreOrder>()
    }

    override val bindingVariable: Int = BR.rechargeReviewViewModel

    override val getLayoutId: Int = R.layout.fragment_recharge_review

    override val viewModel: RechargeReviewViewModel by inject()

    override fun tag(): String = RechargeOpKey.RECHARGE_REVIEW.name

    override fun getStatusBarAppearance(): StatusBarColor = RechargeOpKey.RECHARGE_REVIEW.theme

    override fun getToolbar(): Toolbar? = binding?.rechargeReviewAppBar?.toolbarRechargeMain

    override fun getSoftInputMode() = WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN

    override fun getRequestCode(): MutableList<Int> = mutableListOf(PID_REQUEST_CODE)

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it) }

        favoriteContact?.let { viewModel.setContact(it) }
        order?.let { viewModel.setValue(it) }

        setSwitchListiner()

        setObserver()
    }

    private fun setSwitchListiner() {
        binding?.rechargeReviewSwRegister?.setOnCheckedChangeListener { _, isChecked ->
            viewModel.checkIfSaveContact(isChecked)
        }
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.recharge_review_btn_continue ->
                activity?.let {
                    navigation.navigationToPidActivity(it, PidType.RECHARGE, PID_REQUEST_CODE)
                }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_PID_SUCCESS && requestCode == PID_REQUEST_CODE) {
            viewModel.saveFavorite() {
                order?.let { order -> viewModel.finishRecharge(order) }
            }
        }
    }

    private fun setObserver() {
        viewModel.orderRegisterResponse.observe(
            this,
            Observer {
                if (it.responseCode == SubproductOrderResponseEnum.ACCEPTED.code) {
                    clearAllBackStack()
                    callGoTo(RechargeOpKey.RECHARGE_SUCCESS.name, null)
                }
            }
        )
    }

    companion object : FragmentNewInstance<RechargeReviewFragment>(::RechargeReviewFragment) {
        const val ORDER = "order"
    }
}