package br.com.digio.uber.recharge.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.recharge.GetFavoritesContactsUseCase
import br.com.digio.uber.domain.usecase.recharge.RemoveFavoriteContactUseCase
import br.com.digio.uber.domain.usecase.recharge.UpdateFavoriteContactUseCase
import br.com.digio.uber.model.recharge.FavoriteContact
import br.com.digio.uber.model.recharge.FavoriteContactList
import br.com.digio.uber.recharge.mapper.FavoriteContactListMapper
import br.com.digio.uber.recharge.model.FavoriteContactListUiModel

class RechargeContactsRegisteredViewModel constructor(
    private val getFavoritesContactsUseCase: GetFavoritesContactsUseCase,
    private val updateFavoriteContactUseCase: UpdateFavoriteContactUseCase,
    private val removeFavoriteContactUseCase: RemoveFavoriteContactUseCase
) : BaseViewModel() {

    val favoriteList: LiveData<FavoriteContactListUiModel>
        get() = _favoriteList.map(FavoriteContactListMapper())

    private val _favoriteList: MutableLiveData<FavoriteContactList> = MutableLiveData()

    val emptyList = MutableLiveData<Boolean>(false)

    fun getFavoriteList() {
        getFavoritesContactsUseCase(Unit).singleExec(
            onError = { _, error, _ ->
                emptyList.value = true
                error
            },
            onSuccessBaseViewModel = { favorites ->
                if (favorites.favoritePhoneResponses.isNullOrEmpty()) {
                    emptyList.value = true
                }

                favorites.favoritePhoneResponses?.sortedBy { it.nickname }
                _favoriteList.value = favorites
            }
        )
    }

    fun removeContact(favorite: FavoriteContact) {
        removeFavoriteContactUseCase(favorite).singleExec(
            onError = { _, error, _ ->
                error
            }
        )
    }

    fun editContact(favorite: FavoriteContact) {
        updateFavoriteContactUseCase(favorite).singleExec(
            onError = { _, error, _ ->
                error
            }
        )
    }

    fun showEmptyScreen() {
        emptyList.value = true
    }
}