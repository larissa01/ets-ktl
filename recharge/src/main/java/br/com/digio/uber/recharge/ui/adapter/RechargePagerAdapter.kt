package br.com.digio.uber.recharge.ui.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import br.com.digio.uber.recharge.R
import br.com.digio.uber.recharge.interfaces.CallFragment
import br.com.digio.uber.recharge.ui.fragment.RechargeContactsRegisteredFragment
import br.com.digio.uber.recharge.ui.fragment.RechargeNewFragment

class RechargePagerAdapter(
    fm: FragmentManager,
    private val context: Context,
    callFragment: CallFragment
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val fragments: List<Fragment> by lazy {
        listOf(
            RechargeNewFragment.newInstance(callFragment),
            RechargeContactsRegisteredFragment.newInstance(callFragment)
        )
    }

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size

    override fun getPageTitle(position: Int) =
        if (position == 0) {
            context.getString(R.string.recharge_new)
        } else {
            context.getString(R.string.registered)
        }
}