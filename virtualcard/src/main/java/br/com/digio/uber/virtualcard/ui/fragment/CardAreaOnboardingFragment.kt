package br.com.digio.uber.virtualcard.ui.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.NavHostFragment.findNavController
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.pop
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.virtualcard.BR
import br.com.digio.uber.virtualcard.R
import br.com.digio.uber.virtualcard.databinding.CardAreaOnboardingFragmentBinding
import br.com.digio.uber.virtualcard.ui.viewmodel.CardAreaOnboardingViewModel
import org.koin.android.ext.android.inject

class CardAreaOnboardingFragment : BaseViewModelFragment<CardAreaOnboardingFragmentBinding,
    CardAreaOnboardingViewModel>() {
    override val bindingVariable: Int? = BR.cardAreaOnboardingViewModel
    override val getLayoutId: Int? = R.layout.card_area_onboarding_fragment
    override val viewModel: CardAreaOnboardingViewModel by inject()
    override fun tag(): String = CardAreaOnboardingFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.YELLOW
    override fun getToolbar(): Toolbar? = binding?.appBarAreaCard?.toolbarCardDefault.apply {
        this?.navigationIcon = context?.let {
            ContextCompat.getDrawable(
                it,
                br.com.digio.uber.common.R.drawable.ic_back_arrow_black
            )
        }
    }

    override fun initialize() {
        super.initialize()
        viewModel.initializer { onItemClicked(it) }
        binding?.appBarAreaCard?.appBarCardDefault?.bringToFront()
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.btnOnboardingNext -> {
                viewModel.finishOnboarding(true)
                findNavController(this).navigate(
                    CardAreaOnboardingFragmentDirections.actionCardAreaOnboardingFragmentToCardAreaFragment()
                )
            }
        }
    }

    override fun onNavigationClick(view: View) {
        pop()
    }

    override fun onBackPressed() {
        pop()
    }
}