package br.com.digio.uber.virtualcard.uimodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VirtualCardUiModel constructor(
    val cardId: Long,
    val cardName: String?,
    val cardNumber: String?,
    val cardSecurityCode: String?,
    val cardValidDate: String?,
    val isBlock: Boolean,
    val cardNumberFinalDigit: String?,
    val flagTokenization: Boolean?,
    val labelBlock: String?,
    val cardImage: CardImageUiModel,
    val cardNumberFormatted: String?
) : Parcelable