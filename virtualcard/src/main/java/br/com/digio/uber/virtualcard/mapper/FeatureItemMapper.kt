package br.com.digio.uber.virtualcard.mapper

import br.com.digio.uber.common.util.FeatureInfoRoute
import br.com.digio.uber.model.config.FeatureItem
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.virtualcard.uimodel.FeatureItemUI
import br.com.digio.uber.virtualcard.uimodel.FeatureItemUiModel

class FeatureItemMapper : AbstractMapper<Pair<List<FeatureItem>, Int>, List<FeatureItemUiModel>> {
    override fun map(param: Pair<List<FeatureItem>, Int>): List<FeatureItemUiModel> {
        var featureItemUiModelList: List<FeatureItemUiModel> = listOf()

        param.first.map {
            featureItemUiModelList.apply {
                FeatureInfoRoute.getFeature(it.id)?.let { featureRoute ->
                    featureItemUiModelList = this.plus(
                        FeatureItemUI(
                            label = it.label,
                            image = featureRoute.iconDefault,
                            activity = featureRoute.route,
                            bundle = featureRoute.bundle.apply {
                                this?.putInt("cardId", param.second)
                            }
                        )
                    )
                }
            }
        }

        return featureItemUiModelList
    }
}