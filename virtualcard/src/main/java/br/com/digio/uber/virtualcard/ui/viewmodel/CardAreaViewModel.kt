package br.com.digio.uber.virtualcard.ui.viewmodel

import android.graphics.drawable.Drawable
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.GenericEvent
import br.com.digio.uber.domain.usecase.card.CardUseCaseTypeBlock
import br.com.digio.uber.domain.usecase.card.GetCardListUseCase
import br.com.digio.uber.domain.usecase.card.PostCardBlockUseCase
import br.com.digio.uber.domain.usecase.home.ConfigFeatureType
import br.com.digio.uber.domain.usecase.home.ConfigFeatureUseCase
import br.com.digio.uber.domain.usecase.virtualcard.PostVirtualCardCurrentUseCase
import br.com.digio.uber.model.card.TypeCard
import br.com.digio.uber.virtualcard.R
import br.com.digio.uber.virtualcard.mapper.CardMapper
import br.com.digio.uber.virtualcard.mapper.FeatureItemMapper
import br.com.digio.uber.virtualcard.uimodel.CardUiModel
import br.com.digio.uber.virtualcard.uimodel.FeatureItemUiModel

class CardAreaViewModel constructor(
    val getCardListUseCase: GetCardListUseCase,
    val postCardBlockUseCase: PostCardBlockUseCase,
    val configFeatureUseCase: ConfigFeatureUseCase,
    val postVirtualCardCurrentUseCase: PostVirtualCardCurrentUseCase,
    val resourceManager: ResourceManager
) : BaseViewModel() {
    val cardUi = MutableLiveData<CardUiModel?>()
    val virtualCard = MutableLiveData<CardUiModel?>()
    val shortcutCardTrackList = MutableLiveData<List<FeatureItemUiModel>>()
    val lockerIcon = MutableLiveData<Drawable>()

    val updateValues: MutableLiveData<Boolean> = MutableLiveData()
    val isRefresh: MutableLiveData<Boolean> = MutableLiveData()

    val onRefresh: GenericEvent = {
        getCardAll()
        updateValues.value = true
    }

    fun getCardAll() {
        getCardListUseCase(Unit).singleExec(
            onError = { _, _, _ ->
                ErrorObject().apply {
                    message = R.string.card_area_card_not_found
                    close = true
                }
            },
            onSuccessBaseViewModel = { response ->
                val cardList = CardMapper().map(response)
                cardUi.value = cardList.find { it.typeCard == TypeCard.DEBIT_ELO }
                virtualCard.value = cardList.find { it.typeCard == TypeCard.VIRTUAL_DEBIT_ELO }

                virtualCard.value?.let {
                    postVirtualCardCurrentUseCase(it.cardId).singleExec()
                }

                cardUi.value?.let {
                    if (it.showShortcutTracking) {
                        loadShortcutTrackingCardList(it.cardStatusId)
                    }
                }
                cardUi.value?.let {
                    changeLockerIcon(it.isBlock)
                }
            },
            onCompletionBaseViewModel = {
                isRefresh.value = false
            }
        )
    }

    private fun loadShortcutTrackingCardList(cardId: Int) {
        configFeatureUseCase(ConfigFeatureType.CONFIG_FEATURE_TYPE_SHORTCUT_TRACKING_CARD_LIST).singleExec(
            onSuccessBaseViewModel = { response ->
                shortcutCardTrackList.value = FeatureItemMapper().map(Pair(response, cardId))
            }
        )
    }

    fun cardBlockTemp() {
        cardUi.value?.let { card ->
            if (card.isBlock) {
                postCardBlockUseCase(
                    Pair(
                        CardUseCaseTypeBlock.UNLOCK,
                        card.cardId
                    )
                ).singleExec(
                    onSuccessBaseViewModel = {
                        getCardAll()
                    },
                    onError = { _, error, _ ->
                        error.apply {
                            onPositiveButtonClick = {
                                getCardAll()
                            }
                        }
                    }
                )
            } else postCardBlockUseCase(
                Pair(
                    CardUseCaseTypeBlock.BLOCK,
                    card.cardId
                )
            ).singleExec(
                onSuccessBaseViewModel = {
                    getCardAll()
                },
                onError = { _, error, _ ->
                    error.apply {
                        onPositiveButtonClick = {
                            getCardAll()
                        }
                    }
                }
            )
        }
    }

    private fun changeLockerIcon(value: Boolean) {
        if (value) {
            lockerIcon.value = resourceManager.getDrawable(R.drawable.ic_block_temporary_closed)
        } else {
            lockerIcon.value = resourceManager.getDrawable(R.drawable.ic_block_temporary_open)
        }
    }
}