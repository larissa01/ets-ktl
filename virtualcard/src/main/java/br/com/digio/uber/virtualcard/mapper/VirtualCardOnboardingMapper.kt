package br.com.digio.uber.virtualcard.mapper

import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.model.virtualCard.VirtualCardOnboardingResponse
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.virtualcard.R
import br.com.digio.uber.virtualcard.uimodel.VirtualCardOnboardingUiModel

class VirtualCardOnboardingMapper constructor(
    private val resourceManager: ResourceManager
) : AbstractMapper<VirtualCardOnboardingResponse, List<VirtualCardOnboardingUiModel>> {
    override fun map(param: VirtualCardOnboardingResponse): List<VirtualCardOnboardingUiModel> {
        val virtualCardOnboardingList = mutableListOf<VirtualCardOnboardingUiModel>()

        param.onboardingList.forEach {
            virtualCardOnboardingList.add(
                VirtualCardOnboardingUiModel(
                    drawable = when (it.screen) {
                        0 -> R.drawable.ic_virtual_card_onboarding_01
                        1 -> R.drawable.ic_virtual_card_onboarding_02
                        else -> R.drawable.ic_virtual_card_onboarding_03
                    },
                    title = it.title,
                    description = it.description,
                    btnText = resourceManager.getString(R.string.virtual_card_onboarding_btn_next)
                )
            )
        }

        virtualCardOnboardingList.first().titleVisibility = true
        virtualCardOnboardingList.last().btnText = resourceManager.getString(R.string.virtual_card_onboarding_btn_start)

        return virtualCardOnboardingList.toList()
    }
}