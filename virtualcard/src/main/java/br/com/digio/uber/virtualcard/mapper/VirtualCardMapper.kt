package br.com.digio.uber.virtualcard.mapper

import br.com.digio.uber.model.card.CardStatus
import br.com.digio.uber.model.card.TypeCard
import br.com.digio.uber.model.card.VirtualCardResponse
import br.com.digio.uber.util.getFinalDigitsCardNumber
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.virtualcard.uimodel.VirtualCardUiModel
import br.com.digio.uber.virtualcard.util.CardUtil
import br.com.digio.uber.virtualcard.util.CardUtil.formatLabelBlock

class VirtualCardMapper : AbstractMapper<VirtualCardResponse, VirtualCardUiModel> {

    private companion object {
        const val CARD_DIGITS_SEPARATOR = 4
        const val CARD_LENGTH = 16
        const val FOR_STEP = 4
        const val FOR_MAX_LOOP = 15
        const val FOR_START_LOOP = 0
    }

    override fun map(param: VirtualCardResponse): VirtualCardUiModel {
        return VirtualCardUiModel(
            cardId = param.id,
            cardName = param.cardholderName,
            cardNumber = param.cardNumber,
            cardSecurityCode = param.cvv2,
            cardValidDate = "${param.expiry.month}/${param.expiry.year}",
            cardNumberFinalDigit = param.cardNumber.getFinalDigitsCardNumber(),
            flagTokenization = param.flagDebitTokenization,
            isBlock = param.statusId != CardStatus.NORMAL.id,
            labelBlock = formatLabelBlock(param.statusId != CardStatus.NORMAL.id),
            cardImage = CardUtil.getImageTypeCard(TypeCard.VIRTUAL_DEBIT_ELO),
            cardNumberFormatted = formatCardNumber(param.cardNumber)
        )
    }

    private fun formatCardNumber(number: String?): String {
        var numberFormatted = ""
        if (!number.isNullOrEmpty() && number.length == CARD_LENGTH) {
            for (i in FOR_START_LOOP..FOR_MAX_LOOP step FOR_STEP) {
                numberFormatted += number.substring(i, i + CARD_DIGITS_SEPARATOR).plus(" ")
            }
        }
        return numberFormatted
    }
}