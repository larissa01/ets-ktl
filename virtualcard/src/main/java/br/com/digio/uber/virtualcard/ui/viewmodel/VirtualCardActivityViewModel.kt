package br.com.digio.uber.virtualcard.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.virtualcard.GetCardOnboardingFirstAccessUseCase

class VirtualCardActivityViewModel constructor(
    private val getCardOnboardingFirstAccessUseCase: GetCardOnboardingFirstAccessUseCase
) : BaseViewModel() {
    val showCardOnboarding = MutableLiveData<Boolean>()
    val pidHash = MutableLiveData<String>()

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        checkCardOnboarding()
    }

    private fun checkCardOnboarding() {
        getCardOnboardingFirstAccessUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                showCardOnboarding.value = it
            }
        )
    }
}