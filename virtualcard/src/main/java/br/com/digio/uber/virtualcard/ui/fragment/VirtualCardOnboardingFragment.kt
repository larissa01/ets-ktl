package br.com.digio.uber.virtualcard.ui.fragment

import android.util.Log
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.viewpager2.widget.ViewPager2
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.util.Const
import br.com.digio.uber.virtualcard.BR
import br.com.digio.uber.virtualcard.R
import br.com.digio.uber.virtualcard.databinding.VirtualCardOnboardingFragmentBinding
import br.com.digio.uber.virtualcard.ui.adapter.OnClickVirtualCardOnboardingItem
import br.com.digio.uber.virtualcard.ui.adapter.VirtualCardOnboardingAdapter
import br.com.digio.uber.virtualcard.ui.viewmodel.VirtualCardOnboardingViewModel
import br.com.digio.uber.virtualcard.uimodel.VirtualCardOnboardingUi
import com.google.android.material.tabs.TabLayoutMediator
import org.koin.android.ext.android.inject

class VirtualCardOnboardingFragment :
    BaseViewModelFragment<VirtualCardOnboardingFragmentBinding, VirtualCardOnboardingViewModel>(),
    OnClickVirtualCardOnboardingItem {

    override val bindingVariable: Int? = BR.virtualCardOnboardingViewModel
    override val getLayoutId: Int? = R.layout.virtual_card_onboarding_fragment
    override val viewModel: VirtualCardOnboardingViewModel? by inject()
    override fun tag(): String = VirtualCardOnboardingFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.WHITE
    override val showDisplayShowTitle: Boolean = true
    override val showHomeAsUp: Boolean = true

    override fun getToolbar(): Toolbar? =
        binding?.appBarVirtualCard?.toolbarCardDefault?.apply {
            title = getString(R.string.virtual_card_onboarding_title)
        }

    private lateinit var adapter: VirtualCardOnboardingAdapter

    override fun initialize() {
        super.initialize()
        viewModel?.initializer { btnNextOnClick(it) }
        setupObservables()
        setupViewPagerOnChangeEvent()
    }

    private fun btnNextOnClick(v: View) {
        when (v.id) {
            R.id.btnVirtualCardOnboardingNext -> {
                if (binding?.viewPagerVirtualCardOnboarding?.currentItem == 2) {
                    viewModel?.postVirtualCardOnboardingVerification(true)
                } else {
                    binding?.viewPagerVirtualCardOnboarding?.run {
                        currentItem = currentItem.inc()
                    }
                }
            }
        }
    }

    private fun setupObservables() {
        viewModel?.onboardingContentList?.observe(
            this,
            {
                if (!it.isNullOrEmpty()) {
                    adapter = VirtualCardOnboardingAdapter(it.toMutableList(), this)
                    binding?.viewPagerVirtualCardOnboarding?.adapter = adapter
                    setupViewPagerIndicator()
                }
            }
        )
        viewModel?.isOnboardingDone?.observe(
            this,
            {
                if (it) {
                    activity?.let { fragmentActivity ->
                        navigation.navigationToPidActivity(
                            fragmentActivity,
                            PidType.VIRTUAL_CARD,
                            Const.RequestOnResult.PID.PID_REQUEST_CODE
                        )
                    }
                }
            }
        )
    }

    private fun setupViewPagerOnChangeEvent() {
        binding?.viewPagerVirtualCardOnboarding?.registerOnPageChangeCallback(
            object :
                ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    when (position) {
                        1 -> {
                            binding?.btnVirtualCardOnboardingNext?.text =
                                getString(R.string.virtual_card_onboarding_btn_next)
                        }
                        2 -> {
                            binding?.btnVirtualCardOnboardingNext?.text =
                                getString(R.string.virtual_card_onboarding_btn_start)
                        }
                    }
                }
            }
        )
    }

    private fun setupViewPagerIndicator() {
        binding?.tabLayoutVirtualCardOnboarding?.let { tl ->
            binding?.viewPagerVirtualCardOnboarding?.let { vp ->
                TabLayoutMediator(tl, vp) { _, _ -> }.attach()
            }
        }
    }

    override fun invoke(item: VirtualCardOnboardingUi) {
        Log.i("", "")
    }

    override fun onNavigationClick(view: View) {
        activity?.finish()
    }

    override fun onBackPressed() {
        activity?.finish()
    }
}