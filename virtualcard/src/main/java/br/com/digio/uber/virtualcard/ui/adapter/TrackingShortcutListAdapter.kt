package br.com.digio.uber.virtualcard.ui.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage
import br.com.digio.uber.virtualcard.R
import br.com.digio.uber.virtualcard.databinding.LayoutCardTrackingShortcutItemBinding
import br.com.digio.uber.virtualcard.ui.adapter.viewholder.TrackingShortcutListViewHolder
import br.com.digio.uber.virtualcard.uimodel.FeatureItemUI
import br.com.digio.uber.virtualcard.uimodel.FeatureItemUiModel

typealias OnClickShortcutItem = (item: FeatureItemUiModel) -> Unit

class TrackingShortcutListAdapter(
    private var values: MutableList<FeatureItemUiModel> = mutableListOf(),
    private val listener: OnClickShortcutItem
) : BaseRecyclerViewAdapter<FeatureItemUiModel, TrackingShortcutListAdapter.ViewTypesDataBindingFactoryImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl =
        ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): FeatureItemUiModel =
        values[position]

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<FeatureItemUiModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<FeatureItemUiModel>
    }

    override fun getItemCount(): Int =
        values.size

    override fun onBindViewHolder(
        holder: AbstractViewHolder<FeatureItemUiModel>,
        position: Int
    ) = holder.bind(values[holder.adapterPosition])

    class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<FeatureItemUiModel> {
        override fun type(model: FeatureItemUiModel): Int =
            when (model) {
                is FeatureItemUI -> R.layout.layout_card_tracking_shortcut_item
            }

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<FeatureItemUiModel>
        ): AbstractViewHolder<*> =
            when (type) {
                R.layout.layout_card_tracking_shortcut_item -> TrackingShortcutListViewHolder(
                    view.safeHeritage<LayoutCardTrackingShortcutItemBinding>()!!,
                    listener
                )
                else -> throw IndexOutOfBoundsException("Invalid view type")
            }
    }

    override fun replaceItems(list: List<Any>) {
        addValues(list.safeHeritage())
    }

    private fun addValues(values: List<FeatureItemUiModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }
}