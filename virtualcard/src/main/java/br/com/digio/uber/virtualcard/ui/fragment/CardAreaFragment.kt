package br.com.digio.uber.virtualcard.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.extensions.stateGetLiveData
import br.com.digio.uber.common.extensions.stateSetValue
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const
import br.com.digio.uber.virtualcard.BR
import br.com.digio.uber.virtualcard.R
import br.com.digio.uber.virtualcard.databinding.CardAreaFragmentBinding
import br.com.digio.uber.virtualcard.ui.adapter.TrackingShortcutListAdapter
import br.com.digio.uber.virtualcard.ui.viewmodel.CardAreaViewModel
import br.com.digio.uber.virtualcard.uimodel.FeatureItemUI
import kotlinx.android.synthetic.main.layout_card_status.*
import org.koin.android.ext.android.inject

@Suppress("TooManyFunctions")
class CardAreaFragment : BaseViewModelFragment<CardAreaFragmentBinding, CardAreaViewModel>() {
    override val bindingVariable: Int = BR.cardAreaViewModel
    override val getLayoutId: Int = R.layout.card_area_fragment
    override val viewModel: CardAreaViewModel by inject()

    override fun tag(): String = CardAreaFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor =
        StatusBarColor.WHITE

    override val showDisplayShowTitle: Boolean = true
    override val showHomeAsUp: Boolean = true
    override fun isSecurityScreen(): Boolean = true
    override fun getToolbar(): Toolbar? =
        binding?.appBarAreaCard?.toolbarCardDefault?.apply {
            title = getString(R.string.card_area_fragment_title)
        }

    private val adapter: TrackingShortcutListAdapter by lazy {
        TrackingShortcutListAdapter(
            listener = { value ->
                if (value is FeatureItemUI) {
                    navigation.navigationToDynamic(requireActivity(), value.activity, value.bundle)
                }
            }
        )
    }

    override fun initialize() {
        super.initialize()
        setupAdapter()
        setupObserve()
        viewModel.initializer { onItemClicked(it) }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getCardAll()
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.cardAreaItenOpenVirtualCard -> {
                openVirtualCard()
            }
            R.id.switchCardBlock -> {
                viewModel.cardBlockTemp()
            }
            R.id.cardAreaItencardreissue -> {
                navigation.navigationToDynamic(requireActivity(), Const.Activities.CARD_REISSUE_ACTIVITY, Bundle())
            }
        }
    }

    private fun setupObserve() {
        stateGetLiveData<Boolean>(
            Const.RequestOnResult
                .FeatureRefreshCardArea.FEATURE_ITEM_RESULT_SUCCESS_CARD_AREA
        )?.observe(
            this,
            Observer {
                if (it == true) {
                    viewModel.onRefresh()
                    stateSetValue(
                        Const.RequestOnResult.FeatureRefreshCardArea.FEATURE_ITEM_RESULT_SUCCESS_CARD_AREA,
                        false
                    )
                }
            }
        )
    }

    private fun openVirtualCard() {
        viewModel.virtualCard.value?.let {
            NavHostFragment.findNavController(this).navigate(
                CardAreaFragmentDirections.actionCardAreaFragmentToVirtualCardOnboardingFragment()
            )
        } ?: run {
            showMessage(
                ErrorObject().apply {
                    message = R.string.card_area_virtual_card_erro_get
                },
                true
            )
        }
    }

    override fun onNavigationClick(view: View) {
        activity?.finish()
    }

    override fun onBackPressed() {
        activity?.finish()
    }

    private fun setupAdapter() {
        binding?.apply {
            recyclerCardTrackingShortcut.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            recyclerCardTrackingShortcut.adapter = adapter
        }
    }
}