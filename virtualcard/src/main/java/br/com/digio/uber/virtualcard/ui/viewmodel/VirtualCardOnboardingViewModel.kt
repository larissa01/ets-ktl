package br.com.digio.uber.virtualcard.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.virtualcard.GetVerifyVirtualCardOnboardingUseCase
import br.com.digio.uber.domain.usecase.virtualcard.GetVirtualCardOnboardingUseCase
import br.com.digio.uber.domain.usecase.virtualcard.PostVerifyVirtualCardOnboardingUseCase
import br.com.digio.uber.virtualcard.mapper.VirtualCardOnboardingMapper
import br.com.digio.uber.virtualcard.uimodel.VirtualCardOnboardingUiModel

class VirtualCardOnboardingViewModel constructor(
    private val getVirtualCardOnboardingUseCase: GetVirtualCardOnboardingUseCase,
    private val postVerifyVirtualCardOnboardingUseCase: PostVerifyVirtualCardOnboardingUseCase,
    private val getVerifyVirtualCardOnboardingUseCase: GetVerifyVirtualCardOnboardingUseCase,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    val onboardingContentList = MutableLiveData<List<VirtualCardOnboardingUiModel>>()
    val isOnboardingDone = MutableLiveData<Boolean>()

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        getVirtualCardOnboardingVerification()
    }

    private fun getVirtualCardOnboardingVerification() {
        getVerifyVirtualCardOnboardingUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                isOnboardingDone.value = it
                if (!it) {
                    loadOnboardingContent()
                }
            }
        )
    }

    private fun loadOnboardingContent() {
        getVirtualCardOnboardingUseCase(Unit).singleExec(
            onSuccessBaseViewModel = {
                onboardingContentList.value = VirtualCardOnboardingMapper(resourceManager).map(it)
            }
        )
    }

    fun postVirtualCardOnboardingVerification(isDone: Boolean) {
        postVerifyVirtualCardOnboardingUseCase(isDone).singleExec(
            onSuccessBaseViewModel = {
                isOnboardingDone.value = it
            }
        )
    }
}