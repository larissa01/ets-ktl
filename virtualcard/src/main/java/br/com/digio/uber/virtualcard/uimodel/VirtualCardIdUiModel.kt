package br.com.digio.uber.virtualcard.uimodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VirtualCardIdUiModel constructor(
    val cardId: Long?
) : Parcelable