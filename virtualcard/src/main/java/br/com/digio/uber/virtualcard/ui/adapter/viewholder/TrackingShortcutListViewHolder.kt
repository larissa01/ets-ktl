package br.com.digio.uber.virtualcard.ui.adapter.viewholder

import android.annotation.SuppressLint
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.virtualcard.databinding.LayoutCardTrackingShortcutItemBinding
import br.com.digio.uber.virtualcard.uimodel.FeatureItemUI
import br.com.digio.uber.virtualcard.uimodel.FeatureItemUiModel

class TrackingShortcutListViewHolder(
    private val viewDataBinding: LayoutCardTrackingShortcutItemBinding,
    private val listener: ViewTypesListener<FeatureItemUiModel>
) : AbstractViewHolder<FeatureItemUI>(viewDataBinding.root) {

    @SuppressLint("DefaultLocale")
    override fun bind(item: FeatureItemUI) {
        viewDataBinding.sthortcuItem = item
        viewDataBinding.sthortcuListItemViewHolder = this
        viewDataBinding.executePendingBindings()
    }

    fun onClick(item: FeatureItemUI) {
        listener(item)
    }
}