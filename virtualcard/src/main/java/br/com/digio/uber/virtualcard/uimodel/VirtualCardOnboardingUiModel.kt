package br.com.digio.uber.virtualcard.uimodel

import android.os.Parcelable
import androidx.annotation.DrawableRes
import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import kotlinx.android.parcel.Parcelize

sealed class VirtualCardOnboardingUi : AdapterViewModel<VirtualCardOnboardingUi> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<VirtualCardOnboardingUi>) = typesFactory.type(
        model = this
    )
}

@Parcelize
data class VirtualCardOnboardingUiModel(
    @DrawableRes val drawable: Int,
    val title: String,
    val description: String,
    var titleVisibility: Boolean? = false,
    var btnText: String
) : Parcelable, VirtualCardOnboardingUi()