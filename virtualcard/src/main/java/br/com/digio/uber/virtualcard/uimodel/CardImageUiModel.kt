package br.com.digio.uber.virtualcard.uimodel

import android.os.Parcelable
import br.com.digio.uber.common.R
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CardImageUiModel(
    val backgroundImage: Int = R.drawable.ic_card_debit,
    val flagImage: Int? = R.drawable.ic_logo_elo
) : Parcelable