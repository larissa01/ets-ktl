package br.com.digio.uber.virtualcard.util

import br.com.digio.uber.common.R
import br.com.digio.uber.model.card.TypeCard
import br.com.digio.uber.virtualcard.uimodel.CardImageUiModel

object CardUtil {

    fun getImageTypeCard(typeCard: TypeCard): CardImageUiModel =
        when (typeCard) {
            TypeCard.DEBIT_ELO -> CardImageUiModel(
                backgroundImage = R.drawable.ic_card_debit,
                flagImage = R.drawable.ic_logo_elo
            )
            TypeCard.VIRTUAL_DEBIT_ELO -> CardImageUiModel(
                backgroundImage = R.drawable.ic_card_debit,
                flagImage = R.drawable.ic_logo_elo
            )
            else -> CardImageUiModel(
                backgroundImage = R.drawable.ic_card_debit,
                flagImage = R.drawable.ic_logo_elo
            )
        }

    fun formatLabelBlock(isBlock: Boolean): String =
        if (isBlock) "Status: Bloqueado"
        else "Status: Desbloqueado"
}