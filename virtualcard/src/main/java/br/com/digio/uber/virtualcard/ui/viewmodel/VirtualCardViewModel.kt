package br.com.digio.uber.virtualcard.ui.viewmodel

import android.graphics.drawable.Drawable
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.generics.makeMessageSuccessObject
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.virtualcard.GetVirtualCardCurrentUseCase
import br.com.digio.uber.domain.usecase.virtualcard.GetVirtualCardUseCase
import br.com.digio.uber.domain.usecase.virtualcard.PostVirtualCardBlockUseCase
import br.com.digio.uber.domain.usecase.virtualcard.PostVirtualCardCurrentUseCase
import br.com.digio.uber.domain.usecase.virtualcard.PostVirtualCardRegenerateUseCase
import br.com.digio.uber.domain.usecase.virtualcard.VirtualCardUseCaseTypeBlock
import br.com.digio.uber.virtualcard.R
import br.com.digio.uber.virtualcard.mapper.VirtualCardMapper
import br.com.digio.uber.virtualcard.uimodel.VirtualCardUiModel
import br.com.digio.uber.virtualcard.util.CardUtil

class VirtualCardViewModel constructor(
    val getVirtualCardTypeUseCase: GetVirtualCardUseCase,
    val postVirtualCardRegenerateUseCase: PostVirtualCardRegenerateUseCase,
    val postVirtualCardBlockUseCase: PostVirtualCardBlockUseCase,
    val getVirtualCardCurrentUseCase: GetVirtualCardCurrentUseCase,
    val postVirtualCardCurrentUseCase: PostVirtualCardCurrentUseCase,
    val resourceManager: ResourceManager
) : BaseViewModel() {
    val virtualCardUiModel: MutableLiveData<VirtualCardUiModel> = MutableLiveData<VirtualCardUiModel>(null)
    val carId: MutableLiveData<Long?> = MutableLiveData(null)
    val hash = MutableLiveData("")
    val lockerIcon = MutableLiveData<Drawable>()

    fun initializer(onClickItem: OnClickItem, hash: String) {
        super.initializer(onClickItem)
        this.hash.value = hash
        getVirtualCardCurrent()
    }

    private fun getVirtualCardCurrent() {
        getVirtualCardCurrentUseCase("").singleExec(
            onError = { _, error, _ ->
                ErrorObject().apply {
                    close = false
                }
            },
            onSuccessBaseViewModel = {
                carId.value = it
                loadVirtual()
            }
        )
    }

    private fun loadVirtual() {
        carId.value?.let { cardId ->
            getVirtualCardTypeUseCase(Pair(cardId, hash.value!!)).singleExec(
                onError = { _, _, _ ->
                    ErrorObject().apply {
                        close = false
                    }
                },
                onSuccessBaseViewModel = {
                    virtualCardUiModel?.value = VirtualCardMapper().map(it)
                    virtualCardUiModel.value?.let { vcm ->
                        changeLockerIcon(vcm.isBlock)
                    }
                }
            )
        }
    }

    fun regenerateVirtualCard() {
        carId.value?.let { cardId ->
            postVirtualCardRegenerateUseCase(cardId).singleExec(
                onSuccessBaseViewModel = {
                    virtualCardUiModel.value = VirtualCardMapper().map(it)
                    carId.value = it.id
                    postVirtualCardCurrentUseCase(it.id)
                    message.value = makeMessageSuccessObject {
                        title = R.string.virtual_card_fragment_regenerate_success_message_title
                        message = R.string.virtual_card_fragment_regenerate_success_message_subtitle
                    }
                }
            )
        }
    }

    fun virtualCardBlock() {
        virtualCardUiModel.value?.let { virtualCardUiModelLet ->
            if (virtualCardUiModelLet.isBlock) {
                postVirtualCardBlockUseCase(
                    Pair(
                        VirtualCardUseCaseTypeBlock.UNLOCK,
                        virtualCardUiModelLet.cardId
                    )
                ).singleExec(
                    onSuccessBaseViewModel = {
                        virtualCardUiModel.value = virtualCardUiModelLet.copy(
                            isBlock = false,
                            labelBlock = CardUtil.formatLabelBlock(false)
                        )
                        changeLockerIcon(false)
                    },
                    onError = { _, error, _ ->
                        virtualCardUiModel.value = virtualCardUiModelLet.copy(
                            isBlock = true,
                            labelBlock = CardUtil.formatLabelBlock(true)
                        )

                        error.apply {
                            close = false
                        }
                    }
                )
            } else postVirtualCardBlockUseCase(
                Pair(
                    VirtualCardUseCaseTypeBlock.BLOCK,
                    virtualCardUiModelLet.cardId
                )
            ).singleExec(
                onSuccessBaseViewModel = {
                    virtualCardUiModel.value = virtualCardUiModelLet.copy(
                        isBlock = true,
                        labelBlock = CardUtil.formatLabelBlock(true)
                    )
                    changeLockerIcon(true)
                },
                onError = { _, error, _ ->
                    virtualCardUiModel.value = virtualCardUiModelLet.copy(
                        isBlock = false,
                        labelBlock = CardUtil.formatLabelBlock(false)
                    )
                    error.apply {
                        close = false
                    }
                }
            )
        }
    }

    private fun changeLockerIcon(value: Boolean) {
        if (value) {
            lockerIcon.value = resourceManager.getDrawable(R.drawable.ic_block_temporary_closed)
        } else {
            lockerIcon.value = resourceManager.getDrawable(R.drawable.ic_block_temporary_open)
        }
    }
}