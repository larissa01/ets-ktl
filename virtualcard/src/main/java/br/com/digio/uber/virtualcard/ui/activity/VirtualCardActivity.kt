package br.com.digio.uber.virtualcard.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.core.os.bundleOf
import br.com.digio.uber.common.base.activity.BaseJetNavigationActivity
import br.com.digio.uber.common.extensions.navigateTo
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.util.Const
import br.com.digio.uber.virtualcard.R
import br.com.digio.uber.virtualcard.di.VirtualCardViewModelModule
import br.com.digio.uber.virtualcard.ui.viewmodel.VirtualCardActivityViewModel
import org.koin.android.ext.android.inject
import org.koin.core.module.Module

class VirtualCardActivity : BaseJetNavigationActivity() {

    override fun getLayoutId(): Int? = LayoutIds.navigationLayoutActivity
    override fun navGraphId(): Int? = R.navigation.nav_virtual_card
    override fun getModule(): List<Module>? = listOf(VirtualCardViewModelModule)

    private val viewModel: VirtualCardActivityViewModel? by inject()

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        viewModel?.initializer {}
        setupObservables()
    }

    private fun setupObservables() {
        viewModel?.showCardOnboarding?.observe(
            this,
            {
                if (it == false) {
                    navHostFragmentDefault.navigateTo(
                        R.id.action_cardAreaFragment_to_CardAreaOnboarding,
                        bundleOf()
                    )
                }
            }
        )
        viewModel?.pidHash?.observe(
            this,
            {
                if (!it.isNullOrEmpty()) {
                    navHostFragmentDefault.navigateTo(
                        R.id.action_virtualCardOnboardingFragment_to_virtualCardFragment,
                        bundleOf(Const.RequestOnResult.PID.KEY_PID_HASH to it)
                    )
                }
            }
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Const.RequestOnResult.PID.PID_REQUEST_CODE) {
            if (resultCode == Const.RequestOnResult.PID.RESULT_PID_SUCCESS &&
                data != null && data.hasExtra(Const.RequestOnResult.PID.KEY_PID_HASH)
            ) {
                viewModel?.pidHash?.postValue(data.extras?.getString(Const.RequestOnResult.PID.KEY_PID_HASH))
            } else showMessageAccessDenied()
        } else showMessageAccessDenied()
    }

    private fun showMessageAccessDenied() =
        showMessage(
            ErrorObject().apply {
                title = R.string.card_area_card_access_denied_title
                message = R.string.card_area_card_access_denied_subtitle
            }
        )
}