package br.com.digio.uber.virtualcard.ui.viewmodel

import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.virtualcard.PostCardOnboardingFirstAccessUseCase

class CardAreaOnboardingViewModel constructor(
    private val postCardOnboardingFirstAccessUseCase: PostCardOnboardingFirstAccessUseCase
) : BaseViewModel() {
    fun finishOnboarding(isDone: Boolean) =
        postCardOnboardingFirstAccessUseCase(isDone).singleExec()
}