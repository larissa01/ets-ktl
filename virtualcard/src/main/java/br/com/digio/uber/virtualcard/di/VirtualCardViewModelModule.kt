package br.com.digio.uber.virtualcard.di

import br.com.digio.uber.virtualcard.ui.viewmodel.CardAreaOnboardingViewModel
import br.com.digio.uber.virtualcard.ui.viewmodel.CardAreaViewModel
import br.com.digio.uber.virtualcard.ui.viewmodel.VirtualCardActivityViewModel
import br.com.digio.uber.virtualcard.ui.viewmodel.VirtualCardOnboardingViewModel
import br.com.digio.uber.virtualcard.ui.viewmodel.VirtualCardViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val VirtualCardViewModelModule = module {
    viewModel { VirtualCardActivityViewModel(get()) }
    viewModel { CardAreaOnboardingViewModel(get()) }
    viewModel { CardAreaViewModel(get(), get(), get(), get(), get()) }
    viewModel { VirtualCardViewModel(get(), get(), get(), get(), get(), get()) }
    viewModel { VirtualCardOnboardingViewModel(get(), get(), get(), get()) }
}