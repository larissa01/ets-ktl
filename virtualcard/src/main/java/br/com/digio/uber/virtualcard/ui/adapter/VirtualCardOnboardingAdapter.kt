package br.com.digio.uber.virtualcard.ui.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage
import br.com.digio.uber.virtualcard.R
import br.com.digio.uber.virtualcard.ui.adapter.viewholder.VirtualCardOnboardingViewHolder
import br.com.digio.uber.virtualcard.uimodel.VirtualCardOnboardingUi
import br.com.digio.uber.virtualcard.uimodel.VirtualCardOnboardingUiModel

typealias OnClickVirtualCardOnboardingItem = (item: VirtualCardOnboardingUi) -> Unit

class VirtualCardOnboardingAdapter(
    private var values: MutableList<VirtualCardOnboardingUiModel> = mutableListOf(),
    private val listener: OnClickVirtualCardOnboardingItem
) : BaseRecyclerViewAdapter<VirtualCardOnboardingUi, VirtualCardOnboardingAdapter.ViewTypesDataBindingFactoryImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryImpl = ViewTypesDataBindingFactoryImpl()

    override fun getItemType(position: Int): VirtualCardOnboardingUi = values[position]

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<VirtualCardOnboardingUi> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<VirtualCardOnboardingUi>
    }

    override fun getItemCount(): Int = values.size

    override fun onBindViewHolder(
        holder: AbstractViewHolder<VirtualCardOnboardingUi>,
        position: Int
    ) = holder.bind(values[holder.adapterPosition])

    class ViewTypesDataBindingFactoryImpl : ViewTypesDataBindingFactory<VirtualCardOnboardingUi> {
        override fun type(model: VirtualCardOnboardingUi): Int =
            R.layout.layout_virtual_card_onboarding_item

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<VirtualCardOnboardingUi>
        ): AbstractViewHolder<*> =
            VirtualCardOnboardingViewHolder(view)
    }

    override fun replaceItems(list: List<Any>) {
        addValues(list.safeHeritage())
    }

    private fun addValues(values: List<VirtualCardOnboardingUiModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }
}