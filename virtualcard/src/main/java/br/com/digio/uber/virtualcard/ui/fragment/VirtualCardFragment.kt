package br.com.digio.uber.virtualcard.ui.fragment

import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.NavHostFragment
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.generics.makeChooseMessageObject
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.onlyNumbers
import br.com.digio.uber.util.safeLet
import br.com.digio.uber.util.setInClipboard
import br.com.digio.uber.virtualcard.BR
import br.com.digio.uber.virtualcard.R
import br.com.digio.uber.virtualcard.databinding.VirtualCardFragmentBinding
import br.com.digio.uber.virtualcard.ui.viewmodel.VirtualCardViewModel
import com.google.android.material.snackbar.Snackbar
import org.koin.android.ext.android.inject

class VirtualCardFragment : BaseViewModelFragment<VirtualCardFragmentBinding, VirtualCardViewModel>() {
    override val bindingVariable: Int = BR.virtualCardViewModel
    override val getLayoutId: Int = R.layout.virtual_card_fragment
    override val viewModel: VirtualCardViewModel by inject()

    override fun tag(): String = VirtualCardFragment::class.java.name
    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK

    override val showDisplayShowTitle: Boolean = true
    override val showHomeAsUp: Boolean = true
    override fun isSecurityScreen(): Boolean = true
    override fun getToolbar(): Toolbar? =
        binding?.appBarVirtualCard?.toolbarCardDefault?.apply {
            title = getString(R.string.virtual_card_fragment_title)
        }

    override fun initialize() {
        super.initialize()
        val pidHash: String = (requireArguments().get(Const.RequestOnResult.PID.KEY_PID_HASH) ?: "") as String

        if (pidHash.isEmpty()) {
            showMessageAccessDenied()
        }

        viewModel.initializer({ onItemClicked(it) }, pidHash)
    }

    private fun onItemClicked(v: View) {
        when (v.id) {
            R.id.virtualCardItemGenerateNewCard -> {
                showMessage(
                    makeChooseMessageObject {
                        title = R.string.virtual_card_fragment_regenerate_title
                        message = R.string.virtual_card_fragment_regenerate_message
                        icon = br.com.digio.uber.common.R.drawable.ic_warning
                        positiveOptionTextInt = R.string.virtual_card_fragment_regenerate_positive
                        negativeOptionTextInt = R.string.virtual_card_fragment_regenerate_negative
                        onPositiveButtonClick = {
                            viewModel.regenerateVirtualCard()
                        }
                    }
                )
            }
            R.id.buttonCopy -> safeLet(context, view) { contextLet, view ->
                viewModel.virtualCardUiModel.value?.cardNumber?.onlyNumbers()?.setInClipboard(
                    contextLet.getString(R.string.virtual_card_info_card_clipboard_number_text_english),
                    contextLet
                )
                view.post {
                    Snackbar.make(
                        view,
                        contextLet.getString(R.string.virtual_card_info_card_clipboard_number_successfully_copied),
                        Snackbar.LENGTH_LONG
                    ).show()
                }
            }

            R.id.switchBlockVirtualCard -> {
                viewModel.virtualCardBlock()
            }
        }
    }

    private fun showMessageAccessDenied() =
        showMessage(
            ErrorObject().apply {
                title = R.string.card_area_card_access_denied_title
                message = R.string.card_area_card_access_denied_subtitle
                close = true
            }
        )

    private fun close() {
        NavHostFragment.findNavController(this).navigate(
            VirtualCardFragmentDirections.actionVirtualCardFragmentToCardAreaFragment()
        )
    }

    override fun onNavigationClick(view: View) {
        close()
    }

    override fun onBackPressed() {
        close()
    }
}