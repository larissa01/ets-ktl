package br.com.digio.uber.virtualcard.mapper

import br.com.digio.uber.model.card.CardResponse
import br.com.digio.uber.model.card.CardStatus
import br.com.digio.uber.model.card.TypeCard
import br.com.digio.uber.util.getFinalDigitsCardNumber
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.virtualcard.uimodel.CardUiModel
import br.com.digio.uber.virtualcard.util.CardUtil
import br.com.digio.uber.virtualcard.util.CardUtil.getImageTypeCard

class CardMapper : AbstractMapper<List<CardResponse>, List<CardUiModel>> {

    override fun map(param: List<CardResponse>): List<CardUiModel> {
        return param.map {
            mapItem(it)
        }
    }

    private fun mapItem(param: CardResponse?): CardUiModel {
        return CardUiModel(
            cardId = param?.cardId ?: -1,
            cardNumber = param?.cardMaskedNumber?.getFinalDigitsCardNumber() ?: "",
            typeCard = TypeCard.getCurrentType(param!!),
            cardStatusId = param.statusId.toInt(),
            cardImage = getImageTypeCard(TypeCard.getCurrentType(param)),
            isBlock = param.statusId.toInt() != CardStatus.NORMAL.id,
            showShortcutTracking = param.statusId.toInt() == CardStatus.BLOQUEADO.id,
            labelBlock = CardUtil.formatLabelBlock(param.statusId.toInt() != CardStatus.NORMAL.id)
        )
    }
}