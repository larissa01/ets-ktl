package br.com.digio.uber.virtualcard.uimodel

import br.com.digio.uber.model.card.TypeCard

data class CardUiModel(
    val cardId: Long,
    val cardNumber: String,
    val typeCard: TypeCard,
    val cardStatusId: Int,
    val cardImage: CardImageUiModel,
    val isBlock: Boolean,
    val showShortcutTracking: Boolean = false,
    val labelBlock: String?,
)