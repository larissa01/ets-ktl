package br.com.digio.uber.virtualcard.ui.adapter.viewholder

import android.annotation.SuppressLint
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.virtualcard.BR
import br.com.digio.uber.virtualcard.uimodel.VirtualCardOnboardingUiModel

class VirtualCardOnboardingViewHolder(
    private val viewDataBinding: ViewDataBinding
) : AbstractViewHolder<VirtualCardOnboardingUiModel>(viewDataBinding.root) {

    @SuppressLint("DefaultLocale")
    override fun bind(item: VirtualCardOnboardingUiModel) {
        viewDataBinding.setVariable(BR.virtualCardOnboardingItem, item)
        viewDataBinding.setVariable(BR.virtualCardOnboardingItemViewHolder, this)
        viewDataBinding.executePendingBindings()
    }
}