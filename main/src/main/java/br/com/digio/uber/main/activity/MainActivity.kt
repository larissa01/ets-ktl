package br.com.digio.uber.main.activity

import android.content.Intent
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.faq.di.faqViewModelModule
import br.com.digio.uber.home.di.mapper.homeMapperModule
import br.com.digio.uber.home.di.viewmodel.homeViewModelModule
import br.com.digio.uber.main.R
import br.com.digio.uber.main.databinding.ActivityMainBinding
import br.com.digio.uber.main.di.mainViewModelModule
import br.com.digio.uber.main.viewmodel.MainViewModel
import br.com.digio.uber.menu.di.menuViewModelModule
import br.com.digio.uber.store.di.storeViewModelModule
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.castOrNull
import org.koin.android.ext.android.inject
import org.koin.core.module.Module

class MainActivity : BaseViewModelActivity<ActivityMainBinding, MainViewModel>() {

    override fun getLayoutId(): Int? =
        R.layout.activity_main

    override val bindingVariable: Int? = null
    override val viewModel: MainViewModel by inject()

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        supportFragmentManager.findFragmentById(R.id.mainContainer)
            ?.castOrNull<NavHostFragment>()
            ?.navController
            ?.let { navController ->
                binding?.bnvMain?.setupWithNavController(navController)
            }
    }

    override fun getModule(): List<Module>? = listOf(
        mainViewModelModule,
        homeViewModelModule,
        homeMapperModule,
        faqViewModelModule,
        storeViewModelModule,
        menuViewModelModule
    )

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Const.RequestOnResult.FeatureRefreshHome.KEY_FEATURE_REFRESH_BACK) {
            supportFragmentManager
                .findFragmentById(R.id.mainContainer)
                ?.castOrNull<NavHostFragment>()
                ?.navController
                ?.currentBackStackEntry
                ?.savedStateHandle
                ?.set(Const.RequestOnResult.FeatureRefreshHome.FEATURE_ITEM_RESULT_SUCCESS, true)
        }
    }

    override fun onBackPressed() {
        /*empty*/
    }
}