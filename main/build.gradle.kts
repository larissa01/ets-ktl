plugins {
    id("com.android.dynamic-feature")
    kotlin("android")
    id("kotlin-android-extensions")
    id("androidx.navigation.safeargs")
    id("br.com.digio.uber.plugin.android.dynamic.library")
}

dependencies {
    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.ANDROIDXCORE)
    implementation(Dependencies.ACTIVITYKTX)
    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.FRAGMENTKTX)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.KOIN)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.NAV_FRAGMENT)
    implementation(Dependencies.NAV_UI)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)

    implementation(project(":app"))
    implementation(project(":home"))
    implementation(project(":faq"))
    implementation(project(":menu"))
    implementation(project(":store"))
    implementation(project(":common"))
    implementation(project(":util"))
}