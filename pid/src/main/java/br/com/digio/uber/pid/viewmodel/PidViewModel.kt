package br.com.digio.uber.pid.viewmodel

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.ChooseMessageObject
import br.com.digio.uber.common.generics.MessageGenericObject
import br.com.digio.uber.common.generics.makeChooseMessageObject
import br.com.digio.uber.common.generics.makeErrorObject
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.exception.pid.PidHashExpirationException
import br.com.digio.uber.domain.exception.pid.PidInvalidException
import br.com.digio.uber.domain.usecase.pid.FirstPidUseCase
import br.com.digio.uber.domain.usecase.pid.ValidatePidUseCase
import br.com.digio.uber.model.pid.PidStepReq
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.pid.R
import br.com.digio.uber.pid.mapper.PidMapper
import br.com.digio.uber.pid.uimodel.PidUiModel
import br.com.digio.uber.util.convertToBase64
import br.com.digio.uber.util.convertToFile

class PidViewModel constructor(
    private val firstPidUseCase: FirstPidUseCase,
    private val validatePidUseCase: ValidatePidUseCase,
    private val pidMapper: PidMapper
) : BaseViewModel() {

    val pidRes: MutableLiveData<PidUiModel> = MutableLiveData()
    val biometryTryAgain: MutableLiveData<Boolean> = MutableLiveData()
    val closeFlow: MutableLiveData<Boolean> = MutableLiveData()

    fun initializer(pidType: PidType, onClickItem: OnClickItem) {
        super.initializer(onClickItem)

        firstPidUseCase(pidType).singleExec(
            onSuccessBaseViewModel = {
                pidRes.value = pidMapper.map(it)
            },
            onError = { _, error, _ ->
                error.apply {
                    onCloseDialog = {
                        closeFlow.value = true
                    }
                }
            }
        )
    }

    fun handleFacialBiometry(path: String?) {
        pidRes.value?.let { pidRes ->
            validatePidUseCase(
                PidStepReq(
                    hash = pidRes.hash,
                    value = path
                        ?.convertToFile()
                        ?.convertToBase64(format = Bitmap.CompressFormat.JPEG)
                )
            ).singleExec(
                onError = { _, error, throwable ->
                    when (throwable) {
                        is PidHashExpirationException -> {
                            makeErrorObject {
                                buttonText = R.string.try_again
                                hideMessage = true
                                close = true
                            }
                        }
                        is PidInvalidException -> {
                            this.pidRes.value = this.pidRes.value?.copy(
                                hash = throwable.pidRes.hash
                            )
                            makeChooseMessageForPid(error, throwable)
                        }
                        else -> makeChooseMessageForPid(error, throwable)
                    }
                },
                onSuccessBaseViewModel = {
                    this.pidRes.value = pidMapper.map(it)
                }
            )
        }
    }

    private fun makeChooseMessageForPid(
        error: MessageGenericObject,
        throwable: Throwable?
    ): ChooseMessageObject =
        makeChooseMessageObject {
            icon = br.com.digio.uber.common.R.drawable.ic_alert
            messageString = error.messageString
            positiveOptionTextInt = R.string.error_button_text
            negativeOptionTextInt = R.string.not_now
            onPositiveButtonClick = {
                tryAgainBiometryValidation(throwable)
            }
            onNegativeButtonClick = {
                closeFlow.value = true
            }
            onCloseDialog = { closeWithButton ->
                if (!closeWithButton) {
                    closeFlow.value = true
                }
            }
        }

    private fun tryAgainBiometryValidation(throwable: Throwable?) {
        if (throwable is PidInvalidException) {
            this@PidViewModel.pidRes.value = this@PidViewModel.pidRes.value?.copy(
                hash = throwable.pidRes.hash
            )
        } else {
            biometryTryAgain.value = true
        }
    }
}