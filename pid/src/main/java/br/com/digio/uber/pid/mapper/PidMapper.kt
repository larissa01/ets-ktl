package br.com.digio.uber.pid.mapper

import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.model.pid.PidForm
import br.com.digio.uber.model.pid.PidRes
import br.com.digio.uber.pid.uimodel.PidFieldType
import br.com.digio.uber.pid.uimodel.PidFormUiModel
import br.com.digio.uber.pid.uimodel.PidUiModel
import br.com.digio.uber.util.mapper.AbstractMapper

class PidFormMapper : AbstractMapper<PidForm, PidFormUiModel> {
    override fun map(param: PidForm): PidFormUiModel =
        PidFormUiModel(
            crypto = param.crypto,
            errorMessage = param.errorMessage,
            fieldLength = param.fieldLength,
            title = param.title,
            index = param.index,
            key = param.key,
            mask = param.mask,
            placeholder = param.placeholder,
            regex = param.regex,
            required = param.required,
            type = PidFieldType.getType(param.type.toString()),
            value = param.value,
            statusBarColor = StatusBarColor.WHITE
        )
}

class PidMapper constructor(
    private val pidFormMapper: PidFormMapper
) : AbstractMapper<PidRes, PidUiModel> {
    override fun map(param: PidRes): PidUiModel =
        PidUiModel(
            pidForm = param.pidForm?.let {
                pidFormMapper.map(it)
            },
            hash = param.hash,
            last = param.last
        )
}