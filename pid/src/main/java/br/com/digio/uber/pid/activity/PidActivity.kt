package br.com.digio.uber.pid.activity

import android.content.Intent
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.navigation.GenericNavigation
import br.com.digio.uber.model.pid.PidType
import br.com.digio.uber.pid.di.mapper.pidMapperModule
import br.com.digio.uber.pid.di.viewmodel.pidViewModelModule
import br.com.digio.uber.pid.navigation.PidNavigation
import br.com.digio.uber.pid.uimodel.PidUiModel
import br.com.digio.uber.pid.viewmodel.PidViewModel
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.Const.RequestOnResult.PID.KEY_PID
import br.com.digio.uber.util.Const.RequestOnResult.PID.KEY_PID_TYPE
import br.com.digio.uber.util.Const.RequestOnResult.PID.RESULT_PID_ERROR
import org.koin.android.ext.android.inject
import org.koin.core.module.Module

class PidActivity : BaseViewModelActivity<ViewDataBinding, PidViewModel>() {

    private val genericNavigation by lazy {
        PidNavigation(this, navigation)
    }

    override val bindingVariable: Int? = null

    override val viewModel: PidViewModel? by inject()

    private val pidType: PidType by lazy {
        PidType.valueOf(checkNotNull(intent.extras?.getString(KEY_PID_TYPE)))
    }

    override fun getLayoutId(): Int? =
        br.com.digio.uber.common.R.layout.navigation_layout_activity

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)

        setupObserver()

        viewModel?.initializer(pidType) {
            /*nothing*/
        }
    }

    private fun setupObserver() {
        viewModel?.pidRes?.observe(
            this,
            Observer { pidRes ->
                startNavigation(pidRes)
            }
        )

        viewModel?.biometryTryAgain?.observe(
            this,
            Observer { tryAgain ->
                if (tryAgain == true) {
                    viewModel?.pidRes?.value?.let { pidRes ->
                        startNavigation(pidRes)
                    }
                    viewModel?.biometryTryAgain?.value = false
                }
            }
        )

        viewModel?.closeFlow?.observe(
            this,
            Observer { close ->
                if (close == true) {
                    finishWithFailResult()
                    viewModel?.closeFlow?.value = false
                }
            }
        )
    }

    private fun startNavigation(pidRes: PidUiModel) {
        genericNavigation.init(
            (intent.extras ?: Bundle()).apply {
                putParcelable(KEY_PID, pidRes)
            },
            pidRes
        )
    }

    private fun finishWithFailResult() {
        setResult(RESULT_PID_ERROR)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Const.RequestOnResult.FacialBiometry.REQUEST_FACIAL_CAPTURE -> handleFacialCapture(
                resultCode,
                data
            )
            else -> finishWithFailResult()
        }
    }

    private fun handleFacialCapture(resultCode: Int, data: Intent?) {
        when (resultCode) {
            Const.RequestOnResult.FacialBiometry.REQUEST_FACIAL_CAPTURE_SUCCESS -> {
                val path: String? =
                    data?.getStringExtra(Const.RequestOnResult.FacialBiometry.REQUEST_FACIAL_VALUE)
                viewModel?.handleFacialBiometry(path)
            }
            else -> finishWithFailResult()
        }
    }

    override fun getModule(): List<Module>? =
        listOf(
            pidViewModelModule,
            pidMapperModule
        )

    override fun getNavigation(): GenericNavigation? = genericNavigation
}