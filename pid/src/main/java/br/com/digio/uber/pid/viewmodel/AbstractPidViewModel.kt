package br.com.digio.uber.pid.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.pid.uimodel.PidUiModel

abstract class AbstractPidViewModel : BaseViewModel() {
    val resultOfPid: MutableLiveData<PidUiModel> = MutableLiveData()
}