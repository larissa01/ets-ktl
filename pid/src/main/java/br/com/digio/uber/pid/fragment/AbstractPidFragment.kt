package br.com.digio.uber.pid.fragment

import android.os.Bundle
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.pid.navigation.PidOpKey
import br.com.digio.uber.pid.viewmodel.AbstractPidViewModel
import br.com.digio.uber.util.Const

abstract class AbstractPidFragment<T : ViewDataBinding, VM : AbstractPidViewModel> :
    BNWFViewModelFragment<T, VM>() {

    override fun initialize() {
        super.initialize()
        viewModel?.resultOfPid?.observe(
            this,
            Observer { pidUiModel ->
                if (pidUiModel != null) {
                    callGoTo(
                        PidOpKey.NAVIGATE_TO_NEXT_PID.name,
                        (arguments ?: Bundle()).apply {
                            putParcelable(Const.RequestOnResult.PID.KEY_PID, pidUiModel)
                        }
                    )
                    viewModel?.resultOfPid?.value = null
                }
            }
        )
    }
}