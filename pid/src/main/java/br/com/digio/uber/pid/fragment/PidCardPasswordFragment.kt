package br.com.digio.uber.pid.fragment

import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.pid.BR
import br.com.digio.uber.pid.R
import br.com.digio.uber.pid.databinding.PidGenericFragmentBinding
import br.com.digio.uber.pid.navigation.PidOpKey
import br.com.digio.uber.pid.uimodel.PidUiModel
import br.com.digio.uber.pid.viewmodel.PidCardPasswordViewModel
import br.com.digio.uber.util.Const.RequestOnResult.PID.KEY_PID
import org.koin.android.ext.android.inject

class PidCardPasswordFragment :
    AbstractPidFragment<PidGenericFragmentBinding, PidCardPasswordViewModel>() {

    override val bindingVariable: Int? = BR.pidGenericViewModel

    override val getLayoutId: Int? = R.layout.pid_generic_fragment

    override val viewModel: PidCardPasswordViewModel? by inject()

    private val pidUiModel: PidUiModel by lazy {
        checkNotNull(arguments?.getParcelable<PidUiModel>(KEY_PID))
    }

    override fun tag(): String =
        PidOpKey.CARD_PASSWORD_FRAGMENT.name

    override fun initialize() {
        super.initialize()

        viewModel?.password?.observe(
            this,
            Observer { pass ->
                viewModel?.validatePassword(pass.toString())
            }
        )

        viewModel?.pidScreenUiModel?.value = pidUiModel.pidForm
        viewModel?.pidUiModel?.value = pidUiModel
    }

    override fun getStatusBarAppearance(): StatusBarColor =
        pidUiModel.pidForm?.statusBarColor ?: StatusBarColor.WHITE

    override fun getToolbar(): Toolbar? =
        binding?.toolbarPid

    override val showHomeAsUp: Boolean = true

    override val showDisplayShowTitle: Boolean = true

    companion object : FragmentNewInstance<PidCardPasswordFragment>(::PidCardPasswordFragment)
}