package br.com.digio.uber.pid.di.viewmodel

import br.com.digio.uber.pid.viewmodel.PidCardPasswordViewModel
import br.com.digio.uber.pid.viewmodel.PidViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val pidViewModelModule = module {
    viewModel { PidViewModel(get(), get(), get()) }
    viewModel { PidCardPasswordViewModel(get(), get()) }
}