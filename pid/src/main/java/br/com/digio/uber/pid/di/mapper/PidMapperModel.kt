package br.com.digio.uber.pid.di.mapper

import br.com.digio.uber.pid.mapper.PidFormMapper
import br.com.digio.uber.pid.mapper.PidMapper
import org.koin.dsl.module

val pidMapperModule = module {
    single { PidFormMapper() }
    single { PidMapper(get()) }
}