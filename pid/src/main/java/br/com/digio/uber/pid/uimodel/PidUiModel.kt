package br.com.digio.uber.pid.uimodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PidUiModel(
    val pidForm: PidFormUiModel?,
    val hash: String?,
    val last: Boolean?
) : Parcelable