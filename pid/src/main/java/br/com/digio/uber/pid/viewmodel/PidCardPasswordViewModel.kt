package br.com.digio.uber.pid.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.generics.EmptyErrorObject
import br.com.digio.uber.common.generics.MessageGenericObject
import br.com.digio.uber.common.generics.makeErrorObject
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.exception.pid.PidHashExpirationException
import br.com.digio.uber.domain.exception.pid.PidInvalidException
import br.com.digio.uber.domain.usecase.pid.ValidatePidUseCase
import br.com.digio.uber.model.pid.PidStepReq
import br.com.digio.uber.pid.R
import br.com.digio.uber.pid.mapper.PidMapper
import br.com.digio.uber.pid.uimodel.PidFormUiModel
import br.com.digio.uber.pid.uimodel.PidUiModel

class PidCardPasswordViewModel constructor(
    private val validatePidUseCase: ValidatePidUseCase,
    private val pidMapper: PidMapper
) : AbstractPidViewModel() {

    val pidUiModel: MutableLiveData<PidUiModel> = MutableLiveData()
    val enableButton: MutableLiveData<Boolean> = MutableLiveData()
    val pidScreenUiModel: MutableLiveData<PidFormUiModel> = MutableLiveData()
    val checkPassword: MutableLiveData<Boolean> = MutableLiveData()
    val errorMessage: MutableLiveData<String> = MutableLiveData()
    val password: MutableLiveData<String> = MutableLiveData()

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer {
            when (it.id) {
                R.id.btnNext -> makeValidationPid()
                else -> onClickItem(it)
            }
        }
    }

    private fun makeValidationPid() {
        pidUiModel.value?.let { pidUiModel ->
            validatePidUseCase(
                putValue(pidUiModel)
            ).singleExec(
                onSuccessBaseViewModel = { pidRes ->
                    resultOfPid.value = pidMapper.map(pidRes)
                },
                onError = { _, error, throwable ->
                    when (throwable) {
                        is PidHashExpirationException -> {
                            enableButton.value = false
                            makeErrorObject {
                                buttonText = R.string.try_again
                                hideMessage = true
                                close = true
                            }
                        }
                        is PidInvalidException -> {
                            this.pidUiModel.value = this.pidUiModel.value?.copy(
                                hash = throwable.pidRes.hash
                            )
                            makeSimpleMessage(error)
                        }
                        else -> makeSimpleMessage(error)
                    }
                }
            )
        }
    }

    private fun makeSimpleMessage(error: MessageGenericObject): EmptyErrorObject {
        errorMessage.value = error.messageString
        enableButton.value = false
        return EmptyErrorObject
    }

    private fun putValue(pidUiModel: PidUiModel): PidStepReq =
        PidStepReq(
            hash = pidUiModel.hash,
            value = password.value
        )

    fun validatePassword(password: String) {
        enableButton.value = isValidatedInputData(password)
    }

    private fun isValidatedInputData(confirmationPassword: String?): Boolean {
        return if (confirmationPassword?.isNotEmpty() == true) {
            isValidatedPassword(confirmationPassword)
        } else {
            errorMessage.value = null
            false
        }
    }

    private fun isValidatedPassword(password: String?): Boolean =
        if (pidScreenUiModel.value?.regex?.toRegex()?.matches(password.toString()) == true) {
            errorMessage.value = null
            true
        } else {
            errorMessage.value = null
            false
        }
}