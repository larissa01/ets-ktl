package br.com.digio.uber.pid.uimodel

enum class PidFieldType(val type: String) {
    DATE("date"),
    TEXT("text"),
    NUMBER("number"),
    PHOTO("photo"),
    PASSWORD("password");

    companion object {
        fun getType(value: String): PidFieldType? =
            values().firstOrNull { it.type.toLowerCase() == value.toLowerCase() }
    }
}