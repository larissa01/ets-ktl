package br.com.digio.uber.pid.navigation

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.analytics.utils.Tags
import br.com.digio.uber.common.base.fragment.BaseNavigationWithFlowFragment
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.navigation.Navigation
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName
import br.com.digio.uber.pid.fragment.PidCardPasswordFragment
import br.com.digio.uber.pid.uimodel.PidFieldType
import br.com.digio.uber.pid.uimodel.PidUiModel
import br.com.digio.uber.util.Const

class PidNavigation constructor(
    private val activity: AppCompatActivity,
    private val navigation: Navigation
) : GenericNavigationWithFlow(activity) {

    fun init(bundle: Bundle?, pidRes: PidUiModel) {
        navigateToNextPidScreen(bundle, pidRes)
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            PidOpKey.NAVIGATE_TO_NEXT_PID.name to goToNextPid(),
            PidOpKey.CARD_PASSWORD_FRAGMENT.name to getGenericFragment(),
            PidOpKey.FACE_BIOMETRY.name to goToFaceBiometry()
        )

    private fun goToNextPid(): (bundle: Bundle?) -> BaseNavigationWithFlowFragment? = {
        navigateToNextPidScreen(it, it?.getParcelable(Const.RequestOnResult.PID.KEY_PID))
        null
    }

    private fun goToFaceBiometry(): (bundle: Bundle?) -> BaseNavigationWithFlowFragment? = {
        navigation.navigateToFacialBiometryCaptureActivity(activity, Tags.SHOW_CARD_PASSWORD_BIOMETRY)
        null
    }

    private fun getGenericFragment(): GenericNavigationWIthFlowGetFragmentByName = {
        PidCardPasswordFragment.newInstance(this, it)
    }

    private fun navigateToNextPidScreen(arguments: Bundle?, pidUiModel: PidUiModel?) {
        if (checkPidValuesToEnd(pidUiModel)) {
            finishWithResult(pidUiModel)
        } else {
            when (pidUiModel?.pidForm?.type) {
                PidFieldType.PASSWORD -> goToPasswordCard(arguments, pidUiModel)
                PidFieldType.PHOTO -> goTOFaceBiometry(arguments, pidUiModel)
                else -> finishWithResultError()
            }
        }
    }

    private fun finishWithResult(pidUiModel: PidUiModel?) {
        activity.setResult(
            Const.RequestOnResult.PID.RESULT_PID_SUCCESS,
            Intent().apply {
                putExtra(
                    Const.RequestOnResult.PID.KEY_PID_HASH,
                    pidUiModel?.hash
                )
            }
        )
        activity.finish()
    }

    private fun finishWithResultError() {
        activity.setResult(Const.RequestOnResult.PID.RESULT_PID_ERROR)
        activity.finish()
    }

    private fun goTOFaceBiometry(
        arguments: Bundle?,
        pidUiModel: PidUiModel?
    ) {
        goTo(
            GoTo(
                PidOpKey.FACE_BIOMETRY.name,
                (arguments ?: Bundle()).apply {
                    putParcelable(Const.RequestOnResult.PID.KEY_PID, pidUiModel)
                }
            )
        )
    }

    private fun goToPasswordCard(
        arguments: Bundle?,
        pidUiModel: PidUiModel?
    ) {
        goTo(
            GoTo(
                PidOpKey.CARD_PASSWORD_FRAGMENT.name,
                (arguments ?: Bundle()).apply {
                    putParcelable(Const.RequestOnResult.PID.KEY_PID, pidUiModel)
                }
            )
        )
    }

    private fun checkPidValuesToEnd(pidUiModel: PidUiModel?): Boolean =
        pidUiModel?.last == true &&
            pidUiModel.hash != null
}