package br.com.digio.uber.pid.navigation

enum class PidOpKey {
    NAVIGATE_TO_NEXT_PID,
    CARD_PASSWORD_FRAGMENT,
    FACE_BIOMETRY
}