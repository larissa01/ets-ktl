package br.com.digio.uber.pid.uimodel

import android.os.Parcelable
import br.com.digio.uber.common.kenum.StatusBarColor
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PidFormUiModel(
    val crypto: Boolean,
    val errorMessage: String?,
    val fieldLength: Int?,
    val title: String,
    val index: Int,
    val key: String,
    val mask: String?,
    val placeholder: String?,
    val regex: String?,
    val required: Boolean,
    val type: PidFieldType?,
    val value: String?,
    val statusBarColor: StatusBarColor
) : Parcelable