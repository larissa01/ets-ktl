package br.com.digio.uber.tracking.navigation

import br.com.digio.uber.common.kenum.StatusBarColor

enum class TrackingOpKey(val theme: StatusBarColor) {
    MAIN(StatusBarColor.BLACK)
}