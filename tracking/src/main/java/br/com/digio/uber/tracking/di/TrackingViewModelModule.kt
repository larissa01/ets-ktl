package br.com.digio.uber.tracking.di

import br.com.digio.uber.tracking.ui.viewModel.TrackingMainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val trackingViewModelModule = module {
    viewModel { TrackingMainViewModel(get(), get()) }
}