package br.com.digio.uber.tracking.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.digio.uber.common.listener.GoTo
import br.com.digio.uber.common.navigation.GenericNavigationWithFlow
import br.com.digio.uber.common.typealiases.GenericNavigationWIthFlowGetFragmentByName
import br.com.digio.uber.tracking.ui.fragment.TrackingMainFragment

class TrackingNavigation(
    activity: AppCompatActivity,
) : GenericNavigationWithFlow(activity) {

    fun init(bundle: Bundle?) {
        goTo(GoTo(TrackingOpKey.MAIN.name, bundle))
    }

    override fun getFlow(): List<Pair<String, GenericNavigationWIthFlowGetFragmentByName>> =
        listOf(
            TrackingOpKey.MAIN.name to showMain(),
        )

    private fun showMain(): GenericNavigationWIthFlowGetFragmentByName = {
        TrackingMainFragment.newInstance(this, it)
    }
}