package br.com.digio.uber.tracking.ui.viewModel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.generics.ErrorObject
import br.com.digio.uber.common.typealiases.OnClickItem
import br.com.digio.uber.domain.usecase.card.GetCardTypeUseCase
import br.com.digio.uber.domain.usecase.tracking.GetTrackingLogsUseCase
import br.com.digio.uber.model.card.TypeCard
import br.com.digio.uber.tracking.R
import br.com.digio.uber.tracking.mapper.TrackingLogUiMapper
import br.com.digio.uber.tracking.ui.uiModel.TrackingLogUiItem

class TrackingMainViewModel constructor(
    private val getTrackingLogsUseCase: GetTrackingLogsUseCase,
    private val getCardTypeUseCase: GetCardTypeUseCase,
) : BaseViewModel() {
    val trackingLogs = MutableLiveData<TrackingLogUiItem>()
    private val cardId = MutableLiveData<Long>()
    val isTrackingCodeVisible = MutableLiveData<Boolean>()

    override fun initializer(onClickItem: OnClickItem) {
        super.initializer(onClickItem)
        getCard()
    }

    private fun loadTrackingLogs() {
        cardId.value?.let { id ->
            getTrackingLogsUseCase(id).singleExec(
                onSuccessBaseViewModel = {
                    trackingLogs.value = TrackingLogUiMapper().map(it)
                    isTrackingCodeVisible.value = !it.trackingCode.isNullOrEmpty()
                }
            )
        } ?: errorCard()
    }

    private fun getCard() {
        getCardTypeUseCase(TypeCard.DEBIT_ELO).singleExec(
            onError = { _, error, _ ->
                error.apply {
                    close = false
                }
            },
            onSuccessBaseViewModel = {
                cardId.value = it.cardId
                loadTrackingLogs()
            }
        )
    }

    private fun errorCard() {
        ErrorObject().apply {
            message = R.string.card_reissue_card_not_found
            close = true
        }
    }
}