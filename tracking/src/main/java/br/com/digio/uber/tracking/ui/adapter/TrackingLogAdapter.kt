package br.com.digio.uber.tracking.ui.adapter

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.common.base.adapter.BaseRecyclerViewAdapter
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import br.com.digio.uber.common.base.adapter.ViewTypesListener
import br.com.digio.uber.tracking.R
import br.com.digio.uber.tracking.ui.uiModel.TrackingLogUiModel
import br.com.digio.uber.util.inflateBinding
import br.com.digio.uber.util.safeHeritage

typealias OnClickTrackingLogListener = (item: TrackingLogUiModel) -> Unit

class TrackingLogAdapter constructor(
    private var values: MutableList<TrackingLogUiModel> = mutableListOf(),
    private val listener: OnClickTrackingLogListener
) : BaseRecyclerViewAdapter<TrackingLogUiModel, TrackingLogAdapter.ViewTypesDataBindingFactoryTrackingImpl>() {

    override fun getViewTypeFactory(): ViewTypesDataBindingFactoryTrackingImpl =
        ViewTypesDataBindingFactoryTrackingImpl()

    override fun getItemType(position: Int): TrackingLogUiModel = values[position]

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder<TrackingLogUiModel> {
        val viewDataBinding = parent.inflateBinding(viewType)
        val holder = typeFactory.holder(
            type = viewType,
            view = viewDataBinding,
            listener = listener
        )

        @Suppress("UNCHECKED_CAST")
        return holder as AbstractViewHolder<TrackingLogUiModel>
    }

    override fun onBindViewHolder(holder: AbstractViewHolder<TrackingLogUiModel>, position: Int) =
        values[holder.adapterPosition].let { holder.bind(it) }

    override fun getItemCount(): Int = values.size

    override fun replaceItems(list: List<Any>) = addValues(list.safeHeritage())

    private fun addValues(values: List<TrackingLogUiModel>) {
        this.values.clear()
        this.values.addAll(values)
        notifyDataSetChanged()
    }

    inner class ViewTypesDataBindingFactoryTrackingImpl : ViewTypesDataBindingFactory<TrackingLogUiModel> {
        override fun type(model: TrackingLogUiModel): Int =
            R.layout.item_tracking_log

        override fun holder(
            type: Int,
            view: ViewDataBinding,
            listener: ViewTypesListener<TrackingLogUiModel>
        ): AbstractViewHolder<*> =
            TrackingLogViewHolder(view)
    }
}