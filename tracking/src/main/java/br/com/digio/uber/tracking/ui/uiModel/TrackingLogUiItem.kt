package br.com.digio.uber.tracking.ui.uiModel

import android.os.Parcelable
import br.com.digio.uber.common.base.adapter.AdapterViewModel
import br.com.digio.uber.common.base.adapter.ViewTypesDataBindingFactory
import kotlinx.android.parcel.Parcelize

sealed class TrackingLogUiModel : AdapterViewModel<TrackingLogUiModel> {
    override fun type(typesFactory: ViewTypesDataBindingFactory<TrackingLogUiModel>) = typesFactory.type(
        model = this
    )
}

@Parcelize
data class TrackingLogUiItem(
    val expectedDeliveryDate: String,
    val trackingCode: String?,
    val trackingStates: List<TrackingLogStateUi>
) : Parcelable, TrackingLogUiModel()

@Parcelize
data class TrackingLogStateUi(
    val date: String? = "",
    val time: String? = "",
    val description: String,
    val stage: Int,
    val title: String,
    val type: Int,
    var iconProgress: Int,
    var iconIndicatorTop: Int,
    var iconIndicatorBottom: Int,
    var fontColor: Int
) : Parcelable, TrackingLogUiModel()