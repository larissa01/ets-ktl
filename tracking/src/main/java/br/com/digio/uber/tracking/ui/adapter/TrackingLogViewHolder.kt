package br.com.digio.uber.tracking.ui.adapter

import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.adapter.AbstractViewHolder
import br.com.digio.uber.tracking.BR
import br.com.digio.uber.tracking.ui.uiModel.TrackingLogStateUi

class TrackingLogViewHolder(
    private val viewDataBinding: ViewDataBinding
) : AbstractViewHolder<TrackingLogStateUi>(viewDataBinding.root) {
    override fun bind(item: TrackingLogStateUi) {
        viewDataBinding.setVariable(BR.trackingStateItem, item)
        viewDataBinding.setVariable(BR.trackingLogViewHolder, this)
        viewDataBinding.executePendingBindings()
    }
}