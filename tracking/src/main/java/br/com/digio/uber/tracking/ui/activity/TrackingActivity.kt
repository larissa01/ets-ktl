package br.com.digio.uber.tracking.ui.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.common.helper.LayoutIds
import br.com.digio.uber.common.helper.ViewIds
import br.com.digio.uber.common.navigation.GenericNavigation
import br.com.digio.uber.tracking.R
import br.com.digio.uber.tracking.di.trackingViewModelModule
import br.com.digio.uber.tracking.navigation.TrackingNavigation
import com.google.android.material.appbar.CollapsingToolbarLayout
import org.koin.core.module.Module

class TrackingActivity : BaseViewModelActivity<ViewDataBinding, BaseViewModel>() {

    private val trackingNavigation = TrackingNavigation(this)
    override val bindingVariable: Int? = null
    override val viewModel: BaseViewModel? = null
    override fun getLayoutId(): Int? = LayoutIds.navigationLayoutWithCollapsingToolbarScrollActivity
    override fun getModule(): List<Module>? = listOf(trackingViewModelModule)
    override fun getNavigation(): GenericNavigation? = trackingNavigation
    override fun getToolbar(): Toolbar? = findViewById<Toolbar>(ViewIds.toolbarId).apply {
        this@TrackingActivity.findViewById<CollapsingToolbarLayout>(ViewIds.collapsingToolbarId)
            .title = getString(R.string.title_track_card)
        this.navigationIcon = ContextCompat.getDrawable(context, br.com.digio.uber.common.R.drawable.ic_back_arrow)
    }

    override fun showDisplayShowTitle(): Boolean = true

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        trackingNavigation.init(intent.extras)
    }
}