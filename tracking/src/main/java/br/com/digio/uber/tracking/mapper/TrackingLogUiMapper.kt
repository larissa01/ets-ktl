package br.com.digio.uber.tracking.mapper

import br.com.digio.uber.model.tracking.TrackingLogResponse
import br.com.digio.uber.model.tracking.TrackingState
import br.com.digio.uber.tracking.R
import br.com.digio.uber.tracking.ui.uiModel.TrackingLogStateUi
import br.com.digio.uber.tracking.ui.uiModel.TrackingLogUiItem
import br.com.digio.uber.util.FORMAT_US_DATE_TIME_MINUTES
import br.com.digio.uber.util.getDateBR
import br.com.digio.uber.util.getTimeMinutesBR
import br.com.digio.uber.util.mapper.AbstractMapper
import br.com.digio.uber.util.toCalendar

class TrackingLogUiMapper : AbstractMapper<TrackingLogResponse, TrackingLogUiItem> {
    override fun map(param: TrackingLogResponse): TrackingLogUiItem =
        convertResponseToUiModel(param)

    private fun convertResponseToUiModel(data: TrackingLogResponse): TrackingLogUiItem {
        val stateList = ArrayList<TrackingLogStateUi>()
        data.trackingStates.forEach { it ->
            stateList.add(
                TrackingLogStateUi(
                    date = it.date?.let { it.toCalendar(FORMAT_US_DATE_TIME_MINUTES)?.getDateBR() } ?: "",
                    time = it.date?.let { it.toCalendar(FORMAT_US_DATE_TIME_MINUTES)?.getTimeMinutesBR() } ?: "",
                    description = it.description,
                    stage = it.stage,
                    title = it.title,
                    type = it.type,
                    iconProgress = changeStateIcon(it.type),
                    iconIndicatorTop = changeStateLine(it.type),
                    iconIndicatorBottom = changeStateLine(it.type),
                    fontColor = changeFontColor(it.type)
                )
            )
        }

        if (!stateList.isNullOrEmpty()) {
            stateList[0].iconIndicatorTop = changeStateLine(TrackingState.HIDE)
            stateList[stateList.lastIndex].iconIndicatorBottom = changeStateLine(TrackingState.HIDE)
            if (stateList[stateList.lastIndex].iconProgress != TrackingState.CURRENT &&
                stateList[stateList.lastIndex].iconProgress != TrackingState.FAILURE
            ) {
                stateList[stateList.lastIndex].iconIndicatorTop = changeStateLine(TrackingState.INACTIVE)
                stateList[stateList.lastIndex].iconProgress = changeStateIcon(TrackingState.INACTIVE)
                stateList[stateList.lastIndex].fontColor = changeFontColor(TrackingState.INACTIVE)
            }
        }

        return TrackingLogUiItem(
            expectedDeliveryDate = data.expectedDeliveryDate.let {
                it.toCalendar(FORMAT_US_DATE_TIME_MINUTES)?.getDateBR()
            } ?: "",
            trackingCode = data.trackingCode,
            trackingStates = stateList
        )
    }

    private fun changeFontColor(stage: Int): Int {
        return when (stage) {
            TrackingState.INACTIVE -> br.com.digio.uber.R.color.grey_AFAFAF
            TrackingState.FINISHED -> br.com.digio.uber.R.color.grey_545454
            TrackingState.CURRENT -> br.com.digio.uber.R.color.black
            TrackingState.FAILURE -> br.com.digio.uber.R.color.black
            else -> br.com.digio.uber.R.color.grey_545454
        }
    }

    private fun changeStateIcon(stage: Int): Int {
        return when (stage) {
            TrackingState.INACTIVE -> R.drawable.ic_tracking_state_inactive
            TrackingState.FINISHED -> R.drawable.ic_tracking_state_finished
            TrackingState.CURRENT -> R.drawable.ic_tracking_state_current
            TrackingState.FAILURE -> R.drawable.ic_tracking_state_failure
            else -> R.drawable.ic_tracking_state_inactive
        }
    }

    private fun changeStateLine(stage: Int): Int {
        return when (stage) {
            TrackingState.INACTIVE -> R.drawable.ic_tracking_line_inactive
            TrackingState.FINISHED -> R.drawable.ic_tracking_line_active
            TrackingState.CURRENT -> R.drawable.ic_tracking_line_active
            TrackingState.FAILURE -> R.drawable.ic_tracking_line_failure
            TrackingState.LAST -> R.drawable.ic_tracking_line_inactive
            TrackingState.HIDE -> R.drawable.ic_tracking_line_hide
            else -> R.drawable.ic_tracking_line_inactive
        }
    }
}