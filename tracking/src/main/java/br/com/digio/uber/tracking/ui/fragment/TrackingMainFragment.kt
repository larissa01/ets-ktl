package br.com.digio.uber.tracking.ui.fragment

import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.uber.common.base.fragment.BNWFViewModelFragment
import br.com.digio.uber.common.helper.FragmentNewInstance
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.tracking.BR
import br.com.digio.uber.tracking.R
import br.com.digio.uber.tracking.databinding.FragmentTrackingMainBinding
import br.com.digio.uber.tracking.navigation.TrackingOpKey
import br.com.digio.uber.tracking.ui.adapter.OnClickTrackingLogListener
import br.com.digio.uber.tracking.ui.adapter.TrackingLogAdapter
import br.com.digio.uber.tracking.ui.uiModel.TrackingLogUiModel
import br.com.digio.uber.tracking.ui.viewModel.TrackingMainViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class TrackingMainFragment :
    BNWFViewModelFragment<FragmentTrackingMainBinding, TrackingMainViewModel>(),
    OnClickTrackingLogListener {

    private lateinit var adapter: TrackingLogAdapter

    override val bindingVariable: Int = BR.trackingMainViewModel
    override val getLayoutId: Int = R.layout.fragment_tracking_main
    override val viewModel: TrackingMainViewModel by viewModel()
    override fun tag(): String = TrackingOpKey.MAIN.name
    override fun getStatusBarAppearance(): StatusBarColor = TrackingOpKey.MAIN.theme

    companion object : FragmentNewInstance<TrackingMainFragment>(::TrackingMainFragment)

    override fun initialize() {
        viewModel.initializer {}
        setupObservables()
    }

    private fun setupObservables() {
        viewModel.trackingLogs.observe(
            this,
            {
                if (it != null) {
                    binding?.rvTrackingLogs?.layoutManager = LinearLayoutManager(requireContext())
                    adapter = TrackingLogAdapter(it.trackingStates.toMutableList(), this)
                    binding?.rvTrackingLogs?.adapter = adapter
                }
            }
        )
    }

    override fun invoke(item: TrackingLogUiModel) {
        Log.i("", "")
    }
}