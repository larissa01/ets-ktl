import com.android.build.gradle.TestedExtension
import com.android.build.gradle.internal.dsl.BuildType
import com.android.build.gradle.internal.dsl.DefaultConfig
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.kotlin.dsl.provideDelegate

fun TestedExtension.configureAndroidGeneric(
    moreConfigure: (TestedExtension.() -> Unit)? = null,
    defaultConfig: (DefaultConfig.() -> Unit)? = null,
    releaseBuildTypeConfig: (BuildType.() -> Unit)? = null,
    homolBuildTypeConfig: (BuildType.() -> Unit)? = null,
    debugBuildTypeConfig: (BuildType.() -> Unit)? = null,
    project: Project
) {
    compileSdkVersion(Sdk.compileVersion)
    buildToolsVersion(Sdk.projectBuildToolsVersion)

    defaultConfig {
        minSdkVersion(Sdk.minVersion)
        targetSdkVersion(Sdk.targetVersion)

        minSdkVersion(Sdk.minVersion)
        targetSdkVersion(Sdk.targetVersion)

        versionCode = Sdk.appVersionCode
        versionName = Sdk.appVersionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        defaultConfig?.invoke(this)
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    signingConfigs {
        val buildTypeChoose: String by project
        when (buildTypeChoose) {
            "uberRelease" -> {
                maybeCreate("release")
                getByName("release") {
                    storeFile = project.project(":app").file("../config/android_keystore.jks")
                    storePassword = "r4bhvp2h37"
                    keyAlias = "digio"
                    keyPassword = "r4bhvp2h37"
                }
            }
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            isZipAlignEnabled = true
            isDebuggable = false
            ndk.debugSymbolLevel = "SYMBOL_TABLE"
            if (signingConfigs.names.contains("release")) {
                signingConfig = signingConfigs.getByName("release")
            }
            releaseBuildTypeConfig?.invoke(this)
        }

        val homolDefaultActionBuildType: BuildType.() -> Unit = {
            isMinifyEnabled = false
            isZipAlignEnabled = true
            isDebuggable = false
            if (signingConfigs.names.contains("debug")) {
                signingConfig = signingConfigs.getByName("debug")
            }
            homolBuildTypeConfig?.invoke(this)
        }
        if (buildTypes.names.contains("homol")) {
            getByName("homol", homolDefaultActionBuildType)
        } else {
            create("homol", homolDefaultActionBuildType)
        }

        getByName("debug") {
            isDebuggable = true
            if (signingConfigs.names.contains("debug")) {
                signingConfig = signingConfigs.getByName("debug")
            }
            debugBuildTypeConfig?.invoke(this)
        }
    }

    flavorDimensions("environment")
    productFlavors {
        maybeCreate("uber")
    }

    variantFilter {
        val buildTypeChoose: String by project
        if (name != buildTypeChoose) {
            ignore = true
        }
    }

    moreConfigure?.invoke(this)
}