object Sdk {
    const val minVersion = 22
    const val targetVersion = 30
    const val compileVersion = 30
    const val projectBuildToolsVersion = "30.0.2"
    const val appVersionCode = 15
    const val appVersionName = "1.0.30"
    const val applicationId = "br.com.digio.uber"
}

object LibVersions {

    //PLUGIN DEPENDENCIES
    const val AGP = "4.1.1"
    const val DETEKT = "1.14.1"
    const val KTLINT_PLUGIN = "9.4.1"
    const val NAVIGATION_ARGS = "1.0.0"
    const val BEN_MANES_DEPENDENCY_VERSIONS = "0.33.0"
    const val GOOGLE_SERVICES_VERSION = "4.3.4"
    const val CRASHLYTICS_PLUGIN_VERSION = "2.3.0"

    //COMMON DEPENDENCIES
    const val ANDROID_X_CORE_VERSION = "1.3.2"
    const val APP_COMPAT_VERSION = "1.2.0"
    const val ACTIVITY_KTX_VERSION = "1.1.0"
    const val ANDROIDX_FRAGMENT_KTX_VERSION = "1.2.5"
    const val CARD_VIEW_VERSION = "1.0.0"
    const val CIRCLE_IMAGE_VIEW_VERSION = "3.1.0"
    const val CONSTRAINT_LAYOUT_VERSION = "2.0.2"
    const val GLIDE_VERSION = "4.11.0"
    const val GSON_VERSION = "2.8.6"
    const val KOTLIN_VERSION = "1.4.10"
    const val OK_HTTP_VERSION = "4.9.0"
    const val RECYCLER_VERSION = "1.1.0"
    const val RETROFIT_VERSION = "2.9.0"
    const val COROUTINES_VERSION = "1.3.9"
    const val COROUTINES_ANDROID_VERSION = "1.3.9"
    const val KOTLINX_COROUTINES_VERSION = "1.3.9"
    const val LIFECYCLE_VERSION = "2.2.0"
    const val SQLITE_VERSION = "2.1.0"
    const val DATA_BINDING_VERSION = "4.0.2"
    const val TIMBER_VERSION = "4.7.1"
    const val MATERIAL_VERSION = "1.2.1"
    const val KOIN_VERSION = "2.2.0-rc-2"
    const val KTLINT_VERSION = "0.39.0"
    const val ROOM_VERSION = "2.2.5"
    const val ANDROID_X_PREFERENCE_VERSION = "1.1.1"
    const val SKELETON_VERSION = "1.1.2"
    const val SHIMMER = "0.5.0"
    const val LOCAL_BROADCAST_MANAGER_VERSION = "1.0.0"
    const val LOTTIE_VERSION = "3.4.4"
    const val SQLCIPHER_VERSION = "4.3.0"
    const val NAVIGATION_VERSION = "2.3.0"
    const val ANNOTATION_VERSION = "1.1.0"
    const val SDP_VERSION = "1.0.6"
    const val FIREBASE_BOM_VERSION = "25.12.0"
    const val PERMISSIONS_DISPATCHER_VERSION = "4.8.0"
    const val HAWK_VERSION = "2.0.1"
    const val DEXGUARD_GRADLE_VERSION = "1.0.1"
    const val FOTOAPPARAT_VERSION = "2.7.0"
    const val JNA_VERSION = "4.5.0"
    const val SALESFORCE_MARKETING_CLOUD_VERSION = "7.3.0"
    const val CANARINHO_VERSION = "2.0.1"
    const val GOOGLE_PLAY_SERVICES_LOCATION_VERSION = "17.1.0"
    const val CUSTOM_TABS_VERSION = "1.3.0"
    const val SWIPE_REFRESH_LAYOUT_VERSION = "1.1.0"
    const val MATERIAL_DATE_TIME_PICKER = "4.2.3"

    //TEST DEPENDENCIES
    const val MOCKITO_VERSION = "3.5.13"
    const val MOCKK_VERSION = "1.10.2"
    const val COROUTINES_TEST_VERSION = "1.3.9"
    const val ESPRESSO_VERSION = "3.3.0"
    const val J_UNIT_VERSION = "4.13"
    const val MOCKITO_KOTLIN_VERSION = "2.2.0"
    const val TEST_RULES_VERSION = "1.3.0"
    const val TEST_RUNNER_VERSION = "1.3.0"
    const val HAMCREST_VERSION = "2.2"
    const val ANDROIDX_CORE_TESTING = "2.1.0"

    //PDF VIEWER
    const val PDF_VIEWER = "2.8.2"
}

object Dependencies {
    //jetbrains
    const val KOTLIN = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${LibVersions.KOTLIN_VERSION}"
    const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${LibVersions.COROUTINES_VERSION}"
    const val COROUTINESANDROID =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${LibVersions.COROUTINES_ANDROID_VERSION}"
    const val KOTLINXCOROUTINES =
        "org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:${LibVersions.KOTLINX_COROUTINES_VERSION}"

    //androidx
    const val ANDROIDXCORE = "androidx.core:core-ktx:${LibVersions.ANDROID_X_CORE_VERSION}"
    const val ACTIVITYKTX = "androidx.activity:activity-ktx:${LibVersions.ACTIVITY_KTX_VERSION}"
    const val APPCOMPAT = "androidx.appcompat:appcompat:${LibVersions.APP_COMPAT_VERSION}"
    const val FRAGMENTKTX = "androidx.fragment:fragment-ktx:${LibVersions.ANDROIDX_FRAGMENT_KTX_VERSION}"
    const val CARDVIEW = "androidx.cardview:cardview:${LibVersions.CARD_VIEW_VERSION}"
    const val RECYCLER = "androidx.recyclerview:recyclerview:${LibVersions.RECYCLER_VERSION}"
    const val SQLITE = "androidx.sqlite:sqlite:${LibVersions.SQLITE_VERSION}"
    const val LIFECYCLE_VIEWMODEL = "androidx.lifecycle:lifecycle-viewmodel-ktx:${LibVersions.LIFECYCLE_VERSION}"
    const val LIFECYCLE_RUNTIME = "androidx.lifecycle:lifecycle-runtime:${LibVersions.LIFECYCLE_VERSION}"
    const val LIFECYCLE_RUNTIME_KTX = "androidx.lifecycle:lifecycle-runtime-ktx:${LibVersions.LIFECYCLE_VERSION}"
    const val LIFECYCLE_LIVEDATA = "androidx.lifecycle:lifecycle-livedata-ktx:${LibVersions.LIFECYCLE_VERSION}"
    const val LIFECYCLE_EXTENSIONS = "androidx.lifecycle:lifecycle-extensions:${LibVersions.LIFECYCLE_VERSION}"
    const val DATABINDING_COMPILER = "androidx.databinding:databinding-compiler:${LibVersions.DATA_BINDING_VERSION}"
    const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:${LibVersions.CONSTRAINT_LAYOUT_VERSION}"
    const val LOCAL_BCAST_MANAGER =
        "androidx.localbroadcastmanager:localbroadcastmanager:${LibVersions.LOCAL_BROADCAST_MANAGER_VERSION}"
    const val MATERIAL = "com.google.android.material:material:${LibVersions.MATERIAL_VERSION}"
    const val GOOGLE_PLAY_SERVICES_LOCATION =
        "com.google.android.gms:play-services-location:${LibVersions.GOOGLE_PLAY_SERVICES_LOCATION_VERSION}"
    const val PREFERENCE = "androidx.preference:preference:${LibVersions.ANDROID_X_PREFERENCE_VERSION}"
    const val ANNOTATION = "androidx.annotation:annotation:${LibVersions.ANNOTATION_VERSION}"
    const val CUSTOM_TABS = "androidx.browser:browser:${LibVersions.CUSTOM_TABS_VERSION}"
    const val SWIPE_REFRESH_LAYOUT = "androidx.swiperefreshlayout:swiperefreshlayout:${LibVersions.SWIPE_REFRESH_LAYOUT_VERSION}"

    //square
    const val OKHTTP = "com.squareup.okhttp3:okhttp:${LibVersions.OK_HTTP_VERSION}"
    const val OKHTTPLOGINTERCEPTOR = "com.squareup.okhttp3:logging-interceptor:${LibVersions.OK_HTTP_VERSION}"
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${LibVersions.RETROFIT_VERSION}"
    const val RETROFITSCALARS = "com.squareup.retrofit2:converter-scalars:${LibVersions.RETROFIT_VERSION}"
    const val RETROFITGSONCONVERTER = "com.squareup.retrofit2:converter-gson:${LibVersions.RETROFIT_VERSION}"
    const val RETROFITRXADAPTER = "com.squareup.retrofit2:adapter-rxjava2:${LibVersions.RETROFIT_VERSION}"

    //firebase
    const val FIREBASE_BOM = "com.google.firebase:firebase-bom:${LibVersions.FIREBASE_BOM_VERSION}"
    const val FIREBASE_ANALYTICS = "com.google.firebase:firebase-analytics-ktx"
    const val FIREBASE_MESSAGING = "com.google.firebase:firebase-messaging"
    const val FIREBASE_CRASHLYTICS = "com.google.firebase:firebase-crashlytics-ktx"

    //salesforce marketing cloud
    const val SALESFORCE_MARKETING_CLOUD = "com.salesforce.marketingcloud:marketingcloudsdk:${LibVersions.SALESFORCE_MARKETING_CLOUD_VERSION}"

    //others
    const val TIMBER = "com.jakewharton.timber:timber:${LibVersions.TIMBER_VERSION}"
    // Todo Temporary for toolbar compatibility only digio
    const val CIRCLE_IMAGE_VIEW = "de.hdodenhof:circleimageview:${LibVersions.CIRCLE_IMAGE_VIEW_VERSION}"
    const val GLIDE = "com.github.bumptech.glide:glide:${LibVersions.GLIDE_VERSION}"
    const val GLIDE_COMPILER = "com.github.bumptech.glide:compiler:${LibVersions.GLIDE_VERSION}"
    const val SKELETON = "com.ethanhua:skeleton:${LibVersions.SKELETON_VERSION}"
    const val SHIMMER = "com.facebook.shimmer:shimmer:${LibVersions.SHIMMER}"
    const val HAWK = "com.orhanobut:hawk:${LibVersions.HAWK_VERSION}"
    const val JNA = "net.java.dev.jna:jna:${LibVersions.JNA_VERSION}"

    const val GSON = "com.google.code.gson:gson:${LibVersions.GSON_VERSION}"
    const val LOTTIE = "com.airbnb.android:lottie:${LibVersions.LOTTIE_VERSION}"
    const val SDP = "com.intuit.sdp:sdp-android:${LibVersions.SDP_VERSION}"
    const val SSP = "com.intuit.ssp:ssp-android:${LibVersions.SDP_VERSION}"
    const val PERMISSIONS_DISPATCHER =
        "org.permissionsdispatcher:permissionsdispatcher:${LibVersions.PERMISSIONS_DISPATCHER_VERSION}"
    const val PERMISSIONS_PROCESSOR =
        "org.permissionsdispatcher:permissionsdispatcher-processor:${LibVersions.PERMISSIONS_DISPATCHER_VERSION}"
    const val FOTOAPPARAT = "io.fotoapparat:fotoapparat:${LibVersions.FOTOAPPARAT_VERSION}"
    const val CANARINHO = "br.com.concrete:canarinho:${LibVersions.CANARINHO_VERSION}"
    const val MATERIAL_DATE_TIME_PICKER = "com.wdullaer:materialdatetimepicker:${LibVersions.MATERIAL_DATE_TIME_PICKER}"

    // DI
    const val KOIN = "org.koin:koin-android:${LibVersions.KOIN_VERSION}"
    const val KOINSCOPE = "org.koin:koin-android-scope:${LibVersions.KOIN_VERSION}"
    const val KOINVIEWMODEL = "org.koin:koin-android-viewmodel:${LibVersions.KOIN_VERSION}"
    const val KOINEXT = "org.koin:koin-android-ext:${LibVersions.KOIN_VERSION}"

    // DB
    const val ROOM = "androidx.room:room-runtime:${LibVersions.ROOM_VERSION}"
    const val ROOM_KAPT = "androidx.room:room-compiler:${LibVersions.ROOM_VERSION}"
    const val ROOM_KTX = "androidx.room:room-ktx:${LibVersions.ROOM_VERSION}"
    const val SQLCIPHER = "net.zetetic:android-database-sqlcipher:${LibVersions.SQLCIPHER_VERSION}"

    // Navigation
    const val NAV_FRAGMENT = "androidx.navigation:navigation-fragment-ktx:${LibVersions.NAVIGATION_VERSION}"
    const val NAV_UI = "androidx.navigation:navigation-ui-ktx:${LibVersions.NAVIGATION_VERSION}"
    const val NAV_DYNAMIC_FEATURE = "androidx.navigation:navigation-dynamic-features-fragment:${LibVersions.NAVIGATION_VERSION}"
    const val ZXING_ANDROID_EMBEDDED = "com.journeyapps:zxing-android-embedded:3.6.0"
    const val ZXING_ANDROID_EMBEDDED_OLD = "com.google.zxing:core:3.3.2"

    //PDF
    const val PDF_VIEWER = "com.github.barteksc:android-pdf-viewer:${LibVersions.PDF_VIEWER}"
}

object TestDependencies {
    const val JUNIT = "junit:junit:${LibVersions.J_UNIT_VERSION}"
    const val MOCKK = "io.mockk:mockk:${LibVersions.MOCKK_VERSION}"

    //test androidx
    const val TESTRULES = "androidx.test:rules:${LibVersions.TEST_RULES_VERSION}"
    const val TESTRUNNER = "androidx.test:runner:${LibVersions.TEST_RUNNER_VERSION}"
    const val HAMCREST = "org.hamcrest:hamcrest:${LibVersions.HAMCREST_VERSION}"
    const val ANDROIDX_CORE_TESTING = "androidx.arch.core:core-testing:${LibVersions.ANDROIDX_CORE_TESTING}"
    const val ESPRESSOCORE = "androidx.test.espresso:espresso-core:${LibVersions.ESPRESSO_VERSION}"
    const val ESPRESSOCONTRIB =
        "androidx.test.espresso:espresso-contrib:${LibVersions.ESPRESSO_VERSION}"
    const val ESPRESSOINTENTS =
        "androidx.test.espresso:espresso-intents:${LibVersions.ESPRESSO_VERSION}"

    //test mockito
    const val MOCKITOCORE = "org.mockito:mockito-core:${LibVersions.MOCKITO_VERSION}"
    const val MOCKITOINLINE = "org.mockito:mockito-inline:${LibVersions.MOCKITO_VERSION}"

    //test others
    const val MOCKITOKOTLIN =
        "com.nhaarman.mockitokotlin2:mockito-kotlin:${LibVersions.MOCKITO_KOTLIN_VERSION}"
    const val COROUTINESTEST =
        "org.jetbrains.kotlinx:kotlinx-coroutines-test:${LibVersions.COROUTINES_TEST_VERSION}"
}