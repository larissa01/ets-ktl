plugins {
    id("com.android.dynamic-feature")
    kotlin("android")
    id("kotlin-android-extensions")
    id("androidx.navigation.safeargs")
    id("br.com.digio.uber.plugin.android.dynamic.library")
}

dependencies {

    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.FRAGMENTKTX)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.KOIN)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)
    implementation(Dependencies.NAV_FRAGMENT)
    implementation(Dependencies.NAV_UI)
    implementation(Dependencies.LIFECYCLE_EXTENSIONS)
    implementation(Dependencies.LIFECYCLE_VIEWMODEL)
    implementation(Dependencies.ANNOTATION)

    implementation(project(":app"))
    implementation(project(":common"))
    implementation(project(":util"))
}