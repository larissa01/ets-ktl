package br.com.digio.uber.sample.ui.activity

import br.com.digio.uber.common.base.activity.BaseActivity
import br.com.digio.uber.sample.R

class SampleActivity : BaseActivity() {

    override fun getLayoutId(): Int? = R.layout.activity_sample
}