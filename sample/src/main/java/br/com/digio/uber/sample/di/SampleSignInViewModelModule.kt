package br.com.digio.uber.sample.di

import br.com.digio.uber.sample.ui.viewmodel.MainSampleViewModel
import org.koin.dsl.module

val sampleSignInViewModelModule = module {
    single { MainSampleViewModel() }
}