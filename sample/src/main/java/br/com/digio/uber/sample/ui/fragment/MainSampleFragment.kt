package br.com.digio.uber.sample.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.FragmentNavigatorExtras
import br.com.digio.uber.common.extensions.navigateWithTransition
import br.com.digio.uber.sample.R
import br.com.digio.uber.sample.databinding.MainSampleFragmentBinding
import br.com.digio.uber.sample.ui.viewmodel.MainSampleViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class MainSampleFragment : Fragment() {

    private val viewModel: MainSampleViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = MainSampleFragmentBinding.inflate(inflater)
        binding.onClick = manageClick()
        return binding.root
    }

    private fun manageClick() = View.OnClickListener {
        when (it.id) {
            R.id.btn_sample -> {
                val direction =
                    MainSampleFragmentDirections.actionMainSampleFragmentToHomeSampleFragment()
                val extras = FragmentNavigatorExtras(it to getString(R.string.button_transition))
                it.navigateWithTransition(direction, extras)
            }
        }
    }
}