package br.com.digio.uber.sample.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.transition.TransitionInflater
import br.com.digio.uber.sample.databinding.HomeSampleFragmentBinding
import br.com.digio.uber.util.Const.AnimationDuration.TRANSITION_DURATION

class HomeSampleFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(requireContext())
            .inflateTransition(android.R.transition.move).setDuration(TRANSITION_DURATION)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return HomeSampleFragmentBinding.inflate(inflater).root
    }
}