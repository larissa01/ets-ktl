plugins {
    id("com.android.library")
    kotlin("android")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.library")
}

val debugSalt: String by project
val debugHavenPass: String by project
val debugIv: String by project
val homologSalt: String by project
val homologHavenPass: String by project
val homologIv: String by project
val releaseSalt: String by project
val releaseHavenPass: String by project
val releaseIv: String by project

configureAndroidLibrary(
    releaseBuildTypeConfig = {
        buildConfigField("String", "HAVEN_SALT", releaseSalt)
        buildConfigField("String", "HAVEN_KEY", releaseHavenPass)
        buildConfigField("String", "HAVEN_IV", releaseIv)
    },
    homolBuildTypeConfig = {
        buildConfigField("String", "HAVEN_SALT", homologSalt)
        buildConfigField("String", "HAVEN_KEY", homologHavenPass)
        buildConfigField("String", "HAVEN_IV", homologIv)
    },
    debugBuildTypeConfig = {
        buildConfigField("String", "HAVEN_SALT", debugSalt)
        buildConfigField("String", "HAVEN_KEY", debugHavenPass)
        buildConfigField("String", "HAVEN_IV", debugIv)
    }
)

dependencies {
    implementation(Dependencies.ANDROIDXCORE)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.COROUTINES)
    implementation(Dependencies.GLIDE)
    implementation(Dependencies.RETROFITGSONCONVERTER)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.PREFERENCE)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.GSON)
    implementation(Dependencies.FOTOAPPARAT)
    implementation(Dependencies.HAWK)
    implementation(Dependencies.CUSTOM_TABS)

    implementation(project(":model"))

    testImplementation(TestDependencies.JUNIT)
    testImplementation(TestDependencies.MOCKK)
}