package br.com.digio.uber.util

import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.BlockJUnit4ClassRunner

@RunWith(BlockJUnit4ClassRunner::class)
class SafeHeritageAndCastOrNullTest {

    @Test
    fun `try parse object to another object not Heritage and return null`() {
        class Test1
        class Test2

        val test1 = Test1()

        val result: Test2? = test1.safeHeritage()

        assertNull(result)
    }

    @Test
    fun `try parse object to another object is Heritage between and return another object`() {
        open class Test1
        class Test2 : Test1()

        val test2 = Test2()

        val result: Test1? = test2.safeHeritage()

        assertNotNull(result)
    }

    @Test
    fun `try parse list object to another list object object is Heritage between and return another object`() {
        open class Test1
        class Test2 : Test1()

        val test2 = (0..20).map { Test2() }

        val result: List<Test1>? = test2.safeHeritage()

        assertNotNull(result)
        assert(test2.isNotEmpty())
    }

    @Test
    fun `try parse list object to another list object not Heritage and return null`() {
        class Test1
        class Test2

        val test1 = (0..20).map { Test1() }

        val result: List<Test2> = test1.safeHeritage()

        assert(result.isEmpty())
    }
}