package br.com.digio.uber.util.encrypt

import br.com.digio.uber.util.BuildConfig
import io.mockk.every
import io.mockk.mockkStatic
import io.mockk.unmockkStatic
import io.mockk.verify
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNull
import org.junit.Assert.assertNotEquals
import org.junit.Test

class EncryptExtTest {

    @Test
    fun `on given a String and return string encrypt`() {
        val stringMock = "Teste"

        val stringResult = stringMock.encrypt()

        assertNotEquals(stringMock, stringResult)
    }

    @Test
    fun `on given a String and return error on encrypt`() {
        val stringMock = "Teste"
        val thro = RuntimeException("Error")
        val havenCryptoUtilClass = HavenCryptoUtil::class.java.name

        mockkStatic(havenCryptoUtilClass)

        every {
            HavenCryptoUtil.encrypt(stringMock, BuildConfig.HAVEN_IV, BuildConfig.HAVEN_KEY, BuildConfig.HAVEN_SALT)
        } throws thro

        try {
            val result = stringMock.encrypt()
            assertNull(result)
        } catch (e: RuntimeException) {
            assertEquals(thro, e)
        } catch (thr: Throwable) {
            assertNull(thr)
        }

        verify(exactly = 1) {
            HavenCryptoUtil.encrypt(stringMock, BuildConfig.HAVEN_IV, BuildConfig.HAVEN_KEY, BuildConfig.HAVEN_SALT)
        }

        unmockkStatic(havenCryptoUtilClass)
    }

    @Test
    fun `on given a String and return null on encrypt`() {
        val stringMock = "Teste"
        val thro = RuntimeException("Error")
        val havenCryptoUtilClass = HavenCryptoUtil::class.java.name

        mockkStatic(havenCryptoUtilClass)

        every {
            HavenCryptoUtil.encrypt(stringMock, BuildConfig.HAVEN_IV, BuildConfig.HAVEN_KEY, BuildConfig.HAVEN_SALT)
        } throws thro

        try {
            val result = stringMock.encryptOrNull()
            assertNull(result)
        } catch (e: RuntimeException) {
            assertNull(e)
        } catch (thr: Throwable) {
            assertNull(thr)
        }

        verify(exactly = 1) {
            HavenCryptoUtil.encrypt(stringMock, BuildConfig.HAVEN_IV, BuildConfig.HAVEN_KEY, BuildConfig.HAVEN_SALT)
        }

        unmockkStatic(havenCryptoUtilClass)
    }

    @Test
    fun `on given a String and return string decrypt`() {
        val stringMock = "Teste"

        val stringResult = stringMock.encrypt()

        val stringDecrypt = stringResult.decrypt()

        assertEquals(stringMock, stringDecrypt)
    }

    @Test
    fun `on given a String and return error on decrypt`() {
        val stringMock = "Teste"
        val thro = RuntimeException("Error")
        val havenCryptoUtilClass = HavenCryptoUtil::class.java.name
        val encryptStringMock = stringMock.encrypt()

        mockkStatic(havenCryptoUtilClass)

        every {
            HavenCryptoUtil.decrypt(
                encryptStringMock,
                BuildConfig.HAVEN_IV,
                BuildConfig.HAVEN_KEY,
                BuildConfig.HAVEN_SALT
            )
        } throws thro

        try {
            val result = encryptStringMock.decrypt()
            assertNull(result)
        } catch (e: RuntimeException) {
            assertEquals(thro, e)
        } catch (thr: Throwable) {
            assertNull(thr)
        }

        verify(exactly = 1) {
            HavenCryptoUtil.decrypt(
                encryptStringMock,
                BuildConfig.HAVEN_IV,
                BuildConfig.HAVEN_KEY,
                BuildConfig.HAVEN_SALT
            )
        }

        unmockkStatic(havenCryptoUtilClass)
    }

    @Test
    fun `on given a String and return null on decrypt`() {
        val stringMock = "Teste"
        val thro = RuntimeException("Error")
        val encryptStringMock = stringMock.encrypt()
        val havenCryptoUtilClass = HavenCryptoUtil::class.java.name

        mockkStatic(havenCryptoUtilClass)

        every {
            HavenCryptoUtil.decrypt(
                encryptStringMock,
                BuildConfig.HAVEN_IV,
                BuildConfig.HAVEN_KEY,
                BuildConfig.HAVEN_SALT
            )
        } throws thro

        try {
            val result = encryptStringMock.decryptOrNull()
            assertNull(result)
        } catch (e: RuntimeException) {
            assertNull(e)
        } catch (thr: Throwable) {
            assertNull(thr)
        }

        verify(exactly = 1) {
            HavenCryptoUtil.decrypt(
                encryptStringMock,
                BuildConfig.HAVEN_IV,
                BuildConfig.HAVEN_KEY,
                BuildConfig.HAVEN_SALT
            )
        }

        unmockkStatic(havenCryptoUtilClass)
    }
}