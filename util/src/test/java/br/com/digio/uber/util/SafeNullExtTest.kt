package br.com.digio.uber.util

import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.BlockJUnit4ClassRunner

@RunWith(BlockJUnit4ClassRunner::class)
class SafeNullExtTest {

    @Test
    fun `test safeLet if set two values and one this is null return null`() {
        val value1: String? = "Valor"
        val value2: String? = null

        val valueResult = safeLet(value1, value2) { _, _ ->
            throw RuntimeException("it was not supposed to come by")
        }
        assertNull(valueResult)
    }

    @Test
    fun `test safeLet if set two and has value return other value and execute scope`() {
        val value1: String? = "Valor"
        val value2: String? = "Valor2"

        val valueResult = safeLet(value1, value2) { value1Let, value2Let ->
            "Valor3 is $value1Let plus $value2Let"
        }
        assertNotNull(valueResult)
    }
}