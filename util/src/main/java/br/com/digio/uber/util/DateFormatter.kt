package br.com.digio.uber.util

import br.com.digio.uber.util.DatePattern.DD
import br.com.digio.uber.util.DatePattern.DDMMYYY
import br.com.digio.uber.util.DatePattern.DD_MM_YYYY_BR
import br.com.digio.uber.util.DatePattern.EE
import br.com.digio.uber.util.DatePattern.YYYY_DD_MM
import br.com.digio.uber.util.DatePattern.YYYY_DD_MM_T_M
import br.com.digio.uber.util.DatePattern.YYYY_DD_MM_T_M_S
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Locale

/**
 * @author Marlon D. Rocha
 * @since 06/11/20
 */
object DateFormatter {

    val dateDefaultFormatter: DateFormat = SimpleDateFormat(DDMMYYY, Locale.ROOT)

    val dateBRFormatter: DateFormat = SimpleDateFormat(DD_MM_YYYY_BR, Locale.ROOT)

    val dateUSFormatter: DateFormat = SimpleDateFormat(YYYY_DD_MM, Locale.ROOT)

    val simpleWeekDateFormatter = SimpleDateFormat(EE, Locale.ROOT)

    val simpleMonthDateFormatter = SimpleDateFormat(DD, Locale.ROOT)

    val zonedDateTimeUSFormatter: DateFormat =
        SimpleDateFormat(YYYY_DD_MM_T_M_S, Locale.ROOT)

    val zonedDateTimeShortUSFormatter: DateFormat =
        SimpleDateFormat(YYYY_DD_MM_T_M, Locale.ROOT)

    fun formatReceiptDateToBr(dateString: String): String? {
        val dateParts = dateString.split("-").toTypedArray()
        return String.format("%s/%s/%s", dateParts[2], dateParts[1], dateParts[0])
    }
}