package br.com.digio.uber.util

object PhoneUtils {

    private fun getClearText(phoneNumber: String): String {
        return phoneNumber.replace(" ", "")
            .replace("(", "")
            .replace(")", "")
            .replace("-", "")
    }

    fun checkPhoneNumberFormatted(phoneNumber: String): Boolean =
        phoneNumber.matches(FieldsRegex.PHONE_NUMBER_FULL.regex.toRegex())
}