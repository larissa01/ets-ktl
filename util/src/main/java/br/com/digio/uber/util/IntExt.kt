package br.com.digio.uber.util

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.TypedValue
import androidx.core.content.ContextCompat
import br.com.digio.uber.util.Const.LocaleConst.LOCALE_COUNTRY_BR
import br.com.digio.uber.util.Const.LocaleConst.LOCALE_PT
import java.text.NumberFormat
import java.util.Locale
import kotlin.math.roundToInt

fun Int.dpToPixel(context: Context): Int {
    val metrics = context.resources.displayMetrics
    val unit = TypedValue.COMPLEX_UNIT_DIP
    return TypedValue.applyDimension(unit, this.toFloat(), metrics).roundToInt()
}

fun Int.toColorRes(context: Context) = ContextCompat.getColor(context, this)

fun Int.toDrawableRes(context: Context): Drawable? = ContextCompat.getDrawable(context, this)

fun Int.numberFormat(): String =
    NumberFormat.getInstance(Locale(LOCALE_PT, LOCALE_COUNTRY_BR)).format(this)