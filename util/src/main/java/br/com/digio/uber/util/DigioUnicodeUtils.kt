package br.com.digio.uber.util

object DigioUnicodeUtils {
    const val BULLET = '\u2022'
    const val HAIR_SPACE = '\u200A'
    const val INFORMATION = '\u2139'
    const val DOT = '\u2022'
    const val SPACE = '\u200A'
}