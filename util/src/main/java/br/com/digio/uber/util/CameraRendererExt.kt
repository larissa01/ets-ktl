package br.com.digio.uber.util

import android.content.Context
import android.graphics.Point
import android.util.Size
import android.view.View
import io.fotoapparat.Fotoapparat
import io.fotoapparat.FotoapparatBuilder
import io.fotoapparat.error.CameraErrorCallback
import io.fotoapparat.parameter.FocusMode
import io.fotoapparat.parameter.Resolution
import io.fotoapparat.parameter.ScaleType
import io.fotoapparat.parameter.camera.CameraParameters
import io.fotoapparat.result.WhenDoneListener
import io.fotoapparat.selector.LensPositionSelector
import io.fotoapparat.selector.autoFocus
import io.fotoapparat.selector.back
import io.fotoapparat.selector.continuousFocusPicture
import io.fotoapparat.selector.firstAvailable
import io.fotoapparat.selector.fixed
import io.fotoapparat.selector.highestResolution
import io.fotoapparat.selector.off
import io.fotoapparat.view.CameraRenderer
import io.fotoapparat.view.FocusView

data class CameraRendererExt(
    val context: Context,
    val focusView: FocusView? = null,
    val lensPositionSelector: LensPositionSelector = back(),
    val focusMode: Iterable<FocusMode>.() -> FocusMode? = firstAvailable(
        continuousFocusPicture(),
        autoFocus(),
        fixed()
    ),
    val withoutFocusMode: Boolean = false,
    val withResolution: Boolean = true,
    val cameraErrorCallback: CameraErrorCallback? = null
)

fun CameraRenderer.fotoapparat(
    cameraRendererExt: CameraRendererExt
): Fotoapparat = cameraRendererExt.run {
    if (withoutFocusMode) {
        Fotoapparat.with(context).into(this@fotoapparat)
            .previewScaleType(ScaleType.CenterCrop)
            .lensPosition(lensPositionSelector)
            .photoResolution(withResolution)
            .focusView(focusView)
            .cameraErrorCallback {
                cameraErrorCallback?.invoke(it)
            }
            .flash(off())
            .build()
    } else {
        Fotoapparat.with(context).into(this@fotoapparat)
            .previewScaleType(ScaleType.CenterCrop)
            .lensPosition(lensPositionSelector)
            .photoResolution(withResolution)
            .focusView(focusView)
            .focusMode(focusMode)
            .cameraErrorCallback {
                cameraErrorCallback?.invoke(it)
            }
            .flash(off())
            .build()
    }
}

private const val MAX_RESOLUTION_1_CONFIG_W = 1280
private const val MAX_RESOLUTION_1_CONFIG_H = 720

private const val MAX_RESOLUTION_2_CONFIG_W = 640
private const val MAX_RESOLUTION_2_CONFIG_H = 480

private fun FotoapparatBuilder.photoResolution(selector: Boolean): FotoapparatBuilder =
    if (selector) {
        photoResolution(
            firstAvailable(
                {
                    val contains =
                        contains(Resolution(MAX_RESOLUTION_1_CONFIG_W, MAX_RESOLUTION_1_CONFIG_H))
                    if (contains) {
                        Resolution(MAX_RESOLUTION_1_CONFIG_W, MAX_RESOLUTION_1_CONFIG_H)
                    } else {
                        null
                    }
                },
                {
                    val contains =
                        contains(Resolution(MAX_RESOLUTION_2_CONFIG_W, MAX_RESOLUTION_2_CONFIG_H))
                    if (contains) {
                        Resolution(MAX_RESOLUTION_2_CONFIG_W, MAX_RESOLUTION_2_CONFIG_H)
                    } else {
                        null
                    }
                },
                highestResolution()
            )
        )
    } else {
        this
    }

private fun FotoapparatBuilder.focusView(focusView: FocusView?): FotoapparatBuilder =
    focusView?.let {
        this.focusView(it)
    } ?: run {
        this
    }

private const val ZERO = 0f
private const val PICTURE_WIDTH = 640
private const val PICTURE_HEIGHT = 480
private const val FRAC_POW_4 = 1.40f
private const val FRAC_POW_3 = 3.2f
private const val FRAC_POW_2 = 2.3f
private const val FRAC_POW_1 = 1.42f
private const val HD_RESOLUTION_WIDTH = 1280
private const val HD_RESOLUTION_HEIGHT = 720

fun Fotoapparat.calcAcesso(viewContainer: View, onFinished: (size: Size) -> Unit) {
    getCurrentParameters().whenDone(
        object : WhenDoneListener<CameraParameters> {
            override fun whenDone(it: CameraParameters?) {
                it?.pictureResolution

                val rectW: Float
                val rectH: Float
                var left = ZERO
                var top = ZERO
                var right = ZERO
                var bottom = ZERO

                val centerOfCanvas = Point(viewContainer.width / 2, viewContainer.height / 2)

                if (
                    it?.pictureResolution?.width == PICTURE_WIDTH &&
                    it.pictureResolution.height == PICTURE_HEIGHT
                ) {
                    rectW =
                        it.pictureResolution.width /
                        (FRAC_POW_2 / (viewContainer.height / PICTURE_WIDTH.toFloat()))
                    rectH =
                        it.pictureResolution.height /
                        (FRAC_POW_1 / (viewContainer.width / PICTURE_HEIGHT.toFloat()))

                    left = viewContainer.x - rectW / 2
                    top = centerOfCanvas.y - rectH / 2
                    right = centerOfCanvas.x + rectW / 2
                    bottom = centerOfCanvas.y + rectH / 2
                } else if (
                    it?.pictureResolution?.width == HD_RESOLUTION_WIDTH &&
                    it.pictureResolution.height == HD_RESOLUTION_HEIGHT
                ) {
                    rectW =
                        it.pictureResolution.width /
                        (FRAC_POW_3 / (viewContainer.height / HD_RESOLUTION_WIDTH.toFloat()))
                    rectH =
                        it.pictureResolution.height /
                        (FRAC_POW_4 / (viewContainer.width / HD_RESOLUTION_HEIGHT.toFloat()))

                    left = centerOfCanvas.x - rectW / 2
                    top = centerOfCanvas.y - rectH / 2
                    right = centerOfCanvas.x + rectW / 2
                    bottom = centerOfCanvas.y + rectH / 2
                }

                onFinished(Size((right.toInt() - left.toInt()), (bottom.toInt() - top.toInt())))
            }
        }
    )
}