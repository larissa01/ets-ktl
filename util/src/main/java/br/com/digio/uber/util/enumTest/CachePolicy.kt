package br.com.digio.uber.util.enumTest

/**
 * @author Marlon D. Rocha
 * @since 30/10/20
 */
enum class CachePolicy {
    NETWORK_FIRST,
    LOCAL_FIRST,
    NETWORK_ONLY,
    LOCAL_ONLY
}