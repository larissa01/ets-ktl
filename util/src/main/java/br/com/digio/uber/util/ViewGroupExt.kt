package br.com.digio.uber.util

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

fun ViewGroup.inflate(layoutRes: Int): View =
    LayoutInflater.from(context).inflate(layoutRes, this, false)

fun ViewGroup.inflateBinding(layoutRes: Int): ViewDataBinding =
    DataBindingUtil.inflate(LayoutInflater.from(context), layoutRes, this, false)

fun ViewGroup.inflate(layoutRes: Int, inflater: LayoutInflater): View =
    inflater.inflate(layoutRes, this, false)

fun ViewGroup.inflate(layoutRes: Int, inflater: LayoutInflater, theme: Int, activity: Activity): View =
    inflater.cloneInContext(ContextThemeWrapper(activity, theme)).inflate(layoutRes, this, false)