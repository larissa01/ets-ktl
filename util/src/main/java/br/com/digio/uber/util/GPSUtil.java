package br.com.digio.uber.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import androidx.annotation.Nullable;

import timber.log.Timber;

public class GPSUtil {

    private static final int TWO_MINUTES = 1000 * 60 * 2;
    public static final String[] PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_PHONE_STATE
    };
    public static final int PERMS_REQUEST_CODE = 200;
    private static GPSUtil instance;

    private final LocationManager locationManager;
    private final Context context;

    private GPSUtil(Context context) {
        this.context = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    public static synchronized GPSUtil getInstance(Context context) {
        if (instance == null) {
            instance = new GPSUtil(context);
        }
        return instance;
    }

    @Nullable
    public Location getBestLastKnownLocation() {
        if (PermissionExtKt.hasGpsPermission(context)) {
            try {
                Location lastKnownLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Location lastKnownLocationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (lastKnownLocationNetwork == null) {
                    return lastKnownLocationGPS;
                }
                if (lastKnownLocationGPS == null) {
                    return lastKnownLocationNetwork;
                }
                boolean networkBetterLocation = isBetterLocation(lastKnownLocationGPS, lastKnownLocationNetwork);
                if (networkBetterLocation) {
                    return lastKnownLocationNetwork;
                } else {
                    return lastKnownLocationGPS;
                }
            } catch (SecurityException e) {
                Timber.e(e);
            }
        }
        return null;
    }

    private static boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private static boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;
        if (isSignificantlyNewer) {
            return true;
        } else if (isSignificantlyOlder) {
            return false;
        }
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else {
            return isNewer && !isSignificantlyLessAccurate && isFromSameProvider;
        }
    }

}