package br.com.digio.uber.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Base64;

import java.io.DataInputStream;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import timber.log.Timber;

public class RSAUtil {

    public static String generateHash(Context context, String text) {
        try {
            byte[] bytes = rsaEncrypt(context.getResources().openRawResource(R.raw.public_key), text);
            return Base64.encodeToString(bytes, 0);
        } catch (Exception e) {
            Timber.e(e);
            return "";
        }
    }

    @SuppressLint("TrulyRandom")
    private static byte[] rsaEncrypt(InputStream publicKey, String text) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        PublicKey pk = getPublicKey(publicKey);
        cipher.init(Cipher.ENCRYPT_MODE, pk);
        return cipher.doFinal(text.getBytes());
    }

    private static PublicKey getPublicKey(InputStream publicKey) throws Exception {
        DataInputStream dis = new DataInputStream(publicKey);
        byte[] keyBytes = new byte[publicKey.available()];
        dis.readFully(keyBytes);
        dis.close();
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

}
