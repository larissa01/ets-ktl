package br.com.digio.uber.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async

fun <T> CoroutineScope.lazyPromise(block: suspend CoroutineScope.() -> T): Lazy<Deferred<T>> =
    lazy {
        async(start = CoroutineStart.LAZY) {
            block.invoke(this)
        }
    }