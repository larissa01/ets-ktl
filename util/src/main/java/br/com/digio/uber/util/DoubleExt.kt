package br.com.digio.uber.util

import br.com.digio.uber.util.Const.LocaleConst.LOCALE_COUNTRY_BR
import br.com.digio.uber.util.Const.LocaleConst.LOCALE_PT
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.Locale

fun Double.toMoney(withFraction: Boolean = false): String {
    val formatter = DecimalFormat("###.##")
    val symbol = "R\$"
    formatter.maximumFractionDigits = if (withFraction) 2 else 0
    formatter.minimumFractionDigits = if (withFraction) 2 else 0
    formatter.decimalFormatSymbols = formatter.decimalFormatSymbols.apply {
        groupingSeparator = '.'
        decimalSeparator = ','
    }
    formatter.positivePrefix = "$symbol "
    formatter.negativePrefix = "$symbol -"
    return formatter.format(this)
}

fun Double.toMoneyOnlyNumber(withDecimals: Boolean = false): String {
    val formatter = DecimalFormat.getInstance() as DecimalFormat
    formatter.minimumFractionDigits = if (withDecimals) 2 else 0
    formatter.maximumFractionDigits = if (withDecimals) 2 else 0
    formatter.decimalFormatSymbols = formatter.decimalFormatSymbols.apply {
        groupingSeparator = '.'
        decimalSeparator = ','
    }
    formatter.positivePrefix = ""
    formatter.negativePrefix = "- "
    return formatter.format(this)
}

fun Double.toCurrencyValue(symbol: Boolean): String {
    val country = LOCALE_COUNTRY_BR
    val language = LOCALE_PT
    val format: NumberFormat = NumberFormat.getCurrencyInstance(Locale(language, country))
    val currency: String = format.format(this)

    return if (symbol) currency else currency.substring(2)
}