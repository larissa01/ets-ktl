@file:Suppress("TooManyFunctions")

package br.com.digio.uber.util

import java.math.BigDecimal
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale

fun Double.toCurrencyNumber(): String {
    return this.toCurrency().replace("R$", "").trim()
}

fun Int.toCurrencyNumber(): String {
    return this.toCurrency().replace("R$", "").trim()
}

fun Double.toCurrency(): String {
    return NumberFormat.getCurrencyInstance(LOCALE_PT_BR).format(this)
}

fun Int.toCurrency(): String {
    return NumberFormat.getCurrencyInstance(LOCALE_PT_BR).format(this)
}

private const val ZERO_VALUE = 0.0
private const val HUNDRED_VALUE = 100
private val LOCALE_PT_BR by lazy { Locale("pt", "BR") }

fun BigDecimal.toMoney(type: EntryType): String = when (type) {
    EntryType.CREDIT -> this.toMoney()
    EntryType.DEBIT -> this.toNegativeMoney()
}

fun BigDecimal.toMoney() = formatWithSpace()

fun BigDecimal?.toMoneyOrEmpty(): String {
    return this?.formatWithSpace() ?: ""
}

private fun BigDecimal.formatWithSpace(): String {
    val numberFormat = NumberFormat.getCurrencyInstance(LOCALE_PT_BR)
    return numberFormat.format(this).run {
        val symbol = Currency.getInstance(localeDefault).symbol
        this.removeNoBreakingSpace()
            .replace(symbol, "$symbol ")
    }
}

fun BigDecimal.toNegativeMoney(): String {
    val numberFormat = NumberFormat.getCurrencyInstance(LOCALE_PT_BR)

    return if (this.compareTo(BigDecimal.ZERO) == 1) {
        numberFormat.format(this).run {
            val symbol = Currency.getInstance(localeDefault).symbol
            this.removeNoBreakingSpace()
                .replace(symbol, "$symbol -")
        }
    } else {
        numberFormat.format(this)
    }
}

fun BigDecimal.toMoneyNumber(): String {
    return this.toMoney().replace("R$", "").trim()
}

fun String?.toCurrencyValue(): Double {
    return (this?.onlyNumbers()?.toDouble() ?: ZERO_VALUE) / HUNDRED_VALUE
}

fun BigDecimal.isInRange(minimumValue: BigDecimal?, maximumValue: BigDecimal?): Boolean {
    return this.coerceIn(minimumValue, maximumValue) == this
}

enum class EntryType {
    DEBIT, CREDIT
}