package br.com.digio.uber.util

import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.Currency
import java.util.Locale

/**
 * @author Marlon D. Rocha
 * @since 30/09/20
 */
object MoneyUtils {
    fun toMoney(amount: Any, withDecimals: Boolean = false): String {
        val locale = Locale(Const.LocaleConst.LOCALE_PT, Const.LocaleConst.LOCALE_COUNTRY_BR)
        val symbol = Currency.getInstance(locale).symbol
        val formatter = getFormatter(withDecimals)
        formatter.positivePrefix = "$symbol "
        formatter.negativePrefix = "$symbol -"

        return formatter.format(amount)
    }

    fun toMoneyOnlyNumber(amount: Any, withDecimals: Boolean = false): String {
        val formatter = getFormatter(withDecimals)
        formatter.positivePrefix = ""
        formatter.negativePrefix = "- "

        return formatter.format(amount)
    }

    @Throws(NumberFormatException::class)
    fun toMoneyFraction(amount: Any): Pair<String, String> {
        val formatter = getFormatter(true)
        formatter.positivePrefix = ""
        formatter.negativePrefix = "- "
        val (decimal, fraction) = formatter.format(amount).split("[,]".toRegex())

        return decimal to fraction
    }

    fun stringMoneyToFloat(amount: String, withDecimals: Boolean = false): Float? {
        val locale = Locale(Const.LocaleConst.LOCALE_PT, Const.LocaleConst.LOCALE_COUNTRY_BR)
        val symbol = Currency.getInstance(locale).symbol
        val formatter = getFormatter(withDecimals)
        formatter.positivePrefix = symbol
        formatter.negativePrefix = "$symbol-"

        return formatter
            .parse(amount.replace("\\s+".toRegex(), "").replace("\u00A0", ""))?.toFloat()
    }

    fun String.hideMoney(): String = replace(
        "[0-9]".toRegex(),
        DigioUnicodeUtils.DOT.plus(DigioUnicodeUtils.SPACE.toString())
    )
        .replace(".", "")
        .replace(",", "")

    private fun getFormatter(withDecimals: Boolean): DecimalFormat {
        val formatter = DecimalFormat.getInstance() as DecimalFormat

        formatter.minimumFractionDigits = if (withDecimals) 2 else 0
        formatter.maximumFractionDigits = if (withDecimals) 2 else 0
        formatter.decimalFormatSymbols = formatter.decimalFormatSymbols.apply {
            groupingSeparator = '.'
            decimalSeparator = ','
        }
        return formatter
    }

    fun BigDecimal.toMoneyNumber(): String {
        return this.toMoney().replace("R$", "").trim()
    }
}