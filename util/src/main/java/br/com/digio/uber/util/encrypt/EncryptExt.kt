package br.com.digio.uber.util.encrypt

import br.com.digio.uber.util.BuildConfig
import timber.log.Timber

fun String.encrypt(): String =
    HavenCryptoUtil.encrypt(this, BuildConfig.HAVEN_IV, BuildConfig.HAVEN_KEY, BuildConfig.HAVEN_SALT)

fun String.decrypt(): String =
    HavenCryptoUtil.decrypt(this, BuildConfig.HAVEN_IV, BuildConfig.HAVEN_KEY, BuildConfig.HAVEN_SALT)

/**
 * there is suppress in this part due to the implementation of the algorithm in the java part, there is already a
 * check of the exceptions that can occur, due to that I put this suppress
 */
@Suppress("TooGenericExceptionCaught")
fun String.encryptOrNull(): String? =
    try {
        encrypt()
    } catch (thr: Throwable) {
        Timber.e(thr)
        null
    }

/**
 * there is suppress in this part due to the implementation of the algorithm in the java part, there is already a
 * check of the exceptions that can occur, due to that I put this suppress
 */
@Suppress("TooGenericExceptionCaught")
fun String.decryptOrNull(): String? =
    try {
        decrypt()
    } catch (thr: Throwable) {
        Timber.e(thr)
        null
    }