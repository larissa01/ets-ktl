package br.com.digio.uber.util

import android.os.Environment
import java.io.File
import java.io.FileOutputStream

/**
 * @author Marlon D. Rocha
 * @since 28/10/20
 */

fun ByteArray.saveAsDownload(fileName: String): File {
    val file = File(
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
        fileName
    )
    return FileOutputStream(file, true).use {
        it.write(this)
        it.flush()
        file
    }
}