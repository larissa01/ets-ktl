package br.com.digio.uber.util.enums

enum class CallCashbackFragment {
    HOME, MY_CASHBACK, CASHBACK_STORE, ALL_STORES
}