package br.com.digio.uber.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Environment
import android.util.Base64
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface
import br.com.digio.uber.util.Const.Utils.IMAGE_QUALITY_FULL
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import kotlin.jvm.Throws

fun getFileName(format: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG): String {
    val timeStamp = SimpleDateFormat(
        "yyyyMMdd_HHmmss",
        Locale.getDefault()
    ).format(
        Date()
    )
    return (if (Bitmap.CompressFormat.JPEG == format) "JPEG_" else "PNG_") + timeStamp
}

@Throws(Exception::class)
fun Context.createImageFile(format: Bitmap.CompressFormat): File {
    val prefix = getFileName()
    val suffix = if (Bitmap.CompressFormat.JPEG == format) ".jpg" else ".png"
    val directory = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    return File.createTempFile(prefix, suffix, directory)
}

fun Bitmap.convertToFile(
    context: Context,
    quality: Int = 100,
    format: Bitmap.CompressFormat = Bitmap.CompressFormat.PNG
): File? =
    try {
        val file = context.createImageFile(format)
        file.createNewFile()
        val bos = ByteArrayOutputStream()
        compress(Bitmap.CompressFormat.JPEG, quality, bos)
        val bitmapdata = bos.toByteArray()
        val fos = FileOutputStream(file)
        fos.write(bitmapdata)
        fos.flush()
        fos.close()
        file
    } catch (e: OutOfMemoryError) {
        null
    } catch (e: IllegalStateException) {
        null
    }

private const val MAX_QUALITY = 100

suspend fun File.compressToMaxSize(
    context: Context,
    maxSizeBytes: Long,
    maxInteraction: Int = 10,
    minQuality: Int = 10,
    stepSize: Int = 10
): File? =
    withContext(IO) {
        try {
            var copyToCahe: File? = copyToCache(context)
            var currentInteraction = 0

            delete()

            while (!(
                (
                    copyToCahe?.length()
                        ?: maxSizeBytes
                    ) <= maxSizeBytes || currentInteraction >= maxInteraction
                )
            ) {
                currentInteraction++
                val image = copyToCahe?.loadBitmap()
                copyToCahe = try {
                    val format = copyToCahe?.compressFormat()
                        ?: Bitmap.CompressFormat.JPEG
                    copyToCahe?.delete()
                    image?.convertToFile(
                        context,
                        (MAX_QUALITY - currentInteraction * stepSize)
                            .takeIf { it >= minQuality }
                            ?: minQuality,
                        format
                    )
                } finally {
                    image?.recycle()
                }
            }

            copyToCahe
        } catch (e: OutOfMemoryError) {
            null
        }
    }

fun File.loadBitmap(): Bitmap =
    BitmapFactory.decodeFile(absolutePath).run {
        determineImageRotation(this)
    }

private const val DEGREES_270 = 270f
private const val DEGREES_180 = 180f
private const val DEGREES_90 = 90f

private const val ORIENTATION_3 = 3
private const val ORIENTATION_6 = 6
private const val ORIENTATION_8 = 8

fun File.determineImageRotation(bitmap: Bitmap): Bitmap {
    val exif = ExifInterface(absolutePath)
    val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0)
    val matrix = Matrix()
    when (orientation) {
        ORIENTATION_6 -> matrix.postRotate(DEGREES_90)
        ORIENTATION_3 -> matrix.postRotate(DEGREES_180)
        ORIENTATION_8 -> matrix.postRotate(DEGREES_270)
    }
    return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
}

fun File.compressFormat() = when (extension.toLowerCase()) {
    "png" -> Bitmap.CompressFormat.PNG
    "webp" -> Bitmap.CompressFormat.WEBP
    else -> Bitmap.CompressFormat.JPEG
}

fun Bitmap.rotateBitmap(angle: Float): Bitmap {
    return rotateBitmap(angle, true)
}

fun Bitmap.rotateBitmap(angle: Float, flip: Boolean): Bitmap {
    val matrix = Matrix()
    if (flip) {
        matrix.postScale(-1f, 1f, (width / 2).toFloat(), (height / 2).toFloat())
    }
    matrix.postRotate(angle)
    return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
}

fun File.convertToByteArray(
    quality: Int = 100,
    format: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG
): ByteArray? =
    try {
        val fis = FileInputStream(this)
        val bm = BitmapFactory.decodeStream(fis)
        val baos = ByteArrayOutputStream()
        bm.compress(format, quality, baos)
        baos.toByteArray()
    } catch (e: FileNotFoundException) {
        Timber.e(e)
        null
    }

fun File.convertToBase64(
    quality: Int = 100,
    format: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG
): String? =
    try {
        Base64.encodeToString(convertToByteArray(quality, format), Base64.DEFAULT)
    } catch (e: FileNotFoundException) {
        Timber.e(e)
        null
    }

fun Bitmap.createUri(context: Context, authority: String, imageTitle: String): Uri {
    val cachePath = File(context.cacheDir, "images")
    cachePath.mkdirs()

    val bitmapPath = "$cachePath/$imageTitle"
    Timber.d("BitmapPath = $bitmapPath")

    FileOutputStream(bitmapPath).use {
        this.compress(Bitmap.CompressFormat.JPEG, IMAGE_QUALITY_FULL, it)
    }

    return FileProvider.getUriForFile(context, authority, File(bitmapPath)).also {
        Timber.d("File Uri = $it")
    }
}