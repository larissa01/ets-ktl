package br.com.digio.uber.util

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

suspend fun <T> executor(ex: suspend () -> T): T =
    withContext(Dispatchers.IO) { ex() }