package br.com.digio.uber.util

import androidx.core.text.HtmlCompat

class HtmlUtil private constructor() {

    companion object {
        fun fromHtml(source: String, flags: Int = HtmlCompat.FROM_HTML_MODE_LEGACY) = HtmlCompat.fromHtml(source, flags)
    }
}