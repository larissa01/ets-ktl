package br.com.digio.uber.util.mapper

interface AbstractMapper<in PARAMETER, out RESULT> {
    fun map(param: PARAMETER): RESULT
}