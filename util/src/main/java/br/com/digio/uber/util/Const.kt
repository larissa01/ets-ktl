package br.com.digio.uber.util

/**
 * @author Marlon D. Rocha
 * @since 29/09/20
 */

class Const {

    object AnimationDuration {
        const val TRANSITION_DURATION: Long = 300
    }

    object CryptoKey {
        const val KEY_ID = 2
    }

    object LocaleConst {
        const val LOCALE_PT = "pt"
        const val LOCALE_COUNTRY_BR = "BR"
    }

    object DatePattern {
        const val DD_MM_BR = "dd/MM"
        const val DD_MM_YYYY_BR = "dd/MM/yyyy"
        const val EEEE_DD_MM = "EEEE, dd MMMM"
        const val HH_MM_BR = "HH:mm"
    }

    object MaskPattern {
        const val CPF_LENGTH = 11
        const val CNPJ_LENGTH = 14
        const val CPF = "###.###.###-##"
        const val RG = "##.###.###-#"
        const val PHONE = "(##) #####-####"
        const val CEP = "#####-###"
        const val CNPJ = "##.###.###/####-##"
        const val DATE = "##/##/####"
        const val CELLPHONE = "(##)#####-####"
        const val PREFIX_BR = "+55 "
        const val BANK_NUMBER = "############-#"
        const val CASH_IN_BANKSLIP_NUMBER =
            "#####.#####.#####.#####.#####.#\n#### # ################"
        const val BARCODE_CIP = "#####.##### #####.###### #####.###### # ##############"
        const val BARCODE_ARRECADACAO = "########### #  ########### #  ########### #  ########### #"
    }

    object Utils {
        const val PAD_START_AGENCY_NUMBER = 4
        const val PAD_START_ACCOUNT_NUMBER = 13
        const val CNPJ_LENGHT = 14
        const val IMAGE_QUALITY_FULL = 100
        const val SINGLE_CLICK_INTERVAL = 1500
        const val MILLIS_COUNTDOWN = 1500L
        const val DELAY_MILLISECOND = 500L
        const val SPACE = "\u0020"
        const val OLD_18_YEAR_DAYS = 6575
        const val BANKSLIP_STATUS_CREATED = "CREATED"
        const val TAX_REVENUE_BARCODE_INITIAL_DIGIT = "8"
        const val BARCODE_TAX_REVENUE_LENGTH = 48
        const val BARCODE_CIP_LENGTH = 47
        const val INITIAL_WITHDRAW_VALUE = 0.0
        const val MIN_WITHDRAW_VALUE = 20
        const val MIDDLE_WITHDRAW_VALUE = 50
        const val MAX_WITHDRAW_VALUE = 100
        const val MIN_TIME_GPS = 110L
        const val MIN_DISTANCE_GPS = 110F
        const val RECEIPT_SERVICE_CODE: String = "8016_"
        const val ONE_DAY_UNIT = 1
        const val PAYMENT_TAX_REVENUE_SCHEDULING_LIMIT_DAYS = 30
        const val MILLIS_IN_1_SECOND = 1000
        const val SECONDS_IN_1_MINUTE = 60
        const val MINUTES_IN_1_HOUR = 60
        const val HOURS_IN_1_DAY = 24
        const val MULTIPLY_PADDING_RATIO = 2
    }

    object Extras {
        const val TOOLBAR_ID: String = "toolbar_id"
        const val RECEIPT_ID: String = "receipt_id"
        const val RECEIPT_ORIGIN = "RECEIPT_ORIGIN"
        const val CHOICES: String = "choices"
        const val DOT_PROVIDER = ".provider"
        const val FAQ_QUESTION = "faq_question"
        const val FRAGMENT_KEY = "fragment"
        const val STORE_PRODUCT = "store_product"
        const val POST_QRCODE_READ_USE_CASE_PARAMETER: String =
            "POST_QRCODE_READ_USE_CASE_PARAMETER"
        const val MESSAGE_QR_CODE = "MESSAGE_QR_CODE"
        const val RESULT_QR_CODE_KEY = "RESULT_QR_CODE_KEY"
        const val RESULT_QR_CODE_VALUE = "RESULT_QR_CODE_VALUE"
        const val PAYMENT_RECEIPT = "payment_receipt"
        const val CARD_ID = "CARD_ID"
        const val LAST_DIGITS = "LAST_DIGITS"
        const val REQUEST_PERMISSION_CODE: Int = 555
        const val ARG_CPF = "arg_cpf"
        const val ARG_INCOME = "arg_income"
        const val ARG_PIX_SUMMARY_PAYMENT = "arg_summary_payment"
        const val ARG_PIX_RECEIPT = "arg_receipt"
        const val PAYMENT_RESCHEDULED = "payment_rescheduled"
    }

    object Cache {
        const val KEY_CARD_BY_TYPE_CACHE = "key_card_by_type_cache"
        const val KEY_ACCOUNT_CACHE = "key_account_cache"
        const val KEY_BALANCE_CACHE = "key_balance_cache"
        const val KEY_REMUNERATION_INCOME_CACHE = "key_remuneration_income_cache"
    }

    object MimeTypes {
        const val RECEIPT_PRINT_FILENAME = "receipt.jpeg"
        const val PDF_MIME_TYPE = "application/pdf"
    }

    enum class Qualifier {
        ANDROID_ID
    }

    object RequestOnResult {
        object FacialBiometry {
            const val REQUEST_FACIAL_CAPTURE = 1500
            const val REQUEST_FACIAL_CAPTURE_SUCCESS = 1501
            const val REQUEST_FACIAL_VALUE = "REQUEST_FACIAL_VALUE"
            const val KEY_TAG_FIREBASE = "tagForFirebaseContinue"
        }

        object FeatureRefreshHome {
            const val KEY_FEATURE_REFRESH_BACK = 1600
            const val FEATURE_ITEM_RESULT_SUCCESS = "feature_item_result_success"
        }

        object ResetPassword {
            const val KEY_USER_LOGIN = "userLogin"
            const val RESULT_SUCCESS = "result_success"
        }

        object ChangePassword {
            const val KEY_SCREEN = "key_screen"
            const val KEY_DOCUMENT = "key_document"
            const val KEY_OLD_PASSWORD = "key_old_password"
            const val FIRST_LOGIN_SCREEN = "first_login_screen"
            const val CREATE_PASSWORD_SCREEN = "create_password_screen"
            const val OLD_PASSWORD_SCREEN = "old_password_screen"
            const val CHANGE_PASSWORD_REQUEST_CODE = 1400
        }

        object PID {
            const val PID_REQUEST_CODE = 1300
            const val RESULT_PID_ERROR = 222
            const val RESULT_PID_SUCCESS = 111
            const val KEY_PID_TYPE = "key_pid_type"
            const val KEY_PID = "key_pid"
            const val KEY_PID_HASH = "key_pid_hash"
        }

        object Payment {
            const val REQUEST_RESUMEBARCODE: Int = 4
            const val FORCE_QRCODE = "force_qrcode"
        }

        object QRCode {
            const val QR_REQUEST_CODE: Int = 17
        }

        object ShowCardPassword {
            const val KEY_SHOW_CARD_PASSWORD_HASH = "key_show_card_password_hash"
        }

        object Transfer {
            const val NEW_TRANSFER_REQUEST_CODE: Int = 1000
            const val FINISH_TRANSFER_FLOW_RESULT_CODE: Int = 1001
        }

        object Cashback {
            const val TOTAL_ELEMENTS: String = "total_elements"
            const val STORE: String = "store"
            const val ALL_CATEGORIES: String = "allCategories"
            const val FILTERED_CATEGORIES: String = "filteredCategories"
            const val URL_STORE: String = "urlStore"
            const val BUTTON_VISIBLE: String = "buttonVisible"
            const val MUST_FINISH: String = "mustFinish"
            const val CALL_CASHBACK_FRAGMENT: String = "callCashbackFragment"
        }

        object ChangeRegistration {
            const val KEY_CHANGE_REGISTRATION_BACK = "KEY_CHANGE_REGISTRATION_BACK"
        }

        object FaqDeepLink {
            const val KEY_FAQ_DEEP_LINK = "key_faq_deep_link"
        }

        object FaqSac {
            const val KEY_TOOLBAR_TITLE = "key_toolbar_title"
        }

        object FAQ {
            const val KEY_FAQ_CHAT = "key_faq_chat"
            const val KEY_VALUE_FAQ_CHAT_DELIVERY_ADDRESS = "tag_mudanca_cadastral"
        }

        object FeatureRefreshCardArea {
            const val KEY_REFRESH_BACK_CARD_AREA = 1700
            const val FEATURE_ITEM_RESULT_SUCCESS_CARD_AREA = "feature-item-result-success-card-area"
        }
    }

    object Activities {
        const val SPLASH_ACTIVITY = "br.com.digio.uber.splash.activity.SplashActivity"
        const val MAIN_ACTIVITY = "br.com.digio.uber.main.activity.MainActivity"
        const val DIGIO_TEST_ACTIVITY = "br.com.digio.uber.digiotest.ui.activity.DigioTestActivity"
        const val SAMPLE_ACTIVITY = "br.com.digio.uber.sample.ui.activity.SampleActivity"
        const val SAMPLE_NAVIGATION_ACTIVITY =
            "br.com.digio.uber.samplenavigationdigio.activity.SampleNavigationActivity"
        const val TRANSFER_ACTIVITY = "br.com.digio.uber.transfer.ui.activity.TransferActivity"
        const val BANK_SLIP_ACTIVITY = "br.com.digio.uber.bankslip.ui.activity.BankSlipActivity"
        const val BANK_SLIP_GENERATED_ACTIVITY =
            "br.com.digio.uber.bankslip.ui.activity.BankSlipGeneratedActivity"
        const val FEATURE_NAVIGATION_ACTIVITY =
            "br.com.digio.uber.featurenavigation.activity.FeatureNavigationActivity"
        const val GUIDE_CUSTOMS_ACTIVITY =
            "br.com.digio.uber.guidecustoms.activity.GuideCustomsActivity"
        const val LOGIN_ACTIVITY = "br.com.digio.uber.login.activity.LoginActivity"
        const val VIRTUAL_CARD_ACTIVITY =
            "br.com.digio.uber.virtualcard.ui.activity.VirtualCardActivity"
        const val RECHARGE_ACTIVITY = "br.com.digio.uber.recharge.ui.activity.RechargeActivity"
        const val HOME_TEMPORARY_ACTIVITY = "br.com.digio.uber.home.activity.HomeTempActivity"
        const val CHOICE_ACTIVITY = "br.com.digio.uber.choice.activity.ChoiceActivity"
        const val FAQ_ACTIVITY = "br.com.digio.uber.faq.activity.FaqActivity"
        const val FAQ_PDF = "br.com.digio.uber.faq.activity.FaqPdfActivity"
        const val FRANCHISE_ACTIVITY = "br.com.digio.uber.franchise.activity.FranchiseActivity"
        const val RECEIPT_ACTIVITY = "br.com.digio.uber.receipt.ui.ReceiptActivity"
        const val FACIAL_BIOMETRY_ACTIVITY =
            "br.com.digio.uber.facialbiometry.activity.FacialBiometryActivity"
        const val RESET_PASSWORD_ACTIVITY =
            "br.com.digio.uber.resetpassword.activity.ResetPasswordActivity"
        const val CHANGE_PASSWORD_ACTIVITY =
            "br.com.digio.uber.changepassword.activity.ChangePasswordActivity"
        const val QRCODE_ACTIVITY = "br.com.digio.uber.qrcode.activity.QRCodeActivity"
        const val PAYMENT_ACTIVITY = "br.com.digio.uber.payment.ui.activity.PaymentAccountActivity"
        const val VELOE_ACTIVITY = "br.com.digio.uber.veloe.ui.activity.VeloeActivity"
        const val MENU_ACTIVITY = "br.com.digio.uber.menu.activity.MenuActivity"
        const val STATEMENT_ACTIVITY = "br.com.digio.uber.statement.ui.activity.StatementActivity"
        const val PID_ACTIVITY = "br.com.digio.uber.pid.activity.PidActivity"
        const val INCOME_ACTIVITY = "br.com.digio.uber.income.ui.activity.IncomeActivity"
        const val CASHBACK_ACTIVITY = "br.com.digio.uber.cashback.ui.activity.CashbackActivity"
        const val SHOW_CARD_PASSWORD_ACTIVITY =
            "br.com.digio.uber.showcardpassword.activity.ShowCardPasswordActivity"
        const val STORE_MY_REQUEST_ACTIVITY = "br.com.digio.uber.store.activity.MyRequestActivity"
        const val WITHDRAW_ACTIVITY = "br.com.digio.uber.withdraw.ui.WithdrawActivity"
        const val TRACKING_ACTIVITY = "br.com.digio.uber.tracking.ui.activity.TrackingActivity"
        const val CHANGE_REGISTRATION_ACTIVITY =
            "br.com.digio.uber.changeregistration.activity.ChangeRegistrationActivity"
        const val CARD_RECEIVED_ACTIVITY = "br.com.digio.uber.cardReceived.ui.activity.CardReceivedActivity"
        const val CARD_REISSUE_ACTIVITY = "br.com.digio.uber.cardreissue.activity.CardReissueActivity"
        const val PIX_MENU_ACTIVITY = "br.com.digio.pix.ui.menu.PixMenuActivity"
        const val PIX_MY_KEY_ACTIVITY = "br.com.digio.pix.ui.mykey.PixMyKeyActivity"
        const val PIX_MENU_PAYMENT_ACTIVITY = "br.com.digio.pix.ui.payment.menu.PixMenuPaymentActivity"
        const val PIX_SUMMARY_PAYMENT_ACTIVITY = "br.com.digio.pix.ui.payment.summary.PixSummaryPaymentActivity"
        const val PIX_RECEIPT_ACTIVITY = "br.com.digio.pix.ui.receipt.PixReceiptActivity"
        const val PIX_RECEIVEMENT_INPUT_VALUES_ACTIVITY =
            "br.com.digio.pix.ui.receivement.inputvalues.PixReceivementInputValuesActivity"
        const val FAQ_DEEP_LINK_ACTIVITY = "br.com.digio.uber.faq.activity.FaqDeepLinkActivity"
        const val FAQ_CHAT_ACTIVITY = "br.com.digio.uber.faq.activity.FaqChatActivity"
    }

    object Fragments {
        const val TED_FRAGMENT = "TED"
        const val TED_VALUE_FRAGMENT = "TED_VALUE"
        const val P2P_FRAGMENT = "P2P"
    }

    object ShimmerConfig {
        const val DELAYED_DEFAULT = 1000L
    }

    object Notification {
        const val FCM_DEFAULT_CHANNEL = "fcm_default_channel"
    }

    object URI {
        const val ACQUISITION_DEV_URI = "https://dev-acquirement-uber.digio.com.br/"
        const val ACQUISITION_HML_URI = "https://hml-acquirement-uber.digio.com.br/"
        const val ACQUISITION_PROD_URI = "https://cadastro.ubercontabrasil.com.br"
        const val URL_PDF = "https://www.ubercontabrasil.com.br/termos-e-condicoes.pdf"
        const val URL_VELOI_ACCOUNT =
            "https://minha.veloe.com.br/conta/login?_ga=2.137982200.975023941.1607007225-1340414345.1602101468"
        const val URL_CORREIOS = "https://buscacepinter.correios.com.br/app/endereco/index.php?t"
    }

    object KeyVirtualCardOnboarding {
        const val KEY_NAME_VIRTUAL_CARD_ONBOARDING_IS_DONE: String =
            "VIRTUAL_CARD_ONBOARDING_IS_DONE"
        const val KEY_NAME_CARD_ONBOARDING_FIRST_ACCESS: String =
            "CARD_ONBOARDING_FIRST_ACCESS"
    }

    object ChoiceCategories {
        const val PixReceivementInputValuesActivity = "PixReceivementInputValuesActivity"
    }

    object DeepLink {
        const val DEEPLINK_FAQ_INITIAL = "digiouber://faqInitialFragment"
        const val DEEPLINK_FAQ_FRAGMENT = "digiouber://faqFragment"
        const val DEEPLINK_FAQ_SAC = "digiouber://faqSacFragment"
    }
}