package br.com.digio.uber.util

import android.content.Context
import android.util.TypedValue

fun Float.convertToPercent(): String {
    var percent = this.toString().replace('.', ',')
    val split = percent.split(",")
    if (split[1] == "0") {
        percent = split[0]
    }
    return "$percent%"
}

fun Float.dpToPixel(context: Context): Float {
    val metrics = context.resources.displayMetrics
    val unit = TypedValue.COMPLEX_UNIT_DIP
    return TypedValue.applyDimension(unit, this, metrics)
}

fun Float.spToPixel(context: Context): Float {
    val metrics = context.resources.displayMetrics
    val unit = TypedValue.COMPLEX_UNIT_SP
    return TypedValue.applyDimension(unit, this, metrics)
}