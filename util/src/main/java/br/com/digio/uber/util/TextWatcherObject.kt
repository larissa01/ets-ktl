package br.com.digio.uber.util

import android.text.TextWatcher
import android.widget.EditText

object TextWatcherObject {
    var textWatcherSingle: TextWatcher? = null

    fun EditText.addSingleInstanceOfTextWatcher(textWatcher: TextWatcher) {
        textWatcherSingle = textWatcher
        textWatcherSingle?.let { textWatcherSingleLet ->
            removeTextChangedListener(textWatcherSingleLet)
        }
        textWatcherSingle?.let { textWatcherSingleLet ->
            addTextChangedListener(textWatcherSingleLet)
        }
    }
}