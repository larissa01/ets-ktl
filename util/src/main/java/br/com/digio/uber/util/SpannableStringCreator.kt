package br.com.digio.uber.util

import android.text.SpannableString
import android.text.Spanned
import android.text.TextUtils
import br.com.digio.uber.util.Const.Utils.SPACE

class SpannableStringCreator {

    private val parts = ArrayList<CharSequence>()
    private var length = 0
    private val spanMap: MutableMap<IntRange, Iterable<Any>> = HashMap()

    fun spaceAppend(newText: CharSequence) = append(SPACE).append(newText)

    fun spaceAppend(newText: CharSequence, spans: Iterable<Any>) = append(SPACE).append(newText, spans)

    fun appendLn(newText: CharSequence, spans: Iterable<Any>? = null) = if (spans == null) append("\n") else append(
        "\n"
    ).append(newText, spans)

    fun appendImage(spans: Iterable<Any>) = append(" ").apply {
        parts.add(" ")
        spanMap[(length..length + 1)] = spans
        length += 1
    }

    fun append(newText: CharSequence, spans: Iterable<Any>) = apply {
        val end = newText.length
        parts.add(newText)
        spanMap[(length..length + end)] = spans
        length += end
    }

    fun append(newText: CharSequence) = apply {
        parts.add(newText)
        length += newText.length
    }

    @Suppress("SpreadOperator")
    fun toSpannableString() = SpannableString(
        TextUtils.concat(*parts.toTypedArray())
    ).apply {
        spanMap.forEach {
            val range = it.key
            it.value.forEach { span ->
                setSpan(span, range.first, range.last, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }
    }
}