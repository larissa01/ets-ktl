package br.com.digio.uber.util

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

fun Context.getDefaultSharedPreferences(): SharedPreferences? =
    PreferenceManager.getDefaultSharedPreferences(this)

fun Context.clearPreference(key: String) {
    val editor = getDefaultSharedPreferences()?.edit()
    editor?.remove(key)
    editor?.apply()
}

fun Context.clearAllPreference() {
    val editor = getDefaultSharedPreferences()?.edit()
    editor?.clear()
    editor?.apply()
}

fun <T> Context.setSharedValue(key: String, value: T) {
    val editor = getDefaultSharedPreferences()?.edit()
    when (value) {
        is String -> editor?.putString(key, value)
        is Int -> editor?.putInt(key, value)
        is Long -> editor?.putLong(key, value)
        is Float -> editor?.putFloat(key, value)
        is Boolean -> editor?.putBoolean(key, value)
    }
    editor?.apply()
}

inline fun <reified T : Any> Context.getSharedValue(key: String): T? {
    val shared = getDefaultSharedPreferences()
    return when (T::class) {
        String::class -> shared?.getString(key, null) as? T
        Int::class -> {
            val result = shared?.getInt(key, -1)
            if (result == -1) {
                null
            } else {
                result as? T
            }
        }
        Long::class -> {
            val result = shared?.getLong(key, -1L)
            if (result == -1L) {
                null
            } else {
                result as? T
            }
        }
        Float::class -> {
            val result = shared?.getFloat(key, -1f)
            if (result == -1f) {
                return null
            } else {
                result as? T
            }
        }
        Boolean::class -> shared?.getBoolean(key, false) as? T
        else -> null
    }
}