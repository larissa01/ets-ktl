package br.com.digio.uber.util.encrypt

import android.text.TextUtils
import br.com.digio.uber.util.GsonUtil
import com.google.gson.JsonSyntaxException
import com.orhanobut.hawk.Parser
import java.lang.reflect.Type
import kotlin.jvm.Throws

class HawkEncryptGsonParser : Parser {
    @Throws(JsonSyntaxException::class)
    override fun <T> fromJson(content: String, type: Type): T? {
        val contentEncrypt = content.decryptOrNull()
        return if (TextUtils.isEmpty(contentEncrypt)) {
            null
        } else GsonUtil.gson.fromJson(contentEncrypt, type)
    }

    override fun toJson(body: Any): String? {
        return GsonUtil.gson.toJson(body).encryptOrNull()
    }
}