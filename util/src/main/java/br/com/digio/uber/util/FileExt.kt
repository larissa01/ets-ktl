package br.com.digio.uber.util

import android.content.Context
import timber.log.Timber
import java.io.File
import java.io.File.separator
import java.io.IOException
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

fun Context.cachePath(subPath: String = "file_temp") =
    "${cacheDir.path}$separator$subPath$separator"

fun File.copyToCache(context: Context): File =
    copyTo(File("${context.cachePath()}$name"), true)

fun Array<TrustManager>.getX509TrustManager(): X509TrustManager? =
    mapNotNull { it.safeHeritage<X509TrustManager>() }.firstOrNull()

fun String.convertToFile(): File? =
    try {
        File(this)
    } catch (e: IOException) {
        Timber.e(e)
        null
    }