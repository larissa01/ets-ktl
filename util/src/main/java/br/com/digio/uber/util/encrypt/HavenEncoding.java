package br.com.digio.uber.util.encrypt;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Base64;
import java.util.ArrayList;
import java.util.List;

public class HavenEncoding {

    public String encode(String s, String key) {
        if (Build.VERSION.SDK_INT > 0 && Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return base64Encode(xorWithKey(s.getBytes(), key.getBytes()));
        } else {
            return base64EncodeApi26(xorWithKey(s.getBytes(), key.getBytes()));
        }
    }

    public static String decString(String input) {
        String o = decode(input, "$sbZGRaq");
        String[] arrayOutput = new String[o.length()];
        for (int i = 0; i < o.length(); i++) {
            if (i % 2 == 0) {
                arrayOutput[i + 1] = String.valueOf(o.charAt(i));
            } else {
                arrayOutput[i - 1] = String.valueOf(o.charAt(i));
            }
        }
        return getStringInArray(arrayOutput);
    }

    public String desinvertChars(String input) {
        String[] arrayOutput = new String[input.length()];
        for (int i = 0; i < input.length(); i++) {
            if (i % 2 == 0) {
                arrayOutput[i + 1] = String.valueOf(input.charAt(i));
            } else {
                arrayOutput[i - 1] = String.valueOf(input.charAt(i));
            }
        }
        return getStringInArray(arrayOutput);

    }

    public String invertChars(String input) {
        String[] arrayOutput = new String[input.length()];

        for (int i = 0; i < input.length(); i++) {
            if (i % 2 == 0) {
                arrayOutput[i + 1] = String.valueOf(input.charAt(i));
            } else {
                arrayOutput[i - 1] = String.valueOf(input.charAt(i));
            }
        }
        return getStringInArray(arrayOutput);
    }

    public static String getStringInArray(String[] array) {
        StringBuilder output = new StringBuilder();
        for (int j = 0; j < array.length; j++) {
            output.append(array[j]);
        }
        return output.toString();
    }

    public static String decode(String s, String key) {
        if (Build.VERSION.SDK_INT > 0 && Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return new String(xorWithKey(base64Decode(s), key.getBytes()));
        } else {
            return new String(xorWithKey(base64DecodeApi26(s), key.getBytes()));
        }
    }

    public String shuffle(String input) {
        List<Character> characters = new ArrayList<Character>();
        for (char c : input.toCharArray()) {
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while (characters.size() != 0) {
            int randPicker = (int) (Math.random() * characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }

    public String myShuffle(String input) {
        List<Character> characters = new ArrayList<Character>();
        for (char c : input.toCharArray()) {
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while (characters.size() != 0) {
            int randPicker = (int) (Math.random() * characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }

    private static byte[] xorWithKey(byte[] a, byte[] key) {
        byte[] out = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            out[i] = (byte) (a[i] ^ key[i % key.length]);
        }
        return out;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static byte[] base64DecodeApi26(String s) {
        byte[] bytesEncoded = java.util.Base64.getDecoder().decode(s.getBytes());
        return bytesEncoded;
    }

    public static byte[] base64Decode(String s) {
        return Base64.decode(s.getBytes(), Base64.DEFAULT);
    }

    @TargetApi(Build.VERSION_CODES.O)
    public static String base64EncodeApi26(byte[] bytes) {
        String base64 = java.util.Base64.getEncoder().encodeToString(bytes);
        return base64.replaceAll("\\s", "");
    }

    public static String base64Encode(byte[] bytes) {
        String base64 = Base64.encodeToString(bytes, Base64.DEFAULT);
        return base64.replaceAll("\\s", "");
    }

}
