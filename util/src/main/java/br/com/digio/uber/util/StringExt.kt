@file:Suppress(
    "TooManyFunctions",
    "LongMethod",
    "ComplexMethod",
    "ComplexCondition",
    "MagicNumber",
    "ForEachOnRange",
    "NestedBlockDepth"
)

package br.com.digio.uber.util

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StyleSpan
import android.util.Base64
import br.com.digio.uber.util.Const.MaskPattern.CNPJ
import br.com.digio.uber.util.Const.MaskPattern.CPF
import br.com.digio.uber.util.Const.Utils.CNPJ_LENGHT
import br.com.digio.uber.util.Const.Utils.PAD_START_ACCOUNT_NUMBER
import br.com.digio.uber.util.Const.Utils.PAD_START_AGENCY_NUMBER
import java.math.BigDecimal
import java.text.Normalizer
import java.util.Calendar
import java.util.Locale
import java.util.regex.Pattern

fun String.toStringWithoutAccent(): String {
    val nfdNormalizedString =
        Normalizer.normalize(this, Normalizer.Form.NFD)
    val pattern =
        Pattern.compile("\\p{InCombiningDiacriticalMarks}+")
    return pattern.matcher(nfdNormalizedString).replaceAll("").toLowerCase(Locale.ROOT)
}

fun String.withTypeface(typeface: Int): Spannable {
    return SpannableString(this).apply {
        setSpan(StyleSpan(typeface), 0, length, Spanned.SPAN_INCLUSIVE_INCLUSIVE)
    }
}

fun String.onlyNumbers(): String = this removeAll "\\D".toRegex()

infix fun String.removeAll(regex: Regex): String = this.replace(regex, "")

fun String.capitalizeWords(): String = split(" ").joinToString(" ") { it.capitalize() }

fun String.convertDateBRtoUS(): String? = toCalendar(FORMAT_PT_BR_DATE)?.getDate(FORMAT_US_DATE)

fun String.setInClipboard(label: String? = null, context: Context) {
    val clipboard = context.getSystemService(CLIPBOARD_SERVICE)?.safeHeritage<ClipboardManager>()
    clipboard?.setPrimaryClip(ClipData.newPlainText(label, this))
}

fun String.fromHtml(): Spanned? {
    return HtmlUtil.fromHtml(this)
}

fun String.getFinalDigits(size: Int): String =
    substring(this.length - size)

fun String.getMonth(pattern: String): Int? =
    toCalendar(pattern)?.get(Calendar.MONTH)?.plus(1)

fun String.getFirstName(): String {
    val splitName = this.split(" ")

    return if (splitName.isNotEmpty()) {
        splitName[0]
    } else {
        this
    }
}

fun String.getNameAbbreviation(): String {
    val firstLetter = this.substring(0, 1)
    val splitName = this.trim().split(" ")
    val secondLetter =
        if (splitName.size <= 1) "" else splitName[splitName.size - 1].substring(0, 1)

    return "$firstLetter$secondLetter".toUpperCase(Locale.getDefault())
}

fun String.formatAsAgencyNumber(): String {
    return this.padStart(PAD_START_AGENCY_NUMBER, '0')
}

fun String.formatAsFullAccountNumber(): String {
    return this.padStart(PAD_START_ACCOUNT_NUMBER, '0').applyMask(Const.MaskPattern.BANK_NUMBER)
}

fun String.formatAsAccountNumber(): String {
    val builder = StringBuilder(this)
    builder.insert(this.length - 1, "-")
    return builder.toString()
}

fun String.unmask(): String = this removeAll "\\D".toRegex()

fun String.toByteArray(): ByteArray = Base64.decode(this, Base64.DEFAULT)

fun String.removeNoBreakingSpace(): String {
    return replace('\u00A0'.toString(), "")
}

const val MAX_COUNT_OF_CPF = 11

fun String.formatAsDocument(): String {
    return if (this.unmask().length <= MAX_COUNT_OF_CPF) this.unmask().applyMask(CPF)
    else this.unmask().applyMask(
        CNPJ
    )
}

fun String.getFinalDigitsCardNumber(): String =
    if (isNullOrEmpty()) "" else "**** " + getFinalDigits(4)

fun String?.validateCpf(): Boolean {
    var isValid = false

    if (this != null) {
        isValid = true
        val strCpf: String = onlyNumbers().trim { it <= ' ' }

        if ("^([0-9])\\1*\$".toRegex().matches(strCpf) ||
            strCpf.length != MAX_COUNT_OF_CPF
        ) {
            isValid = false
        }

        if (isValid) {
            var d1: Int
            var d2: Int
            var digito1: Int
            var digito2: Int
            var resto: Int
            var digitoCPF: Int
            val nDigResult: String

            d1 = 0.also { d2 = it }
            digito1 = 0.also { resto = it }.also { digito2 = it }

            for (nCount in 1 until strCpf.length - 1) {
                digitoCPF = Integer.valueOf(strCpf.substring(nCount - 1, nCount)).toInt()

                d1 += (MAX_COUNT_OF_CPF - nCount) * digitoCPF

                d2 += ((MAX_COUNT_OF_CPF + 1) - nCount) * digitoCPF
            }

            resto = d1 % MAX_COUNT_OF_CPF

            digito1 = if (resto < 2) {
                0
            } else {
                MAX_COUNT_OF_CPF - resto
            }

            d2 += 2 * digito1

            resto = d2 % MAX_COUNT_OF_CPF

            digito2 = if (resto < 2) {
                0
            } else {
                MAX_COUNT_OF_CPF - resto
            }

            val nDigVerific = strCpf.substring(strCpf.length - 2, strCpf.length)

            nDigResult = digito1.toString() + digito2.toString()

            isValid = nDigVerific == nDigResult
        }
    }
    return isValid
}

fun String?.validateDocument(): Boolean {
    var isValid = false
    if (!this.isNullOrBlank()) {
        isValid = true
        val numbers = arrayListOf<Int>()

        this.filter { it.isDigit() }.forEach {
            numbers.add(it.toString().toInt())
        }

        if (numbers.size != 11) isValid = false

        if (isValid) {
            (0..9).forEach { n ->
                val digits = arrayListOf<Int>()
                (0..10).forEach { _ -> digits.add(n) }
                if (numbers == digits) isValid = false
            }

            val dv1 = ((0..8).sumBy { (it + 1) * numbers[it] }).rem(11).let {
                if (it >= 10) 0 else it
            }

            val dv2 = ((0..8).sumBy { it * numbers[it] }.let { (it + (dv1 * 9)).rem(11) }).let {
                if (it >= 10) 0 else it
            }

            if (isValid) {
                isValid = numbers[9] == dv1 && numbers[10] == dv2
            }
        }
    }
    return isValid
}

fun String.unmaskMoney(): BigDecimal {
    return this.onlyNumbers().toBigDecimal().divide(BigDecimal("100"))
}

fun String.applyMask(mask: String): String {
    if (this.trim { it <= ' ' }.isEmpty()) {
        return ""
    }

    val newValue = this.unmask()

    val cMask = mask.toCharArray()

    val maskedNewValue = CharArray(cMask.size)

    var i = 0
    var j = 0
    while (i < maskedNewValue.size) {
        val maskChar = cMask[i]

        val hasNewValue = newValue.length > j

        if (hasNewValue && maskChar == '#') {
            maskedNewValue[i] = newValue[j++]
        } else if (hasNewValue) {
            maskedNewValue[i] = maskChar
        } else {
            maskedNewValue[i] = ' '
        }
        i++
    }

    return String(maskedNewValue).trim { it <= ' ' }
}

fun String.validateCNPJ(): Boolean {
    val cnpj = this
    return validateCNPJLength(cnpj) && validateCNPJRepeatedNumbers(cnpj) &&
        validateCNPJVerificationDigit(true, cnpj) &&
        validateCNPJVerificationDigit(false, cnpj)
}

private fun validateCNPJLength(cnpj: String) = cnpj.length == CNPJ_LENGHT

@Suppress("MagicNumber")
private fun validateCNPJRepeatedNumbers(cnpj: String): Boolean {
    return (0..9)
        .map { it.toString().repeat(CNPJ_LENGHT) }
        .map { cnpj == it }
        .all { !it }
}

/**
 * Verifies the CNPJ verification digit.
 *
 * This algorithm checks the verification digit (dígito verificador) do CNPJ.
 * This was based from: https://www.devmedia.com.br/validando-o-cnpj-em-uma-aplicacao-java/22374
 *
 * @param[firstDigit] True when checking the first digit. False to check the second digit.
 *
 * @return True if valid.
 */
@Suppress("MagicNumber")
private fun validateCNPJVerificationDigit(firstDigit: Boolean, cnpj: String): Boolean {
    val startPos = when (firstDigit) {
        true -> 11
        else -> 12
    }
    val weightOffset = when (firstDigit) {
        true -> 0
        false -> 1
    }
    val sum = (startPos downTo 0).fold(0) { acc, pos ->
        val weight = 2 + ((11 + weightOffset - pos) % 8)
        val num = cnpj[pos].toString().toInt()
        val sum = acc + (num * weight)
        sum
    }
    val result = sum % 11
    val expectedDigit = when (result) {
        0, 1 -> 0
        else -> 11 - result
    }

    val actualDigit = cnpj[startPos + 1].toString().toInt()

    return expectedDigit == actualDigit
}

private val REGEX_UNACCENT = "\\p{InCombiningDiacriticalMarks}+".toRegex()

fun String.unaccent(): String {
    val temp = Normalizer.normalize(this, Normalizer.Form.NFD)
    return REGEX_UNACCENT.replace(temp, "")
}