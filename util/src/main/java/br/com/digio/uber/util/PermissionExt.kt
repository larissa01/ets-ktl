package br.com.digio.uber.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

fun Context.hasPermission(permission: String): Boolean {
    val hasPermission = ContextCompat.checkSelfPermission(this, permission)
    return hasPermission == PackageManager.PERMISSION_GRANTED
}

fun Context.hasGpsPermission(): Boolean {
    val hasFineLocation = hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    val hasCoarseLocation = hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
    return hasFineLocation && hasCoarseLocation
}

fun Context.hasPhonePermission(): Boolean {
    return hasPermission(Manifest.permission.READ_PHONE_STATE)
}