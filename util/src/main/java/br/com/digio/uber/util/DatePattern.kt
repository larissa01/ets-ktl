package br.com.digio.uber.util

object DatePattern {
    const val DD_MM_BR = "dd/MM"
    const val DD_MM_YYYY_BR = "dd/MM/yyyy"
    const val EEEE_DD_MM = "EEEE, dd MMMM"
    const val HH_MM_BR = "HH:mm"
    const val DDMMYYY = "ddMMyyyy"
    const val YYYY_DD_MM = "yyyy-MM-dd"
    const val YYYY_DD_MM_T_M_S = "yyyy-MM-dd'T'HH:mm:ss"
    const val YYYY_DD_MM_T_M = "yyyy-MM-dd'T'HH:mm"
    const val EE = "EE"
    const val DD = "dd"
}