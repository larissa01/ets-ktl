package br.com.digio.uber.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

fun <T, K, R> LiveData<T>.combineWith(
    liveData: LiveData<K>,
    block: (T?, K?) -> R
): LiveData<R> {
    val result = MediatorLiveData<R>()
    result.addSource(this) {
        result.value = block.invoke(this.value, liveData.value)
    }
    result.addSource(liveData) {
        result.value = block.invoke(this.value, liveData.value)
    }
    return result
}

fun <T, K, P, R> LiveData<T>.combineWith(
    liveDataK: LiveData<K>,
    liveDataP: LiveData<P>,
    block: (T?, K?, P?) -> R
): LiveData<R> {
    val result = MediatorLiveData<R>()
    result.addSource(this) {
        result.value = block.invoke(this.value, liveDataK.value, liveDataP.value)
    }
    result.addSource(liveDataK) {
        result.value = block.invoke(this.value, liveDataK.value, liveDataP.value)
    }
    result.addSource(liveDataP) {
        result.value = block.invoke(this.value, liveDataK.value, liveDataP.value)
    }
    return result
}

fun <T, P1, P2, P3, R> LiveData<T>.combineWith(
    liveDataP1: LiveData<P1>,
    liveDataP2: LiveData<P2>,
    liveDataP3: LiveData<P3>,
    block: (T?, P1?, P2?, P3?) -> R
): LiveData<R> {
    val result = MediatorLiveData<R>()
    result.addSource(this) {
        result.value = block.invoke(this.value, liveDataP1.value, liveDataP2.value, liveDataP3.value)
    }
    result.addSource(liveDataP1) {
        result.value = block.invoke(this.value, liveDataP1.value, liveDataP2.value, liveDataP3.value)
    }
    result.addSource(liveDataP2) {
        result.value = block.invoke(this.value, liveDataP1.value, liveDataP2.value, liveDataP3.value)
    }
    result.addSource(liveDataP3) {
        result.value = block.invoke(this.value, liveDataP1.value, liveDataP2.value, liveDataP3.value)
    }
    return result
}