@file:Suppress("TooManyFunctions")

package br.com.digio.uber.util

import br.com.digio.uber.util.Const.LocaleConst.LOCALE_COUNTRY_BR
import br.com.digio.uber.util.Const.LocaleConst.LOCALE_PT
import br.com.digio.uber.util.Const.Utils.HOURS_IN_1_DAY
import br.com.digio.uber.util.Const.Utils.MILLIS_IN_1_SECOND
import br.com.digio.uber.util.Const.Utils.MINUTES_IN_1_HOUR
import br.com.digio.uber.util.Const.Utils.OLD_18_YEAR_DAYS
import br.com.digio.uber.util.Const.Utils.SECONDS_IN_1_MINUTE
import timber.log.Timber
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit

const val FORMAT_US_DATE = "yyyy-MM-dd"
const val FORMAT_PT_BR_DATE = "dd/MM/yyyy"
const val FORMAT_PT_BR_DATE_WITHOUT_BAR = "ddMMyyyy"
const val FORMAT_PT_BR_TIME = "HH:mm:ss"
const val FORMAT_PT_BR_TIME_MINUTES = "HH:mm"
const val FORMAT_US_DATE_TIME_MINUTES = "yyyy-MM-dd'T'HH:mm"
const val FORMAT_PT_BR_DATE_TIME_MINUTES_SECONDS = "dd/MM/yyyy 'às' HH:mm:ss"
const val FORMAT_PT_BR_DATE_TIME_MINUTES = "dd/MM/yyyy 'às' HH:mm"
const val FORMAT_US_DATE_TIME_MINUTES_SECONDS = "yyyy-MM-dd'T'HH:mm:ss"
const val FORMAT_PT_BR_DATE_WITH_TIME = "$FORMAT_PT_BR_DATE $FORMAT_PT_BR_TIME"
const val DATE_YEAR = "y"
const val YEAR = "yyyy"
const val MONTH = "MM"
const val SHORT_MONTH = "MMM"
const val ONLY_DAY = "dd"
const val FULL_MONTH = "MMMM"
const val FORMAT_DUE_DATE = "dd/MM"
const val FORMAT_PT_BR_MONTH_AND_YEAR = "MM/yy"
const val FORMAT_US_DATE_COMPLETE2 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
const val FORMAT_SHORT_MONTH_WITH_DAY = "$SHORT_MONTH $ONLY_DAY"
const val FORMAT_DAY_SHORT_MONTH_YEAR = "$ONLY_DAY $SHORT_MONTH $YEAR"
const val FORMAT_PT_BR_DATE_TIME_MINUTES_WITH_BAR_SEPARATOR = "$FORMAT_PT_BR_DATE | $FORMAT_PT_BR_TIME_MINUTES"

val localeDefault: Locale by lazy {
    Locale(LOCALE_PT, LOCALE_COUNTRY_BR)
}

fun getInstanceOfCalendarDefault(locale: Locale = localeDefault, init: (Calendar.() -> Unit)? = null): Calendar =
    Calendar.getInstance(locale).apply {
        init?.invoke(this)
    }

fun Calendar.formatWith(format: String): String {
    val sdf = SimpleDateFormat(format, localeDefault)
    return sdf.format(time)
}

fun Calendar.formatWithOrNull(format: String): String? {
    return try {
        formatWith(format)
    } catch (e: ParseException) {
        Timber.e(e)
        null
    }
}

fun Calendar.getYear(): String? = formatWith(YEAR)

fun Calendar.getShortMonth(): String? = formatWith(SHORT_MONTH)

fun Calendar.getShortMonthWithDay(): String? = formatWith(FORMAT_SHORT_MONTH_WITH_DAY)

fun Calendar.getMonth(): String? = formatWith(MONTH)

fun Calendar.getFullMonth(): String? = formatWith(FULL_MONTH)

fun Calendar.getDate(format: String = FORMAT_US_DATE): String? = formatWith(format)

fun Calendar.getDueDate(): String? = formatWith(FORMAT_DUE_DATE)

fun Calendar.getOnlyYear(): String? = formatWith(DATE_YEAR)

fun Calendar.getDateBR(): String? = formatWith(FORMAT_PT_BR_DATE)

fun Calendar.getTimeBR(): String? = formatWith(FORMAT_PT_BR_TIME)

fun Calendar.getTimeMinutesBR(): String? = formatWith(FORMAT_PT_BR_TIME_MINUTES)

fun Calendar.getMonthSlashYearOnly(): String? = formatWith(FORMAT_PT_BR_MONTH_AND_YEAR)

fun Calendar.getDateTimeWithBarSeparator(): String? = formatWith(FORMAT_PT_BR_DATE_TIME_MINUTES_WITH_BAR_SEPARATOR)

fun Calendar.getDateTime(): String = formatWith(FORMAT_PT_BR_DATE_TIME_MINUTES)

fun Calendar.getDayShortMonthYear(): String = formatWith(FORMAT_DAY_SHORT_MONTH_YEAR)

fun getCurrentFullMonth(): String? =
    Calendar.getInstance(localeDefault).getFullMonth()

fun getCurrentDate(): String? =
    Calendar.getInstance(localeDefault).getDate()

fun Calendar.copy(locale: Locale = localeDefault): Calendar {
    val newCalendar = getInstanceOfCalendarDefault(locale)
    newCalendar.time = time
    return newCalendar
}

fun Calendar.isTheSameDate(other: Calendar): Boolean =
    (
        other.get(Calendar.ERA) == get(Calendar.ERA) &&
            isTheSameYear(other) &&
            isTheSameDay(other)
        )

fun Calendar.isTheSameYear(other: Calendar): Boolean =
    other.get(Calendar.YEAR) == get(Calendar.YEAR)

fun Calendar.isTheSameDay(other: Calendar): Boolean =
    other.get(Calendar.DAY_OF_MONTH) == get(Calendar.DAY_OF_MONTH)

fun Long.millisToDays(): Long = this / MILLIS_IN_1_SECOND / SECONDS_IN_1_MINUTE / MINUTES_IN_1_HOUR / HOURS_IN_1_DAY

fun Long.daysToMillis(): Long = this * MILLIS_IN_1_SECOND * SECONDS_IN_1_MINUTE * MINUTES_IN_1_HOUR * HOURS_IN_1_DAY

fun String.toSimpleDateFormat(): SimpleDateFormat =
    SimpleDateFormat(this, Locale.ROOT)

fun String.toCalendar(): Calendar {
    val trimmedText = this.trim().replace(" ", "")

    val dateFormat: DateFormat = when {
        trimmedText.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}".toRegex()) -> FORMAT_US_DATE.toSimpleDateFormat()
        trimmedText.matches("[0-9]{1,2}/[0-9]{2}/[0-9]{4}".toRegex()) -> FORMAT_PT_BR_DATE.toSimpleDateFormat()
        trimmedText.matches("[0-9]{8}".toRegex()) -> FORMAT_PT_BR_DATE_WITHOUT_BAR.toSimpleDateFormat()
        trimmedText.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}".toRegex()) ->
            FORMAT_US_DATE_TIME_MINUTES.toSimpleDateFormat()
        trimmedText.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}".toRegex()) ->
            FORMAT_US_DATE_TIME_MINUTES_SECONDS.toSimpleDateFormat()
        trimmedText.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}\\w?".toRegex()) ||
            trimmedText.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}".toRegex()) ||
            trimmedText.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\\w?".toRegex()) ->
            FORMAT_US_DATE_COMPLETE2.toSimpleDateFormat()
        else -> throw IllegalArgumentException("Date '$this' didn't match any patterns.")
    }

    return Calendar.getInstance().apply {
        dateFormat.parse(this@toCalendar)?.let { time = it }
    }
}

fun String?.toCalendarOrNull(): Calendar? {
    val trimmedText = this?.trim()?.replace(" ", "")
    val dateFormat: DateFormat

    trimmedText?.let { trimmedTextLet ->
        dateFormat = when {
            trimmedTextLet.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}".toRegex()) -> FORMAT_US_DATE.toSimpleDateFormat()
            trimmedTextLet.matches("[0-9]{1,2}/[0-9]{2}/[0-9]{4}".toRegex()) -> FORMAT_PT_BR_DATE.toSimpleDateFormat()
            trimmedTextLet.matches("[0-9]{8}".toRegex()) -> FORMAT_PT_BR_DATE_WITHOUT_BAR.toSimpleDateFormat()
            trimmedTextLet.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}".toRegex()) ->
                FORMAT_US_DATE_TIME_MINUTES.toSimpleDateFormat()
            trimmedTextLet.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}".toRegex()) ->
                FORMAT_US_DATE_TIME_MINUTES_SECONDS.toSimpleDateFormat()
            trimmedTextLet.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}\\w?".toRegex()) ||
                trimmedTextLet.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3}".toRegex()) ||
                trimmedTextLet.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\\w?".toRegex()) ->
                FORMAT_US_DATE_COMPLETE2.toSimpleDateFormat()
            else -> throw IllegalArgumentException("Date '$this' didn't match any patterns.")
        }
        if (this@toCalendarOrNull != null) {
            return Calendar.getInstance().apply {
                dateFormat.parse(this@toCalendarOrNull)?.let { time = it }
            }
        }
    }
    return null
}

fun String.toCalendar(dateFormat: String): Calendar? {
    val calendar = Calendar.getInstance()
    calendar.time = toDate(dateFormat)
    return calendar
}

fun Calendar.toPattern(pattern: String): String {
    val formatter = SimpleDateFormat(pattern, localeDefault)
    return formatter.format(this.time)
}

fun Calendar.toPatternOrNull(pattern: String?): String? {
    val formatter = SimpleDateFormat(pattern, localeDefault)
    return try {
        formatter.format(this.time)
    } catch (e: ParseException) {
        Timber.e(e)
        null
    }
}

fun String.toDate(dateFormat: String): Date {
    val locale = Locale(LOCALE_PT, LOCALE_COUNTRY_BR)
    val simpleDateFormat = SimpleDateFormat(dateFormat, locale)
    return checkNotNull(simpleDateFormat.parse(this))
}

fun String.toDateOrNull(dateFormat: String): Date? {
    val locale = Locale(LOCALE_PT, LOCALE_COUNTRY_BR)
    val simpleDateFormat = SimpleDateFormat(dateFormat, locale)
    return try {
        simpleDateFormat.parse(this)
    } catch (e: ParseException) {
        Timber.e(e)
        null
    }
}

fun String.isOlderThanEighteen(): Boolean {
    val msDiff = Calendar.getInstance().timeInMillis - toCalendar().timeInMillis
    val daysDiff = TimeUnit.MILLISECONDS.toDays(msDiff)

    return OLD_18_YEAR_DAYS <= daysDiff
}