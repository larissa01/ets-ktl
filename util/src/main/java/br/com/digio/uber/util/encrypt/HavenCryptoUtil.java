package br.com.digio.uber.util.encrypt;

import android.os.Build;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import timber.log.Timber;

public class HavenCryptoUtil {

    public enum HASH_RETURN {
        DATA,
        HEX_STRING,
        CERT_FINGERPRINT,
        BAS64,
    }

    private static byte[] simpleCrypt(byte[] data, Cipher cipher) throws IllegalBlockSizeException, BadPaddingException {
        return cipher.doFinal(data);
    }

    //==============================================================================================
    // AES256 Static Values
    //==============================================================================================
    private static int pswdIterations = Integer.parseInt("1035");
    private static int keySize = Integer.parseInt("256");

    //==============================================================================================
    // AES Chiper Methods
    //==============================================================================================
    private static SecretKeySpec pbkdfInterate(String password, byte[] salt, int keySize, int iterations) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt,
                iterations, keySize);
        SecretKey secretKey = factory.generateSecret(spec);
        return new SecretKeySpec(secretKey.getEncoded(), "AES");
    }

    //==============================================================================================
    // AES Crypt Methods
    //==============================================================================================

    public static String encrypt(byte[] data, byte[] iv, String password, byte[] salt) throws IllegalBlockSizeException, BadPaddingException {
        return encrypt(data, iv, password, salt, keySize, pswdIterations, "AES/CBC/PKCS5Padding");
    }

    public static String encrypt(String plainText, String iv, String password, String salt) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {

        byte[] data = plainText.getBytes("utf-8");
        byte[] ivBytes = iv.getBytes("utf-8");
        byte[] saltBytes = salt.getBytes("utf-8");

        return encrypt(data, ivBytes, password, saltBytes);
    }

    public static String decrypt(byte[] base64Data, byte[] ivBytes, String password, byte[] salt) throws IllegalBlockSizeException, BadPaddingException {
        return decrypt(new String(base64Data), ivBytes, password, salt, keySize, pswdIterations,
                "AES/CBC/PKCS5Padding");
    }

    public static String decrypt(String base64Text, String iv, String password, String salt) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {

        byte[] dataBytes = base64Text.getBytes("utf-8");
        byte[] ivBytes = iv.getBytes("utf-8");
        byte[] saltBytes = salt.getBytes("utf-8");

        return decrypt(dataBytes, ivBytes, password, saltBytes);
    }

    //==============================================================================================
    // Public Methods
    //==============================================================================================

    private static Cipher getCipherWithAlgorithm(
            String algorithm,
            String password, byte[] salt,
            int keySize, int iterations,
            byte[] iv,
            int mode
    ) throws NoSuchAlgorithmException,
            InvalidKeySpecException,
            NoSuchPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException {
        byte[] saltBytes = Arrays.copyOfRange(salt, 0, 8);
        byte[] ivBytes = Arrays.copyOfRange(iv, 0, 16);

        SecretKeySpec keySpec = pbkdfInterate(password, saltBytes, keySize, iterations);
        IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
        Cipher cipher;
        cipher = Cipher.getInstance(algorithm);
        cipher.init(mode, keySpec, ivSpec);
        return cipher;
    }

    public static String encrypt(byte[] data, byte[] iv, String password, byte[] salt, int keySize, int iterations, String algorithm) throws IllegalBlockSizeException, BadPaddingException {
        String final_response = "";
        try {
            int mode = Cipher.ENCRYPT_MODE;
            Cipher cipher = getCipherWithAlgorithm(algorithm, password, salt, keySize, iterations, iv, mode);
            byte[] encrypted = simpleCrypt(data, cipher);
            if (Build.VERSION.SDK_INT > 0 && Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                final_response = HavenEncoding.base64Encode(encrypted);
            } else {
                final_response = HavenEncoding.base64EncodeApi26(encrypted);
            }
        } catch (NoSuchAlgorithmException ex) {
            Timber.e(ex);
        } catch (InvalidKeySpecException ex) {
            Timber.e(ex);
        } catch (NoSuchPaddingException ex) {
            Timber.e(ex);
        } catch (InvalidKeyException ex) {
            Timber.e(ex);
        } catch (InvalidAlgorithmParameterException ex) {
            Timber.e(ex);
        }
        return final_response;
    }

    public static String encrypt(String plainText, String iv, String password, String salt, int keySize, int iterations, String algorithm) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
        byte[] data = plainText.getBytes("utf-8");
        byte[] ivBytes = iv.getBytes("utf-8");
        byte[] saltBytes = salt.getBytes("utf-8");

        return encrypt(data, ivBytes, password, saltBytes, keySize, iterations, algorithm);
    }

    public static String decrypt(String base64Data, byte[] ivBytes, String password, byte[] salt, int keySize, int iterations, String algorithm) throws IllegalBlockSizeException, BadPaddingException {
        String final_response = "";
        try {
            byte[] data;
            if (Build.VERSION.SDK_INT > 0 && Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                data = HavenEncoding.base64Decode(base64Data);
            } else {
                data = HavenEncoding.base64DecodeApi26(base64Data);
            }
            byte[] iv = Arrays.copyOfRange(ivBytes, 0, 16);

            int mode = Cipher.DECRYPT_MODE;
            Cipher cipher = getCipherWithAlgorithm(algorithm,
                    password, salt, keySize, iterations,
                    iv, mode);

            final_response = new String(simpleCrypt(data, cipher));
        } catch (NoSuchAlgorithmException ex) {
            Timber.e(ex);
        } catch (InvalidKeySpecException ex) {
            Timber.e(ex);
        } catch (NoSuchPaddingException ex) {
            Timber.e(ex);
        } catch (InvalidKeyException ex) {
            Timber.e(ex);
        } catch (InvalidAlgorithmParameterException ex) {
            Timber.e(ex);
        }
        return final_response;
    }

    public static String decrypt(String base64Text, String iv, String password, String salt, int keySize, int iterations, String algorithm) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
        byte[] ivBytes = iv.getBytes("utf-8");
        byte[] saltBytes = salt.getBytes("utf-8");

        return decrypt(base64Text, ivBytes, password, saltBytes, keySize, iterations, algorithm);
    }


    //==============================================================================================
    // Util Methods
    //==============================================================================================
    private static final SecureRandom SECURE_RANDOM = new SecureRandom();

    public static String randomString(int length) {
        return new BigInteger(length * 8, SECURE_RANDOM).toString(32).substring(0, length);
    }

    public static List<Integer> randomList(int length, String salt) {
        List<Integer> random = new ArrayList<>(length);

        for (int i = 0; i < length; i++) {
            random.add(i);
        }
        Collections.shuffle(random, new Random(salt.hashCode()));

        return random;
    }

}
