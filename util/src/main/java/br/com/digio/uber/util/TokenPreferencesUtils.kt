package br.com.digio.uber.util

import android.content.Context

class TokenPreferencesUtils private constructor(val context: Context) {

    private fun <T> setSharedValue(key: String, value: T) {
        context.setSharedValue(key, value)
    }

    private inline fun <reified T : Any> getSharedValue(key: String): T? =
        context.getSharedValue(key)

    fun setToken(token: String) {
        setSharedValue(JWT_TOKEN, token)
    }

    fun setRefreshToken(token: String) {
        setSharedValue(JWT_REFRESH_TOKEN, token)
    }

    fun setTimestampToken(timestamp: Long) {
        setSharedValue(JWT_TOKEN_TIMESTAMP, timestamp)
    }

    fun getToken(): String? =
        getSharedValue(JWT_TOKEN)

    fun getRefreshToken(): String? =
        getSharedValue(JWT_REFRESH_TOKEN)

    fun getTimestampToken(): Long? =
        getSharedValue(JWT_TOKEN_TIMESTAMP)

    fun clear(key: String) {
        context.clearPreference(key)
    }

    fun clearAll() {
        context.clearAllPreference()
    }

    fun clearToken() {
        clear(JWT_TOKEN)
    }

    companion object : SingletonHolder<TokenPreferencesUtils, Context>(::TokenPreferencesUtils) {
        const val JWT_TOKEN = "jwt_token"
        const val JWT_REFRESH_TOKEN = "jwt_refresh_token"
        const val JWT_TOKEN_TIMESTAMP = "jwt_token_timestamp"
    }
}