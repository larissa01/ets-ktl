package br.com.digio.uber.util

enum class BuildTypes(val value: String) {
    HOMOL("homol"),
    DEBUG("debug"),
    RELEASE("release")
}