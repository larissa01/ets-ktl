package br.com.digio.uber.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build

object NetworkUtil {

    private const val ConnectionTypeWifi = "WIFI"
    private const val ConnectionTypePhone = "DadosMoveis"

    fun getConnectionType(context: Context): String {
        val connManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager?
        var connectionType = ""

        connManager?.let { cm ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val capabilities: NetworkCapabilities? = cm.getNetworkCapabilities(connManager.activeNetwork)
                if (capabilities != null) {
                    when {
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                            connectionType = ConnectionTypePhone
                        }
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                            connectionType = ConnectionTypeWifi
                        }
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                            connectionType = ""
                        }
                    }
                }
            } else {
                val activeNetworkInfo: NetworkInfo? = connManager.activeNetworkInfo
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                    when (activeNetworkInfo.type) {
                        ConnectivityManager.TYPE_MOBILE -> {
                            connectionType = ConnectionTypePhone
                        }
                        ConnectivityManager.TYPE_WIFI -> {
                            connectionType = ConnectionTypeWifi
                        }
                    }
                }
            }
        }

        return connectionType
    }
}