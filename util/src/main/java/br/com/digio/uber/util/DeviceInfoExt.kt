package br.com.digio.uber.util

import android.content.Context
import android.location.Location
import android.os.Build
import br.com.digio.uber.model.request.DeviceInfo

fun Context.getDeviceInfo(deviceToken: String, deviceId: String): DeviceInfo {
    val location: Location? = GPSUtil.getInstance(this).bestLastKnownLocation
    return DeviceInfo(
        appVersion = getVersionName(),
        brand = Build.MANUFACTURER,
        deviceToken = deviceToken,
        deviceModel = Build.MODEL,
        latitude = location?.latitude ?: 0.0,
        longitude = location?.longitude ?: 0.0,
        osName = "Android",
        osVersion = Build.VERSION.RELEASE,
        root = RootUtil.isDeviceRooted(),
        deviceId = deviceId
    )
}