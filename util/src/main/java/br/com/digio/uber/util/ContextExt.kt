package br.com.digio.uber.util

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.browser.customtabs.CustomTabsIntent
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import java.net.InetAddress
import java.net.UnknownHostException

@SuppressLint("HardwareIds")
fun Context.getAndroidId(): String {
    val contentResolver: ContentResolver = contentResolver
    return Settings.Secure.getString(
        contentResolver,
        Settings.Secure.ANDROID_ID
    )
}

fun Context.getProviderPackage(): String =
    packageName + Const.Extras.DOT_PROVIDER

fun Context.getVersionName(): String =
    packageManager.getPackageInfo(packageName, 0).versionName

fun Context.shareReceiptIntent(uri: Uri, title: String, imageMimeType: String = "image/*") {
    Intent(Intent.ACTION_SEND).apply {
        putExtra(Intent.EXTRA_STREAM, uri)
        addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        type = imageMimeType
    }.run {
        startActivity(
            Intent.createChooser(
                this,
                title
            )
        )
    }
}

suspend fun isNetworkConnected(): Boolean =
    try {
        withContext(IO) {
            val address = InetAddress.getByName("www.google.com")
            address.hostName.isNotEmpty()
        }
    } catch (e: UnknownHostException) {
        false
    }

fun Context.openExternalUrl(url: String) {
    val customTabsIntent = CustomTabsIntent.Builder()
        .setShowTitle(true)
        .build()

    customTabsIntent.launchUrl(this, Uri.parse(url))
}