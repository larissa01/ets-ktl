package br.com.digio.uber.util.enums

enum class EducationFilterType(val value: String) {
    INCOMPLETE_ELEMENTARY_SCHOOL("Fundamental - Incompleto"),
    COMPLETE_ELEMENTARY_SCHOOL("Fundamental - Completo"),
    INCOMPLETE_HIGH_SCHOOL("Médio - Incompleto"),
    COMPLETE_HIGH_SCHOOL("Médio - Completo"),
    INCOMPLETE_UNIVERSITY_EDUCATION("Superior - Incompleto"),
    COMPLETE_UNIVERSITY_EDUCATION("Superior - Completo"),
    INCOMPLETE_TECHNICAL_EDUCATION("Técnico - Incompleto"),
    COMPLETE_TECHNICAL_EDUCATION("Técnico - Completo"),
    INCOMPLETE_POSTGRADUATE("Pós-graduação - Incompleto"),
    COMPLETE_POSTGRADUATE("Pós-graduação - Completo");

    companion object {
        fun fromString(value: String?) =
            values().firstOrNull { it.name == value }

        fun fromStringValue(value: String) =
            values().firstOrNull { it.value == value }
    }
}