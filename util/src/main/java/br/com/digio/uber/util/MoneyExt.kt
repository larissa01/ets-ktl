package br.com.digio.uber.util

import timber.log.Timber
import java.lang.NumberFormatException
import java.text.DecimalFormat

fun Float.toMoney(withDecimals: Boolean = false): String {
    val formatter = DecimalFormat.getInstance() as DecimalFormat
    val symbol = "R\$"
    formatter.minimumFractionDigits = if (withDecimals) 2 else 0
    formatter.maximumFractionDigits = if (withDecimals) 2 else 0
    formatter.decimalFormatSymbols = formatter.decimalFormatSymbols.apply {
        groupingSeparator = '.'
        decimalSeparator = ','
    }
    formatter.positivePrefix = "$symbol "
    formatter.negativePrefix = "$symbol -"
    return formatter.format(this)
}

fun String.moneyToFloat(withDecimals: Boolean = false): Float {
    val formatter = DecimalFormat.getInstance() as DecimalFormat
    val symbol = "R$"
    formatter.minimumFractionDigits = if (withDecimals) 2 else 0
    formatter.maximumFractionDigits = if (withDecimals) 2 else 0
    formatter.decimalFormatSymbols = formatter.decimalFormatSymbols.apply {
        groupingSeparator = '.'
        decimalSeparator = ','
    }
    formatter.positivePrefix = symbol
    formatter.negativePrefix = "$symbol-"
    return formatter.parse(
        (
            if (!contains(symbol)) "$symbol $this"
            else if (replace(symbol, "").trim().isEmpty()) plus("0")
            else this
            ).replace("\\s+".toRegex(), "")
            .replace("\u00A0", "")
    )?.toFloat() ?: 0f
}

fun String.moneyToFloatOrNull(withDecimals: Boolean = false): Float? =
    try {
        moneyToFloat(withDecimals)
    } catch (ex: NumberFormatException) {
        Timber.e(ex)
        null
    }