package br.com.digio.uber.util

import android.graphics.Bitmap
import android.graphics.Matrix
import io.fotoapparat.result.BitmapPhoto

fun BitmapPhoto.rotate(rotationDegrees: Int): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(rotationDegrees.toFloat())
    val scaledBitmap = Bitmap.createScaledBitmap(this.bitmap, this.bitmap.width, this.bitmap.height, true)
    return Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
}

fun Bitmap.rotateMirror(rotationDegrees: Float): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(rotationDegrees)
    matrix.preScale((-1).toFloat(), (1).toFloat())
    val scaledBitmap = Bitmap.createScaledBitmap(this, width, height, true)
    return Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
}

fun Bitmap.rotate(rotationDegrees: Float): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(rotationDegrees)
    return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
}

fun BitmapPhoto.rotateMirror(rotationDegrees: Int): Bitmap {
    val matrix = Matrix()
    matrix.postRotate(rotationDegrees.toFloat())
    matrix.preScale((-1).toFloat(), (1).toFloat())
    val scaledBitmap = Bitmap.createScaledBitmap(this.bitmap, this.bitmap.width, this.bitmap.height, true)
    return Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
}