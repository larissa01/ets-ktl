plugins {
    id("com.android.application")
    kotlin("android")
    id("kotlin-android-extensions")
    id("com.google.firebase.crashlytics")
    id("dexguard")
    id("br.com.digio.uber.plugin.android")
}

configureAndroid(
    moreConfigure = {
        packagingOptions {
            exclude("META-INF/DEPENDENCIES")
            exclude("META-INF/LICENSE")
            exclude("META-INF/LICENSE.txt")
            exclude("META-INF/license.txt")
            exclude("META-INF/NOTICE")
            exclude("META-INF/NOTICE.txt")
            exclude("META-INF/notice.txt")
            exclude("META-INF/AL2.0")
            exclude("META-INF/ASL2.0")
            exclude("META-INF/LGPL2.1")
            exclude("META-INF/*.kotlin_module")
        }

        dynamicFeatures = mutableSetOf(
            ":splash",
            ":sample",
            ":transfer",
            ":bankSlip",
            ":featureNavigation",
            ":guideCustoms",
            ":login",
            ":main",
            ":virtualcard",
            ":recharge",
            ":choice",
            ":ChangePassword",
            ":payment",
            ":qrcode",
            ":veloe",
            ":statement",
            ":pid",
            ":income",
            ":ShowCardPassword",
            ":cashback",
            ":withdraw",
            ":tracking",
            ":ChangeRegistration",
            ":cardReceived",
            ":cardreissue"
        )
    },
    releaseBuildTypeConfig = {
        firebaseCrashlytics {
            mappingFileUploadEnabled = true
            mappingFile = "${project.buildDir}/outputs/dexguard/mapping/apk/release/mapping.txt"
        }
    },
    homolBuildTypeConfig = {
        firebaseCrashlytics {
            mappingFileUploadEnabled = true
            mappingFile = "${project.buildDir}/outputs/dexguard/mapping/apk/homol/mapping.txt"
        }
    }
)

dependencies {

    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.ANDROIDXCORE)
    implementation(Dependencies.ACTIVITYKTX)
    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.FRAGMENTKTX)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.KOIN)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.ANNOTATION)
    implementation(Dependencies.KOINEXT)
    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.LIFECYCLE_EXTENSIONS)
    implementation(Dependencies.LIFECYCLE_VIEWMODEL)
    implementation(platform(Dependencies.FIREBASE_BOM))
    implementation(Dependencies.FIREBASE_CRASHLYTICS)
    implementation(Dependencies.FIREBASE_ANALYTICS)
    implementation(Dependencies.HAWK)
    implementation(Dependencies.NAV_FRAGMENT)
    implementation(Dependencies.NAV_UI)
    implementation(Dependencies.NAV_DYNAMIC_FEATURE)
    implementation(Dependencies.JNA)

    testImplementation(TestDependencies.TESTRULES)
    testImplementation(TestDependencies.TESTRUNNER)
    testImplementation(TestDependencies.MOCKITOCORE)
    testImplementation(TestDependencies.MOCKITOINLINE)
    testImplementation(TestDependencies.MOCKITOKOTLIN)
    testImplementation(TestDependencies.COROUTINESTEST)

    androidTestImplementation(TestDependencies.JUNIT)

    implementation(Dependencies.KOIN)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)
    implementation(Dependencies.CIRCLE_IMAGE_VIEW)

    // MODULES
    implementation(project(":di"))
    implementation(project(":gemalto"))
    implementation(project(":util"))
    implementation(project(":pix"))
    implementation(project(":marketingCloud"))
}

dexguard {
    version = "9.0.1"
    license = "./dexguard-license.txt"
    configurations {
        val buildTypeChoose: String by project
        when (buildTypeChoose) {
            "uberRelease" -> register("uberRelease") {
                defaultConfigurations(
                    "dexguard-release-aggressive.pro",
                    "dexguard-release-conservative.pro",
                    "dexguard-androidx.pro",
                    "dexguard-compatibility-samsung-store.pro",
                    "dexguard-compatibility-android-9.pro"
                )
                configuration("dexguard-project.pro")
            }
            "uberHomol" -> register("uberHomol") {
                defaultConfigurations(
                    "dexguard-release-aggressive.pro",
                    "dexguard-release-conservative.pro",
                    "dexguard-androidx.pro",
                    "dexguard-compatibility-samsung-store.pro",
                    "dexguard-compatibility-android-9.pro"
                )
                configuration("dexguard-project.pro")
            }
        }
    }
}