########################################################################################################################
################################################### CONFIG SESSION #####################################################
########################################################################################################################
#-pack

# OATH SDK
-dalvik
-android

# Ezio native libs
-keepresourcefiles lib/**/libjnidispatch.so
-keepresourcefiles lib/**/libmedl.so
-keepresourcefiles lib/**/libidp-shared.so
# for faceID users
-keepresourcefiles lib/**/libopenblas.so
-keepresourcefiles lib/**/libfid.so
-keepresourcefiles assets/data/**.ndf

-verbose
#-dontshrink
-dontoptimize
#-dontobfuscate
#-dontpreverify
#-repackageclasses ''
-zipalign 4
-dontcompress resources.arsc,**.jpg,**.jpeg,**.png,**.gif,**.wav
-allowaccessmodification
-keepresourcefiles AndroidManifest.xml
-adaptresourcefilecontents AndroidManifest.xml,resources.arsc,!res/raw**,res/**.xml
-keepresourcexmlattributenames manifest/installLocation,manifest/versionCode,manifest/application/*/intent-filter/*/name
-renamesourcefileattribute ''
-keepattributes SourceFile,LineNumberTable
-keepattributes *Annotation*
## Allow 65K methods +. It support to processor to how split the dex' files
-multidex

-assumenosideeffects class android.util.Log {
     public static boolean isLoggable(java.lang.String, int);
     public static int v(...);
     public static int i(...);
     public static int w(...);
     public static int d(...);
     public static int e(...);
}

-keepresourcexmlelements manifest/application/meta-data@name=io.fabric.ApiKey

########################################################################################################################
################################################## DONT WARN SESSION ###################################################
########################################################################################################################
-dontwarn com.squareup.okhttp.**
-dontwarn okio.**
-dontwarn net.bozho.**
-dontwarn com.crashlytics.**
-dontwarn okhttp3.**
-dontwarn org.apache.http.
-dontwarn retrofit2.Platform$Java8
-dontwarn javax.**
-dontwarn br.com.makrosystems.**
-dontwarn android.support.**
-dontwarn androidx.**
-dontwarn com.testfairy.**
-dontwarn kotlinx.coroutines.**
-dontwarn com.gemalto.**
-dontwarn util.**
########################################################################################################################
################################################## DONT NOTE SESSION ###################################################
########################################################################################################################
-dontnote retrofit2.Platform
-dontnote com.android.vending.licensing.ILicensingService
########################################################################################################################
################################################## KEEP CLASS SESSION ##################################################
########################################################################################################################
-keep public class * extends java.lang.Exception
-keepattributes Signature
-keep class com.google.android.material.** { *; }
-keep class androidx.** { *; }
-keep interface androidx.** { *; }

-keep,includedescriptorclasses class com.crashlytics.** { *; }
-keep,includedescriptorclasses class retrofit2.** { *; }
-keep,includedescriptorclasses class org.apache.http.* { *; }
-keep,includedescriptorclasses class com.helpshift.** { *; }
-keep,includedescriptorclasses class io.reactivex.** { *; }
-keep,includedescriptorclasses class io.fabric.** { *; }
-keep,includedescriptorclasses class com.firebase.** { *; }
-keep public interface com.android.vending.licensing.ILicensingService

-keepclasseswithmembernames, includedescriptorclasses class * {
     native <methods>;
 }

-keepnames class kotlinx.coroutines.internal.MainDispatcherFactory {}
-keepnames class kotlinx.coroutines.CoroutineExceptionHandler {}
-keepnames class kotlinx.coroutines.android.AndroidExceptionPreHandler {}
-keepnames class kotlinx.coroutines.android.AndroidDispatcherFactory {}

-keep class br.com.digio.uber.veloe.ui.activity.VeloeActivity
-keep class br.com.digio.uber.cashback.ui.activity.CashbackActivity
-keep class br.com.digio.uber.recharge.ui.activity.RechargeActivity
-keep class br.com.digio.uber.qrcode.activity.QRCodeActivity
-keep class br.com.digio.uber.virtualcard.ui.activity.VirtualCardActivity
-keep class br.com.digio.uber.util.Const



-keepclassmembers class * extends android.view.View {
    public void set*(...);
}
-keepclassmembers class * extends android.content.Context {
   public void *(android.view.View);
   public void *(android.view.MenuItem);
}
-keepclassmembers class * implements android.os.Parcelable {
    static ** CREATOR;
}
-keepclassmembers class **.R$* {
    public static <fields>;
}
-keep class **.R$*

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}
-keepclassmembers class * implements javax.net.ssl.SSLSocketFactory {
    private javax.net.ssl.SSLSocketFactory delegate;
}
-keepclassmembernames class kotlinx.** {
    volatile <fields>;
}
-keepclassmembers class * {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}
-keepclassmembers, allowshrinking enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}
-keepclassmembers class * extends java.lang.Enum {
    <fields>;
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# Required for okhttp 3 library
-keep class okio.** { *; }
-keep class okhttp3.** { *; }
-dontwarn javax.annotation.ParametersAreNonnullByDefault

#Required for ThreatMetrix SDK
-keep,includecode class com.threatmetrix.TrustDefender.** { *; }
-keep,includecode class com.threatmetrix.TrustDefender.internal.** { *; }
-keepresourcefiles lib/**.so

#Suppress warning messages about ThreatMetrix SDK (this rule is optional when using DexGuard)
-dontwarn com.threatmetrix.TrustDefender.**

#Required to suppress warning messages about annotations
-dontwarn android.support.annotation.NonNull
-dontwarn android.support.annotation.Nullable
-dontwarn android.support.annotation.RequiresApi

#Required if optimisation is enabled
#-optimizations !code/removal/*,!method/*

# below is open models because dex crash. Later is necessary remove and fix

# below is the cinnecta sdk
-keep class com.cinnecta.** { *; }
-keep interface com.cinnecta.** { *; }

-keep public class * extends android.app.Activity
-keepattributes *Annotation*

# inmetrics
-keep class com.testfairy.** { *; }
-keepattributes Exceptions, Signature, LineNumberTable

# Gemalto
# Ezio native libs
-keepresourcefiles app/libs/gemalto/**
-keepresourcefiles libs/gemalto/**
-keepresources libs/gemalto/**
-keepresources app/libs/gemalto/**
-keep class com.gemalto.**{ *; }
-keep class util.a.**{ *;}
-keep interface com.gemalto.** { *; }
-keep interface util.a.**{ *; }

# Facebook Shimmer
-keep class com.facebook.crypto.** { *; }

# Encrypt strings

-encryptstrings class br.com.digio.uber.util.encrypt.** { *; }
-encryptstrings class br.com.digio.uber.gateway.** { *; }
-encryptstrings class br.com.digio.pix.gateway.security.** { *; }
-encryptstrings class br.com.digio.pix.gateway.interceptor.** { *; }

-keepresourcefiles **/raw/**
-keepresourcefiles pix/src/uber/res/values/strings.xml
-keepresourcefiles pix/src/main/res/values/strings.xml