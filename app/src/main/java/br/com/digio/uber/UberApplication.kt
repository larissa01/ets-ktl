package br.com.digio.uber

import android.app.Application
import br.com.digio.uber.di.uberModules
import br.com.digio.uber.gemalto.GemaltoInitConfiguration
import br.com.digio.uber.marketingCloud.config.MarketingCloudConfiguration
import br.com.digio.uber.tree.CrashReportingTree
import br.com.digio.uber.util.encrypt.HawkEncryptGsonParser
import com.orhanobut.hawk.Hawk
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class UberApplication : Application() {

    private val gemaltoInitConfiguration: GemaltoInitConfiguration by inject()
    private val marketingCloud: MarketingCloudConfiguration by inject()

    override fun onCreate() {
        super.onCreate()

        buildLibraries()
    }

    private fun buildLibraries() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }

        startKoin()
        setupLibs()
    }

    private fun setupLibs() {
        gemaltoInitConfiguration.initConfiguration()
        Hawk.init(applicationContext)
            .setParser(HawkEncryptGsonParser())
            .build()
        marketingCloud.initConfiguration(this)
    }

    private fun startKoin() {
        startKoin {
            androidLogger()
            androidContext(this@UberApplication)
            androidFileProperties()
            koin.loadModules(uberModules)
        }
    }
}