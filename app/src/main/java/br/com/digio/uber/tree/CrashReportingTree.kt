package br.com.digio.uber.tree

import android.util.Log
import androidx.annotation.NonNull
import com.google.firebase.crashlytics.FirebaseCrashlytics
import timber.log.Timber

internal class CrashReportingTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, @NonNull message: String, t: Throwable?) {
        if (
            priority == Log.VERBOSE ||
            priority == Log.DEBUG ||
            try {
                FirebaseCrashlytics.getInstance()
            } catch (e: ExceptionInInitializerError) {
                null
            } == null
        ) {
            return
        }
        FirebaseCrashlytics.getInstance().log(message)
        if (t != null) {
            FirebaseCrashlytics.getInstance().recordException(t)
        }
    }
}