package br.com.digio.uber.digiotest.mapper

import br.com.digio.uber.digiotest.MockDigioTest
import br.com.digio.uber.digiotest.MockDigioTest.mockProductsListUiModelSuccess
import io.mockk.spyk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.equalToIgnoringCase
import org.junit.Assert
import org.junit.Test

class ProductListMapperTest {

    private val mapper = spyk<ProductListMapper>()

    @Test
    @ExperimentalCoroutinesApi
    fun `on execute mapper giving ProductsList and return ProductListUiModel`() {
        val expectedResponse = mockProductsListUiModelSuccess()
        val productsList = MockDigioTest.mockProductsListSuccess()
        val actualResponse = mapper.map(productsList)

        Assert.assertEquals(expectedResponse, actualResponse)
        verify { mapper.map(productsList) }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `on ProductsListUiModel return check integrity`() {
        val expectedResponse = mockProductsListUiModelSuccess()
        val productsList = MockDigioTest.mockProductsListSuccess()
        val actualResponse = mapper.map(productsList)

        assertThat(expectedResponse.cash, `is`(equalTo(actualResponse.cash)))
        assert(expectedResponse.products == actualResponse.products)
        assert(expectedResponse.spotlight == actualResponse.spotlight)
        assertThat(expectedResponse.cash.title, equalToIgnoringCase(actualResponse.cash.title))
        assert(expectedResponse.products[0].imageURL == actualResponse.products[0].imageURL)
        assert(expectedResponse.spotlight[1].description == actualResponse.spotlight[1].description)

        Assert.assertThrows(ArrayIndexOutOfBoundsException::class.java) { expectedResponse.spotlight[33].name }

        verify(atLeast = 1) { mapper.map(productsList) }
    }
}