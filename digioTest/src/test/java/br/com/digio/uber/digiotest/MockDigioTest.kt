package br.com.digio.uber.digiotest

import br.com.digio.uber.digiotest.uiModel.CashUiModel
import br.com.digio.uber.digiotest.uiModel.ProductListUiModel
import br.com.digio.uber.digiotest.uiModel.ProductUiModel
import br.com.digio.uber.digiotest.uiModel.SpotlightUiModel
import br.com.digio.uber.model.digioTests.Cash
import br.com.digio.uber.model.digioTests.Product
import br.com.digio.uber.model.digioTests.ProductsList
import br.com.digio.uber.model.digioTests.Spotlight

object MockDigioTest {

    fun mockProductsListSuccess(): ProductsList =
        ProductsList(
            Cash(
                "https://s3-sa-east-1.amazonaws.com/digio-exame/cash_banner.png",
                "Dinheiro na conta sem complicação. Transfira parte do limite do seu cartão para sua conta.",
                "digio Cash"
            ),
            listOf(
                Product(
                    "Com o e-Gift Card Xbox você adquire créditos para comprar games, música, filmes, " +
                        "programas de TV e muito mais!",
                    "https://s3-sa-east-1.amazonaws.com/digio-exame/xbox_icon.png",
                    "XBOX"
                ),
                Product(
                    "Com o e-Gift Card Xbox você adquire créditos para comprar games, música, filmes, " +
                        "programas de TV e muito mais!",
                    "https://s3-sa-east-1.amazonaws.com/digio-exame/xbox_icon.png",
                    "XBOX"
                ),
                Product(
                    "Com o e-Gift Card Xbox você adquire créditos para comprar games, música, filmes, " +
                        "programas de TV e muito mais!",
                    "",
                    "XBOX"
                )
            ),
            listOf(
                Spotlight(
                    "https://s3-sa-east-1.amazonaws.com/digio-exame/recharge_banner.png",
                    "Agora ficou mais fácil colocar créditos no seu celular! A digio Store traz a " +
                        "facilidade de fazer recargas... direto pelo s",
                    "Recarga"
                ),
                Spotlight(
                    "https://s3-sa-east-1.amazonaws.com/digio-exame/recharge_banner.png",
                    "Agora ficou mais fácil colocar créditos no seu celular! A digio Store traz a " +
                        "facilidade de fazer recargas... direto pelo s",
                    "Recarga"
                )
            )
        )

    fun mockProductsListUiModelSuccess(): ProductListUiModel =
        ProductListUiModel(
            CashUiModel(
                "https://s3-sa-east-1.amazonaws.com/digio-exame/cash_banner.png",
                "Dinheiro na conta sem complicação. Transfira parte do limite do seu cartão para sua conta.",
                "digio Cash"
            ),
            listOf(
                ProductUiModel(
                    "Com o e-Gift Card Xbox você adquire créditos para comprar games, música, filmes, " +
                        "programas de TV e muito mais!",
                    "https://s3-sa-east-1.amazonaws.com/digio-exame/xbox_icon.png",
                    "XBOX"
                ),
                ProductUiModel(
                    "Com o e-Gift Card Xbox você adquire créditos para comprar games, música, filmes, " +
                        "programas de TV e muito mais!",
                    "https://s3-sa-east-1.amazonaws.com/digio-exame/xbox_icon.png",
                    "XBOX"
                ),
                ProductUiModel(
                    "Com o e-Gift Card Xbox você adquire créditos para comprar games, música, filmes, " +
                        "programas de TV e muito mais!",
                    "",
                    "XBOX"
                )
            ),
            listOf(
                SpotlightUiModel(
                    "https://s3-sa-east-1.amazonaws.com/digio-exame/recharge_banner.png",
                    "Agora ficou mais fácil colocar créditos no seu celular! A digio Store traz a " +
                        "facilidade de fazer recargas... direto pelo s",
                    "Recarga"
                ),
                SpotlightUiModel(
                    "https://s3-sa-east-1.amazonaws.com/digio-exame/recharge_banner.png",
                    "Agora ficou mais fácil colocar créditos no seu celular! A digio Store traz a " +
                        "facilidade de fazer recargas... direto pelo s",
                    "Recarga"
                )
            )
        )
}