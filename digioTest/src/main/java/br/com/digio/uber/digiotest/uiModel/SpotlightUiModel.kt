package br.com.digio.uber.digiotest.uiModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SpotlightUiModel(
    val bannerURL: String,
    val description: String,
    val name: String
) : Parcelable