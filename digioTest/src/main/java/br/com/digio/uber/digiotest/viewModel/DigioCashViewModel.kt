package br.com.digio.uber.digiotest.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.digio.uber.digiotest.uiModel.CashUiModel

class DigioCashViewModel : ViewModel() {

    val cash = MutableLiveData<CashUiModel>()
}