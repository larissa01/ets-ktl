package br.com.digio.uber.digiotest.uiModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductListUiModel(
    val cash: CashUiModel,
    val products: List<ProductUiModel>,
    val spotlight: List<SpotlightUiModel>
) : Parcelable