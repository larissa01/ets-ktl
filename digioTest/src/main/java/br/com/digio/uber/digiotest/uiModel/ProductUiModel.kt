package br.com.digio.uber.digiotest.uiModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductUiModel(
    val description: String,
    val imageURL: String,
    val name: String
) : Parcelable