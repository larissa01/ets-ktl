package br.com.digio.uber.digiotest.di

import br.com.digio.uber.digiotest.viewModel.DigioCashViewModel
import br.com.digio.uber.digiotest.viewModel.DigioTestActivityViewModel
import br.com.digio.uber.digiotest.viewModel.ProductsViewModel
import br.com.digio.uber.digiotest.viewModel.SpotlightsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val digioTestViewModelModule = module {
    viewModel { DigioTestActivityViewModel(get()) }
    viewModel { SpotlightsViewModel() }
    viewModel { DigioCashViewModel() }
    viewModel { ProductsViewModel() }
}