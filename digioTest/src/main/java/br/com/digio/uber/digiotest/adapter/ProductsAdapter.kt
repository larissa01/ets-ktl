package br.com.digio.uber.digiotest.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.extensions.loadImage
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.digiotest.R
import br.com.digio.uber.digiotest.databinding.ItemProductBinding
import br.com.digio.uber.digiotest.uiModel.ProductUiModel
import br.com.digio.uber.util.safeHeritage

class ProductsAdapter : RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder>(), AdapterItemsContract {
    private var products: List<ProductUiModel> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder {
        val binding = ItemProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductsViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        holder.bind(products[position])
    }

    override fun getItemCount() = products.size

    class ProductsViewHolder(private val binding: ItemProductBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(product: ProductUiModel) {
            binding.imgProduct.loadImage(context, product.imageURL) {
                error(R.drawable.ic_image_error)
            }
        }
    }

    override fun replaceItems(list: List<Any>) {
        products = list.safeHeritage()
        notifyDataSetChanged()
    }
}