package br.com.digio.uber.digiotest.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.com.digio.uber.common.extensions.loadImage
import br.com.digio.uber.digiotest.databinding.FragmentDigioCashBinding
import br.com.digio.uber.digiotest.uiModel.CashUiModel
import br.com.digio.uber.digiotest.viewModel.DigioCashViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class DigioCashFragment : Fragment() {

    private val viewModel: DigioCashViewModel by viewModel()
    lateinit var binding: FragmentDigioCashBinding
    private val cash: CashUiModel by lazy { checkNotNull(arguments?.getParcelable(CASH_KEY)) }

    companion object {
        const val CASH_KEY = "cash"
        fun newInstance(cash: CashUiModel) = DigioCashFragment().apply {
            arguments = Bundle().apply {
                putParcelable(CASH_KEY, cash)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDigioCashBinding.inflate(inflater)
        viewModel.cash.postValue(cash)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.cash.observe(
            viewLifecycleOwner,
            {
                binding.imgCash.loadImage(requireContext(), it.bannerURL)
            }
        )
    }
}