package br.com.digio.uber.digiotest.viewModel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.digiotest.uiModel.SpotlightUiModel

class SpotlightsViewModel : BaseViewModel() {

    val spotlights = MutableLiveData<List<SpotlightUiModel>>()
}