package br.com.digio.uber.digiotest.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.extensions.isLastIndex
import br.com.digio.uber.digiotest.R

class SpaceItemDecoration : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val viewPosition = parent.getChildAdapterPosition(view)
        val marginNormal = view.context.resources.getDimensionPixelSize(R.dimen.dp_24)
        val marginXSmall = view.context.resources.getDimensionPixelSize(R.dimen.dp_12)

        val isFirstIndex = viewPosition == 0
        if (isFirstIndex) {
            outRect.left = marginNormal
        }

        if (parent.isLastIndex(viewPosition)) {
            outRect.right = marginNormal
        } else {
            outRect.right = marginXSmall
        }
    }
}