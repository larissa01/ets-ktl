package br.com.digio.uber.digiotest.viewModel

import androidx.lifecycle.LiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.digiotest.mapper.ProductListMapper
import br.com.digio.uber.digiotest.uiModel.ProductListUiModel
import br.com.digio.uber.domain.usecase.digioTest.DigioTestUseCase

class DigioTestActivityViewModel constructor(
    digioTestUseCase: DigioTestUseCase
) : BaseViewModel() {
    val productsList: LiveData<ProductListUiModel> = digioTestUseCase(Unit).exec().map(ProductListMapper())
}