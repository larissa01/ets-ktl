package br.com.digio.uber.digiotest.mapper

import br.com.digio.uber.digiotest.uiModel.CashUiModel
import br.com.digio.uber.digiotest.uiModel.ProductListUiModel
import br.com.digio.uber.digiotest.uiModel.ProductUiModel
import br.com.digio.uber.digiotest.uiModel.SpotlightUiModel
import br.com.digio.uber.model.digioTests.ProductsList
import br.com.digio.uber.util.mapper.AbstractMapper

class ProductListMapper :
    AbstractMapper<ProductsList, ProductListUiModel> {

    override fun map(param: ProductsList): ProductListUiModel = with(param) {
        val cash = CashUiModel(cash.bannerURL, cash.description, cash.title)
        val productsList = products.map {
            ProductUiModel(it.description, it.imageURL, it.name)
        }
        val spotlightList = spotlight.map {
            SpotlightUiModel(it.bannerURL, it.description, it.name)
        }
        ProductListUiModel(cash, productsList, spotlightList)
    }
}