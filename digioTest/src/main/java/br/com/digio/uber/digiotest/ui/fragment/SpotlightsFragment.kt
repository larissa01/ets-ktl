package br.com.digio.uber.digiotest.ui.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.digiotest.BR
import br.com.digio.uber.digiotest.R
import br.com.digio.uber.digiotest.adapter.SpaceItemDecoration
import br.com.digio.uber.digiotest.adapter.SpotlightsAdapter
import br.com.digio.uber.digiotest.databinding.FragmentSpotlightsBinding
import br.com.digio.uber.digiotest.uiModel.SpotlightUiModel
import br.com.digio.uber.digiotest.viewModel.SpotlightsViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class SpotlightsFragment : BaseViewModelFragment<FragmentSpotlightsBinding, SpotlightsViewModel>() {

    override val viewModel: SpotlightsViewModel by viewModel()
    override fun tag(): String = ProductsFragment::class.simpleName.toString()
    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK
    override val bindingVariable: Int? = BR.viewModel
    override val getLayoutId: Int? = R.layout.fragment_spotlights

    private lateinit var adapter: SpotlightsAdapter

    companion object {
        private const val SPOTLIGHTS_KEY = "spotlights"
        fun newInstance(spotlights: List<SpotlightUiModel>) = SpotlightsFragment().apply {
            arguments = Bundle().apply {
                putParcelableArrayList(SPOTLIGHTS_KEY, ArrayList(spotlights))
            }
        }
    }

    override fun initialize() {
        setupUI()
        bindViewModel()
    }

    private fun setupUI() {
        adapter = SpotlightsAdapter()
        val snapHelper = PagerSnapHelper()
        binding?.apply {
            rvSpotlights.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            rvSpotlights.addItemDecoration(SpaceItemDecoration())
            snapHelper.attachToRecyclerView(rvSpotlights)
            rvSpotlights.adapter = adapter
        }
    }

    private fun bindViewModel() {
        viewModel.spotlights.value = checkNotNull(
            arguments?.getParcelableArrayList(SPOTLIGHTS_KEY)
        )
    }
}