package br.com.digio.uber.digiotest.uiModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CashUiModel(
    val bannerURL: String,
    val description: String,
    val title: String
) : Parcelable