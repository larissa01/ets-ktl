package br.com.digio.uber.digiotest.ui.activity

import android.os.Bundle
import androidx.lifecycle.Observer
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.digiotest.BR
import br.com.digio.uber.digiotest.R
import br.com.digio.uber.digiotest.databinding.ActivityDigioTestBinding
import br.com.digio.uber.digiotest.di.digioTestViewModelModule
import br.com.digio.uber.digiotest.ui.fragment.DigioCashFragment
import br.com.digio.uber.digiotest.ui.fragment.ProductsFragment
import br.com.digio.uber.digiotest.ui.fragment.SpotlightsFragment
import br.com.digio.uber.digiotest.uiModel.CashUiModel
import br.com.digio.uber.digiotest.uiModel.ProductUiModel
import br.com.digio.uber.digiotest.uiModel.SpotlightUiModel
import br.com.digio.uber.digiotest.viewModel.DigioTestActivityViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.module.Module

class DigioTestActivity : BaseViewModelActivity<ActivityDigioTestBinding, DigioTestActivityViewModel>() {

    override val viewModel: DigioTestActivityViewModel by viewModel()
    override fun getLayoutId(): Int = R.layout.activity_digio_test
    override val bindingVariable: Int? = BR.viewModel

    override fun getModule(): List<Module>? = listOf(digioTestViewModelModule)

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)
        viewModel.productsList.observe(
            this,
            Observer {
                fetchSpotlights(it.spotlight)
                fetchCash(it.cash)
                fetchProducts(it.products)
            }
        )
    }

    private fun fetchSpotlights(spotlights: List<SpotlightUiModel>) {
        supportFragmentManager.beginTransaction().add(
            R.id.container_spotlights,
            SpotlightsFragment.newInstance(spotlights)
        ).commit()
    }

    private fun fetchCash(cash: CashUiModel) {
        supportFragmentManager.beginTransaction().add(
            R.id.container_cash,
            DigioCashFragment.newInstance(cash)
        ).commit()
    }

    private fun fetchProducts(productsList: List<ProductUiModel>) {
        supportFragmentManager.beginTransaction().add(
            R.id.container_products,
            ProductsFragment.newInstance(productsList)
        ).commit()
    }
}