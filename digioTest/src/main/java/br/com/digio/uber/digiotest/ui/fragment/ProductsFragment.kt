package br.com.digio.uber.digiotest.ui.fragment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.base.fragment.BaseViewModelFragment
import br.com.digio.uber.common.kenum.StatusBarColor
import br.com.digio.uber.digiotest.BR
import br.com.digio.uber.digiotest.R
import br.com.digio.uber.digiotest.adapter.ProductsAdapter
import br.com.digio.uber.digiotest.databinding.FragmentProductsBinding
import br.com.digio.uber.digiotest.uiModel.ProductUiModel
import br.com.digio.uber.digiotest.viewModel.ProductsViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ProductsFragment : BaseViewModelFragment<FragmentProductsBinding, ProductsViewModel>() {

    override val viewModel: ProductsViewModel by viewModel()
    private lateinit var adapter: ProductsAdapter

    companion object {
        private const val PRODUCTS_KEY = "products"
        fun newInstance(products: List<ProductUiModel>) = ProductsFragment().apply {
            arguments = Bundle().apply {
                putParcelableArrayList(PRODUCTS_KEY, ArrayList(products))
            }
        }
    }

    override fun tag(): String = ProductsFragment::class.simpleName.toString()
    override fun getStatusBarAppearance(): StatusBarColor = StatusBarColor.BLACK
    override val bindingVariable: Int? = BR.viewModel
    override val getLayoutId: Int? = R.layout.fragment_products

    override fun initialize() {
        setupUI()
        bindViewModel()
    }

    private fun setupUI() {
        adapter = ProductsAdapter()
        binding?.apply {
            rvProducts.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            rvProducts.adapter = adapter
        }
    }

    private fun bindViewModel() {
        viewModel.productsList.value = checkNotNull(
            arguments?.getParcelableArrayList(PRODUCTS_KEY)
        )
    }
}