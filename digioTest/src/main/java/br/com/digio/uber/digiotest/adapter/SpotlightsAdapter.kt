package br.com.digio.uber.digiotest.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.uber.common.extensions.loadImage
import br.com.digio.uber.common.listener.AdapterItemsContract
import br.com.digio.uber.digiotest.databinding.ItemSpotlightBinding
import br.com.digio.uber.digiotest.uiModel.SpotlightUiModel
import br.com.digio.uber.util.safeHeritage

class SpotlightsAdapter : RecyclerView.Adapter<SpotlightsAdapter.SpotlightViewHolder>(), AdapterItemsContract {
    private var spotlights: List<SpotlightUiModel> = listOf()

    companion object {
        const val CARD_PERCENT_WIDTH = 0.88
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpotlightViewHolder {
        val binding = ItemSpotlightBinding.inflate(LayoutInflater.from(parent.context), parent, false).apply {
            root.updateLayoutParams<RecyclerView.LayoutParams> {
                width = (parent.measuredWidth * CARD_PERCENT_WIDTH).toInt()
            }
        }
        return SpotlightViewHolder(binding, parent.context)
    }

    override fun onBindViewHolder(holder: SpotlightViewHolder, position: Int) {
        holder.bind(spotlights[position])
    }

    override fun getItemCount() = spotlights.size

    class SpotlightViewHolder(private val binding: ItemSpotlightBinding, private val context: Context) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(spotlight: SpotlightUiModel) {
            binding.imgSpotlight.loadImage(context, spotlight.bannerURL)
        }
    }

    override fun replaceItems(list: List<Any>) {
        spotlights = list.safeHeritage()
        notifyDataSetChanged()
    }
}