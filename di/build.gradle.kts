plugins {
    id("com.android.library")
    kotlin("android")
    id("kotlin-android-extensions")
    id("br.com.digio.uber.plugin.android.library")
}

dependencies {

    implementation(Dependencies.KOIN)
    implementation(Dependencies.KOINSCOPE)
    implementation(Dependencies.KOINVIEWMODEL)
    implementation(Dependencies.KOINEXT)

    // DEPENDENCIES FROM FEATURES
    implementation(Dependencies.COROUTINES)
    implementation(Dependencies.OKHTTP)

    implementation(project(":gateway"))
    implementation(project(":dataset"))
    implementation(project(":db"))
    implementation(project(":analytics"))
    implementation(project(":repository"))
    implementation(project(":domain"))
    implementation(project(":common"))
    implementation(project(":gemalto"))
    implementation(project(":util"))
    implementation(project(":model"))
    implementation(project(":abstractFirebase"))
    implementation(project(":marketingCloud"))
}