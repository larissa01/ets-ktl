package br.com.digio.uber.di.db

import br.com.digio.uber.db.createUberDatabase
import br.com.digio.uber.db.file.FaqFileReader
import br.com.digio.uber.db.file.FaqFileReaderImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dbModule = module {
    single { createUberDatabase(androidContext()) }
    single<FaqFileReader> { FaqFileReaderImpl(androidContext()) }
}