package br.com.digio.uber.di

import br.com.digio.uber.abstractFirebase.di.firebaseAbstractModule
import br.com.digio.uber.abstractFirebase.extension.getFirebaseMessagingToken
import br.com.digio.uber.analytics.di.firebaseModule
import br.com.digio.uber.common.dialogs.viewmodel.InformativeViewModel
import br.com.digio.uber.common.helper.ResourceManager
import br.com.digio.uber.common.navigation.Navigation
import br.com.digio.uber.common.navigation.NavigationImpl
import br.com.digio.uber.di.dataset.datasetModule
import br.com.digio.uber.di.db.dbModule
import br.com.digio.uber.di.gateway.gatewayModule
import br.com.digio.uber.di.repository.repositoryModule
import br.com.digio.uber.domain.di.useCaseModule
import br.com.digio.uber.domain.lexis.LexisDefenderInitializer
import br.com.digio.uber.gemalto.di.gemaltoModule
import br.com.digio.uber.marketingCloud.di.marketingCloudModule
import br.com.digio.uber.model.request.DeviceInfo
import br.com.digio.uber.util.Const
import br.com.digio.uber.util.GPSUtil
import br.com.digio.uber.util.TokenPreferencesUtils
import br.com.digio.uber.util.getAndroidId
import br.com.digio.uber.util.getDeviceInfo
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.core.qualifier.qualifier
import org.koin.dsl.module

val uberModule = module {
    single { ResourceManager(androidContext()) }
    single<Navigation> { NavigationImpl() }
    single {
        LexisDefenderInitializer(
            androidContext()
        )
    }
    single { GPSUtil.getInstance(androidContext()) }
    single(qualifier = qualifier(Const.Qualifier.ANDROID_ID)) { androidContext().getAndroidId() }
    single<Deferred<DeviceInfo>> {
        GlobalScope.async {
            androidContext().getDeviceInfo(
                getFirebaseMessagingToken(),
                get(qualifier = qualifier(Const.Qualifier.ANDROID_ID))
            )
        }
    }
    single { TokenPreferencesUtils.getInstance(androidContext()) }

    viewModel { InformativeViewModel() }
}

val uberModules: List<Module> = mutableListOf(
    uberModule,
    gatewayModule,
    datasetModule,
    dbModule,
    repositoryModule,
    useCaseModule,
    firebaseModule,
    gemaltoModule,
    firebaseAbstractModule,
    marketingCloudModule
)