package br.com.digio.uber.di.gateway

import br.com.digio.uber.gateway.client.DigioTestClient
import br.com.digio.uber.gateway.client.UberClient
import br.com.digio.uber.gateway.interceptors.WebTokenInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val gatewayModule = module {
    single { WebTokenInterceptor.getInstance(get()) }
    single { DigioTestClient(androidContext()) }
    single { UberClient(androidContext(), get()) }
}