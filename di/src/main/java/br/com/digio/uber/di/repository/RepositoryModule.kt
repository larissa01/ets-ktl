package br.com.digio.uber.di.repository

import br.com.digio.uber.repository.account.AccountChangeRegistrationRepository
import br.com.digio.uber.repository.account.AccountChangeRegistrationRepositoryImpl
import br.com.digio.uber.repository.account.AccountRepository
import br.com.digio.uber.repository.account.AccountRepositoryImpl
import br.com.digio.uber.repository.balance.BalanceRepository
import br.com.digio.uber.repository.balance.BalanceRepositoryImpl
import br.com.digio.uber.repository.bankSlip.BankSlipRepository
import br.com.digio.uber.repository.bankSlip.BankSlipRepositoryImpl
import br.com.digio.uber.repository.card.CardReissueRepository
import br.com.digio.uber.repository.card.CardReissueRepositoryImpl
import br.com.digio.uber.repository.card.CardRepository
import br.com.digio.uber.repository.card.CardRepositoryImpl
import br.com.digio.uber.repository.cardReceived.CardReceivedRepository
import br.com.digio.uber.repository.cardReceived.CardReceivedRepositoryImpl
import br.com.digio.uber.repository.cashback.CashbackRepository
import br.com.digio.uber.repository.cashback.CashbackRepositoryImpl
import br.com.digio.uber.repository.digioTestRepository.DigioTestRepository
import br.com.digio.uber.repository.digioTestRepository.DigioTestRepositoryImpl
import br.com.digio.uber.repository.eyes.EyesRepository
import br.com.digio.uber.repository.eyes.EyesRepositoryImpl
import br.com.digio.uber.repository.faq.FaqRepository
import br.com.digio.uber.repository.faq.FaqRepositoryImpl
import br.com.digio.uber.repository.franchise.FranchiseRepository
import br.com.digio.uber.repository.franchise.FranchiseRepositoryImpl
import br.com.digio.uber.repository.gemalto.GemaltoRepository
import br.com.digio.uber.repository.gemalto.GemaltoRepositoryImpl
import br.com.digio.uber.repository.home.ConfigFeatureRepository
import br.com.digio.uber.repository.home.ConfigFeatureRepositoryImpl
import br.com.digio.uber.repository.income.IncomeRepository
import br.com.digio.uber.repository.income.IncomeRepositoryImpl
import br.com.digio.uber.repository.location.LocationRepository
import br.com.digio.uber.repository.location.LocationRepositoryImpl
import br.com.digio.uber.repository.login.AccountNotLoggedInRepository
import br.com.digio.uber.repository.login.AccountNotLoggedInRepositoryImpl
import br.com.digio.uber.repository.payment.AccountPaymentRepository
import br.com.digio.uber.repository.payment.AccountPaymentRepositoryImpl
import br.com.digio.uber.repository.payment.QrCodeRepository
import br.com.digio.uber.repository.payment.QrCodeRepositoryImpl
import br.com.digio.uber.repository.pid.PidRepository
import br.com.digio.uber.repository.pid.PidRepositoryImpl
import br.com.digio.uber.repository.recharge.RechargeRepository
import br.com.digio.uber.repository.recharge.RechargeRepositoryImpl
import br.com.digio.uber.repository.remuneration.RemunerationRepository
import br.com.digio.uber.repository.remuneration.RemunerationRepositoryImpl
import br.com.digio.uber.repository.repository.ReceiptRepository
import br.com.digio.uber.repository.repository.ReceiptRepositoryImpl
import br.com.digio.uber.repository.statement.StatementRepository
import br.com.digio.uber.repository.statement.StatementRepositoryImpl
import br.com.digio.uber.repository.store.StoreRepository
import br.com.digio.uber.repository.store.StoreRepositoryImpl
import br.com.digio.uber.repository.tracking.TrackingRepository
import br.com.digio.uber.repository.tracking.TrackingRepositoryImpl
import br.com.digio.uber.repository.transfer.TransferRepository
import br.com.digio.uber.repository.transfer.TransferRepositoryImpl
import br.com.digio.uber.repository.virtualcard.VirtualCardOnboardingRepository
import br.com.digio.uber.repository.virtualcard.VirtualCardOnboardingRepositoryImpl
import br.com.digio.uber.repository.virtualcard.VirtualCardRepository
import br.com.digio.uber.repository.virtualcard.VirtualCardRepositoryImpl
import br.com.digio.uber.repository.withdraw.WithdrawRepository
import br.com.digio.uber.repository.withdraw.WithdrawRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {
    single<DigioTestRepository> { DigioTestRepositoryImpl(get()) }
    single<BankSlipRepository> { BankSlipRepositoryImpl(get()) }
    single<TransferRepository> { TransferRepositoryImpl(get(), get()) }
    single<AccountNotLoggedInRepository> { AccountNotLoggedInRepositoryImpl(get(), get(), get()) }
    single<GemaltoRepository> { GemaltoRepositoryImpl(get(), get(), get(), get()) }
    single<RechargeRepository> { RechargeRepositoryImpl(get()) }
    single<BalanceRepository> { BalanceRepositoryImpl(get()) }
    single<EyesRepository> { EyesRepositoryImpl(get()) }
    single<FaqRepository> { FaqRepositoryImpl(get(), get(), get()) }
    single<ConfigFeatureRepository> { ConfigFeatureRepositoryImpl() }
    single<ReceiptRepository> { ReceiptRepositoryImpl(get()) }
    single<CardRepository> { CardRepositoryImpl(get()) }
    single<VirtualCardRepository> { VirtualCardRepositoryImpl(get()) }
    single<StatementRepository> { StatementRepositoryImpl(get()) }
    single<QrCodeRepository> { QrCodeRepositoryImpl(get()) }
    single<StoreRepository> { StoreRepositoryImpl(get()) }
    single<RemunerationRepository> { RemunerationRepositoryImpl(get()) }
    single<WithdrawRepository> { WithdrawRepositoryImpl(get()) }
    single<IncomeRepository> { IncomeRepositoryImpl(get()) }
    single<AccountRepository> { AccountRepositoryImpl(get(), get(), get(), get(), get()) }
    single<PidRepository> { PidRepositoryImpl(get()) }
    single<AccountPaymentRepository> { AccountPaymentRepositoryImpl(get()) }
    single<CashbackRepository> { CashbackRepositoryImpl(get()) }
    single<LocationRepository> { LocationRepositoryImpl(androidContext()) }
    single<TrackingRepository> { TrackingRepositoryImpl(get()) }
    single<AccountChangeRegistrationRepository> { AccountChangeRegistrationRepositoryImpl(get()) }
    single<CardReceivedRepository> { CardReceivedRepositoryImpl(get()) }
    single<CardReissueRepository> { CardReissueRepositoryImpl(get()) }
    single<VirtualCardOnboardingRepository> { VirtualCardOnboardingRepositoryImpl(get()) }
    single<FranchiseRepository> { FranchiseRepositoryImpl(get()) }
}