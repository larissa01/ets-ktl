package br.com.digio.uber.di.dataset

import br.com.digio.uber.dataset.account.AccountDataSet
import br.com.digio.uber.dataset.account.AccountDataSetImpl
import br.com.digio.uber.dataset.balance.BalanceDataSet
import br.com.digio.uber.dataset.balance.BalanceDataSetImpl
import br.com.digio.uber.dataset.bankSlip.BankSlipDataSet
import br.com.digio.uber.dataset.bankSlip.BankSlipDataSetImpl
import br.com.digio.uber.dataset.card.CardDataSet
import br.com.digio.uber.dataset.card.CardDataSetImpl
import br.com.digio.uber.dataset.card.CardReissueDataSet
import br.com.digio.uber.dataset.card.CardReissueDataSetImpl
import br.com.digio.uber.dataset.cardReceived.CardReceivedDataSet
import br.com.digio.uber.dataset.cardReceived.CardReceivedDataSetImpl
import br.com.digio.uber.dataset.cashback.CashbackDataSet
import br.com.digio.uber.dataset.cashback.CashbackDataSetImpl
import br.com.digio.uber.dataset.digioTest.DigioTestDataSet
import br.com.digio.uber.dataset.digioTest.DigioTestDataSetImpl
import br.com.digio.uber.dataset.faq.FaqDataset
import br.com.digio.uber.dataset.faq.FaqDatasetImpl
import br.com.digio.uber.dataset.franchise.FranchiseDataSet
import br.com.digio.uber.dataset.franchise.FranchiseDataSetImpl
import br.com.digio.uber.dataset.income.IncomeDataSet
import br.com.digio.uber.dataset.income.IncomeDataSetImpl
import br.com.digio.uber.dataset.login.LoginLoggedDataset
import br.com.digio.uber.dataset.login.LoginLoggedDatasetImpl
import br.com.digio.uber.dataset.login.LoginNotLoggedInDataset
import br.com.digio.uber.dataset.login.LoginNotLoggedInDatasetImpl
import br.com.digio.uber.dataset.payment.AccountPaymentDataset
import br.com.digio.uber.dataset.payment.AccountPaymentDatasetImpl
import br.com.digio.uber.dataset.payment.QrCodeDataSet
import br.com.digio.uber.dataset.payment.QrCodeDataSetImpl
import br.com.digio.uber.dataset.pid.PidDataset
import br.com.digio.uber.dataset.pid.PidDatasetImpl
import br.com.digio.uber.dataset.receipt.ReceiptDataSet
import br.com.digio.uber.dataset.receipt.ReceiptDataSetImpl
import br.com.digio.uber.dataset.recharge.RechargeDataSet
import br.com.digio.uber.dataset.recharge.RechargeDataSetImpl
import br.com.digio.uber.dataset.remuneration.RemunerationDataSet
import br.com.digio.uber.dataset.remuneration.RemunerationDataSetImpl
import br.com.digio.uber.dataset.statement.StatementDataSet
import br.com.digio.uber.dataset.statement.StatementDataSetImpl
import br.com.digio.uber.dataset.store.StoreDataSet
import br.com.digio.uber.dataset.store.StoreDataSetImpl
import br.com.digio.uber.dataset.tracking.TrackingDataSet
import br.com.digio.uber.dataset.tracking.TrackingDataSetImpl
import br.com.digio.uber.dataset.transfer.TransferDataSet
import br.com.digio.uber.dataset.transfer.TransferDataSetImpl
import br.com.digio.uber.dataset.virtual.card.VirtualCardDataSet
import br.com.digio.uber.dataset.virtual.card.VirtualCardDataSetImpl
import br.com.digio.uber.dataset.virtual.card.VirtualCardOnboardingDataSet
import br.com.digio.uber.dataset.virtual.card.VirtualCardOnboardingDataSetImpl
import br.com.digio.uber.dataset.withdraw.WithdrawDataSet
import br.com.digio.uber.dataset.withdraw.WithdrawDataSetImpl
import org.koin.dsl.module

val datasetModule = module {
    single<DigioTestDataSet> { DigioTestDataSetImpl(get()) }
    single<BankSlipDataSet> { BankSlipDataSetImpl(get()) }
    single<FaqDataset> { FaqDatasetImpl(get()) }
    single<BalanceDataSet> { BalanceDataSetImpl(get()) }
    single<LoginNotLoggedInDataset> { LoginNotLoggedInDatasetImpl(get()) }
    single<TransferDataSet> { TransferDataSetImpl(get()) }
    single<LoginLoggedDataset> { LoginLoggedDatasetImpl(get()) }
    single<RechargeDataSet> { RechargeDataSetImpl(get()) }
    single<ReceiptDataSet> { ReceiptDataSetImpl(get()) }
    single<CardDataSet> { CardDataSetImpl(get()) }
    single<VirtualCardDataSet> { VirtualCardDataSetImpl(get()) }
    single<QrCodeDataSet> { QrCodeDataSetImpl(get()) }
    single<RemunerationDataSet> { RemunerationDataSetImpl(get()) }
    single<StatementDataSet> { StatementDataSetImpl(get()) }
    single<StoreDataSet> { StoreDataSetImpl(get()) }
    single<AccountDataSet> { AccountDataSetImpl(get()) }
    single<WithdrawDataSet> { WithdrawDataSetImpl(get()) }
    single<PidDataset> { PidDatasetImpl(get()) }
    single<IncomeDataSet> { IncomeDataSetImpl(get()) }
    single<CashbackDataSet> { CashbackDataSetImpl(get()) }
    single<TrackingDataSet> { TrackingDataSetImpl(get()) }
    single<AccountPaymentDataset> { AccountPaymentDatasetImpl(get()) }
    single<CardReceivedDataSet> { CardReceivedDataSetImpl(get()) }
    single<CardReissueDataSet> { CardReissueDataSetImpl(get()) }
    single<VirtualCardOnboardingDataSet> { VirtualCardOnboardingDataSetImpl() }
    single<FranchiseDataSet> { FranchiseDataSetImpl(get()) }
}