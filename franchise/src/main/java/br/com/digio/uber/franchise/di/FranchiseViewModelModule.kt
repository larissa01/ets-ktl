package br.com.digio.uber.franchise.di

import br.com.digio.uber.franchise.viewmodel.FranchiseViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val franchiseModule = module {
    viewModel { FranchiseViewModel(get()) }
}