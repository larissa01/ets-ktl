package br.com.digio.uber.franchise.databinding

import android.widget.TextView
import androidx.databinding.BindingAdapter
import br.com.digio.uber.common.util.MaskUtils

@BindingAdapter("toMonetaryValue")
fun toMonetaryValue(view: TextView, value: Double) {
    view.text = MaskUtils.floatToMonetary(value)
}