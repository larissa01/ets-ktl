package br.com.digio.uber.franchise.dataui

import br.com.digio.uber.model.franchise.FranchiseLimitsData

data class FranchiseDataBase(
    var tedLimitsData: FranchiseLimitsData? = null,
    var p2PTransferLimitsData: FranchiseLimitsData? = null,
    var withdrawLimitsData: FranchiseLimitsData? = null,
    var bankSlipLimitsData: FranchiseLimitsData? = null,
    var instantPaymentData: FranchiseLimitsData? = null,
    var billPaymentLimitsData: FranchiseLimitsData? = null
)