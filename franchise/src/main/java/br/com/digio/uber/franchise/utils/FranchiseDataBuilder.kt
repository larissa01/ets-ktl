package br.com.digio.uber.franchise.utils

import br.com.digio.uber.franchise.R
import br.com.digio.uber.franchise.dataui.FranchiseDataBase
import br.com.digio.uber.model.franchise.FranchiseLimitsData

class FranchiseDataBuilder(private val type: String) {
    companion object {
        private const val GREEN = "courier"
        private const val BLUE = "TIER_1"
        private const val GOLD = "TIER_2"
        private const val PLATINUM = "TIER_3"
        private const val DIAMOND = "TIER_4"
        private const val INACTIVE = "inactive"

        private const val GREEN_NAME = "Verde"
        private const val BLUE_NAME = "Azul"
        private const val GOLD_NAME = "Ouro"
        private const val PLATINUM_NAME = "Platina"
        private const val DIAMOND_NAME = "Diamante"
        private const val INACTIVE_NAME = "Inativo"

        private val mapColors = hashMapOf(
            GREEN to R.color.green_05A357,
            BLUE to R.color.blue_276EF1,
            GOLD to R.color.yellow_FEBF43,
            PLATINUM to R.color.grey_8EA1AB,
            DIAMOND to R.color.black,
            INACTIVE to R.color.red_E11900
        )

        private val mapTiers = hashMapOf(
            GREEN to GREEN_NAME,
            BLUE to BLUE_NAME,
            GOLD to GOLD_NAME,
            PLATINUM to PLATINUM_NAME,
            DIAMOND to DIAMOND_NAME,
            INACTIVE to INACTIVE_NAME
        )

        private val mapIcon = hashMapOf(
            GREEN to R.drawable.ic_top_green,
            BLUE to R.drawable.ic_top_tier_blue,
            GOLD to R.drawable.ic_top_tier_gold,
            PLATINUM to R.drawable.ic_top_tier_platina,
            DIAMOND to R.drawable.ic_top_tier_diamond,
            INACTIVE to 0
        )

        fun buildColor(color: String): Int {
            return mapColors[color] ?: R.color.red_E11900
        }

        fun buildIcon(color: String): Int {
            return mapIcon[color] ?: 0
        }

        fun buildTierName(color: String): String {
            return mapTiers[color] ?: ""
        }
    }

    fun setFranchiseData(dataBase: List<FranchiseLimitsData>): FranchiseDataBase {
        return FranchiseDataBase(
            tedLimitsData = dataBase.findLast { it.limitCategory == LimitCategory.EXT_TRANSFER.name },
            p2PTransferLimitsData = dataBase.findLast { it.limitCategory == LimitCategory.INT_TRANSFER.name },
            withdrawLimitsData = dataBase.findLast { it.limitCategory == LimitCategory.WITHDRAW.name },
            bankSlipLimitsData = dataBase.findLast { it.limitCategory == LimitCategory.BANKSLIP.name },
            instantPaymentData = dataBase.findLast { it.limitCategory == LimitCategory.INSTANT_PAYMENT.name },
            billPaymentLimitsData = dataBase.findLast { it.limitCategory == LimitCategory.BILL_PAYMENT.name }
        )
    }

    enum class LimitCategory {
        EXT_TRANSFER, INT_TRANSFER, WITHDRAW, BANKSLIP, INSTANT_PAYMENT, BILL_PAYMENT
    }
}