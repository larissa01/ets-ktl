package br.com.digio.uber.franchise.viewmodel

import androidx.lifecycle.MutableLiveData
import br.com.digio.uber.common.base.viewmodel.BaseViewModel
import br.com.digio.uber.domain.usecase.franchise.FranchiseUseCase
import br.com.digio.uber.franchise.dataui.FranchiseDataBase
import br.com.digio.uber.franchise.utils.FranchiseDataBuilder

class FranchiseViewModel(
    private val franchiseUseCase: FranchiseUseCase
) : BaseViewModel() {

    val tier: MutableLiveData<String> = MutableLiveData()
    val data: MutableLiveData<FranchiseDataBase> = MutableLiveData()
    val backgroundColor: MutableLiveData<Int> = MutableLiveData()
    val isLoading: MutableLiveData<Boolean> = MutableLiveData(true)
    val icon: MutableLiveData<Int> = MutableLiveData()

    fun getFranchiseData() {
        franchiseUseCase(Unit).singleExec(
            onError = { _, error, _ ->
                error
            },
            onSuccessBaseViewModel = { franchiseResponse ->
                isLoading.value = false
                tier.value = FranchiseDataBuilder.buildTierName(franchiseResponse.code)
                data.value =
                    FranchiseDataBuilder(franchiseResponse.code).setFranchiseData(franchiseResponse.transactionalLimits)
                backgroundColor.value = FranchiseDataBuilder.buildColor(franchiseResponse.code)
                icon.value = FranchiseDataBuilder.buildIcon(franchiseResponse.code)
            }
        )
    }
}