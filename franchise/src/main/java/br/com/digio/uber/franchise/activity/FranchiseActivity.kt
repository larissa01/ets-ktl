package br.com.digio.uber.franchise.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import br.com.digio.uber.common.base.activity.BaseViewModelActivity
import br.com.digio.uber.franchise.BR
import br.com.digio.uber.franchise.R
import br.com.digio.uber.franchise.databinding.ActivityFranchiseBinding
import br.com.digio.uber.franchise.di.franchiseModule
import br.com.digio.uber.franchise.viewmodel.FranchiseViewModel
import org.koin.android.ext.android.inject
import org.koin.core.module.Module

class FranchiseActivity : BaseViewModelActivity<ActivityFranchiseBinding, FranchiseViewModel>() {

    override val viewModel: FranchiseViewModel by inject()
    override val bindingVariable: Int = BR.franchiseViewModel

    override fun showDisplayShowTitle(): Boolean = true
    override fun getLayoutId(): Int = R.layout.activity_franchise
    override fun getModule(): List<Module> = listOf(franchiseModule)

    override fun getToolbar(): Toolbar? = binding?.franchiseToolbar

    override fun initialize(savedInstanceState: Bundle?) {
        super.initialize(savedInstanceState)

        viewModel.getFranchiseData()
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.backgroundColor.observe(this) {
            binding?.containerTier?.setBackgroundColor(ContextCompat.getColor(this, it))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}